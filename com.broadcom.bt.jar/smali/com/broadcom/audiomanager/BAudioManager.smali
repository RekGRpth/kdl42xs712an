.class public Lcom/broadcom/audiomanager/BAudioManager;
.super Ljava/lang/Object;
.source "BAudioManager.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "BAudioManager"

.field static mInstance:Lcom/broadcom/audiomanager/BAudioManager;

.field static final mInstanceSync:Ljava/lang/Object;


# instance fields
.field mService:Landroid/media/IAudioService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/broadcom/audiomanager/BAudioManager;->mInstanceSync:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/broadcom/audiomanager/BAudioManager;->mInstance:Lcom/broadcom/audiomanager/BAudioManager;

    return-void
.end method

.method private constructor <init>(Landroid/media/IAudioService;)V
    .locals 1
    .param p1    # Landroid/media/IAudioService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/broadcom/audiomanager/BAudioManager;->mService:Landroid/media/IAudioService;

    iput-object p1, p0, Lcom/broadcom/audiomanager/BAudioManager;->mService:Landroid/media/IAudioService;

    return-void
.end method

.method public static getInstance()Lcom/broadcom/audiomanager/BAudioManager;
    .locals 4

    sget-object v1, Lcom/broadcom/audiomanager/BAudioManager;->mInstance:Lcom/broadcom/audiomanager/BAudioManager;

    if-nez v1, :cond_1

    sget-object v2, Lcom/broadcom/audiomanager/BAudioManager;->mInstanceSync:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/broadcom/audiomanager/BAudioManager;->mInstance:Lcom/broadcom/audiomanager/BAudioManager;

    if-nez v1, :cond_0

    const-string v1, "audio"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Lcom/broadcom/audiomanager/BAudioManager;

    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/broadcom/audiomanager/BAudioManager;-><init>(Landroid/media/IAudioService;)V

    sput-object v1, Lcom/broadcom/audiomanager/BAudioManager;->mInstance:Lcom/broadcom/audiomanager/BAudioManager;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v1, Lcom/broadcom/audiomanager/BAudioManager;->mInstance:Lcom/broadcom/audiomanager/BAudioManager;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public isBluetoothMicOn()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/audiomanager/BAudioManager;->mService:Landroid/media/IAudioService;

    invoke-interface {v1}, Landroid/media/IAudioService;->isBluetoothMicOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "BAudioManager"

    const-string v2, "Dead object in isBluetoothMicOn"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBluetoothMicOn(Z)V
    .locals 3
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/audiomanager/BAudioManager;->mService:Landroid/media/IAudioService;

    invoke-interface {v1, p1}, Landroid/media/IAudioService;->setBluetoothMicOn(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BAudioManager"

    const-string v2, "Dead object in setBluetoothMicOn"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
