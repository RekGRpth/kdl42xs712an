.class Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;
.super Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback$Stub;
.source "FmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FmReceiverCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;


# direct methods
.method private constructor <init>(Lcom/broadcom/fm/fmreceiver/FmProxy;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/fm/fmreceiver/FmProxy;Lcom/broadcom/fm/fmreceiver/FmProxy$1;)V
    .locals 0
    .param p1    # Lcom/broadcom/fm/fmreceiver/FmProxy;
    .param p2    # Lcom/broadcom/fm/fmreceiver/FmProxy$1;

    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;-><init>(Lcom/broadcom/fm/fmreceiver/FmProxy;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized onAudioModeEvent(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onAudioModeEvent(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onAudioPathEvent(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onAudioPathEvent(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onEstimateNflEvent(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onEstimateNoiseFloorLevelEvent(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onLiveAudioQualityEvent(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onLiveAudioQualityEvent(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onRdsDataEvent(IILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onRdsDataEvent(IILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onRdsModeEvent(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onRdsModeEvent(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onSeekCompleteEvent(IIIZ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onSeekCompleteEvent(IIIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onStatusEvent(IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-interface/range {v0 .. v9}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onStatusEvent(IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onVolumeEvent(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onVolumeEvent(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onWorldRegionEvent(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onWorldRegionEvent(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
