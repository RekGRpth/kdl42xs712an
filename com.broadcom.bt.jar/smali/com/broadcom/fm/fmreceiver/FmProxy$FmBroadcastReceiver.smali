.class Lcom/broadcom/fm/fmreceiver/FmProxy$FmBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FmBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;


# direct methods
.method private constructor <init>(Lcom/broadcom/fm/fmreceiver/FmProxy;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmBroadcastReceiver;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v4, -0x7e

    const/4 v11, 0x0

    const/4 v6, -0x1

    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$FmBroadcastReceiver;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    invoke-static {v1}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/broadcom/fm/fmreceiver/FmProxy$FmBroadcastReceiver;->abortBroadcast()V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    const-string v1, "com.broadcom.bt.app.fm.action.ON_STATUS"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "FREQ"

    invoke-virtual {p2, v1, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "RSSI"

    invoke-virtual {p2, v2, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "SNR"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "RADIO_ON"

    invoke-virtual {p2, v4, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "RDS_PRGM_TYPE"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v6, "RDS_PRGM_SVC"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "RDS_TXT"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "RDS_PRGM_TYPE_NAME"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "MUTED"

    invoke-virtual {p2, v9, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    invoke-interface/range {v0 .. v9}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onStatusEvent(IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const-string v1, "com.broadcom.bt.app.fm.action.ON_AUDIO_MODE"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "AUDIO_MODE"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onAudioModeEvent(I)V

    goto :goto_0

    :cond_3
    const-string v1, "com.broadcom.bt.app.fm.action.ON_AUDIO_PATH"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "AUDIO_PATH"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onAudioPathEvent(I)V

    goto :goto_0

    :cond_4
    const-string v1, "com.broadcom.bt.app.fm.action.ON_AUDIO_QUAL"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "RSSI"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "SNR"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onLiveAudioQualityEvent(II)V

    goto/16 :goto_0

    :cond_5
    const-string v1, "com.broadcom.bt.app.fm.action.ON_EST_NFL"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "NFL"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onEstimateNoiseFloorLevelEvent(I)V

    goto/16 :goto_0

    :cond_6
    const-string v1, "com.broadcom.bt.app.fm.action.ON_RDS_DATA"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "RDS_DATA_TYPE"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "RDS_IDX"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "RDS_TXT"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onRdsDataEvent(IILjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v1, "com.broadcom.bt.app.fm.action.ON_RDS_MODE"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "RDS_MODE"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "ALT_FREQ_MODE"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onRdsModeEvent(II)V

    goto/16 :goto_0

    :cond_8
    const-string v1, "com.broadcom.bt.app.fm.action.ON_SEEK_CMPL"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "FREQ"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "RSSI"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "SNR"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "RDS_SUCCESS"

    invoke-virtual {p2, v4, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onSeekCompleteEvent(IIIZ)V

    goto/16 :goto_0

    :cond_9
    const-string v1, "ON_VOL"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "STATUS"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "VOL"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onVolumeEvent(II)V

    goto/16 :goto_0

    :cond_a
    const-string v1, "com.broadcom.bt.app.fm.action.ON_WRLD_RGN"

    # getter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$200()I

    move-result v2

    invoke-static {v1, v10, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "WRLD_RGN"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;->onWorldRegionEvent(I)V

    goto/16 :goto_0
.end method
