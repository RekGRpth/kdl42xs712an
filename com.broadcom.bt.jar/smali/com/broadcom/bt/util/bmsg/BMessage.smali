.class public Lcom/broadcom/bt/util/bmsg/BMessage;
.super Lcom/broadcom/bt/util/bmsg/BMessageBase;
.source "BMessage.java"


# static fields
.field private static final ERR_CHECK:Z = true

.field private static final TAG:Ljava/lang/String; = "BMessage"


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/broadcom/bt/util/bmsg/BMessageBase;-><init>()V

    invoke-static {}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->createBMsg()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessage;->setNativeRef(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create BMesage object"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Lcom/broadcom/bt/util/bmsg/BMessageBase;-><init>()V

    invoke-virtual {p0, p1}, Lcom/broadcom/bt/util/bmsg/BMessage;->setNativeRef(I)Z

    return-void
.end method

.method public static parse(I)Lcom/broadcom/bt/util/bmsg/BMessage;
    .locals 2
    .param p0    # I

    invoke-static {p0}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->parseBMsgFileFD(I)I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessage;

    invoke-direct {v1, v0}, Lcom/broadcom/bt/util/bmsg/BMessage;-><init>(I)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parse(Ljava/io/File;)Lcom/broadcom/bt/util/bmsg/BMessage;
    .locals 5
    .param p0    # Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const-string v2, "BMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to parse "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Invalid file."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->parseBMsgFile(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessage;

    invoke-direct {v1, v0}, Lcom/broadcom/bt/util/bmsg/BMessage;-><init>(I)V

    goto :goto_0
.end method


# virtual methods
.method public addEnvelope()Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    .locals 4

    iget v2, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v2}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->addBMsgEnv(I)I

    move-result v0

    if-gtz v0, :cond_0

    const-string v2, "BMessage"

    const-string v3, "Unable to create native Envelope object for BMessage"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    invoke-direct {v1, p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;-><init>(Lcom/broadcom/bt/util/bmsg/BMessage;I)V

    goto :goto_0
.end method

.method public addOriginator()Lcom/broadcom/bt/util/bmsg/BMessageVCard;
    .locals 4

    iget v2, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v2}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->addBMsgOrig(I)I

    move-result v0

    if-gtz v0, :cond_0

    const-string v2, "BMessage"

    const-string v3, "Unable to create native VCard for BMessage originator object"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessageVCard;

    invoke-direct {v1, p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;-><init>(Lcom/broadcom/bt/util/bmsg/BMessageBase;I)V

    goto :goto_0
.end method

.method public decodeSMSSubmitPDU(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BMessage"

    const-string v1, "decodeSMSSubmitPDU"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->decodeSMSSubmitPDU(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public encodeSMSDeliverPDU(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v0, "BMessage"

    const-string v1, "encodeSMSDeliverPDU"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2, p3, p4}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->encodeSMSDeliverPDU(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 0

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessage;->finish()V

    return-void
.end method

.method public finish()V
    .locals 1

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessage;->isNativeCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->deleteBMsg(I)V

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessage;->clearNativeRef()V

    :cond_0
    return-void
.end method

.method public getEnvelope()Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    .locals 3

    iget v2, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v2}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->getBMsgEnv(I)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    invoke-direct {v1, p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;-><init>(Lcom/broadcom/bt/util/bmsg/BMessage;I)V

    goto :goto_0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->getBMsgFldr(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMessageType()B
    .locals 1

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->getBMsgMType(I)B

    move-result v0

    return v0
.end method

.method public getOriginator()Lcom/broadcom/bt/util/bmsg/BMessageVCard;
    .locals 3

    iget v2, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v2}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->getBMsgOrig(I)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessageVCard;

    invoke-direct {v1, p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;-><init>(Lcom/broadcom/bt/util/bmsg/BMessageBase;I)V

    goto :goto_0
.end method

.method public isRead()Z
    .locals 1

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->isBMsgRd(I)Z

    move-result v0

    return v0
.end method

.method public setFolder(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0, p1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->setBMsgFldr(ILjava/lang/String;)V

    return-void
.end method

.method public setMessageType(B)V
    .locals 3
    .param p1    # B

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->hasBitError(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BMessage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid message type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0, p1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->setBMsgMType(IB)V

    goto :goto_0
.end method

.method public setReadStatus(Z)V
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0, p1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->setBMsgRd(IZ)V

    return-void
.end method

.method public write(I)Z
    .locals 3
    .param p1    # I

    if-lez p1, :cond_0

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-static {v0, p1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->writeBMsgFileFD(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "BMessage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to write bmessage to file descriptor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public write(Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BMessage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to write to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". File already exists."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessage;->mNativeObjectRef:I

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->writeBMsgFile(ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
