.class public Lcom/broadcom/bt/util/io/output/CloseShieldOutputStream;
.super Lcom/broadcom/bt/util/io/output/ProxyOutputStream;
.source "CloseShieldOutputStream.java"


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0
    .param p1    # Ljava/io/OutputStream;

    invoke-direct {p0, p1}, Lcom/broadcom/bt/util/io/output/ProxyOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    new-instance v0, Lcom/broadcom/bt/util/io/output/ClosedOutputStream;

    invoke-direct {v0}, Lcom/broadcom/bt/util/io/output/ClosedOutputStream;-><init>()V

    iput-object v0, p0, Lcom/broadcom/bt/util/io/output/CloseShieldOutputStream;->out:Ljava/io/OutputStream;

    return-void
.end method
