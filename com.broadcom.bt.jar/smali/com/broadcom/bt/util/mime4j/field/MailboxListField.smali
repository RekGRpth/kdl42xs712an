.class public Lcom/broadcom/bt/util/mime4j/field/MailboxListField;
.super Lcom/broadcom/bt/util/mime4j/field/Field;
.source "MailboxListField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/util/mime4j/field/MailboxListField$Parser;
    }
.end annotation


# instance fields
.field private mailboxList:Lcom/broadcom/bt/util/mime4j/field/address/MailboxList;

.field private parseException:Lcom/broadcom/bt/util/mime4j/field/address/parser/ParseException;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/broadcom/bt/util/mime4j/field/address/MailboxList;Lcom/broadcom/bt/util/mime4j/field/address/parser/ParseException;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/broadcom/bt/util/mime4j/field/address/MailboxList;
    .param p5    # Lcom/broadcom/bt/util/mime4j/field/address/parser/ParseException;

    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/util/mime4j/field/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/broadcom/bt/util/mime4j/field/MailboxListField;->mailboxList:Lcom/broadcom/bt/util/mime4j/field/address/MailboxList;

    iput-object p5, p0, Lcom/broadcom/bt/util/mime4j/field/MailboxListField;->parseException:Lcom/broadcom/bt/util/mime4j/field/address/parser/ParseException;

    return-void
.end method


# virtual methods
.method public getMailboxList()Lcom/broadcom/bt/util/mime4j/field/address/MailboxList;
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/util/mime4j/field/MailboxListField;->mailboxList:Lcom/broadcom/bt/util/mime4j/field/address/MailboxList;

    return-object v0
.end method

.method public getParseException()Lcom/broadcom/bt/util/mime4j/field/address/parser/ParseException;
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/util/mime4j/field/MailboxListField;->parseException:Lcom/broadcom/bt/util/mime4j/field/address/parser/ParseException;

    return-object v0
.end method
