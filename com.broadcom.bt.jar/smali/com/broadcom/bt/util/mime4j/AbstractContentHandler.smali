.class public abstract Lcom/broadcom/bt/util/mime4j/AbstractContentHandler;
.super Ljava/lang/Object;
.source "AbstractContentHandler.java"

# interfaces
.implements Lcom/broadcom/bt/util/mime4j/ContentHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public body(Lcom/broadcom/bt/util/mime4j/BodyDescriptor;Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/util/mime4j/BodyDescriptor;
    .param p2    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public endBodyPart()V
    .locals 0

    return-void
.end method

.method public endHeader()V
    .locals 0

    return-void
.end method

.method public endMessage()V
    .locals 0

    return-void
.end method

.method public endMultipart()V
    .locals 0

    return-void
.end method

.method public epilogue(Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public field(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public preamble(Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public raw(Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public startBodyPart()V
    .locals 0

    return-void
.end method

.method public startHeader()V
    .locals 0

    return-void
.end method

.method public startMessage()V
    .locals 0

    return-void
.end method

.method public startMultipart(Lcom/broadcom/bt/util/mime4j/BodyDescriptor;)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/util/mime4j/BodyDescriptor;

    return-void
.end method
