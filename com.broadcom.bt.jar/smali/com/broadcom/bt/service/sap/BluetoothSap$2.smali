.class Lcom/broadcom/bt/service/sap/BluetoothSap$2;
.super Ljava/lang/Object;
.source "BluetoothSap.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/sap/BluetoothSap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/sap/BluetoothSap;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "Bluetoothsap"

    const-string v1, "BluetoothSAP Proxy object connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    invoke-static {p2}, Lcom/broadcom/bt/service/sap/IBluetoothSap$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/bt/service/sap/IBluetoothSap;

    move-result-object v1

    # setter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mSapService:Lcom/broadcom/bt/service/sap/IBluetoothSap;
    invoke-static {v0, v1}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$202(Lcom/broadcom/bt/service/sap/BluetoothSap;Lcom/broadcom/bt/service/sap/IBluetoothSap;)Lcom/broadcom/bt/service/sap/IBluetoothSap;

    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/16 v1, 0x65

    iget-object v2, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    invoke-interface {v0, v1, v2}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "Bluetoothsap"

    const-string v1, "BluetoothSAP Proxy object disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    const/4 v1, 0x0

    # setter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mSapService:Lcom/broadcom/bt/service/sap/IBluetoothSap;
    invoke-static {v0, v1}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$202(Lcom/broadcom/bt/service/sap/BluetoothSap;Lcom/broadcom/bt/service/sap/IBluetoothSap;)Lcom/broadcom/bt/service/sap/IBluetoothSap;

    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/16 v1, 0x65

    invoke-interface {v0, v1}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceDisconnected(I)V

    :cond_0
    return-void
.end method
