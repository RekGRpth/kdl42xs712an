.class public final Lcom/broadcom/bt/gatt/BluetoothGattServer;
.super Ljava/lang/Object;
.source "BluetoothGattServer.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile;


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "BluetoothGattServer"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothGattServerCallback:Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

.field private final mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

.field private mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mServerIf:B

.field private mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

.field private mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mServices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/gatt/BluetoothGattService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v3, Lcom/broadcom/bt/gatt/BluetoothGattServer$1;

    invoke-direct {v3, p0}, Lcom/broadcom/bt/gatt/BluetoothGattServer$1;-><init>(Lcom/broadcom/bt/gatt/BluetoothGattServer;)V

    iput-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    new-instance v3, Lcom/broadcom/bt/gatt/BluetoothGattServer$2;

    invoke-direct {v3, p0}, Lcom/broadcom/bt/gatt/BluetoothGattServer$2;-><init>(Lcom/broadcom/bt/gatt/BluetoothGattServer;)V

    iput-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mConnection:Landroid/content/ServiceConnection;

    new-instance v3, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;

    invoke-direct {v3, p0}, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;-><init>(Lcom/broadcom/bt/gatt/BluetoothGattServer;)V

    iput-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mBluetoothGattServerCallback:Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    const-string v3, "BluetoothGattServer"

    const-string v4, "Constructor()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServices:Ljava/util/List;

    const-string v3, "bluetooth_manager"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v1

    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v1, v3}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/broadcom/bt/gatt/IBluetoothGatt;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mConnection:Landroid/content/ServiceConnection;

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "BluetoothGattServer"

    const-string v4, "Could not bind to Bluetooth Gatt Service"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :catch_0
    move-exception v2

    const-string v3, "BluetoothGattServer"

    const-string v4, "Unable to register BluetoothStateChangeCallback"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    const-string v3, "BluetoothGattServer"

    const-string v4, "Unable to get BluetoothManager interface."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$100(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/IBluetoothGatt;
    .locals 1
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    return-object v0
.end method

.method static synthetic access$102(Lcom/broadcom/bt/gatt/BluetoothGattServer;Lcom/broadcom/bt/gatt/IBluetoothGatt;)Lcom/broadcom/bt/gatt/IBluetoothGatt;
    .locals 0
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;
    .param p1    # Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    return-object p1
.end method

.method static synthetic access$200(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .locals 1
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/broadcom/bt/gatt/BluetoothGattServer;B)B
    .locals 0
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;
    .param p1    # B

    iput-byte p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    return p1
.end method

.method static synthetic access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    .locals 1
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0    # Lcom/broadcom/bt/gatt/BluetoothGattServer;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method


# virtual methods
.method public addService(Lcom/broadcom/bt/gatt/BluetoothGattService;)Z
    .locals 18
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattService;

    const-string v1, "BluetoothGattServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addService() - service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-byte v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServices:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    move-object/from16 v0, p0

    iget-byte v2, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual/range {p1 .. p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getType()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getInstanceId()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getHandles()I

    move-result v5

    new-instance v6, Landroid/os/ParcelUuid;

    invoke-virtual/range {p1 .. p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-interface/range {v1 .. v6}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->beginServiceDeclaration(BIIILandroid/os/ParcelUuid;)V

    invoke-virtual/range {p1 .. p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getIncludedServices()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    move-object/from16 v0, p0

    iget-byte v2, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual {v14}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getType()I

    move-result v3

    invoke-virtual {v14}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getInstanceId()I

    move-result v4

    new-instance v5, Landroid/os/ParcelUuid;

    invoke-virtual {v14}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->addIncludedService(BIILandroid/os/ParcelUuid;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v11

    const-string v1, "BluetoothGattServer"

    const-string v2, ""

    invoke-static {v1, v2, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    invoke-virtual {v7}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getKeySize()I

    move-result v1

    add-int/lit8 v1, v1, -0x7

    shl-int/lit8 v1, v1, 0xc

    invoke-virtual {v7}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getPermissions()I

    move-result v2

    add-int v16, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    move-object/from16 v0, p0

    iget-byte v2, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    new-instance v3, Landroid/os/ParcelUuid;

    invoke-virtual {v7}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {v7}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getProperties()I

    move-result v4

    move/from16 v0, v16

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->addCharacteristic(BLandroid/os/ParcelUuid;II)V

    invoke-virtual {v7}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getDescriptors()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    move-object/from16 v0, p0

    iget-byte v2, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    new-instance v3, Landroid/os/ParcelUuid;

    invoke-virtual {v9}, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {v9}, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->getPermissions()I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->addDescriptor(BLandroid/os/ParcelUuid;I)V

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    move-object/from16 v0, p0

    iget-byte v2, v0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-interface {v1, v2}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->endServiceDeclaration(B)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public cancelConnection(Landroid/bluetooth/BluetoothDevice;)V
    .locals 4
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const-string v1, "BluetoothGattServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelConnection() - device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v1, :cond_0

    iget-byte v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->serverDisconnect(BLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BluetoothGattServer"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public clearServices()V
    .locals 3

    const-string v1, "BluetoothGattServer"

    const-string v2, "clearServices()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v1, :cond_0

    iget-byte v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-interface {v1, v2}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->clearServices(B)V

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BluetoothGattServer"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method close()V
    .locals 6

    const/4 v5, 0x0

    const-string v3, "BluetoothGattServer"

    const-string v4, "close()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->unregisterApp()V

    iput-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const-string v3, "bluetooth_manager"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v1

    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v1, v3}, Landroid/bluetooth/IBluetoothManager;->unregisterStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mConnection:Landroid/content/ServiceConnection;

    monitor-enter v4

    :try_start_1
    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    :try_start_2
    iput-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v3, v5}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_1
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void

    :catch_0
    move-exception v2

    const-string v3, "BluetoothGattServer"

    const-string v4, "Unable to unregister BluetoothStateChangeCallback"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_4
    const-string v3, "BluetoothGattServer"

    const-string v5, ""

    invoke-static {v3, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;Z)Z
    .locals 7
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "BluetoothGattServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connect() - device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", auto: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v3, :cond_0

    iget-byte v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v3, :cond_1

    :cond_0
    move v1, v2

    :goto_0
    return v1

    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    if-eqz p2, :cond_2

    move v3, v2

    :goto_1
    invoke-interface {v4, v5, v6, v3}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->serverConnect(BLjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BluetoothGattServer"

    const-string v3, ""

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v2

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1
.end method

.method public getConnectedDevices()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const-string v6, "BluetoothGattServer"

    const-string v7, "getConnectedDevices"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    :try_start_0
    iget-object v6, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    invoke-interface {v6}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v6, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    const-string v6, "BluetoothGattServer"

    const-string v7, ""

    invoke-static {v6, v7, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .locals 7
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v4, 0x0

    const-string v5, "BluetoothGattServer"

    const-string v6, "getConnectionState()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    invoke-interface {v5}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x2

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v5, "BluetoothGattServer"

    const-string v6, ""

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .locals 1
    .param p1    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getService(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattService;
    .locals 3
    .param p1    # Ljava/util/UUID;

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/broadcom/bt/gatt/BluetoothGattService;

    invoke-virtual {v1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getService(Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;
    .locals 3
    .param p1    # Ljava/util/UUID;
    .param p2    # I
    .param p3    # I

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/broadcom/bt/gatt/BluetoothGattService;

    invoke-virtual {v1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getType()I

    move-result v2

    if-ne v2, p3, :cond_0

    invoke-virtual {v1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getInstanceId()I

    move-result v2

    if-ne v2, p2, :cond_0

    invoke-virtual {v1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getServices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/gatt/BluetoothGattService;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServices:Ljava/util/List;

    return-object v0
.end method

.method public notifyCharacteristicChanged(Landroid/bluetooth/BluetoothDevice;Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;Z)Z
    .locals 12
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .param p3    # Z

    const-string v0, "BluetoothGattServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyCharacteristicChanged() - device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v0, :cond_0

    iget-byte v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getService()Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v11

    if-nez v11, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getType()I

    move-result v3

    invoke-virtual {v11}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getInstanceId()I

    move-result v4

    new-instance v5, Landroid/os/ParcelUuid;

    invoke-virtual {v11}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {p2}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getInstanceId()I

    move-result v6

    new-instance v7, Landroid/os/ParcelUuid;

    invoke-virtual {p2}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {p2}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v9

    move v8, p3

    invoke-interface/range {v0 .. v9}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->sendNotification(BLjava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Z[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v10

    const-string v0, "BluetoothGattServer"

    const-string v1, ""

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerApp(Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;)Z
    .locals 6
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    const/4 v2, 0x0

    const-string v3, "BluetoothGattServer"

    const-string v4, "registerApp()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    const-string v3, "BluetoothGattServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "registerApp() - UUID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    new-instance v4, Landroid/os/ParcelUuid;

    invoke-direct {v4, v1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mBluetoothGattServerCallback:Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    invoke-interface {v3, v4, v5}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->registerServer(Landroid/os/ParcelUuid;Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "BluetoothGattServer"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removeService(Lcom/broadcom/bt/gatt/BluetoothGattService;)Z
    .locals 9
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattService;

    const/4 v2, 0x0

    const-string v3, "BluetoothGattServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removeService() - service: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v3, :cond_0

    iget-byte v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getInstanceId()I

    move-result v4

    invoke-virtual {p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getType()I

    move-result v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->getService(Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v4, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual {p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getType()I

    move-result v5

    invoke-virtual {p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getInstanceId()I

    move-result v6

    new-instance v7, Landroid/os/ParcelUuid;

    invoke-virtual {p1}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-interface {v3, v4, v5, v6, v7}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->removeService(BIILandroid/os/ParcelUuid;)V

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServices:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "BluetoothGattServer"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public sendResponse(Landroid/bluetooth/BluetoothDevice;II[B)Z
    .locals 8
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # I
    .param p4    # [B

    const/4 v7, 0x0

    const-string v0, "BluetoothGattServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendResponse() - device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v0, :cond_0

    iget-byte v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v0, :cond_1

    :cond_0
    move v0, v7

    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->sendResponse(BLjava/lang/String;II[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v0, "BluetoothGattServer"

    const-string v1, ""

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v7

    goto :goto_0
.end method

.method public startScan()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "BluetoothGattServer"

    const-string v4, "startScan()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v3, :cond_0

    iget-byte v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v3, :cond_1

    :cond_0
    move v1, v2

    :goto_0
    return v1

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v4, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->startScan(BZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BluetoothGattServer"

    const-string v3, ""

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v2

    goto :goto_0
.end method

.method public startScan([Ljava/util/UUID;)Z
    .locals 8
    .param p1    # [Ljava/util/UUID;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v5, "BluetoothGattServer"

    const-string v6, "startScan() - with UUIDs"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v5, :cond_0

    iget-byte v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v5, :cond_1

    :cond_0
    move v3, v4

    :goto_0
    return v3

    :cond_1
    :try_start_0
    array-length v5, p1

    new-array v2, v5, [Landroid/os/ParcelUuid;

    const/4 v1, 0x0

    :goto_1
    array-length v5, v2

    if-eq v1, v5, :cond_2

    new-instance v5, Landroid/os/ParcelUuid;

    aget-object v6, p1, v1

    invoke-direct {v5, v6}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    aput-object v5, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v6, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7, v2}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->startScanWithUuids(BZ[Landroid/os/ParcelUuid;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "BluetoothGattServer"

    const-string v5, ""

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v3, v4

    goto :goto_0
.end method

.method public stopScan()V
    .locals 4

    const-string v1, "BluetoothGattServer"

    const-string v2, "stopScan()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v1, :cond_0

    iget-byte v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->stopScan(BZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BluetoothGattServer"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unregisterApp()V
    .locals 4

    const-string v1, "BluetoothGattServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterApp() - mServerIf="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-byte v3, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    if-eqz v1, :cond_0

    iget-byte v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-byte v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B

    invoke-interface {v1, v2}, Lcom/broadcom/bt/gatt/IBluetoothGatt;->unregisterServer(B)V

    const/4 v1, 0x0

    iput-byte v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BluetoothGattServer"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
