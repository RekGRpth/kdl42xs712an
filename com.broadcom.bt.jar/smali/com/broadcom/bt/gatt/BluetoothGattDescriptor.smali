.class public Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;
.super Ljava/lang/Object;
.source "BluetoothGattDescriptor.java"


# static fields
.field public static final DISABLE_NOTIFICATION_VALUE:[B

.field public static final ENABLE_INDICATION_VALUE:[B

.field public static final ENABLE_NOTIFICATION_VALUE:[B


# instance fields
.field protected mCharacteristic:Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

.field protected mPermissions:I

.field protected mUuid:Ljava/util/UUID;

.field protected mValue:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x2

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->ENABLE_NOTIFICATION_VALUE:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->ENABLE_INDICATION_VALUE:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->DISABLE_NOTIFICATION_VALUE:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x2t
        0x0t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method constructor <init>(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;Ljava/util/UUID;I)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .param p2    # Ljava/util/UUID;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mCharacteristic:Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    iput-object p2, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mUuid:Ljava/util/UUID;

    iput p3, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mPermissions:I

    return-void
.end method


# virtual methods
.method public getCharacteristic()Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mCharacteristic:Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    return-object v0
.end method

.method public getPermissions()I
    .locals 1

    iget v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mPermissions:I

    return v0
.end method

.method public getUuid()Ljava/util/UUID;
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mUuid:Ljava/util/UUID;

    return-object v0
.end method

.method public getValue()[B
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mValue:[B

    return-object v0
.end method

.method public setValue([B)Z
    .locals 1
    .param p1    # [B

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->mValue:[B

    const/4 v0, 0x1

    return v0
.end method
