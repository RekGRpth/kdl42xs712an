.class public Lcom/broadcom/bt/gatt/MutableBluetoothGattService;
.super Lcom/broadcom/bt/gatt/BluetoothGattService;
.source "MutableBluetoothGattService.java"


# direct methods
.method public constructor <init>(Ljava/util/UUID;I)V
    .locals 0
    .param p1    # Ljava/util/UUID;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/gatt/BluetoothGattService;-><init>(Ljava/util/UUID;I)V

    return-void
.end method


# virtual methods
.method public addCharacteristic(Lcom/broadcom/bt/gatt/MutableBluetoothGattCharacteristic;)Z
    .locals 1
    .param p1    # Lcom/broadcom/bt/gatt/MutableBluetoothGattCharacteristic;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattService;->mCharacteristics:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, p0}, Lcom/broadcom/bt/gatt/MutableBluetoothGattCharacteristic;->setService(Lcom/broadcom/bt/gatt/BluetoothGattService;)V

    const/4 v0, 0x1

    return v0
.end method

.method public addService(Lcom/broadcom/bt/gatt/BluetoothGattService;)Z
    .locals 1
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattService;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattService;->mIncludedServices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setHandles(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattService;->mHandles:I

    return-void
.end method

.method public setInstanceId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattService;->mInstanceId:I

    return-void
.end method
