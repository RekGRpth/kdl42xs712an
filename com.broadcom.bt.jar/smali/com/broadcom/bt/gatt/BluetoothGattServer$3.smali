.class Lcom/broadcom/bt/gatt/BluetoothGattServer$3;
.super Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback$Stub;
.source "BluetoothGattServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/gatt/BluetoothGattServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/gatt/BluetoothGattServer;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    invoke-direct {p0}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCharacteristicReadRequest(Ljava/lang/String;IIZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # I
    .param p6    # I
    .param p7    # Landroid/os/ParcelUuid;
    .param p8    # I
    .param p9    # Landroid/os/ParcelUuid;

    invoke-virtual {p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual/range {p9 .. p9}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v0

    const-string v5, "BluetoothGattServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCharacteristicReadRequest() - service="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", characteristic="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v5}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    invoke-virtual {v5, v4, p6, p5}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->getService(Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v3, v0}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v5}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v5}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v5

    invoke-virtual {v5, v2, p2, p3, v1}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onCharacteristicReadRequest(Landroid/bluetooth/BluetoothDevice;IILcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V

    goto :goto_0
.end method

.method public onCharacteristicWriteRequest(Ljava/lang/String;IIIZZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;[B)V
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .param p8    # I
    .param p9    # Landroid/os/ParcelUuid;
    .param p10    # I
    .param p11    # Landroid/os/ParcelUuid;
    .param p12    # [B

    invoke-virtual/range {p9 .. p9}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v12

    invoke-virtual/range {p11 .. p11}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v10

    const-string v2, "BluetoothGattServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCharacteristicWriteRequest() - service="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", characteristic="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    move/from16 v0, p8

    move/from16 v1, p7

    invoke-virtual {v2, v12, v0, v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->getService(Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v11

    if-nez v11, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v11, v10}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v2

    move v4, p2

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p3

    move-object/from16 v9, p12

    invoke-virtual/range {v2 .. v9}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onCharacteristicWriteRequest(Landroid/bluetooth/BluetoothDevice;ILcom/broadcom/bt/gatt/BluetoothGattCharacteristic;ZZI[B)V

    goto :goto_0
.end method

.method public onDescriptorReadRequest(Ljava/lang/String;IIZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;)V
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # I
    .param p6    # I
    .param p7    # Landroid/os/ParcelUuid;
    .param p8    # I
    .param p9    # Landroid/os/ParcelUuid;
    .param p10    # Landroid/os/ParcelUuid;

    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual/range {p9 .. p9}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual/range {p10 .. p10}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    const-string v9, "BluetoothGattServer"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onCharacteristicReadRequest() - service="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", characteristic="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "descriptor="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v9, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v9}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    iget-object v9, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    move/from16 v0, p6

    move/from16 v1, p5

    invoke-virtual {v9, v8, v0, v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->getService(Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v7, v2}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v4}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v9, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v9}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v9}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v9

    invoke-virtual {v9, v6, p2, p3, v5}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onDescriptorReadRequest(Landroid/bluetooth/BluetoothDevice;IILcom/broadcom/bt/gatt/BluetoothGattDescriptor;)V

    goto :goto_0
.end method

.method public onDescriptorWriteRequest(Ljava/lang/String;IIIZZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;[B)V
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .param p8    # I
    .param p9    # Landroid/os/ParcelUuid;
    .param p10    # I
    .param p11    # Landroid/os/ParcelUuid;
    .param p12    # Landroid/os/ParcelUuid;
    .param p13    # [B

    invoke-virtual/range {p9 .. p9}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v14

    invoke-virtual/range {p11 .. p11}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-virtual/range {p12 .. p12}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v12

    const-string v2, "BluetoothGattServer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDescriptorWriteRequest() - service="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", characteristic="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "descriptor="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    move/from16 v0, p8

    move/from16 v1, p7

    invoke-virtual {v2, v14, v0, v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->getService(Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v13

    if-nez v13, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v13, v10}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v11

    if-eqz v11, :cond_0

    invoke-virtual {v11, v12}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v2

    move/from16 v4, p2

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p3

    move-object/from16 v9, p13

    invoke-virtual/range {v2 .. v9}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onDescriptorWriteRequest(Landroid/bluetooth/BluetoothDevice;ILcom/broadcom/bt/gatt/BluetoothGattDescriptor;ZZI[B)V

    goto :goto_0
.end method

.method public onExecuteWrite(Ljava/lang/String;IZ)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    const-string v1, "BluetoothGattServer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onExecuteWrite() - device="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", transId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "execWrite="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v1

    invoke-virtual {v1, v0, p2, p3}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onExecuteWrite(Landroid/bluetooth/BluetoothDevice;IZ)V

    goto :goto_0
.end method

.method public onScanResult(Ljava/lang/String;I[B)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # [B

    const-string v0, "BluetoothGattServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScanResult() - Device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " RSSI="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onScanResult(Landroid/bluetooth/BluetoothDevice;I[B)V

    :cond_0
    return-void
.end method

.method public onServerConnectionState(BBZLjava/lang/String;)V
    .locals 3
    .param p1    # B
    .param p2    # B
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    const-string v0, "BluetoothGattServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServerConnectionState() - status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " serverIf="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$600(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    if-eqz p3, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v2, p1, v0}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onConnectionStateChange(Landroid/bluetooth/BluetoothDevice;II)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onServerRegistered(BB)V
    .locals 3
    .param p1    # B
    .param p2    # B

    const-string v0, "BluetoothGattServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServerRegistered() - status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " serverIf="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # setter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mServerIf:B
    invoke-static {v0, p2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$402(Lcom/broadcom/bt/gatt/BluetoothGattServer;B)B

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onAppRegistered(I)V

    :cond_0
    return-void
.end method

.method public onServiceAdded(BIILandroid/os/ParcelUuid;)V
    .locals 5
    .param p1    # B
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/ParcelUuid;

    invoke-virtual {p4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v1

    const-string v2, "BluetoothGattServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onServiceAdded() - service="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "status="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    invoke-virtual {v2, v1, p3, p2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->getService(Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattServer$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGattServer;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGattServer;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
    invoke-static {v2}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->access$500(Lcom/broadcom/bt/gatt/BluetoothGattServer;)Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;->onServiceAdded(ILcom/broadcom/bt/gatt/BluetoothGattService;)V

    goto :goto_0
.end method
