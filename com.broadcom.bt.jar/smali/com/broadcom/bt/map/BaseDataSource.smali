.class public abstract Lcom/broadcom/bt/map/BaseDataSource;
.super Ljava/lang/Object;
.source "BaseDataSource.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/map/BaseDataSource$1;,
        Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;,
        Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;,
        Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;,
        Lcom/broadcom/bt/map/BaseDataSource$EventHandler;
    }
.end annotation


# static fields
.field public static final CHARSET_NATIVE:I = 0x0

.field public static final CHARSET_UNKNOWN:I = 0x2

.field public static final CHARSET_UTF_8:I = 0x1

.field static final DATE_TIME_FORMATTER:Ljava/text/SimpleDateFormat;

.field private static final DEFAULT_TMP_DIR:Ljava/io/File;

.field private static final DEFAULT_TMP_DIR_PATH:Ljava/lang/String; = "/data/data/com.android.bluetooth/files/map"

.field private static final EMPTY_FOLDER_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_MESSAGE_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAP_CONTENT_URI:Landroid/net/Uri;

.field public static final MSG_RECEPTION_STATUS_COMPLETE:B = 0x0t

.field public static final MSG_RECEPTION_STATUS_FRACTION:B = 0x1t

.field public static final MSG_RECEPTION_STATUS_NOTIFICATION:B = 0x2t

.field public static final MSG_TYPE_EMAIL:B = 0x1t

.field public static final MSG_TYPE_MMS:B = 0x8t

.field public static final MSG_TYPE_SMS_CDMA:B = 0x4t

.field public static final MSG_TYPE_SMS_GSM:B = 0x2t

.field private static final TAG:Ljava/lang/String; = "BtMap.BaseDataSource"

.field private static final V:Z = true


# instance fields
.field protected mCallback:Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;

.field protected mCleanupAfterClose:Z

.field protected mConnectedDevice:Landroid/bluetooth/BluetoothDevice;

.field protected mContentResolver:Landroid/content/ContentResolver;

.field protected mContext:Landroid/content/Context;

.field protected mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

.field protected mEventReceiver:Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;

.field private mHasClientConnected:Z

.field private mHasClientsRegistered:Z

.field protected mIsStarted:Z

.field protected mMapService:Lcom/broadcom/bt/map/BluetoothMap;

.field protected mPendingRequest:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "content://com.broadcom.bt.map"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/broadcom/bt/map/BaseDataSource;->MAP_CONTENT_URI:Landroid/net/Uri;

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/data/com.android.bluetooth/files/map"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/broadcom/bt/map/BaseDataSource;->DEFAULT_TMP_DIR:Ljava/io/File;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/broadcom/bt/map/BaseDataSource;->EMPTY_FOLDER_LIST:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/broadcom/bt/map/BaseDataSource;->EMPTY_MESSAGE_LIST:Ljava/util/ArrayList;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMddHHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/broadcom/bt/map/BaseDataSource;->DATE_TIME_FORMATTER:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/broadcom/bt/map/BaseDataSource;)V
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BaseDataSource;

    invoke-direct {p0}, Lcom/broadcom/bt/map/BaseDataSource;->onStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/broadcom/bt/map/BaseDataSource;Z)V
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BaseDataSource;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/broadcom/bt/map/BaseDataSource;->onStop(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/broadcom/bt/map/BaseDataSource;)V
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BaseDataSource;

    invoke-direct {p0}, Lcom/broadcom/bt/map/BaseDataSource;->stopped()V

    return-void
.end method

.method static synthetic access$302(Lcom/broadcom/bt/map/BaseDataSource;Z)Z
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BaseDataSource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mHasClientConnected:Z

    return p1
.end method

.method static synthetic access$402(Lcom/broadcom/bt/map/BaseDataSource;Z)Z
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BaseDataSource;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mHasClientsRegistered:Z

    return p1
.end method

.method private createBMessageFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getBMsgMaxByteLength(Landroid/bluetooth/BluetoothDevice;)I
    .locals 3
    .param p0    # Landroid/bluetooth/BluetoothDevice;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "BMW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "getBMsgMaxByteLength(): IOP Exception for BMW carkits"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x3a98

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private getMSETime()Ljava/lang/String;
    .locals 5

    const/16 v4, 0x8

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sget-object v2, Lcom/broadcom/bt/map/BaseDataSource;->DATE_TIME_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "T"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xe

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BtMap.BaseDataSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MSEDateTime :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method public static isRootFolder(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isValidFolderPath(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_0

    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    if-gtz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized onStart()V
    .locals 11

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mIsStarted:Z

    if-eqz v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStart():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". DataSource is already started!!!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "BtMap.BaseDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStart(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->preStart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_1

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceType()B

    move-result v3

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->supportsMessageFiltering()Z

    move-result v6

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->supportsMessageOffsetBrowsing()Z

    move-result v7

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getFolderMappings()[Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/broadcom/bt/map/BaseDataSource;->mCallback:Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;

    invoke-virtual/range {v0 .. v9}, Lcom/broadcom/bt/map/BluetoothMap;->registerDatasource(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v10

    :try_start_3
    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "Unable to register datasource with Map Profile Service"

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private onStop(Z)V
    .locals 4
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v1, :cond_0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "MapService not available"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p1}, Lcom/broadcom/bt/map/BluetoothMap;->unregisterDatasource(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "Unable to register datasource with Map Profile Service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private declared-synchronized stopped()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {v1}, Lcom/broadcom/bt/map/BluetoothMap;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "cleanup(): closing MAP proxy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public declared-synchronized cleanup()V
    .locals 4

    monitor-enter p0

    :try_start_0
    const-string v1, "BtMap.BaseDataSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cleanup():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventReceiver:Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventReceiver:Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventReceiver:Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    invoke-virtual {v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->finish()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "cleanup(): error unregistering receiver"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :catch_1
    move-exception v0

    :try_start_5
    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "cleanup(): error stopping handler thread"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method protected disconnect(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v1, :cond_0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "MapService not available"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/broadcom/bt/map/BluetoothMap;->disconnect(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "disconnect(): error while calling disconnect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected abstract getDatasourceDisplayName()Ljava/lang/String;
.end method

.method protected abstract getDatasourceId()Ljava/lang/String;
.end method

.method protected abstract getDatasourceType()B
.end method

.method protected declared-synchronized getEventHandlerThread()Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;-><init>(Lcom/broadcom/bt/map/BaseDataSource;)V

    iput-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    invoke-virtual {v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->start()V

    :goto_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract getFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/FolderListFilter;)V
.end method

.method protected abstract getFolderMappings()[Ljava/lang/String;
.end method

.method protected getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    return-object v0
.end method

.method protected abstract getMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V
.end method

.method protected abstract getMsgListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
.end method

.method protected abstract getMsgListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
.end method

.method protected abstract getProviderDisplayName()Ljava/lang/String;
.end method

.method protected abstract getProviderId()Ljava/lang/String;
.end method

.method protected hasConnectedClient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mHasClientConnected:Z

    return v0
.end method

.method public declared-synchronized init(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    monitor-enter p0

    :try_start_0
    const-string v0, "BtMap.BaseDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getEventHandlerThread()Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    new-instance v0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;-><init>(Lcom/broadcom/bt/map/BaseDataSource;Lcom/broadcom/bt/map/BaseDataSource$1;)V

    iput-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventReceiver:Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventReceiver:Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v2, v2, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    # invokes: Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->init(Landroid/content/Context;Landroid/os/Handler;)V
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->access$600(Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;Landroid/content/Context;Landroid/os/Handler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isNotificationEnabled()Z
    .locals 4

    iget-boolean v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mHasClientConnected:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mHasClientsRegistered:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "BtMap.BaseDataSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isNotificationEnabled(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDeliveryStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    if-eqz p4, :cond_1

    const/4 v5, 0x1

    :goto_1
    const/4 v7, 0x0

    move-object v3, p1

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/map/BluetoothMap;->sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v5, 0x3

    goto :goto_1
.end method

.method public notifyMessageDeleted(Ljava/lang/String;BLjava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    return-void
.end method

.method public notifyMessageMoved(Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x8

    move-object v3, p1

    move v4, p2

    move-object v6, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/map/BluetoothMap;->sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public notifyNewMessage(Ljava/lang/String;BLjava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v3, p1

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/map/BluetoothMap;->sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    if-eqz p4, :cond_1

    const/4 v5, 0x2

    :goto_1
    const/4 v7, 0x0

    move-object v3, p1

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/map/BluetoothMap;->sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v5, 0x4

    goto :goto_1
.end method

.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/bluetooth/BluetoothProfile;

    const-string v0, "BtMap.BaseDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceConnected():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Proxy object = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p2, Lcom/broadcom/bt/map/BluetoothMap;

    iput-object p2, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    new-instance v0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;-><init>(Lcom/broadcom/bt/map/BaseDataSource;)V

    iput-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mCallback:Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->onStart()V

    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1    # I

    const-string v0, "BtMap.BaseDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceDisconnected():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStartError()V
    .locals 2

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "onStartError()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStarted()V
    .locals 2

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "onStarted()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStopError()V
    .locals 2

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "onStopError()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStopped()V
    .locals 2

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "onStopped()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public openOutputStream(Ljava/lang/String;[Landroid/net/Uri;)Ljava/io/OutputStream;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # [Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v3, 0x0

    :try_start_0
    sget-object v5, Lcom/broadcom/bt/map/BaseDataSource;->MAP_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    const/4 v0, 0x0

    :try_start_1
    iget-object v5, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v5, "cwt"

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    if-eqz p2, :cond_0

    const/4 v4, 0x0

    aput-object v3, p2, v4

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v2

    const-string v5, "BtMap.BaseDataSource"

    const-string v6, "Unable to parse URI for writing BMessage"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v4

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v5, "BtMap.BaseDataSource"

    const-string v6, "Unable to open content stream"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v4

    goto :goto_0
.end method

.method public parseBMessage(Ljava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessage;
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "BtMap.BaseDataSource"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "parseBMessage contentUri: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_1

    const-string v7, "BtMap.BaseDataSource"

    const-string v8, "Content URI is null"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const-string v7, "file://"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "BtMap.BaseDataSource"

    const-string v8, "Parsing from file..."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Ljava/io/File;

    const-string v8, "file://"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/broadcom/bt/util/bmsg/BMessage;->parse(Ljava/io/File;)Lcom/broadcom/bt/util/bmsg/BMessage;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v7, "BtMap.BaseDataSource"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to parse for BMessage from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v7, Lcom/broadcom/bt/map/BaseDataSource;->MAP_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "BtMap.BaseDataSource"

    const-string v8, "Parsing from MAP Content Provider..."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x0

    :try_start_0
    const-string v7, "r"

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v1

    invoke-static {v1}, Lcom/broadcom/bt/util/bmsg/BMessage;->parse(I)Lcom/broadcom/bt/util/bmsg/BMessage;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_1
    if-eqz v3, :cond_3

    :try_start_1
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    move-object v2, v5

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v7, "BtMap.BaseDataSource"

    const-string v8, "Error parsing BMessage "

    invoke-static {v7, v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_4
    const-string v7, "BtMap.BaseDataSource"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to parse BMessage from unknown content url "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_1
    move-exception v7

    goto :goto_2
.end method

.method protected preStart()V
    .locals 2

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "preStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected abstract pushMessage(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;Ljava/lang/String;ZZI)V
.end method

.method public declared-synchronized setAutoCleanupOnClose(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mCleanupAfterClose:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setFolderListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v1, :cond_0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "MapService not available"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {v1, p1, p2, p4}, Lcom/broadcom/bt/map/BluetoothMap;->setFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "setFolderListingResult() error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    sget-object v2, Lcom/broadcom/bt/map/BaseDataSource;->EMPTY_FOLDER_LIST:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, p2, v2}, Lcom/broadcom/bt/map/BluetoothMap;->setFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setFolderListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    if-eqz p3, :cond_0

    array-length v3, p3

    if-nez v3, :cond_1

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, p1, p2, v3, v4}, Lcom/broadcom/bt/map/BaseDataSource;->setFolderListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    array-length v3, p3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    array-length v3, p3

    if-ge v2, v3, :cond_2

    new-instance v0, Lcom/broadcom/bt/map/FolderInfo;

    invoke-direct {v0}, Lcom/broadcom/bt/map/FolderInfo;-><init>()V

    aget-object v3, p3, v2

    iput-object v3, v0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    invoke-virtual {p0, p1, p2, v3, v1}, Lcom/broadcom/bt/map/BaseDataSource;->setFolderListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    return-void
.end method

.method public setGetMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLcom/broadcom/bt/util/bmsg/BMessage;)V
    .locals 5
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Lcom/broadcom/bt/util/bmsg/BMessage;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v1, :cond_0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "MapService not available"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-nez p3, :cond_1

    const-string v1, "BtMap.BaseDataSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get BMessage for messageId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {v1, p1, p2, v4}, Lcom/broadcom/bt/map/BluetoothMap;->returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p2, p4}, Lcom/broadcom/bt/map/BaseDataSource;->writeBMessage(Ljava/lang/String;Lcom/broadcom/bt/util/bmsg/BMessage;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p4}, Lcom/broadcom/bt/util/bmsg/BMessage;->finish()V

    const/4 p4, 0x0

    if-eqz v0, :cond_2

    const-string v1, "BtMap.BaseDataSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BMessage for messageId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " created in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lcom/broadcom/bt/map/BluetoothMap;->returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v1, "BtMap.BaseDataSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error create BMessage for messageId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {v1, p1, p2, v4}, Lcom/broadcom/bt/map/BluetoothMap;->returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract setMessageDeleted(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V
.end method

.method public setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 6
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/map/BluetoothMap;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    goto :goto_0
.end method

.method public setMessageListingCountResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZIZ)V
    .locals 7
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # I
    .param p5    # Z

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-direct {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getMSETime()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move v3, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/map/BluetoothMap;->setMessageListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "setMessageListingResult() error"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getMSETime()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/map/BluetoothMap;->setMessageListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setMessageListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V
    .locals 4
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v1, :cond_0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "MapService not available"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-direct {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getMSETime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, p2, p4, v2}, Lcom/broadcom/bt/map/BluetoothMap;->setMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BtMap.BaseDataSource"

    const-string v2, "setMessageListingResult() error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    sget-object v2, Lcom/broadcom/bt/map/BaseDataSource;->EMPTY_MESSAGE_LIST:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getMSETime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/broadcom/bt/map/BluetoothMap;->setMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected abstract setMessageRead(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V
.end method

.method public setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Lcom/broadcom/bt/util/bmsg/BMessage;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/broadcom/bt/util/bmsg/BMessage;->finish()V

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_1

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-eqz p4, :cond_2

    :goto_1
    invoke-virtual {v0, p1, p3, p5}, Lcom/broadcom/bt/map/BluetoothMap;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 p5, 0x0

    goto :goto_1
.end method

.method public declared-synchronized start()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->deferStart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop(Z)V
    .locals 3
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mIsStarted:Z

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". DataSource is already stopped!!!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "BtMap.BaseDataSource"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0, p1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->onStop(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract supportsMessageFiltering()Z
.end method

.method protected abstract supportsMessageOffsetBrowsing()Z
.end method

.method protected abstract updateInbox()V
.end method

.method public updateMessageId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    if-nez v0, :cond_0

    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "MapService not available"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getProviderId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/broadcom/bt/map/BluetoothMap;->updateMessageId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeBMessage(Ljava/lang/String;Lcom/broadcom/bt/util/bmsg/BMessage;)Ljava/io/File;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/broadcom/bt/util/bmsg/BMessage;

    const/4 v7, 0x0

    invoke-direct {p0, p1}, Lcom/broadcom/bt/map/BaseDataSource;->createBMessageFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    :try_start_0
    sget-object v8, Lcom/broadcom/bt/map/BaseDataSource;->MAP_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    iget-object v8, p0, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x0

    :try_start_1
    const-string v8, "cwt"

    invoke-virtual {v4, v6, v8}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/broadcom/bt/util/bmsg/BMessage;->write(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    :goto_0
    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_1
    if-nez v5, :cond_1

    :goto_2
    return-object v7

    :catch_0
    move-exception v0

    const-string v8, "BtMap.BaseDataSource"

    const-string v9, "Unable to parse URI for writing BMessage"

    invoke-static {v8, v9, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v8, "BtMap.BaseDataSource"

    const-string v9, "Error creating BMessage "

    invoke-static {v8, v9, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    new-instance v7, Ljava/io/File;

    sget-object v8, Lcom/broadcom/bt/map/BaseDataSource;->DEFAULT_TMP_DIR:Ljava/io/File;

    invoke-direct {v7, v8, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_2

    :catch_2
    move-exception v8

    goto :goto_1
.end method
