.class Lcom/broadcom/bt/map/BluetoothMap$1;
.super Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;
.source "BluetoothMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/map/BluetoothMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/map/BluetoothMap;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/map/BluetoothMap;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-direct {p0}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onBluetoothStateChange(Z)V
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x1

    if-eqz p1, :cond_2

    const-string v1, "BtMap.BluetoothMap"

    const-string v2, "onBluetoothStateChange(on)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$100(Lcom/broadcom/bt/map/BluetoothMap;)I

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "BtMap.BluetoothMap"

    const-string v3, "onBluetoothStateChange(on): bind state is not  BIND_STATE_UNBOUND..Skipping bind..."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit v2

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x1

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$102(Lcom/broadcom/bt/map/BluetoothMap;I)I

    const-string v1, "BtMap.BluetoothMap"

    const-string v3, "onBluetoothStateChange(on): binding to MapService..."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "BtMap.BluetoothMap"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mConnection="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v4}, Lcom/broadcom/bt/map/BluetoothMap;->access$500(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/ServiceConnection;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$600(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/Context;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/broadcom/bt/map/IBluetoothMap;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v4}, Lcom/broadcom/bt/map/BluetoothMap;->access$500(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/ServiceConnection;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x0

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$102(Lcom/broadcom/bt/map/BluetoothMap;I)I

    const-string v1, "BtMap.BluetoothMap"

    const-string v3, "Could not bind to Bluetooth Map Service"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "BtMap.BluetoothMap"

    const-string v3, "Error binding to connection"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v1, "BtMap.BluetoothMap"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mConnection="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v4}, Lcom/broadcom/bt/map/BluetoothMap;->access$500(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/ServiceConnection;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "BtMap.BluetoothMap"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mContext="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/broadcom/bt/map/BluetoothMap;->access$600(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_2
    const-string v1, "BtMap.BluetoothMap"

    const-string v2, "onBluetoothStateChange(off)..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # invokes: Lcom/broadcom/bt/map/BluetoothMap;->debugPrintStackTrace()V
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$700(Lcom/broadcom/bt/map/BluetoothMap;)V

    iget-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    monitor-enter v2

    :try_start_3
    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$100(Lcom/broadcom/bt/map/BluetoothMap;)I

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "BtMap.BluetoothMap"

    const-string v3, "onBluetoothStateChange(off): state = BIND_STATE_UNBOUND.. Skipping unbind.."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    monitor-exit v2

    goto/16 :goto_1

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    :cond_3
    :try_start_5
    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$100(Lcom/broadcom/bt/map/BluetoothMap;)I

    move-result v1

    if-ne v1, v3, :cond_4

    const-string v1, "BtMap.BluetoothMap"

    const-string v3, "onBluetoothStateChange(off): state = BIND_STATE_BINDING.. Setting mPendingClose.."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x1

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mPendingClose:Z
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$202(Lcom/broadcom/bt/map/BluetoothMap;Z)Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    :cond_4
    :try_start_7
    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x0

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$102(Lcom/broadcom/bt/map/BluetoothMap;I)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$600(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$500(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/ServiceConnection;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x0

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$002(Lcom/broadcom/bt/map/BluetoothMap;Lcom/broadcom/bt/map/IBluetoothMap;)Lcom/broadcom/bt/map/IBluetoothMap;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :goto_2
    :try_start_8
    monitor-exit v2

    goto/16 :goto_1

    :catch_1
    move-exception v0

    const-string v1, "BtMap.BluetoothMap"

    const-string v3, "Error unbinding from connection"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$1;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x2

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$102(Lcom/broadcom/bt/map/BluetoothMap;I)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2
.end method
