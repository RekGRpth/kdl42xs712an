.class public interface abstract Lcom/broadcom/bt/map/IBluetoothMapCallback;
.super Ljava/lang/Object;
.source "IBluetoothMapCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/map/IBluetoothMapCallback$Stub;
    }
.end annotation


# virtual methods
.method public abstract onDatasourceRegistrationChanged()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onDatasourceStateChanged()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
