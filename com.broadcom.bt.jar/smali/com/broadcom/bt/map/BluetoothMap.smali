.class public Lcom/broadcom/bt/map/BluetoothMap;
.super Ljava/lang/Object;
.source "BluetoothMap.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;
    }
.end annotation


# static fields
.field public static final ACTION_DISCONNECT:Ljava/lang/String; = ""

.field public static final ACTION_DS_DISCOVER:Ljava/lang/String; = "com.broadcom.bt.service.map.DS_DISCOVER"

.field public static final ACTION_PREFIX:Ljava/lang/String; = "com.broadcom.bt.service.map."

.field public static final ACTION_PREFIX_LENGTH:I

.field public static final ACTION_START:Ljava/lang/String; = "broadcom.bluetooth.map.START"

.field public static final ACTION_STOP:Ljava/lang/String; = "broadcom.bluetooth.map.STOP"

.field private static final BIND_STATE_BINDING:I = 0x1

.field private static final BIND_STATE_BOUND:I = 0x2

.field private static final BIND_STATE_UNBINDING:I = 0x3

.field private static final BIND_STATE_UNBOUND:I = 0x0

.field static final DBG:Z = true

.field public static final EXTRA_BDA:Ljava/lang/String; = "BDA"

.field public static final EXTRA_BD_ADDR:Ljava/lang/String; = ""

.field public static final MAP_LENGTH_ADJUSTER:I = 0x16

.field public static final MSG_STATUS_TYPE_DELETED:B = 0x1t

.field public static final MSG_STATUS_TYPE_READ:B = 0x0t

.field public static final NOTIFICATION_TYPE_DELIVERY_FAILURE:B = 0x3t

.field public static final NOTIFICATION_TYPE_DELIVERY_SUCCESS:B = 0x1t

.field public static final NOTIFICATION_TYPE_MESSAGE_DELETED:B = 0x7t

.field public static final NOTIFICATION_TYPE_MESSAGE_SHIFT:B = 0x8t

.field public static final NOTIFICATION_TYPE_NEW_MESSAGE:B = 0x0t

.field public static final NOTIFICATION_TYPE_SENDING_FAILURE:B = 0x4t

.field public static final NOTIFICATION_TYPE_SENDING_SUCCESS:B = 0x2t

.field public static final PROVIDER_PERMISSION:Ljava/lang/String; = "android.Manifest.permission.BLUETOOTH"

.field private static final SERVICE_ID:I = 0x3e8

.field public static final STATE_MSE_STARTED:B = 0x2t

.field public static final STATE_MSE_STOPPED:B = 0x1t

.field public static final STATUS_MSE_CANNOT_START:I = 0x1

.field public static final STATUS_MSE_RETURN_CODE_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BtMap.BluetoothMap"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBindingState:I

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mIsClosed:Z

.field private mManagerService:Landroid/bluetooth/IBluetoothManager;

.field private mPendingClose:Z

.field private mService:Lcom/broadcom/bt/map/IBluetoothMap;

.field private mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "com.broadcom.bt.service.map."

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/broadcom/bt/map/BluetoothMap;->ACTION_PREFIX_LENGTH:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;-><init>(Lcom/broadcom/bt/map/BluetoothMap;Lcom/broadcom/bt/map/BluetoothMap$1;)V

    iput-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;

    new-instance v2, Lcom/broadcom/bt/map/BluetoothMap$1;

    invoke-direct {v2, p0}, Lcom/broadcom/bt/map/BluetoothMap$1;-><init>(Lcom/broadcom/bt/map/BluetoothMap;)V

    iput-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    iput-object p1, p0, Lcom/broadcom/bt/map/BluetoothMap;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    const-string v2, "bluetooth_manager"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v2

    iput-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    iget-object v3, p0, Lcom/broadcom/bt/map/BluetoothMap;->mStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/broadcom/bt/map/BluetoothMap;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "BluetoothMap(): Bluetooth enabled. Binding to MapService..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    if-eqz v2, :cond_2

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "BluetoothMap(): Binding state not unbound...Skipping binding..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "Unable to register BluetoothStateChangeCallback"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v2, Landroid/os/RemoteException;

    const-string v3, "Bluetooth is not available"

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "Bluetooth is not available"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const/4 v2, 0x1

    :try_start_2
    iput v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/broadcom/bt/map/IBluetoothMap;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "Could not bind to Bluetooth Map Service"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    iput v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_3
    :try_start_3
    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "BluetoothMap(): Bluetooth not enabled. Skipping MapService binding..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/broadcom/bt/map/BluetoothMap;Lcom/broadcom/bt/map/IBluetoothMap;)Lcom/broadcom/bt/map/IBluetoothMap;
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;
    .param p1    # Lcom/broadcom/bt/map/IBluetoothMap;

    iput-object p1, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    return-object p1
.end method

.method static synthetic access$100(Lcom/broadcom/bt/map/BluetoothMap;)I
    .locals 1
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;

    iget v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    return v0
.end method

.method static synthetic access$102(Lcom/broadcom/bt/map/BluetoothMap;I)I
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;
    .param p1    # I

    iput p1, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    return p1
.end method

.method static synthetic access$200(Lcom/broadcom/bt/map/BluetoothMap;)Z
    .locals 1
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;

    iget-boolean v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mPendingClose:Z

    return v0
.end method

.method static synthetic access$202(Lcom/broadcom/bt/map/BluetoothMap;Z)Z
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/broadcom/bt/map/BluetoothMap;->mPendingClose:Z

    return p1
.end method

.method static synthetic access$300(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .locals 1
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$600(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/broadcom/bt/map/BluetoothMap;)V
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BluetoothMap;

    invoke-direct {p0}, Lcom/broadcom/bt/map/BluetoothMap;->debugPrintStackTrace()V

    return-void
.end method

.method private debugPrintStackTrace()V
    .locals 4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "Called from.."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    :goto_0
    const/4 v2, 0x5

    if-ge v0, v2, :cond_0

    array-length v2, v1

    if-ge v0, v2, :cond_0

    const-string v2, "BtMap.BluetoothMap"

    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/broadcom/bt/map/BluetoothMap;->getDebugStackTrace(Ljava/lang/StackTraceElement;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static getDebugStackTrace(Ljava/lang/StackTraceElement;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/StackTraceElement;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(): ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)Lcom/broadcom/bt/map/BluetoothMap;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const-string v6, "BtMap.BluetoothMap"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getProxy() ctx = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez p0, :cond_0

    const-string v5, "null"

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "l ="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez p1, :cond_1

    const-string v5, "null"

    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    if-eqz v3, :cond_2

    const-string v5, "BtMap.BluetoothMap"

    const-string v6, "Called from.."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    :goto_2
    const/4 v5, 0x5

    if-ge v0, v5, :cond_2

    array-length v5, v3

    if-ge v0, v5, :cond_2

    const-string v5, "BtMap.BluetoothMap"

    aget-object v6, v3, v0

    invoke-static {v6}, Lcom/broadcom/bt/map/BluetoothMap;->getDebugStackTrace(Ljava/lang/StackTraceElement;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move-object v5, p0

    goto :goto_0

    :cond_1
    move-object v5, p1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lcom/broadcom/bt/map/BluetoothMap;

    invoke-direct {v2, p0, p1}, Lcom/broadcom/bt/map/BluetoothMap;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    :goto_3
    return-object v2

    :catch_0
    move-exception v4

    const-string v5, "BtMap.BluetoothMap"

    const-string v6, "Unable to get MAP Proxy"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v2, 0x0

    goto :goto_3
.end method

.method private isEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 5

    const/4 v4, 0x1

    monitor-enter p0

    :try_start_0
    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "close()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/broadcom/bt/map/BluetoothMap;->debugPrintStackTrace()V

    iget-boolean v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mIsClosed:Z

    if-eqz v2, :cond_0

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "BluetoothMap proxy is already closed"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    if-nez v2, :cond_1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "close(): service not bound...Skipping unbind..."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    iget-object v3, p0, Lcom/broadcom/bt/map/BluetoothMap;->mStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothManager;->unregisterStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mIsClosed:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    :try_start_4
    iget v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    if-ne v2, v4, :cond_2

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "close(): service binding in progress. Setting pending close flag.."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :cond_2
    const/4 v2, 0x3

    :try_start_5
    iput v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    iget-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mConnection:Landroid/content/ServiceConnection;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_3
    const/4 v2, 0x0

    :try_start_6
    iput v2, p0, Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "Error unbinding service"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "Unable to register BluetoothStateChangeCallback"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method public disconnect(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method protected finalize()V
    .locals 0

    invoke-virtual {p0}, Lcom/broadcom/bt/map/BluetoothMap;->close()V

    return-void
.end method

.method public getConnectedDevices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v0, 0x0

    return v0
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .locals 1
    .param p1    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public registerDatasource(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # B
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .param p7    # Z
    .param p8    # [Ljava/lang/String;
    .param p9    # Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    :try_start_0
    invoke-interface/range {v0 .. v9}, Lcom/broadcom/bt/map/IBluetoothMap;->registerDatasource(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v10

    const-string v1, "BtMap.BluetoothMap"

    const-string v2, "registerDatasource() error"

    invoke-static {v1, v2, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lcom/broadcom/bt/map/IBluetoothMap;->returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "returnMessage() error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # B
    .param p5    # B
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    :try_start_0
    invoke-interface/range {v0 .. v7}, Lcom/broadcom/bt/map/IBluetoothMap;->sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v8

    const-string v1, "BtMap.BluetoothMap"

    const-string v2, "sendNotification() error"

    invoke-static {v1, v2, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lcom/broadcom/bt/map/IBluetoothMap;->setFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "setFolderListing() error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 7
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    :try_start_0
    invoke-interface/range {v0 .. v5}, Lcom/broadcom/bt/map/IBluetoothMap;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v6

    const-string v1, "BtMap.BluetoothMap"

    const-string v2, "setPushMessageResult() error"

    invoke-static {v1, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/broadcom/bt/map/IBluetoothMap;->setMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "setMessageListing() error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setMessageListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 7
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    :try_start_0
    invoke-interface/range {v0 .. v5}, Lcom/broadcom/bt/map/IBluetoothMap;->setMessageListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v6

    const-string v1, "BtMap.BluetoothMap"

    const-string v2, "setMessageListing() error"

    invoke-static {v1, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lcom/broadcom/bt/map/IBluetoothMap;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "setPushMessageResult() error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unregisterDatasource(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lcom/broadcom/bt/map/IBluetoothMap;->unregisterDatasource(Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "registerDatasource() error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public updateMessageId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/broadcom/bt/map/IBluetoothMap;->updateMessageId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "BtMap.BluetoothMap"

    const-string v3, "updateMessageHandle() error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
