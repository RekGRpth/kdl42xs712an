.class public Lcom/broadcom/bt/map/BaseDataSource$EventHandler;
.super Landroid/os/Handler;
.source "BaseDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/map/BaseDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "EventHandler"
.end annotation


# static fields
.field private static final MSG_DEFER_START:I = 0x3e8

.field private static final MSG_DEFER_STOP:I = 0x3e9

.field private static final MSG_GET_FOLDER_LISTING:I = 0x12c

.field private static final MSG_GET_MSG:I = 0x12e

.field private static final MSG_GET_MSG_LISTING:I = 0x12d

.field private static final MSG_PUSH_MSG:I = 0xc8

.field private static final MSG_SET_MSG_STATUS:I = 0x130

.field private static final MSG_START:I = 0x1

.field private static final MSG_START_COMPLETED:I = 0x64

.field private static final MSG_STOP:I = 0x2

.field private static final MSG_STOP_COMPLETED:I = 0x66

.field private static final MSG_UPDATE_INBOX:I = 0x12f

.field private static final PENDING_REQUEST_TIMEOUT:I = 0x7d0


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/map/BaseDataSource;


# direct methods
.method protected constructor <init>(Lcom/broadcom/bt/map/BaseDataSource;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method declared-synchronized cancelDeferredStart()V
    .locals 1

    monitor-enter p0

    const/16 v0, 0x3e8

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized cancelDeferredStop()V
    .locals 1

    monitor-enter p0

    const/16 v0, 0x3e9

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method cancelPendingRequestTimeout()V
    .locals 1

    const/16 v0, 0x7d0

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->removeMessages(I)V

    return-void
.end method

.method declared-synchronized deferStart()V
    .locals 3

    monitor-enter p0

    const/16 v0, 0x3e8

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->hasMessages(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "Pending start already requested..Ignoring deferStart request..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized deferStop()V
    .locals 3

    monitor-enter p0

    const/16 v0, 0x3e9

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->hasMessages(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "BtMap.BaseDataSource"

    const-string v1, "Pending start already requested..Ignoring deferStart request..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;

    const/16 v2, 0x12c

    invoke-virtual {p0, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "p"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method getMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # B

    const/16 v2, 0x12e

    invoke-virtual {p0, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "p"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "v"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "m"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "a"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "c"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method getMsgListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;JZ)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/broadcom/bt/map/MessageListFilter;
    .param p4    # J
    .param p6    # Z

    const/16 v2, 0x12d

    invoke-virtual {p0, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "p"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "c"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "mp"

    invoke-virtual {v0, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iput-object p3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 26
    .param p1    # Landroid/os/Message;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "PENDING_REQUEST_TIMEOUT occurred!"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    monitor-enter v4

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v5, 0x0

    iput-object v5, v2, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :sswitch_1
    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_DEFER_START: getting proxy..."

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    monitor-enter v4

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const-string v5, "broadcom.bluetooth.map.START"

    iput-object v5, v2, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->startPendingRequestTimeout()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v5, v5, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-static {v5, v7}, Lcom/broadcom/bt/map/BluetoothMap;->getProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)Lcom/broadcom/bt/map/BluetoothMap;

    move-result-object v5

    iput-object v5, v2, Lcom/broadcom/bt/map/BaseDataSource;->mMapService:Lcom/broadcom/bt/map/BluetoothMap;

    monitor-exit v4

    goto :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    # invokes: Lcom/broadcom/bt/map/BaseDataSource;->onStart()V
    invoke-static {v2}, Lcom/broadcom/bt/map/BaseDataSource;->access$000(Lcom/broadcom/bt/map/BaseDataSource;)V

    goto :goto_0

    :sswitch_3
    :try_start_2
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v2}, Lcom/broadcom/bt/map/BaseDataSource;->onStartError()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    monitor-enter v4

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v5, 0x0

    iput-object v5, v2, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->cancelPendingRequestTimeout()V

    monitor-exit v4

    goto :goto_0

    :catchall_2
    move-exception v2

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v2

    :cond_1
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v2}, Lcom/broadcom/bt/map/BaseDataSource;->onStarted()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_START: error calling data source onStart()"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_2

    const/4 v2, 0x1

    :goto_2
    # invokes: Lcom/broadcom/bt/map/BaseDataSource;->onStop(Z)V
    invoke-static {v4, v2}, Lcom/broadcom/bt/map/BaseDataSource;->access$100(Lcom/broadcom/bt/map/BaseDataSource;Z)V

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :sswitch_5
    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_STOP_COMPLETED"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v2}, Lcom/broadcom/bt/map/BaseDataSource;->onStopError()V

    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    monitor-enter v4

    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v5, 0x0

    iput-object v5, v2, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->cancelPendingRequestTimeout()V

    monitor-exit v4

    goto/16 :goto_0

    :catchall_3
    move-exception v2

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v2

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v4, 0x0

    iput-boolean v4, v2, Lcom/broadcom/bt/map/BaseDataSource;->mIsStarted:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v2}, Lcom/broadcom/bt/map/BaseDataSource;->onStopped()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    # invokes: Lcom/broadcom/bt/map/BaseDataSource;->stopped()V
    invoke-static {v2}, Lcom/broadcom/bt/map/BaseDataSource;->access$200(Lcom/broadcom/bt/map/BaseDataSource;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-boolean v2, v2, Lcom/broadcom/bt/map/BaseDataSource;->mCleanupAfterClose:Z

    if-eqz v2, :cond_3

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_STOP_COMPLETED: mCleanupAfterClose = true...Auto-cleanup started..."

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v2}, Lcom/broadcom/bt/map/BaseDataSource;->cleanup()V

    goto :goto_3

    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v15

    const-string v2, "r"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/broadcom/bt/map/RequestId;

    const-string v2, "p"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0, v4}, Lcom/broadcom/bt/map/BaseDataSource;->getFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/FolderListFilter;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_GET_FOLDER_LISTING: error"

    move-object/from16 v0, v25

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/broadcom/bt/map/BaseDataSource;->setFolderListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v15

    const-string v2, "r"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/broadcom/bt/map/RequestId;

    const-string v2, "p"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string v2, "c"

    const/4 v4, 0x1

    invoke-virtual {v15, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lcom/broadcom/bt/map/MessageListFilter;

    if-nez v17, :cond_5

    new-instance v17, Lcom/broadcom/bt/map/MessageListFilter;

    invoke-direct/range {v17 .. v17}, Lcom/broadcom/bt/map/MessageListFilter;-><init>()V

    :cond_5
    const-string v2, "mp"

    const-wide/16 v4, 0x0

    invoke-virtual {v15, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v23

    if-eqz v20, :cond_6

    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    new-instance v4, Lcom/broadcom/bt/map/MessageParameterFilter;

    move-wide/from16 v0, v23

    invoke-direct {v4, v0, v1}, Lcom/broadcom/bt/map/MessageParameterFilter;-><init>(J)V

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/broadcom/bt/map/BaseDataSource;->getMsgListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_GET_MSG_LISTING: error"

    move-object/from16 v0, v25

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/broadcom/bt/map/BaseDataSource;->setMessageListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    goto/16 :goto_0

    :cond_6
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    new-instance v4, Lcom/broadcom/bt/map/MessageParameterFilter;

    move-wide/from16 v0, v23

    invoke-direct {v4, v0, v1}, Lcom/broadcom/bt/map/MessageParameterFilter;-><init>(J)V

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/broadcom/bt/map/BaseDataSource;->getMsgListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v15

    const-string v2, "r"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/broadcom/bt/map/RequestId;

    const-string v2, "m"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const-string v4, "p"

    invoke-virtual {v15, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "v"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "a"

    const/4 v8, 0x0

    invoke-virtual {v15, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    const-string v8, "c"

    const/4 v10, 0x0

    invoke-virtual {v15, v8, v10}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Byte;->byteValue()B

    move-result v8

    invoke-virtual/range {v2 .. v8}, Lcom/broadcom/bt/map/BaseDataSource;->getMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_GET_MSG: error"

    move-object/from16 v0, v25

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v6, v4, v5}, Lcom/broadcom/bt/map/BaseDataSource;->setGetMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLcom/broadcom/bt/util/bmsg/BMessage;)V

    goto/16 :goto_0

    :sswitch_9
    const/16 v21, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v15

    const-string v2, "r"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/broadcom/bt/map/RequestId;

    const-string v2, "u"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v2, "f"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const/4 v9, 0x0

    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/broadcom/bt/map/BaseDataSource;->parseBMessage(Ljava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessage;
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_4

    move-result-object v9

    :goto_4
    if-nez v9, :cond_7

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_PUSH_MSG: unable to parse for BMessage Object"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v21, 0x1

    :goto_5
    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const/4 v11, 0x0

    const-string v12, ""

    move-object v8, v3

    move-object/from16 v10, v19

    invoke-virtual/range {v7 .. v12}, Lcom/broadcom/bt/map/BaseDataSource;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;ZLjava/lang/String;)V

    goto/16 :goto_0

    :catch_4
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "Error while parsing for BMessage"

    move-object/from16 v0, v25

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :cond_7
    :try_start_b
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const-string v2, "r"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/broadcom/bt/map/RequestId;

    const-string v2, "f"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v2, "v"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v2, "t"

    const/4 v4, 0x0

    invoke-virtual {v15, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    const-string v2, "ir"

    const/4 v4, 0x0

    invoke-virtual {v15, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    const-string v2, "c"

    const/4 v4, 0x0

    invoke-virtual {v15, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v14

    invoke-virtual/range {v7 .. v14}, Lcom/broadcom/bt/map/BaseDataSource;->pushMessage(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;Ljava/lang/String;ZZI)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_5

    goto :goto_5

    :catch_5
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_PUSH_MSG: error calling pushMessage()"

    move-object/from16 v0, v25

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v21, 0x1

    goto :goto_5

    :sswitch_a
    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v2}, Lcom/broadcom/bt/map/BaseDataSource;->updateInbox()V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_UPDATE_INBOX: error calling updateInbox()"

    move-object/from16 v0, v25

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :sswitch_b
    :try_start_d
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v15

    const-string v2, "t"

    const/4 v4, -0x1

    invoke-virtual {v15, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v22

    const/4 v2, 0x1

    move/from16 v0, v22

    if-ne v0, v2, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const-string v2, "r"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/broadcom/bt/map/RequestId;

    const-string v5, "m"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "i"

    const/4 v8, 0x0

    invoke-virtual {v15, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    invoke-virtual {v4, v2, v5, v7}, Lcom/broadcom/bt/map/BaseDataSource;->setMessageDeleted(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_7

    goto/16 :goto_0

    :catch_7
    move-exception v25

    const-string v2, "BtMap.BaseDataSource"

    const-string v4, "MSG_SET_MSG_STATUS: error calling setMessageStatus()"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    if-nez v22, :cond_9

    :try_start_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const-string v2, "r"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/broadcom/bt/map/RequestId;

    const-string v5, "m"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "i"

    const/4 v8, 0x0

    invoke-virtual {v15, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    invoke-virtual {v4, v2, v5, v7}, Lcom/broadcom/bt/map/BaseDataSource;->setMessageRead(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_9
    const-string v2, "BtMap.BaseDataSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MSG_SET_MSG_STATUS: invalid message status type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_7

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_4
        0x64 -> :sswitch_3
        0x66 -> :sswitch_5
        0xc8 -> :sswitch_9
        0x12c -> :sswitch_6
        0x12d -> :sswitch_7
        0x12e -> :sswitch_8
        0x12f -> :sswitch_a
        0x130 -> :sswitch_b
        0x3e8 -> :sswitch_1
        0x3e9 -> :sswitch_5
        0x7d0 -> :sswitch_0
    .end sparse-switch
.end method

.method onStart()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method onStartCompleted(Z)V
    .locals 2
    .param p1    # Z

    const/16 v1, 0x64

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onStop(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method onStopCompleted(Z)V
    .locals 2
    .param p1    # Z

    const/16 v1, 0x66

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method pushMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Z
    .param p7    # I

    const/16 v2, 0xc8

    invoke-virtual {p0, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "u"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "f"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "v"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "t"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "ir"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "c"

    invoke-virtual {v0, v2, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public setClientConnectionStateChanged(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method setMessageStatus(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;IZ)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z

    const/16 v2, 0x130

    invoke-virtual {p0, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "m"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "t"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "i"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method startPendingRequestTimeout()V
    .locals 3

    const/16 v0, 0x7d0

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0xfa0

    invoke-virtual {p0, v0, v1, v2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method updateInbox()V
    .locals 2

    const/16 v1, 0x12f

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
