.class public Lcom/broadcom/bt/map/BMessageUtil;
.super Ljava/lang/Object;
.source "BMessageUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BtMap.BMessageUtil"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findMessageBody(Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;)Lcom/broadcom/bt/util/bmsg/BMessageBody;
    .locals 5
    .param p0    # Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    const/4 v1, 0x1

    :goto_0
    if-eqz p0, :cond_1

    const-string v2, "BtMap.BMessageUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Finding message body in envelope level #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;->getBody()Lcom/broadcom/bt/util/bmsg/BMessageBody;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "BtMap.BMessageUtil"

    const-string v3, "findMessageBody(): Found body!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;->getChildEnvelope()Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    move-result-object p0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static findRecipientProperty(Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;B)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;
    .locals 6
    .param p0    # Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    .param p1    # B

    const/4 v2, 0x1

    :goto_0
    if-eqz p0, :cond_1

    const-string v3, "BtMap.BMessageUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Finding recipient in envelope level #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;->getRecipient()Lcom/broadcom/bt/util/bmsg/BMessageVCard;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->getProperty(B)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "BtMap.BMessageUtil"

    const-string v4, "findRecipientProperty(): Found property!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;->getChildEnvelope()Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    move-result-object p0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static setBMessageHeaderInfo(Lcom/broadcom/bt/util/bmsg/BMessage;BLjava/lang/String;Lcom/broadcom/bt/map/MessageInfo;)V
    .locals 7
    .param p0    # Lcom/broadcom/bt/util/bmsg/BMessage;
    .param p1    # B
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/broadcom/bt/map/MessageInfo;

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_1

    if-eq p1, v5, :cond_1

    :cond_0
    const-string v3, "BtMap.BMessageUtil"

    const-string v4, "Unable to set BMessage Header Info"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    iget-boolean v3, p3, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    invoke-virtual {p0, v3}, Lcom/broadcom/bt/util/bmsg/BMessage;->setReadStatus(Z)V

    invoke-virtual {p0, p2}, Lcom/broadcom/bt/util/bmsg/BMessage;->setFolder(Ljava/lang/String;)V

    invoke-static {p0, p3}, Lcom/broadcom/bt/map/BMessageUtil;->setBMessageType(Lcom/broadcom/bt/util/bmsg/BMessage;Lcom/broadcom/bt/map/MessageInfo;)V

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessage;->addOriginator()Lcom/broadcom/bt/util/bmsg/BMessageVCard;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->setVersion(B)V

    iget-object v3, p3, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    invoke-virtual {v1, v5, v3, v4}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->addProperty(BLjava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    iget-object v3, p3, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    invoke-virtual {v1, v6, v3, v4}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->addProperty(BLjava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessage;->addEnvelope()Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;->addRecipient()Lcom/broadcom/bt/util/bmsg/BMessageVCard;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->setVersion(B)V

    iget-object v3, p3, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    invoke-virtual {v2, v5, v3, v4}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->addProperty(BLjava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    iget-object v3, p3, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    invoke-virtual {v2, v6, v3, v4}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->addProperty(BLjava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    goto :goto_0
.end method

.method public static setBMessageType(Lcom/broadcom/bt/util/bmsg/BMessage;Lcom/broadcom/bt/map/MessageInfo;)V
    .locals 5
    .param p0    # Lcom/broadcom/bt/util/bmsg/BMessage;
    .param p1    # Lcom/broadcom/bt/map/MessageInfo;

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iget-byte v0, p1, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/broadcom/bt/util/bmsg/BMessage;->setMessageType(B)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-byte v0, p1, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    invoke-virtual {p0, v3}, Lcom/broadcom/bt/util/bmsg/BMessage;->setMessageType(B)V

    goto :goto_0

    :cond_2
    iget-byte v0, p1, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_3

    invoke-virtual {p0, v2}, Lcom/broadcom/bt/util/bmsg/BMessage;->setMessageType(B)V

    goto :goto_0

    :cond_3
    iget-byte v0, p1, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    invoke-virtual {p0, v4}, Lcom/broadcom/bt/util/bmsg/BMessage;->setMessageType(B)V

    goto :goto_0

    :cond_4
    const-string v0, "BtMap.BMessageUtil"

    const-string v1, "Unable to set message type"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static toBMessage(Lcom/broadcom/bt/map/MessageInfo;Ljava/lang/String;BLjava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessage;
    .locals 10
    .param p0    # Lcom/broadcom/bt/map/MessageInfo;
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Ljava/lang/String;

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Lcom/broadcom/bt/util/bmsg/BMessage;

    invoke-direct {v4}, Lcom/broadcom/bt/util/bmsg/BMessage;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v7, 0x0

    :try_start_1
    invoke-static {v4, v7, p1, p0}, Lcom/broadcom/bt/map/BMessageUtil;->setBMessageHeaderInfo(Lcom/broadcom/bt/util/bmsg/BMessage;BLjava/lang/String;Lcom/broadcom/bt/map/MessageInfo;)V

    invoke-virtual {v4}, Lcom/broadcom/bt/util/bmsg/BMessage;->getEnvelope()Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    move-result-object v2

    invoke-virtual {v2}, Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;->addBody()Lcom/broadcom/bt/util/bmsg/BMessageBody;

    move-result-object v0

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Lcom/broadcom/bt/util/bmsg/BMessageBody;->setCharSet(B)V

    invoke-virtual {v0}, Lcom/broadcom/bt/util/bmsg/BMessageBody;->addContent()Lcom/broadcom/bt/util/bmsg/BMessageBodyContent;

    move-result-object v1

    if-nez p2, :cond_0

    iget-byte v7, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    const-string v7, "BtMap.BMessageUtil"

    const-string v8, "Native charset requested"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    iget-object v8, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    iget-object v9, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    invoke-virtual {v4, p3, v7, v8, v9}, Lcom/broadcom/bt/util/bmsg/BMessage;->encodeSMSDeliverPDU(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    const-string v7, "BtMap.BMessageUtil"

    const-string v8, "Native charset requested but encoding failed"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-virtual {v1, p3}, Lcom/broadcom/bt/util/bmsg/BMessageBodyContent;->addMessageContent(Ljava/lang/String;)V

    move-object v3, v4

    :cond_1
    :goto_1
    return-object v3

    :cond_2
    const-string v7, "BtMap.BMessageUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Native charset requested - encoding succeeded - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object p3, v5

    invoke-virtual {v0, p2}, Lcom/broadcom/bt/util/bmsg/BMessageBody;->setCharSet(B)V

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Lcom/broadcom/bt/util/bmsg/BMessageBody;->setEncoding(B)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    move-object v3, v4

    :goto_2
    const-string v7, "BtMap.BMessageUtil"

    const-string v8, "Error creating BMessage"

    invoke-static {v7, v8, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/broadcom/bt/util/bmsg/BMessage;->finish()V

    const/4 v3, 0x0

    goto :goto_1

    :catch_1
    move-exception v6

    goto :goto_2
.end method
