.class public Lcom/android/contacts/model/account/BaseAccountType$PhoneActionAltInflater;
.super Lcom/android/contacts/model/account/BaseAccountType$CommonInflater;
.source "BaseAccountType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/model/account/BaseAccountType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhoneActionAltInflater"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/contacts/model/account/BaseAccountType$CommonInflater;-><init>()V

    return-void
.end method


# virtual methods
.method protected getTypeLabelResource(Ljava/lang/Integer;)I
    .locals 2
    .param p1    # Ljava/lang/Integer;

    const v0, 0x7f0b012c    # com.android.contacts.R.string.sms_other

    if-nez p1, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const v0, 0x7f0b0125    # com.android.contacts.R.string.sms_custom

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b0126    # com.android.contacts.R.string.sms_home

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b0127    # com.android.contacts.R.string.sms_mobile

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0b0128    # com.android.contacts.R.string.sms_work

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0b0129    # com.android.contacts.R.string.sms_fax_work

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0b012a    # com.android.contacts.R.string.sms_fax_home

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0b012b    # com.android.contacts.R.string.sms_pager

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0b012d    # com.android.contacts.R.string.sms_callback

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0b012e    # com.android.contacts.R.string.sms_car

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0b012f    # com.android.contacts.R.string.sms_company_main

    goto :goto_0

    :pswitch_a
    const v0, 0x7f0b0130    # com.android.contacts.R.string.sms_isdn

    goto :goto_0

    :pswitch_b
    const v0, 0x7f0b0131    # com.android.contacts.R.string.sms_main

    goto :goto_0

    :pswitch_c
    const v0, 0x7f0b0132    # com.android.contacts.R.string.sms_other_fax

    goto :goto_0

    :pswitch_d
    const v0, 0x7f0b0133    # com.android.contacts.R.string.sms_radio

    goto :goto_0

    :pswitch_e
    const v0, 0x7f0b0134    # com.android.contacts.R.string.sms_telex

    goto :goto_0

    :pswitch_f
    const v0, 0x7f0b0135    # com.android.contacts.R.string.sms_tty_tdd

    goto :goto_0

    :pswitch_10
    const v0, 0x7f0b0136    # com.android.contacts.R.string.sms_work_mobile

    goto :goto_0

    :pswitch_11
    const v0, 0x7f0b0137    # com.android.contacts.R.string.sms_work_pager

    goto :goto_0

    :pswitch_12
    const v0, 0x7f0b0138    # com.android.contacts.R.string.sms_assistant

    goto :goto_0

    :pswitch_13
    const v0, 0x7f0b0139    # com.android.contacts.R.string.sms_mms

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method protected isCustom(Ljava/lang/Integer;)Z
    .locals 2
    .param p1    # Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
