.class public Lcom/android/contacts/model/account/FallbackAccountType;
.super Lcom/android/contacts/model/account/BaseAccountType;
.source "FallbackAccountType.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/contacts/model/account/FallbackAccountType;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/contacts/model/account/BaseAccountType;-><init>()V

    iput-object v1, p0, Lcom/android/contacts/model/account/FallbackAccountType;->accountType:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/contacts/model/account/FallbackAccountType;->dataSet:Ljava/lang/String;

    const v1, 0x7f0b010f    # com.android.contacts.R.string.account_phone

    iput v1, p0, Lcom/android/contacts/model/account/FallbackAccountType;->titleRes:I

    const/high16 v1, 0x7f030000    # com.android.contacts.R.mipmap.ic_launcher_contacts

    iput v1, p0, Lcom/android/contacts/model/account/FallbackAccountType;->iconRes:I

    iput-object p2, p0, Lcom/android/contacts/model/account/FallbackAccountType;->resourcePackageName:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/contacts/model/account/FallbackAccountType;->syncAdapterPackageName:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindStructuredName(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindDisplayName(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindPhoneticName(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindNickname(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindPhone(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindEmail(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindStructuredPostal(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindIm(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindOrganization(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindPhoto(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindNote(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindWebsite(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    invoke-virtual {p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;->addDataKindSipAddress(Landroid/content/Context;)Lcom/android/contacts/model/dataitem/DataKind;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/contacts/model/account/FallbackAccountType;->mIsInitialized:Z
    :try_end_0
    .catch Lcom/android/contacts/model/account/AccountType$DefinitionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "FallbackAccountType"

    const-string v2, "Problem building account type"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static createWithPackageNameForTest(Landroid/content/Context;Ljava/lang/String;)Lcom/android/contacts/model/account/AccountType;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/android/contacts/model/account/FallbackAccountType;

    invoke-direct {v0, p0, p1}, Lcom/android/contacts/model/account/FallbackAccountType;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public areContactsWritable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
