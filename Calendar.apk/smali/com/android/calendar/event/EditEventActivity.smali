.class public Lcom/android/calendar/event/EditEventActivity;
.super Lcom/android/calendar/AbstractCalendarActivity;
.source "EditEventActivity.java"


# static fields
.field private static mIsMultipane:Z


# instance fields
.field private mEditFragment:Lcom/android/calendar/event/EditEventFragment;

.field private mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/calendar/AbstractCalendarActivity;-><init>()V

    return-void
.end method

.method private getEventInfoFromIntent(Landroid/os/Bundle;)Lcom/android/calendar/CalendarController$EventInfo;
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v12, -0x1

    new-instance v8, Lcom/android/calendar/CalendarController$EventInfo;

    invoke-direct {v8}, Lcom/android/calendar/CalendarController$EventInfo;-><init>()V

    const-wide/16 v6, -0x1

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_5

    :try_start_0
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    :cond_0
    :goto_0
    const-string v10, "allDay"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v10, "beginTime"

    invoke-virtual {v9, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v10, "endTime"

    invoke-virtual {v9, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v10, v4, v12

    if-eqz v10, :cond_2

    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    iput-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    const-string v11, "UTC"

    iput-object v11, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    :cond_1
    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->endTime:Landroid/text/format/Time;

    invoke-virtual {v10, v4, v5}, Landroid/text/format/Time;->set(J)V

    :cond_2
    cmp-long v10, v1, v12

    if-eqz v10, :cond_4

    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    iput-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    if-eqz v0, :cond_3

    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    const-string v11, "UTC"

    iput-object v11, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    :cond_3
    iget-object v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->startTime:Landroid/text/format/Time;

    invoke-virtual {v10, v1, v2}, Landroid/text/format/Time;->set(J)V

    :cond_4
    iput-wide v6, v8, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    if-eqz v0, :cond_6

    const-wide/16 v10, 0x10

    iput-wide v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    :goto_1
    return-object v8

    :cond_5
    if-eqz p1, :cond_0

    const-string v10, "key_event_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "key_event_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    goto :goto_0

    :cond_6
    const-wide/16 v10, 0x0

    iput-wide v10, v8, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    goto :goto_1

    :catch_0
    move-exception v10

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v8, -0x1

    const v7, 0x7f1000b0    # com.android.calendar.R.id.main_frame

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/calendar/AbstractCalendarActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f040032    # com.android.calendar.R.layout.simple_frame_layout

    invoke-virtual {p0, v2}, Lcom/android/calendar/event/EditEventActivity;->setContentView(I)V

    invoke-direct {p0, p1}, Lcom/android/calendar/event/EditEventActivity;->getEventInfoFromIntent(Landroid/os/Bundle;)Lcom/android/calendar/CalendarController$EventInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/event/EditEventFragment;

    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    const v2, 0x7f090007    # com.android.calendar.R.bool.multiple_pane_config

    invoke-static {p0, v2}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v2

    sput-boolean v2, Lcom/android/calendar/event/EditEventActivity;->mIsMultipane:Z

    sget-boolean v2, Lcom/android/calendar/event/EditEventActivity;->mIsMultipane:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/16 v3, 0x8

    const/16 v4, 0xe

    invoke-virtual {v2, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    iget-wide v4, v2, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    cmp-long v2, v4, v8

    if-nez v2, :cond_2

    const v2, 0x7f0c0014    # com.android.calendar.R.string.event_create

    :goto_0
    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setTitle(I)V

    :goto_1
    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    iget-wide v2, v2, Lcom/android/calendar/CalendarController$EventInfo;->id:J

    cmp-long v2, v2, v8

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    :cond_0
    new-instance v2, Lcom/android/calendar/event/EditEventFragment;

    iget-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->mEventInfo:Lcom/android/calendar/CalendarController$EventInfo;

    invoke-direct {v2, v3, v6, v1}, Lcom/android/calendar/event/EditEventFragment;-><init>(Lcom/android/calendar/CalendarController$EventInfo;ZLandroid/content/Intent;)V

    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "editMode"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v2, Lcom/android/calendar/event/EditEventFragment;->mShowModifyDialogOnLaunch:Z

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v0, v7, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->mEditFragment:Lcom/android/calendar/event/EditEventFragment;

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_1
    return-void

    :cond_2
    const v2, 0x7f0c0015    # com.android.calendar.R.string.event_edit

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/16 v3, 0x10

    const/16 v4, 0x1e

    invoke-virtual {v2, v3, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c    # android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lcom/android/calendar/Utils;->returnToCalendarHome(Landroid/content/Context;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/AbstractCalendarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
