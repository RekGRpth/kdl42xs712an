.class Lcom/android/calendar/EventInfoFragment$QueryHandler;
.super Lcom/android/calendar/AsyncQueryService;
.source "EventInfoFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/EventInfoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/calendar/EventInfoFragment;


# direct methods
.method public constructor <init>(Lcom/android/calendar/EventInfoFragment;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-direct {p0, p2}, Lcom/android/calendar/AsyncQueryService;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 25
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-virtual {v2}, Lcom/android/calendar/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    if-eqz v17, :cond_0

    invoke-virtual/range {v17 .. v17}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    if-eqz p3, :cond_1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sparse-switch p1, :sswitch_data_0

    :goto_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move/from16 v0, p1

    # invokes: Lcom/android/calendar/EventInfoFragment;->sendAccessibilityEventIfQueryDone(I)V
    invoke-static {v2, v0}, Lcom/android/calendar/EventInfoFragment;->access$1500(Lcom/android/calendar/EventInfoFragment;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$2300(Lcom/android/calendar/EventInfoFragment;)I

    move-result v2

    const/16 v3, 0x3f

    if-ne v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mLoadingMsgView:Landroid/view/View;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$500(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getAlpha()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    const-wide/16 v2, 0x258

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mLoadingMsgStartTime:J
    invoke-static {v4}, Lcom/android/calendar/EventInfoFragment;->access$400(Lcom/android/calendar/EventInfoFragment;)J

    move-result-wide v9

    sub-long/2addr v6, v9

    sub-long v23, v2, v6

    const-wide/16 v2, 0x0

    cmp-long v2, v23, v2

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$200(Lcom/android/calendar/EventInfoFragment;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    move-wide/from16 v0, v23

    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$200(Lcom/android/calendar/EventInfoFragment;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v2

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$200(Lcom/android/calendar/EventInfoFragment;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v2

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mNoCrossFade:Z
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$2500(Lcom/android/calendar/EventInfoFragment;)Z

    move-result v2

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mAnimateAlpha:Landroid/animation/ObjectAnimator;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$200(Lcom/android/calendar/EventInfoFragment;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_0

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static/range {p3 .. p3}, Lcom/android/calendar/Utils;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v3

    # setter for: Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$602(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # invokes: Lcom/android/calendar/EventInfoFragment;->initEventCursor()Z
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$700(Lcom/android/calendar/EventInfoFragment;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {v17 .. v17}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    # invokes: Lcom/android/calendar/EventInfoFragment;->updateEvent(Landroid/view/View;)V
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$100(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # invokes: Lcom/android/calendar/EventInfoFragment;->prepareReminders()V
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$800(Lcom/android/calendar/EventInfoFragment;)V

    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mEventCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$600(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;

    move-result-object v3

    const/4 v4, 0x4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    sget-object v6, Lcom/android/calendar/EventInfoFragment;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const-string v7, "_id=?"

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/EventInfoFragment$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static/range {p3 .. p3}, Lcom/android/calendar/Utils;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v3

    # setter for: Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$902(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    # invokes: Lcom/android/calendar/EventInfoFragment;->updateCalendar(Landroid/view/View;)V
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$1000(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # invokes: Lcom/android/calendar/EventInfoFragment;->updateTitle()V
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$1100(Lcom/android/calendar/EventInfoFragment;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mIsBusyFreeCalendar:Z
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$1200(Lcom/android/calendar/EventInfoFragment;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mEventId:J
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$1300(Lcom/android/calendar/EventInfoFragment;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v2

    sget-object v5, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x4

    const/4 v4, 0x0

    # getter for: Lcom/android/calendar/EventInfoFragment;->ATTENDEES_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/calendar/EventInfoFragment;->access$1400()[Ljava/lang/String;

    move-result-object v6

    const-string v7, "event_id=?"

    const-string v9, "attendeeName ASC, attendeeEmail ASC"

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/EventInfoFragment$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mHasAlarm:Z
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$1600(Lcom/android/calendar/EventInfoFragment;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mEventId:J
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$1300(Lcom/android/calendar/EventInfoFragment;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v2

    sget-object v5, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    const/16 v3, 0x10

    const/4 v4, 0x0

    # getter for: Lcom/android/calendar/EventInfoFragment;->REMINDERS_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/calendar/EventInfoFragment;->access$1700()[Ljava/lang/String;

    move-result-object v6

    const-string v7, "event_id=?"

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Lcom/android/calendar/EventInfoFragment$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    const/4 v3, 0x4

    # invokes: Lcom/android/calendar/EventInfoFragment;->sendAccessibilityEventIfQueryDone(I)V
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$1500(Lcom/android/calendar/EventInfoFragment;I)V

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    const/16 v3, 0x10

    # invokes: Lcom/android/calendar/EventInfoFragment;->sendAccessibilityEventIfQueryDone(I)V
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$1500(Lcom/android/calendar/EventInfoFragment;I)V

    goto/16 :goto_1

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static/range {p3 .. p3}, Lcom/android/calendar/Utils;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v3

    # setter for: Lcom/android/calendar/EventInfoFragment;->mAttendeesCursor:Landroid/database/Cursor;
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$1802(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    # invokes: Lcom/android/calendar/EventInfoFragment;->initAttendeesCursor(Landroid/view/View;)V
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$1900(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/calendar/EventInfoFragment;->updateResponse(Landroid/view/View;)V

    goto/16 :goto_1

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    invoke-static/range {p3 .. p3}, Lcom/android/calendar/Utils;->matrixCursorFromCursor(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v3

    # setter for: Lcom/android/calendar/EventInfoFragment;->mRemindersCursor:Landroid/database/Cursor;
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$2002(Lcom/android/calendar/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mRemindersCursor:Landroid/database/Cursor;
    invoke-static {v4}, Lcom/android/calendar/EventInfoFragment;->access$2000(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/calendar/EventInfoFragment;->initReminders(Landroid/view/View;Landroid/database/Cursor;)V

    goto/16 :goto_1

    :sswitch_4
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$900(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mHandler:Lcom/android/calendar/EventInfoFragment$QueryHandler;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$2100(Lcom/android/calendar/EventInfoFragment;)Lcom/android/calendar/EventInfoFragment$QueryHandler;

    move-result-object v9

    const/16 v10, 0x8

    const/4 v11, 0x0

    sget-object v12, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v13, Lcom/android/calendar/EventInfoFragment;->CALENDARS_PROJECTION:[Ljava/lang/String;

    const-string v14, "calendar_displayName=?"

    const/4 v2, 0x1

    new-array v15, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v19, v15, v2

    const/16 v16, 0x0

    invoke-virtual/range {v9 .. v16}, Lcom/android/calendar/EventInfoFragment$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f10008f    # com.android.calendar.R.id.calendar_container

    const/16 v6, 0x8

    # invokes: Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V
    invoke-static {v2, v3, v4, v6}, Lcom/android/calendar/EventInfoFragment;->access$2200(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    const/16 v3, 0x8

    # |= operator for: Lcom/android/calendar/EventInfoFragment;->mCurrentQuery:I
    invoke-static {v2, v3}, Lcom/android/calendar/EventInfoFragment;->access$2376(Lcom/android/calendar/EventInfoFragment;I)I

    goto/16 :goto_1

    :sswitch_5
    invoke-virtual/range {v17 .. v17}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    new-instance v22, Landroid/text/SpannableStringBuilder;

    invoke-direct/range {v22 .. v22}, Landroid/text/SpannableStringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$900(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mCalendarsCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$900(Lcom/android/calendar/EventInfoFragment;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_8

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static/range {v20 .. v20}, Lcom/android/calendar/Utils;->isValidEmail(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, " ("

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f10008f    # com.android.calendar.R.id.calendar_container

    const/4 v6, 0x0

    # invokes: Lcom/android/calendar/EventInfoFragment;->setVisibilityCommon(Landroid/view/View;II)V
    invoke-static {v2, v3, v4, v6}, Lcom/android/calendar/EventInfoFragment;->access$2200(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mView:Landroid/view/View;
    invoke-static {v3}, Lcom/android/calendar/EventInfoFragment;->access$000(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f100034    # com.android.calendar.R.id.calendar_name

    move-object/from16 v0, v22

    # invokes: Lcom/android/calendar/EventInfoFragment;->setTextCommon(Landroid/view/View;ILjava/lang/CharSequence;)V
    invoke-static {v2, v3, v4, v0}, Lcom/android/calendar/EventInfoFragment;->access$2400(Lcom/android/calendar/EventInfoFragment;Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$300(Lcom/android/calendar/EventInfoFragment;)Landroid/widget/ScrollView;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->setAlpha(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/EventInfoFragment$QueryHandler;->this$0:Lcom/android/calendar/EventInfoFragment;

    # getter for: Lcom/android/calendar/EventInfoFragment;->mLoadingMsgView:Landroid/view/View;
    invoke-static {v2}, Lcom/android/calendar/EventInfoFragment;->access$500(Lcom/android/calendar/EventInfoFragment;)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_5
        0x10 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method
