.class public Lcom/android/calendar/CalendarEventModel;
.super Ljava/lang/Object;
.source "CalendarEventModel.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/CalendarEventModel$ReminderEntry;,
        Lcom/android/calendar/CalendarEventModel$Attendee;
    }
.end annotation


# instance fields
.field public mAccessLevel:I

.field public mAllDay:Z

.field public mAttendeesList:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/calendar/CalendarEventModel$Attendee;",
            ">;"
        }
    .end annotation
.end field

.field public mAvailability:I

.field public mCalendarAccessLevel:I

.field public mCalendarAllowedAttendeeTypes:Ljava/lang/String;

.field public mCalendarAllowedAvailability:Ljava/lang/String;

.field public mCalendarAllowedReminders:Ljava/lang/String;

.field public mCalendarColor:I

.field public mCalendarDisplayName:Ljava/lang/String;

.field public mCalendarId:J

.field public mCalendarMaxReminders:I

.field public mDefaultReminders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;"
        }
    .end annotation
.end field

.field public mDescription:Ljava/lang/String;

.field public mDuration:Ljava/lang/String;

.field public mEnd:J

.field public mEventStatus:I

.field public mGuestsCanInviteOthers:Z

.field public mGuestsCanModify:Z

.field public mGuestsCanSeeGuests:Z

.field public mHasAlarm:Z

.field public mHasAttendeeData:Z

.field public mId:J

.field public mIsFirstEventInSeries:Z

.field public mIsOrganizer:Z

.field public mLocation:Ljava/lang/String;

.field public mModelUpdatedWithEventCursor:Z

.field public mOrganizer:Ljava/lang/String;

.field public mOrganizerCanRespond:Z

.field public mOrganizerDisplayName:Ljava/lang/String;

.field public mOriginalAllDay:Ljava/lang/Boolean;

.field public mOriginalEnd:J

.field public mOriginalId:J

.field public mOriginalStart:J

.field public mOriginalSyncId:Ljava/lang/String;

.field public mOriginalTime:Ljava/lang/Long;

.field public mOwnerAccount:Ljava/lang/String;

.field public mOwnerAttendeeId:I

.field public mReminders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/CalendarEventModel$ReminderEntry;",
            ">;"
        }
    .end annotation
.end field

.field public mRrule:Ljava/lang/String;

.field public mSelfAttendeeStatus:I

.field public mStart:J

.field public mSyncAccount:Ljava/lang/String;

.field public mSyncAccountType:Ljava/lang/String;

.field public mSyncId:Ljava/lang/String;

.field public mTimezone:Ljava/lang/String;

.field public mTimezone2:Ljava/lang/String;

.field public mTitle:Ljava/lang/String;

.field public mUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, -0x1

    const/4 v5, 0x1

    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mId:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarDisplayName:Ljava/lang/String;

    iput v2, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarColor:I

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizerDisplayName:Ljava/lang/String;

    iput-boolean v5, p0, Lcom/android/calendar/CalendarEventModel;->mIsOrganizer:Z

    iput-boolean v5, p0, Lcom/android/calendar/CalendarEventModel;->mIsFirstEventInSeries:Z

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mStart:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mAllDay:Z

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mHasAlarm:Z

    iput v2, p0, Lcom/android/calendar/CalendarEventModel;->mAvailability:I

    iput-boolean v5, p0, Lcom/android/calendar/CalendarEventModel;->mHasAttendeeData:Z

    iput v6, p0, Lcom/android/calendar/CalendarEventModel;->mSelfAttendeeStatus:I

    iput v6, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAttendeeId:I

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanModify:Z

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanInviteOthers:Z

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanSeeGuests:Z

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizerCanRespond:Z

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarAccessLevel:I

    iput v5, p0, Lcom/android/calendar/CalendarEventModel;->mEventStatus:I

    iput v2, p0, Lcom/android/calendar/CalendarEventModel;->mAccessLevel:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mDefaultReminders:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/calendar/CalendarEventModel;-><init>()V

    const/4 v3, 0x0

    invoke-static {p1, v3}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/calendar/GeneralPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "preferences_default_reminder"

    const-string v4, "-1"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/calendar/CalendarEventModel;->mHasAlarm:Z

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(I)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mDefaultReminders:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->valueOf(I)Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/CalendarEventModel;-><init>(Landroid/content/Context;)V

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string v13, "title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    :cond_2
    const-string v13, "eventLocation"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    :cond_3
    const-string v13, "description"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    :cond_4
    const-string v13, "availability"

    const/4 v14, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v13, -0x1

    if-eq v3, v13, :cond_5

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/calendar/CalendarEventModel;->mAvailability:I

    :cond_5
    const-string v13, "accessLevel"

    const/4 v14, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v13, -0x1

    if-eq v1, v13, :cond_7

    if-lez v1, :cond_6

    add-int/lit8 v1, v1, -0x1

    :cond_6
    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/calendar/CalendarEventModel;->mAccessLevel:I

    :cond_7
    const-string v13, "rrule"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_8

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    :cond_8
    const-string v13, "android.intent.extra.EMAIL"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    const-string v13, "[ ,;]"

    invoke-virtual {v7, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    move-object v2, v6

    array-length v9, v2

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v5, v2, v8

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_9

    const-string v13, "@"

    invoke-virtual {v5, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    invoke-virtual {v13, v5}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    new-instance v14, Lcom/android/calendar/CalendarEventModel$Attendee;

    const-string v15, ""

    invoke-direct {v14, v15, v5}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v5, v14}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V
    .locals 2
    .param p1    # Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addAttendees(Ljava/lang/String;Lcom/android/common/Rfc822Validator;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/common/Rfc822Validator;

    invoke-static {p1, p2}, Lcom/android/calendar/event/EditEventHelper;->getAddressesFromList(Ljava/lang/String;Lcom/android/common/Rfc822Validator;)Ljava/util/LinkedHashSet;

    move-result-object v1

    monitor-enter p0

    :try_start_0
    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/util/Rfc822Token;

    new-instance v2, Lcom/android/calendar/CalendarEventModel$Attendee;

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/android/calendar/CalendarEventModel$Attendee;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v2, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iput-object v4, v2, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, v2}, Lcom/android/calendar/CalendarEventModel;->addAttendee(Lcom/android/calendar/CalendarEventModel$Attendee;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method protected checkOriginalModelFields(Lcom/android/calendar/CalendarEventModel;)Z
    .locals 5
    .param p1    # Lcom/android/calendar/CalendarEventModel;

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mAllDay:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mAllDay:Z

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    if-nez v1, :cond_e

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    if-nez v1, :cond_0

    :cond_2
    iget-wide v1, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    iget-wide v3, p1, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanInviteOthers:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mGuestsCanInviteOthers:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanModify:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mGuestsCanModify:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanSeeGuests:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mGuestsCanSeeGuests:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizerCanRespond:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mOrganizerCanRespond:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarAccessLevel:I

    iget v2, p1, Lcom/android/calendar/CalendarEventModel;->mCalendarAccessLevel:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mModelUpdatedWithEventCursor:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mModelUpdatedWithEventCursor:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mHasAlarm:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mHasAlarm:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mHasAttendeeData:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mHasAttendeeData:Z

    if-ne v1, v2, :cond_0

    iget-wide v1, p0, Lcom/android/calendar/CalendarEventModel;->mId:J

    iget-wide v3, p1, Lcom/android/calendar/CalendarEventModel;->mId:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/CalendarEventModel;->mIsOrganizer:Z

    iget-boolean v2, p1, Lcom/android/calendar/CalendarEventModel;->mIsOrganizer:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    if-nez v1, :cond_f

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_3
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    if-nez v1, :cond_10

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    :cond_4
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    if-nez v1, :cond_11

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    if-nez v1, :cond_0

    :cond_5
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    if-nez v1, :cond_12

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_6
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    if-nez v1, :cond_13

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    :cond_7
    iget v1, p0, Lcom/android/calendar/CalendarEventModel;->mSelfAttendeeStatus:I

    iget v2, p1, Lcom/android/calendar/CalendarEventModel;->mSelfAttendeeStatus:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAttendeeId:I

    iget v2, p1, Lcom/android/calendar/CalendarEventModel;->mOwnerAttendeeId:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    if-nez v1, :cond_14

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_8
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    if-nez v1, :cond_15

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_9
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    if-nez v1, :cond_16

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_a
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    if-nez v1, :cond_17

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_b
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    if-nez v1, :cond_18

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_c
    iget v1, p0, Lcom/android/calendar/CalendarEventModel;->mAvailability:I

    iget v2, p1, Lcom/android/calendar/CalendarEventModel;->mAvailability:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    if-nez v1, :cond_19

    iget-object v1, p1, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_d
    iget v1, p0, Lcom/android/calendar/CalendarEventModel;->mAccessLevel:I

    iget v2, p1, Lcom/android/calendar/CalendarEventModel;->mAccessLevel:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/android/calendar/CalendarEventModel;->mEventStatus:I

    iget v2, p1, Lcom/android/calendar/CalendarEventModel;->mEventStatus:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_e
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    :cond_f
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    :cond_10
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    :cond_11
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    :cond_12
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    :cond_13
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    :cond_14
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    :cond_15
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    :cond_16
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    :cond_17
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    :cond_18
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    :cond_19
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0
.end method

.method public clear()V
    .locals 6

    const/4 v5, -0x1

    const/4 v0, 0x1

    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mId:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizerDisplayName:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/android/calendar/CalendarEventModel;->mIsOrganizer:Z

    iput-boolean v0, p0, Lcom/android/calendar/CalendarEventModel;->mIsFirstEventInSeries:Z

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mStart:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mAllDay:Z

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mHasAlarm:Z

    iput-boolean v0, p0, Lcom/android/calendar/CalendarEventModel;->mHasAttendeeData:Z

    iput v5, p0, Lcom/android/calendar/CalendarEventModel;->mSelfAttendeeStatus:I

    iput v5, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAttendeeId:I

    iput-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanModify:Z

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanInviteOthers:Z

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanSeeGuests:Z

    iput v2, p0, Lcom/android/calendar/CalendarEventModel;->mAccessLevel:I

    iput v0, p0, Lcom/android/calendar/CalendarEventModel;->mEventStatus:I

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizerCanRespond:Z

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarAccessLevel:I

    iput-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mModelUpdatedWithEventCursor:Z

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarAllowedReminders:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarAllowedAttendeeTypes:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarAllowedAvailability:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    instance-of v3, p1, Lcom/android/calendar/CalendarEventModel;

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/android/calendar/CalendarEventModel;

    invoke-virtual {p0, v0}, Lcom/android/calendar/CalendarEventModel;->checkOriginalModelFields(Lcom/android/calendar/CalendarEventModel;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    goto :goto_0

    :cond_6
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    goto :goto_0

    :cond_7
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    goto :goto_0

    :cond_8
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    goto :goto_0

    :cond_9
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    goto :goto_0

    :cond_a
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    if-eqz v3, :cond_c

    move v1, v2

    goto :goto_0

    :cond_b
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    goto :goto_0

    :cond_c
    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    iget-wide v5, v0, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_d

    move v1, v2

    goto :goto_0

    :cond_d
    iget-boolean v3, p0, Lcom/android/calendar/CalendarEventModel;->mIsFirstEventInSeries:Z

    iget-boolean v4, v0, Lcom/android/calendar/CalendarEventModel;->mIsFirstEventInSeries:Z

    if-eq v3, v4, :cond_e

    move v1, v2

    goto :goto_0

    :cond_e
    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    iget-wide v5, v0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_f

    move v1, v2

    goto/16 :goto_0

    :cond_f
    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    iget-wide v5, v0, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_10

    move v1, v2

    goto/16 :goto_0

    :cond_10
    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mStart:J

    iget-wide v5, v0, Lcom/android/calendar/CalendarEventModel;->mStart:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_11

    move v1, v2

    goto/16 :goto_0

    :cond_11
    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    iget-wide v5, v0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_12

    move v1, v2

    goto/16 :goto_0

    :cond_12
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    if-eqz v3, :cond_14

    move v1, v2

    goto/16 :goto_0

    :cond_13
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    move v1, v2

    goto/16 :goto_0

    :cond_14
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    if-nez v3, :cond_15

    iget-object v3, v0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    goto/16 :goto_0

    :cond_15
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto/16 :goto_0
.end method

.method public getAttendeesString()Ljava/lang/String;
    .locals 7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$Attendee;

    iget-object v4, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mName:Ljava/lang/String;

    iget-object v2, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mEmail:Ljava/lang/String;

    iget v6, v0, Lcom/android/calendar/CalendarEventModel$Attendee;->mStatus:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "name:"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " email:"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " status:"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public hashCode()I
    .locals 11

    const/16 v10, 0x20

    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v5, 0x0

    const/16 v0, 0x1f

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mAllDay:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mAttendeesList:Ljava/util/LinkedHashMap;

    if-nez v2, :cond_1

    move v2, v5

    :goto_1
    add-int v1, v6, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    iget-wide v8, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v5

    :goto_2
    add-int v1, v6, v2

    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v5

    :goto_3
    add-int v1, v6, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    iget-wide v8, p0, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanInviteOthers:Z

    if-eqz v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanModify:Z

    if-eqz v2, :cond_5

    move v2, v3

    :goto_5
    add-int v1, v6, v2

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mGuestsCanSeeGuests:Z

    if-eqz v2, :cond_6

    move v2, v3

    :goto_6
    add-int v1, v6, v2

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizerCanRespond:Z

    if-eqz v2, :cond_7

    move v2, v3

    :goto_7
    add-int v1, v6, v2

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mModelUpdatedWithEventCursor:Z

    if-eqz v2, :cond_8

    move v2, v3

    :goto_8
    add-int v1, v6, v2

    mul-int/lit8 v2, v1, 0x1f

    iget v6, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarAccessLevel:I

    add-int v1, v2, v6

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mHasAlarm:Z

    if-eqz v2, :cond_9

    move v2, v3

    :goto_9
    add-int v1, v6, v2

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mHasAttendeeData:Z

    if-eqz v2, :cond_a

    move v2, v3

    :goto_a
    add-int v1, v6, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mId:J

    iget-wide v8, p0, Lcom/android/calendar/CalendarEventModel;->mId:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v6, v6

    add-int v1, v2, v6

    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/calendar/CalendarEventModel;->mIsFirstEventInSeries:Z

    if-eqz v2, :cond_b

    move v2, v3

    :goto_b
    add-int v1, v6, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lcom/android/calendar/CalendarEventModel;->mIsOrganizer:Z

    if-eqz v6, :cond_c

    :goto_c
    add-int v1, v2, v3

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    if-nez v2, :cond_d

    move v2, v5

    :goto_d
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    if-nez v2, :cond_e

    move v2, v5

    :goto_e
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    if-nez v2, :cond_f

    move v2, v5

    :goto_f
    add-int v1, v3, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    ushr-long/2addr v6, v10

    xor-long/2addr v3, v6

    long-to-int v3, v3

    add-int v1, v2, v3

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    if-nez v2, :cond_10

    move v2, v5

    :goto_10
    add-int v1, v3, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    ushr-long/2addr v6, v10

    xor-long/2addr v3, v6

    long-to-int v3, v3

    add-int v1, v2, v3

    mul-int/lit8 v2, v1, 0x1f

    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    ushr-long/2addr v6, v10

    xor-long/2addr v3, v6

    long-to-int v3, v3

    add-int v1, v2, v3

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    if-nez v2, :cond_11

    move v2, v5

    :goto_11
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v5

    :goto_12
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    if-nez v2, :cond_13

    move v2, v5

    :goto_13
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    if-nez v2, :cond_14

    move v2, v5

    :goto_14
    add-int v1, v3, v2

    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/android/calendar/CalendarEventModel;->mSelfAttendeeStatus:I

    add-int v1, v2, v3

    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAttendeeId:I

    add-int v1, v2, v3

    mul-int/lit8 v2, v1, 0x1f

    iget-wide v3, p0, Lcom/android/calendar/CalendarEventModel;->mStart:J

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mStart:J

    ushr-long/2addr v6, v10

    xor-long/2addr v3, v6

    long-to-int v3, v3

    add-int v1, v2, v3

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    if-nez v2, :cond_15

    move v2, v5

    :goto_15
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    if-nez v2, :cond_16

    move v2, v5

    :goto_16
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    if-nez v2, :cond_17

    move v2, v5

    :goto_17
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    if-nez v2, :cond_18

    move v2, v5

    :goto_18
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    if-nez v2, :cond_19

    move v2, v5

    :goto_19
    add-int v1, v3, v2

    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    if-nez v2, :cond_1a

    move v2, v5

    :goto_1a
    add-int v1, v3, v2

    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/android/calendar/CalendarEventModel;->mAvailability:I

    add-int v1, v2, v3

    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    if-nez v3, :cond_1b

    :goto_1b
    add-int v1, v2, v5

    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/android/calendar/CalendarEventModel;->mAccessLevel:I

    add-int v1, v2, v3

    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/android/calendar/CalendarEventModel;->mEventStatus:I

    add-int v1, v2, v3

    return v1

    :cond_0
    move v2, v4

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/CalendarEventModel;->getAttendeesString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    :cond_3
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    :cond_4
    move v2, v4

    goto/16 :goto_4

    :cond_5
    move v2, v4

    goto/16 :goto_5

    :cond_6
    move v2, v4

    goto/16 :goto_6

    :cond_7
    move v2, v4

    goto/16 :goto_7

    :cond_8
    move v2, v4

    goto/16 :goto_8

    :cond_9
    move v2, v4

    goto/16 :goto_9

    :cond_a
    move v2, v4

    goto/16 :goto_a

    :cond_b
    move v2, v4

    goto/16 :goto_b

    :cond_c
    move v3, v4

    goto/16 :goto_c

    :cond_d
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_d

    :cond_e
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOrganizer:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_e

    :cond_f
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalAllDay:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    goto/16 :goto_f

    :cond_10
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_10

    :cond_11
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    goto/16 :goto_11

    :cond_12
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_12

    :cond_13
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->hashCode()I

    move-result v2

    goto/16 :goto_13

    :cond_14
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_14

    :cond_15
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccount:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_15

    :cond_16
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mSyncAccountType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_16

    :cond_17
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_17

    :cond_18
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_18

    :cond_19
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mTimezone2:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_19

    :cond_1a
    iget-object v2, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1a

    :cond_1b
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mUri:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    goto/16 :goto_1b
.end method

.method public isUnchanged(Lcom/android/calendar/CalendarEventModel;)Z
    .locals 8
    .param p1    # Lcom/android/calendar/CalendarEventModel;

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    move v2, v3

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/calendar/CalendarEventModel;->checkOriginalModelFields(Lcom/android/calendar/CalendarEventModel;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p1, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p1, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_3
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    iget-object v4, p1, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_4
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    iget-object v4, p1, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_5
    iget-wide v4, p0, Lcom/android/calendar/CalendarEventModel;->mEnd:J

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalEnd:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    iget-wide v4, p0, Lcom/android/calendar/CalendarEventModel;->mStart:J

    iget-wide v6, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalStart:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    iget-wide v4, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    iget-wide v6, p1, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_6

    iget-wide v4, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    iget-wide v6, p1, Lcom/android/calendar/CalendarEventModel;->mId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    :cond_6
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_10

    iget-object v4, p1, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalSyncId:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/calendar/CalendarEventModel;->mSyncId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    :cond_7
    move v1, v3

    :goto_1
    iget-wide v4, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_8

    iget-wide v4, p0, Lcom/android/calendar/CalendarEventModel;->mOriginalId:J

    iget-wide v6, p1, Lcom/android/calendar/CalendarEventModel;->mId:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_f

    :cond_8
    move v0, v3

    :goto_2
    if-eqz v1, :cond_9

    if-nez v0, :cond_0

    :cond_9
    move v2, v3

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/calendar/CalendarEventModel;->mLocation:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto/16 :goto_0

    :cond_b
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/calendar/CalendarEventModel;->mTitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    goto/16 :goto_0

    :cond_c
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/calendar/CalendarEventModel;->mDescription:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    goto/16 :goto_0

    :cond_d
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/calendar/CalendarEventModel;->mDuration:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    goto/16 :goto_0

    :cond_e
    move v1, v2

    goto :goto_1

    :cond_f
    move v0, v2

    goto :goto_2

    :cond_10
    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    iget-object v5, p1, Lcom/android/calendar/CalendarEventModel;->mRrule:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    goto/16 :goto_0
.end method

.method public isValid()Z
    .locals 5

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/android/calendar/CalendarEventModel;->mCalendarId:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/calendar/CalendarEventModel;->mOwnerAccount:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public normalizeReminders()Z
    .locals 6

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v3, v5, :cond_1

    :cond_0
    return v5

    :cond_1
    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x2

    :goto_0
    if-ltz v1, :cond_0

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarEventModel$ReminderEntry;

    invoke-virtual {v2, v0}, Lcom/android/calendar/CalendarEventModel$ReminderEntry;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/CalendarEventModel;->mReminders:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2
    move-object v2, v0

    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method
