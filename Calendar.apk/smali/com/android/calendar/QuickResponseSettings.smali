.class public Lcom/android/calendar/QuickResponseSettings;
.super Landroid/preference/PreferenceFragment;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field mEditTextPrefs:[Landroid/preference/EditTextPreference;

.field mResponses:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    check-cast p1, Lcom/android/calendar/CalendarSettingsActivity;

    invoke-virtual {p1}, Lcom/android/calendar/CalendarSettingsActivity;->hideMenuButtons()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/calendar/QuickResponseSettings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/calendar/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v6

    const v8, 0x7f0c004a    # com.android.calendar.R.string.quick_response_settings_title

    invoke-virtual {v6, v8}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    invoke-virtual {p0}, Lcom/android/calendar/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/android/calendar/Utils;->getQuickResponses(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    iget-object v8, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    array-length v8, v8

    new-array v8, v8, [Landroid/preference/EditTextPreference;

    iput-object v8, p0, Lcom/android/calendar/QuickResponseSettings;->mEditTextPrefs:[Landroid/preference/EditTextPreference;

    iget-object v8, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    invoke-static {v8}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    array-length v5, v0

    const/4 v4, 0x0

    move v3, v2

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v7, v0, v4

    new-instance v1, Landroid/preference/EditTextPreference;

    invoke-virtual {p0}, Lcom/android/calendar/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v1, v8}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    const v8, 0x7f0c004b    # com.android.calendar.R.string.quick_response_settings_edit_title

    invoke-virtual {v1, v8}, Landroid/preference/EditTextPreference;->setDialogTitle(I)V

    invoke-virtual {v1, v7}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v8, p0, Lcom/android/calendar/QuickResponseSettings;->mEditTextPrefs:[Landroid/preference/EditTextPreference;

    add-int/lit8 v2, v3, 0x1

    aput-object v1, v8, v3

    invoke-virtual {v6, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v4, v4, 0x1

    move v3, v2

    goto :goto_0

    :cond_0
    const-string v8, "QuickResponseSettings"

    const-string v9, "No responses found"

    invoke-static {v8, v9}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0, v6}, Lcom/android/calendar/QuickResponseSettings;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/calendar/QuickResponseSettings;->mEditTextPrefs:[Landroid/preference/EditTextPreference;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/QuickResponseSettings;->mEditTextPrefs:[Landroid/preference/EditTextPreference;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/preference/EditTextPreference;->compareTo(Landroid/preference/Preference;)I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    aput-object p2, v1, v0

    iget-object v1, p0, Lcom/android/calendar/QuickResponseSettings;->mEditTextPrefs:[Landroid/preference/EditTextPreference;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/calendar/QuickResponseSettings;->mEditTextPrefs:[Landroid/preference/EditTextPreference;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/calendar/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "preferences_quick_responses"

    iget-object v3, p0, Lcom/android/calendar/QuickResponseSettings;->mResponses:[Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/android/calendar/Utils;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/calendar/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/CalendarSettingsActivity;

    invoke-virtual {v0}, Lcom/android/calendar/CalendarSettingsActivity;->isMultiPane()Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0c004a    # com.android.calendar.R.string.quick_response_settings_title

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSettingsActivity;->setTitle(I)V

    :cond_0
    return-void
.end method
