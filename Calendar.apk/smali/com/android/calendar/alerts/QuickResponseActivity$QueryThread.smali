.class Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;
.super Ljava/lang/Thread;
.source "QuickResponseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/alerts/QuickResponseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryThread"
.end annotation


# instance fields
.field mBody:Ljava/lang/String;

.field mEventId:J

.field final synthetic this$0:Lcom/android/calendar/alerts/QuickResponseActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/QuickResponseActivity;JLjava/lang/String;)V
    .locals 0
    .param p2    # J
    .param p4    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->this$0:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-wide p2, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->mEventId:J

    iput-object p4, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->mBody:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v2, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->this$0:Lcom/android/calendar/alerts/QuickResponseActivity;

    iget-wide v3, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->mEventId:J

    iget-object v5, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->mBody:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/alerts/AlertReceiver;->createEmailIntent(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->this$0:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-virtual {v2, v0}, Lcom/android/calendar/alerts/QuickResponseActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->this$0:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-virtual {v2}, Lcom/android/calendar/alerts/QuickResponseActivity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;->this$0:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-virtual {v2}, Lcom/android/calendar/alerts/QuickResponseActivity;->getListView()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread$1;

    invoke-direct {v3, p0}, Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread$1;-><init>(Lcom/android/calendar/alerts/QuickResponseActivity$QueryThread;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
