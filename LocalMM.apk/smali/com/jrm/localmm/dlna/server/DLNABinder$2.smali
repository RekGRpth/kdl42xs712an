.class Lcom/jrm/localmm/dlna/server/DLNABinder$2;
.super Ljava/lang/Object;
.source "DLNABinder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/dlna/server/DLNABinder;->initBinder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/dlna/server/DLNABinder;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->dlna:Landroid/net/dlna/DLNA;
    invoke-static {v0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$300(Lcom/jrm/localmm/dlna/server/DLNABinder;)Landroid/net/dlna/DLNA;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/net/dlna/DLNA;->Initialize(Ljava/lang/String;I)Z

    const-string v0, "DLNABinder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->dlna:Landroid/net/dlna/DLNA;
    invoke-static {v2}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$300(Lcom/jrm/localmm/dlna/server/DLNABinder;)Landroid/net/dlna/DLNA;

    move-result-object v2

    invoke-interface {v2}, Landroid/net/dlna/DLNA;->GetIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->dlna:Landroid/net/dlna/DLNA;
    invoke-static {v2}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$300(Lcom/jrm/localmm/dlna/server/DLNABinder;)Landroid/net/dlna/DLNA;

    move-result-object v2

    invoke-interface {v2}, Landroid/net/dlna/DLNA;->GetPort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # invokes: Lcom/jrm/localmm/dlna/server/DLNABinder;->isXMLExist()Z
    invoke-static {v0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$400(Lcom/jrm/localmm/dlna/server/DLNABinder;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # invokes: Lcom/jrm/localmm/dlna/server/DLNABinder;->replaceAssetsFiles()V
    invoke-static {v0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$500(Lcom/jrm/localmm/dlna/server/DLNABinder;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->dlna:Landroid/net/dlna/DLNA;
    invoke-static {v0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$300(Lcom/jrm/localmm/dlna/server/DLNABinder;)Landroid/net/dlna/DLNA;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$2;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceListener:Landroid/net/dlna/DeviceListener;
    invoke-static {v1}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$600(Lcom/jrm/localmm/dlna/server/DLNABinder;)Landroid/net/dlna/DeviceListener;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/net/dlna/DLNA;->SetDeviceListener(Landroid/net/dlna/DeviceListener;)V

    return-void
.end method
