.class public Lcom/jrm/localmm/dlna/server/DLNABinder;
.super Landroid/os/Binder;
.source "DLNABinder.java"


# instance fields
.field private context:Landroid/content/Context;

.field private deviceChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/ui/main/DeviceChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private deviceListener:Landroid/net/dlna/DeviceListener;

.field private dlna:Landroid/net/dlna/DLNA;

.field private mediaServerControllers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/MediaServerDisplay;",
            ">;"
        }
    .end annotation
.end field

.field private mediaServerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/jrm/localmm/business/data/MediaServerDisplay;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerControllers:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerMap:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceChangeListeners:Ljava/util/List;

    new-instance v0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/dlna/server/DLNABinder$1;-><init>(Lcom/jrm/localmm/dlna/server/DLNABinder;)V

    iput-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceListener:Landroid/net/dlna/DeviceListener;

    iput-object p1, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->context:Landroid/content/Context;

    invoke-static {}, Landroid/net/dlna/DLNAFactory;->CreateInstance()Landroid/net/dlna/DLNA;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->dlna:Landroid/net/dlna/DLNA;

    invoke-virtual {p0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->initBinder()V

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerControllers:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceChangeListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/jrm/localmm/dlna/server/DLNABinder;)Landroid/net/dlna/DLNA;
    .locals 1
    .param p0    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->dlna:Landroid/net/dlna/DLNA;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/dlna/server/DLNABinder;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-direct {p0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->isXMLExist()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/dlna/server/DLNABinder;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-direct {p0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->replaceAssetsFiles()V

    return-void
.end method

.method static synthetic access$600(Lcom/jrm/localmm/dlna/server/DLNABinder;)Landroid/net/dlna/DeviceListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceListener:Landroid/net/dlna/DeviceListener;

    return-object v0
.end method

.method private copyFile(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    :cond_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v3, Ljava/io/BufferedOutputStream;

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v5, 0x1400

    new-array v0, v5, [B

    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v2

    const/4 v5, -0x1

    if-eq v2, v5, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v2}, Ljava/io/BufferedOutputStream;->write([BII)V

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method private isXMLExist()Z
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/data/com.jrm.filefly/DLNA.xml"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/jrm/localmm/dlna/server/DLNABinder;

    const-string v1, "DLNA.xml exist."

    invoke-static {v0, v1}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printI(Ljava/lang/Class;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-class v0, Lcom/jrm/localmm/dlna/server/DLNABinder;

    const-string v1, "DLNA.xml not exist."

    invoke-static {v0, v1}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printI(Ljava/lang/Class;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private replaceAssetsFiles()V
    .locals 9

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/data/com.jrm.filefly/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, v2, v1

    const-class v4, Lcom/jrm/localmm/dlna/server/DLNABinder;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fileName:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printI(Ljava/lang/Class;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/data/data/com.jrm.filefly/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-nez v4, :cond_1

    :try_start_1
    iget-object v4, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/data/data/com.jrm.filefly/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-class v6, Lcom/jrm/localmm/dlna/server/DLNABinder;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createFile:/data/data/com.jrm.filefly/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printI(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-direct {p0, v4, v5}, Lcom/jrm/localmm/dlna/server/DLNABinder;->copyFile(Ljava/io/InputStream;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-class v4, Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printI(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_3
    return-void
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/jrm/localmm/ui/main/DeviceChangeListener;)V
    .locals 2

    const-class v0, Lcom/jrm/localmm/dlna/server/DLNABinder;

    const-string v1, "binder.addDeviceChangeListener"

    invoke-static {v0, v1}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printI(Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public finalizeDlna()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/dlna/server/DLNABinder$3;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/dlna/server/DLNABinder$3;-><init>(Lcom/jrm/localmm/dlna/server/DLNABinder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getMediaServerControllers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/MediaServerDisplay;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerControllers:Ljava/util/List;

    return-object v0
.end method

.method public initBinder()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/dlna/server/DLNABinder$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/dlna/server/DLNABinder$2;-><init>(Lcom/jrm/localmm/dlna/server/DLNABinder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
