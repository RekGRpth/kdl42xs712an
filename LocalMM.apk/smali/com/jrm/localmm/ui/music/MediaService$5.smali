.class Lcom/jrm/localmm/ui/music/MediaService$5;
.super Ljava/lang/Object;
.source "MediaService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MediaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MediaService;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MediaService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MediaService$5;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$5;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # getter for: Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/ui/music/MediaService;->access$200(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "MediaService"

    const-string v3, "MediaService onSeekComplete"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$5;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/music/MediaService;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    :try_start_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$5;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # getter for: Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/music/MediaService;->access$400(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicSeekCompleted()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
