.class public Lcom/jrm/localmm/ui/music/LrcRead;
.super Ljava/lang/Object;
.source "LrcRead.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/music/LrcRead$Sort;
    }
.end annotation


# instance fields
.field private LyricList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/ui/music/LyricContent;",
            ">;"
        }
    .end annotation
.end field

.field private bis:Ljava/io/BufferedInputStream;

.field private isClose:Z

.field private mBufferedReader:Ljava/io/BufferedReader;

.field private mInputStreamReader:Ljava/io/InputStreamReader;

.field private mLyricContent:Lcom/jrm/localmm/ui/music/LyricContent;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/music/LrcRead;->isClose:Z

    new-instance v0, Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-direct {v0}, Lcom/jrm/localmm/ui/music/LyricContent;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mLyricContent:Lcom/jrm/localmm/ui/music/LyricContent;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/LrcRead;->LyricList:Ljava/util/List;

    return-void
.end method

.method private AnalyzeLRC(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/lang/String;

    const-wide/16 v11, -0x1

    const/4 v10, -0x1

    :try_start_0
    const-string v7, "["

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const-string v7, "]"

    invoke-virtual {p1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-nez v3, :cond_6

    if-eq v4, v10, :cond_6

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/music/LrcRead;->GetPossiblyTagCount(Ljava/lang/String;)I

    move-result v7

    new-array v6, v7, [Ljava/lang/Long;

    const/4 v7, 0x0

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p1, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/music/LrcRead;->TimeToLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v7, v7, v11

    if-nez v7, :cond_1

    const-string v5, ""

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    move-object v5, p1

    const/4 v1, 0x1

    :cond_2
    :goto_1
    if-nez v3, :cond_4

    if-eq v4, v10, :cond_4

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "["

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const-string v7, "]"

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v10, :cond_2

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v5, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/jrm/localmm/ui/music/LrcRead;->TimeToLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v1

    aget-object v7, v6, v1

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v7, v7, v11

    if-nez v7, :cond_3

    const-string v5, ""

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_2
    array-length v7, v6

    if-ge v2, v7, :cond_0

    aget-object v7, v6, v2

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mLyricContent:Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-virtual {v7, v5}, Lcom/jrm/localmm/ui/music/LyricContent;->setLyric(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mLyricContent:Lcom/jrm/localmm/ui/music/LyricContent;

    aget-object v8, v6, v2

    invoke-virtual {v8}, Ljava/lang/Long;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/jrm/localmm/ui/music/LyricContent;->setLyricTime(I)V

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->LyricList:Ljava/util/List;

    iget-object v8, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mLyricContent:Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-direct {v7}, Lcom/jrm/localmm/ui/music/LyricContent;-><init>()V

    iput-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mLyricContent:Lcom/jrm/localmm/ui/music/LyricContent;

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    const-string v5, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, ""

    goto :goto_0
.end method

.method private GetPossiblyTagCount(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v2, "\\["

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\]"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v0

    if-nez v2, :cond_0

    array-length v2, v1

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    array-length v2, v0

    array-length v3, v1

    if-le v2, v3, :cond_1

    array-length v2, v0

    goto :goto_0

    :cond_1
    array-length v2, v1

    goto :goto_0
.end method


# virtual methods
.method public GetCharset(Ljava/io/File;)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/io/File;

    const/16 v12, 0xbf

    const/16 v11, 0x80

    const/4 v10, -0x1

    const-string v0, "GBK"

    const/4 v7, 0x3

    new-array v5, v7, [B

    const/4 v2, 0x0

    :try_start_0
    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    :cond_0
    new-instance v7, Ljava/io/BufferedInputStream;

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v7, v5, v8, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v6

    if-ne v6, v10, :cond_1

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_1
    const/4 v7, 0x0

    aget-byte v7, v5, v7

    if-ne v7, v10, :cond_4

    const/4 v7, 0x1

    aget-byte v7, v5, v7

    const/4 v8, -0x2

    if-ne v7, v8, :cond_4

    const-string v0, "UTF-16LE"

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_3

    :cond_2
    :goto_2
    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->read()I

    move-result v6

    if-eq v6, v10, :cond_3

    const/16 v7, 0xf0

    if-lt v6, v7, :cond_8

    :cond_3
    :goto_3
    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    :goto_4
    move-object v1, v0

    goto :goto_0

    :cond_4
    const/4 v7, 0x0

    aget-byte v7, v5, v7

    const/4 v8, -0x2

    if-ne v7, v8, :cond_5

    const/4 v7, 0x1

    aget-byte v7, v5, v7

    if-ne v7, v10, :cond_5

    const-string v0, "UTF-16BE"

    const/4 v2, 0x1

    goto :goto_1

    :cond_5
    const/4 v7, 0x0

    aget-byte v7, v5, v7

    const/16 v8, -0x11

    if-ne v7, v8, :cond_6

    const/4 v7, 0x1

    aget-byte v7, v5, v7

    const/16 v8, -0x45

    if-ne v7, v8, :cond_6

    const/4 v7, 0x2

    aget-byte v7, v5, v7

    const/16 v8, -0x41

    if-ne v7, v8, :cond_6

    const-string v0, "UTF-8"

    const/4 v2, 0x1

    goto :goto_1

    :cond_6
    const/4 v7, 0x0

    aget-byte v7, v5, v7

    if-ne v7, v10, :cond_7

    const/4 v7, 0x1

    aget-byte v7, v5, v7

    const/4 v8, -0x2

    if-ne v7, v8, :cond_7

    const-string v0, "unicode"

    const/4 v2, 0x1

    goto :goto_1

    :cond_7
    const-string v0, "GBK"

    const/4 v2, 0x1

    goto :goto_1

    :cond_8
    if-gt v11, v6, :cond_9

    if-le v6, v12, :cond_3

    :cond_9
    const/16 v7, 0xc0

    if-gt v7, v6, :cond_a

    const/16 v7, 0xdf

    if-gt v6, v7, :cond_a

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->read()I

    move-result v6

    if-gt v11, v6, :cond_3

    if-gt v6, v12, :cond_3

    goto :goto_2

    :cond_a
    const/16 v7, 0xe0

    if-gt v7, v6, :cond_2

    const/16 v7, 0xef

    if-gt v6, v7, :cond_2

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->read()I

    move-result v6

    if-gt v11, v6, :cond_3

    if-gt v6, v12, :cond_3

    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->read()I

    move-result v6

    if-gt v11, v6, :cond_3

    if-gt v6, v12, :cond_3

    const-string v0, "UTF-8"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    :try_start_1
    iget-object v7, p0, Lcom/jrm/localmm/ui/music/LrcRead;->bis:Ljava/io/BufferedInputStream;

    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4
.end method

.method public GetLyricContent()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/ui/music/LyricContent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/LrcRead;->LyricList:Ljava/util/List;

    return-object v0
.end method

.method public Read(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, ""

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/music/LrcRead;->GetCharset(Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mInputStreamReader:Ljava/io/InputStreamReader;

    new-instance v3, Ljava/io/BufferedReader;

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mInputStreamReader:Ljava/io/InputStreamReader;

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mBufferedReader:Ljava/io/BufferedReader;

    :goto_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mBufferedReader:Ljava/io/BufferedReader;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->isClose:Z

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/music/LrcRead;->AnalyzeLRC(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->LyricList:Ljava/util/List;

    new-instance v4, Lcom/jrm/localmm/ui/music/LrcRead$Sort;

    invoke-direct {v4, p0}, Lcom/jrm/localmm/ui/music/LrcRead$Sort;-><init>(Lcom/jrm/localmm/ui/music/LrcRead;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mBufferedReader:Ljava/io/BufferedReader;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mInputStreamReader:Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/music/LrcRead;->isClose:Z

    return-void
.end method

.method public TimeToLong(Ljava/lang/String;)J
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x1

    :try_start_0
    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    aget-object v6, v3, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v6, 0x1

    aget-object v6, v3, v6

    const-string v7, "\\."

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v1, 0x0

    array-length v6, v4

    if-le v6, v8, :cond_0

    const/4 v6, 0x1

    aget-object v6, v4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    mul-int/lit8 v6, v2, 0x3c

    mul-int/lit16 v6, v6, 0x3e8

    mul-int/lit16 v7, v5, 0x3e8

    add-int/2addr v6, v7

    mul-int/lit8 v7, v1, 0xa

    add-int/2addr v6, v7

    int-to-long v6, v6

    :goto_0
    return-wide v6

    :catch_0
    move-exception v0

    const-wide/16 v6, -0x1

    goto :goto_0
.end method

.method public close()V
    .locals 2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mBufferedReader:Ljava/io/BufferedReader;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/LrcRead;->isClose:Z

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mBufferedReader:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mInputStreamReader:Ljava/io/InputStreamReader;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/LrcRead;->isClose:Z

    if-nez v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/LrcRead;->mInputStreamReader:Ljava/io/InputStreamReader;

    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
