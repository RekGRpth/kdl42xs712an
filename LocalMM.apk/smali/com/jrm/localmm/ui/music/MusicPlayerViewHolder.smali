.class public Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
.super Ljava/lang/Object;
.source "MusicPlayerViewHolder.java"


# instance fields
.field protected bt_MusicCir:Landroid/widget/ImageView;

.field protected bt_MusicImageAlbum:Landroid/widget/ImageView;

.field protected bt_MusicInfo:Landroid/widget/ImageView;

.field protected bt_MusicList:Landroid/widget/ImageView;

.field protected bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

.field protected bt_MusicPlay:Landroid/widget/ImageView;

.field protected bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

.field protected currentMusicArtist:Landroid/widget/TextView;

.field protected currentMusicNum:Landroid/widget/TextView;

.field protected currentMusicTitle:Landroid/widget/TextView;

.field protected durationTime:Landroid/widget/TextView;

.field protected listLayout:Landroid/widget/LinearLayout;

.field protected mLyricView:Lcom/jrm/localmm/ui/music/LyricView;

.field private mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

.field protected musicListView:Landroid/widget/ListView;

.field protected playLayout:Lcom/jrm/localmm/ui/music/ViewGroupHook;

.field protected playTime:Landroid/widget/TextView;

.field private rootLayout:Landroid/widget/RelativeLayout;

.field protected scrollView:Lcom/jrm/localmm/ui/music/ScrollableViewGroup;

.field protected seekBar:Landroid/widget/SeekBar;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    return-void
.end method


# virtual methods
.method protected findView()V
    .locals 3

    const v2, 0x7f080059    # com.jrm.localmm.R.id.txtLapse

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    const v1, 0x7f08005c    # com.jrm.localmm.R.id.music_suspension_layout2

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08005d    # com.jrm.localmm.R.id.ViewFlipper

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->scrollView:Lcom/jrm/localmm/ui/music/ScrollableViewGroup;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->scrollView:Lcom/jrm/localmm/ui/music/ScrollableViewGroup;

    const v1, 0x7f080060    # com.jrm.localmm.R.id.frmList

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->listLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->listLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f08003a    # com.jrm.localmm.R.id.PlayList

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->musicListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->scrollView:Lcom/jrm/localmm/ui/music/ScrollableViewGroup;

    const v1, 0x7f08005f    # com.jrm.localmm.R.id.frmMain

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/ViewGroupHook;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->playLayout:Lcom/jrm/localmm/ui/music/ViewGroupHook;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080053    # com.jrm.localmm.R.id.music_previous

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080054    # com.jrm.localmm.R.id.music_play

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080055    # com.jrm.localmm.R.id.music_next

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080056    # com.jrm.localmm.R.id.music_cir

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080057    # com.jrm.localmm.R.id.music_list

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080058    # com.jrm.localmm.R.id.music_info

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080062    # com.jrm.localmm.R.id.music_image_album

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicImageAlbum:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080050    # com.jrm.localmm.R.id.music_current_artist

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicArtist:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08004f    # com.jrm.localmm.R.id.music_current_name

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f080051    # com.jrm.localmm.R.id.music_current_num

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicNum:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->playTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08005b    # com.jrm.localmm.R.id.txtDuration

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->durationTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->playTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    const v1, 0x7f080064    # com.jrm.localmm.R.id.LyricShow

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/LyricView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mLyricView:Lcom/jrm/localmm/ui/music/LyricView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->rootLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f08005a    # com.jrm.localmm.R.id.skbGuage

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    return-void
.end method
