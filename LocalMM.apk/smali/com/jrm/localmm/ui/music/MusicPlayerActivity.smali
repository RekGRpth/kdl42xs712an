.class public Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
.super Landroid/app/Activity;
.source "MusicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;
    }
.end annotation


# static fields
.field public static countTime:I

.field public static currentPlayStatus:I

.field public static currentPosition:I

.field private static currentTime:I

.field private static index:I

.field private static mLrcRead:Lcom/jrm/localmm/ui/music/LrcRead;

.field protected static mService:Lcom/jrm/localmm/ui/music/IMediaService;

.field public static musicList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field public static state:I

.field public static thread:Ljava/lang/Thread;


# instance fields
.field private appSkin:Lcom/mstar/android/tv/TvCommonManager;

.field protected clickable:Z

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private isAudioSupport:Z

.field public isLrcShow:Z

.field protected isNextMusic:Z

.field public isPrepared:Z

.field private isVideoSupport:Z

.field private mLyricList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/ui/music/LyricContent;",
            ">;"
        }
    .end annotation
.end field

.field private mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

.field private mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

.field public musicPlayHandle:Landroid/os/Handler;

.field private musicServiceConnection:Landroid/content/ServiceConnection;

.field private musicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

.field private netDisconnectReceiver:Landroid/content/BroadcastReceiver;

.field private path:Ljava/lang/String;

.field private receiver:Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;

.field private scanListener:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

.field public scrollstate:I

.field private shutdownReceiver:Landroid/content/BroadcastReceiver;

.field private sourceChangeReceiver:Landroid/content/BroadcastReceiver;

.field private sourceFrom:I

.field private stToken:Lcom/jrm/localmm/util/MusicUtils$ServiceToken;

.field private stopMusicReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0}, Ljava/lang/Thread;-><init>()V

    sput-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->thread:Ljava/lang/Thread;

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->index:I

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    const/4 v0, 0x0

    sput-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->path:Ljava/lang/String;

    iput v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->scrollstate:I

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isLrcShow:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isNextMusic:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->clickable:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isAudioSupport:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isVideoSupport:Z

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$6;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$6;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicServiceConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$7;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$7;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->scanListener:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$8;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$8;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->netDisconnectReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$9;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$9;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->shutdownReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$10;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$10;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stopMusicReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$11;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$11;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceChangeReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->exceptionProcess(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isVideoSupport:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isVideoSupport:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stopMusicService()V

    return-void
.end method

.method static synthetic access$1300(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceFrom:I

    return v0
.end method

.method static synthetic access$1500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/mstar/android/tv/TvCommonManager;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/mstar/android/tv/TvCommonManager;)Lcom/mstar/android/tv/TvCommonManager;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .param p1    # Lcom/mstar/android/tv/TvCommonManager;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object p1
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isAudioSupport:Z

    return v0
.end method

.method static synthetic access$202(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isAudioSupport:Z

    return p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->exitMusicPaly()V

    return-void
.end method

.method static synthetic access$400()I
    .locals 1

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    return v0
.end method

.method static synthetic access$402(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    return p0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->setLRCIndex()V

    return-void
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->index:I

    return v0
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->initPlay()V

    return-void
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayPrepare()V

    return-void
.end method

.method private changeSource()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private continuing()V
    .locals 5

    const/4 v1, 0x1

    sput-boolean v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    const/4 v1, 0x2

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v2, 0x7

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :try_start_0
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMediaService;->continuePlay()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private exceptionProcess(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v1, 0x11

    invoke-static {p0, p1, v1}, Lcom/jrm/localmm/util/ToastFactory;->getToast(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->exitMusicPaly()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayCompletion()V

    goto :goto_0
.end method

.method private exitMusicPaly()V
    .locals 5

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v1, :cond_0

    :try_start_0
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMediaService;->stop()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/16 v2, 0xc

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initCurrentPosition()V
    .locals 3

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getDuration()I

    move-result v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->durationTime:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/jrm/localmm/util/MusicUtils;->durationToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setMax(I)V

    :cond_0
    return-void
.end method

.method private initLrc()V
    .locals 8

    const/4 v7, 0x0

    sget-object v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLrcRead:Lcom/jrm/localmm/ui/music/LrcRead;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLrcRead:Lcom/jrm/localmm/ui/music/LrcRead;

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/music/LrcRead;->close()V

    :cond_0
    iput-boolean v7, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isLrcShow:Z

    new-instance v5, Lcom/jrm/localmm/ui/music/LrcRead;

    invoke-direct {v5}, Lcom/jrm/localmm/ui/music/LrcRead;-><init>()V

    sput-object v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLrcRead:Lcom/jrm/localmm/ui/music/LrcRead;

    :try_start_0
    sget-object v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v6, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v5}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v6, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v5}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".lrc"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLrcRead:Lcom/jrm/localmm/ui/music/LrcRead;

    invoke-virtual {v5, v3}, Lcom/jrm/localmm/ui/music/LrcRead;->Read(Ljava/lang/String;)V

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isLrcShow:Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    sget-object v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLrcRead:Lcom/jrm/localmm/ui/music/LrcRead;

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/music/LrcRead;->GetLyricContent()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    return-void

    :cond_1
    const/4 v5, 0x0

    :try_start_1
    iput-boolean v5, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isLrcShow:Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    iput-boolean v7, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isLrcShow:Z

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private initPlay()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    if-ltz v2, :cond_0

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    sget-object v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->path:Ljava/lang/String;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->path:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v4, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isVideoSupport:Z

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isAudioSupport:Z

    iput-boolean v5, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    :try_start_0
    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->path:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/jrm/localmm/ui/music/IMediaService;->initPlayer(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    new-array v1, v4, [Ljava/lang/String;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->path:Ljava/lang/String;

    aput-object v2, v1, v5

    iget v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceFrom:I

    const/16 v3, 0x10

    if-eq v2, v3, :cond_3

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->scanListener:Landroid/media/MediaScannerConnection$OnScanCompletedListener;

    invoke-static {p0, v1, v2, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    :cond_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    sget-object v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v4, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v2, v3, v4}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->getPicOfAlbum(Ljava/util/List;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    sget-object v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v4, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v2, v3, v4}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setSongsContent(Ljava/util/List;I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->initLrc()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->initCurrentPosition()V

    const/4 v2, 0x2

    sput v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setViewDefault()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->updateListViewSelection()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private pause()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    :try_start_0
    sget-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v0}, Lcom/jrm/localmm/ui/music/IMediaService;->pause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private processPlayPrepare()V
    .locals 6

    const/4 v5, 0x1

    :try_start_0
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMediaService;->getDuration()J

    move-result-wide v1

    long-to-int v1, v1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    const/4 v1, 0x2

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v1, v2, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->getPicOfAlbum(Ljava/util/List;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v1, v2, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setSongsContent(Ljava/util/List;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mLyricView:Lcom/jrm/localmm/ui/music/LyricView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/LyricView;->setSentenceEntities(Ljava/util/List;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mLyricView:Lcom/jrm/localmm/ui/music/LyricView;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/LyricView;->invalidate()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    const/4 v1, 0x0

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->playTime:Landroid/widget/TextView;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    invoke-static {v2}, Lcom/jrm/localmm/util/Tools;->formatDuration2(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    :try_start_1
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMediaService;->getDuration()J

    move-result-wide v1

    long-to-int v1, v1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    :goto_1
    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->durationTime:Landroid/widget/TextView;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    invoke-static {v2}, Lcom/jrm/localmm/util/Tools;->formatDuration2(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v2, 0x7

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const-string v1, "MusicPlayerActivity"

    const-string v2, "MusicPlayActivity.HANDLE_MESSAGE_SEEKBAR_UPDATE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private rewind()V
    .locals 3

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit16 v0, v0, -0x2710

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v2, v0}, Lcom/jrm/localmm/ui/music/IMediaService;->playerToPosiztion(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private setLRCIndex()V
    .locals 5

    const/4 v4, 0x5

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    if-ge v1, v2, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/LyricContent;->getLyricTime()I

    move-result v1

    if-ge v2, v1, :cond_1

    if-nez v0, :cond_1

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->index:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/LyricContent;->getLyricTime()I

    move-result v1

    if-le v2, v1, :cond_0

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/LyricContent;->getLyricTime()I

    move-result v1

    if-ge v2, v1, :cond_0

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->index:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mLyricList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/ui/music/LyricContent;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/LyricContent;->getLyricTime()I

    move-result v1

    if-le v2, v1, :cond_0

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->index:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_3
    return-void
.end method

.method private stopMusicService()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method private wind()V
    .locals 6

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v2, :cond_0

    int-to-long v2, v0

    :try_start_0
    sget-object v4, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v4}, Lcom/jrm/localmm/ui/music/IMediaService;->getDuration()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    add-int/lit16 v0, v0, 0x2710

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v2, v0}, Lcom/jrm/localmm/ui/music/IMediaService;->playerToPosiztion(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected nextMusic()V
    .locals 3

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isNextMusic:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayCompletion()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v1, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setSongsContent(Ljava/util/List;I)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    invoke-static {}, Ljava/lang/System;->gc()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x3

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f03000d    # com.jrm.localmm.R.layout.music_player_init

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    const/4 v3, 0x2

    sput v3, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.jrm.index"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "sourceFrom"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceFrom:I

    iget v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceFrom:I

    const/16 v4, 0x10

    if-ne v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.jrm.arraylist"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    sput-object v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    :goto_0
    new-instance v3, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    invoke-direct {v3, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    iput-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->findView()V

    new-instance v3, Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    invoke-direct {v3, p0, v4}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;)V

    iput-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->addMusicListener()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->initPlayMode()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_SHUTDOWN"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->shutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v3, "com.jrm.dtv.stop.music"

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stopMusicReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "source.switch.from.storage"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v2}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_0
    iget v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceFrom:I

    const/16 v4, 0x11

    if-ne v3, v4, :cond_1

    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v3

    sput-object v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    sget-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stToken:Lcom/jrm/localmm/util/MusicUtils$ServiceToken;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stToken:Lcom/jrm/localmm/util/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/jrm/localmm/util/MusicUtils;->unbindFromService(Lcom/jrm/localmm/util/MusicUtils$ServiceToken;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->netDisconnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->shutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stopMusicReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v3, 0x10

    const/4 v2, 0x0

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stopMusicService()V

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    const/16 v1, 0x7e

    if-ne p1, v1, :cond_4

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_1
    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->continuing()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    :cond_4
    const/16 v1, 0x7f

    if-ne p1, v1, :cond_6

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_2
    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->pause()V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006b    # com.jrm.localmm.R.drawable.player_icon_play

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    :cond_6
    const/16 v1, 0x57

    if-ne p1, v1, :cond_7

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceFrom:I

    if-eq v1, v3, :cond_0

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->nextMusic()V

    goto :goto_0

    :cond_7
    const/16 v1, 0x58

    if-ne p1, v1, :cond_8

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->sourceFrom:I

    if-eq v1, v3, :cond_0

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->preMusic()V

    goto/16 :goto_0

    :cond_8
    const/16 v1, 0x133

    if-eq p1, v1, :cond_9

    const/16 v1, 0x59

    if-ne p1, v1, :cond_a

    :cond_9
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->rewind()V

    goto/16 :goto_0

    :cond_a
    const/16 v1, 0x106

    if-eq p1, v1, :cond_b

    const/16 v1, 0x5a

    if-ne p1, v1, :cond_c

    :cond_b
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->wind()V

    goto/16 :goto_0

    :cond_c
    const/16 v1, 0x52

    if-eq p1, v1, :cond_0

    const/16 v1, 0x15

    if-ne p1, v1, :cond_d

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->drapLeft()V

    goto/16 :goto_0

    :cond_d
    const/16 v1, 0x16

    if-ne p1, v1, :cond_e

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->drapRight()V

    goto/16 :goto_0

    :cond_e
    const/16 v1, 0x42

    if-eq p1, v1, :cond_f

    const/16 v1, 0x17

    if-ne p1, v1, :cond_10

    :cond_f
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->registerListeners()V

    goto/16 :goto_0

    :cond_10
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->receiver:Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$4;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$4;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method protected onResume()V
    .locals 4

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicServiceConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v2}, Lcom/jrm/localmm/util/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/jrm/localmm/util/MusicUtils$ServiceToken;

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->stToken:Lcom/jrm/localmm/util/MusicUtils$ServiceToken;

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->changeSource()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/jrm/localmm/ui/music/MusicPlayerActivity$1;)V

    iput-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->receiver:Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->receiver:Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mstar.localmm.network.disconnect"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->netDisconnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$5;

    invoke-direct {v3, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$5;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected pauseMusic()V
    .locals 2

    sget-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->pause()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPlayStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->continuing()V

    goto :goto_0
.end method

.method protected preMusic()V
    .locals 3

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isNextMusic:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayCompletion()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v1, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setSongsContent(Ljava/util/List;I)V

    return-void
.end method

.method protected processPlayCompletion()V
    .locals 4

    const/4 v3, 0x0

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$1;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$1;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :pswitch_1
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gtz v1, :cond_0

    sput v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    goto :goto_0

    :pswitch_2
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isNextMusic:Z

    if-eqz v1, :cond_3

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_2

    sput v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    :cond_1
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isNextMusic:Z

    goto :goto_0

    :cond_2
    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_4

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    goto :goto_1

    :cond_4
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public queryCurrentMusicExtraInfo()V
    .locals 10

    const/4 v6, 0x0

    const-string v3, "_data=?"

    sget-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v8}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v0, "MusicPlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*****queryCurrentMusicExtraInfo*****"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "title"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "artist"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "album_id"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "year"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "track"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v8, v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->setId(J)V

    const-string v0, "title"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/jrm/localmm/business/data/BaseData;->setTitle(Ljava/lang/String;)V

    const-string v0, "artist"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/jrm/localmm/business/data/BaseData;->setArtist(Ljava/lang/String;)V

    const-string v0, "album_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v8, v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->setAlbum(J)V

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v7

    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected seekTo(I)V
    .locals 2
    .param p1    # I

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v1, :cond_0

    :try_start_0
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1, p1}, Lcom/jrm/localmm/ui/music/IMediaService;->playerToPosiztion(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
