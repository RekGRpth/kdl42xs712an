.class Lcom/jrm/localmm/ui/music/MediaService$3;
.super Ljava/lang/Object;
.source "MediaService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MediaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MediaService;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MediaService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MediaService$3;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;

    const-string v1, "MediaService"

    const-string v2, "MediaService onPrepared"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService$3;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    const/4 v2, 0x2

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/music/MediaService;->access$002(Lcom/jrm/localmm/ui/music/MediaService;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService$3;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$3;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService$3;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    const/4 v4, 0x1

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mCanSeekForward:Z
    invoke-static {v3, v4}, Lcom/jrm/localmm/ui/music/MediaService;->access$702(Lcom/jrm/localmm/ui/music/MediaService;Z)Z

    move-result v3

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mCanSeekBack:Z
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/music/MediaService;->access$602(Lcom/jrm/localmm/ui/music/MediaService;Z)Z

    move-result v2

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mCanPause:Z
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/music/MediaService;->access$502(Lcom/jrm/localmm/ui/music/MediaService;Z)Z

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService$3;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # getter for: Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MediaService;->access$400(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPrepared()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService$3;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MediaService;->start()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
