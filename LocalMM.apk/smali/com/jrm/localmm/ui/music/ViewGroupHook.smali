.class public Lcom/jrm/localmm/ui/music/ViewGroupHook;
.super Landroid/widget/FrameLayout;
.source "ViewGroupHook.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/ViewGroupHook;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/ViewGroupHook;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/ViewGroupHook;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/ViewGroupHook;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/ViewGroupHook;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/ViewGroupHook;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method
