.class Lcom/jrm/localmm/ui/music/MediaService$4;
.super Ljava/lang/Object;
.source "MediaService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MediaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MediaService;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MediaService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MediaService$4;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v3, 0x5

    const-string v1, "MediaService"

    const-string v2, "MediaService onCompletion"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService$4;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I
    invoke-static {v1, v3}, Lcom/jrm/localmm/ui/music/MediaService;->access$002(Lcom/jrm/localmm/ui/music/MediaService;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService$4;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I
    invoke-static {v1, v3}, Lcom/jrm/localmm/ui/music/MediaService;->access$102(Lcom/jrm/localmm/ui/music/MediaService;I)I

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService$4;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # getter for: Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MediaService;->access$400(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicCompleted()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
