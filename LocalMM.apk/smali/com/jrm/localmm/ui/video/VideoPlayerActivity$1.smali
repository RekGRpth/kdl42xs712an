.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;
.super Landroid/os/Handler;
.source "VideoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 19
    .param p1    # Landroid/os/Message;

    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v9

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideController()V
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v2

    if-eq v15, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I
    invoke-static {v2, v15}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    const-string v2, "VideoPlayActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDuration()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->total_time_video:Landroid/widget/TextView;

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    :cond_1
    const-string v2, "VideoPlayActivity"

    const-string v3, "******getCurrentPosition()  start*******"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v3

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$302(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    const-string v2, "VideoPlayActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "******getCurrentPosition()  end*******"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->current_time_video:Landroid/widget/TextView;

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getVideoTimeCurrentPositionTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xe

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v2, "index"

    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v2, "VideoPlayActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "******************"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    if-ne v9, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-object/from16 v17, v0

    new-instance v1, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/high16 v3, 0x7f070000    # com.jrm.localmm.R.style.dialog

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v18, 0x7f0600c7    # com.jrm.localmm.R.string.subtitle_setting_0_value_1

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-direct/range {v1 .. v7}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;Z)V

    move-object/from16 v0, v17

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogOne:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$602(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getPlaySettingSubtitleDialog(I)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->requestWindowFeature(I)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getPlaySettingSubtitleDialog(I)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->show()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideController()V
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-object/from16 v17, v0

    new-instance v1, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/high16 v3, 0x7f070000    # com.jrm.localmm.R.style.dialog

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v18, 0x7f0600c7    # com.jrm.localmm.R.string.subtitle_setting_0_value_1

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-direct/range {v1 .. v7}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;Z)V

    move-object/from16 v0, v17

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogTwo:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$802(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    goto/16 :goto_1

    :sswitch_3
    new-instance v1, Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/high16 v3, 0x7f070000    # com.jrm.localmm.R.style.dialog

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v6, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getPlaySettingSubtitleDialog(I)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v7, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getPlaySettingSubtitleDialog(I)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-result-object v7

    invoke-virtual {v7}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getAdapter()Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;)V

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->show()V

    goto/16 :goto_0

    :sswitch_4
    new-instance v11, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v11}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->ThreeDInit()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg1:I

    const/4 v5, 0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoTimeSelect(Z)V

    goto/16 :goto_0

    :sswitch_7
    sget v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    const/16 v3, 0x14

    if-ne v2, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResumeFromDualSwitch(Z)V

    :goto_2
    const/4 v2, 0x0

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1302(I)I

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    goto :goto_2

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindow()V

    goto/16 :goto_0

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->finish()V

    goto/16 :goto_0

    :sswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v16

    const/4 v2, 0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_6

    const/16 v16, 0x2

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v2

    add-int/lit8 v3, v16, -0x1

    aget-object v2, v2, v3

    iget v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionB:I

    if-lt v13, v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->pause()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v3

    add-int/lit8 v4, v16, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionA:I

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x17

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_6
    const/16 v16, 0x1

    goto/16 :goto_3

    :sswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVoiceLayoutVisibility(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :sswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPointDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPointDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPointDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_b
        0xd -> :sswitch_0
        0xe -> :sswitch_1
        0x10 -> :sswitch_4
        0x11 -> :sswitch_5
        0x12 -> :sswitch_6
        0x13 -> :sswitch_7
        0x15 -> :sswitch_8
        0x16 -> :sswitch_9
        0x17 -> :sswitch_a
        0x18 -> :sswitch_c
        0x72 -> :sswitch_3
        0x7c -> :sswitch_2
    .end sparse-switch
.end method
