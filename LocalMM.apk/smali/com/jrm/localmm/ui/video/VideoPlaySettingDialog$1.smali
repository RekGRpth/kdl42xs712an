.class Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;
.super Ljava/lang/Object;
.source "VideoPlaySettingDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvS3DManager;->getDisplayFormat()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    move-result-object v1

    iput-object v1, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v1

    const v2, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    :cond_0
    return-void
.end method
