.class Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;
.super Ljava/lang/Object;
.source "VideoPlayABDialog.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/jrm/localmm/util/Constants;->abFlag:Z

    :goto_0
    return-void

    :cond_0
    sput-boolean v1, Lcom/jrm/localmm/util/Constants;->aFlag:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aFlag:Z

    sput-boolean v1, Lcom/jrm/localmm/util/Constants;->abFlag:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
