.class Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;
.super Ljava/lang/Object;
.source "DlnaVideoPlayActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    const/4 v2, 0x1

    const/16 v3, 0x13

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mbNotSeek:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekPosition:I
    invoke-static {v0, p2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$602(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;I)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$900(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v0

    if-gt v0, p2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    add-int/lit16 v1, p2, -0x3e8

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekPosition:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$602(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;I)I

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localPause(Z)V

    # ++operator for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekTimes:I
    invoke-static {}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$704()I

    move-result v0

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0600c1    # com.jrm.localmm.R.string.choose_time_failed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method
