.class public Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
.super Ljava/lang/Object;
.source "VideoPlayerViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;
    }
.end annotation


# static fields
.field public static playSettingIcon:[I

.field public static playSettingName:[I

.field protected static state:I


# instance fields
.field private activityLayout:Landroid/widget/FrameLayout;

.field protected bt_dualFocusSwitch:Landroid/widget/ImageView;

.field protected bt_dualMode:Landroid/widget/ImageView;

.field protected bt_dualSwitch:Landroid/widget/ImageView;

.field protected bt_playA:Landroid/widget/ImageView;

.field protected bt_playAB:Landroid/widget/ImageView;

.field protected bt_playB:Landroid/widget/ImageView;

.field protected bt_playVoice:Landroid/widget/ImageView;

.field protected bt_videoInfo:Landroid/widget/ImageView;

.field protected bt_videoList:Landroid/widget/ImageView;

.field protected bt_videoNext:Landroid/widget/ImageView;

.field protected bt_videoPlay:Landroid/widget/ImageView;

.field protected bt_videoPre:Landroid/widget/ImageView;

.field protected bt_videoRewind:Landroid/widget/ImageView;

.field protected bt_videoSetting:Landroid/widget/ImageView;

.field protected bt_videoTime:Landroid/widget/ImageView;

.field protected bt_videoWind:Landroid/widget/ImageView;

.field public currentDualMode:I

.field private currentDualModeFocus:I

.field private currentPIPPosition:I

.field protected current_time_video:Landroid/widget/TextView;

.field protected dual_fullScreen:Landroid/widget/ImageView;

.field protected dual_leftBottom:Landroid/widget/ImageView;

.field protected dual_leftRight:Landroid/widget/ImageView;

.field protected dual_leftTop:Landroid/widget/ImageView;

.field protected dual_rightBottom:Landroid/widget/ImageView;

.field protected dual_rightTop:Landroid/widget/ImageView;

.field private dualmodeLayout:Landroid/widget/LinearLayout;

.field private firstLayout:Landroid/widget/LinearLayout;

.field private isDualLayoutLoaded:Z

.field public isDualLayoutShow:Z

.field private isDualVideo:Z

.field private isPIPMode:Z

.field private isVoiceLayoutLoaded:Z

.field public isVoiceLayoutShow:Z

.field private mHandler:Landroid/os/Handler;

.field protected mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

.field protected mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

.field protected mbOneSeek:Z

.field protected mbTwoSeek:Z

.field private params:Landroid/widget/FrameLayout$LayoutParams;

.field protected playControlLayout:Landroid/widget/LinearLayout;

.field private screenHeight:I

.field private screenWidth:I

.field private secondLayout:Landroid/widget/LinearLayout;

.field protected total_time_video:Landroid/widget/TextView;

.field private videoFocus:Lcom/jrm/localmm/ui/video/BorderTextViews;

.field private videoFocusLayout:Landroid/widget/LinearLayout;

.field private videoPlayActivity:Landroid/app/Activity;

.field private videoPlayerImageSurfaceViewOne:Landroid/view/SurfaceView;

.field private videoPlayerImageSurfaceViewTwo:Landroid/view/SurfaceView;

.field protected videoPlayerTextViewOne:Lcom/jrm/localmm/ui/video/BorderTextViews;

.field protected videoPlayerTextViewTwo:Lcom/jrm/localmm/ui/video/BorderTextViews;

.field protected videoSeekBar:Landroid/widget/SeekBar;

.field private videoWindowType:Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

.field protected video_list:Landroid/widget/TextView;

.field protected video_name:Landroid/widget/TextView;

.field protected video_playSpeed:Landroid/widget/TextView;

.field private viewId:I

.field protected voiceBar:Lcom/jrm/localmm/util/VerticalSeekBar;

.field private voiceLayout:Landroid/widget/LinearLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playSettingIcon:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playSettingName:[I

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    return-void

    :array_0
    .array-data 4
        0x7f02009b    # com.jrm.localmm.R.drawable.video_play_mode_icon
        0x7f020085    # com.jrm.localmm.R.drawable.screen_size_icon
        0x7f020098    # com.jrm.localmm.R.drawable.three_d_setting_icon
        0x7f020096    # com.jrm.localmm.R.drawable.subtitle_icon
        0x7f02009a    # com.jrm.localmm.R.drawable.track_icon
        0x7f020007    # com.jrm.localmm.R.drawable.channel_icon
    .end array-data

    :array_1
    .array-data 4
        0x7f060096    # com.jrm.localmm.R.string.video_three_d_setting
        0x7f060064    # com.jrm.localmm.R.string.video_subtitle
        0x7f0600a7    # com.jrm.localmm.R.string.video_breakpoint
        0x7f060065    # com.jrm.localmm.R.string.video_track
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x5

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->activityLayout:Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocusLayout:Landroid/widget/LinearLayout;

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isPIPMode:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutLoaded:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isVoiceLayoutLoaded:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isVoiceLayoutShow:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mbOneSeek:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mbTwoSeek:Z

    const/4 v0, 0x4

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentPIPPosition:I

    iput v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    iput v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoWindowType:Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    iput p2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iput p3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private loadVoiceLayout()V
    .locals 4

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isVoiceLayoutLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x1e

    const/16 v2, 0xbe

    const/16 v3, 0x55

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    const-string v0, "***************8"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "************** "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getBottom()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->activityLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVoiceLayoutVisibility(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceBar:Lcom/jrm/localmm/util/VerticalSeekBar;

    invoke-virtual {v0, p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->setHolder(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isVoiceLayoutLoaded:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public addVoice(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/business/video/VideoPlayView;->addVoice(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->updateVoiceBar()V

    return-void
.end method

.method public changeDualMode()V
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->showDualModeLayout()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isPIPMode:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewTwo:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-static {}, Lcom/jrm/localmm/business/video/SubtitleTool;->getCurrentSubSize()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextSize(F)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualModeUnSelect()V

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f02004d    # com.jrm.localmm.R.drawable.player_icon_dual_fullscreen

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_fullScreen:Landroid/widget/ImageView;

    const v1, 0x7f02004e    # com.jrm.localmm.R.drawable.player_icon_dual_fullscreen_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f02004f    # com.jrm.localmm.R.drawable.player_icon_dual_left_right

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftRight:Landroid/widget/ImageView;

    const v1, 0x7f020051    # com.jrm.localmm.R.drawable.player_icon_dual_left_right_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020056    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_bottom

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightBottom:Landroid/widget/ImageView;

    const v1, 0x7f020057    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020058    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightTop:Landroid/widget/ImageView;

    const v1, 0x7f020059    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020052    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_bottom

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftBottom:Landroid/widget/ImageView;

    const v1, 0x7f020053    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020054    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftTop:Landroid/widget/ImageView;

    const v1, 0x7f020055    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method findViews()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b0    # com.jrm.localmm.R.id.videoplayactivity

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->activityLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b1    # com.jrm.localmm.R.id.firstvideolayout

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b2    # com.jrm.localmm.R.id.secondvideolayout

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b5    # com.jrm.localmm.R.id.video_suspension_layout

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b9    # com.jrm.localmm.R.id.video_previous

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800ba    # com.jrm.localmm.R.id.video_rewind

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoRewind:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800bb    # com.jrm.localmm.R.id.video_play

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPlay:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800bc    # com.jrm.localmm.R.id.video_wind

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoWind:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800bd    # com.jrm.localmm.R.id.video_next

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800be    # com.jrm.localmm.R.id.video_time

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoTime:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800bf    # com.jrm.localmm.R.id.video_list

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoList:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c1    # com.jrm.localmm.R.id.video_setting

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoSetting:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c0    # com.jrm.localmm.R.id.video_info

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoInfo:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c2    # com.jrm.localmm.R.id.play_icon_ab

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playAB:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c3    # com.jrm.localmm.R.id.play_icon_voice

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playVoice:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c4    # com.jrm.localmm.R.id.play_icon_dual_switch

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualSwitch:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c5    # com.jrm.localmm.R.id.play_icon_dual_focus_switch

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c6    # com.jrm.localmm.R.id.play_icon_dual_mode_switch

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c9    # com.jrm.localmm.R.id.seek_a

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800ca    # com.jrm.localmm.R.id.seek_b

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c7    # com.jrm.localmm.R.id.control_timer_current

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->current_time_video:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800cb    # com.jrm.localmm.R.id.control_timer_total

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->total_time_video:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800c8    # com.jrm.localmm.R.id.progress

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b6    # com.jrm.localmm.R.id.video_name_display

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->video_name:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b7    # com.jrm.localmm.R.id.video_list_display

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->video_list:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b8    # com.jrm.localmm.R.id.video_play_speed_display

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->video_playSpeed:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08002d    # com.jrm.localmm.R.id.videoviewone

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/business/video/VideoPlayView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08002e    # com.jrm.localmm.R.id.videoPlayerImageSurfaceViewOne

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewOne:Landroid/view/SurfaceView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewOne:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/SurfaceHolder;->setFormat(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewOne:Landroid/view/SurfaceView;

    invoke-virtual {v2, v4}, Landroid/view/SurfaceView;->setBackgroundColor(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewOne:Landroid/view/SurfaceView;

    invoke-virtual {v2, v4}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08002f    # com.jrm.localmm.R.id.video_player_text_one

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/ui/video/BorderTextViews;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewOne:Lcom/jrm/localmm/ui/video/BorderTextViews;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08008d    # com.jrm.localmm.R.id.videoviewtwo

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/business/video/VideoPlayView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08008e    # com.jrm.localmm.R.id.videoPlayerImageSurfaceViewTwo

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewTwo:Landroid/view/SurfaceView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewTwo:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/SurfaceHolder;->setFormat(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewTwo:Landroid/view/SurfaceView;

    invoke-virtual {v2, v4}, Landroid/view/SurfaceView;->setBackgroundColor(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewTwo:Landroid/view/SurfaceView;

    invoke-virtual {v2, v4}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08008f    # com.jrm.localmm.R.id.video_player_text_two

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/ui/video/BorderTextViews;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewTwo:Lcom/jrm/localmm/ui/video/BorderTextViews;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f0800b3    # com.jrm.localmm.R.id.video_focus_layout

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocusLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocusLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f0800b4    # com.jrm.localmm.R.id.video_focus

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/ui/video/BorderTextViews;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocus:Lcom/jrm/localmm/ui/video/BorderTextViews;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030003    # com.jrm.localmm.R.layout.dual_mode

    invoke-virtual {v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f080025    # com.jrm.localmm.R.id.dualmodelayout

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08002b    # com.jrm.localmm.R.id.dual_mode_left_right

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftRight:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f08002a    # com.jrm.localmm.R.id.dual_mode_fullscreen

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_fullScreen:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f080028    # com.jrm.localmm.R.id.dual_mode_left_top

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftTop:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f080029    # com.jrm.localmm.R.id.dual_mode_left_bottom

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftBottom:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f080026    # com.jrm.localmm.R.id.dual_mode_right_top

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightTop:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f080027    # com.jrm.localmm.R.id.dual_mode_right_bottom

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightBottom:Landroid/widget/ImageView;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualModeOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f030026    # com.jrm.localmm.R.layout.voicebar

    invoke-virtual {v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0800da    # com.jrm.localmm.R.id.voicebarlayout

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f0800db    # com.jrm.localmm.R.id.voicebar

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/util/VerticalSeekBar;

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceBar:Lcom/jrm/localmm/util/VerticalSeekBar;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->openOrCloseDualDecode(Z)V

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->showVideoFocus(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualButtonNotChoose()V

    goto/16 :goto_0
.end method

.method public getDualVideoMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    return v0
.end method

.method public getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    goto :goto_0
.end method

.method public getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    goto :goto_0
.end method

.method public getSubtitleTextView()Lcom/jrm/localmm/ui/video/BorderTextViews;
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewOne:Lcom/jrm/localmm/ui/video/BorderTextViews;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewTwo:Lcom/jrm/localmm/ui/video/BorderTextViews;

    goto :goto_0
.end method

.method public getSurfaceView(I)Landroid/view/SurfaceView;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewOne:Landroid/view/SurfaceView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerImageSurfaceViewTwo:Landroid/view/SurfaceView;

    goto :goto_0
.end method

.method public getViewId()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    return v0
.end method

.method public isPIPMode(I)Z
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isPIPMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSeekable(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mbOneSeek:Z

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mbTwoSeek:Z

    goto :goto_0
.end method

.method public openOrCloseDualDecode(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x5

    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    iput v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->showVideoFocus(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocusLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindow()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualButtonOptional()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->showVideoFocus(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->pause()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_2
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x55

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindow()V

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    if-eq v0, v3, :cond_3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->switchFocusedView()V

    :cond_3
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualButtonNotChoose()V

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v3, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindowVisable(ZZ)V

    goto :goto_0
.end method

.method public processDownKey(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualModeUnSelect()V

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v2

    :pswitch_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftRight:Landroid/widget/ImageView;

    const v1, 0x7f020051    # com.jrm.localmm.R.drawable.player_icon_dual_left_right_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightTop:Landroid/widget/ImageView;

    const v1, 0x7f020059    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x4

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightBottom:Landroid/widget/ImageView;

    const v1, 0x7f020057    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    iput v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftTop:Landroid/widget/ImageView;

    const v1, 0x7f020055    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x2

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftBottom:Landroid/widget/ImageView;

    const v1, 0x7f020053    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x6

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_fullScreen:Landroid/widget/ImageView;

    const v1, 0x7f02004e    # com.jrm.localmm.R.drawable.player_icon_dual_fullscreen_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public processLeftKey(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v4, 0x14

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPreSelect(Z)V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x16

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualModeSelect(Z)V

    goto :goto_0

    :cond_0
    sput v4, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    goto :goto_0

    :pswitch_2
    sput v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoRewindSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPreSelect(Z)V

    goto :goto_0

    :pswitch_3
    sput v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoRewindSelect(Z)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindSelect(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x3

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoNextSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindSelect(Z)V

    goto :goto_0

    :pswitch_6
    sput v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoTimeSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoNextSelect(Z)V

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x7

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoTimeSelect(Z)V

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x8

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoInforSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListSelect(Z)V

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x9

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoSettingSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoInforSelect(Z)V

    goto :goto_0

    :pswitch_a
    const/16 v0, 0x10

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoSettingSelect(Z)V

    goto :goto_0

    :pswitch_b
    const/16 v0, 0x11

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayVoiceSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    goto/16 :goto_0

    :pswitch_c
    const/16 v0, 0x12

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayVoiceSelect(Z)V

    goto/16 :goto_0

    :pswitch_d
    sput v4, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualFocusSwitchSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    const/16 v0, 0x15

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualModeSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualFocusSwitchSelect(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public processOkKey(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualMode(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public processRightKey(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v2

    :pswitch_1
    sput v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPreSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoRewindSelect(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoRewindSelect(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindSelect(Z)V

    goto :goto_0

    :pswitch_4
    sput v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoNextSelect(Z)V

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x7

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoNextSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoTimeSelect(Z)V

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x8

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoTimeSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListSelect(Z)V

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x9

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoInforSelect(Z)V

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x10

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoInforSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoSettingSelect(Z)V

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x11

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoSettingSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    goto :goto_0

    :pswitch_a
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    const/16 v0, 0x12

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayVoiceSelect(Z)V

    goto :goto_0

    :pswitch_b
    const/16 v0, 0x14

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayVoiceSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    goto/16 :goto_0

    :pswitch_c
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualFocusSwitchSelect(Z)V

    goto/16 :goto_0

    :cond_0
    sput v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPreSelect(Z)V

    goto/16 :goto_0

    :pswitch_d
    const/16 v0, 0x16

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualFocusSwitchSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualModeSelect(Z)V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    sput v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualModeSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPreSelect(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public processUpKey(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualModeUnSelect()V

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v2

    :pswitch_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftBottom:Landroid/widget/ImageView;

    const v1, 0x7f020053    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x6

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_fullScreen:Landroid/widget/ImageView;

    const v1, 0x7f02004e    # com.jrm.localmm.R.drawable.player_icon_dual_fullscreen_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x5

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftRight:Landroid/widget/ImageView;

    const v1, 0x7f020051    # com.jrm.localmm.R.drawable.player_icon_dual_left_right_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightTop:Landroid/widget/ImageView;

    const v1, 0x7f020059    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightBottom:Landroid/widget/ImageView;

    const v1, 0x7f020057    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_5
    iput v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftTop:Landroid/widget/ImageView;

    const v1, 0x7f020055    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public refreshControlInfo(Ljava/lang/String;IIII)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->reset()V

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoName(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    invoke-virtual {p0, p3, p4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListText(II)V

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->total_time_video:Landroid/widget/TextView;

    int-to-long v1, p5

    invoke-static {v1, v2}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p5}, Landroid/widget/SeekBar;->setMax(I)V

    :cond_0
    return-void
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->current_time_video:Landroid/widget/TextView;

    const-string v1, "--:--:--"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->total_time_video:Landroid/widget/TextView;

    const-string v1, "--:--:--"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    return-void
.end method

.method public setActivity(Landroid/app/Activity;Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->findViews()V

    return-void
.end method

.method public setAllUnSelect(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPreSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoRewindSelect(Z)V

    invoke-virtual {p0, v2, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoNextSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoTimeSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoInforSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoSettingSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayVoiceSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualFocusSwitchSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualModeSelect(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    :cond_0
    return-void
.end method

.method public setDualButtonNotChoose()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    const v1, 0x7f02004b    # com.jrm.localmm.R.drawable.player_icon_dual_focus_switch_cannot_choose

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020050    # com.jrm.localmm.R.drawable.player_icon_dual_left_right_cannot_choose

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method public setDualButtonOptional()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    const v1, 0x7f02004a    # com.jrm.localmm.R.drawable.player_icon_dual_focus_switch

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f02004f    # com.jrm.localmm.R.drawable.player_icon_dual_left_right

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method public setDualMode(I)V
    .locals 8
    .param p1    # I

    const/16 v7, 0x55

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x1

    iput p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iput-boolean v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualModeSelect(Z)V

    invoke-virtual {p0, v4, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindowVisable(ZZ)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocusLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindow()V

    return-void

    :pswitch_0
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_2
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v6, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindowVisable(ZZ)V

    goto :goto_2

    :pswitch_1
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x4

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    div-int/lit8 v2, v2, 0x4

    invoke-direct {v0, v1, v2, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :pswitch_3
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x4

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    div-int/lit8 v2, v2, 0x4

    const/16 v3, 0x35

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x4

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    div-int/lit8 v2, v2, 0x4

    const/16 v3, 0x53

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :pswitch_5
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    div-int/lit8 v1, v1, 0x4

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    div-int/lit8 v2, v2, 0x4

    const/16 v3, 0x33

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setDualModeOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_fullScreen:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftBottom:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftRight:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftTop:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightBottom:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightTop:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setDualModeUnSelect()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftBottom:Landroid/widget/ImageView;

    const v1, 0x7f020052    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_bottom

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_fullScreen:Landroid/widget/ImageView;

    const v1, 0x7f02004d    # com.jrm.localmm.R.drawable.player_icon_dual_fullscreen

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftRight:Landroid/widget/ImageView;

    const v1, 0x7f02004f    # com.jrm.localmm.R.drawable.player_icon_dual_left_right

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightTop:Landroid/widget/ImageView;

    const v1, 0x7f020058    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_rightBottom:Landroid/widget/ImageView;

    const v1, 0x7f020056    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_bottom

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dual_leftTop:Landroid/widget/ImageView;

    const v1, 0x7f020054    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoRewind:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoWind:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoTime:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoList:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoInfo:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoSetting:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playAB:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playVoice:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualSwitch:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setPlaySpeed(I)V
    .locals 5
    .param p1    # I

    const/16 v1, 0x40

    if-ge p1, v1, :cond_0

    const/16 v1, -0x40

    if-le p1, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*%d "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f060092    # com.jrm.localmm.R.string.x_times_speed

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "str....."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->video_playSpeed:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setSeekVar(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iput-boolean p2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mbOneSeek:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean p2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mbTwoSeek:Z

    goto :goto_0
.end method

.method public setSubTitleText(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewOne:Lcom/jrm/localmm/ui/video/BorderTextViews;

    if-eqz v0, :cond_0

    const-string v0, "videoPlayerTextViewOne"

    const-string v1, "**********videoPlayerTextViewOne*********"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewOne:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewTwo:Lcom/jrm/localmm/ui/video/BorderTextViews;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayerTextViewTwo:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setVideoDualFocusSwitchSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    const v1, 0x7f02004c    # com.jrm.localmm.R.drawable.player_icon_dual_focus_switch_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    const v1, 0x7f02004a    # com.jrm.localmm.R.drawable.player_icon_dual_focus_switch

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualFocusSwitch:Landroid/widget/ImageView;

    const v1, 0x7f02004b    # com.jrm.localmm.R.drawable.player_icon_dual_focus_switch_cannot_choose

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoDualModeSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f02004e    # com.jrm.localmm.R.drawable.player_icon_dual_fullscreen_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020051    # com.jrm.localmm.R.drawable.player_icon_dual_left_right_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020057    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020059    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020053    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_bottom_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020055    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_top_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020054    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f02004d    # com.jrm.localmm.R.drawable.player_icon_dual_fullscreen

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f02004f    # com.jrm.localmm.R.drawable.player_icon_dual_left_right

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020056    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_bottom

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020058    # com.jrm.localmm.R.drawable.player_icon_dual_pip_right_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020052    # com.jrm.localmm.R.drawable.player_icon_dual_pip_left_bottom

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    const v1, 0x7f020050    # com.jrm.localmm.R.drawable.player_icon_dual_left_right_cannot_choose

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public setVideoDualSwitchSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualSwitch:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualSwitch:Landroid/widget/ImageView;

    const v1, 0x7f02005b    # com.jrm.localmm.R.drawable.player_icon_dual_switch_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualSwitch:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualSwitch:Landroid/widget/ImageView;

    const v1, 0x7f02005a    # com.jrm.localmm.R.drawable.player_icon_dual_switch

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoInforSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoInfo:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoInfo:Landroid/widget/ImageView;

    const v1, 0x7f02005d    # com.jrm.localmm.R.drawable.player_icon_infor_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoInfo:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoInfo:Landroid/widget/ImageView;

    const v1, 0x7f02005c    # com.jrm.localmm.R.drawable.player_icon_infor

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoListSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoList:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoList:Landroid/widget/ImageView;

    const v1, 0x7f020061    # com.jrm.localmm.R.drawable.player_icon_list_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoList:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoList:Landroid/widget/ImageView;

    const v1, 0x7f020060    # com.jrm.localmm.R.drawable.player_icon_list

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoListText(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->video_list:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVideoName(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->video_name:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoPlayActivity:Landroid/app/Activity;

    const v3, 0x7f060063    # com.jrm.localmm.R.string.playing

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVideoNextSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    const v1, 0x7f020067    # com.jrm.localmm.R.drawable.player_icon_next_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    const v1, 0x7f020066    # com.jrm.localmm.R.drawable.player_icon_next

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoPlayABSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playAB:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playAB:Landroid/widget/ImageView;

    const v1, 0x7f020047    # com.jrm.localmm.R.drawable.player_icon_ab_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playAB:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playAB:Landroid/widget/ImageView;

    const v1, 0x7f020046    # com.jrm.localmm.R.drawable.player_icon_ab

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoPlaySelect(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    if-eqz p1, :cond_1

    invoke-virtual {p0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006b    # com.jrm.localmm.R.drawable.player_icon_play

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoPlayVoiceSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playVoice:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playVoice:Landroid/widget/ImageView;

    const v1, 0x7f02007f    # com.jrm.localmm.R.drawable.player_icon_voice_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playVoice:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playVoice:Landroid/widget/ImageView;

    const v1, 0x7f02007e    # com.jrm.localmm.R.drawable.player_icon_voice

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoPreSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    const v1, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    const v1, 0x7f02006d    # com.jrm.localmm.R.drawable.player_icon_previous

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoRewindSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const v1, 0x7f020073    # com.jrm.localmm.R.drawable.player_icon_rewind_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const v1, 0x7f020072    # com.jrm.localmm.R.drawable.player_icon_rewind

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoSettingSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoSetting:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoSetting:Landroid/widget/ImageView;

    const v1, 0x7f020079    # com.jrm.localmm.R.drawable.player_icon_setting_foucs

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoSetting:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoSetting:Landroid/widget/ImageView;

    const v1, 0x7f020078    # com.jrm.localmm.R.drawable.player_icon_setting

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoTimeSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoTime:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoTime:Landroid/widget/ImageView;

    const v1, 0x7f02007d    # com.jrm.localmm.R.drawable.player_icon_time_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoTime:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoTime:Landroid/widget/ImageView;

    const v1, 0x7f02007c    # com.jrm.localmm.R.drawable.player_icon_time

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoWindSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoWind:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoWind:Landroid/widget/ImageView;

    const v1, 0x7f020081    # com.jrm.localmm.R.drawable.player_icon_wind_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoWind:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoWind:Landroid/widget/ImageView;

    const v1, 0x7f020080    # com.jrm.localmm.R.drawable.player_icon_wind

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setVideoWindow()V
    .locals 3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    const-string v0, "************"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********params********"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoWindowType:Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoWindowType:Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoWindowType:Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoWindowType:Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    return-void
.end method

.method public setVideoWindowVisable(ZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    :goto_0
    if-eqz p2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setWindowVisible()V

    :goto_1
    return-void

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_SUB_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setWindowInvisible()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public setVoice(I)V
    .locals 3

    const-string v0, "***************"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******voice********"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->updateVoiceBar()V

    return-void
.end method

.method public setVoiceLayoutVisibility(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public showDualModeLayout()V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutLoaded:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x41

    const/16 v2, 0x12c

    const/16 v3, 0x55

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    const-string v0, "***************8"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "************** "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_dualMode:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getBottom()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->activityLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutLoaded:Z

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->dualmodeLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    return-void
.end method

.method public showVideoFocus(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocus:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setPaintColor(I)V

    const-string v0, "VideoPlayerViewHolder"

    const-string v1, "*******showVideoFocus***********true"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocus:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->invalidate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocus:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setPaintColor(I)V

    const-string v0, "VideoPlayerViewHolder"

    const-string v1, "*******showVideoFocus***********false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public switchFocusedView()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    if-ne v0, v4, :cond_1

    iput v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    :goto_0
    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v4, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindowVisable(ZZ)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->screenHeight:I

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x55

    invoke-direct {v0, v5, v5, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoFocusLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1
    iput v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->viewId:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->firstLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->secondLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->params:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v4, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindowVisable(ZZ)V

    goto :goto_1
.end method

.method public updateVoiceBar()V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->loadVoiceLayout()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVoiceLayoutVisibility(Z)V

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isVoiceLayoutShow:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getVoice()F

    move-result v0

    float-to-int v0, v0

    const-string v1, "************"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "******voice*******"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f020095    # com.jrm.localmm.R.drawable.sound_zore

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f02008f    # com.jrm.localmm.R.drawable.sound_one

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f020094    # com.jrm.localmm.R.drawable.sound_two

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f020093    # com.jrm.localmm.R.drawable.sound_three

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f02008d    # com.jrm.localmm.R.drawable.sound_four

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f02008c    # com.jrm.localmm.R.drawable.sound_five

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f020091    # com.jrm.localmm.R.drawable.sound_six

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f020090    # com.jrm.localmm.R.drawable.sound_seven

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f02008b    # com.jrm.localmm.R.drawable.sound_eight

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f02008e    # com.jrm.localmm.R.drawable.sound_nine

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->voiceLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f020092    # com.jrm.localmm.R.drawable.sound_ten

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
