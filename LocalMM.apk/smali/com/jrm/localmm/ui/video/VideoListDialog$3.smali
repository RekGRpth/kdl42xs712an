.class Lcom/jrm/localmm/ui/video/VideoListDialog$3;
.super Ljava/lang/Object;
.source "VideoListDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoListDialog;->addListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoListDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoListDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoListDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoListDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    if-ltz p3, :cond_0

    const-string v2, "JHQ"

    const-string v3, "Video mouse click effective\uff1a\uff1a\uff1a\uff1a\uff1a"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoListDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoListDialog;

    # setter for: Lcom/jrm/localmm/ui/video/VideoListDialog;->selected:I
    invoke-static {v2, p3}, Lcom/jrm/localmm/ui/video/VideoListDialog;->access$002(Lcom/jrm/localmm/ui/video/VideoListDialog;I)I

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "index"

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoListDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoListDialog;->selected:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoListDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoListDialog;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoListDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoListDialog;->onClickHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoListDialog;->access$200(Lcom/jrm/localmm/ui/video/VideoListDialog;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoListDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoListDialog;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoListDialog;->dismiss()V

    :cond_0
    return-void
.end method
