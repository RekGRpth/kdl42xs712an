.class Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;
.super Ljava/lang/Object;
.source "VideoPlayerViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DualModeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setDualMode(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x6

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$002(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;I)I

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x2

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$002(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;I)I

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x5

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$002(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;I)I

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x1

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$002(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;I)I

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x4

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$002(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;I)I

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder$DualModeListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x3

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualModeFocus:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->access$002(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;I)I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f080026
        :pswitch_5    # com.jrm.localmm.R.id.dual_mode_right_top
        :pswitch_4    # com.jrm.localmm.R.id.dual_mode_right_bottom
        :pswitch_3    # com.jrm.localmm.R.id.dual_mode_left_top
        :pswitch_1    # com.jrm.localmm.R.id.dual_mode_left_bottom
        :pswitch_0    # com.jrm.localmm.R.id.dual_mode_fullscreen
        :pswitch_2    # com.jrm.localmm.R.id.dual_mode_left_right
    .end packed-switch
.end method
