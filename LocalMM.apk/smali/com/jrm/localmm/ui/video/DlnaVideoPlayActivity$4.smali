.class Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;
.super Ljava/lang/Object;
.source "DlnaVideoPlayActivity.java"

# interfaces
.implements Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    return-void
.end method

.method public onCloseMusic()V
    .locals 0

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;I)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    const-string v0, "DlnaVideoPlayActivity"

    const-string v1, "onCompletion()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->finish()V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;III)Z
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v1, ""

    sparse-switch p2, :sswitch_data_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showErrorDialog(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1300(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V

    const/4 v2, 0x1

    :goto_1
    return v2

    :sswitch_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayback()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060076    # com.jrm.localmm.R.string.video_media_error_server_died

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->processErrorUnknown(Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;
    invoke-static {v2, p1, p2, p3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1200(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;

    move-result-object v0

    iget-object v1, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;->strMessage:Ljava/lang/String;

    iget-boolean v2, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;->showStateWithError:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;->strMessage:Ljava/lang/String;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060075    # com.jrm.localmm.R.string.video_media_error_not_valid

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_2
    .end sparse-switch
.end method

.method public onInfo(Landroid/media/MediaPlayer;III)Z
    .locals 6
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    sparse-switch p2, :sswitch_data_0

    const-string v2, "DlnaVideoPlayActivity"

    const-string v4, "Play onInfo::: default onInfo!"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    move v2, v3

    :cond_1
    :goto_1
    return v2

    :sswitch_0
    if-ne p3, v2, :cond_2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getSubtitleData()Ljava/lang/String;

    move-result-object v0

    const-string v3, "*************"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "*******setSubTitleText******"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v2, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setSubTitleText(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    if-nez p3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setSubTitleText(Ljava/lang/String;)V

    goto :goto_1

    :sswitch_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v3

    iput-boolean v2, v3, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mbNotSeek:Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060077    # com.jrm.localmm.R.string.video_media_infor_no_index

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setPlaySpeed(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v3, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isAudioSupport:Z
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1502(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Z)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060070    # com.jrm.localmm.R.string.video_media_error_audio_unsupport

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isVideoSupport:Z
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1602(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Z)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060071    # com.jrm.localmm.R.string.video_media_error_video_unsupport

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x321 -> :sswitch_1
        0x3e8 -> :sswitch_0
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_3
    .end sparse-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;I)V
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->dismissProgressDialog()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v2

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$902(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;I)I

    const-string v1, "DlnaVideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDuration()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$900(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$900(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->total_time_video:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$900(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPrepared:Z
    invoke-static {v1, v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1402(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Z)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v4, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;I)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setPlaySpeed(I)V

    return-void
.end method

.method public onUpdateSubtitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method
