.class public Lcom/jrm/localmm/ui/video/SubtitleListDialog;
.super Landroid/app/Dialog;
.source "SubtitleListDialog.java"


# instance fields
.field private adapter:Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

.field private context:Landroid/content/Context;

.field private getInfoNum:I

.field private listSubtitle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

.field private onkeyListenter:Landroid/view/View$OnKeyListener;

.field private path:Ljava/lang/String;

.field private playSettingSubtitleDialog:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

.field private selected:I

.field private subtitleSettingList:Landroid/widget/ListView;

.field private subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

.field private subtitleTool:Lcom/jrm/localmm/business/video/SubtitleTool;

.field private videoPath:Ljava/lang/String;

.field private viewId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->path:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    new-instance v0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog$3;-><init>(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;-><init>(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->context:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p5    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p6    # Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->path:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    new-instance v0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog$3;-><init>(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;-><init>(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->context:Landroid/content/Context;

    iput-object p3, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->videoPath:Ljava/lang/String;

    iput-object p5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->playSettingSubtitleDialog:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-object v0, p1

    check-cast v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getSubtitleTextView()Lcom/jrm/localmm/ui/video/BorderTextViews;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    move-object v0, p1

    check-cast v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    check-cast p1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/video/SubtitleListDialog;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->refreshSubtitleDialog()V

    return-void
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->playSettingSubtitleDialog:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->path:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Lcom/jrm/localmm/ui/video/BorderTextViews;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Lcom/jrm/localmm/business/video/VideoPlayView;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    return v0
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I

    return v0
.end method

.method static synthetic access$702(Lcom/jrm/localmm/ui/video/SubtitleListDialog;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I

    return p1
.end method

.method static synthetic access$708(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I
    .locals 2
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I

    return v0
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    return-object v0
.end method

.method private addListener()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/SubtitleListDialog$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog$1;-><init>(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/SubtitleListDialog$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog$2;-><init>(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method private findView()V
    .locals 2

    const/4 v1, 0x1

    const v0, 0x7f080095    # com.jrm.localmm.R.id.subtitle_list

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getVideoPathName()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    return-void
.end method

.method private getVideoPathName()V
    .locals 4

    new-instance v2, Lcom/jrm/localmm/business/video/SubtitleTool;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->videoPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/jrm/localmm/business/video/SubtitleTool;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleTool:Lcom/jrm/localmm/business/video/SubtitleTool;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleTool:Lcom/jrm/localmm/business/video/SubtitleTool;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/SubtitleTool;->getSubtitlePathList(I)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->context:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->adapter:Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->adapter:Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private refreshSubtitleDialog()V
    .locals 14

    const-wide/16 v12, 0x3e8

    const/4 v11, 0x3

    const/4 v10, 0x0

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0600af    # com.jrm.localmm.R.string.subtitle_3_value_1

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    iget v8, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    if-le v7, v8, :cond_1

    iget v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    if-ltz v7, :cond_1

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v7}, Lcom/jrm/localmm/business/video/VideoPlayView;->offSubtitleTrack()V

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v7}, Lcom/jrm/localmm/business/video/VideoPlayView;->onSubtitleTrack()V

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    iget v8, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->path:Ljava/lang/String;

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v8, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->path:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleDataSource(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->path:Ljava/lang/String;

    const-string v8, "idx"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const-string v8, ""

    invoke-virtual {v7, v8}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v7}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v6

    const-string v7, "*************"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "***info**"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllInternalSubtitleCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-array v5, v6, [I

    invoke-virtual {v0, v5}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleType([I)V

    new-array v1, v6, [I

    invoke-virtual {v0, v1}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleCodeType([I)V

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v0, v3, v10}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleLanguageType([Ljava/lang/String;Z)V

    if-eqz v6, :cond_2

    const-string v7, "*************"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "*****"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v3, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v7, v3, v10

    iget v8, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    invoke-static {v11, v7, v8}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    invoke-static {v3, v7}, Lcom/jrm/localmm/util/Tools;->setSubtitleLanguageType([Ljava/lang/String;I)V

    :cond_0
    :goto_0
    const/4 v8, 0x1

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;

    iget v9, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget v9, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    invoke-static {v8, v7, v9}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->dismiss()V

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->playSettingSubtitleDialog:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-virtual {v7}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->show()V

    :goto_1
    return-void

    :cond_2
    iput v10, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->dismiss()V

    goto :goto_1

    :cond_3
    const/4 v7, 0x0

    iget v8, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    invoke-static {v7, v8}, Lcom/jrm/localmm/util/Tools;->setSubtitleLanguageType([Ljava/lang/String;I)V

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v8, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I

    invoke-virtual {v7, v8}, Lcom/jrm/localmm/business/video/VideoPlayView;->getSubtitleTrackInfo(I)Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v4

    const-string v7, "********subtitleTrackInfo*******"

    const-string v8, "***********"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_4

    invoke-virtual {v4, v10}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleLanguageType(Z)Ljava/lang/String;

    move-result-object v2

    const-string v7, "********subtitleTrackInfo*******"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "***********"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_4

    iput v10, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->dismiss()V

    goto :goto_1

    :cond_4
    iget v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I

    invoke-static {v11, v2, v7}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->refreshSubtitleDialog()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030018    # com.jrm.localmm.R.layout.subtitle_video_play_list

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v5, 0x106000d    # android.R.color.transparent

    invoke-virtual {v2, v5}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->findView()V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleSettingList:Landroid/widget/ListView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setClickable(Z)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->addListener()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const-string v0, "JHQ"

    const-string v1, "The mouse to click the::::::::;"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->refreshSubtitleDialog()V

    invoke-super {p0, p1}, Landroid/app/Dialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
