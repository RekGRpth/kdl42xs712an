.class public Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
.super Landroid/app/Activity;
.source "DlnaVideoPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;,
        Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;
    }
.end annotation


# static fields
.field private static seekTimes:I

.field protected static state:I


# instance fields
.field private appSkin:Lcom/mstar/android/tv/TvCommonManager;

.field private chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

.field private currentPlayerPosition:I

.field private duration:I

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private isAudioSupport:Z

.field private isControllerShow:Z

.field private isPlaying:Z

.field private isPrepared:Z

.field private isVideoSupport:Z

.field public myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

.field private playSpeed:I

.field private progressDialog:Landroid/app/ProgressDialog;

.field receiver:Landroid/content/BroadcastReceiver;

.field private seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private seekPosition:I

.field private videoHandler:Landroid/os/Handler;

.field private videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

.field protected videoPlayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field protected video_position:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    const/4 v0, 0x0

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekTimes:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    iput v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->currentPlayerPosition:I

    iput v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->video_position:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayList:Ljava/util/ArrayList;

    iput v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekPosition:I

    iput v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isAudioSupport:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isVideoSupport:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPrepared:Z

    new-instance v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$2;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$4;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    new-instance v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->receiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private InitVideoPlayer(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isAudioSupport:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isVideoSupport:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPrepared:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mbNotSeek:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, p1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVideoPath(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private InitView()V
    .locals 2

    new-instance v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    const v0, 0x7f08001b    # com.jrm.localmm.R.id.dlna_video_suspension_layout

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->playControlLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideController()V

    return-void
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showToastTip(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->processErrorUnknown(Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showErrorDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPrepared:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPrepared:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isAudioSupport:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isVideoSupport:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->slowForward()V

    return-void
.end method

.method static synthetic access$1900(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    return v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->currentPlayerPosition:I

    return v0
.end method

.method static synthetic access$2000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    return v0
.end method

.method static synthetic access$202(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->currentPlayerPosition:I

    return p1
.end method

.method static synthetic access$2100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->fastForward()V

    return-void
.end method

.method static synthetic access$2200(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showVideoTimeSet()V

    return-void
.end method

.method static synthetic access$2300(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/mstar/android/tv/TvCommonManager;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Lcom/mstar/android/tv/TvCommonManager;)Lcom/mstar/android/tv/TvCommonManager;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Lcom/mstar/android/tv/TvCommonManager;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekPosition:I

    return v0
.end method

.method static synthetic access$602(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekPosition:I

    return p1
.end method

.method static synthetic access$702(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekTimes:I

    return p0
.end method

.method static synthetic access$704()I
    .locals 1

    sget v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekTimes:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekTimes:I

    return v0
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V

    return-void
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I

    return v0
.end method

.method static synthetic access$902(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I

    return p1
.end method

.method private cancleDelayHide()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private dismissProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private fastForward()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResume(Z)V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getPlayMode()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    iget v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    if-lez v1, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setPlaySpeed(I)V

    goto :goto_0
.end method

.method private hideController()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->playControlLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->playControlLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "playControlLayout is null ptr!!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processErrorUnknown(Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    new-instance v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    const v1, 0x7f06006a    # com.jrm.localmm.R.string.video_media_error_unknown

    sparse-switch p3, :sswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;->strMessage:Ljava/lang/String;

    return-object v0

    :sswitch_0
    const v1, 0x7f06006b    # com.jrm.localmm.R.string.video_media_error_malformed

    goto :goto_0

    :sswitch_1
    const v1, 0x7f06006c    # com.jrm.localmm.R.string.video_media_error_io

    goto :goto_0

    :sswitch_2
    const v1, 0x7f06006d    # com.jrm.localmm.R.string.video_media_error_unsupported

    goto :goto_0

    :sswitch_3
    const v1, 0x7f06006e    # com.jrm.localmm.R.string.video_media_error_format_unsupport

    goto :goto_0

    :sswitch_4
    const v1, 0x7f06006f    # com.jrm.localmm.R.string.video_media_error_not_connected

    goto :goto_0

    :sswitch_5
    const v1, 0x7f060070    # com.jrm.localmm.R.string.video_media_error_audio_unsupport

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$errorStruct;->showStateWithError:Z

    goto :goto_0

    :sswitch_6
    const v1, 0x7f060071    # com.jrm.localmm.R.string.video_media_error_video_unsupport

    goto :goto_0

    :sswitch_7
    const v1, 0x7f060072    # com.jrm.localmm.R.string.video_media_error_no_license

    goto :goto_0

    :sswitch_8
    const v1, 0x7f060073    # com.jrm.localmm.R.string.video_media_error_license_expired

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x138b -> :sswitch_3
        -0x138a -> :sswitch_6
        -0x1389 -> :sswitch_5
        -0x7d2 -> :sswitch_8
        -0x7d1 -> :sswitch_7
        -0x3f2 -> :sswitch_2
        -0x3ef -> :sswitch_0
        -0x3ec -> :sswitch_1
        -0x3e9 -> :sswitch_4
    .end sparse-switch
.end method

.method private registerListeners()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->slowForward()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPrepared:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResumeFromSpeed(Z)V

    :goto_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localPause(Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResume(Z)V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->fastForward()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showVideoTimeSet()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showController()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showController()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->playControlLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->playControlLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "DlnaVideoPlayActivity"

    const-string v1, "playControlLayout is null ptr==="

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showErrorDialog(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060061    # com.jrm.localmm.R.string.show_info

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060007    # com.jrm.localmm.R.string.exit_ok

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$3;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$3;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method private showProgressDialog(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    return-void
.end method

.method private showToastTip(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isVideoSupport:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isAudioSupport:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->finish()V

    :cond_0
    return-void
.end method

.method private showVideoTimeSet()V
    .locals 7

    const/4 v6, 0x1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoTimeSelect(Z)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    if-nez v3, :cond_0

    new-instance v3, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    const v4, 0x7f070001    # com.jrm.localmm.R.style.choose_time_dialog

    invoke-direct {v3, p0, v4}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->requestWindowFeature(I)Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/16 v3, 0x96

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    const/16 v3, 0xb4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-boolean v4, v4, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mbNotSeek:Z

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4, v5}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setVariable(ZLandroid/os/Handler;)V

    :cond_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->show()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v3

    iput v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I

    const-string v3, "DlnaVideoPlayActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDuration()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->duration:I

    int-to-long v3, v3

    invoke-static {v3, v4}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getVideoTimeDurationTextView()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getVideoTimeCurrentPositionTextView()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06005c    # com.jrm.localmm.R.string.default_time

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setVariable(Z)V

    :cond_1
    return-void
.end method

.method private slowForward()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResume(Z)V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getPlayMode()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    iget v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    if-gez v1, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x2

    iput v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setPlaySpeed(I)V

    goto :goto_0
.end method


# virtual methods
.method public changeSource()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$6;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$6;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public hideControlDelay()V
    .locals 4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    const/16 v1, 0xd

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method protected localPause(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->pause()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setPlaySpeed(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    invoke-virtual {v0, p1, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    return-void
.end method

.method protected localResume(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setPlaySpeed(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    invoke-virtual {v0, p1, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    return-void
.end method

.method protected localResumeFromSpeed(Z)V
    .locals 4
    .param p1    # Z

    const/16 v3, 0xe

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    iput v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setPlaySpeed(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    invoke-virtual {v0, p1, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f060062    # com.jrm.localmm.R.string.buffering

    invoke-direct {p0, v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showProgressDialog(I)V

    const v4, 0x7f030002    # com.jrm.localmm.R.layout.dlna_video_player_frame

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->InitView()V

    const-string v3, ""

    const-string v2, ""

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.jrm.index"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->video_position:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.jrm.arraylist"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->video_position:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->video_position:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->InitVideoPlayer(Ljava/lang/String;)V

    new-instance v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;-><init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v4, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v4, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setVideoName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "source.switch.from.storage"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "com.mstar.localmm.network.disconnect"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->changeSource()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "DlnaVideoPlayActivity"

    const-string v1, "***************onDestroy********"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->dismissProgressDialog()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v2, 0x42

    const/4 v0, 0x1

    const/16 v1, 0x7e

    if-eq v1, p1, :cond_0

    const/16 v1, 0x7f

    if-eq v1, p1, :cond_0

    const/16 v1, 0x57

    if-eq v1, p1, :cond_0

    const/16 v1, 0x58

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    if-nez v1, :cond_2

    if-eq v2, p1, :cond_2

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    if-eq v2, p1, :cond_0

    const/4 v1, 0x4

    if-eq v1, p1, :cond_0

    const/16 v1, 0x15

    if-eq v1, p1, :cond_0

    const/16 v1, 0x16

    if-eq v1, p1, :cond_0

    const/16 v1, 0x14

    if-eq v1, p1, :cond_0

    const/16 v1, 0x13

    if-eq v1, p1, :cond_0

    const/16 v1, 0x52

    if-eq v1, p1, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/16 v2, 0x57

    if-eq v2, p1, :cond_0

    const/16 v2, 0x58

    if-ne v2, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v0, 0x0

    const/16 v2, 0x7e

    if-ne p1, v2, :cond_3

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    if-nez v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResume(Z)V

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResumeFromSpeed(Z)V

    goto :goto_0

    :cond_3
    const/16 v2, 0x7f

    if-ne p1, v2, :cond_4

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localPause(Z)V

    goto :goto_0

    :cond_4
    const/16 v2, 0x59

    if-ne p1, v2, :cond_5

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->slowForward()V

    :cond_5
    const/16 v2, 0x5a

    if-ne p1, v2, :cond_6

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->fastForward()V

    :cond_6
    iget-boolean v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    if-nez v2, :cond_7

    const/16 v2, 0x52

    if-ne p1, v2, :cond_7

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showController()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    :cond_7
    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->registerListeners()V

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v2, p1, p2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->processRightKey(ILandroid/view/KeyEvent;)Z

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    invoke-virtual {v2, p1, p2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->processLeftKey(ILandroid/view/KeyEvent;)Z

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_4
    const/4 v0, 0x1

    goto :goto_1

    :sswitch_5
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    :sswitch_6
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x15 -> :sswitch_3
        0x16 -> :sswitch_2
        0x18 -> :sswitch_5
        0x19 -> :sswitch_6
        0x42 -> :sswitch_1
        0x52 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    const-string v0, "DlnaVideoPlayActivity"

    const-string v1, "***************onResume********"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showController()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
