.class Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;
.super Ljava/lang/Object;
.source "ChooseTimePlayDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v9, 0x4

    const/4 v8, 0x5

    const/4 v4, 0x1

    const/4 v7, 0x0

    const v6, 0x7f020001    # com.jrm.localmm.R.drawable.accurate_seek_focus

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    move v3, v4

    :goto_1
    return v3

    :pswitch_0
    const/4 v0, 0x0

    const/16 v3, 0x16

    if-ne p2, v3, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setBackgroundColor(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    :goto_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # operator++ for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$008(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v3

    if-le v3, v8, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3, v7}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto :goto_0

    :pswitch_1
    const v3, 0x7f08009b    # com.jrm.localmm.R.id.videoTimeChooseNumTwo

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_2
    const v3, 0x7f08009c    # com.jrm.localmm.R.id.videoTimeChooseNumThree

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_3
    const v3, 0x7f08009d    # com.jrm.localmm.R.id.videoTimeChooseNumFour

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_4
    const v3, 0x7f08009e    # com.jrm.localmm.R.id.videoTimeChooseNumFive

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_5
    const v3, 0x7f08009a    # com.jrm.localmm.R.id.videoTimeChooseNumSix

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_2

    :pswitch_6
    const v3, 0x7f080099    # com.jrm.localmm.R.id.videoTimeChooseNumOne

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_2

    :cond_1
    const/16 v3, 0x15

    if-ne p2, v3, :cond_2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setBackgroundColor(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v3

    packed-switch v3, :pswitch_data_2

    :goto_3
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # operator-- for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$010(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v3

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v3, v8}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto/16 :goto_0

    :pswitch_7
    const v3, 0x7f08009a    # com.jrm.localmm.R.id.videoTimeChooseNumSix

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_3

    :pswitch_8
    const v3, 0x7f080099    # com.jrm.localmm.R.id.videoTimeChooseNumOne

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_3

    :pswitch_9
    const v3, 0x7f08009b    # com.jrm.localmm.R.id.videoTimeChooseNumTwo

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_3

    :pswitch_a
    const v3, 0x7f08009c    # com.jrm.localmm.R.id.videoTimeChooseNumThree

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto :goto_3

    :pswitch_b
    const v3, 0x7f08009d    # com.jrm.localmm.R.id.videoTimeChooseNumFour

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    const/4 v5, 0x3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_3

    :pswitch_c
    const v3, 0x7f08009e    # com.jrm.localmm.R.id.videoTimeChooseNumFive

    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setBackgroundResource(I)V

    goto/16 :goto_3

    :cond_2
    const/16 v3, 0x42

    if-ne p2, v3, :cond_3

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # invokes: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->chooseTime()Z
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$200(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Z

    move-result v3

    goto/16 :goto_1

    :cond_3
    add-int/lit8 v2, p2, -0x7

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x11

    if-ge p2, v3, :cond_0

    const/4 v3, 0x6

    if-le p2, v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v3

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v5

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_d
    const/16 v3, 0x42

    if-eq p2, v3, :cond_4

    if-ne p2, v9, :cond_0

    :cond_4
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->dismiss()V

    move v3, v4

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_d
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
