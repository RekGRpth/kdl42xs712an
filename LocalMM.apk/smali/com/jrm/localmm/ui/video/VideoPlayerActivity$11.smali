.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    return-void
.end method

.method public onCloseMusic()V
    .locals 0

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;I)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    const-string v0, "VideoPlayActivity"

    const-string v1, "onCompletion()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v1, 0x1

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V
    invoke-static {v0, v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showController()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;III)Z
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, "VideoPlayActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*********onError*********"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayback()V

    const-string v1, ""

    sparse-switch p2, :sswitch_data_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showErrorToast(Ljava/lang/String;I)V
    invoke-static {v2, v1, p4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;I)V

    const/4 v2, 0x1

    :goto_1
    return v2

    :sswitch_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060076    # com.jrm.localmm.R.string.video_media_error_server_died

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->processErrorUnknown(Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;
    invoke-static {v2, p1, p2, p3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3800(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;

    move-result-object v0

    iget-object v1, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;->strMessage:Ljava/lang/String;

    iget-boolean v2, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;->showStateWithError:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;->strMessage:Ljava/lang/String;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_1

    :sswitch_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060075    # com.jrm.localmm.R.string.video_media_error_not_valid

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_2
    .end sparse-switch
.end method

.method public onInfo(Landroid/media/MediaPlayer;III)Z
    .locals 7
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v4, "*************"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*******onInfo******"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p2, :sswitch_data_0

    const-string v2, "VideoPlayActivity"

    const-string v4, "Play onInfo::: default onInfo!"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    move v2, v3

    :cond_1
    :goto_1
    return v2

    :sswitch_0
    if-ne p3, v2, :cond_2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3, p4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getSubtitleData()Ljava/lang/String;

    move-result-object v0

    const-string v3, "*************"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "*******setSubTitleText******"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v2, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3, v0, p4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setSubTitleText(Ljava/lang/String;I)V

    goto :goto_1

    :cond_2
    if-nez p3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4, p4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setSubTitleText(Ljava/lang/String;I)V

    goto :goto_1

    :sswitch_1
    const-string v4, "*************"

    const-string v5, "*******MEDIA_INFO_NOT_SEEKABLE******"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v4

    invoke-virtual {v4, p4, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setSeekVar(IZ)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060077    # com.jrm.localmm.R.string.video_media_infor_no_index

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v4

    add-int/lit8 v5, p4, -0x1

    aget-object v4, v4, v5

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v4

    add-int/lit8 v5, p4, -0x1

    aget-object v4, v4, v5

    iget-boolean v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aFlag:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060078    # com.jrm.localmm.R.string.video_media_infor_no_index_ab

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v4

    add-int/lit8 v5, p4, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_3
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I
    invoke-static {v4, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2302(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v3, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_2
    if-ne p4, v2, :cond_4

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportOne:Z
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4002(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z

    :goto_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060070    # com.jrm.localmm.R.string.video_media_error_audio_unsupport

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportTwo:Z
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4102(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z

    goto :goto_2

    :sswitch_3
    if-ne p4, v2, :cond_5

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportOne:Z
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z

    :goto_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060071    # com.jrm.localmm.R.string.video_media_error_video_unsupport

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportTwo:Z
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4302(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z

    goto :goto_3

    :sswitch_4
    const-string v2, "VideoPlayActivity"

    const-string v4, "*********MEDIA_INFO_RENDERING_START************"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startRenderingTime:[J
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[J

    move-result-object v2

    add-int/lit8 v4, p4, -0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    aput-wide v5, v2, v4

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_4
        0x321 -> :sswitch_1
        0x3e8 -> :sswitch_0
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_3
    .end sparse-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;I)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setSubTitleText(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getSurfaceView(I)Landroid/view/SurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isBreakPointPlay:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPoint:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->initPlayer(I)V
    invoke-static {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)V

    return-void
.end method

.method public onSeekComplete(Landroid/media/MediaPlayer;I)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v1, 0x0

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2302(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    return-void
.end method

.method public onUpdateSubtitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method
