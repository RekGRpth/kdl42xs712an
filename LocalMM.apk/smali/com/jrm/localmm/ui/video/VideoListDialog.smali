.class public Lcom/jrm/localmm/ui/video/VideoListDialog;
.super Landroid/app/Dialog;
.source "VideoListDialog.java"


# instance fields
.field list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mPosition:I

.field private mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field private onClickHandler:Landroid/os/Handler;

.field private selected:I

.field simpleAdapter:Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

.field private videoList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->selected:I

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/VideoListDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoListDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->selected:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/video/VideoListDialog;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoListDialog;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->selected:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/VideoListDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoListDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->getVideoName()V

    return-void
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/VideoListDialog;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->onClickHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addListener()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoListDialog$3;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoListDialog$3;-><init>(Lcom/jrm/localmm/ui/video/VideoListDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoListDialog$4;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoListDialog$4;-><init>(Lcom/jrm/localmm/ui/video/VideoListDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method private findView()V
    .locals 3

    const/4 v2, 0x1

    const v0, 0x7f0800ad    # com.jrm.localmm.R.id.VidoFilename

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->getVideoName()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mPosition:I

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->setSelection(I)V

    return-void
.end method

.method private getVideoName()V
    .locals 5

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayList()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->list:Ljava/util/List;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->list:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->list:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {v3, v4, v1}, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    iput-object v3, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->simpleAdapter:Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->simpleAdapter:Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f03001e    # com.jrm.localmm.R.layout.video_play_list

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoListDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v5, 0x106000d    # android.R.color.transparent

    invoke-virtual {v2, v5}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentViewPosition:I

    iput v5, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mPosition:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->findView()V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setClickable(Z)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->addListener()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/16 v2, 0x7e

    if-eq v2, p1, :cond_0

    const/16 v2, 0x7f

    if-eq v2, p1, :cond_0

    const/16 v2, 0x57

    if-eq v2, p1, :cond_0

    const/16 v2, 0x58

    if-ne v2, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/16 v2, 0x42

    if-ne p1, v2, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_3

    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    :cond_3
    const/16 v2, 0x52

    if-ne p1, v2, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f06000d    # com.jrm.localmm.R.string.exit_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000e    # com.jrm.localmm.R.string.exit_confirm

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v2, 0x7f060007    # com.jrm.localmm.R.string.exit_ok

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/video/VideoListDialog$1;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/video/VideoListDialog$1;-><init>(Lcom/jrm/localmm/ui/video/VideoListDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v2, 0x7f060008    # com.jrm.localmm.R.string.exit_cancel

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/video/VideoListDialog$2;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/video/VideoListDialog$2;-><init>(Lcom/jrm/localmm/ui/video/VideoListDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->onClickHandler:Landroid/os/Handler;

    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->videoList:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoListDialog;->simpleAdapter:Lcom/jrm/localmm/business/adapter/ListDataListAdapter;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->notifyDataSetChanged()V

    return-void
.end method
