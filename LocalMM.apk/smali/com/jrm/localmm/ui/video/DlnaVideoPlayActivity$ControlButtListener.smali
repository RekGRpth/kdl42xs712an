.class Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;
.super Ljava/lang/Object;
.source "DlnaVideoPlayActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ControlButtListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isControllerShow:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1700(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->slowForward()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoRewindSelect(Z)V

    sput v2, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPrepared:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->isPlaying:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1900(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->playSpeed:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$2000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResumeFromSpeed(Z)V

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localPause(Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResume(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->fastForward()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$2100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoWindSelect(Z)V

    const/4 v0, 0x3

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setAllUnSelect(Z)V

    const/4 v0, 0x4

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$800(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->showVideoTimeSet()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$2200(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$ControlButtListener;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideControlDelay()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08001e
        :pswitch_0    # com.jrm.localmm.R.id.dlna_video_rewind
        :pswitch_1    # com.jrm.localmm.R.id.dlna_video_play
        :pswitch_2    # com.jrm.localmm.R.id.dlna_video_wind
        :pswitch_3    # com.jrm.localmm.R.id.dlna_video_time
    .end packed-switch
.end method
