.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ControlButtonListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v2

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V
    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPreSelect(Z)V

    const/4 v0, 0x0

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    const v1, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->slowForward()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoRewindSelect(Z)V

    sput v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2800(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResumeFromSpeed(Z)V

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPause(Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->fastForward()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindSelect(Z)V

    const/4 v0, 0x3

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V
    invoke-static {v0, v3, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoNextSelect(Z)V

    const/4 v0, 0x4

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    const v1, 0x7f020067    # com.jrm.localmm.R.drawable.player_icon_next_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    const/4 v0, 0x7

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoTimeSetDialog()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoListDialog()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListSelect(Z)V

    const/16 v0, 0x8

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoInfoDialog()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoInforSelect(Z)V

    const/16 v0, 0x9

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showSettingDialog()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoSettingSelect(Z)V

    const/16 v0, 0x10

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showPlayABDialog()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    const/16 v0, 0x11

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->updateVoiceBar()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayVoiceSelect(Z)V

    const/16 v0, 0x12

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isOnePrepared:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    const/16 v0, 0x14

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualMode()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$3600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualFocus()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualFocusSwitchSelect(Z)V

    const/16 v0, 0x15

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->changeDualMode()V

    const/16 v0, 0x16

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0800b9
        :pswitch_0    # com.jrm.localmm.R.id.video_previous
        :pswitch_1    # com.jrm.localmm.R.id.video_rewind
        :pswitch_2    # com.jrm.localmm.R.id.video_play
        :pswitch_3    # com.jrm.localmm.R.id.video_wind
        :pswitch_4    # com.jrm.localmm.R.id.video_next
        :pswitch_5    # com.jrm.localmm.R.id.video_time
        :pswitch_6    # com.jrm.localmm.R.id.video_list
        :pswitch_7    # com.jrm.localmm.R.id.video_info
        :pswitch_8    # com.jrm.localmm.R.id.video_setting
        :pswitch_9    # com.jrm.localmm.R.id.play_icon_ab
        :pswitch_a    # com.jrm.localmm.R.id.play_icon_voice
        :pswitch_b    # com.jrm.localmm.R.id.play_icon_dual_switch
        :pswitch_c    # com.jrm.localmm.R.id.play_icon_dual_focus_switch
        :pswitch_d    # com.jrm.localmm.R.id.play_icon_dual_mode_switch
    .end packed-switch
.end method
