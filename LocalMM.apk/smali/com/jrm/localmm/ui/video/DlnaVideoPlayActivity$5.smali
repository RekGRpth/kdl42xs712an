.class Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;
.super Landroid/content/BroadcastReceiver;
.source "DlnaVideoPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "DlnaVideoPlayActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*******BroadcastReceiver**********"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->dismissProgressDialog()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$1100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$5;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->finish()V

    return-void
.end method
