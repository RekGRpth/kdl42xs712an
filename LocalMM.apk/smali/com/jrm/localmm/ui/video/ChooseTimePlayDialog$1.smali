.class Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;
.super Ljava/lang/Object;
.source "ChooseTimePlayDialog.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setBackgroundColor(I)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    const v1, 0x7f020001    # com.jrm.localmm.R.drawable.accurate_seek_focus

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setBackgroundResource(I)V

    new-instance v1, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1$1;-><init>(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return v3

    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v1, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    const/4 v2, 0x1

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    const/4 v2, 0x2

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    const/4 v2, 0x3

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    const/4 v2, 0x4

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;->this$0:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    const/4 v2, 0x5

    # setter for: Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080099
        :pswitch_0    # com.jrm.localmm.R.id.videoTimeChooseNumOne
        :pswitch_5    # com.jrm.localmm.R.id.videoTimeChooseNumSix
        :pswitch_1    # com.jrm.localmm.R.id.videoTimeChooseNumTwo
        :pswitch_2    # com.jrm.localmm.R.id.videoTimeChooseNumThree
        :pswitch_3    # com.jrm.localmm.R.id.videoTimeChooseNumFour
        :pswitch_4    # com.jrm.localmm.R.id.videoTimeChooseNumFive
    .end packed-switch
.end method
