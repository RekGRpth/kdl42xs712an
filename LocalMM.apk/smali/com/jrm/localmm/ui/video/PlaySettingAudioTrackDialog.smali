.class public Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
.super Landroid/app/Dialog;
.source "PlaySettingAudioTrackDialog.java"


# static fields
.field public static audioTrackSettingName:[I


# instance fields
.field private adapter:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;

.field private audioTrackListView:Landroid/widget/ListView;

.field private context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field private duration:I

.field private handler:Landroid/os/Handler;

.field private isResponseKey:Z

.field private onkeyListenter:Landroid/view/View$OnKeyListener;

.field private strFormatDuration:Ljava/lang/String;

.field private trackCount:I

.field private trackDuration:[I

.field private trackLanguage:[Ljava/lang/String;

.field private videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

.field private viewId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackSettingName:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f06008b    # com.jrm.localmm.R.string.audio_track_number
        0x7f06008a    # com.jrm.localmm.R.string.audio_track_count
        0x7f06008c    # com.jrm.localmm.R.string.audio_track_duration
        0x7f06008d    # com.jrm.localmm.R.string.audio_track_language
    .end array-data
.end method

.method public constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p2    # I
    .param p3    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->isResponseKey:Z

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    new-instance v0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$1;-><init>(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;-><init>(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput-object p3, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->isResponseKey:Z

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->isResponseKey:Z

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->changeAudioTrack(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->adapter:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private changeAudioTrack(I)V
    .locals 12
    .param p1    # I

    const v11, 0x7f0600c6    # com.jrm.localmm.R.string.audio_track_setting_0_value_1

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-eq p1, v8, :cond_0

    const/4 v4, -0x1

    if-ne p1, v4, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const-string v4, "temp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Track number: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    if-nez v4, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v7, v4}, Lcom/jrm/localmm/util/Tools;->getAudioTackSettingOpt(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    iget v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    if-ne v4, v8, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v6, v11}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v4, v7, v5, v6}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    goto :goto_0

    :cond_3
    const-string v4, "temp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Track number: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "temp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Track label: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    add-int/2addr v4, v1

    add-int/2addr v4, p1

    iget v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    rem-int v1, v4, v5

    invoke-virtual {v3, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->setAudioTrack(I)V

    iget v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    if-lez v4, :cond_7

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    aget v4, v4, v1

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAudioTrackDuration()I

    move-result v5

    aput v5, v4, v1

    :cond_4
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    aget v4, v4, v1

    const/16 v5, 0x3e8

    if-lt v4, v5, :cond_5

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    aget v4, v4, v1

    div-int/lit16 v4, v4, 0x3e8

    iput v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    const-string v4, "%02d:%02d:%02d"

    new-array v5, v10, [Ljava/lang/Object;

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    div-int/lit16 v6, v6, 0xe10

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    rem-int/lit16 v6, v6, 0xe10

    div-int/lit8 v6, v6, 0x3c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    rem-int/lit8 v6, v6, 0x3c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->strFormatDuration:Ljava/lang/String;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->strFormatDuration:Ljava/lang/String;

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v4, v9, v5, v6}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    :cond_5
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackLanguage:[Ljava/lang/String;

    aget-object v4, v4, v1

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackLanguage:[Ljava/lang/String;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAudioTrackLanguage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    :cond_6
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackLanguage:[Ljava/lang/String;

    aget-object v5, v5, v1

    iget v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v4, v10, v5, v6}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    :cond_7
    add-int/lit8 v1, v1, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v5, v11}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v4, v7, v2, v5}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    const-string v4, "temp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The new track label: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$2;-><init>(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->show()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f03001f    # com.jrm.localmm.R.layout.video_play_setting_audiotrack_dialog

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v7}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAudioTrackLanguage()Ljava/lang/String;

    move-result-object v7

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v5, v6, v7, v8}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    const v5, 0x7f0800ae    # com.jrm.localmm.R.id.playsetting_audiotrack_list

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;

    new-instance v5, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    sget-object v7, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackSettingName:[I

    iget-object v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v9, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v8, v9}, Lcom/jrm/localmm/util/Tools;->initAudioTackSettingOpt(Landroid/content/Context;I)[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;-><init>(Landroid/content/Context;[I[Ljava/lang/String;)V

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->adapter:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->adapter:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->setListeners()V

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fdb851eb851eb85L    # 0.43

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAudioTrackCount()I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v5, v6, v7, v8}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    iget v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    if-lez v5, :cond_1

    iget v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v7}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAudioTrackDuration()I

    move-result v7

    aput v7, v5, v6

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    const/16 v6, 0x3e8

    if-lt v5, v6, :cond_0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackDuration:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    div-int/lit16 v5, v5, 0x3e8

    iput v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    const-string v5, "%02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    div-int/lit16 v8, v8, 0xe10

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    rem-int/lit16 v8, v8, 0xe10

    div-int/lit8 v8, v8, 0x3c

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->duration:I

    rem-int/lit8 v8, v8, 0x3c

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->strFormatDuration:Ljava/lang/String;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->strFormatDuration:Ljava/lang/String;

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v5, v6, v7, v8}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    :cond_0
    iget v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackCount:I

    new-array v5, v5, [Ljava/lang/String;

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackLanguage:[Ljava/lang/String;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackLanguage:[Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v7}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAudioTrackLanguage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->trackLanguage:[Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    iget v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->viewId:I

    invoke-static {v5, v6, v7, v8}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
