.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    const/16 v5, 0x11

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->sourceFrom:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$4800(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v1

    if-ne v1, v5, :cond_2

    const-string v1, "VideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "netDisconnectReceiver: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v2, 0x7f060011    # com.jrm.localmm.R.string.net_disconnect

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v5, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->finish()V

    :cond_2
    return-void
.end method
