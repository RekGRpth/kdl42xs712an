.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    const-wide/16 v7, 0x3e8

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x13

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    if-eqz p3, :cond_b

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v2, "bPOSITION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-gt p2, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v2, "APOSITION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lt p2, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I
    invoke-static {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    # ++operator for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I
    invoke-static {}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1304()I

    move-result v1

    if-ne v1, v5, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    sput v6, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aFlag:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v2, "APOSITION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lt p2, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I
    invoke-static {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    # ++operator for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I
    invoke-static {}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1304()I

    move-result v1

    if-ne v1, v5, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_2
    sput v6, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I
    invoke-static {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    # ++operator for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I
    invoke-static {}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1304()I

    move-result v1

    if-ne v1, v5, :cond_5

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    sput v6, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_1

    :cond_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isSeekable(I)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I
    invoke-static {v1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    sget v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    const/16 v2, 0x14

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPauseFromDualSwitch(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    if-ne v1, v6, :cond_7

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualFocus()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    :cond_7
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->openOrCloseDualDecode(Z)V

    :goto_4
    # ++operator for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I
    invoke-static {}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1304()I

    move-result v1

    if-ne v1, v5, :cond_9

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    sput v6, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    goto/16 :goto_1

    :cond_8
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPause(Z)V

    goto :goto_4

    :cond_9
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_5

    :cond_a
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0600c1    # com.jrm.localmm.R.string.choose_time_failed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_b
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v2, "bPOSITION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ge p2, v1, :cond_c

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I

    move-result v1

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v2, "APOSITION"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ge p2, v1, :cond_0

    :cond_c
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPause(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v3, "APOSITION"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v3, "APOSITION"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    goto/16 :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method
