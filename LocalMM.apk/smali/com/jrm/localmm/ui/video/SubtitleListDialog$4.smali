.class Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;
.super Landroid/os/Handler;
.source "SubtitleListDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/SubtitleListDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    const/4 v12, 0x0

    const v11, 0x7f0600af    # com.jrm.localmm.R.string.subtitle_3_value_1

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$200(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->path:Ljava/lang/String;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$300(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "idx"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$400(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Lcom/jrm/localmm/ui/video/BorderTextViews;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$500(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v4

    const-string v5, "*************"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "***info**"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllInternalSubtitleCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {v0, v2, v8}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleLanguageType([Ljava/lang/String;Z)V

    if-eqz v4, :cond_0

    const-string v5, "*************"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "*****"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v5, v2, v8

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$600(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v6

    invoke-static {v10, v5, v6}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$600(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v5

    invoke-static {v2, v5}, Lcom/jrm/localmm/util/Tools;->setSubtitleLanguageType([Ljava/lang/String;I)V

    :goto_0
    const/4 v6, 0x1

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->listSubtitle:Ljava/util/List;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$900(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Ljava/util/List;

    move-result-object v5

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I
    invoke-static {v7}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$000(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v7

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I
    invoke-static {v7}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$600(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v7

    invoke-static {v6, v5, v7}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->playSettingSubtitleDialog:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$1000(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->show()V

    :goto_1
    return-void

    :cond_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$600(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v5

    invoke-static {v12, v5}, Lcom/jrm/localmm/util/Tools;->setSubtitleLanguageType([Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # operator++ for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$708(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$700(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v5

    if-gt v5, v9, :cond_1

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$800(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Landroid/os/Handler;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # setter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I
    invoke-static {v5, v8}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$702(Lcom/jrm/localmm/ui/video/SubtitleListDialog;I)I

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$600(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v5

    invoke-static {v12, v5}, Lcom/jrm/localmm/util/Tools;->setSubtitleLanguageType([Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$500(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v5

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->selected:I
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$000(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/business/video/VideoPlayView;->getSubtitleTrackInfo(I)Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v3

    const-string v5, "********subtitleTrackInfo*******"

    const-string v6, "***********"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_4

    invoke-virtual {v3, v8}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleLanguageType(Z)Ljava/lang/String;

    move-result-object v1

    const-string v5, "********subtitleTrackInfo*******"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "***********"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleType()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v1, :cond_4

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # operator++ for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$708(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->getInfoNum:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$700(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v5

    if-gt v5, v9, :cond_3

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$800(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Landroid/os/Handler;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    :cond_3
    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$200(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_4
    iget-object v5, p0, Lcom/jrm/localmm/ui/video/SubtitleListDialog$4;->this$0:Lcom/jrm/localmm/ui/video/SubtitleListDialog;

    # getter for: Lcom/jrm/localmm/ui/video/SubtitleListDialog;->viewId:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/SubtitleListDialog;->access$600(Lcom/jrm/localmm/ui/video/SubtitleListDialog;)I

    move-result v5

    invoke-static {v10, v1, v5}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto/16 :goto_0
.end method
