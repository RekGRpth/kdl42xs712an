.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPointPlay(IILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field final synthetic val$path:Ljava/lang/String;

.field final synthetic val$viewId:I


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->val$path:Ljava/lang/String;

    iput p3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->val$viewId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->val$path:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->val$viewId:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->val$path:Ljava/lang/String;

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;->val$viewId:I

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVideoPath(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
