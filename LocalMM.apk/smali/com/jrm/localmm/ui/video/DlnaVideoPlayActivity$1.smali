.class Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;
.super Landroid/os/Handler;
.source "DlnaVideoPlayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/16 v6, 0xe

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xd

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # invokes: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->hideController()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$000(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v6, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v2

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->currentPlayerPosition:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$202(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->currentPlayerPosition:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$200(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->currentPlayerPosition:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$200(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->current_time_video:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$300(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$300(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getVideoTimeCurrentPositionTextView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$400(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_3
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$500(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v2, v3, v5}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoTimeSelect(Z)V

    goto/16 :goto_0

    :cond_4
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x13

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->videoPlayHolder:Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$100(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    # getter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekPosition:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$600(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity$1;->this$0:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->localResume(Z)V

    # setter for: Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->seekTimes:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->access$702(I)I

    goto/16 :goto_0
.end method
