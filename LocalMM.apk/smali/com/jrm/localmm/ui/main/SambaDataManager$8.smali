.class Lcom/jrm/localmm/ui/main/SambaDataManager$8;
.super Landroid/os/Handler;
.source "SambaDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/LoginSambaListener;

    move-result-object v1

    const/16 v2, 0x11

    invoke-interface {v1, v2}, Lcom/jrm/localmm/ui/main/LoginSambaListener;->onEnd(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const-string v2, "USERNAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->usr:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1202(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const-string v2, "PASSWORD"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->pwd:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1302(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "SambaDataManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "user: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->usr:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pass: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->pwd:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    new-instance v2, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager$8;)V

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1402(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/PingDeviceListener;)Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/PingDeviceListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
