.class public Lcom/jrm/localmm/ui/main/LocalDataManager;
.super Lcom/jrm/localmm/ui/main/BaseDataManager;
.source "LocalDataManager.java"


# static fields
.field private static final AUDIO_PROJECTION:[Ljava/lang/String;


# instance fields
.field private activity:Landroid/app/Activity;

.field private activityType:I

.field private currentDirectory:Ljava/lang/String;

.field private currentPage:I

.field private deviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private position:I

.field private refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

.field private returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

.field private state:I

.field private storageManager:Landroid/os/storage/StorageManager;

.field private totalPage:I

.field private type:I

.field private uiData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/jrm/localmm/ui/main/LocalDataManager;->AUDIO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/jrm/localmm/ui/main/RefreshUIListener;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/jrm/localmm/ui/main/RefreshUIListener;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/BaseDataManager;-><init>(Landroid/content/res/Resources;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->deviceList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    iput v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activity:Landroid/app/Activity;

    new-instance v0, Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-direct {v0}, Lcom/jrm/localmm/business/data/ReturnStack;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    invoke-static {}, Lcom/jrm/localmm/util/Tools;->getUSBMountedPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "storage"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->storageManager:Landroid/os/storage/StorageManager;

    return-void
.end method

.method private addReturn()V
    .locals 7

    const v6, 0x7f060002    # com.jrm.localmm.R.string.back

    const/4 v5, 0x6

    invoke-static {}, Lcom/jrm/localmm/util/Tools;->getUSBMountedPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LocalDataManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "usbMountedPath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " currentDirectory : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "LocalDataManager"

    const-string v3, "STATUS_LOCAL_DEVICE_DISPLAY"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    iput v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    new-instance v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v0, v5}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activity:Landroid/app/Activity;

    invoke-virtual {v2, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    const-string v2, "top"

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    const-string v2, "LocalDataManager"

    const-string v3, "STATUS_LOCAL_RESOURCE_DISPLAY"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    iput v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    new-instance v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v0, v5}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activity:Landroid/app/Activity;

    invoke-virtual {v2, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    const-string v2, "local"

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private constructCurrentPage(I)V
    .locals 8
    .param p1    # I

    const/4 v5, 0x1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget v6, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    if-ne v6, v5, :cond_1

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_0
    iget-object v6, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->addReturn()V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    div-int/lit8 v6, v3, 0x9

    iput v6, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iget v6, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    rem-int/lit8 v7, v3, 0x9

    if-nez v7, :cond_0

    const/4 v5, 0x0

    :cond_0
    add-int/2addr v5, v6

    iput v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    const/4 v4, 0x0

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    mul-int/lit8 v5, v5, 0x9

    if-le v3, v5, :cond_2

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    mul-int/lit8 v4, v5, 0x9

    :goto_1
    const-string v5, "LocalDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "size : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " tail : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "totalPage:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    add-int/lit8 v5, v5, -0x1

    mul-int/lit8 v1, v5, 0x9

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_1

    :cond_3
    iput v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    :cond_4
    return-void
.end method

.method private enterDirectory(I)V
    .locals 9
    .param p1    # I

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    iput v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    const/4 v1, 0x0

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    if-ne v5, v8, :cond_1

    if-ltz p1, :cond_0

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge p1, v5, :cond_0

    iput v6, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-static {}, Lcom/jrm/localmm/util/Tools;->getUSBMountedPath()Ljava/lang/String;

    move-result-object v1

    const-string v5, "LocalDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "stack file path\uff041\ufffd7"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    new-instance v3, Lcom/jrm/localmm/business/data/ReturnData;

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    invoke-direct {v3, v1, v5, p1}, Lcom/jrm/localmm/business/data/ReturnData;-><init>(Ljava/lang/String;II)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v5, v3}, Lcom/jrm/localmm/business/data/ReturnStack;->push(Lcom/jrm/localmm/business/data/ReturnData;)V

    const-string v5, "LocalDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "push stack page:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " index : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iput v8, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iput v8, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/main/LocalDataManager;->startScan(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    if-ne v5, v6, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activityType:I

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v4, 0x0

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    add-int/lit8 v5, v5, -0x1

    mul-int/lit8 v5, v5, 0x9

    add-int/2addr v5, p1

    add-int/lit8 v4, v5, -0x1

    if-ltz v4, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_3

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getParentPath()Ljava/lang/String;

    move-result-object v1

    const-string v5, "LocalDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "activityType:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activityType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "LocalDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "stack file path\uff041\ufffd7"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getType()I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->type:I

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->type:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    new-instance v3, Lcom/jrm/localmm/business/data/ReturnData;

    iget v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    invoke-direct {v3, v1, v5, p1}, Lcom/jrm/localmm/business/data/ReturnData;-><init>(Ljava/lang/String;II)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v5, v3}, Lcom/jrm/localmm/business/data/ReturnStack;->push(Lcom/jrm/localmm/business/data/ReturnData;)V

    :cond_2
    const-string v5, "LocalDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "push stack page:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "index"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    const-string v5, "LocalDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "invalid index in browser, index : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private enterParentDirectory()V
    .locals 4

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnStack;->getTankage()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnStack;->pop()Lcom/jrm/localmm/business/data/ReturnData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/ReturnData;->getId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/ReturnData;->getPosition()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/ReturnData;->getPage()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    const-string v1, "LocalDataManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "returns path:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-static {}, Lcom/jrm/localmm/util/Tools;->getUSBMountedPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->showUSBDevice()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/main/LocalDataManager;->startScan(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private loadUSBDevice()V
    .locals 11

    iget-object v9, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    iget-object v9, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->storageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v9}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v8

    if-eqz v8, :cond_0

    array-length v9, v8

    if-nez v9, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string v6, ""

    move-object v0, v8

    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->storageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v9, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    const-string v9, "mounted"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/jrm/localmm/business/data/BaseData;

    const/4 v9, 0x5

    invoke-direct {v1, v9}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/business/data/BaseData;->setPath(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method protected browser(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iput p2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activityType:I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->showUSBDevice()V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->enterParentDirectory()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/LocalDataManager;->enterDirectory(I)V

    goto :goto_0
.end method

.method protected getCurrentPage(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-nez p2, :cond_3

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    const/16 v0, 0x9

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    :cond_0
    :goto_0
    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activityType:I

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->constructCurrentPage(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iget v3, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFinish(Ljava/util/List;IIILjava/util/List;)V

    :cond_1
    return-void

    :cond_2
    if-ne p1, v1, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iput v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    goto :goto_0

    :cond_3
    iput p2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activityType:I

    iput v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    goto :goto_0
.end method

.method protected getMediaFile(II)I
    .locals 11
    .param p1    # I
    .param p2    # I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-gez p1, :cond_0

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    neg-int v8, p1

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getMediaFileList(I)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :goto_0
    iget v8, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    add-int/lit8 v8, v8, -0x1

    mul-int/lit8 v8, v8, 0x9

    add-int/2addr v8, p2

    add-int/lit8 v3, v8, -0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    const-string v8, "LocalDataManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "all media file size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " currentPage : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " position : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v3, :cond_2

    if-ge v3, v7, :cond_2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v8, v3

    :goto_2
    return v8

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getMediaFileList(I)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public onFinish()V
    .locals 6

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activityType:I

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->constructCurrentPage(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iget v3, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFinish(Ljava/util/List;IIILjava/util/List;)V

    return-void
.end method

.method protected showUSBDevice()V
    .locals 11

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->loadUSBDevice()V

    new-instance v6, Lcom/jrm/localmm/business/data/BaseData;

    const/4 v1, 0x6

    invoke-direct {v6, v1}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->activity:Landroid/app/Activity;

    const v2, 0x7f060002    # com.jrm.localmm.R.string.back

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    const-string v1, "top"

    invoke-virtual {v6, v1}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_2

    div-int/lit8 v1, v9, 0x9

    iput v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    rem-int/lit8 v2, v9, 0x9

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    add-int/2addr v0, v1

    iput v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    const/4 v10, 0x0

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    mul-int/lit8 v0, v0, 0x9

    if-le v9, v0, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    mul-int/lit8 v10, v0, 0x9

    :goto_0
    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v8, v0, 0x9

    :goto_1
    if-ge v8, v10, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/jrm/localmm/business/data/BaseData;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_1
    move v10, v9

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->uiData:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentPage:I

    iget v3, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->totalPage:I

    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->position:I

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFinish(Ljava/util/List;IIILjava/util/List;)V

    return-void
.end method

.method protected showUSBDevice(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "LocalDataManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unmount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " currentDirectory : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->currentDirectory:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/main/LocalDataManager;->showUSBDevice()V

    :cond_1
    return-void
.end method
