.class Lcom/jrm/localmm/ui/main/DlnaDataManager$1;
.super Ljava/lang/Object;
.source "DlnaDataManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/DlnaDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "DlnaDataManager"

    const-string v1, "dlna onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    check-cast p2, Lcom/jrm/localmm/dlna/server/DLNABinder;

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;
    invoke-static {v0, p2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$002(Lcom/jrm/localmm/ui/main/DlnaDataManager;Lcom/jrm/localmm/dlna/server/DLNABinder;)Lcom/jrm/localmm/dlna/server/DLNABinder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$000(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/dlna/server/DLNABinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$000(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/dlna/server/DLNABinder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceChangeListener:Lcom/jrm/localmm/ui/main/DeviceChangeListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$100(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/ui/main/DeviceChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/dlna/server/DLNABinder;->addDeviceChangeListener(Lcom/jrm/localmm/ui/main/DeviceChangeListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # invokes: Lcom/jrm/localmm/ui/main/DlnaDataManager;->loadDeviceData()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "DlnaDataManager"

    const-string v1, "dlna onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$400(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->serviceConnection:Landroid/content/ServiceConnection;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$300(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method
