.class public Lcom/jrm/localmm/ui/main/FileBrowserActivity;
.super Landroid/app/Activity;
.source "FileBrowserActivity.java"


# instance fields
.field private adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;

.field private appSkin:Lcom/mstar/android/tv/TvCommonManager;

.field protected canResponse:Z

.field protected dataSource:I

.field protected dataType:I

.field private desDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private diskChangeReceiver:Landroid/content/BroadcastReceiver;

.field private dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

.field private handler:Landroid/os/Handler;

.field private holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

.field private lastDataSource:I

.field private localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

.field private motionY:I

.field private networkReceiver:Landroid/content/BroadcastReceiver;

.field private onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private onItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private onKeyListener:Landroid/view/View$OnKeyListener;

.field private onTouchListener:Landroid/view/View$OnTouchListener;

.field private sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

.field private sourceData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field protected tmpType:I

.field private topDataBrowser:Lcom/jrm/localmm/ui/main/TopDataBrowser;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->topDataBrowser:Lcom/jrm/localmm/ui/main/TopDataBrowser;

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;

    iput v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    iput v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I

    iput v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    iput v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    iput-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity$1;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity$2;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->diskChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$7;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity$7;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->networkReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;II)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dispatchKeyEvent(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showHits(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dismissScanMessage()V

    return-void
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/business/adapter/DataAdapter;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->switchMediaType(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->processExceptions(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->release()V

    return-void
.end method

.method static synthetic access$1700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I

    return v0
.end method

.method static synthetic access$202(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I

    return p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I

    return v0
.end method

.method static synthetic access$602(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I

    return p1
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    return-object v0
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/TopDataBrowser;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->topDataBrowser:Lcom/jrm/localmm/ui/main/TopDataBrowser;

    return-object v0
.end method

.method private dismissScanMessage()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->showScanStatus(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0, v1, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->updateScanStatusText(ZI)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    return-void
.end method

.method private dispatchKeyEvent(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    :cond_0
    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->topDataBrowser:Lcom/jrm/localmm/ui/main/TopDataBrowser;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/TopDataBrowser;->processKeyDown(II)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->processKeyDown(II)Z

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->processKeyDown(II)Z

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->processKeyDown(II)Z

    goto :goto_0
.end method

.method private processDownKeyDown()Z
    .locals 4

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dispatchKeyEvent(II)V

    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "FileBrowserActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "old type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {v1, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iput v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    add-int/lit8 v0, v1, 0x1

    goto :goto_1
.end method

.method private processExceptions(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-ne p1, v3, :cond_2

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v0, v5, :cond_1

    const v0, 0x7f060029    # com.jrm.localmm.R.string.miss_dlna_device

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    const v0, 0x7f060027    # com.jrm.localmm.R.string.loading_dlna_device

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    invoke-virtual {v0, v2, v1}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->browser(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v0, v4, :cond_0

    const v0, 0x7f06002c    # com.jrm.localmm.R.string.miss_samba_device

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    const v0, 0x7f06002a    # com.jrm.localmm.R.string.loading_samba_device

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v0, v2, v1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browser(II)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    const v0, 0x7f06002e    # com.jrm.localmm.R.string.login_wrong_password

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    :cond_3
    :goto_1
    iput-boolean v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    goto :goto_0

    :cond_4
    if-ne p1, v4, :cond_5

    const v0, 0x7f06002f    # com.jrm.localmm.R.string.login_failed

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x5

    if-ne p1, v0, :cond_6

    const v0, 0x7f060030    # com.jrm.localmm.R.string.login_other_failed

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    goto :goto_1

    :cond_6
    const/16 v0, 0x10

    if-ne p1, v0, :cond_7

    const v0, 0x7f060006    # com.jrm.localmm.R.string.unsupport_format

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    goto :goto_1

    :cond_7
    if-ne p1, v5, :cond_8

    const v0, 0x7f060037    # com.jrm.localmm.R.string.mount_failed

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    goto :goto_1

    :cond_8
    const/16 v0, 0xd

    if-ne p1, v0, :cond_3

    const v0, 0x7f060033    # com.jrm.localmm.R.string.network_unconnected

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showToast(I)V

    goto :goto_1
.end method

.method private processLeftKeyDown()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iput v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v0, v1, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setLeftFocus(IZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0, v2, v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setListViewFocus(ZI)V

    :cond_0
    return v3
.end method

.method private processRightKeyDown()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setLeftFocus(IZ)V

    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0, v3, v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setListViewFocus(ZI)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showHits(I)V

    goto :goto_0
.end method

.method private processUpKeyDown()Z
    .locals 5

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dispatchKeyEvent(II)V

    :goto_0
    return v4

    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    if-ne v1, v4, :cond_1

    const/4 v0, 0x4

    :goto_1
    const-string v1, "FileBrowserActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "old type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {v1, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iput v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    add-int/lit8 v0, v1, -0x1

    goto :goto_1
.end method

.method private release()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->release()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->release()V

    return-void
.end method

.method private showHits(I)V
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    iget v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "FileBrowserActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showHits invalid index : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showScanMessage(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->showScanStatus(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0, v1, p1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->updateScanStatusText(ZI)V

    :cond_0
    return-void
.end method

.method private showToast(I)V
    .locals 2
    .param p1    # I

    if-gtz p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private showVersion()V
    .locals 2

    const-string v0, "soft name[MM Product JB]; version[1.1.02]; build date[2013-02-25]; public development name[neddy.xu]; CL#:[11756]"

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private switchMediaType(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    iget v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030001    # com.jrm.localmm.R.layout.browser_list

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showVersion()V

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->findViews()V

    new-instance v0, Lcom/jrm/localmm/ui/main/TopDataBrowser;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/jrm/localmm/ui/main/TopDataBrowser;-><init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->topDataBrowser:Lcom/jrm/localmm/ui/main/TopDataBrowser;

    new-instance v0, Lcom/jrm/localmm/business/adapter/DataAdapter;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/jrm/localmm/business/adapter/DataAdapter;-><init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;-><init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    new-instance v0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;-><init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;-><init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->unmount()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/jrm/localmm/util/Tools;->changeSource(Z)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->release()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->diskChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->networkReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-static {}, Ljava/lang/System;->gc()V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/16 v0, 0x15

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->processLeftKeyDown()Z

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x16

    if-ne p1, v0, :cond_3

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->processRightKeyDown()Z

    move-result v0

    goto :goto_0

    :cond_3
    const/16 v0, 0x13

    if-ne p1, v0, :cond_4

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->processUpKeyDown()Z

    move-result v0

    goto :goto_0

    :cond_4
    const/16 v0, 0x14

    if-ne p1, v0, :cond_5

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->processDownKeyDown()Z

    move-result v0

    goto :goto_0

    :cond_5
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    invoke-static {v2}, Lcom/jrm/localmm/util/Tools;->changeSource(Z)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mstar.localmm.network.disconnect"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->networkReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->diskChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected sendKeyEvent(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
