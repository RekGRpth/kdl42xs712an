.class public Lcom/jrm/localmm/ui/main/SambaDataManager;
.super Lcom/jrm/localmm/ui/main/BaseDataManager;
.source "SambaDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;,
        Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private browseType:I

.field private currentPage:I

.field private currentPath:Ljava/lang/String;

.field private deviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/samba/SmbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private findHostThread:Ljava/lang/Thread;

.field private handler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;

.field private mStorageManager:Lcom/mstar/android/storage/MStorageManager;

.field private pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

.field private position:I

.field private pwd:Ljava/lang/String;

.field private refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

.field private returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

.field private smbClient:Landroid/net/samba/SmbClient;

.field private smbDevice:Landroid/net/samba/SmbDevice;

.field private state:I

.field private totalPage:I

.field private uiData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private unmountListener:Lcom/jrm/localmm/ui/main/UnMountListener;

.field private usr:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/jrm/localmm/ui/main/LoginSambaListener;Lcom/jrm/localmm/ui/main/RefreshUIListener;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/jrm/localmm/ui/main/LoginSambaListener;
    .param p3    # Lcom/jrm/localmm/ui/main/RefreshUIListener;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/main/BaseDataManager;-><init>(Landroid/content/res/Resources;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    iput v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I

    const-string v0, ""

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->findHostThread:Ljava/lang/Thread;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataManager$8;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$8;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->handler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;

    iput-object p3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    new-instance v0, Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-direct {v0}, Lcom/jrm/localmm/business/data/ReturnStack;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-static {}, Landroid/net/samba/SmbClient;->initSamba()V

    invoke-static {p1}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->mStorageManager:Lcom/mstar/android/storage/MStorageManager;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbDevice;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    return-object v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/main/SambaDataManager;Landroid/net/samba/SmbDevice;)Landroid/net/samba/SmbDevice;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # Landroid/net/samba/SmbDevice;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    return-object p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/UnMountListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->unmountListener:Lcom/jrm/localmm/ui/main/UnMountListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/business/data/ReturnStack;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    return p1
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->usr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->usr:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pwd:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pwd:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/PingDeviceListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/PingDeviceListener;)Lcom/jrm/localmm/ui/main/PingDeviceListener;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;Ljava/lang/String;Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;

    invoke-direct {p0, p1, p2, p3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->login(Ljava/lang/String;Ljava/lang/String;Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbClient;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/jrm/localmm/ui/main/SambaDataManager;Landroid/net/samba/SmbClient;)Landroid/net/samba/SmbClient;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # Landroid/net/samba/SmbClient;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/jrm/localmm/ui/main/SambaDataManager;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->loadDeviceData()V

    return-void
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    return p1
.end method

.method static synthetic access$202(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/LoginSambaListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/main/SambaDataManager;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    return v0
.end method

.method static synthetic access$702(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    return p1
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/main/SambaDataManager;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    return v0
.end method

.method static synthetic access$802(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    return p1
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/main/SambaDataManager;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I

    return v0
.end method

.method private enterDirectory(I)V
    .locals 8
    .param p1    # I

    iget v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    iget v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    add-int/lit8 v5, v5, -0x1

    mul-int/lit8 v5, v5, 0x9

    add-int/2addr v5, p1

    add-int/lit8 v4, v5, -0x1

    if-ltz v4, :cond_2

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    const-string v5, "SambaDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "enterDirectory index : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " tmp index : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/samba/SmbDevice;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    invoke-virtual {v5}, Landroid/net/samba/SmbDevice;->isMounted()Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Lcom/jrm/localmm/ui/main/SambaDataManager$5;

    invoke-direct {v5, p0, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager$5;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;Landroid/net/samba/SmbDevice;)V

    iput-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->unmountListener:Lcom/jrm/localmm/ui/main/UnMountListener;

    new-instance v1, Lcom/jrm/localmm/ui/main/SambaDataManager$6;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$6;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    new-instance v3, Ljava/lang/Thread;

    invoke-direct {v3, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    new-instance v0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->activity:Landroid/app/Activity;

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->handler:Landroid/os/Handler;

    invoke-direct {v0, v5, v6}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->show()V

    goto :goto_0

    :cond_2
    const-string v5, "SambaDataManager"

    const-string v6, "invalid index when show login dialog"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    new-instance v5, Lcom/jrm/localmm/ui/main/SambaDataManager$7;

    invoke-direct {v5, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$7;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    iput-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    goto :goto_0
.end method

.method private enterParentDirectory()V
    .locals 5

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/ReturnStack;->getTankage()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/ReturnStack;->pop()Lcom/jrm/localmm/business/data/ReturnData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getPage()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;

    const-string v2, "SambaDataManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pop stack, page : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "samba"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    iput v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->loadDeviceData()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x2

    iput v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    new-instance v2, Lcom/jrm/localmm/ui/main/SambaDataManager$4;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$4;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    iput-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    goto :goto_0
.end method

.method private findSambaDevice()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/SambaDataManager$1;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->findHostThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->findHostThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private loadDeviceData()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iput v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;

    invoke-virtual {v4}, Landroid/net/samba/SmbClient;->getSmbDeviceList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_2

    div-int/lit8 v4, v1, 0x9

    iput v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    iget v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    rem-int/lit8 v5, v1, 0x9

    if-nez v5, :cond_1

    move v2, v3

    :cond_1
    add-int/2addr v2, v4

    iput v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    :goto_1
    const-string v2, "SambaDataManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadDeviceData, totalPage : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3, v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getCurrentPage(II)V

    goto :goto_0

    :cond_2
    const-string v2, "SambaDataManager"

    const-string v4, "load samba device failed"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const-string v2, "SambaDataManager"

    const-string v3, "smbDevice list is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private login(Ljava/lang/String;Ljava/lang/String;Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    new-instance v0, Landroid/net/samba/SmbAuthentication;

    invoke-direct {v0, p1, p2}, Landroid/net/samba/SmbAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    invoke-virtual {v1, v0}, Landroid/net/samba/SmbDevice;->setAuth(Landroid/net/samba/SmbAuthentication;)V

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->mStorageManager:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v1, v2}, Landroid/net/samba/SmbDevice;->setStorageManager(Lcom/mstar/android/storage/MStorageManager;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    invoke-virtual {v1, p3}, Landroid/net/samba/SmbDevice;->setOnRecvMsg(Landroid/net/samba/OnRecvMsg;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;

    const/16 v2, 0xa

    invoke-interface {v1, v2}, Lcom/jrm/localmm/ui/main/LoginSambaListener;->onEnd(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    invoke-virtual {v1}, Landroid/net/samba/SmbDevice;->getSharefolderList()Ljava/util/ArrayList;

    return-void

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/samba/SmbDevice;->setAuth(Landroid/net/samba/SmbAuthentication;)V

    goto :goto_0
.end method

.method private packageUIData()V
    .locals 13

    const v12, 0x7f060002    # com.jrm.localmm.R.string.back

    const/4 v11, 0x6

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    if-ne v8, v7, :cond_1

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    mul-int/lit8 v8, v8, 0x9

    if-le v5, v8, :cond_0

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    mul-int/lit8 v6, v8, 0x9

    :goto_0
    const-string v8, "SambaDataManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "device size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " tail : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    new-instance v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v0, v11}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->activity:Landroid/app/Activity;

    invoke-virtual {v8, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    const-string v8, "top"

    invoke-virtual {v0, v8}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    add-int/lit8 v8, v8, -0x1

    mul-int/lit8 v1, v8, 0x9

    :goto_1
    if-ge v1, v6, :cond_3

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/samba/SmbDevice;

    new-instance v2, Lcom/jrm/localmm/business/data/BaseData;

    const/4 v8, 0x5

    invoke-direct {v2, v8}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    invoke-virtual {v4}, Landroid/net/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v6, v5

    goto :goto_0

    :cond_1
    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_3

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    new-instance v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v0, v11}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->activity:Landroid/app/Activity;

    invoke-virtual {v8, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    const-string v8, "samba"

    invoke-virtual {v0, v8}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v8

    invoke-interface {v3, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    mul-int/lit8 v8, v8, 0x9

    if-le v5, v8, :cond_2

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    mul-int/lit8 v6, v8, 0x9

    :goto_2
    const-string v8, "SambaDataManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " tail : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    add-int/lit8 v8, v8, -0x1

    mul-int/lit8 v1, v8, 0x9

    :goto_3
    if-ge v1, v6, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/business/data/BaseData;

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    move v6, v5

    goto :goto_2

    :cond_3
    if-eqz v5, :cond_5

    div-int/lit8 v8, v5, 0x9

    iput v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    rem-int/lit8 v9, v5, 0x9

    if-nez v9, :cond_4

    const/4 v7, 0x0

    :cond_4
    add-int/2addr v7, v8

    iput v7, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    :goto_4
    return-void

    :cond_5
    iput v7, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    goto :goto_4
.end method


# virtual methods
.method protected browser(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iput p2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I

    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->findSambaDevice()V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->enterParentDirectory()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->enterDirectory(I)V

    goto :goto_0
.end method

.method protected getCurrentPage(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "SambaDataManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCurrentPage, increase : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_3

    if-ne p1, v3, :cond_2

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    iput v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->packageUIData()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->uiData:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    iget v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    iget v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFinish(Ljava/util/List;IIILjava/util/List;)V

    :cond_1
    return-void

    :cond_2
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    if-le v0, v3, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    const/16 v0, 0x9

    iput v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I

    if-eq v0, p2, :cond_0

    iput p2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I

    iput v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    iput v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    goto :goto_0
.end method

.method protected getMediaFile(II)I
    .locals 11
    .param p1    # I
    .param p2    # I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-gez p1, :cond_0

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    neg-int v8, p1

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getMediaFileList(I)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :goto_0
    iget v8, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    add-int/lit8 v8, v8, -0x1

    mul-int/lit8 v8, v8, 0x9

    add-int/2addr v8, p2

    add-int/lit8 v3, v8, -0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    const-string v8, "SambaDataManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "all media file size : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " currentPage : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " position : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v3, :cond_2

    if-ge v3, v7, :cond_2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v8, v3

    :goto_2
    return v8

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getMediaFileList(I)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public onFinish()V
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I

    invoke-virtual {p0, v0, v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getCurrentPage(II)V

    return-void
.end method

.method protected pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V
    .locals 3
    .param p1    # Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    if-eqz v2, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataManager$1;

    invoke-direct {v0, p0, p1}, Lcom/jrm/localmm/ui/main/SambaDataManager$1;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Lcom/jrm/localmm/ui/main/PingDeviceListener;->onFinish(Z)V

    goto :goto_0
.end method

.method protected release()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;

    if-eqz v0, :cond_2

    iput v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;

    invoke-virtual {v0}, Landroid/net/samba/SmbClient;->isUpdating()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;

    invoke-virtual {v0}, Landroid/net/samba/SmbClient;->StopUpdate()V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->findHostThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->findHostThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->findHostThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->findHostThread:Ljava/lang/Thread;

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/ReturnStack;->getTankage()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/ReturnStack;->clear()V

    :cond_2
    return-void
.end method

.method protected unmount()V
    .locals 3

    new-instance v2, Lcom/jrm/localmm/ui/main/SambaDataManager$2;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$2;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    iput-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->unmountListener:Lcom/jrm/localmm/ui/main/UnMountListener;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;

    invoke-virtual {v2}, Landroid/net/samba/SmbDevice;->isMounted()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataManager$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$3;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method
