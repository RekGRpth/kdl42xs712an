.class Lcom/jrm/localmm/ui/main/SambaDataManager$2;
.super Ljava/lang/Object;
.source "SambaDataManager.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/UnMountListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager;->unmount()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const/4 v1, 0x0

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$002(Lcom/jrm/localmm/ui/main/SambaDataManager;Landroid/net/samba/SmbDevice;)Landroid/net/samba/SmbDevice;

    const/16 v0, 0x12

    if-ne p1, v0, :cond_1

    const-string v0, "SambaDataManager"

    const-string v1, "unmount success"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    const-string v0, "SambaDataManager"

    const-string v1, "unmount failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
