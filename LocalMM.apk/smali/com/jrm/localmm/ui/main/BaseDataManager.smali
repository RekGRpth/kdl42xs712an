.class public Lcom/jrm/localmm/ui/main/BaseDataManager;
.super Ljava/lang/Object;
.source "BaseDataManager.java"


# instance fields
.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

.field private resource:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    new-instance v0, Lcom/jrm/localmm/ui/main/BaseDataManager$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/BaseDataManager$1;-><init>(Lcom/jrm/localmm/ui/main/BaseDataManager;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->comparator:Ljava/util/Comparator;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->resource:Landroid/content/res/Resources;

    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/BaseDataManager;[Ljava/io/File;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/BaseDataManager;
    .param p1    # [Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/BaseDataManager;->scan([Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/BaseDataManager;)Lcom/jrm/localmm/ui/MediaContainerApplication;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/BaseDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    return-object v0
.end method

.method private check(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    move-object v0, p2

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private putAllToCache(ILjava/util/List;)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->comparator:Ljava/util/Comparator;

    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/MediaContainerApplication;->putMediaData(ILjava/util/List;)V

    return-void
.end method

.method private scan([Ljava/io/File;)V
    .locals 18
    .param p1    # [Ljava/io/File;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v2, p1

    array-length v8, v2

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_5

    aget-object v4, v2, v7

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    new-instance v5, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v5}, Lcom/jrm/localmm/business/data/BaseData;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/jrm/localmm/business/data/BaseData;->setPath(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/jrm/localmm/business/data/BaseData;->setParentPath(Ljava/lang/String;)V

    invoke-virtual {v5, v14}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v16

    if-eqz v16, :cond_0

    const/16 v16, 0x5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    invoke-interface {v10, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    const-string v16, "."

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v15

    const-string v3, ""

    if-lez v15, :cond_1

    invoke-virtual {v14}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    add-int/lit8 v17, v15, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/jrm/localmm/business/data/BaseData;->setFormat(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/jrm/localmm/util/Tools;->formatSize(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/business/data/BaseData;->setSize(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->setModifyTime(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/BaseDataManager;->resource:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f050003    # com.jrm.localmm.R.array.photo_filter

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v1}, Lcom/jrm/localmm/ui/main/BaseDataManager;->check(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_2

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/BaseDataManager;->resource:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f050001    # com.jrm.localmm.R.array.audio_filter

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v1}, Lcom/jrm/localmm/ui/main/BaseDataManager;->check(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_3

    const/16 v16, 0x3

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/BaseDataManager;->resource:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f050002    # com.jrm.localmm.R.array.video_filter

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v1}, Lcom/jrm/localmm/ui/main/BaseDataManager;->check(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_4

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    invoke-interface {v13, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/jrm/localmm/ui/MediaContainerApplication;->clearAll()V

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_6

    const/16 v16, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v9}, Lcom/jrm/localmm/ui/main/BaseDataManager;->putAllToCache(ILjava/util/List;)V

    :cond_6
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_7

    const/16 v16, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v10}, Lcom/jrm/localmm/ui/main/BaseDataManager;->putAllToCache(ILjava/util/List;)V

    :cond_7
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_8

    const/16 v16, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v11}, Lcom/jrm/localmm/ui/main/BaseDataManager;->putAllToCache(ILjava/util/List;)V

    :cond_8
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_9

    const/16 v16, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v12}, Lcom/jrm/localmm/ui/main/BaseDataManager;->putAllToCache(ILjava/util/List;)V

    :cond_9
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_a

    const/16 v16, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v13}, Lcom/jrm/localmm/ui/main/BaseDataManager;->putAllToCache(ILjava/util/List;)V

    :cond_a
    return-void
.end method


# virtual methods
.method public final declared-synchronized getMediaFileList(I)Ljava/util/ArrayList;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch p1, :pswitch_data_0

    :goto_0
    monitor-exit p0

    return-object v0

    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :pswitch_1
    :try_start_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized getUIDataList(I)Ljava/util/List;
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x1

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-ne p1, v3, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/main/BaseDataManager;->getMediaFileList(I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onFinish()V
    .locals 0

    return-void
.end method

.method public final declared-synchronized startScan(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/main/BaseDataManager$2;

    invoke-direct {v1, p0, p1}, Lcom/jrm/localmm/ui/main/BaseDataManager$2;-><init>(Lcom/jrm/localmm/ui/main/BaseDataManager;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
