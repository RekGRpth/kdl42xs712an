.class Lcom/jrm/localmm/ui/main/FileBrowserActivity$1;
.super Ljava/lang/Object;
.source "FileBrowserActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/FileBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-string v0, "FileBrowserActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "list view onItemClick ... dataSource : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v2, v2, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const/16 v1, 0x42

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dispatchKeyEvent(II)V
    invoke-static {v0, v1, p3}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;II)V

    goto :goto_0
.end method
