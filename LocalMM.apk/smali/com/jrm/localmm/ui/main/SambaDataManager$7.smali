.class Lcom/jrm/localmm/ui/main/SambaDataManager$7;
.super Ljava/lang/Object;
.source "SambaDataManager.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/PingDeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager;->enterDirectory(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish(Z)V
    .locals 9
    .param p1    # Z

    const/4 v8, 0x1

    if-eqz p1, :cond_1

    const-string v2, ""

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$700(Lcom/jrm/localmm/ui/main/SambaDataManager;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    mul-int/lit8 v5, v5, 0x9

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I
    invoke-static {v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$800(Lcom/jrm/localmm/ui/main/SambaDataManager;)I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v4, v5, -0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->browseType:I
    invoke-static {v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$900(Lcom/jrm/localmm/ui/main/SambaDataManager;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getUIDataList(I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-ltz v4, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$202(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getParentPath()Ljava/lang/String;

    move-result-object v2

    :goto_0
    new-instance v3, Lcom/jrm/localmm/business/data/ReturnData;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$700(Lcom/jrm/localmm/ui/main/SambaDataManager;)I

    move-result v5

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I
    invoke-static {v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$800(Lcom/jrm/localmm/ui/main/SambaDataManager;)I

    move-result v6

    invoke-direct {v3, v2, v5, v6}, Lcom/jrm/localmm/business/data/ReturnData;-><init>(Ljava/lang/String;II)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/business/data/ReturnStack;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/jrm/localmm/business/data/ReturnStack;->push(Lcom/jrm/localmm/business/data/ReturnData;)V

    const-string v5, "SambaDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "scan file path : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const/4 v6, 0x0

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I
    invoke-static {v5, v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$802(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I
    invoke-static {v5, v8}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$702(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I
    invoke-static {v5, v8}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1102(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/LoginSambaListener;

    move-result-object v5

    const/16 v6, 0xe

    invoke-interface {v5, v6}, Lcom/jrm/localmm/ui/main/LoginSambaListener;->onEnd(I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/ui/main/SambaDataManager;->startScan(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string v5, "SambaDataManager"

    const-string v6, "invalied index on browser"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$7;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-result-object v5

    invoke-interface {v5, v8}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    goto :goto_1
.end method
