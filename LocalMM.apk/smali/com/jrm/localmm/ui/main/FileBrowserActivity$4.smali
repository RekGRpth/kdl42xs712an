.class Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;
.super Ljava/lang/Object;
.source "FileBrowserActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/FileBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    const/4 v5, 0x6

    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v1, 0x1

    if-ne p2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-boolean v2, v2, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x5c

    if-ne p2, v2, :cond_6

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    :cond_3
    :goto_1
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    goto :goto_1

    :cond_6
    const/16 v2, 0x5d

    if-ne p2, v2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    :cond_7
    :goto_2
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v0, v3, :cond_9

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$4;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    goto :goto_2
.end method
