.class Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;
.super Ljava/lang/Object;
.source "SambaDataManager.java"

# interfaces
.implements Landroid/net/samba/OnRecvMsg;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginStatus"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;


# direct methods
.method private constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/SambaDataManager$1;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p2    # Lcom/jrm/localmm/ui/main/SambaDataManager$1;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    return-void
.end method


# virtual methods
.method public onRecvMsg(I)V
    .locals 6
    .param p1    # I

    const/4 v4, 0x2

    const/4 v5, 0x1

    sparse-switch p1, :sswitch_data_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-result-object v2

    const/4 v3, 0x5

    invoke-interface {v2, v3}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    :goto_0
    return-void

    :sswitch_0
    const-string v2, "SambaDataManager"

    const-string v3, "wrong password"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    goto :goto_0

    :sswitch_1
    const-string v2, "SambaDataManager"

    const-string v3, "login failed "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    goto :goto_0

    :sswitch_2
    const-string v2, "SambaDataManager"

    const-string v3, "login ok"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->state:I
    invoke-static {v2, v4}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$2002(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I

    new-instance v1, Lcom/jrm/localmm/business/data/ReturnData;

    const-string v2, "samba"

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$700(Lcom/jrm/localmm/ui/main/SambaDataManager;)I

    move-result v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$800(Lcom/jrm/localmm/ui/main/SambaDataManager;)I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/jrm/localmm/business/data/ReturnData;-><init>(Ljava/lang/String;II)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/business/data/ReturnStack;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/business/data/ReturnStack;->push(Lcom/jrm/localmm/business/data/ReturnData;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/samba/SmbDevice;->getAuth()Landroid/net/samba/SmbAuthentication;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbDevice;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/samba/SmbDevice;->mount(Landroid/net/samba/SmbAuthentication;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->activity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$500(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f060005    # com.jrm.localmm.R.string.sambarootDirectory

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$202(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "SambaDataManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scan device path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/samba/SmbDevice;->isMounted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const/4 v3, 0x0

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->position:I
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$802(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPage:I
    invoke-static {v2, v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$702(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->totalPage:I
    invoke-static {v2, v5}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1102(Lcom/jrm/localmm/ui/main/SambaDataManager;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/LoginSambaListener;

    move-result-object v2

    const/16 v3, 0xe

    invoke-interface {v2, v3}, Lcom/jrm/localmm/ui/main/LoginSambaListener;->onEnd(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->currentPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->startScan(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/LoginSambaListener;

    move-result-object v2

    const/16 v3, 0x10

    invoke-interface {v2, v3}, Lcom/jrm/localmm/ui/main/LoginSambaListener;->onEnd(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const/4 v3, 0x0

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$002(Lcom/jrm/localmm/ui/main/SambaDataManager;Landroid/net/samba/SmbDevice;)Landroid/net/samba/SmbDevice;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3fffff96 -> :sswitch_0
        -0x3fffff93 -> :sswitch_1
        0x0 -> :sswitch_2
    .end sparse-switch
.end method
