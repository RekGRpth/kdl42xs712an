.class Lcom/jrm/localmm/ui/main/DlnaDataManager$2;
.super Ljava/lang/Object;
.source "DlnaDataManager.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/DeviceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/DlnaDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mediaServerDisappeared()V
    .locals 2

    const-string v0, "DlnaDataManager"

    const-string v1, "mediaServerDisappeared"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$500(Lcom/jrm/localmm/ui/main/DlnaDataManager;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # invokes: Lcom/jrm/localmm/ui/main/DlnaDataManager;->loadDeviceData()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    :cond_0
    return-void
.end method

.method public mediaServerDiscovered()V
    .locals 2

    const-string v0, "DlnaDataManager"

    const-string v1, "mediaServerDiscovered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$500(Lcom/jrm/localmm/ui/main/DlnaDataManager;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # invokes: Lcom/jrm/localmm/ui/main/DlnaDataManager;->loadDeviceData()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    :cond_0
    return-void
.end method
