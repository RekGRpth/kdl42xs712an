.class public Lcom/jrm/localmm/ui/main/DlnaDataManager;
.super Ljava/lang/Object;
.source "DlnaDataManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private currentPage:I

.field private currentPath:Ljava/lang/String;

.field private deviceChangeListener:Lcom/jrm/localmm/ui/main/DeviceChangeListener;

.field private deviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/MediaServerDisplay;",
            ">;"
        }
    .end annotation
.end field

.field private dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

.field private isBindOk:Z

.field private mediaServerDisplay:Lcom/jrm/localmm/business/data/MediaServerDisplay;

.field private pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

.field private position:I

.field private refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

.field private resourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;"
        }
    .end annotation
.end field

.field private returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

.field private serviceConnection:Landroid/content/ServiceConnection;

.field private state:I

.field private totalDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private totalPage:I

.field private uiDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/jrm/localmm/ui/main/RefreshUIListener;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/jrm/localmm/ui/main/RefreshUIListener;

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->isBindOk:Z

    new-instance v0, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager$1;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->serviceConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/jrm/localmm/ui/main/DlnaDataManager$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager$2;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceChangeListener:Lcom/jrm/localmm/ui/main/DeviceChangeListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    new-instance v0, Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-direct {v0}, Lcom/jrm/localmm/business/data/ReturnStack;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/dlna/server/DLNABinder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/main/DlnaDataManager;Lcom/jrm/localmm/dlna/server/DLNABinder;)Lcom/jrm/localmm/dlna/server/DLNABinder;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;
    .param p1    # Lcom/jrm/localmm/dlna/server/DLNABinder;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

    return-object p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/ui/main/DeviceChangeListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceChangeListener:Lcom/jrm/localmm/ui/main/DeviceChangeListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    return p1
.end method

.method static synthetic access$1312(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;
    .param p1    # I

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    return v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->loadDeviceData()V

    return-void
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->serviceConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/main/DlnaDataManager;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    return v0
.end method

.method static synthetic access$502(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    return p1
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/business/data/MediaServerDisplay;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->mediaServerDisplay:Lcom/jrm/localmm/business/data/MediaServerDisplay;

    return-object v0
.end method

.method static synthetic access$702(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    return p1
.end method

.method static synthetic access$802(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataManager;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    return p1
.end method

.method private constructDeviceData()V
    .locals 12

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    mul-int/lit8 v0, v0, 0x9

    if-le v10, v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    mul-int/lit8 v11, v0, 0x9

    :goto_0
    const-string v0, "DlnaDataManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tail : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deviceList size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Lcom/jrm/localmm/business/data/BaseData;

    const/4 v0, 0x6

    invoke-direct {v6, v0}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->activity:Landroid/app/Activity;

    const v1, 0x7f060002    # com.jrm.localmm.R.string.back

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    const-string v0, "top"

    invoke-virtual {v6, v0}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v7, v0, 0x9

    :goto_1
    if-ge v7, v11, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/jrm/localmm/business/data/MediaServerDisplay;

    new-instance v8, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v8, v3}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->getFriendlyName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v9}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->getSerilNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    move v11, v10

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    :goto_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/jrm/localmm/business/data/MediaServerDisplay;

    new-instance v8, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v8, v3}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->getFriendlyName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v9}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->getSerilNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    iget v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    invoke-interface/range {v0 .. v5}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFinish(Ljava/util/List;IIILjava/util/List;)V

    return-void
.end method

.method private constructSourceData()V
    .locals 24

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    invoke-interface {v15, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/16 v21, 0x0

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v20

    move-object/from16 v0, p0

    iget v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    mul-int/lit8 v1, v1, 0x9

    move/from16 v0, v20

    if-le v0, v1, :cond_1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    mul-int/lit8 v21, v1, 0x9

    :goto_0
    const-string v1, "DlnaDataManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tail : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resources size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Lcom/jrm/localmm/business/data/BaseData;

    const/4 v1, 0x6

    invoke-direct {v7, v1}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->activity:Landroid/app/Activity;

    const v2, 0x7f060002    # com.jrm.localmm.R.string.back

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    const-string v1, "dlna"

    invoke-virtual {v7, v1}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v11, v1, 0x9

    :goto_1
    move/from16 v0, v21

    if-ge v11, v0, :cond_8

    invoke-interface {v15, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/net/dlna/ShareObject;

    new-instance v12, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v12}, Lcom/jrm/localmm/business/data/BaseData;-><init>()V

    invoke-virtual/range {v16 .. v16}, Landroid/net/dlna/ShareObject;->getTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    move-object/from16 v0, v16

    instance-of v1, v0, Landroid/net/dlna/ShareContainer;

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    :cond_0
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_1
    move/from16 v21, v20

    goto :goto_0

    :cond_2
    move-object/from16 v0, v16

    instance-of v1, v0, Landroid/net/dlna/ShareItem;

    if-eqz v1, :cond_0

    check-cast v16, Landroid/net/dlna/ShareItem;

    invoke-virtual/range {v16 .. v16}, Landroid/net/dlna/ShareItem;->getShareResource()Ljava/util/ArrayList;

    move-result-object v13

    if-eqz v13, :cond_3

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    :goto_3
    return-void

    :cond_4
    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/net/dlna/ShareResource;

    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Landroid/net/dlna/ShareResource;->getProtocolInfo()Landroid/net/dlna/ProtocolInfo;

    move-result-object v14

    if-eqz v14, :cond_3

    invoke-virtual/range {v17 .. v17}, Landroid/net/dlna/ShareResource;->getURI()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Lcom/jrm/localmm/business/data/BaseData;->setPath(Ljava/lang/String;)V

    invoke-virtual {v14}, Landroid/net/dlna/ProtocolInfo;->getContentFormat()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jrm/localmm/business/data/BaseData;->setFormat(Ljava/lang/String;)V

    invoke-static/range {v19 .. v19}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->checkMimeType(Ljava/lang/String;)I

    move-result v18

    const/4 v1, 0x1

    move/from16 v0, v18

    if-ne v0, v1, :cond_5

    const/4 v1, 0x2

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    :goto_4
    invoke-virtual/range {v17 .. v17}, Landroid/net/dlna/ShareResource;->getSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v1}, Lcom/jrm/localmm/util/Tools;->formatSize(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12, v10}, Lcom/jrm/localmm/business/data/BaseData;->setSize(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    const/4 v1, 0x2

    move/from16 v0, v18

    if-ne v0, v1, :cond_6

    const/4 v1, 0x3

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    goto :goto_4

    :cond_6
    const/4 v1, 0x3

    move/from16 v0, v18

    if-ne v0, v1, :cond_7

    const/4 v1, 0x4

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    goto :goto_4

    :cond_7
    const/4 v1, 0x1

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    goto :goto_4

    :cond_8
    const/4 v11, 0x0

    :goto_5
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v1

    if-ge v11, v1, :cond_e

    invoke-interface {v15, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/net/dlna/ShareObject;

    new-instance v12, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v12}, Lcom/jrm/localmm/business/data/BaseData;-><init>()V

    invoke-virtual/range {v16 .. v16}, Landroid/net/dlna/ShareObject;->getTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    move-object/from16 v0, v16

    instance-of v1, v0, Landroid/net/dlna/ShareContainer;

    if-eqz v1, :cond_a

    const/4 v1, 0x5

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    :cond_9
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :cond_a
    move-object/from16 v0, v16

    instance-of v1, v0, Landroid/net/dlna/ShareItem;

    if-eqz v1, :cond_9

    check-cast v16, Landroid/net/dlna/ShareItem;

    invoke-virtual/range {v16 .. v16}, Landroid/net/dlna/ShareItem;->getShareResource()Ljava/util/ArrayList;

    move-result-object v13

    if-eqz v13, :cond_3

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/net/dlna/ShareResource;

    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Landroid/net/dlna/ShareResource;->getProtocolInfo()Landroid/net/dlna/ProtocolInfo;

    move-result-object v14

    if-eqz v14, :cond_3

    invoke-virtual/range {v17 .. v17}, Landroid/net/dlna/ShareResource;->getURI()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Lcom/jrm/localmm/business/data/BaseData;->setPath(Ljava/lang/String;)V

    invoke-virtual {v14}, Landroid/net/dlna/ProtocolInfo;->getContentFormat()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jrm/localmm/business/data/BaseData;->setFormat(Ljava/lang/String;)V

    invoke-static/range {v19 .. v19}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->checkMimeType(Ljava/lang/String;)I

    move-result v18

    const/4 v1, 0x1

    move/from16 v0, v18

    if-ne v0, v1, :cond_b

    const/4 v1, 0x2

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    :goto_7
    invoke-virtual/range {v17 .. v17}, Landroid/net/dlna/ShareResource;->getSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v1}, Lcom/jrm/localmm/util/Tools;->formatSize(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12, v10}, Lcom/jrm/localmm/business/data/BaseData;->setSize(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Lcom/jrm/localmm/business/data/BaseData;->setDescription(Ljava/lang/String;)V

    goto :goto_6

    :cond_b
    const/4 v1, 0x2

    move/from16 v0, v18

    if-ne v0, v1, :cond_c

    const/4 v1, 0x3

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    goto :goto_7

    :cond_c
    const/4 v1, 0x3

    move/from16 v0, v18

    if-ne v0, v1, :cond_d

    const/4 v1, 0x4

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    goto :goto_7

    :cond_d
    const/4 v1, 0x1

    invoke-virtual {v12, v1}, Lcom/jrm/localmm/business/data/BaseData;->setType(I)V

    goto :goto_7

    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    invoke-interface/range {v1 .. v6}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFinish(Ljava/util/List;IIILjava/util/List;)V

    goto/16 :goto_3
.end method

.method private enterDirectory(I)V
    .locals 7
    .param p1    # I

    const/4 v3, 0x0

    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    add-int/lit8 v4, v4, -0x1

    mul-int/lit8 v4, v4, 0x9

    add-int/2addr v4, p1

    add-int/lit8 v3, v4, -0x1

    if-ltz v3, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/MediaServerDisplay;

    iput-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->mediaServerDisplay:Lcom/jrm/localmm/business/data/MediaServerDisplay;

    const-string v4, "0"

    iput-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPath:Ljava/lang/String;

    new-instance v1, Lcom/jrm/localmm/business/data/ReturnData;

    const-string v4, "dlna"

    iget v5, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    iget v6, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    invoke-direct {v1, v4, v5, v6, p1}, Lcom/jrm/localmm/business/data/ReturnData;-><init>(Ljava/lang/String;III)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v4, v1}, Lcom/jrm/localmm/business/data/ReturnStack;->push(Lcom/jrm/localmm/business/data/ReturnData;)V

    const-string v4, "DlnaDataManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "push stack, page : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " index : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    new-instance v4, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;

    invoke-direct {v4, p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    iput-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    :goto_1
    return-void

    :cond_1
    const-string v4, "DlnaDataManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "enterDirectory, invalid device index : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    add-int/lit8 v4, p1, -0x1

    iget v5, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    add-int/lit8 v5, v5, -0x1

    mul-int/lit8 v5, v5, 0x9

    add-int v3, v4, v5

    const-string v4, "DlnaDataManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tmp : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " resourceList.size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v3, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/dlna/ShareObject;

    instance-of v4, v2, Landroid/net/dlna/ShareContainer;

    if-eqz v4, :cond_0

    new-instance v0, Lcom/jrm/localmm/business/data/ReturnData;

    invoke-virtual {v2}, Landroid/net/dlna/ShareObject;->getParentID()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    iget v6, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    invoke-direct {v0, v4, v5, v6, p1}, Lcom/jrm/localmm/business/data/ReturnData;-><init>(Ljava/lang/String;III)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v4, v0}, Lcom/jrm/localmm/business/data/ReturnStack;->push(Lcom/jrm/localmm/business/data/ReturnData;)V

    const-string v4, "DlnaDataManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "push stack, page : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " totalPage : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " index : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/net/dlna/ShareObject;->getID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPath:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    const-string v4, "DlnaDataManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "enterDirectory, invalid device index : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private enterParentDirectory()V
    .locals 5

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/ReturnStack;->getTankage()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/ReturnStack;->pop()Lcom/jrm/localmm/business/data/ReturnData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getPage()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getTotal()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/ReturnData;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    const-string v2, "DlnaDataManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pop stack, page : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " total : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pop id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "dlna"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->loadDeviceData()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPath:Ljava/lang/String;

    new-instance v2, Lcom/jrm/localmm/ui/main/DlnaDataManager$5;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager$5;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    iput-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDeviceListener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    goto :goto_0
.end method

.method private initialDlna()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "DlnaDataManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialDlna, isBindOk : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->isBindOk:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->isBindOk:Z

    if-eqz v0, :cond_0

    iput v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    iput v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->loadDeviceData()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->activity:Landroid/app/Activity;

    const-class v2, Lcom/jrm/localmm/dlna/server/DLNAServer;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->isBindOk:Z

    goto :goto_0
.end method

.method private loadDeviceData()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-virtual {v3}, Lcom/jrm/localmm/dlna/server/DLNABinder;->getMediaServerControllers()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->deviceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    div-int/lit8 v3, v0, 0x9

    iput v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    iget v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    rem-int/lit8 v4, v0, 0x9

    if-nez v4, :cond_0

    move v1, v2

    :cond_0
    add-int/2addr v1, v3

    iput v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    :cond_1
    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->getCurrentPage(I)V

    return-void
.end method


# virtual methods
.method protected browser(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "DlnaDataManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "browser, index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->initialDlna()V

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->enterParentDirectory()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->enterDirectory(I)V

    goto :goto_0
.end method

.method protected getCurrentPage(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const-string v0, "DlnaDataManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCurrentPage, page : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v3, :cond_2

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    :cond_0
    :goto_0
    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    if-ne v0, v3, :cond_3

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->constructDeviceData()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    if-le v0, v3, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    const/16 v0, 0x9

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->constructSourceData()V

    goto :goto_1
.end method

.method protected pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V
    .locals 3
    .param p1    # Lcom/jrm/localmm/ui/main/PingDeviceListener;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->mediaServerDisplay:Lcom/jrm/localmm/business/data/MediaServerDisplay;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Lcom/jrm/localmm/ui/main/PingDeviceListener;->onFinish(Z)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/jrm/localmm/ui/main/DlnaDataManager$3;

    invoke-direct {v0, p0, p1}, Lcom/jrm/localmm/ui/main/DlnaDataManager$3;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method protected release()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->uiDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I

    iput v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I

    iput v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager;->returnStack:Lcom/jrm/localmm/business/data/ReturnStack;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/ReturnStack;->clear()V

    :cond_3
    return-void
.end method
