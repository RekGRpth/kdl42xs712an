.class Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;
.super Ljava/lang/Object;
.source "FileBrowserViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v1, 0x9

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iput v2, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iput v3, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iput v4, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v0

    iput v5, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080007
        :pswitch_1    # com.jrm.localmm.R.id.otherTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.otherTitleText
        :pswitch_2    # com.jrm.localmm.R.id.pictureTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.pictureTitleText
        :pswitch_3    # com.jrm.localmm.R.id.songTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.songTitleText
        :pswitch_4    # com.jrm.localmm.R.id.videoTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.videoTitleText
        :pswitch_0    # com.jrm.localmm.R.id.progress_scan
        :pswitch_0    # com.jrm.localmm.R.id.scanner_id
        :pswitch_5    # com.jrm.localmm.R.id.leftArrowImg
        :pswitch_0    # com.jrm.localmm.R.id.videoCurrentPageNumText
        :pswitch_0    # com.jrm.localmm.R.id.videoTotalPageNumText
        :pswitch_6    # com.jrm.localmm.R.id.rightArrowImg
    .end packed-switch
.end method
