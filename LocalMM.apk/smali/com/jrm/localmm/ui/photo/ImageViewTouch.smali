.class public Lcom/jrm/localmm/ui/photo/ImageViewTouch;
.super Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;
.source "ImageViewTouch.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEnableTrackballScroll:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setEnableTrackballScroll(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->mEnableTrackballScroll:Z

    return-void
.end method
