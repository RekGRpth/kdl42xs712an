.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;
.super Ljava/lang/Object;
.source "PhotoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/RotateBitmap;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v3, v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v0, v1, v2, v3}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setImageRotateBitmapResetBase(Lcom/jrm/localmm/ui/photo/RotateBitmap;ZLcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isAnimationOpened:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->startPhotoAnimation()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$4000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->drawImage()V

    goto :goto_0
.end method
