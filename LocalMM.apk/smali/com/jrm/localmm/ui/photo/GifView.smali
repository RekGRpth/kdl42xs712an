.class public Lcom/jrm/localmm/ui/photo/GifView;
.super Landroid/view/View;
.source "GifView.java"


# instance fields
.field private bmp:Landroid/graphics/Bitmap;

.field private delta:I

.field private dst:Landroid/graphics/Rect;

.field private gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

.field private is:Ljava/io/InputStream;

.field private isStop:Z

.field private src:Landroid/graphics/Rect;

.field private updateTimer:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jrm/localmm/ui/photo/GifView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->isStop:Z

    iput v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->delta:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/GifView;->setDelta(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/photo/GifView;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/GifView;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->isStop:Z

    return v0
.end method

.method private getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :cond_0
    return-object v0
.end method

.method private resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-ne v1, v5, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/GifView;->getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v4, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, p2, p2}, Landroid/graphics/Canvas;->scale(FF)V

    new-instance v2, Landroid/graphics/Paint;

    const/4 v5, 0x6

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    if-eqz p3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    move-object p1, v3

    goto :goto_0
.end method

.method private resizeDownIfTooBig(Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v2

    div-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    int-to-float v5, v1

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1, v0, p2}, Lcom/jrm/localmm/ui/photo/GifView;->resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method protected center()V
    .locals 9

    const/high16 v8, 0x40000000    # 2.0f

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    const/4 v7, 0x1

    invoke-direct {p0, v6, v7}, Lcom/jrm/localmm/ui/photo/GifView;->resizeDownIfTooBig(Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v2, v6

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v5, v6

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getHeight()I

    move-result v3

    int-to-float v6, v3

    cmpg-float v6, v2, v6

    if-gtz v6, :cond_2

    int-to-float v6, v3

    sub-float/2addr v6, v2

    div-float/2addr v6, v8

    iget-object v7, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    sub-float v1, v6, v7

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getWidth()I

    move-result v4

    int-to-float v6, v4

    cmpg-float v6, v5, v6

    if-gtz v6, :cond_4

    int-to-float v6, v4

    sub-float/2addr v6, v5

    div-float/2addr v6, v8

    iget-object v7, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    sub-float v0, v6, v7

    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    float-to-int v8, v1

    add-int/2addr v7, v8

    iput v7, v6, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    float-to-int v8, v0

    add-int/2addr v7, v8

    iput v7, v6, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    float-to-int v8, v1

    add-int/2addr v7, v8

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    float-to-int v8, v0

    add-int/2addr v7, v8

    iput v7, v6, Landroid/graphics/Rect;->right:I

    return-void

    :cond_2
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    neg-int v6, v6

    int-to-float v1, v6

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    if-ge v6, v3, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v6, v7

    int-to-float v1, v6

    goto :goto_0

    :cond_4
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    if-lez v6, :cond_5

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    neg-int v6, v6

    int-to-float v0, v6

    goto :goto_1

    :cond_5
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    if-ge v6, v4, :cond_1

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int v6, v4, v6

    int-to-float v0, v6

    goto :goto_1
.end method

.method public decodeBitmapFromNet(Ljava/lang/String;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    if-nez v1, :cond_2

    :goto_0
    new-instance v1, Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-direct {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;-><init>()V

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->is:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/GifView;->is:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/photo/GifDecoder;->read(Ljava/io/InputStream;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->postInvalidate()V

    const/4 v1, 0x1

    return v1

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->reset()V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->getImage()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public getFrameCount()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/photo/GifDecoder;->getFrameCount()I

    move-result v0

    return v0
.end method

.method public getPlayTime()I
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/photo/GifDecoder;->getFrameCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getPlayTimeEachFrame()I

    move-result v1

    mul-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlayTimeEachFrame()I
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    if-eqz v0, :cond_0

    const/16 v0, 0x64

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->nextDelay()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->delta:I

    div-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iput v3, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iput v3, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    iput v3, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    iput v3, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getHeight()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getWidth()I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->center()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/GifView;->src:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/GifView;->dst:Landroid/graphics/Rect;

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->nextBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public setDelta(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/photo/GifView;->delta:I

    return-void
.end method

.method public setSrc(Ljava/lang/String;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    if-nez v1, :cond_2

    :goto_0
    new-instance v1, Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-direct {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;-><init>()V

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->is:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/GifView;->is:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/photo/GifDecoder;->read(Ljava/io/InputStream;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->postInvalidate()V

    const/4 v1, 0x1

    return v1

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->reset()V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->gDecoder:Lcom/jrm/localmm/business/photo/GifDecoder;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/photo/GifDecoder;->getImage()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->bmp:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public setStart()V
    .locals 3

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->updateTimer:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->updateTimer:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->isStop:Z

    :try_start_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/GifView;->getPlayTimeEachFrame()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/jrm/localmm/ui/photo/GifView$1;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/photo/GifView$1;-><init>(Lcom/jrm/localmm/ui/photo/GifView;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->updateTimer:Ljava/lang/Thread;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->isStop:Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/GifView;->updateTimer:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public setStop()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/GifView;->isStop:Z

    return-void
.end method
