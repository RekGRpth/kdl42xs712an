.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;
.super Ljava/lang/Object;
.source "PhotoPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhotoImageViewClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p2    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const v6, 0x7f06004c    # com.jrm.localmm.R.string.photo_GIF_toast

    const v5, 0x7f06003f    # com.jrm.localmm.R.string.photo_3D_toast

    const/4 v4, 0x3

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showController()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v1, -0x1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPreSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->PlayProcess()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v1, 0x2

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V
    invoke-static {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNextSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->zoomIn()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoEnlargeSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v1, 0x4

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-ne v0, v4, :cond_5

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->zoomOut()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :goto_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNarrowSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v1, 0x5

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-ne v0, v4, :cond_7

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->rotateImageLeft()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :goto_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnLeftSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v1, 0x6

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-ne v0, v4, :cond_9

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->rotateImageRight()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :goto_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnRightSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v1, 0x7

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-ne v0, v4, :cond_b

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showPhotoInfo()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoInfoSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/16 v1, 0x8

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanSetWallpaper:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setPhoto2Wallpaper()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    :goto_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoWallpaperSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/16 v1, 0x9

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v2, 0x7f060045    # com.jrm.localmm.R.string.is_setting_wallpaper

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_5

    :pswitch_9
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setPhotoThreeD()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhoto3DSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/16 v1, 0xa

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setAllUnSelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->sbowPhotoSetting()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoSettingSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/16 v1, 0xb

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08006a
        :pswitch_0    # com.jrm.localmm.R.id.player_previous
        :pswitch_1    # com.jrm.localmm.R.id.photo_play
        :pswitch_2    # com.jrm.localmm.R.id.photo_next
        :pswitch_3    # com.jrm.localmm.R.id.photo_enlarge
        :pswitch_4    # com.jrm.localmm.R.id.photo_narrow
        :pswitch_5    # com.jrm.localmm.R.id.photo_turn_left
        :pswitch_6    # com.jrm.localmm.R.id.photo_turn_right
        :pswitch_7    # com.jrm.localmm.R.id.photo_info
        :pswitch_8    # com.jrm.localmm.R.id.photo_wallpaper
        :pswitch_9    # com.jrm.localmm.R.id.photo_3d
        :pswitch_a    # com.jrm.localmm.R.id.photo_setting
    .end packed-switch
.end method
