.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;
.super Ljava/lang/Object;
.source "PhotoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeGif(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v2

    const/16 v3, 0x10

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->val$url:Ljava/lang/String;

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v2, v3, v4}, Lcom/jrm/localmm/ui/photo/GifView;->setSrc(Ljava/lang/String;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/GifView;->getFrameCount()I

    move-result v2

    if-le v2, v5, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/GifView;->setStart()V

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/GifView;->getFrameCount()I

    move-result v2

    if-le v2, v5, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/photo/GifView;->getPlayTime()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_1
    const-string v2, "PhotoPlayerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*****mGifView playtime******"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xb

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    return-void

    :cond_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->val$url:Ljava/lang/String;

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v2, v3, v4}, Lcom/jrm/localmm/ui/photo/GifView;->decodeBitmapFromNet(Ljava/lang/String;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v0

    goto/16 :goto_0
.end method
