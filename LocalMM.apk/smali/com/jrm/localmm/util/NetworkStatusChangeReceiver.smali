.class public Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkStatusChangeReceiver.java"


# instance fields
.field private final ETHERNET_STATE_CHANGED_ACTION:Ljava/lang/String;

.field private final EVENT_HW_CONNECTED:I

.field private final EVENT_HW_DISCONNECTED:I

.field private final EVENT_INTERFACE_CONFIGURATION_FAILED:I

.field private final EVENT_INTERFACE_CONFIGURATION_SUCCEEDED:I

.field private final PPPOE_STATE_ACTION:Ljava/lang/String;

.field private final PPPOE_STATE_CONNECT:Ljava/lang/String;

.field private final PPPOE_STATE_DISCONNECT:Ljava/lang/String;

.field private final PPPOE_STATE_STATUE:Ljava/lang/String;

.field private mEthernetFlag:Z

.field private mPppoeFlag:Z

.field private mWifiFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, "android.net.pppoe.PPPOE_STATE_ACTION"

    iput-object v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->PPPOE_STATE_ACTION:Ljava/lang/String;

    const-string v0, "PppoeStatus"

    iput-object v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->PPPOE_STATE_STATUE:Ljava/lang/String;

    const-string v0, "connect"

    iput-object v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->PPPOE_STATE_CONNECT:Ljava/lang/String;

    const-string v0, "disconnect"

    iput-object v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->PPPOE_STATE_DISCONNECT:Ljava/lang/String;

    const-string v0, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    iput-object v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->ETHERNET_STATE_CHANGED_ACTION:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->EVENT_INTERFACE_CONFIGURATION_SUCCEEDED:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->EVENT_INTERFACE_CONFIGURATION_FAILED:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->EVENT_HW_CONNECTED:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->EVENT_HW_DISCONNECTED:I

    iput-boolean v1, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mPppoeFlag:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mEthernetFlag:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mWifiFlag:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v8, "android.net.pppoe.PPPOE_STATE_ACTION"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "PppoeStatus"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "connect"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iput-boolean v6, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mPppoeFlag:Z

    :cond_0
    :goto_0
    iget-boolean v6, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mWifiFlag:Z

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mEthernetFlag:Z

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mPppoeFlag:Z

    if-nez v6, :cond_1

    new-instance v4, Landroid/content/Intent;

    const-string v6, "com.mstar.localmm.network.disconnect"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_1
    return-void

    :cond_2
    const-string v6, "disconnect"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iput-boolean v7, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mPppoeFlag:Z

    goto :goto_0

    :cond_3
    const-string v8, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "ETHERNET_state"

    invoke-virtual {p2, v8, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput-boolean v6, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mEthernetFlag:Z

    goto :goto_0

    :pswitch_1
    iput-boolean v7, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mEthernetFlag:Z

    goto :goto_0

    :cond_4
    const-string v8, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "networkInfo"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/NetworkInfo;

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_5

    move v2, v6

    :goto_1
    if-eqz v2, :cond_6

    iput-boolean v6, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mWifiFlag:Z

    goto :goto_0

    :cond_5
    move v2, v7

    goto :goto_1

    :cond_6
    iput-boolean v7, p0, Lcom/jrm/localmm/util/NetworkStatusChangeReceiver;->mWifiFlag:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
