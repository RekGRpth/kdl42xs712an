.class final Lcom/jrm/localmm/business/data/BaseData$1;
.super Ljava/lang/Object;
.source "BaseData.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/data/BaseData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/jrm/localmm/business/data/BaseData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/jrm/localmm/business/data/BaseData;
    .locals 3
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/jrm/localmm/business/data/BaseData;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->name:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->access$002(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->path:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->access$102(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->size:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->access$202(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->format:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->access$302(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->artist:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->access$402(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->modifyTime:J
    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/business/data/BaseData;->access$502(Lcom/jrm/localmm/business/data/BaseData;J)J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->id:J
    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/business/data/BaseData;->access$602(Lcom/jrm/localmm/business/data/BaseData;J)J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->album:J
    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/business/data/BaseData;->access$702(Lcom/jrm/localmm/business/data/BaseData;J)J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    # setter for: Lcom/jrm/localmm/business/data/BaseData;->_duration:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/data/BaseData;->access$802(Lcom/jrm/localmm/business/data/BaseData;I)I

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/business/data/BaseData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/jrm/localmm/business/data/BaseData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/jrm/localmm/business/data/BaseData;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/jrm/localmm/business/data/BaseData;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/business/data/BaseData$1;->newArray(I)[Lcom/jrm/localmm/business/data/BaseData;

    move-result-object v0

    return-object v0
.end method
