.class Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;
.super Ljava/lang/Object;
.source "DataAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/adapter/DataAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyOnHoverListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/adapter/DataAdapter;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/adapter/DataAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;->this$0:Lcom/jrm/localmm/business/adapter/DataAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    iget-object v4, p0, Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;->this$0:Lcom/jrm/localmm/business/adapter/DataAdapter;

    # getter for: Lcom/jrm/localmm/business/adapter/DataAdapter;->itemHeight:I
    invoke-static {v4}, Lcom/jrm/localmm/business/adapter/DataAdapter;->access$100(Lcom/jrm/localmm/business/adapter/DataAdapter;)I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;->this$0:Lcom/jrm/localmm/business/adapter/DataAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    # setter for: Lcom/jrm/localmm/business/adapter/DataAdapter;->itemHeight:I
    invoke-static {v4, v5}, Lcom/jrm/localmm/business/adapter/DataAdapter;->access$102(Lcom/jrm/localmm/business/adapter/DataAdapter;I)I

    :cond_0
    packed-switch v3, :pswitch_data_0

    :goto_0
    const/4 v4, 0x0

    return v4

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;->this$0:Lcom/jrm/localmm/business/adapter/DataAdapter;

    # getter for: Lcom/jrm/localmm/business/adapter/DataAdapter;->itemHeight:I
    invoke-static {v5}, Lcom/jrm/localmm/business/adapter/DataAdapter;->access$100(Lcom/jrm/localmm/business/adapter/DataAdapter;)I

    move-result v5

    div-int v2, v4, v5

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "adapter.position"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v4, 0x13

    iput v4, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;->this$0:Lcom/jrm/localmm/business/adapter/DataAdapter;

    # getter for: Lcom/jrm/localmm/business/adapter/DataAdapter;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/jrm/localmm/business/adapter/DataAdapter;->access$200(Lcom/jrm/localmm/business/adapter/DataAdapter;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method
