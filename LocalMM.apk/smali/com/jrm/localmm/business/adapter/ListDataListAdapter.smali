.class public Lcom/jrm/localmm/business/adapter/ListDataListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ListDataListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private holder:Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;

.field private mInflater:Landroid/view/LayoutInflater;

.field private optionData:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    :try_start_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->optionData:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->optionData:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v3, 0x7f080034    # com.jrm.localmm.R.id.ListNameTextView

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030008    # com.jrm.localmm.R.layout.list_data_list_adapter

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->holder:Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->holder:Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->holder:Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->holder:Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->optionData:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/ListDataListAdapter;->holder:Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/ListDataListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    goto :goto_0
.end method
