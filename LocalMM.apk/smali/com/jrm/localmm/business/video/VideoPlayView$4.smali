.class Lcom/jrm/localmm/business/video/VideoPlayView$4;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v1, 0x5

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$402(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$802(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1100(Lcom/jrm/localmm/business/video/VideoPlayView;)Landroid/media/AudioManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*********DualAudioOff******onCompletion***"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1100(Lcom/jrm/localmm/business/video/VideoPlayView;)Landroid/media/AudioManager;

    move-result-object v0

    const-string v1, "DualAudioOff"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$4;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;->onCompletion(Landroid/media/MediaPlayer;I)V

    :cond_1
    return-void
.end method
