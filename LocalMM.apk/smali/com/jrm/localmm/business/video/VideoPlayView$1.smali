.class Lcom/jrm/localmm/business/video/VideoPlayView$1;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*****stop start*****"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->stop()V

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*****stop end*****"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "***stopPlayer()**release start*****"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "***stopPlayer()**release end*****"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$1;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v1, 0x0

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$002(Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/mstar/android/media/MMediaPlayer;)Lcom/mstar/android/media/MMediaPlayer;

    return-void
.end method
