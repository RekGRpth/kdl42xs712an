.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_ThreeD_Video_SELFADAPTIVE_DETECT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

.field public static final enum DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

.field public static final enum DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_RIGHT_NOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

.field public static final enum DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    const-string v1, "DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    const-string v1, "DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_RIGHT_NOW"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_RIGHT_NOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    const-string v1, "DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_COUNT"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_RIGHT_NOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
