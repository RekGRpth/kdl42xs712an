.class Lcom/konka/kkvideoplayer/VideoPlayerMain$3;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->bTeacCustomer:Z
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$20(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v2

    if-nez v2, :cond_4

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return v5

    :sswitch_0
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyBack()Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$21(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_1 # KeyEvent.KEYCODE_MENU
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_long_press

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyMenu()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$22(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :cond_long_press
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v3, 0x0

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPopAwindow(Landroid/view/View;)V
    invoke-static {v2, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$10(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/view/View;)V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$23(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "eeeeeeeee progress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    add-int/lit8 v3, v1, -0x32

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V
    invoke-static {v2, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$24(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$23(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "eeeeeeeee progress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    add-int/lit8 v3, v1, 0x32

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V
    invoke-static {v2, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$24(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :sswitch_4
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Play/Pause"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$25(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v2

    if-eq v2, v4, :cond_0

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$26(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$27(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    :cond_1
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePauseKey()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$28(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :sswitch_5
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key stop"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->bMute:Z
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$29(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$30(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/media/AudioManager;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$30(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/media/AudioManager;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currentVolume:I
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$31(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v3

    invoke-virtual {v2, v6, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_2
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "hello world destroy!!! stop"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_3
    const-string v2, "hello world will exec finish 2"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    goto/16 :goto_0

    :sswitch_6
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Prev"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePreviousKey()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$11(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :sswitch_7
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Next"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleNextKey()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$12(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :sswitch_8
    const-string v2, "KEYCODE_3D_MODE onpop"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v2, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$32(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$27(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :cond_4
    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyBack()Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$21(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyMenu()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$22(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :sswitch_b
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Play/Pause"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$25(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v2

    if-eq v2, v4, :cond_0

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$26(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$27(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    :cond_5
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePauseKey()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$28(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :sswitch_c
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key stop"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->bMute:Z
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$29(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$30(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/media/AudioManager;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$30(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/media/AudioManager;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currentVolume:I
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$31(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v3

    invoke-virtual {v2, v6, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_6
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "hello world destroy!!! stop"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_7
    const-string v2, "hello world will exec finish 2"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Prev"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePreviousKey()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$11(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Next"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleNextKey()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$12(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "KEYCODE_3D_MODE onpop"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v2, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$32(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$27(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb9 -> :sswitch_2
        0xba -> :sswitch_3
        0xce -> :sswitch_8
        0x106 -> :sswitch_7
        0x12e -> :sswitch_4
        0x12f -> :sswitch_5
        0x133 -> :sswitch_6
        0x205 -> :sswitch_8
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x4 -> :sswitch_9
        0x52 -> :sswitch_a
        0x56 -> :sswitch_c
        0x7e -> :sswitch_b
        0xce -> :sswitch_f
        0x12e -> :sswitch_e
        0x132 -> :sswitch_d
        0x205 -> :sswitch_f
    .end sparse-switch
.end method
