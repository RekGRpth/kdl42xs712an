.class public Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EthStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method public constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isConneted2Network(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$35(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v0

    const/16 v1, 0x66

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v1, 0x7f0a001c    # com.konka.kkvideoplayer.R.string.net_check_disconnect

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    :cond_0
    return-void
.end method
