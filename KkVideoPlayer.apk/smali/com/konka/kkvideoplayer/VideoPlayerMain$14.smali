.class Lcom/konka/kkvideoplayer/VideoPlayerMain$14;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initVideoPlay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 9
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const v6, 0x7f0a0028    # com.konka.kkvideoplayer.R.string.Your_File_Format_not_support

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v5, "VideoPlayActivity.onInfo !!!!!!!!!!!!!!!!!!!!!!"

    invoke-static {v5}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    sparse-switch p2, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkvideoplayer/VideoView;->getSubtitleData()Ljava/lang/String;

    move-result-object v4

    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v5, "UTF-16"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "VideoPlayActivity.onInfo getSubtitleData  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    if-ne p3, v8, :cond_1

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkvideoplayer/VideoView;->getSubtitleData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v8, :cond_1

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubTitleText(Ljava/lang/String;)V
    invoke-static {v5, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$66(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V

    :cond_1
    if-nez p3, :cond_0

    const-string v4, ""

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubTitleText(Ljava/lang/String;)V
    invoke-static {v5, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$66(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V

    const-string v5, "tank"

    const-string v6, "\u5b57\u5e55\u9690\u85cf\uff1a\uff1a\uff1a\uff1a\uff1a\uff1a\uff1a\uff1a\uff1a\uff1a"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    :sswitch_2
    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v5, v7}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$73(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildHintDialog(I)V
    invoke-static {v5, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$60(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :sswitch_3
    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v5, v7}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$74(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildHintDialog(I)V
    invoke-static {v5, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$60(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :sswitch_4
    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a001b    # com.konka.kkvideoplayer.R.string.seekdisable

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    const/16 v5, 0x11

    invoke-virtual {v2, v5, v7, v7}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_0
        0x321 -> :sswitch_4
        0x3e8 -> :sswitch_1
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_3
    .end sparse-switch
.end method
