.class Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BroadCastPop"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "=====pop reciver:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.hotkey.enablePop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v1, 0x1

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->popSwitch(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$38(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.hotkey.disablePop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v1, 0x2

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->popSwitch(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$38(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.hotkey.enablePip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.konka.hotkey.enablePip"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v1, 0x3

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->popSwitch(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$38(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->fixScreenToFullForMstarBug()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$39(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.hotkey.disablePip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "com.konka.hotkey.disablePip"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v1, 0x4

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->popSwitch(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$38(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setOldScreen()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$40(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.hotkey.enable3dDualView"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "com.konka.hotkey.enable3dDualView\n"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->fixScreenToFullForMstarBug()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$39(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.hotkey.disabledDualView"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setOldScreen()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$40(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0
.end method
