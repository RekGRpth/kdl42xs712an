.class public Lcom/konka/kkvideoplayer/MediaError;
.super Ljava/lang/Object;
.source "MediaError.java"


# static fields
.field public static final DRM_ERROR_BASE:I = -0x7d0

.field public static final ERROR_ALREADY_CONNECTED:I = -0x3e8

.field public static final ERROR_AUDIO_UNSUPPORT:I = -0x1389

.field public static final ERROR_BUFFER_TOO_SMALL:I = -0x3f1

.field public static final ERROR_CANNOT_CONNECT:I = -0x3eb

.field public static final ERROR_CONNECTED_TIMEOUT:I = -0x1388

.field public static final ERROR_CONNECTION_LOST:I = -0x3ed

.field public static final ERROR_DRM_CANNOT_HANDLE:I = -0x7d6

.field public static final ERROR_DRM_DECRYPT:I = -0x7d5

.field public static final ERROR_DRM_DECRYPT_UNIT_NOT_INITIALIZED:I = -0x7d4

.field public static final ERROR_DRM_LICENSE_EXPIRED:I = -0x7d2

.field public static final ERROR_DRM_NO_LICENSE:I = -0x7d1

.field public static final ERROR_DRM_SESSION_NOT_OPENED:I = -0x7d3

.field public static final ERROR_DRM_TAMPER_DETECTED:I = -0x7d7

.field public static final ERROR_DRM_UNKNOWN:I = -0x7d0

.field public static final ERROR_END_OF_STREAM:I = -0x3f3

.field public static final ERROR_FILE_FORMAT_UNSUPPORT:I = -0x138b

.field public static final ERROR_HEARTBEAT_AUTHENTICATION_FAILURE:I = -0xbb8

.field public static final ERROR_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I = -0xbbd

.field public static final ERROR_HEARTBEAT_CONCURRENT_PLAYBACK:I = -0xbba

.field public static final ERROR_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I = -0xbb9

.field public static final ERROR_HEARTBEAT_STREAMING_UNAVAILABLE:I = -0xbbc

.field public static final ERROR_HEARTBEAT_TERMINATE_REQUESTED:I = -0xbbe

.field public static final ERROR_HEARTBEAT_UNUSUAL_ACTIVITY:I = -0xbbb

.field public static final ERROR_IO:I = -0x3ec

.field public static final ERROR_MALFORMED:I = -0x3ef

.field public static final ERROR_NOT_CONNECTED:I = -0x3e9

.field public static final ERROR_OUT_OF_RANGE:I = -0x3f0

.field public static final ERROR_UNKNOWN_HOST:I = -0x3ea

.field public static final ERROR_UNSUPPORTED:I = -0x3f2

.field public static final ERROR_VIDEO_UNSUPPORT:I = -0x138a

.field public static final HEARTBEAT_ERROR_BASE:I = -0xbb8

.field public static final INFO_DISCONTINUITY:I = -0x3f5

.field public static final INFO_FORMAT_CHANGED:I = -0x3f4

.field public static final MEDIA_ERROR_BASE:I = -0x3e8

.field public static final USER_ERROR_BASE:I = -0x1388


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
