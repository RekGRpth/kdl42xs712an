.class public final Lcom/konka/kkvideoplayer/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final behind_default:I = 0x7f020000

.field public static final blankabove_commend:I = 0x7f020001

.field public static final bottom_bar_bg:I = 0x7f020002

.field public static final btv_appname:I = 0x7f020003

.field public static final button_box_app_select:I = 0x7f020004

.field public static final button_box_app_un:I = 0x7f020005

.field public static final button_box_blog_select:I = 0x7f020006

.field public static final button_box_blog_un:I = 0x7f020007

.field public static final button_box_news_select:I = 0x7f020008

.field public static final button_box_news_un:I = 0x7f020009

.field public static final button_box_video_select:I = 0x7f02000a

.field public static final button_box_video_un:I = 0x7f02000b

.field public static final button_style_arrange:I = 0x7f02000c

.field public static final button_style_mute_disable:I = 0x7f02000d

.field public static final button_style_mute_enable:I = 0x7f02000e

.field public static final button_style_next:I = 0x7f02000f

.field public static final button_style_pause:I = 0x7f020010

.field public static final button_style_play:I = 0x7f020011

.field public static final button_style_prev:I = 0x7f020012

.field public static final button_style_setting:I = 0x7f020013

.field public static final button_style_setting_track:I = 0x7f020014

.field public static final button_style_stop:I = 0x7f020015

.field public static final button_style_volume_down:I = 0x7f020016

.field public static final button_style_volume_up:I = 0x7f020017

.field public static final cab_divider_vertical_dark:I = 0x7f020018

.field public static final camera_crop_holo:I = 0x7f020019

.field public static final com_bg:I = 0x7f02001a

.field public static final com_bg_03:I = 0x7f02001b

.field public static final com_bg_3:I = 0x7f02001c

.field public static final com_bg_bottom:I = 0x7f02001d

.field public static final com_bg_bottom_list:I = 0x7f02001e

.field public static final com_bg_popmenu1_01:I = 0x7f02001f

.field public static final com_bg_top:I = 0x7f020020

.field public static final com_button_arrange_s:I = 0x7f020021

.field public static final com_button_arrange_uns:I = 0x7f020022

.field public static final com_button_pause_s:I = 0x7f020023

.field public static final com_button_pause_un:I = 0x7f020024

.field public static final com_button_play_s:I = 0x7f020025

.field public static final com_button_play_uns:I = 0x7f020026

.field public static final com_button_sortord_s:I = 0x7f020027

.field public static final com_button_sortord_uns:I = 0x7f020028

.field public static final com_default_nextpage_images:I = 0x7f020029

.field public static final com_play_next_s:I = 0x7f02002a

.field public static final com_play_next_uns:I = 0x7f02002b

.field public static final com_play_pause_s:I = 0x7f02002c

.field public static final com_play_pause_uns:I = 0x7f02002d

.field public static final com_play_prev_s:I = 0x7f02002e

.field public static final com_play_prev_uns:I = 0x7f02002f

.field public static final com_play_stop_s:I = 0x7f020030

.field public static final com_play_stop_uns:I = 0x7f020031

.field public static final com_play_voice_s:I = 0x7f020032

.field public static final com_play_voice_s1:I = 0x7f020033

.field public static final com_play_voice_s2:I = 0x7f020034

.field public static final com_play_voice_s_1:I = 0x7f020035

.field public static final com_play_voice_uns:I = 0x7f020036

.field public static final com_play_voice_uns1:I = 0x7f020037

.field public static final com_play_voice_uns2:I = 0x7f020038

.field public static final com_play_voice_uns_1:I = 0x7f020039

.field public static final com_progressbar_arrived_all_s:I = 0x7f02003a

.field public static final com_progressbar_arrived_all_uns:I = 0x7f02003b

.field public static final com_progressbar_arrived_button_s:I = 0x7f02003c

.field public static final com_progressbar_arrived_button_uns:I = 0x7f02003d

.field public static final com_progressbar_arrived_s1:I = 0x7f02003e

.field public static final com_progressbar_arrived_s2:I = 0x7f02003f

.field public static final com_progressbar_arrived_s3:I = 0x7f020040

.field public static final com_progressbar_arrived_uns1:I = 0x7f020041

.field public static final com_progressbar_arrived_uns2:I = 0x7f020042

.field public static final com_progressbar_arrived_uns3:I = 0x7f020043

.field public static final com_progressbar_default1:I = 0x7f020044

.field public static final com_progressbar_default2:I = 0x7f020045

.field public static final com_progressbar_default3:I = 0x7f020046

.field public static final com_progressbar_default_all:I = 0x7f020047

.field public static final com_select_rim:I = 0x7f020048

.field public static final com_separator1:I = 0x7f020049

.field public static final com_set_s:I = 0x7f02004a

.field public static final com_set_uns:I = 0x7f02004b

.field public static final com_stat_pause:I = 0x7f02004c

.field public static final commend_backg:I = 0x7f02004d

.field public static final commend_behind:I = 0x7f02004e

.field public static final commend_content:I = 0x7f02004f

.field public static final commend_forward:I = 0x7f020050

.field public static final commend_item_default:I = 0x7f020051

.field public static final commend_item_select:I = 0x7f020052

.field public static final commend_line_select:I = 0x7f020053

.field public static final commend_month_title:I = 0x7f020054

.field public static final commend_title:I = 0x7f020055

.field public static final commend_title_noselect:I = 0x7f020056

.field public static final commend_title_select:I = 0x7f020057

.field public static final commend_titlehalf_select:I = 0x7f020058

.field public static final dropdown_normal_holo_dark:I = 0x7f020059

.field public static final explore_selected:I = 0x7f02005a

.field public static final focus_box:I = 0x7f02005b

.field public static final grid_pressed:I = 0x7f02005c

.field public static final grid_selected:I = 0x7f02005d

.field public static final ic_launcher:I = 0x7f02005e

.field public static final icon_localvideo_default:I = 0x7f02005f

.field public static final leftbackgroud_noselect:I = 0x7f020060

.field public static final leftbackgroud_select:I = 0x7f020061

.field public static final main_rb_bg_style:I = 0x7f020062

.field public static final navigation_default:I = 0x7f020063

.field public static final navigation_halfselect:I = 0x7f020064

.field public static final navigation_select:I = 0x7f020065

.field public static final pagedown:I = 0x7f020066

.field public static final pageup:I = 0x7f020067

.field public static final pop_bg_bottom:I = 0x7f020068

.field public static final pop_bg_top:I = 0x7f020069

.field public static final popup_full_dark:I = 0x7f02006a

.field public static final right_bg_noselect:I = 0x7f02006b

.field public static final right_bg_select:I = 0x7f02006c

.field public static final right_bg_tv_noselect:I = 0x7f02006d

.field public static final right_bg_tv_select:I = 0x7f02006e

.field public static final scrubber_knob:I = 0x7f02006f

.field public static final seekbar_thumb_style:I = 0x7f020070

.field public static final selected:I = 0x7f020071

.field public static final selected_item:I = 0x7f020072

.field public static final selected_item_bestv:I = 0x7f020073

.field public static final sport:I = 0x7f020074

.field public static final text_color_selector:I = 0x7f020075

.field public static final thumb_selected:I = 0x7f020076

.field public static final title_select:I = 0x7f020077

.field public static final video_bar:I = 0x7f020078

.field public static final video_icon_set_bg:I = 0x7f020079

.field public static final video_icon_set_model_cycle_s:I = 0x7f02007a

.field public static final video_icon_set_model_image_s:I = 0x7f02007b

.field public static final video_icon_set_model_scale_s:I = 0x7f02007c

.field public static final video_icon_set_model_subtitles_s:I = 0x7f02007d

.field public static final video_icon_set_model_track_s:I = 0x7f02007e

.field public static final video_icon_set_select_bar:I = 0x7f02007f

.field public static final video_icon_set_select_button_s:I = 0x7f020080

.field public static final video_icon_set_select_button_uns:I = 0x7f020081

.field public static final video_progress_style:I = 0x7f020082

.field public static final view_style_leftbg:I = 0x7f020083

.field public static final view_style_right_tv:I = 0x7f020084

.field public static final view_style_rightbg:I = 0x7f020085

.field public static final widget_app_select:I = 0x7f020086

.field public static final widget_app_top:I = 0x7f020087

.field public static final widget_app_unselect:I = 0x7f020088

.field public static final widget_blog_select:I = 0x7f020089

.field public static final widget_blog_top:I = 0x7f02008a

.field public static final widget_blog_unselect:I = 0x7f02008b

.field public static final widget_news_select:I = 0x7f02008c

.field public static final widget_news_top:I = 0x7f02008d

.field public static final widget_news_unselect:I = 0x7f02008e

.field public static final widget_style_app:I = 0x7f02008f

.field public static final widget_style_blog:I = 0x7f020090

.field public static final widget_style_news:I = 0x7f020091

.field public static final widget_style_video:I = 0x7f020092

.field public static final widget_video_select:I = 0x7f020093

.field public static final widget_video_top:I = 0x7f020094

.field public static final widget_video_unselect:I = 0x7f020095


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
