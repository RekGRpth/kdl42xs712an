.class Lcom/konka/kkvideoplayer/VideoPlayerMain$9;
.super Landroid/os/Handler;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initHandle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x0

    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :sswitch_0
    const/4 v1, 0x0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setProgress()I
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$48(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v1

    invoke-virtual {p0, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    rem-int/lit16 v3, v1, 0x3e8

    rsub-int v3, v3, 0x3e8

    int-to-long v3, v3

    invoke-virtual {p0, p1, v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :sswitch_1
    const-string v3, "CMD_MYHANDLER_DRAW_MEHU!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->drawMenuShow()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$49(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :sswitch_2
    const-string v3, "CMD_MYHANDLER_HIDE_MENU!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$27(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :sswitch_3
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildVideoDataSource()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$50(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :sswitch_4
    const-string v3, "CMD_MYHANDLER_MEDIA_SCAN_START!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$51(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :sswitch_5
    const-string v3, "CMD_MYHANDLER_MEDIA_SCAN_FINISH!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v3, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$51(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :sswitch_6
    const-string v3, "USB_UNMOUNT_START!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isFileExists(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "USB_file is not exist!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildVideoDataSource()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$50(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :sswitch_7
    const-string v3, "CMD_USB_MOUNT_START!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildVideoDataSource()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$50(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto :goto_0

    :sswitch_8
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePlay:Ljava/lang/Boolean;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$52(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    const-wide/16 v3, 0x1f4

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePoint:J
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$53(Lcom/konka/kkvideoplayer/VideoPlayerMain;)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/konka/kkvideoplayer/VideoView;->seekTo(I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$54(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/Boolean;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CMD_MYHANDLER_PREPARE_FINISH!!!!!!!!!!!!!! sleep seek "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v4

    iget-wide v4, v4, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_1
    const-wide/16 v3, 0x1f4

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getImageMode()I

    move-result v4

    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$55(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$56(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setImageMode(I)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " getImageMode:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$56(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v4, 0x7f09001f    # com.konka.kkvideoplayer.R.id.layout_stat_play

    invoke-virtual {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    :sswitch_9
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->initWidget()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$57(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$58(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->hide()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_3
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_8
        0xe -> :sswitch_9
        0xf -> :sswitch_a
        0x12c -> :sswitch_7
        0x12d -> :sswitch_6
    .end sparse-switch
.end method
