.class public Lcom/konka/kkvideoplayer/util/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static AUDIO_PROJECTION:[Ljava/lang/String; = null

.field public static final AUDIO_URI:Landroid/net/Uri;

.field public static final CLEAR_DATA:Ljava/lang/String; = "com.mstar.localmm.cleardata"

.field public static final CLICK_VIDEO_INFO:I = 0x2

.field public static final CREATE_TABLE_MUSIC_PLAY_LIST:Ljava/lang/String; = "create table music_play_list(music_id int not null,display_name nvarchar(40) not null,title nvarchar(40) not null,singer_name nvarchar(40) not null,path nvarchar(40) not null,album nvarchar(40) not null,size int,duration int,constraint PK_IBS_USER_INF primary key (music_id))"

.field public static final CREATE_TABLE_VIDEO_PLAY_LIST:Ljava/lang/String; = "create table video_play_list(id int not null,video_id int not null,display_name nvarchar(40) not null,title nvarchar(40) not null,path nvarchar(40) not null,size int,duration int,type int,constraint PK_IBS_USER_INF primary key (id))"

.field public static final DATABASE_NAME:Ljava/lang/String; = "localmm.db"

.field public static final DATABASE_VERSION:I = 0x5

.field public static IMAGE_PROJECTION:[Ljava/lang/String; = null

.field public static final IMAGE_URI:Landroid/net/Uri;

.field public static final LIST_COLUMNS_ALL:[Ljava/lang/String;

.field public static final MUSIC_ALBUM:Ljava/lang/String; = "album"

.field public static final MUSIC_DATA_TYPE:S = 0x1s

.field public static final MUSIC_DISPLAY_NAME:Ljava/lang/String; = "display_name"

.field public static final MUSIC_DURATION:Ljava/lang/String; = "duration"

.field public static final MUSIC_ID:Ljava/lang/String; = "music_id"

.field public static final MUSIC_LIST_COLUMNS_ALL:[Ljava/lang/String;

.field public static final MUSIC_PATH:Ljava/lang/String; = "path"

.field public static final MUSIC_PLAY_LIST_TABLE_NAME:Ljava/lang/String; = "music_play_list"

.field public static final MUSIC_SINGER_NAME:Ljava/lang/String; = "singer_name"

.field public static final MUSIC_SIZE:Ljava/lang/String; = "size"

.field public static final MUSIC_TITLE:Ljava/lang/String; = "title"

.field public static final OTHER_DATA_TYPE:S = 0x4s

.field public static final OTHER_FILTER_TYPE:[Ljava/lang/String;

.field public static final PDF_TYPE:S = 0x1s

.field public static final PHOTO_DATA_TYPE:S = 0x5s

.field public static final PHOTO_FOLDER_DATA_TYPE:S = 0x3s

.field public static final REFRESH_DATA:Ljava/lang/String; = "com.mstar.localmm.refresh"

.field public static final STARTED:Ljava/lang/String; = "com.mstar.localmm.started"

.field public static final SettingDialogArgs:Ljava/lang/String; = "settingDialog"

.field public static final TXT_TYPE:S = 0x2s

.field public static final VIDEO_DATA_TYPE:S = 0x2s

.field public static final VIDEO_FOLDER_DATA_TYPE:S = 0x6s

.field public static final VIDEO_FULLSCREEN_OR_DEFAULT:I = 0xa

.field public static final VIDEO_INFO:I = 0x9

.field public static final VIDEO_LIST:I = 0x8

.field public static final VIDEO_NEXT:I = 0x3

.field public static final VIDEO_PLAYLIST_DISPLAYNAME:Ljava/lang/String; = "display_name"

.field public static final VIDEO_PLAYLIST_DURATION:Ljava/lang/String; = "duration"

.field public static final VIDEO_PLAYLIST_ID:Ljava/lang/String; = "id"

.field public static final VIDEO_PLAYLIST_PATH:Ljava/lang/String; = "path"

.field public static final VIDEO_PLAYLIST_SIZE:Ljava/lang/String; = "size"

.field public static final VIDEO_PLAYLIST_TITLE:Ljava/lang/String; = "title"

.field public static final VIDEO_PLAYLIST_VIDEO_ID:Ljava/lang/String; = "video_id"

.field public static final VIDEO_PLAY_LIST_TABLE_NAME:Ljava/lang/String; = "video_play_list"

.field public static final VIDEO_PLAY_OR_PAUSE:I = 0x1

.field public static final VIDEO_PRE:I = 0x2

.field public static VIDEO_PROJECTION:[Ljava/lang/String; = null

.field public static final VIDEO_REWIND:I = 0x4

.field public static final VIDEO_TYPE:Ljava/lang/String; = "type"

.field public static final VIDEO_URI:Landroid/net/Uri;

.field public static final VIDEO_VOLUME_DEC:I = 0x7

.field public static final VIDEO_VOLUME_INC:I = 0x6

.field public static final VIDEO_WIND:I = 0x5

.field public static final VIDEO_WORD:I = 0xb

.field public static isMusicPlaying:Z

.field public static isPlayerVisibility:Z

.field public static playFromFlag:Z

.field public static playListPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->IMAGE_URI:Landroid/net/Uri;

    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->AUDIO_URI:Landroid/net/Uri;

    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->VIDEO_URI:Landroid/net/Uri;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "duration"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "_display_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->AUDIO_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "datetaken"

    aput-object v1, v0, v5

    const-string v1, "_display_name"

    aput-object v1, v0, v6

    const-string v1, "duration"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mini_thumb_magic"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->VIDEO_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_size"

    aput-object v1, v0, v5

    const-string v1, "_display_name"

    aput-object v1, v0, v6

    const-string v1, "mime_type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "date_added"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bucket_display_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->IMAGE_PROJECTION:[Ljava/lang/String;

    sput-boolean v3, Lcom/konka/kkvideoplayer/util/Constants;->isPlayerVisibility:Z

    sput-boolean v3, Lcom/konka/kkvideoplayer/util/Constants;->playFromFlag:Z

    sput-boolean v3, Lcom/konka/kkvideoplayer/util/Constants;->isMusicPlaying:Z

    sput v3, Lcom/konka/kkvideoplayer/util/Constants;->playListPosition:I

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, ".txt"

    aput-object v1, v0, v3

    const-string v1, ".pdf"

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->OTHER_FILTER_TYPE:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "id"

    aput-object v1, v0, v3

    const-string v1, "video_id"

    aput-object v1, v0, v4

    const-string v1, "display_name"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    const-string v1, "path"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->LIST_COLUMNS_ALL:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "music_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "singer_name"

    aput-object v1, v0, v6

    const-string v1, "path"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "size"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkvideoplayer/util/Constants;->MUSIC_LIST_COLUMNS_ALL:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
