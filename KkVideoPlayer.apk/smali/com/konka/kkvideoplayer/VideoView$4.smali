.class Lcom/konka/kkvideoplayer/VideoView$4;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoView;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoView$4;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$4;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$15(Lcom/konka/kkvideoplayer/VideoView;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$4;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$7(Lcom/konka/kkvideoplayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$4;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$7(Lcom/konka/kkvideoplayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$4;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$16(Lcom/konka/kkvideoplayer/VideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$4;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$16(Lcom/konka/kkvideoplayer/VideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoView$4;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-interface {v0, v1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_1
    return v3
.end method
