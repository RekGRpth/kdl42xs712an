.class Lcom/konka/kkvideoplayer/VideoView$7;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoView;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-static {v0, p1}, Lcom/konka/kkvideoplayer/VideoView;->access$19(Lcom/konka/kkvideoplayer/VideoView;Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # invokes: Lcom/konka/kkvideoplayer/VideoView;->openVideo()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$20(Lcom/konka/kkvideoplayer/VideoView;)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->access$19(Lcom/konka/kkvideoplayer/VideoView;Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$7(Lcom/konka/kkvideoplayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$7(Lcom/konka/kkvideoplayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->reset()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$7;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    iput-object v1, v0, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    :cond_1
    return-void
.end method
