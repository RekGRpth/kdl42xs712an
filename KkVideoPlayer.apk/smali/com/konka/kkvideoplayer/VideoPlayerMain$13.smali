.class Lcom/konka/kkvideoplayer/VideoPlayerMain$13;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initVideoPlay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/kkvideoplayer/VideoPlayerMain$13;)Lcom/konka/kkvideoplayer/VideoPlayerMain;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    return-object v0
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 8
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$36(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->hide()V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIfFBScreenModeSet()I
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$61(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v3, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$62(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    :cond_0
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, v4, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    if-ne v3, v4, :cond_4

    const-string v3, "frontChannel == doubleChannel !!!!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v3, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$62(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    :goto_0
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubtitleLayout()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$63(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$64(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v4

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V
    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$65(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$47(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$23(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const-string v4, ""

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubTitleText(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$66(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$4(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/Button;

    move-result-object v3

    const v4, 0x7f020010    # com.konka.kkvideoplayer.R.drawable.button_style_pause

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkvideoplayer/VideoView;->start()V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "START setOnPreparedListener !!!currentVideoScale "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$64(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->resetSensorSetting()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$67(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    new-instance v0, Lcom/konka/kkvideoplayer/SubtitleTool;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-direct {v0, v3}, Lcom/konka/kkvideoplayer/SubtitleTool;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/SubtitleTool;->getSubtitlePath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkvideoplayer/VideoView;->onSubtitleTrack()V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$68(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleTrack(I)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleDataSource(Ljava/lang/String;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$69(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "strSubtitleDataSources.length() > 0,is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$70(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleTrack(I)V

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/SubtitleTool;->getCuurentSubtitleType()I

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_5

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$71(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/Boolean;)V

    :goto_1
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkvideoplayer/VideoView;->offSubtitleTrack()V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->clearPauseStat()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$5(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$47(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIfFBScreenModeSet()I
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$61(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v3, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$62(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$64(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v4

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V
    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$65(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMVCSource()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const-string v4, "SetMVC3DFlagTrue"

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setFullScreenWithoutSave()V
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$41(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$72(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/S3DDesk;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v3

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$72(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/S3DDesk;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v3

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMVCSource()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "isIn4K2KMode"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getValueByTvosCommon(Ljava/lang/String;)S

    move-result v3

    if-eq v7, v3, :cond_2

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/konka/kkvideoplayer/VideoPlayerMain$13$1;

    invoke-direct {v4, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$13$1;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain$13;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    :cond_2
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$72(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/S3DDesk;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v3

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$72(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/S3DDesk;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v3

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMVCSource()Z

    :cond_3
    return-void

    :cond_4
    const-string v3, "frontChannel == singlechannel !!!!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$71(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    :cond_6
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleDataSource(Ljava/lang/String;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$69(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    const-string v3, "vv.setSubtitleDataSource(null);"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleTrack(I)V

    goto/16 :goto_1

    :cond_7
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const-string v4, "SetMVC3DFlagFalse"

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_8
    :try_start_2
    const-string v3, "TvManger.getInstance() == null"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2
.end method
