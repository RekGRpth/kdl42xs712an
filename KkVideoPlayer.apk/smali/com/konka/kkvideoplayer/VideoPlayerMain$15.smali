.class Lcom/konka/kkvideoplayer/VideoPlayerMain$15;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initVideoPlay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    const-string v1, "oncompletion !!!!!!!!!!"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const-string v2, ""

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubTitleText(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$66(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$75(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->getNextVideo(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$76(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "oncompletion  null"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "oncompletion  get next"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getPath()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPlayParam(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$77(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$36(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    const-string v1, "oncompletion  get next 2"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$75(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPlayParam(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$77(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$36(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$75(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->getNextVideo(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$76(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v1, "bestVideo  is null !!!!!!!!!!!!! finish "

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getPath()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPlayParam(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$77(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$36(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
