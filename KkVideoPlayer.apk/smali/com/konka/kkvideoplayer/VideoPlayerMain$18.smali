.class Lcom/konka/kkvideoplayer/VideoPlayerMain$18;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenScreenMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const v6, 0x7f0a001f    # com.konka.kkvideoplayer.R.string.hint_mode_pip_inscreen_mode

    const v5, 0x7f0a001e    # com.konka.kkvideoplayer.R.string.hint_mode_3d_inscreen_mode

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIfFBScreenModeSet()I
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$61(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return v3

    :pswitch_0
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$80(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "setOnKeyListener PIP \u6a21\u5f0f\n"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v1, v6, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    if-ne v0, v2, :cond_2

    const-string v1, "setOnKeyListener 3d \u6a21\u5f0f\n"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v1, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    if-ne v0, v4, :cond_3

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v2, 0x7f0a0020    # com.konka.kkvideoplayer.R.string.hint_mode_4k_inscreen_mode

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/16 v2, 0x100

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleScreenModeKey(I)V
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$81(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$80(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "setOnKeyListener PIP \u6a21\u5f0f\n"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v1, v6, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    if-ne v0, v2, :cond_5

    const-string v1, "setOnKeyListener 3d \u6a21\u5f0f\n"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v1, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    if-ne v0, v4, :cond_6

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v2, 0x7f0a0020    # com.konka.kkvideoplayer.R.string.hint_mode_4k_inscreen_mode

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/16 v2, 0x200

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleScreenModeKey(I)V
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$81(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
