.class Lcom/konka/kkvideoplayer/VideoPlayerMain$24;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenCircleMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$24;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$24;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/16 v1, 0x100

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleCircleModeKey(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$86(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    const-string v0, "==============KEYCODE_DPAD_LEFT"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$24;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/16 v1, 0x200

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleCircleModeKey(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$86(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    const-string v0, "==============KEYCODE_DPAD_RIGHT"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
