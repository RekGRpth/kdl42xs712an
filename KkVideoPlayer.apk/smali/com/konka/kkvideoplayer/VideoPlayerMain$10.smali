.class Lcom/konka/kkvideoplayer/VideoPlayerMain$10;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public queueIdle()Z
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideSettingMenu()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$59(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$25(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->twoChannelPlayer:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->twoChannelPlayer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
