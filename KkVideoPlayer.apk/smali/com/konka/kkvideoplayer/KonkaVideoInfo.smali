.class public Lcom/konka/kkvideoplayer/KonkaVideoInfo;
.super Ljava/lang/Object;
.source "KonkaVideoInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private album:Ljava/lang/String;

.field private artist:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private duration:J

.field private icon:Ljava/lang/String;

.field private id:I

.field private mimeType:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private size:J

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # J
    .param p10    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->id:I

    iput-object p2, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->album:Ljava/lang/String;

    iput-object p4, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->artist:Ljava/lang/String;

    iput-object p5, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->displayName:Ljava/lang/String;

    iput-object p6, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->mimeType:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->path:Ljava/lang/String;

    iput-wide p8, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->size:J

    iput-wide p10, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->duration:J

    return-void
.end method


# virtual methods
.method public getAlbum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->album:Ljava/lang/String;

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->duration:J

    return-wide v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->icon:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->id:I

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->size:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->album:Ljava/lang/String;

    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->artist:Ljava/lang/String;

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->duration:J

    return-void
.end method

.method public setIcon(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->id:I

    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->mimeType:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->path:Ljava/lang/String;

    return-void
.end method

.method public setSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->size:J

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->title:Ljava/lang/String;

    return-void
.end method
