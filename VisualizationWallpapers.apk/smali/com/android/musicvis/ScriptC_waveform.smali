.class public Lcom/android/musicvis/ScriptC_waveform;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_waveform.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gIdle:I

.field private mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

.field private mExportVar_gPoints:Lcom/android/musicvis/ScriptField_Vertex;

.field private mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

.field private mExportVar_gWaveCounter:I

.field private mExportVar_gWidth:I

.field private mExportVar_gYRotation:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/ScriptC_waveform;->__MESH:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_gPoints(Lcom/android/musicvis/ScriptField_Vertex;)V
    .locals 2
    .param p1    # Lcom/android/musicvis/ScriptField_Vertex;

    const/4 v1, 0x6

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPoints:Lcom/android/musicvis/ScriptField_Vertex;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/musicvis/ScriptC_waveform;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/musicvis/ScriptField_Vertex;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/musicvis/ScriptC_waveform;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public declared-synchronized set_gCubeMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gCubeMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gIdle(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(II)V

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gIdle:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFBackground(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPFBackground:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPVBackground(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPointBuffer(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gPointBuffer:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTlinetexture(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x8

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gTlinetexture:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gWaveCounter(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(II)V

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gWaveCounter:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gWidth(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(II)V

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gWidth:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gYRotation(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/ScriptC_waveform;->setVar(IF)V

    iput p1, p0, Lcom/android/musicvis/ScriptC_waveform;->mExportVar_gYRotation:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
