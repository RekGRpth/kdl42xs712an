.class Lcom/android/musicvis/vis5/Visualization5RS;
.super Lcom/android/musicvis/RenderScriptScene;
.source "Visualization5RS.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/musicvis/vis5/Visualization5RS$WorldState;
    }
.end annotation


# instance fields
.field private mAudioCapture:Lcom/android/musicvis/AudioCapture;

.field private mCubeMesh:Landroid/renderscript/Mesh;

.field private final mDrawCube:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mIndexData:[S

.field private mLineIdxAlloc:Landroid/renderscript/Allocation;

.field private mNeedleMass:I

.field private mNeedlePos:I

.field private mNeedleSpeed:I

.field private mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

.field private mPVBackground:Landroid/renderscript/ProgramVertex;

.field private mPfBackgroundMip:Landroid/renderscript/ProgramFragment;

.field private mPfBackgroundNoMip:Landroid/renderscript/ProgramFragment;

.field private mPfsBackground:Landroid/renderscript/ProgramStore;

.field protected mPointAlloc:Landroid/renderscript/Allocation;

.field protected mPointData:[F

.field private mPr:Landroid/renderscript/ProgramRaster;

.field private mSamplerMip:Landroid/renderscript/Sampler;

.field private mSamplerNoMip:Landroid/renderscript/Sampler;

.field mScript:Lcom/android/musicvis/vis5/ScriptC_many;

.field private mSpringForceAtOrigin:I

.field private mTextures:[Landroid/renderscript/Allocation;

.field private mVertexBuffer:Lcom/android/musicvis/vis5/ScriptField_Vertex;

.field private mVisible:Z

.field private mVizData:[I

.field mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;


# direct methods
.method constructor <init>(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/musicvis/RenderScriptScene;-><init>(II)V

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/musicvis/vis5/Visualization5RS$1;

    invoke-direct {v3, p0}, Lcom/android/musicvis/vis5/Visualization5RS$1;-><init>(Lcom/android/musicvis/vis5/Visualization5RS;)V

    iput-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mDrawCube:Ljava/lang/Runnable;

    iput v4, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    iput v4, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleSpeed:I

    const/16 v3, 0xa

    iput v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleMass:I

    const/16 v3, 0xc8

    iput v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mSpringForceAtOrigin:I

    new-instance v3, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    invoke-direct {v3}, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;-><init>()V

    iput-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    const/16 v3, 0x800

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    const/16 v3, 0x200

    new-array v3, v3, [S

    iput-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mIndexData:[S

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    const/16 v3, 0x400

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    iput p1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWidth:I

    iput p2, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mHeight:I

    iget-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    array-length v3, v3

    div-int/lit8 v2, v3, 0x8

    div-int/lit8 v0, v2, 0x2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    sub-int v5, v1, v0

    int-to-float v5, v5

    aput v5, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x2

    aput v6, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x3

    aput v6, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x4

    sub-int v5, v1, v0

    int-to-float v5, v5

    aput v5, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x6

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v3, v4

    iget-object v3, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v4, v1, 0x8

    add-int/lit8 v4, v4, 0x7

    aput v6, v3, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected createScript()Landroid/renderscript/ScriptC;
    .locals 15

    const/4 v10, 0x3

    const/4 v14, 0x5

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x2

    new-instance v5, Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const/high16 v8, 0x7f040000    # com.android.musicvis.R.raw.many

    invoke-direct {v5, v6, v7, v8}, Lcom/android/musicvis/vis5/ScriptC_many;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    new-instance v4, Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v4, v5}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v4}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->create()Landroid/renderscript/ProgramVertexFixedFunction;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    new-instance v5, Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v5, v6}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;-><init>(Landroid/renderscript/RenderScript;)V

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    check-cast v5, Landroid/renderscript/ProgramVertexFixedFunction;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v5, v6}, Landroid/renderscript/ProgramVertexFixedFunction;->bindConstants(Landroid/renderscript/ProgramVertexFixedFunction$Constants;)V

    new-instance v3, Landroid/renderscript/Matrix4f;

    invoke-direct {v3}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWidth:I

    iget v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mHeight:I

    invoke-virtual {v3, v5, v6}, Landroid/renderscript/Matrix4f;->loadProjectionNormalized(II)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v5, v3}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gPVBackground(Landroid/renderscript/ProgramVertex;)V

    const/16 v5, 0x8

    new-array v5, v5, [Landroid/renderscript/Allocation;

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f020001    # com.android.musicvis.R.drawable.background

    sget-object v9, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v6, v7, v8, v9, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v6

    aput-object v6, v5, v12

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v6, v6, v12

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTvumeter_background(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f020004    # com.android.musicvis.R.drawable.frame

    sget-object v9, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v6, v7, v8, v9, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v6

    aput-object v6, v5, v13

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v6, v6, v13

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTvumeter_frame(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f020009    # com.android.musicvis.R.drawable.peak_on

    sget-object v9, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v6, v7, v8, v9, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v6

    aput-object v6, v5, v11

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v6, v6, v11

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTvumeter_peak_on(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f020008    # com.android.musicvis.R.drawable.peak_off

    sget-object v9, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v6, v7, v8, v9, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v6

    aput-object v6, v5, v10

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v6, v6, v10

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTvumeter_peak_off(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v6, 0x4

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f020007    # com.android.musicvis.R.drawable.needle

    sget-object v10, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v7, v8, v9, v10, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v7

    aput-object v7, v5, v6

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTvumeter_needle(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f020002    # com.android.musicvis.R.drawable.black

    sget-object v9, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v6, v7, v8, v9, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v6

    aput-object v6, v5, v14

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v6, v6, v14

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTvumeter_black(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v6, 0x6

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const/high16 v9, 0x7f020000    # com.android.musicvis.R.drawable.albumart

    sget-object v10, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v7, v8, v9, v10, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v7

    aput-object v7, v5, v6

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTvumeter_album(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v6, 0x7

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f020003    # com.android.musicvis.R.drawable.fire

    sget-object v10, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_ON_SYNC_TO_TEXTURE:Landroid/renderscript/Allocation$MipmapControl;

    invoke-static {v7, v8, v9, v10, v11}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;ILandroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v7

    aput-object v7, v5, v6

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v7, 0x7

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTlinetexture(Landroid/renderscript/Allocation;)V

    new-instance v0, Landroid/renderscript/Sampler$Builder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/Sampler$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setMinification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setMagnification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setWrapS(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setWrapT(Landroid/renderscript/Sampler$Value;)V

    invoke-virtual {v0}, Landroid/renderscript/Sampler$Builder;->create()Landroid/renderscript/Sampler;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mSamplerNoMip:Landroid/renderscript/Sampler;

    new-instance v0, Landroid/renderscript/Sampler$Builder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/Sampler$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR_MIP_LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setMinification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setMagnification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setWrapS(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v0, v5}, Landroid/renderscript/Sampler$Builder;->setWrapT(Landroid/renderscript/Sampler$Value;)V

    invoke-virtual {v0}, Landroid/renderscript/Sampler$Builder;->create()Landroid/renderscript/Sampler;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mSamplerMip:Landroid/renderscript/Sampler;

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->REPLACE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v6, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGBA:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v5, v6, v12}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfBackgroundNoMip:Landroid/renderscript/ProgramFragment;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfBackgroundNoMip:Landroid/renderscript/ProgramFragment;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mSamplerNoMip:Landroid/renderscript/Sampler;

    invoke-virtual {v5, v6, v12}, Landroid/renderscript/ProgramFragment;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfBackgroundNoMip:Landroid/renderscript/ProgramFragment;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gPFBackgroundNoMip(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->REPLACE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v6, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGBA:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v5, v6, v12}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfBackgroundMip:Landroid/renderscript/ProgramFragment;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfBackgroundMip:Landroid/renderscript/ProgramFragment;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mSamplerMip:Landroid/renderscript/Sampler;

    invoke-virtual {v5, v6, v12}, Landroid/renderscript/ProgramFragment;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfBackgroundMip:Landroid/renderscript/ProgramFragment;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gPFBackgroundMip(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramRaster$Builder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramRaster$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/ProgramRaster$CullMode;->NONE:Landroid/renderscript/ProgramRaster$CullMode;

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramRaster$Builder;->setCullMode(Landroid/renderscript/ProgramRaster$CullMode;)Landroid/renderscript/ProgramRaster$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramRaster$Builder;->create()Landroid/renderscript/ProgramRaster;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPr:Landroid/renderscript/ProgramRaster;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPr:Landroid/renderscript/ProgramRaster;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gPR(Landroid/renderscript/ProgramRaster;)V

    new-instance v0, Landroid/renderscript/ProgramStore$Builder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramStore$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/ProgramStore$DepthFunc;->EQUAL:Landroid/renderscript/ProgramStore$DepthFunc;

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramStore$Builder;->setDepthFunc(Landroid/renderscript/ProgramStore$DepthFunc;)Landroid/renderscript/ProgramStore$Builder;

    sget-object v5, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v6, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v5, v6}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0, v13}, Landroid/renderscript/ProgramStore$Builder;->setDitherEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0, v12}, Landroid/renderscript/ProgramStore$Builder;->setDepthMaskEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfsBackground:Landroid/renderscript/ProgramStore;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPfsBackground:Landroid/renderscript/ProgramStore;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gPFSBackground(Landroid/renderscript/ProgramStore;)V

    new-instance v5, Lcom/android/musicvis/vis5/ScriptField_Vertex;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    array-length v7, v7

    div-int/lit8 v7, v7, 0x4

    invoke-direct {v5, v6, v7}, Lcom/android/musicvis/vis5/ScriptField_Vertex;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVertexBuffer:Lcom/android/musicvis/vis5/ScriptField_Vertex;

    new-instance v2, Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v2, v5}, Landroid/renderscript/Mesh$AllocationBuilder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVertexBuffer:Lcom/android/musicvis/vis5/ScriptField_Vertex;

    invoke-virtual {v5}, Lcom/android/musicvis/vis5/ScriptField_Vertex;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/renderscript/Mesh$AllocationBuilder;->addVertexAllocation(Landroid/renderscript/Allocation;)Landroid/renderscript/Mesh$AllocationBuilder;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v6}, Landroid/renderscript/Element;->U16(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v6

    iget-object v7, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mIndexData:[S

    array-length v7, v7

    invoke-static {v5, v6, v7, v14}, Landroid/renderscript/Allocation;->createSized(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Allocation;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mLineIdxAlloc:Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mLineIdxAlloc:Landroid/renderscript/Allocation;

    sget-object v6, Landroid/renderscript/Mesh$Primitive;->LINE:Landroid/renderscript/Mesh$Primitive;

    invoke-virtual {v2, v5, v6}, Landroid/renderscript/Mesh$AllocationBuilder;->addIndexSetAllocation(Landroid/renderscript/Allocation;Landroid/renderscript/Mesh$Primitive;)Landroid/renderscript/Mesh$AllocationBuilder;

    invoke-virtual {v2}, Landroid/renderscript/Mesh$AllocationBuilder;->create()Landroid/renderscript/Mesh;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mCubeMesh:Landroid/renderscript/Mesh;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVertexBuffer:Lcom/android/musicvis/vis5/ScriptField_Vertex;

    invoke-virtual {v5}, Lcom/android/musicvis/vis5/ScriptField_Vertex;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    iput-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointAlloc:Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVertexBuffer:Lcom/android/musicvis/vis5/ScriptField_Vertex;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->bind_gPoints(Lcom/android/musicvis/vis5/ScriptField_Vertex;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointAlloc:Landroid/renderscript/Allocation;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gPointBuffer(Landroid/renderscript/Allocation;)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mCubeMesh:Landroid/renderscript/Mesh;

    invoke-virtual {v5, v6}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gCubeMesh(Landroid/renderscript/Mesh;)V

    invoke-virtual {p0}, Lcom/android/musicvis/vis5/Visualization5RS;->updateWave()V

    const/4 v1, 0x0

    :goto_0
    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mIndexData:[S

    array-length v5, v5

    if-ge v1, v5, :cond_0

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mIndexData:[S

    int-to-short v6, v1

    aput-short v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointAlloc:Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    invoke-virtual {v5, v6}, Landroid/renderscript/Allocation;->copyFromUnchecked([F)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mLineIdxAlloc:Landroid/renderscript/Allocation;

    iget-object v6, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mIndexData:[S

    invoke-virtual {v5, v6}, Landroid/renderscript/Allocation;->copyFrom([S)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mLineIdxAlloc:Landroid/renderscript/Allocation;

    invoke-virtual {v5, v13}, Landroid/renderscript/Allocation;->syncAll(I)V

    iget-object v5, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    return-object v5
.end method

.method public resize(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/musicvis/RenderScriptScene;->resize(II)V

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/renderscript/Matrix4f;

    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/renderscript/Matrix4f;->loadProjectionNormalized(II)V

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v1, v0}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    :cond_0
    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    const/high16 v2, -0x3e600000    # -20.0f

    iput v2, v1, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mTilt:F

    return-void
.end method

.method public setOffset(FFII)V
    .locals 3
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    const/high16 v1, 0x3f000000    # 0.5f

    sub-float v1, p1, v1

    const/high16 v2, 0x42b40000    # 90.0f

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mRotate:F

    invoke-virtual {p0}, Lcom/android/musicvis/vis5/Visualization5RS;->updateWorldState()V

    return-void
.end method

.method public start()V
    .locals 3

    invoke-super {p0}, Lcom/android/musicvis/RenderScriptScene;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVisible:Z

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/musicvis/AudioCapture;

    const/4 v1, 0x0

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcom/android/musicvis/AudioCapture;-><init>(II)V

    iput-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->start()V

    invoke-virtual {p0}, Lcom/android/musicvis/vis5/Visualization5RS;->updateWave()V

    return-void
.end method

.method public stop()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicvis/RenderScriptScene;->stop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVisible:Z

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->stop()V

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    return-void
.end method

.method updateWave()V
    .locals 15

    const/16 v14, 0x7fff

    const/4 v13, 0x1

    const/4 v12, 0x0

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mDrawCube:Ljava/lang/Runnable;

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVisible:Z

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mDrawCube:Ljava/lang/Runnable;

    const-wide/16 v10, 0x14

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v3, 0x0

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    const/16 v9, 0x200

    invoke-virtual {v8, v9, v13}, Lcom/android/musicvis/AudioCapture;->getFormattedData(II)[I

    move-result-object v8

    iput-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    array-length v3, v8

    :cond_1
    const/4 v7, 0x0

    if-lez v3, :cond_4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    aget v6, v8, v2

    if-gez v6, :cond_2

    neg-int v6, v6

    :cond_2
    add-int/2addr v7, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    div-int/2addr v7, v3

    :cond_4
    iget v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleSpeed:I

    mul-int/lit8 v8, v8, 0x3

    sub-int v8, v7, v8

    iget v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    iget v10, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mSpringForceAtOrigin:I

    add-int/2addr v9, v10

    sub-int v4, v8, v9

    iget v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleMass:I

    div-int v0, v4, v8

    iget v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleSpeed:I

    add-int/2addr v8, v0

    iput v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleSpeed:I

    iget v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    iget v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleSpeed:I

    add-int/2addr v8, v9

    iput v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    iget v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    if-gez v8, :cond_8

    iput v12, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    iput v12, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleSpeed:I

    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v8, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mPeak:I

    if-lez v8, :cond_6

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v9, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mPeak:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mPeak:I

    :cond_6
    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    const/high16 v9, 0x43030000    # 131.0f

    iget v10, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    int-to-float v10, v10

    const/high16 v11, 0x43cd0000    # 410.0f

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    iput v9, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mAngle:F

    if-nez v3, :cond_a

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v8, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mIdle:I

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iput v13, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mIdle:I

    :cond_7
    :goto_3
    invoke-virtual {p0}, Lcom/android/musicvis/vis5/Visualization5RS;->updateWorldState()V

    goto :goto_0

    :cond_8
    iget v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    if-le v8, v14, :cond_5

    iget v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    const v9, 0x8235

    if-le v8, v9, :cond_9

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    const/16 v9, 0xa

    iput v9, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mPeak:I

    :cond_9
    iput v14, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedlePos:I

    iput v12, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mNeedleSpeed:I

    goto :goto_2

    :cond_a
    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v8, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mIdle:I

    if-eqz v8, :cond_b

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iput v12, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mIdle:I

    :cond_b
    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    array-length v8, v8

    div-int/lit8 v5, v8, 0x8

    div-int/lit8 v3, v3, 0x4

    if-le v3, v5, :cond_c

    move v3, v5

    :cond_c
    const/4 v2, 0x0

    :goto_4
    if-ge v2, v3, :cond_d

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    mul-int/lit8 v9, v2, 0x4

    aget v8, v8, v9

    iget-object v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    mul-int/lit8 v10, v2, 0x4

    add-int/lit8 v10, v10, 0x1

    aget v9, v9, v10

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    mul-int/lit8 v10, v2, 0x4

    add-int/lit8 v10, v10, 0x2

    aget v9, v9, v10

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mVizData:[I

    mul-int/lit8 v10, v2, 0x4

    add-int/lit8 v10, v10, 0x3

    aget v9, v9, v10

    add-int v1, v8, v9

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v9, v2, 0x8

    add-int/lit8 v9, v9, 0x1

    int-to-float v10, v1

    aput v10, v8, v9

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    mul-int/lit8 v9, v2, 0x8

    add-int/lit8 v9, v9, 0x5

    neg-int v10, v1

    int-to-float v10, v10

    aput v10, v8, v9

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_d
    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointAlloc:Landroid/renderscript/Allocation;

    iget-object v9, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mPointData:[F

    invoke-virtual {v8, v9}, Landroid/renderscript/Allocation;->copyFromUnchecked([F)V

    iget-object v8, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v9, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mWaveCounter:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mWaveCounter:I

    goto :goto_3
.end method

.method protected updateWorldState()V
    .locals 2

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v1, v1, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mAngle:F

    invoke-virtual {v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gAngle(F)V

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v1, v1, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mPeak:I

    invoke-virtual {v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gPeak(I)V

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v1, v1, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mRotate:F

    invoke-virtual {v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gRotate(F)V

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v1, v1, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mTilt:F

    invoke-virtual {v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gTilt(F)V

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v1, v1, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mIdle:I

    invoke-virtual {v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gIdle(I)V

    iget-object v0, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mScript:Lcom/android/musicvis/vis5/ScriptC_many;

    iget-object v1, p0, Lcom/android/musicvis/vis5/Visualization5RS;->mWorldState:Lcom/android/musicvis/vis5/Visualization5RS$WorldState;

    iget v1, v1, Lcom/android/musicvis/vis5/Visualization5RS$WorldState;->mWaveCounter:I

    invoke-virtual {v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->set_gWaveCounter(I)V

    return-void
.end method
