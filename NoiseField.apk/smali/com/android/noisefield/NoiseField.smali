.class public Lcom/android/noisefield/NoiseField;
.super Landroid/app/Activity;
.source "NoiseField.java"


# instance fields
.field private mView:Lcom/android/noisefield/NoiseFieldView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/noisefield/NoiseFieldView;

    invoke-direct {v0, p0}, Lcom/android/noisefield/NoiseFieldView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/noisefield/NoiseField;->mView:Lcom/android/noisefield/NoiseFieldView;

    iget-object v0, p0, Lcom/android/noisefield/NoiseField;->mView:Lcom/android/noisefield/NoiseFieldView;

    invoke-virtual {p0, v0}, Lcom/android/noisefield/NoiseField;->setContentView(Landroid/view/View;)V

    return-void
.end method
