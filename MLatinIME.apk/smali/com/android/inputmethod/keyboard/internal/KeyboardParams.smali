.class public Lcom/android/inputmethod/keyboard/internal/KeyboardParams;
.super Ljava/lang/Object;
.source "KeyboardParams.java"


# instance fields
.field public GRID_HEIGHT:I

.field public GRID_WIDTH:I

.field public final mAltCodeKeysWhileTyping:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field public mBaseHeight:I

.field public mBaseWidth:I

.field public mBottomPadding:I

.field public final mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

.field public mDefaultKeyWidth:I

.field public mDefaultRowHeight:I

.field private final mHeightHistogram:Landroid/util/SparseIntArray;

.field public mHorizontalCenterPadding:I

.field public mHorizontalEdgesPadding:I

.field public mHorizontalGap:I

.field public final mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

.field public mId:Lcom/android/inputmethod/keyboard/KeyboardId;

.field public final mKeyStyles:Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;

.field public mKeyVisualAttributes:Lcom/android/inputmethod/keyboard/internal/KeyVisualAttributes;

.field public final mKeys:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field public mKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;

.field private mMaxHeightCount:I

.field public mMaxMoreKeysKeyboardColumn:I

.field private mMaxWidthCount:I

.field public mMoreKeysTemplate:I

.field public mMostCommonKeyHeight:I

.field public mMostCommonKeyWidth:I

.field public mOccupiedHeight:I

.field public mOccupiedWidth:I

.field public mProximityCharsCorrectionEnabled:Z

.field public final mShiftKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/keyboard/Key;",
            ">;"
        }
    .end annotation
.end field

.field public final mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

.field public mThemeId:I

.field public mTopPadding:I

.field public final mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;

.field public mVerticalGap:I

.field private final mWidthHistogram:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newTreeSet()Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeys:Ljava/util/TreeSet;

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mShiftKeys:Ljava/util/ArrayList;

    invoke-static {}, Lcom/android/inputmethod/latin/CollectionUtils;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mAltCodeKeysWhileTyping:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mIconsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardIconsSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mCodesSet:Lcom/android/inputmethod/keyboard/internal/KeyboardCodesSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;

    iget-object v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTextsSet:Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;

    invoke-direct {v0, v1}, Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;-><init>(Lcom/android/inputmethod/keyboard/internal/KeyboardTextsSet;)V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeyStyles:Lcom/android/inputmethod/keyboard/internal/KeyStylesSet;

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMostCommonKeyHeight:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMostCommonKeyWidth:I

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mTouchPositionCorrection:Lcom/android/inputmethod/keyboard/internal/TouchPositionCorrection;

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxHeightCount:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxWidthCount:I

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHeightHistogram:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mWidthHistogram:Landroid/util/SparseIntArray;

    return-void
.end method

.method private clearHistogram()V
    .locals 2

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMostCommonKeyHeight:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxHeightCount:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHeightHistogram:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxWidthCount:I

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMostCommonKeyWidth:I

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mWidthHistogram:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    return-void
.end method

.method private updateHistogram(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 6
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget v4, p1, Lcom/android/inputmethod/keyboard/Key;->mHeight:I

    iget v5, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mVerticalGap:I

    add-int v0, v4, v5

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHeightHistogram:Landroid/util/SparseIntArray;

    invoke-static {v4, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->updateHistogramCounter(Landroid/util/SparseIntArray;I)I

    move-result v1

    iget v4, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxHeightCount:I

    if-le v1, v4, :cond_0

    iput v1, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxHeightCount:I

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMostCommonKeyHeight:I

    :cond_0
    iget v4, p1, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    iget v5, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mHorizontalGap:I

    add-int v2, v4, v5

    iget-object v4, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mWidthHistogram:Landroid/util/SparseIntArray;

    invoke-static {v4, v2}, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->updateHistogramCounter(Landroid/util/SparseIntArray;I)I

    move-result v3

    iget v4, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxWidthCount:I

    if-le v3, v4, :cond_1

    iput v3, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMaxWidthCount:I

    iput v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mMostCommonKeyWidth:I

    :cond_1
    return-void
.end method

.method private static updateHistogramCounter(Landroid/util/SparseIntArray;I)I
    .locals 3
    .param p0    # Landroid/util/SparseIntArray;
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    :goto_0
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p0, p1, v0}, Landroid/util/SparseIntArray;->put(II)V

    return v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected clearKeys()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeys:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    iget-object v0, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mShiftKeys:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->clearHistogram()V

    return-void
.end method

.method public onAddKey(Lcom/android/inputmethod/keyboard/Key;)V
    .locals 4
    .param p1    # Lcom/android/inputmethod/keyboard/Key;

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeysCache:Lcom/android/inputmethod/keyboard/internal/KeysCache;

    invoke-virtual {v2, p1}, Lcom/android/inputmethod/keyboard/internal/KeysCache;->get(Lcom/android/inputmethod/keyboard/Key;)Lcom/android/inputmethod/keyboard/Key;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Key;->isSpacer()Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mWidth:I

    if-nez v2, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mKeys:Ljava/util/TreeSet;

    invoke-virtual {v2, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->updateHistogram(Lcom/android/inputmethod/keyboard/Key;)V

    :cond_0
    iget v2, v0, Lcom/android/inputmethod/keyboard/Key;->mCode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mShiftKeys:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lcom/android/inputmethod/keyboard/Key;->altCodeWhileTyping()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/inputmethod/keyboard/internal/KeyboardParams;->mAltCodeKeysWhileTyping:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void

    :cond_3
    move-object v0, p1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
