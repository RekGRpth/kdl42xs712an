.class Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;
.super Ljava/lang/Thread;
.source "DemoDeskImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;


# direct methods
.method constructor <init>(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;->this$0:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :goto_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;->this$0:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->bforceThreadSleep:Z
    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v1, 0x3e8

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;->this$0:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;
    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->access$1(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;->E_MS_DBC_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;->this$0:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    # invokes: Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->dbchandler()V
    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->access$2(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)V

    :cond_1
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;->this$0:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;
    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->access$3(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;->E_MS_DCC_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;->this$0:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    # invokes: Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->dcchandler()V
    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->access$4(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)V

    :cond_2
    :try_start_1
    # getter for: Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->sleeptimeCnt:I
    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->access$5()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
