.class public interface abstract Lcom/konka/kkinterface/tv/AtvInterface;
.super Ljava/lang/Object;
.source "AtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/AtvInterface$ATV_DATABASE;,
        Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;,
        Lcom/konka/kkinterface/tv/AtvInterface$ATV_SCAN_EVENT;
    }
.end annotation


# static fields
.field public static final atvcolorsystem:[Ljava/lang/String;

.field public static final atvsoundsystem:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "PAL"

    aput-object v1, v0, v2

    const-string v1, "NTSC"

    aput-object v1, v0, v3

    const-string v1, "SECAM"

    aput-object v1, v0, v4

    const-string v1, "AUTO"

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface;->atvcolorsystem:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "BG"

    aput-object v1, v0, v2

    const-string v1, "DK"

    aput-object v1, v0, v3

    const-string v1, "I"

    aput-object v1, v0, v4

    const-string v1, "L"

    aput-object v1, v0, v5

    const-string v1, "M"

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface;->atvsoundsystem:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract atvGetCurrentFrequency()I
.end method

.method public abstract atvGetProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;IILjava/lang/StringBuffer;)I
.end method

.method public abstract atvGetSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;
.end method

.method public abstract atvGetVideoSystem()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
.end method

.method public abstract atvSetAutoTuningEnd()Z
.end method

.method public abstract atvSetAutoTuningPause()Z
.end method

.method public abstract atvSetAutoTuningResume()Z
.end method

.method public abstract atvSetAutoTuningStart(III)Z
.end method

.method public abstract atvSetChannel(SZ)I
.end method

.method public abstract atvSetForceSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z
.end method

.method public abstract atvSetForceVedioSystem(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V
.end method

.method public abstract atvSetFrequency(I)Z
.end method

.method public abstract atvSetManualTuningEnd()V
.end method

.method public abstract atvSetManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z
.end method

.method public abstract atvSetProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;IILjava/lang/String;)I
.end method

.method public abstract getAtvStationName(I)Ljava/lang/String;
.end method

.method public abstract getCurrentChannelNumber()I
.end method

.method public abstract getProgramCtrl(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;IILjava/lang/String;)I
.end method

.method public abstract sendAtvScaninfo()V
.end method

.method public abstract setProgramCtrl(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;IILjava/lang/String;)I
.end method
