.class public interface abstract Lcom/konka/kkinterface/tv/EpgDesk;
.super Ljava/lang/Object;
.source "EpgDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# virtual methods
.method public abstract ProgramSel(Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;)V
.end method

.method public abstract UpdateEpgTimer(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract addEpgTimerEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;
.end method

.method public abstract cancelEpgTimerEvent(I)V
.end method

.method public abstract cancelEpgTimerEvent(IZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract closeDB()V
.end method

.method public abstract delAllEpgEvent()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract delEpgEvent(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract delEpgTimerEvent(IZ)Z
.end method

.method public abstract deletePastEpgTimer()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract deletePastScheduleInfo()V
.end method

.method public abstract deleteScheduleInfoByStartTime(I)V
.end method

.method public abstract disableEpgBarkerChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract enableEpgBarkerChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract execEpgTimerAction()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract execEpgTimerEvent()V
.end method

.method public abstract getCridAlternateList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCridSeriesList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCridSplitList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCridStatus(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCurClkTime()Landroid/text/format/Time;
.end method

.method public abstract getCurInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
.end method

.method public abstract getCurKonkaProgInfo()Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;
.end method

.method public abstract getCurProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
.end method

.method public abstract getDtvProgCount()Lcom/konka/kkimplements/tv/vo/KonkaProgCount;
.end method

.method public abstract getEitInfo(Z)Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEpgEventOffsetTime(Landroid/text/format/Time;Z)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEpgSearchReslut(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEpgTimerEventCount()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEpgTimerEventList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEpgTimerRecordingProgram()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEvent1stMatchHdBroadcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEvent1stMatchHdSimulcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEventCount(SILandroid/text/format/Time;)I
.end method

.method public abstract getEventDescriptor(SILandroid/text/format/Time;Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEventDetailDescriptor(I)Ljava/lang/String;
.end method

.method public abstract getEventHdSimulcast(SILandroid/text/format/Time;S)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            "S)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEventInfoById(SIS)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEventInfoByRctLink(Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEventInfoByTime(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getEventList(SI)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEventListByDay(I)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNvodTimeShiftEventCount(SI)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getNvodTimeShiftEventInfo(SIILcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SII",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getPresentFollowingEventInfo(SIZLcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getProgListByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mstar/android/tvapi/common/vo/EnumServiceType;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getProgListByServiceType(S)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRctTrailerLink()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getScheduleInfoByIndex(I)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
.end method

.method public abstract getTimeZone()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract isEpgTimerSettingValid(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract modifyTimerNotifyTime(I)V
.end method

.method public abstract openDB()V
.end method

.method public abstract resetEpgProgramPriority()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setDispalyWindow(IIII)V
.end method

.method public abstract setDtvInputSource()V
.end method

.method public abstract setEpgProgramPriority(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setEpgProgramPriority(SI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setScheduleNofityTime(I)V
.end method

.method public abstract updateTimerInfo(Ljava/lang/String;Ljava/lang/String;I)V
.end method
