.class public Lcom/android/packageinstaller/SilentUninstallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SilentUninstallReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/SilentUninstallReceiver$PackageDeleteObserver;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final UNINSTALL_COMPLETE:I

.field private mAppInfo:Landroid/content/pm/ApplicationInfo;

.field private mHandler:Landroid/os/Handler;

.field private mIsToastShow:Ljava/lang/Boolean;

.field private volatile mResultCode:I

.field private mToast:Landroid/widget/Toast;

.field private pm:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, "SilentUninstallReceiver"

    iput-object v0, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->TAG:Ljava/lang/String;

    iput v1, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->UNINSTALL_COMPLETE:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mResultCode:I

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    new-instance v0, Lcom/android/packageinstaller/SilentUninstallReceiver$1;

    invoke-direct {v0, p0}, Lcom/android/packageinstaller/SilentUninstallReceiver$1;-><init>(Lcom/android/packageinstaller/SilentUninstallReceiver;)V

    iput-object v0, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/android/packageinstaller/SilentUninstallReceiver;I)V
    .locals 0

    iput p1, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mResultCode:I

    return-void
.end method

.method static synthetic access$1(Lcom/android/packageinstaller/SilentUninstallReceiver;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$2(Lcom/android/packageinstaller/SilentUninstallReceiver;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$3(Lcom/android/packageinstaller/SilentUninstallReceiver;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const v5, 0x7f06004c    # com.android.packageinstaller.R.string.toast_applicationInfo_error

    const/4 v4, 0x1

    const-string v2, ""

    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.konka.ACTION.SILENT_UNINSTALL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "packagename"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "NEED_HINT_INSTALL_RESULT"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    const-string v2, "SilentUninstallReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid package name:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2, v5}, Landroid/widget/Toast;->setText(I)V

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->pm:Landroid/content/pm/PackageManager;

    const/16 v3, 0x2000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-nez v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2, v5}, Landroid/widget/Toast;->setText(I)V

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/android/packageinstaller/SilentUninstallReceiver$PackageDeleteObserver;

    invoke-direct {v0, p0}, Lcom/android/packageinstaller/SilentUninstallReceiver$PackageDeleteObserver;-><init>(Lcom/android/packageinstaller/SilentUninstallReceiver;)V

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->pm:Landroid/content/pm/PackageManager;

    iget-object v3, p0, Lcom/android/packageinstaller/SilentUninstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1
.end method
