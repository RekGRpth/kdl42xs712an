.class public final Lcom/android/packageinstaller/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final activity_text:I = 0x7f080033

.field public static final app_icon:I = 0x7f080001

.field public static final app_name:I = 0x7f080002

.field public static final app_size:I = 0x7f080003

.field public static final app_snippet:I = 0x7f080000

.field public static final buttons_panel:I = 0x7f08002d

.field public static final cancel_button:I = 0x7f080020

.field public static final center_explanation:I = 0x7f080030

.field public static final center_text:I = 0x7f080029

.field public static final change_install_location:I = 0x7f080008

.field public static final default_install_location:I = 0x7f080006

.field public static final default_install_location_hint:I = 0x7f080009

.field public static final default_install_location_line_down:I = 0x7f080007

.field public static final default_install_location_line_up:I = 0x7f080005

.field public static final default_install_location_title:I = 0x7f080004

.field public static final device_manager_button:I = 0x7f080038

.field public static final done_button:I = 0x7f08002f

.field public static final flash_SeekBar:I = 0x7f08000c

.field public static final flash_free_icon:I = 0x7f08000f

.field public static final flash_free_size:I = 0x7f080010

.field public static final flash_size:I = 0x7f08000b

.field public static final flash_used_icon:I = 0x7f08000d

.field public static final flash_used_size:I = 0x7f08000e

.field public static final install_cancel_button:I = 0x7f080018

.field public static final install_confirm_panel:I = 0x7f080023

.field public static final install_error_text:I = 0x7f08002c

.field public static final install_hint:I = 0x7f08002a

.field public static final install_ok_button:I = 0x7f080017

.field public static final install_over_text:I = 0x7f08002b

.field public static final install_start_warning:I = 0x7f08001a

.field public static final install_start_warning_icon:I = 0x7f080019

.field public static final install_title:I = 0x7f080022

.field public static final install_title_text_one:I = 0x7f080024

.field public static final install_title_text_three:I = 0x7f080026

.field public static final install_title_text_two:I = 0x7f080025

.field public static final launch_button:I = 0x7f08002e

.field public static final leftSpacer:I = 0x7f08001e

.field public static final memory_size:I = 0x7f08000a

.field public static final ok_button:I = 0x7f08001f

.field public static final ok_panel:I = 0x7f080035

.field public static final permissions_section:I = 0x7f08001b

.field public static final progress_bar:I = 0x7f080028

.field public static final rightSpacer:I = 0x7f080021

.field public static final sdcard_SeekBar:I = 0x7f080012

.field public static final sdcard_free_icon:I = 0x7f080015

.field public static final sdcard_free_size:I = 0x7f080016

.field public static final sdcard_size:I = 0x7f080011

.field public static final sdcard_used_icon:I = 0x7f080013

.field public static final sdcard_used_size:I = 0x7f080014

.field public static final security_settings_desc:I = 0x7f08001c

.field public static final security_settings_list:I = 0x7f08001d

.field public static final top_divider:I = 0x7f080027

.field public static final uninstall_activity_snippet:I = 0x7f080031

.field public static final uninstall_confirm:I = 0x7f080032

.field public static final uninstall_hint:I = 0x7f080034

.field public static final uninstall_holder:I = 0x7f080036

.field public static final uninstall_title_text_one:I = 0x7f080039

.field public static final uninstall_title_text_three:I = 0x7f08003b

.field public static final uninstall_title_text_two:I = 0x7f08003a

.field public static final uninstalling_scrollview:I = 0x7f080037


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
