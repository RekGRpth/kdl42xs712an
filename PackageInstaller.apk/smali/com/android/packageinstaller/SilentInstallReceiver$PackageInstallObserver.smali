.class Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "SilentInstallReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/SilentInstallReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/packageinstaller/SilentInstallReceiver;


# direct methods
.method constructor <init>(Lcom/android/packageinstaller/SilentInstallReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$8(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p2, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$8(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
