.class Lgreendroid/widget/PagedDialog$1;
.super Ljava/lang/Object;
.source "PagedDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreendroid/widget/PagedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lgreendroid/widget/PagedDialog;


# direct methods
.method constructor <init>(Lgreendroid/widget/PagedDialog;)V
    .locals 0

    iput-object p1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    iget-object v1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;
    invoke-static {v1}, Lgreendroid/widget/PagedDialog;->access$0(Lgreendroid/widget/PagedDialog;)Landroid/widget/Button;

    move-result-object v1

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mButtonPositiveMessage:Landroid/os/Message;
    invoke-static {v1}, Lgreendroid/widget/PagedDialog;->access$1(Lgreendroid/widget/PagedDialog;)Landroid/os/Message;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mButtonPositiveMessage:Landroid/os/Message;
    invoke-static {v1}, Lgreendroid/widget/PagedDialog;->access$1(Lgreendroid/widget/PagedDialog;)Landroid/os/Message;

    move-result-object v1

    invoke-static {v1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    iget-object v1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lgreendroid/widget/PagedDialog;->access$4(Lgreendroid/widget/PagedDialog;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_2
    iget-object v1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;
    invoke-static {v1}, Lgreendroid/widget/PagedDialog;->access$2(Lgreendroid/widget/PagedDialog;)Landroid/widget/Button;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mButtonNegativeMessage:Landroid/os/Message;
    invoke-static {v1}, Lgreendroid/widget/PagedDialog;->access$3(Lgreendroid/widget/PagedDialog;)Landroid/os/Message;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgreendroid/widget/PagedDialog$1;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mButtonNegativeMessage:Landroid/os/Message;
    invoke-static {v1}, Lgreendroid/widget/PagedDialog;->access$3(Lgreendroid/widget/PagedDialog;)Landroid/os/Message;

    move-result-object v1

    invoke-static {v1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    goto :goto_0
.end method
