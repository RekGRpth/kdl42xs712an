.class Lgreendroid/widget/PagedView$1;
.super Landroid/database/DataSetObserver;
.source "PagedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreendroid/widget/PagedView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lgreendroid/widget/PagedView;


# direct methods
.method constructor <init>(Lgreendroid/widget/PagedView;)V
    .locals 0

    iput-object p1, p0, Lgreendroid/widget/PagedView$1;->this$0:Lgreendroid/widget/PagedView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    iget-object v1, p0, Lgreendroid/widget/PagedView$1;->this$0:Lgreendroid/widget/PagedView;

    # getter for: Lgreendroid/widget/PagedView;->mCurrentPage:I
    invoke-static {v1}, Lgreendroid/widget/PagedView;->access$0(Lgreendroid/widget/PagedView;)I

    move-result v0

    iget-object v1, p0, Lgreendroid/widget/PagedView$1;->this$0:Lgreendroid/widget/PagedView;

    iget-object v2, p0, Lgreendroid/widget/PagedView$1;->this$0:Lgreendroid/widget/PagedView;

    # getter for: Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;
    invoke-static {v2}, Lgreendroid/widget/PagedView;->access$1(Lgreendroid/widget/PagedView;)Lgreendroid/widget/PagedAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedView;->setAdapter(Lgreendroid/widget/PagedAdapter;)V

    iget-object v1, p0, Lgreendroid/widget/PagedView$1;->this$0:Lgreendroid/widget/PagedView;

    invoke-static {v1, v0}, Lgreendroid/widget/PagedView;->access$2(Lgreendroid/widget/PagedView;I)V

    iget-object v1, p0, Lgreendroid/widget/PagedView$1;->this$0:Lgreendroid/widget/PagedView;

    iget-object v2, p0, Lgreendroid/widget/PagedView$1;->this$0:Lgreendroid/widget/PagedView;

    # invokes: Lgreendroid/widget/PagedView;->getOffsetForPage(I)I
    invoke-static {v2, v0}, Lgreendroid/widget/PagedView;->access$3(Lgreendroid/widget/PagedView;I)I

    move-result v2

    # invokes: Lgreendroid/widget/PagedView;->setOffsetX(I)V
    invoke-static {v1, v2}, Lgreendroid/widget/PagedView;->access$4(Lgreendroid/widget/PagedView;I)V

    return-void
.end method

.method public onInvalidated()V
    .locals 0

    return-void
.end method
