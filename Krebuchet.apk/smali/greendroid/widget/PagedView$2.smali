.class Lgreendroid/widget/PagedView$2;
.super Ljava/lang/Object;
.source "PagedView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreendroid/widget/PagedView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lgreendroid/widget/PagedView;


# direct methods
.method constructor <init>(Lgreendroid/widget/PagedView;)V
    .locals 0

    iput-object p1, p0, Lgreendroid/widget/PagedView$2;->this$0:Lgreendroid/widget/PagedView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lgreendroid/widget/PagedView$2;->this$0:Lgreendroid/widget/PagedView;

    # getter for: Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;
    invoke-static {v1}, Lgreendroid/widget/PagedView;->access$5(Lgreendroid/widget/PagedView;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    iget-object v1, p0, Lgreendroid/widget/PagedView$2;->this$0:Lgreendroid/widget/PagedView;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    # invokes: Lgreendroid/widget/PagedView;->setOffsetX(I)V
    invoke-static {v1, v2}, Lgreendroid/widget/PagedView;->access$4(Lgreendroid/widget/PagedView;I)V

    iget-object v1, p0, Lgreendroid/widget/PagedView$2;->this$0:Lgreendroid/widget/PagedView;

    # getter for: Lgreendroid/widget/PagedView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lgreendroid/widget/PagedView;->access$6(Lgreendroid/widget/PagedView;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x10

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lgreendroid/widget/PagedView$2;->this$0:Lgreendroid/widget/PagedView;

    iget-object v2, p0, Lgreendroid/widget/PagedView$2;->this$0:Lgreendroid/widget/PagedView;

    # getter for: Lgreendroid/widget/PagedView;->mTargetPage:I
    invoke-static {v2}, Lgreendroid/widget/PagedView;->access$7(Lgreendroid/widget/PagedView;)I

    move-result v2

    # invokes: Lgreendroid/widget/PagedView;->performPageChange(I)V
    invoke-static {v1, v2}, Lgreendroid/widget/PagedView;->access$8(Lgreendroid/widget/PagedView;I)V

    goto :goto_0
.end method
