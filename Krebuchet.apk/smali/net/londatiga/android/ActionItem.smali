.class public Lnet/londatiga/android/ActionItem;
.super Ljava/lang/Object;
.source "ActionItem.java"


# instance fields
.field private actionId:I

.field private icon:Landroid/graphics/drawable/Drawable;

.field private selected:Z

.field private sticky:Z

.field private thumb:Landroid/graphics/Bitmap;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0, v0, v1, v1}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lnet/londatiga/android/ActionItem;->actionId:I

    iput-object p2, p0, Lnet/londatiga/android/ActionItem;->title:Ljava/lang/String;

    iput-object p3, p0, Lnet/londatiga/android/ActionItem;->icon:Landroid/graphics/drawable/Drawable;

    iput p1, p0, Lnet/londatiga/android/ActionItem;->actionId:I

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public getActionId()I
    .locals 1

    iget v0, p0, Lnet/londatiga/android/ActionItem;->actionId:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lnet/londatiga/android/ActionItem;->icon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getThumb()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lnet/londatiga/android/ActionItem;->thumb:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lnet/londatiga/android/ActionItem;->title:Ljava/lang/String;

    return-object v0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lnet/londatiga/android/ActionItem;->selected:Z

    return v0
.end method

.method public isSticky()Z
    .locals 1

    iget-boolean v0, p0, Lnet/londatiga/android/ActionItem;->sticky:Z

    return v0
.end method

.method public setActionId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lnet/londatiga/android/ActionItem;->actionId:I

    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lnet/londatiga/android/ActionItem;->icon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lnet/londatiga/android/ActionItem;->selected:Z

    return-void
.end method

.method public setSticky(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lnet/londatiga/android/ActionItem;->sticky:Z

    return-void
.end method

.method public setThumb(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lnet/londatiga/android/ActionItem;->thumb:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lnet/londatiga/android/ActionItem;->title:Ljava/lang/String;

    return-void
.end method
