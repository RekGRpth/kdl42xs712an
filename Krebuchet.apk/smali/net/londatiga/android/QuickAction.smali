.class public Lnet/londatiga/android/QuickAction;
.super Lnet/londatiga/android/ArrowedPopupWindow;
.source "QuickAction.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/londatiga/android/QuickAction$OnActionItemClickListener;
    }
.end annotation


# static fields
.field public static final HORIZONTAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "QuickAction"

.field public static final VERTICAL:I = 0x1


# instance fields
.field private ACCESSIBLE_COLOR:Landroid/content/res/ColorStateList;

.field private INACCESSIBLE_COLOR:Landroid/content/res/ColorStateList;

.field private actionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/londatiga/android/ActionItem;",
            ">;"
        }
    .end annotation
.end field

.field private mChildPos:I

.field private mDidAction:Z

.field private mInsertPos:I

.field private mItemClickListener:Lnet/londatiga/android/QuickAction$OnActionItemClickListener;

.field private mOrientation:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x5

    invoke-direct {p0, p1, p2, v0}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p3}, Lnet/londatiga/android/ArrowedPopupWindow;-><init>(Landroid/content/Context;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/londatiga/android/QuickAction;->actionItems:Ljava/util/List;

    if-nez p2, :cond_0

    const v0, 0x7f030023    # com.konka.avenger.R.layout.quickaction_popup_horizontal

    invoke-virtual {p0, v0}, Lnet/londatiga/android/QuickAction;->setContentView(I)V

    :goto_0
    iput p2, p0, Lnet/londatiga/android/QuickAction;->mOrientation:I

    const/4 v0, 0x0

    iput v0, p0, Lnet/londatiga/android/QuickAction;->mChildPos:I

    invoke-direct {p0}, Lnet/londatiga/android/QuickAction;->createColorSelectors()V

    return-void

    :cond_0
    const v0, 0x7f030024    # com.konka.avenger.R.layout.quickaction_popup_vertical

    invoke-virtual {p0, v0}, Lnet/londatiga/android/QuickAction;->setContentView(I)V

    goto :goto_0
.end method

.method static synthetic access$0(Lnet/londatiga/android/QuickAction;)Lnet/londatiga/android/QuickAction$OnActionItemClickListener;
    .locals 1

    iget-object v0, p0, Lnet/londatiga/android/QuickAction;->mItemClickListener:Lnet/londatiga/android/QuickAction$OnActionItemClickListener;

    return-object v0
.end method

.method static synthetic access$1(Lnet/londatiga/android/QuickAction;Z)V
    .locals 0

    iput-boolean p1, p0, Lnet/londatiga/android/QuickAction;->mDidAction:Z

    return-void
.end method

.method private createColorSelectors()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    new-array v2, v7, [[I

    new-array v3, v5, [I

    const v4, 0x10100a7    # android.R.attr.state_pressed

    aput v4, v3, v6

    aput-object v3, v2, v6

    new-array v3, v5, [I

    const v4, 0x101009c    # android.R.attr.state_focused

    aput v4, v3, v6

    aput-object v3, v2, v5

    const/4 v3, 0x2

    new-array v4, v5, [I

    aput-object v4, v2, v3

    new-array v0, v7, [I

    fill-array-data v0, :array_0

    new-array v1, v7, [I

    fill-array-data v1, :array_1

    new-instance v3, Landroid/content/res/ColorStateList;

    invoke-direct {v3, v2, v0}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v3, p0, Lnet/londatiga/android/QuickAction;->ACCESSIBLE_COLOR:Landroid/content/res/ColorStateList;

    new-instance v3, Landroid/content/res/ColorStateList;

    invoke-direct {v3, v2, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v3, p0, Lnet/londatiga/android/QuickAction;->INACCESSIBLE_COLOR:Landroid/content/res/ColorStateList;

    return-void

    nop

    :array_0
    .array-data 4
        -0x87e2
        -0x87e2
        -0x1
    .end array-data

    :array_1
    .array-data 4
        -0x87e2
        -0x87e2
        -0xaaaaab
    .end array-data
.end method


# virtual methods
.method public addActionItem(Lnet/londatiga/android/ActionItem;)V
    .locals 1
    .param p1    # Lnet/londatiga/android/ActionItem;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;II)V

    return-void
.end method

.method public addActionItem(Lnet/londatiga/android/ActionItem;I)V
    .locals 1
    .param p1    # Lnet/londatiga/android/ActionItem;
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;II)V

    return-void
.end method

.method public addActionItem(Lnet/londatiga/android/ActionItem;II)V
    .locals 15
    .param p1    # Lnet/londatiga/android/ActionItem;
    .param p2    # I
    .param p3    # I

    iget-object v11, p0, Lnet/londatiga/android/QuickAction;->actionItems:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p1 .. p1}, Lnet/londatiga/android/ActionItem;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lnet/londatiga/android/ActionItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget v11, p0, Lnet/londatiga/android/QuickAction;->mOrientation:I

    if-nez v11, :cond_4

    iget-object v11, p0, Lnet/londatiga/android/QuickAction;->mInflater:Landroid/view/LayoutInflater;

    const v12, 0x7f030021    # com.konka.avenger.R.layout.quickaction_item_horizontal

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    :goto_0
    move-object v9, v2

    check-cast v9, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    :cond_0
    if-eqz p3, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lnet/londatiga/android/QuickAction;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    if-eqz v5, :cond_2

    iget v11, p0, Lnet/londatiga/android/QuickAction;->mOrientation:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_5

    move-object v11, v5

    :goto_2
    iget v12, p0, Lnet/londatiga/android/QuickAction;->mOrientation:I

    if-nez v12, :cond_6

    :goto_3
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v11, v5, v12, v13}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz v10, :cond_7

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    iget v7, p0, Lnet/londatiga/android/QuickAction;->mChildPos:I

    invoke-virtual/range {p1 .. p1}, Lnet/londatiga/android/ActionItem;->getActionId()I

    move-result v1

    new-instance v11, Lnet/londatiga/android/QuickAction$1;

    invoke-direct {v11, p0, v7, v1}, Lnet/londatiga/android/QuickAction$1;-><init>(Lnet/londatiga/android/QuickAction;II)V

    invoke-virtual {v2, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setId(I)V

    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/view/View;->setFocusable(Z)V

    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/view/View;->setClickable(Z)V

    iget v11, p0, Lnet/londatiga/android/QuickAction;->mOrientation:I

    if-nez v11, :cond_3

    iget v11, p0, Lnet/londatiga/android/QuickAction;->mChildPos:I

    if-eqz v11, :cond_3

    iget-object v11, p0, Lnet/londatiga/android/QuickAction;->mInflater:Landroid/view/LayoutInflater;

    const v12, 0x7f030020    # com.konka.avenger.R.layout.quickaction_horiz_separator

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v11, -0x2

    const/4 v12, -0x1

    invoke-direct {v6, v11, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v11, 0x5

    const/4 v12, 0x0

    const/4 v13, 0x5

    const/4 v14, 0x0

    invoke-virtual {v8, v11, v12, v13, v14}, Landroid/view/View;->setPadding(IIII)V

    iget-object v11, p0, Lnet/londatiga/android/QuickAction;->mContentView:Landroid/view/ViewGroup;

    iget v12, p0, Lnet/londatiga/android/QuickAction;->mInsertPos:I

    invoke-virtual {v11, v8, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget v11, p0, Lnet/londatiga/android/QuickAction;->mInsertPos:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lnet/londatiga/android/QuickAction;->mInsertPos:I

    :cond_3
    iget-object v11, p0, Lnet/londatiga/android/QuickAction;->mContentView:Landroid/view/ViewGroup;

    iget v12, p0, Lnet/londatiga/android/QuickAction;->mInsertPos:I

    invoke-virtual {v11, v2, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget v11, p0, Lnet/londatiga/android/QuickAction;->mChildPos:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lnet/londatiga/android/QuickAction;->mChildPos:I

    iget v11, p0, Lnet/londatiga/android/QuickAction;->mInsertPos:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lnet/londatiga/android/QuickAction;->mInsertPos:I

    return-void

    :cond_4
    iget-object v11, p0, Lnet/londatiga/android/QuickAction;->mInflater:Landroid/view/LayoutInflater;

    const v12, 0x7f030022    # com.konka.avenger.R.layout.quickaction_item_vertical

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_0

    :catch_0
    move-exception v4

    const-string v11, "QuickAction"

    const-string v12, "resorting to constant color!"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_7
    const/16 v11, 0x8

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public getActionItem(I)Lnet/londatiga/android/ActionItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lnet/londatiga/android/QuickAction;->actionItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/londatiga/android/ActionItem;

    return-object v0
.end method

.method public onDismiss()V
    .locals 1

    iget-boolean v0, p0, Lnet/londatiga/android/QuickAction;->mDidAction:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/londatiga/android/QuickAction;->mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/londatiga/android/QuickAction;->mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    invoke-interface {v0}, Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;->onDismiss()V

    :cond_0
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p1, p2, p3}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x52

    if-ne p2, v1, :cond_1

    invoke-virtual {p0}, Lnet/londatiga/android/QuickAction;->dismiss()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setActionItemAccessible(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z

    iget-object v2, p0, Lnet/londatiga/android/QuickAction;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v0, p2}, Landroid/view/View;->setClickable(Z)V

    instance-of v2, v0, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    if-eqz p2, :cond_1

    iget-object v2, p0, Lnet/londatiga/android/QuickAction;->ACCESSIBLE_COLOR:Landroid/content/res/ColorStateList;

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_0
    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_1
    iget-object v2, p0, Lnet/londatiga/android/QuickAction;->INACCESSIBLE_COLOR:Landroid/content/res/ColorStateList;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public setOnActionItemClickListener(Lnet/londatiga/android/QuickAction$OnActionItemClickListener;)V
    .locals 0
    .param p1    # Lnet/londatiga/android/QuickAction$OnActionItemClickListener;

    iput-object p1, p0, Lnet/londatiga/android/QuickAction;->mItemClickListener:Lnet/londatiga/android/QuickAction$OnActionItemClickListener;

    return-void
.end method
