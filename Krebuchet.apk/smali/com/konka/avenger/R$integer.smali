.class public final Lcom/konka/avenger/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final apps_customize_cling_focused_x:I = 0x7f0b002a

.field public static final apps_customize_cling_focused_y:I = 0x7f0b002b

.field public static final apps_customize_maxCellCountX:I = 0x7f0b0026

.field public static final apps_customize_maxCellCountY:I = 0x7f0b0027

.field public static final apps_customize_widget_cell_count_x:I = 0x7f0b0028

.field public static final apps_customize_widget_cell_count_y:I = 0x7f0b0029

.field public static final config_appsCustomizeDragSlopeThreshold:I = 0x7f0b0014

.field public static final config_appsCustomizeFadeInTime:I = 0x7f0b000e

.field public static final config_appsCustomizeFadeOutTime:I = 0x7f0b000f

.field public static final config_appsCustomizeSpringLoadedBgAlpha:I = 0x7f0b0000

.field public static final config_appsCustomizeWorkspaceAnimationStagger:I = 0x7f0b0011

.field public static final config_appsCustomizeWorkspaceShrinkTime:I = 0x7f0b0010

.field public static final config_appsCustomizeZoomInTime:I = 0x7f0b000b

.field public static final config_appsCustomizeZoomOutTime:I = 0x7f0b000c

.field public static final config_appsCustomizeZoomScaleFactor:I = 0x7f0b000d

.field public static final config_crosshairsFadeInTime:I = 0x7f0b0016

.field public static final config_dragAppsCustomizeIconFadeAlpha:I = 0x7f0b0003

.field public static final config_dragAppsCustomizeIconFadeInDuration:I = 0x7f0b0001

.field public static final config_dragAppsCustomizeIconFadeOutDuration:I = 0x7f0b0002

.field public static final config_dragOutlineFadeTime:I = 0x7f0b0018

.field public static final config_dragOutlineMaxAlpha:I = 0x7f0b0019

.field public static final config_dragViewExtraPixels:I = 0x7f0b0017

.field public static final config_dropAnimMaxDist:I = 0x7f0b001c

.field public static final config_dropAnimMaxDuration:I = 0x7f0b001a

.field public static final config_dropTargetBgTransitionDuration:I = 0x7f0b0015

.field public static final config_folderAnimDuration:I = 0x7f0b001b

.field public static final config_maxScaleForUsingWorkspaceScreenBitmapCache:I = 0x7f0b001e

.field public static final config_screenOnDropAlphaFadeDelay:I = 0x7f0b0009

.field public static final config_screenOnDropAlphaFadeDuration:I = 0x7f0b000a

.field public static final config_screenOnDropScaleDownDuration:I = 0x7f0b0008

.field public static final config_screenOnDropScalePercent:I = 0x7f0b0006

.field public static final config_screenOnDropScaleUpDuration:I = 0x7f0b0007

.field public static final config_tabTransitionDuration:I = 0x7f0b0013

.field public static final config_workspaceAppsCustomizeAnimationStagger:I = 0x7f0b0012

.field public static final config_workspaceColumns:I = 0x7f0b0024

.field public static final config_workspaceRows:I = 0x7f0b0023

.field public static final config_workspaceScreenBitmapCacheScale:I = 0x7f0b001d

.field public static final config_workspaceShrinkPercent:I = 0x7f0b001f

.field public static final config_workspaceSpringLoadShrinkPercentage:I = 0x7f0b0005

.field public static final config_workspaceUnshrinkTime:I = 0x7f0b0004

.field public static final default_workspace_page:I = 0x7f0b0025

.field public static final folder_max_count_x:I = 0x7f0b0020

.field public static final folder_max_count_y:I = 0x7f0b0021

.field public static final folder_max_num_items:I = 0x7f0b0022


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
