.class public final Lcom/konka/avenger/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AppsCustomizePagedView:[I

.field public static final AppsCustomizePagedView_clingFocusedX:I = 0x6

.field public static final AppsCustomizePagedView_clingFocusedY:I = 0x7

.field public static final AppsCustomizePagedView_maxAppCellCountX:I = 0x0

.field public static final AppsCustomizePagedView_maxAppCellCountY:I = 0x1

.field public static final AppsCustomizePagedView_widgetCellHeightGap:I = 0x3

.field public static final AppsCustomizePagedView_widgetCellWidthGap:I = 0x2

.field public static final AppsCustomizePagedView_widgetCountX:I = 0x4

.field public static final AppsCustomizePagedView_widgetCountY:I = 0x5

.field public static final CellLayout:[I

.field public static final CellLayout_cellHeight:I = 0x1

.field public static final CellLayout_cellWidth:I = 0x0

.field public static final CellLayout_heightGap:I = 0x3

.field public static final CellLayout_maxGap:I = 0x4

.field public static final CellLayout_widthGap:I = 0x2

.field public static final Cling:[I

.field public static final Cling_drawIdentifier:I = 0x0

.field public static final DoubleNumberPickerPreference:[I

.field public static final DoubleNumberPickerPreference_defaultValue1:I = 0x0

.field public static final DoubleNumberPickerPreference_defaultValue2:I = 0x1

.field public static final DoubleNumberPickerPreference_max1:I = 0x2

.field public static final DoubleNumberPickerPreference_max2:I = 0x4

.field public static final DoubleNumberPickerPreference_maxExternal1:I = 0x6

.field public static final DoubleNumberPickerPreference_maxExternal2:I = 0x8

.field public static final DoubleNumberPickerPreference_min1:I = 0x3

.field public static final DoubleNumberPickerPreference_min2:I = 0x5

.field public static final DoubleNumberPickerPreference_minExternal1:I = 0x7

.field public static final DoubleNumberPickerPreference_minExternal2:I = 0x9

.field public static final DoubleNumberPickerPreference_pickerTitle1:I = 0xa

.field public static final DoubleNumberPickerPreference_pickerTitle2:I = 0xb

.field public static final Favorite:[I

.field public static final Favorite_className:I = 0x0

.field public static final Favorite_container:I = 0x2

.field public static final Favorite_icon:I = 0x8

.field public static final Favorite_lock:I = 0xb

.field public static final Favorite_packageName:I = 0x1

.field public static final Favorite_screen:I = 0x3

.field public static final Favorite_spanX:I = 0x6

.field public static final Favorite_spanY:I = 0x7

.field public static final Favorite_title:I = 0x9

.field public static final Favorite_uri:I = 0xa

.field public static final Favorite_x:I = 0x4

.field public static final Favorite_y:I = 0x5

.field public static final HolographicLinearLayout:[I

.field public static final HolographicLinearLayout_sourceImageViewId:I = 0x0

.field public static final Hotseat:[I

.field public static final Hotseat_cellCountX:I = 0x0

.field public static final Hotseat_cellCountY:I = 0x1

.field public static final NumberPickerPreference:[I

.field public static final NumberPickerPreference_max:I = 0x0

.field public static final NumberPickerPreference_maxExternal:I = 0x2

.field public static final NumberPickerPreference_min:I = 0x1

.field public static final NumberPickerPreference_minExternal:I = 0x3

.field public static final PageIndicator:[I

.field public static final PageIndicator_activeDot:I = 0x1

.field public static final PageIndicator_dotCount:I = 0x0

.field public static final PageIndicator_dotDrawable:I = 0x2

.field public static final PageIndicator_dotSpacing:I = 0x3

.field public static final PageIndicator_dotType:I = 0x5

.field public static final PageIndicator_gravity:I = 0x4

.field public static final PagedView:[I

.field public static final PagedViewIcon:[I

.field public static final PagedViewIcon_blurColor:I = 0x0

.field public static final PagedViewIcon_outlineColor:I = 0x1

.field public static final PagedViewWidget:[I

.field public static final PagedViewWidget_blurColor:I = 0x0

.field public static final PagedViewWidget_outlineColor:I = 0x1

.field public static final PagedView_pageLayoutHeightGap:I = 0x1

.field public static final PagedView_pageLayoutPaddingBottom:I = 0x3

.field public static final PagedView_pageLayoutPaddingLeft:I = 0x4

.field public static final PagedView_pageLayoutPaddingRight:I = 0x5

.field public static final PagedView_pageLayoutPaddingTop:I = 0x2

.field public static final PagedView_pageLayoutWidthGap:I = 0x0

.field public static final PagedView_pageSpacing:I = 0x6

.field public static final PagedView_scrollIndicatorPaddingLeft:I = 0x7

.field public static final PagedView_scrollIndicatorPaddingRight:I = 0x8

.field public static final Workspace:[I

.field public static final Workspace_cellCountX:I = 0x0

.field public static final Workspace_cellCountY:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0xc

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/avenger/R$styleable;->AppsCustomizePagedView:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/konka/avenger/R$styleable;->CellLayout:[I

    new-array v0, v4, [I

    const/high16 v1, 0x7f010000    # com.konka.avenger.R.attr.drawIdentifier

    aput v1, v0, v3

    sput-object v0, Lcom/konka/avenger/R$styleable;->Cling:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/konka/avenger/R$styleable;->DoubleNumberPickerPreference:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/konka/avenger/R$styleable;->Favorite:[I

    new-array v0, v4, [I

    const v1, 0x7f010008    # com.konka.avenger.R.attr.sourceImageViewId

    aput v1, v0, v3

    sput-object v0, Lcom/konka/avenger/R$styleable;->HolographicLinearLayout:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/konka/avenger/R$styleable;->Hotseat:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/konka/avenger/R$styleable;->NumberPickerPreference:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/konka/avenger/R$styleable;->PageIndicator:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/konka/avenger/R$styleable;->PagedView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/konka/avenger/R$styleable;->PagedViewIcon:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/konka/avenger/R$styleable;->PagedViewWidget:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/konka/avenger/R$styleable;->Workspace:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010014    # com.konka.avenger.R.attr.maxAppCellCountX
        0x7f010015    # com.konka.avenger.R.attr.maxAppCellCountY
        0x7f010016    # com.konka.avenger.R.attr.widgetCellWidthGap
        0x7f010017    # com.konka.avenger.R.attr.widgetCellHeightGap
        0x7f010018    # com.konka.avenger.R.attr.widgetCountX
        0x7f010019    # com.konka.avenger.R.attr.widgetCountY
        0x7f01001a    # com.konka.avenger.R.attr.clingFocusedX
        0x7f01001b    # com.konka.avenger.R.attr.clingFocusedY
    .end array-data

    :array_1
    .array-data 4
        0x7f010003    # com.konka.avenger.R.attr.cellWidth
        0x7f010004    # com.konka.avenger.R.attr.cellHeight
        0x7f010005    # com.konka.avenger.R.attr.widthGap
        0x7f010006    # com.konka.avenger.R.attr.heightGap
        0x7f010007    # com.konka.avenger.R.attr.maxGap
    .end array-data

    :array_2
    .array-data 4
        0x7f01002c    # com.konka.avenger.R.attr.defaultValue1
        0x7f01002d    # com.konka.avenger.R.attr.defaultValue2
        0x7f01002e    # com.konka.avenger.R.attr.max1
        0x7f01002f    # com.konka.avenger.R.attr.min1
        0x7f010030    # com.konka.avenger.R.attr.max2
        0x7f010031    # com.konka.avenger.R.attr.min2
        0x7f010032    # com.konka.avenger.R.attr.maxExternal1
        0x7f010033    # com.konka.avenger.R.attr.minExternal1
        0x7f010034    # com.konka.avenger.R.attr.maxExternal2
        0x7f010035    # com.konka.avenger.R.attr.minExternal2
        0x7f010036    # com.konka.avenger.R.attr.pickerTitle1
        0x7f010037    # com.konka.avenger.R.attr.pickerTitle2
    .end array-data

    :array_3
    .array-data 4
        0x7f01001c    # com.konka.avenger.R.attr.className
        0x7f01001d    # com.konka.avenger.R.attr.packageName
        0x7f01001e    # com.konka.avenger.R.attr.container
        0x7f01001f    # com.konka.avenger.R.attr.screen
        0x7f010020    # com.konka.avenger.R.attr.x
        0x7f010021    # com.konka.avenger.R.attr.y
        0x7f010022    # com.konka.avenger.R.attr.spanX
        0x7f010023    # com.konka.avenger.R.attr.spanY
        0x7f010024    # com.konka.avenger.R.attr.icon
        0x7f010025    # com.konka.avenger.R.attr.title
        0x7f010026    # com.konka.avenger.R.attr.uri
        0x7f010027    # com.konka.avenger.R.attr.lock
    .end array-data

    :array_4
    .array-data 4
        0x7f010001    # com.konka.avenger.R.attr.cellCountX
        0x7f010002    # com.konka.avenger.R.attr.cellCountY
    .end array-data

    :array_5
    .array-data 4
        0x7f010028    # com.konka.avenger.R.attr.max
        0x7f010029    # com.konka.avenger.R.attr.min
        0x7f01002a    # com.konka.avenger.R.attr.maxExternal
        0x7f01002b    # com.konka.avenger.R.attr.minExternal
    .end array-data

    :array_6
    .array-data 4
        0x7f010039    # com.konka.avenger.R.attr.dotCount
        0x7f01003a    # com.konka.avenger.R.attr.activeDot
        0x7f01003b    # com.konka.avenger.R.attr.dotDrawable
        0x7f01003c    # com.konka.avenger.R.attr.dotSpacing
        0x7f01003d    # com.konka.avenger.R.attr.gravity
        0x7f01003e    # com.konka.avenger.R.attr.dotType
    .end array-data

    :array_7
    .array-data 4
        0x7f01000b    # com.konka.avenger.R.attr.pageLayoutWidthGap
        0x7f01000c    # com.konka.avenger.R.attr.pageLayoutHeightGap
        0x7f01000d    # com.konka.avenger.R.attr.pageLayoutPaddingTop
        0x7f01000e    # com.konka.avenger.R.attr.pageLayoutPaddingBottom
        0x7f01000f    # com.konka.avenger.R.attr.pageLayoutPaddingLeft
        0x7f010010    # com.konka.avenger.R.attr.pageLayoutPaddingRight
        0x7f010011    # com.konka.avenger.R.attr.pageSpacing
        0x7f010012    # com.konka.avenger.R.attr.scrollIndicatorPaddingLeft
        0x7f010013    # com.konka.avenger.R.attr.scrollIndicatorPaddingRight
    .end array-data

    :array_8
    .array-data 4
        0x7f010009    # com.konka.avenger.R.attr.blurColor
        0x7f01000a    # com.konka.avenger.R.attr.outlineColor
    .end array-data

    :array_9
    .array-data 4
        0x7f010009    # com.konka.avenger.R.attr.blurColor
        0x7f01000a    # com.konka.avenger.R.attr.outlineColor
    .end array-data

    :array_a
    .array-data 4
        0x7f010001    # com.konka.avenger.R.attr.cellCountX
        0x7f010002    # com.konka.avenger.R.attr.cellCountY
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
