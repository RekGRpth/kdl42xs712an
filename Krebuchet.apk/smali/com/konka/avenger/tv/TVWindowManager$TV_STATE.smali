.class public final enum Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;
.super Ljava/lang/Enum;
.source "TVWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/tv/TVWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TV_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CLOSE:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

.field public static final enum OPEN:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    const-string v1, "OPEN"

    invoke-direct {v0, v1, v2}, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->OPEN:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    new-instance v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    const-string v1, "CLOSE"

    invoke-direct {v0, v1, v3}, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->CLOSE:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    sget-object v1, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->OPEN:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->CLOSE:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->ENUM$VALUES:[Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;
    .locals 1

    const-class v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    return-object v0
.end method

.method public static values()[Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->ENUM$VALUES:[Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
