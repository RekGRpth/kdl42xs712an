.class public Lcom/konka/avenger/tv/TvController;
.super Ljava/lang/Object;
.source "TvController.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I = null

.field public static final TIME_DELAY_CONTROL_TV:I = 0xc8

.field private static mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;


# instance fields
.field private OSD_WIDTH:I

.field private final TAG:Ljava/lang/String;

.field private final TIME_DELAY_SWITCH_SOURCE:I

.field private final TV_CLOSE:I

.field private final TV_DISMISS_AUDIO_MUTE:I

.field private final TV_DISMISS_VIDEO_MUTE:I

.field private final TV_SHOW:I

.field private final TV_SWITCH_SOURCE_TO_STORAGE:I

.field private TV_WINDOW_PADDING:I

.field private isTvWindowShow:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mNeedScaleWindow:Z

.field private mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

.field private mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mTVRect:Landroid/graphics/Rect;

.field private mTvWindowView:Landroid/view/SurfaceView;

.field private mfResolutionRatio:D


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I
    .locals 3

    sget-object v0, Lcom/konka/avenger/tv/TvController;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_d
    sput-object v0, Lcom/konka/avenger/tv/TvController;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_d

    :catch_1
    move-exception v1

    goto :goto_c

    :catch_2
    move-exception v1

    goto :goto_b

    :catch_3
    move-exception v1

    goto :goto_a

    :catch_4
    move-exception v1

    goto :goto_9

    :catch_5
    move-exception v1

    goto :goto_8

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_6

    :catch_8
    move-exception v1

    goto :goto_5

    :catch_9
    move-exception v1

    goto :goto_4

    :catch_a
    move-exception v1

    goto :goto_3

    :catch_b
    move-exception v1

    goto/16 :goto_2

    :catch_c
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    sput-object v0, Lcom/konka/avenger/tv/TvController;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v3, "TvWindowController"

    iput-object v3, p0, Lcom/konka/avenger/tv/TvController;->TAG:Ljava/lang/String;

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/avenger/tv/TvController;->isTvWindowShow:Z

    iput-boolean v5, p0, Lcom/konka/avenger/tv/TvController;->mNeedScaleWindow:Z

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v3, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iput-object v4, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    const/16 v3, 0x500

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->OSD_WIDTH:I

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    iput-wide v3, p0, Lcom/konka/avenger/tv/TvController;->mfResolutionRatio:D

    iput v5, p0, Lcom/konka/avenger/tv/TvController;->TV_SHOW:I

    const/4 v3, 0x2

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->TV_CLOSE:I

    const/4 v3, 0x3

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->TV_SWITCH_SOURCE_TO_STORAGE:I

    const/4 v3, 0x4

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->TV_DISMISS_AUDIO_MUTE:I

    const/4 v3, 0x5

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->TV_DISMISS_VIDEO_MUTE:I

    const/16 v3, 0x64

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->TIME_DELAY_SWITCH_SOURCE:I

    const/4 v3, 0x6

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->TV_WINDOW_PADDING:I

    iput-object p1, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    move-object v3, p1

    check-cast v3, Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object v3, p0, Lcom/konka/avenger/tv/TvController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v3, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->OSD_WIDTH:I

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    int-to-double v3, v3

    iget v5, p0, Lcom/konka/avenger/tv/TvController;->OSD_WIDTH:I

    int-to-double v5, v5

    div-double/2addr v3, v5

    iput-wide v3, p0, Lcom/konka/avenger/tv/TvController;->mfResolutionRatio:D

    iget v3, p0, Lcom/konka/avenger/tv/TvController;->TV_WINDOW_PADDING:I

    int-to-float v3, v3

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/avenger/tv/TvController;->TV_WINDOW_PADDING:I

    const-string v3, "TvWindowController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "The panel Size=====["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "X"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "osd_width"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/konka/avenger/tv/TvController;->OSD_WIDTH:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]@@@@@The ratio is ==="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/avenger/tv/TvController;->mfResolutionRatio:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " the density==="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/konka/avenger/tv/TvController;->createTvWidgetView(Landroid/content/Context;)V

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0()Lcom/mstar/android/tvapi/common/TvPlayer;
    .locals 1

    sget-object v0, Lcom/konka/avenger/tv/TvController;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/avenger/tv/TvController;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TvController;->scaleSmallWindow()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/avenger/tv/TvController;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/avenger/tv/TvController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/avenger/tv/TvController;->mNeedScaleWindow:Z

    return-void
.end method

.method static synthetic access$4(Lcom/konka/avenger/tv/TvController;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method private closeTvWindow()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TvController;->isTvWindowShow:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TvController;->isTvWindowShow:Z

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    :cond_1
    invoke-direct {p0}, Lcom/konka/avenger/tv/TvController;->setInputSource2Storage()V

    goto :goto_0
.end method

.method private createTvWidgetView(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getTVWindowRect()Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    const-string v2, "TvWindowController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get the original rect size====[[[[[[[[[[[[[[the [x,y][w,h]=====["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/konka/avenger/tv/TvController;->TV_WINDOW_PADDING:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/konka/avenger/tv/TvController;->TV_WINDOW_PADDING:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/konka/avenger/tv/TvController;->TV_WINDOW_PADDING:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lcom/konka/avenger/tv/TvController;->TV_WINDOW_PADDING:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    invoke-virtual {v2, v1}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/konka/avenger/tv/TvController$1;

    invoke-direct {v0, p0}, Lcom/konka/avenger/tv/TvController$1;-><init>(Lcom/konka/avenger/tv/TvController;)V

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method

.method private dismuteVideo()V
    .locals 6

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "TvWindowController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "========dissmuteVideo=========the source==="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/16 v4, 0x378

    iget-object v5, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private getCurInputsoureFromDB()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 8

    const/4 v2, 0x0

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TvController;->mContentResolver:Landroid/content/ContentResolver;

    :cond_0
    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    const-string v1, "enInputSourceType"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_1
    return-object v7
.end method

.method private isLauncherOnTop()Z
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    const-string v5, "activity"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v2, "TvWindowController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the top activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.iflytek.vaf"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_0

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v2, "TvWindowController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the second activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.avenger"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "TvWindowController"

    const-string v4, "the launcher is on the top task list"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    :goto_0
    return v2

    :cond_1
    move v2, v4

    goto :goto_0
.end method

.method private isMediaAppOnTop()Z
    .locals 8

    const/4 v3, 0x0

    const/4 v7, 0x2

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    const-string v5, "activity"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v2, "TvWindowController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the top activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.iflytek.vaf"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.hotkey"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v4, :cond_1

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v2, "TvWindowController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the second activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.tvsettings"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.karaoke"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string v2, "TvWindowController"

    const-string v3, "the media app is on the top task list"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v4

    :goto_0
    return v2

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method private muteVideo()V
    .locals 6

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "TvWindowController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "========muteVideo=========the source==="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const/4 v2, 0x1

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/16 v4, 0x378

    iget-object v5, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private openTVMute()V
    .locals 5

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v2, "TvWindowController"

    const-string v3, "open the mute!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    const-string v2, "TvWindowController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "open the mute,the current inputsource =="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    const/16 v3, 0xe1

    invoke-virtual {v2, v3, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setMuteStatus(ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    const/16 v3, 0xf1

    invoke-virtual {v2, v3, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setMuteStatus(ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private scaleSmallWindow()V
    .locals 8

    iget-boolean v4, p0, Lcom/konka/avenger/tv/TvController;->mNeedScaleWindow:Z

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/konka/avenger/tv/TvController;->mNeedScaleWindow:Z

    new-instance v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v3}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-double v4, v4

    iget-wide v6, p0, Lcom/konka/avenger/tv/TvController;->mfResolutionRatio:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-double v4, v4

    iget-wide v6, p0, Lcom/konka/avenger/tv/TvController;->mfResolutionRatio:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-double v4, v4

    iget-wide v6, p0, Lcom/konka/avenger/tv/TvController;->mfResolutionRatio:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mTVRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-double v4, v4

    iget-wide v6, p0, Lcom/konka/avenger/tv/TvController;->mfResolutionRatio:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    const-string v4, "TvWindowController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[[[[[[[[[[[[[[the [x,y][w,h]=====["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z

    iget-object v4, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[wjx]current source ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[wjx]backlight = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v5

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private setInputSource2Storage()V
    .locals 2

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TvWindowController"

    const-string v1, "can not setInputSource2Storage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/konka/avenger/tv/TvController;->isMediaAppOnTop()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "TvWindowController"

    const-string v1, "the Media app is on top, do not need to switch to MM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/konka/avenger/tv/TvController$2;

    const-string v1, "setInputSource2Storage"

    invoke-direct {v0, p0, v1}, Lcom/konka/avenger/tv/TvController$2;-><init>(Lcom/konka/avenger/tv/TvController;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TvController$2;->start()V

    goto :goto_0
.end method

.method private showTvWindow()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TvController;->isTvWindowShow:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVAppOnTop()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "TvWindowController"

    const-string v1, "can not showTvWindow "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TvController;->isTvWindowShow:Z

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->closePIP()V

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->setInputSource2TV()V

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->close3D()V

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public changeScaleWindowStatus(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/avenger/tv/TvController;->mNeedScaleWindow:Z

    return-void
.end method

.method public close3D()V
    .locals 5

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "TvWindowController"

    const-string v3, "can not close 3D "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    const-string v2, "TvWindowController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "====================================get current 3d mode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/avenger/tv/TvController;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    goto :goto_0

    :pswitch_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public closePIP()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    if-nez v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    :cond_0
    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disablePip"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_1
    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disablePop"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController;->mPipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_2
    return-void
.end method

.method public closeTV()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TvController;->closeTvWindow()V

    return-void
.end method

.method public closeTvWindowforSTR()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TvController;->isTvWindowShow:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/avenger/tv/TvController;->mTvWindowView:Landroid/view/SurfaceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TvController;->isTvWindowShow:Z

    goto :goto_0
.end method

.method public destroyTvWidget()V
    .locals 0

    return-void
.end method

.method public dismissTVMute()V
    .locals 5

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "TvWindowController"

    const-string v3, "can not dismissTVMute"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    const-string v2, "TvWindowController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dismiss the mute,the current inputsource =="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    const/16 v3, 0xe0

    invoke-virtual {v2, v3, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setMuteStatus(ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    const/16 v3, 0xf0

    invoke-virtual {v2, v3, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setMuteStatus(ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v2, "TvWindowController"

    const-string v3, "dismiss the mute!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public isTVAppOnTop()Z
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController;->mContext:Landroid/content/Context;

    const-string v5, "activity"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v2, "TvWindowController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the top activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.iflytek.vaf"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_0

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v2, "TvWindowController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the second activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.tvsettings"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "TvWindowController"

    const-string v4, "the TV app is on the top task list"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    :goto_0
    return v2

    :cond_1
    move v2, v4

    goto :goto_0
.end method

.method public isTVServiceAlive()Z
    .locals 4

    const-string v1, "1"

    const-string v2, "mstar.str.suspending"

    const-string v3, "0"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "TvWindowController"

    const-string v2, "tv service is not alive!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public scaleFullWindow()V
    .locals 4

    const v3, 0xffff

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "TvWindowController"

    const-string v3, "can not scaleFullWindow"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/avenger/tv/TvController;->mNeedScaleWindow:Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setInputSource2MM()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TvController;->setInputSource2Storage()V

    return-void
.end method

.method public setInputSource2TV()V
    .locals 6

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "TvWindowController"

    const-string v4, "can not setInputSource2TV "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    const-string v3, "TvWindowController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "================getCurrentInputSource==== "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    if-ge v3, v4, :cond_1

    iput-object v1, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v3, "TvWindowController"

    const-string v4, "the current input source is TV source,do not switch!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0}, Lcom/konka/avenger/tv/TvController;->getCurInputsoureFromDB()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    const-string v3, "TvWindowController"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "=====================get the input source from DB ==== "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_2

    const-string v3, "TvWindowController"

    const-string v4, "the current input source is not tv source,so switch to ATV"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :cond_2
    iput-object v2, p0, Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-instance v3, Lcom/konka/avenger/tv/TvController$3;

    const-string v4, "setInputSource2TV"

    invoke-direct {v3, p0, v4}, Lcom/konka/avenger/tv/TvController$3;-><init>(Lcom/konka/avenger/tv/TvController;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/konka/avenger/tv/TvController$3;->start()V

    goto :goto_0
.end method

.method public showTV()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TvController;->showTvWindow()V

    return-void
.end method
