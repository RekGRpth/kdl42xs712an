.class Lcom/konka/avenger/tv/TVView$InputSourceThread;
.super Ljava/lang/Thread;
.source "TVView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/tv/TVView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InputSourceThread"
.end annotation


# instance fields
.field private bExitThread:Z

.field private tempIsSetTVInput:Z

.field private tempIsStorageInput:Z

.field final synthetic this$0:Lcom/konka/avenger/tv/TVView;


# direct methods
.method constructor <init>(Lcom/konka/avenger/tv/TVView;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->tempIsSetTVInput:Z

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->tempIsStorageInput:Z

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->bExitThread:Z

    return-void
.end method

.method static synthetic access$0(Lcom/konka/avenger/tv/TVView$InputSourceThread;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->bExitThread:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :goto_0
    iget-boolean v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->bExitThread:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    # getter for: Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/konka/avenger/tv/TVView;->access$2(Lcom/konka/avenger/tv/TVView;)Ljava/lang/Boolean;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    # getter for: Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/konka/avenger/tv/TVView;->access$3(Lcom/konka/avenger/tv/TVView;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->tempIsSetTVInput:Z

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/konka/avenger/tv/TVView;->access$4(Lcom/konka/avenger/tv/TVView;Ljava/lang/Boolean;)V

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    # getter for: Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/konka/avenger/tv/TVView;->access$5(Lcom/konka/avenger/tv/TVView;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->tempIsStorageInput:Z

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/konka/avenger/tv/TVView;->access$6(Lcom/konka/avenger/tv/TVView;Ljava/lang/Boolean;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->tempIsSetTVInput:Z

    if-eqz v1, :cond_2

    const-string v1, "InputSourceThread"

    const-string v2, "set tv input source and scaleSmallWindow"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v1}, Lcom/konka/avenger/tv/TVView;->setInputSource2TV()V

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v1}, Lcom/konka/avenger/tv/TVView;->scaleSmallWindow()V

    :cond_1
    :goto_1
    const-wide/16 v1, 0x64

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_2
    iget-boolean v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->tempIsStorageInput:Z

    if-eqz v1, :cond_1

    const-string v1, "InputSourceThread"

    const-string v2, "set storage input source "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView$InputSourceThread;->this$0:Lcom/konka/avenger/tv/TVView;

    # invokes: Lcom/konka/avenger/tv/TVView;->setInputSource2Storage()V
    invoke-static {v1}, Lcom/konka/avenger/tv/TVView;->access$7(Lcom/konka/avenger/tv/TVView;)V

    goto :goto_1
.end method
