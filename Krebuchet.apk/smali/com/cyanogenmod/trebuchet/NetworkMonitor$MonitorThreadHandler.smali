.class final Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;
.super Landroid/os/Handler;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MonitorThreadHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # invokes: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->testReachable()Z
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$0(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    :goto_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$1(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;

    move-result-object v2

    const/16 v3, 0x3e9

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$1(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->sendMessage(Landroid/os/Message;)Z

    const/16 v2, 0x7d1

    const-wide/16 v3, 0x1388

    invoke-virtual {p0, v2, v3, v4}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
    .end packed-switch
.end method
