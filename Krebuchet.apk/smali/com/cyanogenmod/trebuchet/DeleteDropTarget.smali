.class public Lcom/cyanogenmod/trebuchet/DeleteDropTarget;
.super Lcom/cyanogenmod/trebuchet/ButtonDropTarget;
.source "DeleteDropTarget.java"


# static fields
.field private static DELETE_ANIMATION_DURATION:I = 0x0

.field private static final MODE_DELETE:I = 0x0

.field private static final MODE_UNINSTALL:I = 0x1


# instance fields
.field private mCurrentDrawable:Landroid/graphics/drawable/Drawable;

.field private final mHandler:Landroid/os/Handler;

.field private mHoverColor:I

.field private mMode:I

.field private mOriginalTextColor:Landroid/content/res/ColorStateList;

.field private mRemoveActiveDrawable:Landroid/graphics/drawable/Drawable;

.field private mRemoveNormalDrawable:Landroid/graphics/drawable/Drawable;

.field private final mShowUninstaller:Ljava/lang/Runnable;

.field private mUninstall:Z

.field private mUninstallActiveDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xfa

    sput v0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->DELETE_ANIMATION_DURATION:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mMode:I

    const/high16 v0, -0x10000

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHoverColor:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget$1;-><init>(Lcom/cyanogenmod/trebuchet/DeleteDropTarget;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mShowUninstaller:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/DeleteDropTarget;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->switchToUninstallTarget()V

    return-void
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/DeleteDropTarget;Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->completeDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    return-void
.end method

.method private animateToTrashAndCompleteDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 17
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v2, v3, v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->getViewRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v5}, Lcom/cyanogenmod/trebuchet/DragLayer;->getViewRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v13

    iget v3, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getPaddingLeft()I

    move-result v6

    add-int/2addr v3, v6

    iget v6, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v7, v14

    iget v8, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v3, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v3, v14

    neg-int v3, v3

    div-int/lit8 v15, v3, 0x2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v3, v13

    neg-int v3, v3

    div-int/lit8 v16, v3, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Landroid/graphics/Rect;->offset(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->deferOnDragEnd()V

    new-instance v11, Lcom/cyanogenmod/trebuchet/DeleteDropTarget$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v11, v0, v1}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget$2;-><init>(Lcom/cyanogenmod/trebuchet/DeleteDropTarget;Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    const v6, 0x3dcccccd    # 0.1f

    const v7, 0x3dcccccd    # 0.1f

    sget v8, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->DELETE_ANIMATION_DURATION:I

    new-instance v9, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v10, 0x40000000    # 2.0f

    invoke-direct {v9, v10}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    new-instance v10, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v12, 0x3fc00000    # 1.5f

    invoke-direct {v10, v12}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    const/4 v12, 0x0

    invoke-virtual/range {v2 .. v12}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateView(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;FFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;Z)V

    return-void
.end method

.method private completeDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 6
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v2, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mMode:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v4, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    invoke-direct {p0, v4, v2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isWorkspaceOrFolderApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v4, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    goto :goto_0

    :cond_1
    iget-object v4, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    iget-object v5, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isWorkspaceFolder(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v1, v2

    check-cast v1, Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->removeFolder(Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v4, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteFolderContentsFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    goto :goto_0

    :cond_2
    iget-object v4, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    invoke-direct {p0, v4, v2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isWorkspaceWidget(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object v4, v2

    check-cast v4, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    invoke-virtual {v5, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->removeAppWidget(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v4, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    move-object v3, v2

    check-cast v3, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getAppWidgetHost()Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v4, Lcom/cyanogenmod/trebuchet/DeleteDropTarget$3;

    const-string v5, "deleteAppWidgetId"

    invoke-direct {v4, p0, v5, v0, v3}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget$3;-><init>(Lcom/cyanogenmod/trebuchet/DeleteDropTarget;Ljava/lang/String;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget$3;->start()V

    goto :goto_0

    :pswitch_1
    iget-object v4, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    invoke-direct {p0, v4, v2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isAllAppsApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    check-cast v2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    invoke-virtual {v4, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->startApplicationUninstallActivity(Lcom/cyanogenmod/trebuchet/ApplicationInfo;)V

    goto :goto_0

    :cond_3
    iget-object v4, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    invoke-direct {p0, v4, v2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isWorkspaceOrFolderApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    check-cast v2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {v4, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->startShortcutUninstallActivity(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private isAllAppsApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAllAppsItem(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isAllAppsApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isAllAppsWidget(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isAllAppsWidget(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDragSourceWorkspaceOrFolder(Lcom/cyanogenmod/trebuchet/DragSource;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/Workspace;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/Folder;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isWorkspaceFolder(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/Workspace;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWorkspaceOrFolderApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isDragSourceWorkspaceOrFolder(Lcom/cyanogenmod/trebuchet/DragSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWorkspaceWidget(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isDragSourceWorkspaceOrFolder(Lcom/cyanogenmod/trebuchet/DragSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private switchToUninstallTarget()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mUninstall:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mMode:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    const v0, 0x7f0a002e    # com.konka.avenger.R.string.delete_target_uninstall_label

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setText(I)V

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mUninstallActiveDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public acceptDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v0, 0x1

    return v0
.end method

.method public onDragEnd()V
    .locals 1

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->onDragEnd()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mActive:Z

    return-void
.end method

.method public onDragEnter(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 5
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->onDragEnter(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mUninstall:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mShowUninstaller:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mShowUninstaller:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mRemoveActiveDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v4, v4, v4}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    iget v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHoverColor:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setTextColor(I)V

    return-void
.end method

.method public onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mShowUninstaller:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragComplete:Z

    if-nez v0, :cond_1

    iput v3, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mMode:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isAllAppsItem(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a002d    # com.konka.avenger.R.string.cancel_target_label

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setText(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mRemoveNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mOriginalTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_1
    return-void

    :cond_2
    const v0, 0x7f0a002c    # com.konka.avenger.R.string.delete_target_label

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setText(I)V

    goto :goto_0
.end method

.method public onDragStart(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;I)V
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isAllAppsApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget v5, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mRemoveNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v5, v7, v7, v7}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aget-object v5, v5, v6

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mCurrentDrawable:Landroid/graphics/drawable/Drawable;

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mUninstall:Z

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mActive:Z

    iput v6, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mMode:I

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mOriginalTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isAllAppsItem(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const v5, 0x7f0a002d    # com.konka.avenger.R.string.cancel_target_label

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setText(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->isWorkspaceOrFolderApplication(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v4, p2

    check-cast v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v5, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v5, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const v5, 0x7f0a002c    # com.konka.avenger.R.string.delete_target_label

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setText(I)V

    goto :goto_1
.end method

.method public onDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->animateToTrashAndCompleteDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mOriginalTextColor:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f080000    # com.konka.avenger.R.color.delete_target_hover_tint

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHoverColor:I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHoverPaint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mHoverColor:I

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4, v5}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    const v2, 0x7f020076    # com.konka.avenger.R.drawable.ic_launcher_trashcan_active_holo

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mUninstallActiveDrawable:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f020070    # com.konka.avenger.R.drawable.ic_launcher_clear_active_holo

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mRemoveActiveDrawable:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f020071    # com.konka.avenger.R.drawable.ic_launcher_clear_normal_holo

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->mRemoveNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v0, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ""

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
