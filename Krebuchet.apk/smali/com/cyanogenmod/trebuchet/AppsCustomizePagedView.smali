.class public Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;
.super Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;
.source "AppsCustomizePagedView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Lcom/cyanogenmod/trebuchet/AppsCustomizeView;
.implements Lcom/cyanogenmod/trebuchet/DragSource;
.implements Lnet/londatiga/android/QuickAction$OnActionItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizePagedView$TransitionEffect:[I = null

.field private static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType:[I = null

.field private static final CAMERA_DISTANCE:F = 6500.0f

.field private static final ID_QUICKACTION_ADD_TO_HOMEPAGE:I = 0x1

.field private static final ID_QUICKACTION_RUN:I = 0x0

.field private static final ID_QUICKACTION_UNINSTALL:I = 0x2

.field static final LOG_TAG:Ljava/lang/String; = "AppsCustomizePagedView"

.field private static final PERFORM_OVERSCROLL_ROTATION:Z = true

.field private static final TRANSITION_MAX_ROTATION:F = 22.0f

.field private static final TRANSITION_PIVOT:F = 0.65f

.field private static final TRANSITION_SCALE_FACTOR:F = 0.74f

.field private static final TRANSITION_SCREEN_ROTATION:F = 12.5f

.field static final sLookAheadPageCount:I = 0x2

.field static final sLookBehindPageCount:I = 0x2

.field private static final sPageSleepDelay:I = 0xc8


# instance fields
.field private mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

.field private mAppIconSize:I

.field private final mAppListFileName:Ljava/lang/String;

.field private mApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCanvas:Landroid/graphics/Canvas;

.field private mClingFocusedX:I

.field private mClingFocusedY:I

.field private mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

.field private mContentWidth:I

.field private mCurSelectView:Landroid/view/View;

.field private mDefaultWidgetBackground:Landroid/graphics/drawable/Drawable;

.field private mDragController:Lcom/cyanogenmod/trebuchet/DragController;

.field private mDragViewMultiplyColor:I

.field private mFadeScrollingIndicator:Z

.field private mHasShownAllAppsCling:Z

.field private mHasShownAllAppsSortCling:Z

.field private mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

.field private mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

.field private mJoinWidgetsApps:Z

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLeftScreenAlphaInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private mMaxAppCellCountX:I

.field private mMaxAppCellCountY:I

.field private mNumAppsPages:I

.field private mNumWidgetPages:I

.field private mOverscrollTransformsDirty:Z

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mQuickAction:Lnet/londatiga/android/QuickAction;

.field mRunningTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private mSaveInstanceStateItemIndex:I

.field private mShowScrollingIndicator:Z

.field private mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

.field private mTransitionEffect:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

.field private mWidgetCountX:I

.field private mWidgetCountY:I

.field private mWidgetHeightGap:I

.field private final mWidgetPreviewIconPaddedDimension:I

.field private mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

.field private mWidgetWidthGap:I

.field private mWidgets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mZInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

.field private final sWidgetPreviewIconPaddingPercentage:F


# direct methods
.method static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizePagedView$TransitionEffect()[I
    .locals 3

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizePagedView$TransitionEffect:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->values()[Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->CubeIn:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->CubeOut:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->RotateDown:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->RotateUp:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->Stack:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->Standard:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->Tablet:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ZoomIn:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ZoomOut:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizePagedView$TransitionEffect:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType()[I
    .locals 3

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->values()[Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSaveInstanceStateItemIndex:I

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->KKDefault:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    const/high16 v2, 0x3e800000    # 0.25f

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->sWidgetPreviewIconPaddingPercentage:F

    iput v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    iput v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumWidgetPages:I

    new-instance v2, Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;-><init>(F)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mZInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mOverscrollTransformsDirty:Z

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    const v3, 0x3f666666    # 0.9f

    invoke-direct {v2, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLeftScreenAlphaInterpolator:Landroid/view/animation/DecelerateInterpolator;

    const-string v2, "defaultAppList"

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppListFileName:Ljava/lang/String;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/LauncherApplication;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getIconCache()Lcom/cyanogenmod/trebuchet/IconCache;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    new-instance v2, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-direct {v2}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCanvas:Landroid/graphics/Canvas;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02002e    # com.konka.avenger.R.drawable.default_widget_preview_holo

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDefaultWidgetBackground:Landroid/graphics/drawable/Drawable;

    const v2, 0x7f0c0022    # com.konka.avenger.R.dimen.app_icon_size

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    const v2, 0x7f080002    # com.konka.avenger.R.color.drag_view_multiply_color

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDragViewMultiplyColor:I

    sget-object v2, Lcom/konka/avenger/R$styleable;->AppsCustomizePagedView:[I

    invoke-virtual {p1, p2, v2, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mMaxAppCellCountX:I

    invoke-virtual {v0, v7, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mMaxAppCellCountY:I

    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetWidthGap:I

    invoke-virtual {v0, v8, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetHeightGap:I

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mClingFocusedX:I

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mClingFocusedY:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v2, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    int-to-float v2, v2

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetPreviewIconPaddedDimension:I

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHandleFadeInAdjacentScreens:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Drawer;->getJoinWidgetsApps(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    const/high16 v2, 0x7f0a0000    # com.konka.avenger.R.string.config_drawerDefaultTransitionEffect

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Drawer$Scrolling;->getTransitionEffect(Landroid/content/Context;Ljava/lang/String;)Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Drawer$Scrolling;->getFadeInAdjacentScreens(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeInAdjacentScreens:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Drawer$Indicator;->getShowScrollingIndicator(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mShowScrollingIndicator:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Drawer$Indicator;->getFadeScrollingIndicator(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeScrollingIndicator:Z

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mShowScrollingIndicator:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->disableScrollingIndicator()V

    :cond_0
    new-instance v2, Lnet/londatiga/android/QuickAction;

    invoke-direct {v2, p1, v4, v8}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->initQuickActionMenu()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->loadAppListFile()V

    return-void
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)Lcom/cyanogenmod/trebuchet/Launcher;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    return-object v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    .locals 1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->loadWidgetPreviewsInBackground(Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V

    return-void
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    return v0
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    return-object v0
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->onSyncWidgetPageItems(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V

    return-void
.end method

.method static synthetic access$7(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->onHolographicPageItemsLoaded(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V

    return-void
.end method

.method static synthetic access$8(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;ILjava/util/ArrayList;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->prepareLoadWidgetPreviewsTask(ILjava/util/ArrayList;II)V

    return-void
.end method

.method private addAppsWithoutInvalidate(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v4, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_NAME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v3, v1, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    :cond_2
    :goto_1
    if-gez v0, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    add-int/lit8 v4, v0, 0x1

    neg-int v4, v4

    invoke-virtual {v3, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v4, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v3, v1, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v4, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->KKDefault:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_KONKA_DEFAULT_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v3, v1, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    goto :goto_1
.end method

.method private addToHomePage(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "duplicate"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "android.intent.extra.shortcut.NAME"

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.shortcut.ICON"

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->iconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.shortcut.INTENT"

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private beginDraggingApplication(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->onDragStartedWithItem(Landroid/view/View;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/cyanogenmod/trebuchet/Workspace;->beginDragShared(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;)V

    return-void
.end method

.method private beginDraggingWidget(Landroid/view/View;)V
    .locals 18
    .param p1    # Landroid/view/View;

    const v1, 0x7f0d0022    # com.konka.avenger.R.id.widget_preview

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    instance-of v1, v11, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    if-eqz v1, :cond_1

    move-object v12, v11

    check-cast v12, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v2, 0x0

    invoke-virtual {v1, v12, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;[I)[I

    move-result-object v17

    const/4 v1, 0x0

    aget v1, v17, v1

    iput v1, v11, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->spanX:I

    const/4 v1, 0x1

    aget v1, v17, v1

    iput v1, v11, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->spanY:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v1

    const/4 v2, 0x0

    aget v2, v17, v2

    const/4 v4, 0x1

    aget v4, v17, v4

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v4, v12, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->estimateItemSize(IILcom/cyanogenmod/trebuchet/PendingAddItemInfo;Z)[I

    move-result-object v15

    iget-object v2, v12, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->componentName:Landroid/content/ComponentName;

    iget v3, v12, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->previewImage:I

    iget v4, v12, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->icon:I

    const/4 v1, 0x0

    aget v5, v17, v1

    const/4 v1, 0x1

    aget v6, v17, v1

    const/4 v1, 0x0

    aget v7, v15, v1

    const/4 v1, 0x1

    aget v8, v15, v1

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getWidgetPreview(Landroid/content/ComponentName;IIIIII)Landroid/graphics/Bitmap;

    move-result-object v3

    :goto_0
    instance-of v1, v11, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    if-eqz v1, :cond_0

    move-object v1, v11

    check-cast v1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget v1, v1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->previewImage:I

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    const/16 v2, 0xff

    invoke-static {v1, v2}, Landroid/graphics/TableMaskFilter;->CreateClipTable(II)Landroid/graphics/TableMaskFilter;

    move-result-object v10

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    :cond_0
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    const/4 v4, 0x0

    invoke-static {v3, v1, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDragViewMultiplyColor:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCanvas:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->lockScreenOrientationOnLargeUI()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v11, v0, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->onDragStartedWithItem(Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    sget v6, Lcom/cyanogenmod/trebuchet/DragController;->DRAG_ACTION_COPY:I

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v8

    move-object v2, v14

    move-object/from16 v4, p0

    move-object v5, v11

    invoke-virtual/range {v1 .. v8}, Lcom/cyanogenmod/trebuchet/DragController;->startDrag(Landroid/view/View;Landroid/graphics/Bitmap;Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Rect;Z)V

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetPreviewIconPaddedDimension:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetPreviewIconPaddedDimension:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v14}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v13, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCanvas:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x1

    iput v1, v11, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->spanY:I

    iput v1, v11, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->spanX:I

    goto/16 :goto_0
.end method

.method private cancelAllTasks()V
    .locals 3

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->cancel(Z)Z

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method private dumpAppWidgetProviderInfoList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v3, :cond_2

    move-object v1, v0

    check-cast v1, Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   label=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" previewImage="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " resizeMode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " configure="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " initialLayout="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minWidth="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minHeight="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    instance-of v3, v0, Landroid/content/pm/ResolveInfo;

    if-eqz v3, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/content/pm/ResolveInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   label=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" icon="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/content/pm/ResolveInfo;->icon:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private endDragging(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/cyanogenmod/trebuchet/Workspace;->onDragStopped(Z)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    if-eq p1, v0, :cond_1

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->exitSpringLoadedDragMode()V

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->unlockScreenOrientationOnLargeUI()V

    return-void
.end method

.method private findAppByComponent(Ljava/util/List;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)I
    .locals 5
    .param p2    # Lcom/cyanogenmod/trebuchet/ApplicationInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ")I"
        }
    .end annotation

    iget-object v4, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_1

    const/4 v0, -0x1

    :cond_0
    return v0

    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v4, v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getAppNameList(Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v6

    :goto_0
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getRightNameString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    const/4 v7, 0x0

    :goto_1
    if-nez v8, :cond_0

    :goto_2
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    return-object v0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_0
    :try_start_3
    invoke-interface {v0, v7, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getRightNameString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v8

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method private getMiddleComponentIndexOnCurrentPage()I
    .locals 9

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageCount()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getCurrentPage()I

    move-result v2

    iget-boolean v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    if-ge v2, v7, :cond_1

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    move-result-object v1

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    mul-int v6, v7, v8

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    mul-int v7, v2, v6

    div-int/lit8 v8, v0, 0x2

    add-int v3, v7, v8

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    mul-int v6, v7, v8

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    sub-int v7, v2, v7

    mul-int/2addr v7, v6

    add-int/2addr v7, v5

    div-int/lit8 v8, v0, 0x2

    add-int v3, v7, v8

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType()[I

    move-result-object v7

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    move-result-object v1

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    mul-int v6, v7, v8

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    mul-int v7, v2, v6

    div-int/lit8 v8, v0, 0x2

    add-int v3, v7, v8

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    mul-int v6, v7, v8

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    mul-int v7, v2, v6

    div-int/lit8 v8, v0, 0x2

    add-int v3, v7, v8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getRightNameString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    const/16 v2, 0x23

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, p1

    goto :goto_0
.end method

.method private getShortcutPreview(Landroid/content/pm/ResolveInfo;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1    # Landroid/content/pm/ResolveInfo;

    const/4 v3, 0x0

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v7, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/IconCache;->getFullResIcon(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    return-object v2
.end method

.method private getSleepForPage(I)I
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getWidgetPageLoadPriority(I)I

    move-result v0

    const/4 v1, 0x0

    mul-int/lit16 v2, v0, 0xc8

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method private getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const v1, 0x7f0d0038    # com.konka.avenger.R.id.apps_customize_pane

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    return-object v0
.end method

.method private getThreadPriorityForPage(I)I
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    sub-int v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v1, -0x2

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    const/4 v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getWidgetPageLoadPriority(I)I
    .locals 7
    .param p1    # I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNextPage:I

    const/4 v6, -0x1

    if-le v5, v6, :cond_0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNextPage:I

    :cond_0
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const v1, 0x7fffffff

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    sub-int v5, p1, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    sub-int v5, v2, v5

    return v5

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v5, v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int/2addr v5, v6

    sub-int/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    goto :goto_0
.end method

.method private getWidgetPreview(Landroid/content/ComponentName;IIIIII)Landroid/graphics/Bitmap;
    .locals 23
    .param p1    # Landroid/content/ComponentName;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    invoke-virtual/range {p1 .. p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v18

    if-gez p6, :cond_0

    const p6, 0x7fffffff

    :cond_0
    if-gez p7, :cond_1

    const p7, 0x7fffffff

    :cond_1
    const/4 v3, 0x0

    if-eqz p2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1, v5}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v2, "AppsCustomizePagedView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Can\'t load widget preview drawable 0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for provider: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v3, :cond_7

    const/16 v22, 0x1

    :goto_0
    if-eqz v22, :cond_8

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->estimateCellWidth(I)I

    move-result v2

    move/from16 v0, p6

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result p6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->estimateCellHeight(I)I

    move-result v2

    move/from16 v0, p7

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result p7

    :cond_3
    :goto_1
    const/high16 v20, 0x3f800000    # 1.0f

    move/from16 v0, p6

    if-le v7, v0, :cond_4

    move/from16 v0, p6

    int-to-float v2, v0

    int-to-float v5, v7

    div-float v20, v2, v5

    :cond_4
    int-to-float v2, v8

    mul-float v2, v2, v20

    move/from16 v0, p7

    int-to-float v5, v0

    cmpl-float v2, v2, v5

    if-lez v2, :cond_5

    move/from16 v0, p7

    int-to-float v2, v0

    int-to-float v5, v8

    div-float v20, v2, v5

    :cond_5
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v20, v2

    if-eqz v2, :cond_6

    int-to-float v2, v7

    mul-float v2, v2, v20

    float-to-int v7, v2

    int-to-float v2, v8

    mul-float v2, v2, v20

    float-to-int v8, v2

    :cond_6
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v22, :cond_a

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    :goto_2
    return-object v4

    :cond_7
    const/16 v22, 0x0

    goto :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->estimateCellWidth(I)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->estimateCellHeight(I)I

    move-result v8

    move/from16 v0, p4

    move/from16 v1, p5

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    int-to-float v2, v2

    const/high16 v5, 0x3e800000    # 0.25f

    mul-float/2addr v2, v5

    float-to-int v0, v2

    move/from16 v17, v0

    const/4 v2, 0x1

    move/from16 v0, p4

    if-gt v0, v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    mul-int/lit8 v5, v17, 0x2

    add-int v8, v2, v5

    move v7, v8

    goto :goto_1

    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    mul-int/lit8 v5, v17, 0x4

    add-int v8, v2, v5

    move v7, v8

    goto :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    int-to-float v2, v2

    const/high16 v5, 0x3e800000    # 0.25f

    mul-float/2addr v2, v5

    float-to-int v0, v2

    move/from16 v17, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v21

    move/from16 v0, v21

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    mul-int/lit8 v6, v17, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    div-float/2addr v2, v5

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v16

    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_b

    const/4 v2, 0x1

    move/from16 v0, p5

    if-eq v0, v2, :cond_c

    :cond_b
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDefaultWidgetBackground:Landroid/graphics/drawable/Drawable;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v9, p0

    move-object v11, v4

    move v14, v7

    move v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    :cond_c
    const/4 v10, 0x0

    :try_start_0
    div-int/lit8 v2, v7, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    int-to-float v5, v5

    mul-float v5, v5, v16

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v2, v5

    float-to-int v12, v2

    div-int/lit8 v2, v8, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    int-to-float v5, v5

    mul-float v5, v5, v16

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v2, v5

    float-to-int v13, v2

    if-lez p3, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v2, v0, v1}, Lcom/cyanogenmod/trebuchet/IconCache;->getFullResIcon(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    if-nez v10, :cond_e

    const v2, 0x7f02006f    # com.konka.avenger.R.drawable.ic_launcher_application

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    :cond_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    int-to-float v2, v2

    mul-float v2, v2, v16

    float-to-int v14, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAppIconSize:I

    int-to-float v2, v2

    mul-float v2, v2, v16

    float-to-int v15, v2

    move-object/from16 v9, p0

    move-object v11, v4

    invoke-direct/range {v9 .. v15}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v2

    goto/16 :goto_2
.end method

.method private initQuickActionMenu()V
    .locals 7

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Lnet/londatiga/android/ActionItem;

    const/4 v5, 0x0

    const v6, 0x7f0a009f    # com.konka.avenger.R.string.quick_action_run

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v0, Lnet/londatiga/android/ActionItem;

    const/4 v5, 0x1

    const v6, 0x7f0a00a0    # com.konka.avenger.R.string.quick_action_add_to_homepage

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v4, Lnet/londatiga/android/ActionItem;

    const/4 v5, 0x2

    const v6, 0x7f0a00a2    # com.konka.avenger.R.string.quick_action_uninstall

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, v3}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, v0}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, v4}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, p0}, Lnet/londatiga/android/QuickAction;->setOnActionItemClickListener(Lnet/londatiga/android/QuickAction$OnActionItemClickListener;)V

    :goto_0
    return-void

    :cond_0
    const-string v5, "AppsCustomizePagedView"

    const-string v6, "the quickAciton has been initialized!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private loadAppListFile()V
    .locals 8

    :try_start_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f060000    # com.konka.avenger.R.raw.defaultapplist

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v4

    new-array v0, v4, [B

    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v6, "defaultAppList"

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private loadWidgetPreviewsInBackground(Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 16
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;
    .param p2    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->syncThreadPriority()V

    :cond_0
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->items:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    return-void

    :cond_3
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    if-eqz p1, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->syncThreadPriority()V

    :cond_4
    instance-of v1, v13, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v1, :cond_5

    move-object v12, v13

    check-cast v12, Landroid/appwidget/AppWidgetProviderInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v2, 0x0

    invoke-virtual {v1, v12, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/appwidget/AppWidgetProviderInfo;[I)[I

    move-result-object v10

    iget-object v2, v12, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v3, v12, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    iget v4, v12, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    const/4 v1, 0x0

    aget v5, v10, v1

    const/4 v1, 0x1

    aget v6, v10, v1

    move-object/from16 v0, p2

    iget v7, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->maxImageWidth:I

    move-object/from16 v0, p2

    iget v8, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->maxImageHeight:I

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getWidgetPreview(Landroid/content/ComponentName;IIIIII)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    instance-of v1, v13, Landroid/content/pm/ResolveInfo;

    if-eqz v1, :cond_1

    move-object v12, v13

    check-cast v12, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getShortcutPreview(Landroid/content/pm/ResolveInfo;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private onHolographicPageItemsLoaded(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidate()V

    iget v5, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->page:I

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    instance-of v7, v4, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    if-eqz v7, :cond_2

    move-object v0, v4

    check-cast v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getPageChildCount()I

    move-result v1

    iget-object v7, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v1, v7, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildOnPageAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    iget-object v7, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v7}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setHolographicOutline(Landroid/graphics/Bitmap;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    iget-object v7, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v1, v7, :cond_0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_0

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    iget-object v7, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setHolographicOutline(Landroid/graphics/Bitmap;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private onSyncWidgetPageItems(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 12
    .param p1    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    iget v4, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->page:I

    iget v9, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int/2addr v9, v4

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->createHardwareLayer()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidate()V

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    return-void

    :cond_0
    invoke-virtual {v3, v1}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    if-eqz v8, :cond_1

    iget-object v9, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    new-instance v9, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    invoke-direct {v9, v6}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v8, v9}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->applyPreview(Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v10, v7, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    iget v11, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int v5, v10, v11

    invoke-direct {p0, v5}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getThreadPriorityForPage(I)I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    goto :goto_1
.end method

.method private popQuickActionMenu(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurSelectView:Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v0, p1}, Lnet/londatiga/android/QuickAction;->show(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private prepareGenerateHoloOutlinesTask(ILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 11
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    const/4 v10, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    new-instance v4, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$7;

    invoke-direct {v4, p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$7;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)V

    new-instance v5, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$8;

    invoke-direct {v5, p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$8;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)V

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;-><init>(ILjava/util/ArrayList;Ljava/util/ArrayList;Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;)V

    new-instance v7, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;->LoadHolographicIconsData:Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

    invoke-direct {v7, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;-><init>(ILcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;)V

    const/16 v1, 0xa

    invoke-virtual {v7, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    aput-object v0, v2, v10

    invoke-virtual {v7, v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v9, v8, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    if-ne v9, p1, :cond_0

    iget-object v1, v8, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->dataType:Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;->LoadHolographicIconsData:Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

    if-ne v1, v2, :cond_0

    invoke-virtual {v8, v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->cancel(Z)Z

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method private prepareLoadWidgetPreviewsTask(ILjava/util/ArrayList;II)V
    .locals 12
    .param p1    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;II)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int/2addr v1, p1

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getSleepForPage(I)I

    move-result v8

    new-instance v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    new-instance v5, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$5;

    invoke-direct {v5, p0, v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$5;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;I)V

    new-instance v6, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$6;

    invoke-direct {v6, p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$6;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)V

    move v1, p1

    move-object v2, p2

    move v3, p3

    move/from16 v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;-><init>(ILjava/util/ArrayList;IILcom/cyanogenmod/trebuchet/AsyncTaskCallback;Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;)V

    new-instance v9, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;->LoadWidgetPreviewData:Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

    invoke-direct {v9, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;-><init>(ILcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;)V

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int/2addr v1, p1

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getThreadPriorityForPage(I)I

    move-result v1

    invoke-virtual {v9, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v9, v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v1, v10, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int v11, v1, v2

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getAssociatedLowerPageBound(I)I

    move-result v1

    if-lt v11, v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getAssociatedUpperPageBound(I)I

    move-result v1

    if-le v11, v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->cancel(Z)Z

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v11}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getThreadPriorityForPage(I)I

    move-result v1

    invoke-virtual {v10, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    goto :goto_0
.end method

.method private removeAppsWithoutInvalidate(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->findAppByComponent(Ljava/util/List;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)I

    move-result v1

    const/4 v3, -0x1

    if-le v1, v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method private renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    .locals 9
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIFI)V

    return-void
.end method

.method private renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIFI)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # F
    .param p8    # I

    if-eqz p2, :cond_1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, p7, p7}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v1

    add-int v2, p3, p5

    add-int v3, p4, p6

    invoke-virtual {p1, p3, p4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    const/4 v2, -0x1

    if-eq p8, v2, :cond_0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDragViewMultiplyColor:I

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    return-void
.end method

.method private screenScrolledCube(IZ)V
    .locals 7
    .param p1    # I
    .param p2    # Z

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v5

    if-lt v1, v5, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, p1, v4, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v3

    if-eqz p2, :cond_3

    const/high16 v5, 0x42b40000    # 90.0f

    :goto_1
    mul-float v2, v5, v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v0, v5, v6

    if-eqz p2, :cond_1

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDensity:F

    const v6, 0x45cb2000    # 6500.0f

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/view/View;->setCameraDistance(F)V

    :cond_1
    const/4 v5, 0x0

    cmpg-float v5, v3, v5

    if-gez v5, :cond_4

    const/4 v5, 0x0

    :goto_2
    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setRotationY(F)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/high16 v5, -0x3d4c0000    # -90.0f

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    goto :goto_2
.end method

.method private screenScrolledRotate(IZ)V
    .locals 11
    .param p1    # I
    .param p2    # Z

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v7

    if-lt v1, v7, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {p0, p1, v6, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    if-eqz p2, :cond_2

    const/high16 v7, 0x41480000    # 12.5f

    :goto_1
    mul-float v3, v7, v4

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float v5, v7, v4

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v10

    const-wide/high16 v8, 0x4019000000000000L    # 6.25

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    double-to-float v8, v8

    div-float v2, v7, v8

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v10

    invoke-virtual {v6, v7}, Landroid/view/View;->setPivotX(F)V

    if-eqz p2, :cond_3

    neg-float v7, v2

    invoke-virtual {v6, v7}, Landroid/view/View;->setPivotY(F)V

    :goto_2
    invoke-virtual {v6, v3}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {v6, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-boolean v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeInAdjacentScreens:Z

    if-eqz v7, :cond_1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v8

    sub-float v0, v7, v8

    invoke-virtual {v6, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/high16 v7, -0x3eb80000    # -12.5f

    goto :goto_1

    :cond_3
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v7, v2

    invoke-virtual {v6, v7}, Landroid/view/View;->setPivotY(F)V

    goto :goto_2
.end method

.method private screenScrolledStack(I)V
    .locals 11
    .param p1    # I

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v8

    if-lt v1, v8, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {p0, p1, v6, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mZInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

    invoke-static {v4, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    invoke-virtual {v8, v9}, Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;->getInterpolation(F)F

    move-result v2

    sub-float v8, v7, v2

    const v9, 0x3f428f5c    # 0.76f

    mul-float/2addr v9, v2

    add-float v3, v8, v9

    invoke-static {v10, v4}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    int-to-float v9, v9

    mul-float v5, v8, v9

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v8

    if-eqz v8, :cond_1

    cmpg-float v8, v4, v10

    if-gez v8, :cond_4

    :cond_1
    cmpg-float v8, v4, v10

    if-gez v8, :cond_3

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float v9, v7, v9

    invoke-virtual {v8, v9}, Landroid/view/animation/AccelerateInterpolator;->getInterpolation(F)F

    move-result v0

    :goto_1
    invoke-virtual {v6, v5}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setAlpha(F)V

    const v8, 0x3caaaaab

    cmpg-float v8, v0, v8

    if-gtz v8, :cond_5

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    move v0, v7

    goto :goto_1

    :cond_4
    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLeftScreenAlphaInterpolator:Landroid/view/animation/DecelerateInterpolator;

    sub-float v9, v7, v4

    invoke-virtual {v8, v9}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_1

    :cond_5
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method private screenScrolledStandard(I)V
    .locals 6
    .param p1    # I

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v4

    if-lt v1, v4, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, p1, v3, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v2

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeInAdjacentScreens:Z

    if-eqz v4, :cond_1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    sub-float v0, v4, v5

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private screenScrolledTablet(I)V
    .locals 9
    .param p1    # I

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v6

    if-lt v1, v6, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p0, p1, v5, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v3

    const/high16 v6, 0x41480000    # 12.5f

    mul-float v2, v6, v3

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v6

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v8

    invoke-virtual {v6, v2, v7, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->getOffsetXForRotation(FII)F

    move-result v4

    invoke-virtual {v5, v4}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {v5, v2}, Landroid/view/View;->setRotationY(F)V

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeInAdjacentScreens:Z

    if-eqz v6, :cond_1

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v7

    sub-float v0, v6, v7

    invoke-virtual {v5, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private screenScrolledZoom(IZ)V
    .locals 10
    .param p1    # I
    .param p2    # Z

    const/high16 v9, 0x3f800000    # 1.0f

    const v7, 0x3dcccccd    # 0.1f

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v6

    if-lt v1, v6, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p0, p1, v5, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v3

    if-eqz p2, :cond_3

    const v6, -0x41b33333    # -0.2f

    :goto_1
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v8

    mul-float/2addr v6, v8

    add-float v2, v9, v6

    if-nez p2, :cond_1

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v7

    neg-float v8, v3

    mul-float v4, v6, v8

    invoke-virtual {v5, v4}, Landroid/view/View;->setTranslationX(F)V

    :cond_1
    invoke-virtual {v5, v2}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {v5, v2}, Landroid/view/View;->setScaleY(F)V

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeInAdjacentScreens:Z

    if-eqz v6, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v0, v9, v6

    invoke-virtual {v5, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    move v6, v7

    goto :goto_1
.end method

.method private setVisibilityOnChildren(Landroid/view/ViewGroup;I)V
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private setupPage(Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;)V
    .locals 7
    .param p1    # Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    const/high16 v6, -0x80000000

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    invoke-virtual {p1, v2, v3}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setCellCount(II)V

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutWidthGap:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutHeightGap:I

    invoke-virtual {p1, v2, v3}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setGap(II)V

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingLeft:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingTop:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingRight:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingBottom:I

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setPadding(IIII)V

    const/16 v2, 0x8

    invoke-direct {p0, p1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setVisibilityOnChildren(Landroid/view/ViewGroup;I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageContentWidth()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setMinimumWidth(I)V

    invoke-virtual {p1, v1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->measure(II)V

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setVisibilityOnChildren(Landroid/view/ViewGroup;I)V

    return-void
.end method

.method private setupPage(Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;)V
    .locals 7
    .param p1    # Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    const/high16 v6, -0x80000000

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingLeft:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingTop:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingRight:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingBottom:I

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->setPadding(IIII)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageContentWidth()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->setMinimumWidth(I)V

    invoke-virtual {p1, v1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->measure(II)V

    return-void
.end method

.method private sortByKonka(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "defaultAppList"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getAppNameList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    sget-object v6, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_KONKA_DEFAULT_COMPARATOR:Ljava/util/Comparator;

    invoke-static {p1, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v6, v2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_0

    iput v4, v2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    goto :goto_0
.end method

.method private startApplication(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    new-instance v1, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$10;

    invoke-direct {v1, p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$10;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)V

    invoke-virtual {p0, p1, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->animateClickFeedback(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method private testDataReady()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    sget-object v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-eq v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method private uninstallApp(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget v3, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->flags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0a00a7    # com.konka.avenger.R.string.app_can_not_uninstall

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->title:Ljava/lang/CharSequence;

    aput-object v5, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->startApplicationUninstallActivity(Lcom/cyanogenmod/trebuchet/ApplicationInfo;)V

    goto :goto_0
.end method

.method private updateCurrentTab(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    if-lt p1, v2, :cond_1

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getTabTagForContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setCurrentTabFromContent(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    if-ge p1, v2, :cond_0

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getTabTagForContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setCurrentTabFromContent(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    goto :goto_0
.end method

.method private updatePageCounts()V
    .locals 3

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumWidgetPages:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    :cond_0
    return-void
.end method


# virtual methods
.method public addApps(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->addAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->updatePageCounts()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData()V

    return-void
.end method

.method protected beginDragging(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissAllAppsCling(Landroid/view/View;)V

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->beginDragging(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->enterSpringLoadedDragMode()V

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->beginDraggingApplication(Landroid/view/View;)V

    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->beginDraggingWidget(Landroid/view/View;)V

    goto :goto_1
.end method

.method public clearAllWidgetPreviews()V
    .locals 5

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->cancelAllTasks()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    if-eqz v3, :cond_1

    check-cast v2, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->removeAllViewsOnPage()V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected determineDraggingStart(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public dumpState()V
    .locals 3

    const-string v0, "AppsCustomizePagedView"

    const-string v1, "mApps"

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->dumpApplicationInfoList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "AppsCustomizePagedView"

    const-string v1, "mWidgets"

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->dumpAppWidgetProviderInfoList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public flashIndicator(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->flashScrollingIndicator(Z)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected flashScrollingIndicator(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeScrollingIndicator:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->flashScrollingIndicator(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->showScrollingIndicator(Z)V

    goto :goto_0
.end method

.method protected getAssociatedLowerPageBound(I)I
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    const/4 v3, 0x5

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/lit8 v3, p1, -0x2

    sub-int v4, v0, v2

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method protected getAssociatedUpperPageBound(I)I
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    const/4 v3, 0x5

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/lit8 v3, p1, 0x2

    add-int/lit8 v4, v2, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v4, v0, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method public getContentType()Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    return-object v0
.end method

.method public getCurSelectView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurSelectView:Landroid/view/View;

    return-object v0
.end method

.method protected getCurrentPageDescription()Ljava/lang/String;
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNextPage:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNextPage:I

    :goto_0
    const v2, 0x7f0a0053    # com.konka.avenger.R.string.default_scroll_format

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    if-ge v1, v3, :cond_1

    const v2, 0x7f0a0055    # com.konka.avenger.R.string.apps_customize_apps_scroll_format

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    :goto_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    add-int/lit8 v5, v1, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    return-object v3

    :cond_0
    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    sub-int/2addr v1, v3

    const v2, 0x7f0a0056    # com.konka.avenger.R.string.apps_customize_widgets_scroll_format

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumWidgetPages:I

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType()[I

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_3
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    add-int/lit8 v5, v1, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :pswitch_0
    const v2, 0x7f0a0055    # com.konka.avenger.R.string.apps_customize_apps_scroll_format

    goto :goto_3

    :pswitch_1
    const v2, 0x7f0a0056    # com.konka.avenger.R.string.apps_customize_widgets_scroll_format

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getFocusOnFistItem()V
    .locals 6

    const/4 v5, 0x0

    const-string v2, "====>leedebug"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getFocusOnFistItem mCurrentPage="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    if-eqz v0, :cond_1

    const-string v2, "====>leedebug"

    const-string v3, "getFocusOnFistItem requestFocus"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "====>leedebug"

    const-string v3, "getFocusOnFistItem requestFocus"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isInTouchMode()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "====>leedebug"

    const-string v3, "getFocusOnFistItem setFocusableInTouchMode"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    :cond_0
    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void
.end method

.method getPageAt(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPageContentWidth()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentWidth:I

    return v0
.end method

.method getPageForComponent(I)I
    .locals 3
    .param p1    # I

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v1, :cond_2

    if-gez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    mul-int v0, v1, v2

    div-int v1, p1, v0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    mul-int v0, v1, v2

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v2, p1, v2

    div-int/2addr v2, v0

    add-int/2addr v1, v2

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType()[I

    move-result-object v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v1, -0x1

    goto :goto_0

    :pswitch_0
    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    mul-int v0, v1, v2

    div-int v1, p1, v0

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    mul-int v0, v1, v2

    div-int v1, p1, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getSaveInstanceStateIndex()I
    .locals 2

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSaveInstanceStateItemIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMiddleComponentIndexOnCurrentPage()I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSaveInstanceStateItemIndex:I

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSaveInstanceStateItemIndex:I

    return v0
.end method

.method public getSortMode()Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    return-object v0
.end method

.method public hideIndicator(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->hideScrollingIndicator(Z)V

    return-void
.end method

.method protected indexToPage(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method protected init()V
    .locals 4

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->init()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCenterPagesVertically:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0014    # com.konka.avenger.R.integer.config_appsCustomizeDragSlopeThreshold

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setDragSlopeThreshold(F)V

    return-void
.end method

.method public isContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadContent()V
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->loadAssociatedPages(I)V

    return-void
.end method

.method public loadContent(Z)V
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {p0, v0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->loadAssociatedPages(IZ)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Launcher;->isAllAppsCustomizeOpen()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurSelectView:Landroid/view/View;

    instance-of v6, p1, Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    new-instance v6, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$3;

    invoke-direct {v6, p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$3;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)V

    invoke-virtual {p0, p1, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->animateClickFeedback(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    instance-of v6, p1, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0a0010    # com.konka.avenger.R.string.long_press_widget_to_add

    invoke-static {v6, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0037    # com.konka.avenger.R.dimen.dragViewOffsetY

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v2, v6

    const v6, 0x7f0d0022    # com.konka.avenger.R.id.widget_preview

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    const-string v6, "translationY"

    new-array v7, v8, [F

    aput v2, v7, v9

    invoke-static {v3, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const-wide/16 v6, 0x7d

    invoke-virtual {v5, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    const-string v6, "translationY"

    new-array v7, v8, [F

    const/4 v8, 0x0

    aput v8, v7, v9

    invoke-static {v3, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    new-instance v6, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method protected onDataReady(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    const/4 v3, 0x1

    :goto_0
    const v4, 0x7fffffff

    const v5, 0x7fffffff

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountX()I

    move-result v4

    :goto_1
    if-eqz v3, :cond_6

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountY()I

    move-result v5

    :cond_0
    :goto_2
    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mMaxAppCellCountX:I

    const/4 v9, -0x1

    if-le v8, v9, :cond_1

    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mMaxAppCellCountX:I

    invoke-static {v4, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    :cond_1
    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mMaxAppCellCountY:I

    const/4 v9, -0x1

    if-le v8, v9, :cond_2

    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mMaxAppCellCountY:I

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    :cond_2
    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    iget v9, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutWidthGap:I

    iget v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutHeightGap:I

    invoke-virtual {v8, v9, v10}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setGap(II)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    iget v9, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingLeft:I

    iget v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingTop:I

    iget v11, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingRight:I

    iget v12, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingBottom:I

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setPadding(IIII)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v8, p1, p2, v4, v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->calculateCellCount(IIII)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getCellCountX()I

    move-result v8

    iput v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getCellCountY()I

    move-result v8

    iput v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->updatePageCounts()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v8

    const/high16 v9, -0x80000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v8

    const/high16 v9, -0x80000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v8, v7, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->measure(II)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getContentWidth()I

    move-result v8

    iput v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentWidth:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->isTransitioning()Z

    move-result v2

    iget v8, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSaveInstanceStateItemIndex:I

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageForComponent(I)I

    move-result v6

    const/4 v8, 0x0

    invoke-static {v8, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p0, v8, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData(IZ)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getFocusOnFistItem()V

    if-nez v2, :cond_3

    new-instance v8, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$1;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$1;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)V

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->post(Ljava/lang/Runnable;)Z

    :cond_3
    return-void

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_5
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountY()I

    move-result v4

    goto/16 :goto_1

    :cond_6
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountX()I

    move-result v5

    goto/16 :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->cancelAllTasks()V

    return-void
.end method

.method public onDropCompleted(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)V
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;
    .param p3    # Z

    invoke-direct {p0, p1, p3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->endDragging(Landroid/view/View;Z)V

    if-nez p3, :cond_1

    const/4 v3, 0x0

    instance-of v5, p1, Lcom/cyanogenmod/trebuchet/Workspace;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v0

    move-object v4, p1

    check-cast v4, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v4, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v1, p2, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    if-eqz v2, :cond_0

    invoke-virtual {v2, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->calculateSpans(Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    const/4 v5, 0x0

    iget v6, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    iget v7, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    invoke-virtual {v2, v5, v6, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v3, 0x0

    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onItemClick(Lnet/londatiga/android/QuickAction;II)V
    .locals 1
    .param p1    # Lnet/londatiga/android/QuickAction;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mQuickAction:Lnet/londatiga/android/QuickAction;

    if-ne p1, v0, :cond_0

    packed-switch p3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurSelectView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->startApplication(Landroid/view/View;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurSelectView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->addToHomePage(Landroid/view/View;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurSelectView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->uninstallApp(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/16 v0, 0x52

    if-ne p2, v0, :cond_0

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->popQuickActionMenu(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x42

    if-eq p2, v0, :cond_1

    const/16 v0, 0x17

    if-ne p2, v0, :cond_2

    :cond_1
    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurSelectView:Landroid/view/View;

    :cond_2
    invoke-static {p1, p2, p3}, Lcom/cyanogenmod/trebuchet/FocusHelper;->handleAppsCustomizeKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->isDataReady()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->testDataReady()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setDataIsReady()V

    invoke-virtual {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setMeasuredDimension(II)V

    invoke-virtual {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->onDataReady(II)V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->onMeasure(II)V

    return-void
.end method

.method public onPackagesUpdated()V
    .locals 3

    new-instance v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$2;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$2;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected onPageEndMoving()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mFadeScrollingIndicator:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->hideScrollingIndicator(Z)V

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSaveInstanceStateItemIndex:I

    return-void
.end method

.method public onTabChanged(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->isContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0013    # com.konka.avenger.R.integer.config_tabTransitionDuration

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    new-instance v2, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;

    invoke-direct {v2, p0, p1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;I)V

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method protected onUnhandledTap(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    return-void
.end method

.method protected overScroll(F)V
    .locals 0
    .param p1    # F

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->acceleratedOverScroll(F)V

    return-void
.end method

.method public reloadCurrentPage()V
    .locals 1

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->flashScrollingIndicator(Z)V

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->loadAssociatedPages(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->requestFocus()Z

    return-void
.end method

.method public removeApps(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->removeAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->updatePageCounts()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData()V

    return-void
.end method

.method public reset()V
    .locals 5

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getTabTagForContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setCurrentTabFromContent(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    :cond_0
    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    sget-object v4, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const v4, 0x7f0d0038    # com.konka.avenger.R.id.apps_customize_pane

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->selectAppsTab()V

    goto :goto_0
.end method

.method public restore(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->loadAssociatedPages(I)V

    if-gez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSaveInstanceStateItemIndex:I

    goto :goto_0
.end method

.method protected screenScrolled(I)V
    .locals 11
    .param p1    # I

    const/high16 v10, 0x44a00000    # 1280.0f

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->screenScrolled(I)V

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mOverScrollX:I

    if-ltz v6, :cond_0

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mOverScrollX:I

    iget v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mMaxScrollX:I

    if-le v6, v7, :cond_4

    :cond_0
    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mOverScrollX:I

    if-gez v6, :cond_2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, p1, v3, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v2

    const/high16 v6, -0x3e500000    # -22.0f

    mul-float v1, v6, v2

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDensity:F

    const v7, 0x45cb2000    # 6500.0f

    mul-float/2addr v6, v7

    invoke-virtual {v3, v6}, Landroid/view/View;->setCameraDistance(F)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    int-to-float v7, v6

    if-nez v0, :cond_3

    const v6, 0x3f266666    # 0.65f

    :goto_1
    mul-float/2addr v6, v7

    invoke-virtual {v3, v6}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    invoke-virtual {v3, v6}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {v3, v1}, Landroid/view/View;->setRotationY(F)V

    invoke-virtual {v3, v8}, Landroid/view/View;->setTranslationX(F)V

    iput-boolean v9, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mOverscrollTransformsDirty:Z

    :cond_1
    :goto_2
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v6

    add-int/lit8 v0, v6, -0x1

    goto :goto_0

    :cond_3
    const v6, 0x3eb33334    # 0.35000002f

    goto :goto_1

    :cond_4
    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mOverscrollTransformsDirty:Z

    if-eqz v6, :cond_5

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mOverscrollTransformsDirty:Z

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v8}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setRotationY(F)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setRotationY(F)V

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDensity:F

    mul-float/2addr v6, v10

    invoke-virtual {v4, v6}, Landroid/view/View;->setCameraDistance(F)V

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDensity:F

    mul-float/2addr v6, v10

    invoke-virtual {v5, v6}, Landroid/view/View;->setCameraDistance(F)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setPivotY(F)V

    :cond_5
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizePagedView$TransitionEffect()[I

    move-result-object v6

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$TransitionEffect;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledStandard(I)V

    goto :goto_2

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledTablet(I)V

    goto :goto_2

    :pswitch_2
    invoke-direct {p0, p1, v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledZoom(IZ)V

    goto :goto_2

    :pswitch_3
    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledZoom(IZ)V

    goto :goto_2

    :pswitch_4
    invoke-direct {p0, p1, v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledRotate(IZ)V

    goto/16 :goto_2

    :pswitch_5
    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledRotate(IZ)V

    goto/16 :goto_2

    :pswitch_6
    invoke-direct {p0, p1, v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledCube(IZ)V

    goto/16 :goto_2

    :pswitch_7
    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledCube(IZ)V

    goto/16 :goto_2

    :pswitch_8
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->screenScrolledStack(I)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public setApps(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_NAME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->updatePageCounts()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->testDataReady()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->KKDefault:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->sortByKonka(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-ne p1, v2, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    invoke-virtual {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-ne p1, v2, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData(IZ)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-eq p1, v2, :cond_3

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData(IZ)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public setCurrentToApps()V
    .locals 0

    return-void
.end method

.method public setCurrentToWidgets()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData(I)V

    return-void
.end method

.method public setSortMode(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v1, p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_NAME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v1, :cond_5

    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncAppsPageItems(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->KKDefault:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->sortByKonka(Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncAppsPageItems(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public setup(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/DragController;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Lcom/cyanogenmod/trebuchet/DragController;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    return-void
.end method

.method public showAllAppsCling()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v4, 0x7f0d001c    # com.konka.avenger.R.id.all_apps_cling

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Cling;

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHasShownAllAppsCling:Z

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->isDataReady()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->testDataReady()Z

    move-result v4

    if-eqz v4, :cond_2

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHasShownAllAppsCling:Z

    const/4 v4, 0x2

    new-array v1, v4, [I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mClingFocusedX:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mClingFocusedY:I

    invoke-virtual {v4, v5, v6}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->estimateCellPosition(II)[I

    move-result-object v2

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v4

    invoke-virtual {v4, p0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocationInDragLayer(Landroid/view/View;[I)V

    aget v4, v2, v8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    aget v6, v1, v8

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    aput v4, v2, v8

    aget v4, v2, v7

    aget v5, v1, v7

    add-int/2addr v4, v5

    aput v4, v2, v7

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showFirstRunAllAppsCling([I)V

    goto :goto_0

    :cond_2
    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHasShownAllAppsSortCling:Z

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->isDataReady()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->testDataReady()Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Cling;->isDismissed()Z

    move-result v4

    if-eqz v4, :cond_0

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHasShownAllAppsSortCling:Z

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->showFirstRunAllAppsSortCling()V

    goto :goto_0
.end method

.method public showIndicator(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->showScrollingIndicator(Z)V

    return-void
.end method

.method protected snapToPage(III)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->snapToPage(III)V

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v2, :cond_0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->updateCurrentTab(I)V

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v3, v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int v0, v3, v4

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNextPage:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    if-le v3, v4, :cond_2

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    if-ge v0, v3, :cond_3

    :cond_2
    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNextPage:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    if-ge v3, v4, :cond_4

    iget v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCurrentPage:I

    if-gt v0, v3, :cond_4

    :cond_3
    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getThreadPriorityForPage(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    goto :goto_0

    :cond_4
    const/16 v3, 0x13

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    goto :goto_0
.end method

.method public surrender()V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->cancelAllTasks()V

    return-void
.end method

.method public syncAppsPageItems(IZ)V
    .locals 14
    .param p1    # I
    .param p2    # Z

    iget v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v11, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    mul-int v6, v10, v11

    mul-int v7, p1, v6

    add-int v10, v7, v6

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->removeAllViewsOnPage()V

    move v1, v7

    :goto_0
    if-lt v1, v0, :cond_0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->createHardwareLayers()V

    return-void

    :cond_0
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f030005    # com.konka.avenger.R.layout.apps_customize_application

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v5, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {v2, v4, v10}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->applyFromApplicationInfo(Lcom/cyanogenmod/trebuchet/ApplicationInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V

    invoke-virtual {v2, p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v2, p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v2, p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    sub-int v3, v1, v7

    iget v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    rem-int v8, v3, v10

    iget v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    div-int v9, v3, v10

    const/4 v10, -0x1

    new-instance v11, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-direct {v11, v8, v9, v12, v13}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;-><init>(IIII)V

    invoke-virtual {v5, v2, v10, v1, v11}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->addViewToCellLayout(Landroid/view/View;IILcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public syncAppsPages()V
    .locals 7

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountX:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mCellCountY:I

    mul-int/2addr v5, v6

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-direct {v2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setupPage(Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;)V

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public syncPageItems(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncAppsPageItems(IZ)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    sub-int v0, p1, v0

    invoke-virtual {p0, v0, p2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncWidgetPageItems(IZ)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType()[I

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncAppsPageItems(IZ)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncWidgetPageItems(IZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public syncPages()V
    .locals 7

    const/4 v6, -0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->removeAllViews()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->cancelAllTasks()V

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mJoinWidgetsApps:Z

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumWidgetPages:I

    if-lt v2, v4, :cond_0

    const/4 v1, 0x0

    :goto_1
    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    if-lt v1, v4, :cond_1

    :goto_2
    return-void

    :cond_0
    new-instance v3, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    invoke-direct {v3, v0, v4, v5}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;-><init>(Landroid/content/Context;II)V

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setupPage(Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;)V

    new-instance v4, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3, v4}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-direct {v3, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setupPage(Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;)V

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$ContentType()[I

    move-result-object v4

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncAppsPages()V

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->syncWidgetPages()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public syncWidgetPageItems(IZ)V
    .locals 23
    .param p1    # I
    .param p2    # Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    mul-int v19, v2, v3

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getContentWidth()I

    move-result v12

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingLeft:I

    sub-int v2, v12, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingRight:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetWidthGap:I

    mul-int/2addr v3, v7

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    div-int v4, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetSpacingLayout:Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getContentHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingTop:I

    sub-int v2, v11, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPageLayoutPaddingBottom:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetHeightGap:I

    mul-int/2addr v3, v7

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    div-int v5, v2, v3

    mul-int v20, p1, v19

    move/from16 v14, v20

    :goto_0
    add-int v2, v20, v19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-lt v14, v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mNumAppsPages:I

    add-int v2, v2, p1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getCellCountX()I

    move-result v2

    invoke-virtual {v6, v2}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->setColumnCount(I)V

    const/4 v14, 0x0

    :goto_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v14, v2, :cond_1

    new-instance v2, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$9;

    move-object/from16 v3, p0

    move/from16 v7, p2

    move/from16 v8, p1

    invoke-direct/range {v2 .. v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$9;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;IILcom/cyanogenmod/trebuchet/PagedViewGridLayout;ZILjava/util/ArrayList;)V

    invoke-virtual {v6, v2}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->setOnLayoutListener(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030008    # com.konka.avenger.R.layout.apps_customize_widget

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v22

    check-cast v22, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    move-object/from16 v0, v21

    instance-of v2, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v2, :cond_5

    move-object/from16 v15, v21

    check-cast v15, Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v13, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v13, v15, v2, v3}, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;-><init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v3, 0x0

    invoke-virtual {v2, v15, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/appwidget/AppWidgetProviderInfo;[I)[I

    move-result-object v10

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    move-object/from16 v0, v22

    invoke-virtual {v0, v15, v2, v10, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->applyFromAppWidgetProviderInfo(Landroid/appwidget/AppWidgetProviderInfo;I[ILcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setTag(Ljava/lang/Object;)V

    :cond_2
    :goto_2
    invoke-virtual/range {v22 .. v23}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {v22 .. v23}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual/range {v22 .. v23}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual/range {v22 .. v23}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    rem-int v16, v14, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    div-int v17, v14, v2

    new-instance v18, Landroid/widget/GridLayout$LayoutParams;

    sget-object v2, Landroid/widget/GridLayout;->LEFT:Landroid/widget/GridLayout$Alignment;

    move/from16 v0, v17

    invoke-static {v0, v2}, Landroid/widget/GridLayout;->spec(ILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v2

    sget-object v3, Landroid/widget/GridLayout;->TOP:Landroid/widget/GridLayout$Alignment;

    move/from16 v0, v16

    invoke-static {v0, v3}, Landroid/widget/GridLayout;->spec(ILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-direct {v0, v2, v3}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    move-object/from16 v0, v18

    iput v4, v0, Landroid/widget/GridLayout$LayoutParams;->width:I

    move-object/from16 v0, v18

    iput v5, v0, Landroid/widget/GridLayout$LayoutParams;->height:I

    const/16 v2, 0x33

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/widget/GridLayout$LayoutParams;->setGravity(I)V

    if-lez v16, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetWidthGap:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/widget/GridLayout$LayoutParams;->leftMargin:I

    :cond_3
    if-lez v17, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetHeightGap:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    :cond_4
    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, v21

    instance-of v2, v0, Landroid/content/pm/ResolveInfo;

    if-eqz v2, :cond_2

    move-object/from16 v15, v21

    check-cast v15, Landroid/content/pm/ResolveInfo;

    new-instance v13, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    invoke-direct {v13}, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;-><init>()V

    const/4 v2, 0x1

    iput v2, v13, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->itemType:I

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v15, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v7, v15, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v13, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->componentName:Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v15, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->applyFromResolveInfo(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_2
.end method

.method public syncWidgetPages()V
    .locals 8

    const/4 v7, -0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    mul-int/2addr v5, v6

    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    return-void

    :cond_0
    new-instance v2, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    iget v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountX:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgetCountY:I

    invoke-direct {v2, v0, v4, v5}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;-><init>(Landroid/content/Context;II)V

    invoke-direct {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setupPage(Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;)V

    new-instance v4, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v4}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public updateApps(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->removeAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->addAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->updatePageCounts()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData()V

    return-void
.end method

.method public updatePackages()V
    .locals 9

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v5}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v4

    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    new-instance v6, Lcom/cyanogenmod/trebuchet/LauncherModel$WidgetAndShortcutNameComparator;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v6, v7}, Lcom/cyanogenmod/trebuchet/LauncherModel$WidgetAndShortcutNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v5, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->updatePageCounts()V

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->testDataReady()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->requestLayout()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/appwidget/AppWidgetProviderInfo;

    iget v6, v3, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    if-lez v6, :cond_2

    iget v6, v3, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v6, "AppsCustomizePagedView"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Widget "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " has invalid dimensions ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->cancelAllTasks()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->invalidatePageData()V

    goto :goto_1
.end method
