.class public Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;
.super Landroid/view/ViewGroup;
.source "PagedViewCellLayout.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/Page;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "PagedViewCellLayout"


# instance fields
.field private mCellCountX:I

.field private mCellCountY:I

.field private mCellHeight:I

.field private mCellWidth:I

.field protected mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

.field private mHeightGap:I

.field private mMaxGap:I

.field private mOriginalCellHeight:I

.field private mOriginalCellWidth:I

.field private mOriginalHeightGap:I

.field private mOriginalWidthGap:I

.field private mWidthGap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setAlwaysDrawnWithCacheEnabled(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0023    # com.konka.avenger.R.dimen.apps_customize_cell_width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalCellWidth:I

    const v1, 0x7f0c0024    # com.konka.avenger.R.dimen.apps_customize_cell_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalCellHeight:I

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountX()I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountY()I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalHeightGap:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalWidthGap:I

    const v1, 0x7f0c0025    # com.konka.avenger.R.dimen.apps_customize_max_gap

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mMaxGap:I

    new-instance v1, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-direct {v1, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    invoke-virtual {v1, v2, v3}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setCellDimensions(II)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    invoke-virtual {v1, v2, v3}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setGap(II)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public addViewToCellLayout(Landroid/view/View;IILcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    move-object v0, p4

    iget v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellX:I

    if-ltz v1, :cond_2

    iget v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_2

    iget v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellY:I

    if-ltz v1, :cond_2

    iget v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellY:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_2

    iget v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellHSpan:I

    if-gez v1, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    iput v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellHSpan:I

    :cond_0
    iget v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellVSpan:I

    if-gez v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    iput v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->cellVSpan:I

    :cond_1
    invoke-virtual {p1, p3}, Landroid/view/View;->setId(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v1, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public calculateCellCount(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->estimateCellHSpan(I)I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    invoke-virtual {p0, p2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->estimateCellVSpan(I)I

    move-result v0

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->requestLayout()V

    return-void
.end method

.method public cancelLongPress()V
    .locals 3

    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    return v0
.end method

.method createHardwareLayers()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->createHardwareLayer()V

    return-void
.end method

.method destroyHardwareLayers()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->destroyHardwareLayer()V

    return-void
.end method

.method public enableCenteredContent(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->enableCenteredContent(Z)V

    return-void
.end method

.method public estimateCellHSpan(I)I
    .locals 6
    .param p1    # I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingLeft:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingRight:I

    add-int/2addr v2, v3

    sub-int v0, p1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    add-int/2addr v4, v5

    div-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method public estimateCellHeight(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public estimateCellPosition(II)[I
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingLeft:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    mul-int/2addr v3, p1

    add-int/2addr v2, v3

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    mul-int/2addr v3, p1

    add-int/2addr v2, v3

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingTop:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    mul-int/2addr v3, p2

    add-int/2addr v2, v3

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    mul-int/2addr v3, p2

    add-int/2addr v2, v3

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    aput v2, v0, v1

    return-object v0
.end method

.method public estimateCellVSpan(I)I
    .locals 6
    .param p1    # I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingTop:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingBottom:I

    add-int/2addr v2, v3

    sub-int v0, p1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    add-int/2addr v4, v5

    div-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method public estimateCellWidth(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getCellCountForDimensions(II)[I
    .locals 5
    .param p1    # I
    .param p2    # I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int v3, p1, v0

    div-int v1, v3, v0

    add-int v3, p2, v0

    div-int v2, v3, v0

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v1, v3, v4

    const/4 v4, 0x1

    aput v2, v3, v4

    return-object v3
.end method

.method public getCellCountX()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    return v0
.end method

.method public getCellCountY()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    return v0
.end method

.method public getCellHeight()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    return v0
.end method

.method public getCellWidth()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    return v0
.end method

.method public getChildOnPageAt(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildrenLayout()Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    return-object v0
.end method

.method getContentHeight()I
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method getContentWidth()I
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getWidthBeforeFirstLayout()I

    move-result v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingLeft:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingRight:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getPageChildCount()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v0

    return v0
.end method

.method getWidthBeforeFirstLayout()I
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public indexOfChildOnPage(Landroid/view/View;)I
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingLeft:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingTop:I

    sub-int v5, p4, p2

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingRight:I

    sub-int/2addr v5, v6

    sub-int v6, p5, p3

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingBottom:I

    sub-int/2addr v6, v7

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 22
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v17

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v18

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v8

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    if-eqz v17, :cond_0

    if-nez v8, :cond_1

    :cond_0
    new-instance v19, Ljava/lang/RuntimeException;

    const-string v20, "CellLayout cannot have UNSPECIFIED dimensions"

    invoke-direct/range {v19 .. v20}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v19

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    move/from16 v19, v0

    add-int/lit8 v14, v19, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    move/from16 v19, v0

    add-int/lit8 v13, v19, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalWidthGap:I

    move/from16 v19, v0

    if-ltz v19, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalHeightGap:I

    move/from16 v19, v0

    if-gez v19, :cond_6

    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingLeft:I

    move/from16 v19, v0

    sub-int v19, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingRight:I

    move/from16 v20, v0

    sub-int v7, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingTop:I

    move/from16 v19, v0

    sub-int v19, v9, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingBottom:I

    move/from16 v20, v0

    sub-int v16, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalCellWidth:I

    move/from16 v20, v0

    mul-int v19, v19, v20

    sub-int v6, v7, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalCellHeight:I

    move/from16 v20, v0

    mul-int v19, v19, v20

    sub-int v15, v16, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mMaxGap:I

    move/from16 v20, v0

    if-lez v14, :cond_4

    div-int v19, v6, v14

    :goto_0
    move/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mMaxGap:I

    move/from16 v20, v0

    if-lez v13, :cond_5

    div-int v19, v15, v13

    :goto_1
    move/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setGap(II)V

    :goto_2
    move/from16 v12, v18

    move v11, v9

    const/high16 v19, -0x80000000

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingLeft:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingRight:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellWidth:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v12, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingTop:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingBottom:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v11, v19, v20

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v11}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setMeasuredDimension(II)V

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildCount()I

    move-result v5

    const/4 v10, 0x0

    :goto_3
    if-lt v10, v5, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v11}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->setMeasuredDimension(II)V

    return-void

    :cond_4
    const/16 v19, 0x0

    goto/16 :goto_0

    :cond_5
    const/16 v19, 0x0

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalWidthGap:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalHeightGap:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingLeft:I

    move/from16 v19, v0

    sub-int v19, v12, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingRight:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingTop:I

    move/from16 v19, v0

    sub-int v19, v11, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mPaddingBottom:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getPageChildCount()I

    move-result v2

    if-lez v2, :cond_1

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getChildOnPageAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getPageChildCount()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getCellCountX()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v3, v5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getCellCountY()I

    move-result v5

    if-ge v3, v5, :cond_0

    iget v5, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellHeight:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    :cond_0
    if-nez v4, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    int-to-float v6, v0

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_2

    const/4 v4, 0x0

    :cond_1
    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public removeAllViewsOnPage()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->removeAllViews()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->destroyHardwareLayers()V

    return-void
.end method

.method public removeViewOnPageAt(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->removeViewAt(I)V

    return-void
.end method

.method public resetChildrenOnKeyListeners()V
    .locals 4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v2, v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setAlpha(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setAlpha(F)V

    return-void
.end method

.method public setCellCount(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountX:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mCellCountY:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->requestLayout()V

    return-void
.end method

.method protected setChildrenDrawingCacheEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method public setGap(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mWidthGap:I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalWidthGap:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mHeightGap:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mOriginalHeightGap:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    invoke-virtual {v0, p1, p2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setGap(II)V

    return-void
.end method
