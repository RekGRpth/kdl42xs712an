.class Lcom/cyanogenmod/trebuchet/FolderIcon$1;
.super Ljava/lang/Object;
.source "FolderIcon.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/FolderIcon;->fromXml(ILcom/cyanogenmod/trebuchet/Launcher;Landroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/FolderInfo;Lcom/cyanogenmod/trebuchet/IconCache;)Lcom/cyanogenmod/trebuchet/FolderIcon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    move-object v0, p1

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    # getter for: Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->access$2(Lcom/cyanogenmod/trebuchet/FolderIcon;)Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setSelected(Z)V

    return-void
.end method
