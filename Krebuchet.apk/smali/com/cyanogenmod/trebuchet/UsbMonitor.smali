.class public Lcom/cyanogenmod/trebuchet/UsbMonitor;
.super Landroid/os/storage/StorageEventListener;
.source "UsbMonitor.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/ConnectivityService$IConnectivityMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field public static final KEY_USB_DEVICE_COUNT:Ljava/lang/String; = "UsbMonitor.usbCount"

.field public static final KEY_USB_IS_CONNECTED:Ljava/lang/String; = "UsbMonitor.isUsbConnected"

.field private static final TAG:Ljava/lang/String; = "wzd_usb"


# instance fields
.field private mConnected:Z

.field private mCurrentConnectivity:Landroid/os/Bundle;

.field private mListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

.field private mStorageManagerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/storage/StorageManager;",
            ">;"
        }
    .end annotation
.end field

.field private mUsbNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    invoke-direct {p0}, Landroid/os/storage/StorageEventListener;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    const-string v1, "storage"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mStorageManagerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->isUsbMassStorageConnected()Z

    move-result v1

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mConnected:Z

    const-string v1, "wzd_usb"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "initial usb "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mConnected:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getCurrentConnectivity()Landroid/os/Bundle;
    .locals 3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "UsbMonitor.isUsbConnected"

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mConnected:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "UsbMonitor.usbCount"

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    return-object v0
.end method

.method private getPartitionName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-lez v0, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private queryUsbMassStorage()V
    .locals 8

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mStorageManagerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/storage/StorageManager;

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v3}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    if-eqz v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v6, v5

    if-ge v0, v6, :cond_0

    aget-object v6, v5, v0

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "mounted"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string v6, "wzd_usb"

    const-string v7, "query USB:"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mConnected:Z

    invoke-direct {p0, v2}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->getPartitionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->updateUsbMounted(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private updateUsbMounted(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "wzd_usb"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addUSB("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->updateUsbState()V

    goto :goto_0
.end method

.method private updateUsbState()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mConnected:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->getCurrentConnectivity()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;->onUpdateUsbConnectivity(Landroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateUsbUnmounted(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "wzd_usb"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeUSB("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mUsbNameMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->updateUsbState()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onStorageStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v1, "wzd_usb"

    const-string v2, "Media {%s} state changed from {%s} -> {%s}"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->getPartitionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "wzd_usb"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "partitionName="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "mounted"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->updateUsbMounted(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "removed"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "bad_removal"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->updateUsbUnmounted(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUsbMassStorageConnectionChanged(Z)V
    .locals 6
    .param p1    # Z

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_1

    const-string v1, "removed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "checking"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mConnected:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->queryUsbMassStorage()V

    const-string v1, "wzd_usb"

    const-string v2, "connection changed to %s (media state %s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->getCurrentConnectivity()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;->onUpdateUsbConnectivity(Landroid/os/Bundle;)V

    :cond_2
    return-void
.end method

.method public startMonitor()V
    .locals 2

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mConnected:Z

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->onUsbMassStorageConnectionChanged(Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mStorageManagerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/os/storage/StorageManager;->registerListener(Landroid/os/storage/StorageEventListener;)V

    :cond_0
    return-void
.end method

.method public stopMonitor()V
    .locals 4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/UsbMonitor;->mStorageManagerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1, p0}, Landroid/os/storage/StorageManager;->unregisterListener(Landroid/os/storage/StorageEventListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "wzd_usb"

    const-string v3, "Unable to stop UsbMonitor"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
