.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;
.super Lgreendroid/widget/PagedAdapter;
.source "SearchDropTargetBar.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;
    }
.end annotation


# instance fields
.field private mAdapters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mOnKeyListener:Landroid/view/View$OnKeyListener;

.field private mSelectedApps:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Landroid/content/Context;)V
    .locals 2
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-direct {p0}, Lgreendroid/widget/PagedAdapter;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mAdapters:Ljava/util/ArrayList;

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$1;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    new-instance v0, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    return-object v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    const/16 v0, 0x15

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x15

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x15

    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedApps()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v11, 0x0

    const/4 v10, -0x1

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03001c    # com.konka.avenger.R.layout.paged_dialog_apps

    invoke-virtual {v8, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;

    invoke-direct {v0, p0, v11}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mAdapters:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v3, p2

    check-cast v3, Landroid/widget/GridView;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v6}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 v8, 0x7

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setNumColumns(I)V

    const/4 v8, 0x2

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setChoiceMode(I)V

    invoke-virtual {v3, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v3, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :goto_0
    const/16 v5, 0x15

    mul-int/lit8 v7, p1, 0x15

    const/16 v8, 0x15

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sub-int/2addr v9, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v0, v7, v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->setRange(II)V

    move-object v8, p2

    check-cast v8, Landroid/widget/GridView;

    invoke-virtual {v8}, Landroid/widget/GridView;->clearChoices()V

    move v4, v7

    :goto_1
    add-int v8, v7, v2

    if-lt v4, v8, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->notifyDataSetChanged()V

    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    return-object p2

    :cond_0
    move-object v8, p2

    check-cast v8, Landroid/widget/GridView;

    invoke-virtual {v8}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;

    goto :goto_0

    :cond_1
    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;

    invoke-virtual {v8, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move-object v8, p2

    check-cast v8, Landroid/widget/GridView;

    sub-int v9, v4, v7

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/widget/GridView;->setItemChecked(IZ)V

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->notifyDataSetChanged()V

    return-void
.end method
