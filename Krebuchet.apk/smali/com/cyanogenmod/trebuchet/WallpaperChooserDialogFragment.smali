.class public Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;
.super Landroid/app/DialogFragment;
.source "WallpaperChooserDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$ImageAdapter;,
        Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;,
        Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/DialogFragment;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static final EMBEDDED_KEY:Ljava/lang/String; = "com.cyanogenmod.trebuchet.WallpaperChooserDialogFragment.EMBEDDED_KEY"

.field private static final TAG:Ljava/lang/String; = "Launcher.WallpaperChooserDialogFragment"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mEmbedded:Z

.field private mImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

.field private mThumbs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWallpaperDrawable:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v1, 0x18

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mThumbs:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mImages:Ljava/util/ArrayList;

    new-instance v0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mWallpaperDrawable:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mThumbs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mImages:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mWallpaperDrawable:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;

    return-object v0
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    return-void
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->selectWallpaper(I)V

    return-void
.end method

.method static addWallpapers(Landroid/content/res/Resources;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 8
    .param p0    # Landroid/content/res/Resources;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    return-void

    :cond_0
    aget-object v0, v1, v4

    const-string v6, "drawable"

    invoke-virtual {p0, v0, v6, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_small"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "drawable"

    invoke-virtual {p0, v6, v7, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method static findWallpapers(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const v2, 0x7f070005    # com.konka.avenger.R.array.wallpapers

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2, p1, p2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->addWallpapers(Landroid/content/res/Resources;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    const v2, 0x7f070004    # com.konka.avenger.R.array.extra_wallpapers

    invoke-static {v1, v0, v2, p1, p2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->addWallpapers(Landroid/content/res/Resources;Ljava/lang/String;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static newInstance()Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;
    .locals 2

    new-instance v0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->setCancelable(Z)V

    return-object v0
.end method

.method private selectWallpaper(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mImages:Ljava/util/ArrayList;

    invoke-static {v1, p1, v2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->setWallpaper(Landroid/content/Context;ILjava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method static setPreviewView(Landroid/widget/ImageView;ILjava/util/ArrayList;)V
    .locals 5
    .param p0    # Landroid/widget/ImageView;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v2, "Launcher.WallpaperChooserDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error decoding thumbnail resId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for wallpaper #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static setWallpaper(Landroid/content/Context;ILjava/util/ArrayList;)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    :try_start_0
    const-string v2, "wallpaper"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/WallpaperManager;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/WallpaperManager;->setResource(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v2, "Launcher.WallpaperChooserDialogFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to set wallpaper: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "com.cyanogenmod.trebuchet.WallpaperChooserDialogFragment.EMBEDDED_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.cyanogenmod.trebuchet.WallpaperChooserDialogFragment.EMBEDDED_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mEmbedded:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->isInLayout()Z

    move-result v0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mEmbedded:Z

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mThumbs:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mImages:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->findWallpapers(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mThumbs:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mImages:Ljava/util/ArrayList;

    invoke-static {v3, v4, v5}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->findWallpapers(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mEmbedded:Z

    if-eqz v3, :cond_0

    const v3, 0x7f03002d    # com.konka.avenger.R.layout.wallpaper_chooser

    invoke-virtual {p1, v3, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mWallpaperDrawable:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v3, 0x7f0d0066    # com.konka.avenger.R.id.gallery

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Gallery;

    invoke-virtual {v0, v6}, Landroid/widget/Gallery;->setCallbackDuringFling(Z)V

    invoke-virtual {v0, p0}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v3, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$ImageAdapter;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$ImageAdapter;-><init>(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;Landroid/app/Activity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v3, 0x7f0d0067    # com.konka.avenger.R.id.set

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$1;-><init>(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;Landroid/widget/Gallery;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    :cond_0
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-direct {p0, p3}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->selectWallpaper(I)V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->cancel()V

    :cond_0
    new-instance v0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;-><init>(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mLoader:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "com.cyanogenmod.trebuchet.WallpaperChooserDialogFragment.EMBEDDED_KEY"

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mEmbedded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
