.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;
.super Landroid/database/DataSetObserver;
.source "SearchDropTargetBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getAppsDialog()Lgreendroid/widget/PagedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

.field private final synthetic val$adapter:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;->val$adapter:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method private updateShowProgressBar()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$12(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lgreendroid/widget/PagedDialog;

    move-result-object v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;->val$adapter:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lgreendroid/widget/PagedDialog;->setShowProgress(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;->updateShowProgressBar()V

    return-void
.end method

.method public onInvalidated()V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;->updateShowProgressBar()V

    return-void
.end method
