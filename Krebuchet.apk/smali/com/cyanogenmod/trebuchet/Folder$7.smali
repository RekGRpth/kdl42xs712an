.class Lcom/cyanogenmod/trebuchet/Folder$7;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Folder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Folder;->animateClosed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Folder;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Folder;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Folder$7;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder$7;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    # invokes: Lcom/cyanogenmod/trebuchet/Folder;->onCloseComplete()V
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Folder;->access$11(Lcom/cyanogenmod/trebuchet/Folder;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder$7;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->access$5(Lcom/cyanogenmod/trebuchet/Folder;I)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder$7;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$7;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    # getter for: Lcom/cyanogenmod/trebuchet/Folder;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Folder;->access$3(Lcom/cyanogenmod/trebuchet/Folder;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0065    # com.konka.avenger.R.string.folder_closed

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/cyanogenmod/trebuchet/Folder;->sendCustomAccessibilityEvent(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Folder;->access$4(Lcom/cyanogenmod/trebuchet/Folder;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder$7;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->access$5(Lcom/cyanogenmod/trebuchet/Folder;I)V

    return-void
.end method
