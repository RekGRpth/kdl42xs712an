.class Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;
.super Ljava/lang/Object;
.source "LauncherModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/LauncherModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageUpdatedTask"
.end annotation


# static fields
.field public static final OP_ADD:I = 0x1

.field public static final OP_NONE:I = 0x0

.field public static final OP_REMOVE:I = 0x3

.field public static final OP_UNAVAILABLE:I = 0x4

.field public static final OP_UPDATE:I = 0x2


# instance fields
.field mOp:I

.field mPackages:[Ljava/lang/String;

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/LauncherModel;I[Ljava/lang/String;)V
    .locals 0
    .param p2    # I
    .param p3    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->mOp:I

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->mPackages:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;)Lcom/cyanogenmod/trebuchet/LauncherModel;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mApp:Lcom/cyanogenmod/trebuchet/LauncherApplication;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$17(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/LauncherApplication;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->mPackages:[Ljava/lang/String;

    array-length v1, v10

    move-object/from16 v0, p0

    iget v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->mOp:I

    packed-switch v14, :pswitch_data_0

    :cond_0
    const/4 v2, 0x0

    const/4 v12, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    iget-object v14, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->added:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    iget-object v2, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->added:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    iput-object v15, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->added:Ljava/util/ArrayList;

    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    iget-object v14, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->removed:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    iget-object v12, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->removed:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    iput-object v15, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->removed:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_4

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    iget-object v14, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->modified:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-lez v14, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    iget-object v8, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->modified:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    iput-object v15, v14, Lcom/cyanogenmod/trebuchet/AllAppsList;->modified:Ljava/util/ArrayList;

    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v14

    if-eqz v14, :cond_5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    move-object v4, v14

    :goto_1
    if-nez v4, :cond_6

    const-string v14, "Launcher.Model"

    const-string v15, "Nobody to tell about the new app.  Launcher is probably loading."

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void

    :pswitch_0
    const/4 v6, 0x0

    :goto_3
    if-ge v6, v1, :cond_0

    const-string v14, "Launcher.Model"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "mAllAppsList.addPackage "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v16, v10, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    aget-object v15, v10, v6

    invoke-virtual {v14, v5, v15}, Lcom/cyanogenmod/trebuchet/AllAppsList;->addPackage(Landroid/content/Context;Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :pswitch_1
    const/4 v6, 0x0

    :goto_4
    if-ge v6, v1, :cond_0

    const-string v14, "Launcher.Model"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "mAllAppsList.updatePackage "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v16, v10, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    aget-object v15, v10, v6

    invoke-virtual {v14, v5, v15}, Lcom/cyanogenmod/trebuchet/AllAppsList;->updatePackage(Landroid/content/Context;Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :pswitch_2
    const/4 v6, 0x0

    :goto_5
    if-ge v6, v1, :cond_0

    const-string v14, "Launcher.Model"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "mAllAppsList.removePackage "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v16, v10, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v14

    aget-object v15, v10, v6

    invoke-virtual {v14, v15}, Lcom/cyanogenmod/trebuchet/AllAppsList;->removePackage(Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;
    invoke-static {v15}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$16(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/IconCache;

    move-result-object v15

    iget-object v0, v7, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->intent:Landroid/content/Intent;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/cyanogenmod/trebuchet/IconCache;->remove(Landroid/content/ComponentName;)V

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_6
    if-eqz v2, :cond_7

    move-object v3, v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v14

    new-instance v15, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v4, v3}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$1;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Ljava/util/ArrayList;)V

    invoke-virtual {v14, v15}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    :cond_7
    if-eqz v8, :cond_8

    move-object v9, v8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v14

    new-instance v15, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v4, v9}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$2;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Ljava/util/ArrayList;)V

    invoke-virtual {v14, v15}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    :cond_8
    if-eqz v12, :cond_9

    move-object/from16 v0, p0

    iget v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->mOp:I

    const/4 v15, 0x4

    if-eq v14, v15, :cond_a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v14, v10}, Lcom/cyanogenmod/trebuchet/LauncherModel;->isUpdateWorkspace([Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_a

    const/4 v11, 0x1

    :goto_6
    move-object v13, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v14

    new-instance v15, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v4, v13, v11}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Ljava/util/ArrayList;Z)V

    invoke-virtual {v14, v15}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v14

    new-instance v15, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v4}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$4;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;)V

    invoke-virtual {v14, v15}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    goto/16 :goto_2

    :cond_a
    const/4 v11, 0x0

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
