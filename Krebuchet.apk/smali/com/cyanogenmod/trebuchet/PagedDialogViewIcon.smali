.class public Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;
.super Landroid/widget/FrameLayout;
.source "PagedDialogViewIcon.java"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;
    }
.end annotation


# static fields
.field private static final CHECKED_STATE_SET:[I

.field private static final TAG:Ljava/lang/String; = "PagedDialogViewIcon"


# instance fields
.field private mBroadcasting:Z

.field private mCheckbox:Landroid/widget/CheckBox;

.field private mOnCheckedChangeListener:Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;

.field private mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0    # android.R.attr.state_checked

    aput v2, v0, v1

    sput-object v0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, -0x2

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v2, 0x60000

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->setDescendantFocusability(I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03001d    # com.konka.avenger.R.layout.paged_dialog_apps_icon

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v2, 0x7f0d004c    # com.konka.avenger.R.id.paged_view_icon

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    invoke-direct {v1, v5, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    invoke-virtual {v2, v1}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setPadding(IIII)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    invoke-virtual {v2, v4}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setClickable(Z)V

    const v2, 0x7f0d004d    # com.konka.avenger.R.id.paged_view_checkbox

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    return-void
.end method


# virtual methods
.method public applyFromApplicationInfo(Lcom/cyanogenmod/trebuchet/ApplicationInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/ApplicationInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    invoke-virtual {v0, p1, p2}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->applyFromApplicationInfo(Lcom/cyanogenmod/trebuchet/ApplicationInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V

    return-void
.end method

.method public getApplicationInfo()Lcom/cyanogenmod/trebuchet/ApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mPagedViewIcon:Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2
    .param p1    # I

    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->CHECKED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mBroadcasting:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mBroadcasting:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mOnCheckedChangeListener:Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mOnCheckedChangeListener:Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;->onCheckedChanged(Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;Z)V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mBroadcasting:Z

    goto :goto_0
.end method

.method public setOnCheckedChangeListener(Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mOnCheckedChangeListener:Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon$OnCheckedChangeListener;

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    return-void
.end method
