.class Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;
.super Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;
.source "PendingAddItemInfo.java"


# instance fields
.field configurationData:Landroid/os/Parcelable;

.field icon:I

.field mimeType:Ljava/lang/String;

.field minHeight:I

.field minWidth:I

.field previewImage:I


# direct methods
.method public constructor <init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 1
    .param p1    # Landroid/appwidget/AppWidgetProviderInfo;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Parcelable;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->itemType:I

    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->componentName:Landroid/content/ComponentName;

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->minWidth:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->minHeight:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->previewImage:I

    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->icon:I

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->configurationData:Landroid/os/Parcelable;

    :cond_0
    return-void
.end method
