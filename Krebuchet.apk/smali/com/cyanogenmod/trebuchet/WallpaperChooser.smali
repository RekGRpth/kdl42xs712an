.class public Lcom/cyanogenmod/trebuchet/WallpaperChooser;
.super Landroid/app/Activity;
.source "WallpaperChooser.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Launcher.WallpaperChooser"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f03002e    # com.konka.avenger.R.layout.wallpaper_chooser_base

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/WallpaperChooser;->setContentView(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooser;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f0d0068    # com.konka.avenger.R.id.wallpaper_chooser_fragment

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->newInstance()Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooser;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v0, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
