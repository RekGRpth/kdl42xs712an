.class public Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;
.super Landroid/preference/DialogPreference;
.source "DoubleNumberPickerPreference.java"


# instance fields
.field private mDefault1:I

.field private mDefault2:I

.field private mMax1:I

.field private mMax2:I

.field private mMaxExternalKey1:Ljava/lang/String;

.field private mMaxExternalKey2:Ljava/lang/String;

.field private mMin1:I

.field private mMin2:I

.field private mMinExternalKey1:Ljava/lang/String;

.field private mMinExternalKey2:Ljava/lang/String;

.field private mNumberPicker1:Landroid/widget/NumberPicker;

.field private mNumberPicker2:Landroid/widget/NumberPicker;

.field private mPickerTitle1:Ljava/lang/String;

.field private mPickerTitle2:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v4, 0x5

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v2, Lcom/android/internal/R$styleable;->DialogPreference:[I

    invoke-virtual {p1, p2, v2, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget-object v2, Lcom/konka/avenger/R$styleable;->DoubleNumberPickerPreference:[I

    invoke-virtual {p1, p2, v2, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMaxExternalKey1:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMinExternalKey1:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMaxExternalKey2:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMinExternalKey2:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mPickerTitle1:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mPickerTitle2:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax1:I

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin1:I

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax2:I

    invoke-virtual {v1, v4, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin2:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin1:I

    invoke-virtual {v1, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault1:I

    const/4 v2, 0x1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin2:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault2:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private getPersistedValue(I)I
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault1:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\\|"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-ne p1, v4, :cond_0

    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault1:I

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :try_start_1
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    goto :goto_0

    :catch_1
    move-exception v0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault2:I

    goto :goto_0
.end method


# virtual methods
.method protected onCreateDialogView()Landroid/view/View;
    .locals 15

    const v14, 0x10203f9    # android.R.id.enter_pin

    const/4 v13, 0x0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax1:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin1:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax2:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin2:I

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMaxExternalKey1:Ljava/lang/String;

    if-eqz v10, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMaxExternalKey1:Ljava/lang/String;

    iget v12, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax1:I

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    :cond_0
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMinExternalKey1:Ljava/lang/String;

    if-eqz v10, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMinExternalKey1:Ljava/lang/String;

    iget v12, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin1:I

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    :cond_1
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMaxExternalKey2:Ljava/lang/String;

    if-eqz v10, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMaxExternalKey2:Ljava/lang/String;

    iget v12, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax2:I

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    :cond_2
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMinExternalKey2:Ljava/lang/String;

    if-eqz v10, :cond_3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMinExternalKey2:Ljava/lang/String;

    iget v12, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin2:I

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    :cond_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "layout_inflater"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v10, 0x7f03000d    # com.konka.avenger.R.layout.double_number_picker_dialog

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    const v10, 0x7f0d0027    # com.konka.avenger.R.id.number_picker_1

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/NumberPicker;

    iput-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    const v10, 0x7f0d0029    # com.konka.avenger.R.id.number_picker_2

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/NumberPicker;

    iput-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    if-nez v10, :cond_5

    :cond_4
    new-instance v10, Ljava/lang/RuntimeException;

    const-string v11, "mNumberPicker1 or mNumberPicker2 is null!"

    invoke-direct {v10, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_5
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v13}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v3}, Landroid/widget/NumberPicker;->setMinValue(I)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    const/4 v11, 0x1

    invoke-direct {p0, v11}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getPersistedValue(I)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/NumberPicker;->setValue(I)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v13}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v4}, Landroid/widget/NumberPicker;->setMinValue(I)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    const/4 v11, 0x2

    invoke-direct {p0, v11}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->getPersistedValue(I)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/NumberPicker;->setValue(I)V

    const v10, 0x7f0d0026    # com.konka.avenger.R.id.picker_title_1

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v10, 0x7f0d0028    # com.konka.avenger.R.id.picker_title_2

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    if-eqz v5, :cond_6

    if-eqz v6, :cond_6

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mPickerTitle1:Ljava/lang/String;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mPickerTitle2:Ljava/lang/String;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    invoke-virtual {v10, v14}, Landroid/widget/NumberPicker;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    if-eqz v7, :cond_7

    if-eqz v8, :cond_7

    invoke-virtual {v7, v13}, Landroid/widget/EditText;->setCursorVisible(Z)V

    invoke-virtual {v7, v13}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {v7, v13}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    invoke-virtual {v8, v13}, Landroid/widget/EditText;->setCursorVisible(Z)V

    invoke-virtual {v8, v13}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {v8, v13}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    :cond_7
    return-object v9
.end method

.method protected onDialogClosed(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker1:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mNumberPicker2:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->persistString(Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method public setDefault1(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault1:I

    return-void
.end method

.method public setDefault2(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mDefault2:I

    return-void
.end method

.method public setMax1(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax1:I

    return-void
.end method

.method public setMax2(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMax2:I

    return-void
.end method

.method public setMin1(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin1:I

    return-void
.end method

.method public setMin2(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/preference/DoubleNumberPickerPreference;->mMin2:I

    return-void
.end method
