.class Lcom/cyanogenmod/trebuchet/Launcher$17;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Launcher;->showAppsCustomizeHelper(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field animationCancelled:Z

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;

.field private final synthetic val$instance:Lcom/cyanogenmod/trebuchet/Launcher;

.field private final synthetic val$scaleAnim:Landroid/animation/ValueAnimator;

.field private final synthetic val$springLoaded:Z

.field private final synthetic val$toView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/ValueAnimator;Z)V
    .locals 1

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$instance:Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p4, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$scaleAnim:Landroid/animation/ValueAnimator;

    iput-boolean p5, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$springLoaded:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->animationCancelled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->animationCancelled:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$instance:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$scaleAnim:Landroid/animation/ValueAnimator;

    invoke-interface {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;->onLauncherTransitionEnd(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$springLoaded:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->access$0(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->hideScrollingIndicator(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->hideDockDivider()V

    :cond_1
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->animationCancelled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->updateWallpaperVisibility(Z)V

    :cond_2
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->updateWallpaperVisibility(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$17;->val$toView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    return-void
.end method
