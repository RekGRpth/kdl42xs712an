.class Lcom/cyanogenmod/trebuchet/Launcher$3;
.super Landroid/os/Handler;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$6(Lcom/cyanogenmod/trebuchet/Launcher;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const-wide/16 v5, 0x4e20

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->sendAdvanceMessage(J)V
    invoke-static {v4, v5, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->access$7(Lcom/cyanogenmod/trebuchet/Launcher;J)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$6(Lcom/cyanogenmod/trebuchet/Launcher;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/appwidget/AppWidgetProviderInfo;

    iget v4, v4, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    mul-int/lit16 v0, v1, 0xfa

    instance-of v4, v3, Landroid/widget/Advanceable;

    if-eqz v4, :cond_2

    new-instance v4, Lcom/cyanogenmod/trebuchet/Launcher$3$1;

    invoke-direct {v4, p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher$3$1;-><init>(Lcom/cyanogenmod/trebuchet/Launcher$3;Landroid/view/View;)V

    int-to-long v6, v0

    invoke-virtual {p0, v4, v6, v7}, Lcom/cyanogenmod/trebuchet/Launcher$3;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->ClearTVManager_RestorSTR()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const-string v5, "0"

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->SetPropertyForSTR(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->access$8(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$9(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/konka/avenger/tv/TVWindowManager;

    move-result-object v4

    if-eqz v4, :cond_4

    const-string v4, "Launcher"

    const-string v5, "start to resume tv window!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "Launcher"

    const-string v5, "restart to resume tv window!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->destroyTVWindow()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->setupTVWindow()V
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$10(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$9(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/konka/avenger/tv/TVWindowManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/avenger/tv/TVWindowManager;->resetTVWindow()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissTVMute()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$9(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/konka/avenger/tv/TVWindowManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/avenger/tv/TVWindowManager;->hideBackground()V

    goto :goto_1

    :cond_4
    const-string v4, "Launcher"

    const-string v5, "mTVWindowManager====null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$3;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->setupTVWindow()V
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$10(Lcom/cyanogenmod/trebuchet/Launcher;)V

    goto :goto_1
.end method
