.class public abstract Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;
.super Landroid/widget/BaseAdapter;
.source "VidgetAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/VidgetAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "VidgetGridAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/BaseAdapter;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field protected mCount:I

.field protected mStartIdx:I

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/VidgetAdapter;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/VidgetAdapter;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;->this$0:Lcom/cyanogenmod/trebuchet/VidgetAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput v0, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;->mStartIdx:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;->mCount:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;->mCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;->mStartIdx:I

    add-int/2addr v0, p1

    int-to-long v0, v0

    return-wide v0
.end method

.method public setRange(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;->mStartIdx:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;->mCount:I

    return-void
.end method
