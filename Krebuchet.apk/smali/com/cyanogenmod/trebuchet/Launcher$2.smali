.class Lcom/cyanogenmod/trebuchet/Launcher$2;
.super Landroid/content/BroadcastReceiver;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v2, 0x1

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v1, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->access$1(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$2(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$3(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$4(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$5(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/ItemInfo;

    move-result-object v1

    iget-wide v1, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$4(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->reset()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->showWorkspace(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$1(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$3(Lcom/cyanogenmod/trebuchet/Launcher;)V

    goto :goto_0

    :cond_2
    const-string v1, "com.konka.launcher.action.SHOW_ALL_APP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$2;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showAllApps(Z)V

    goto :goto_0
.end method
