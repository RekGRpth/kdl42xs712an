.class public Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;
.super Landroid/appwidget/AppWidgetHostView;
.source "LauncherAppWidgetHostView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;
    }
.end annotation


# static fields
.field public static final CHILDS_FOCUSABLE:I = 0x1

.field public static final PARENT_FOCUSABLE:I


# instance fields
.field private mFocusableState:I

.field private mHasPerformedLongPress:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetProviderInfo;

    const v5, 0x7f02004b    # com.konka.avenger.R.drawable.home_selectbox_s

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/appwidget/AppWidgetHostView;-><init>(Landroid/content/Context;)V

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mInflater:Landroid/view/LayoutInflater;

    iput v3, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mFocusableState:I

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->setFocusable(Z)V

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v4, [I

    const v2, 0x101009c    # android.R.attr.state_focused

    aput v2, v1, v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v4, [I

    const v2, 0x1010367    # android.R.attr.state_hovered

    aput v2, v1, v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020006    # com.konka.avenger.R.drawable.app_hover

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;)Landroid/view/ViewParent;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mParent:Landroid/view/ViewParent;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;)I
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mHasPerformedLongPress:Z

    return v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mHasPerformedLongPress:Z

    return-void
.end method

.method private postCheckForLongClick()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mHasPerformedLongPress:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    if-nez v0, :cond_0

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;-><init>(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;->rememberWindowAttachCount()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public cancelLongPress()V
    .locals 1

    invoke-super {p0}, Landroid/appwidget/AppWidgetHostView;->cancelLongPress()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mHasPerformedLongPress:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetHostView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mFocusableState:I

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->requestParentFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public getDescendantFocusability()I
    .locals 2

    const/high16 v0, 0x60000

    iget v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mFocusableState:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/high16 v0, 0x20000

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getErrorView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030009    # com.konka.avenger.R.layout.appwidget_error

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mHasPerformedLongPress:Z

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mHasPerformedLongPress:Z

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->postCheckForLongClick()V

    goto :goto_0

    :pswitch_2
    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mHasPerformedLongPress:Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mPendingCheckForLongPress:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView$CheckForLongPress;

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public requestChildFocus()Z
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x1

    iput v4, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mFocusableState:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    return v3

    :cond_1
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSearchScreen(Lcom/cyanogenmod/trebuchet/CellLayout;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput v2, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mFocusableState:I

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetHostView;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v2

    return v2
.end method

.method public requestParentFocus()Z
    .locals 4

    const/4 v3, 0x0

    iput v3, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->mFocusableState:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    invoke-super {p0}, Landroid/appwidget/AppWidgetHostView;->requestFocus()Z

    move-result v3

    return v3

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    invoke-virtual {v0, v3}, Landroid/view/View;->setPressed(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
