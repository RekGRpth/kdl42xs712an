.class public Lcom/cyanogenmod/trebuchet/Workspace;
.super Lcom/cyanogenmod/trebuchet/PagedView;
.source "Workspace.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/cyanogenmod/trebuchet/DragController$DragListener;
.implements Lcom/cyanogenmod/trebuchet/DragScroller;
.implements Lcom/cyanogenmod/trebuchet/DragSource;
.implements Lcom/cyanogenmod/trebuchet/DropTarget;
.implements Lnet/londatiga/android/QuickAction$OnActionItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;,
        Lcom/cyanogenmod/trebuchet/Workspace$InverseZInterpolator;,
        Lcom/cyanogenmod/trebuchet/Workspace$State;,
        Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;,
        Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;,
        Lcom/cyanogenmod/trebuchet/Workspace$WallpaperVerticalOffset;,
        Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;,
        Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;,
        Lcom/cyanogenmod/trebuchet/Workspace$ZoomOutInterpolator;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$Workspace$TransitionEffect:[I = null

.field private static final ADJACENT_SCREEN_DROP_DURATION:I = 0x12c

.field private static final BACKGROUND_FADE_OUT_DURATION:I = 0x15e

.field private static CAMERA_DISTANCE:F = 0.0f

.field private static final CHILDREN_OUTLINE_FADE_IN_DURATION:I = 0x64

.field private static final CHILDREN_OUTLINE_FADE_OUT_DELAY:I = 0x0

.field private static final CHILDREN_OUTLINE_FADE_OUT_DURATION:I = 0x177

.field private static final DEFAULT_CELL_COUNT_X:I = 0x8

.field private static final DEFAULT_CELL_COUNT_Y:I = 0x2

.field private static final FOLDER_CREATION_TIMEOUT:I = 0xfa

.field private static final ID_QUICKACTION_APP_DELETE:I = 0x2

.field private static final ID_QUICKACTION_APP_MOVE:I = 0x1

.field private static final ID_QUICKACTION_APP_RUN:I = 0x0

.field private static final ID_QUICKACTION_FOLDER_DELETE:I = 0x3

.field private static final ID_QUICKACTION_FOLDER_MOVE:I = 0x2

.field private static final ID_QUICKACTION_FOLDER_RENAME:I = 0x1

.field private static final ID_QUICKACTION_FOLDER_RUN:I = 0x0

.field private static final ID_QUICKACTION_WIDGET_DELETE:I = 0x1

.field private static final ID_QUICKACTION_WIDGET_MOVE:I = 0x0

.field static final MAX_SWIPE_ANGLE:F = 1.0471976f

.field private static final SEARCH_SCREEN_PAGE:I = 0x0

.field static final START_DAMPING_TOUCH_SLOP_ANGLE:F = 0.5235988f

.field private static STATE_TO_RUN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Launcher.Workspace"

.field static final TOUCH_SLOP_DAMPING_FACTOR:F = 4.0f

.field private static final WALLPAPER_SCREENS_SPAN:F = 2.0f

.field private static final WORKSPACE_OVERSCROLL_ROTATION:F = 24.0f

.field private static final WORKSPACE_ROTATION:F = 12.5f

.field private static final WORKSPACE_ROTATION_ANGLE:F = 12.5f


# instance fields
.field private mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

.field mAnimatingViewIntoPlace:Z

.field private mAnimator:Landroid/animation/AnimatorSet;

.field private mAppQuickAction:Lnet/londatiga/android/QuickAction;

.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mBackgroundAlpha:F

.field private mBackgroundFadeInAnimation:Landroid/animation/ValueAnimator;

.field private mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

.field private final mCamera:Landroid/graphics/Camera;

.field private mChangeStateAnimationListener:Landroid/animation/Animator$AnimatorListener;

.field mChildrenLayersEnabled:Z

.field private mChildrenOutlineAlpha:F

.field private mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

.field private mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

.field private mCreateUserFolderOnDrop:Z

.field private mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

.field private mCurrentRotationY:F

.field private mCurrentScaleX:F

.field private mCurrentScaleY:F

.field private mCurrentTranslationX:F

.field private mCurrentTranslationY:F

.field private mDefaultHomescreen:I

.field private mDelayedResizeRunnable:Ljava/lang/Runnable;

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mDragController:Lcom/cyanogenmod/trebuchet/DragController;

.field private mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

.field private mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

.field private mDragOutline:Landroid/graphics/Bitmap;

.field private mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

.field private mDragViewMultiplyColor:I

.field private mDragViewVisualCenter:[F

.field mDrawBackground:Z

.field private final mExternalDragOutlinePaint:Landroid/graphics/Paint;

.field private mFadeScrollingIndicator:Z

.field private final mFolderCreationAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

.field private mFolderQuickAction:Lnet/londatiga/android/QuickAction;

.field private mHideIconLabels:Z

.field private mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

.field private mInScrollArea:Z

.field mIsDragOccuring:Z

.field private mIsSwitchingState:Z

.field private mLastDragOverView:Landroid/view/View;

.field public mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mLeftScreenAlphaInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private final mMatrix:Landroid/graphics/Matrix;

.field private mNewAlphas:[F

.field private mNewBackgroundAlphaMultipliers:[F

.field private mNewBackgroundAlphas:[F

.field private mNewRotationYs:[F

.field private mNewRotations:[F

.field private mNewScaleXs:[F

.field private mNewScaleYs:[F

.field private mNewTranslationXs:[F

.field private mNewTranslationYs:[F

.field private mNumberHomescreens:I

.field private mOldAlphas:[F

.field private mOldBackgroundAlphaMultipliers:[F

.field private mOldBackgroundAlphas:[F

.field private mOldRotationYs:[F

.field private mOldRotations:[F

.field private mOldScaleXs:[F

.field private mOldScaleYs:[F

.field private mOldTranslationXs:[F

.field private mOldTranslationYs:[F

.field private final mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

.field private mOverScrollMaxBackgroundAlpha:F

.field private mOverScrollPageIndex:I

.field private mOverscrollFade:F

.field private mResizeAnyWidget:Z

.field private mSavedRotationY:F

.field private mSavedScrollX:I

.field private mSavedTranslationX:F

.field private mScreenPaddingHorizontal:I

.field private mScreenPaddingVertical:I

.field private mScrollWallpaper:Z

.field private mShowDockDivider:Z

.field private mShowScrollingIndicator:Z

.field private mShowSearchBar:Z

.field private mSpringLoadedDragController:Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;

.field private mSpringLoadedShrinkFactor:F

.field private mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

.field private mStateAfterFirstLayout:Lcom/cyanogenmod/trebuchet/Workspace$State;

.field private mSwitchStateAfterFirstLayout:Z

.field private mTargetCell:[I

.field private mTempCell:[I

.field private mTempCellLayoutCenterCoordinates:[F

.field private mTempDragBottomRightCoordinates:[F

.field private mTempDragCoordinates:[F

.field private mTempEstimate:[I

.field private final mTempFloat2:[F

.field private mTempInverseMatrix:Landroid/graphics/Matrix;

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mTempXY:[I

.field private mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

.field private mTransitionProgress:F

.field mUpdateWallpaperOffsetImmediately:Z

.field mWallpaperHeight:I

.field private final mWallpaperManager:Landroid/app/WallpaperManager;

.field mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

.field private mWallpaperScrollRatio:F

.field private mWallpaperTravelWidth:I

.field mWallpaperWidth:I

.field private mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

.field private mWindowToken:Landroid/os/IBinder;

.field private mXDown:F

.field private mYDown:F

.field private final mZInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

.field private final mZoomInInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;


# direct methods
.method static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$Workspace$TransitionEffect()[I
    .locals 3

    sget-object v0, Lcom/cyanogenmod/trebuchet/Workspace;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$Workspace$TransitionEffect:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->values()[Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->CubeIn:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->CubeOut:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateDown:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateUp:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->Stack:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->Standard:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->Tablet:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ZoomIn:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ZoomOut:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lcom/cyanogenmod/trebuchet/Workspace;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$Workspace$TransitionEffect:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const v0, 0x45cb2000    # 6500.0f

    sput v0, Lcom/cyanogenmod/trebuchet/Workspace;->CAMERA_DISTANCE:F

    const/4 v0, 0x1

    sput v0, Lcom/cyanogenmod/trebuchet/Workspace;->STATE_TO_RUN:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/Workspace;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/cyanogenmod/trebuchet/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineAlpha:F

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDrawBackground:Z

    iput v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundAlpha:F

    iput v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollMaxBackgroundAlpha:F

    const/4 v5, -0x1

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollPageIndex:I

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperScrollRatio:F

    new-array v5, v6, [I

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    iput-object v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    new-array v5, v6, [I

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempCell:[I

    new-array v5, v6, [I

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempEstimate:[I

    new-array v5, v6, [F

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    new-array v5, v6, [F

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempDragCoordinates:[F

    new-array v5, v6, [F

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempCellLayoutCenterCoordinates:[F

    new-array v5, v6, [F

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempDragBottomRightCoordinates:[F

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempInverseMatrix:Landroid/graphics/Matrix;

    sget-object v5, Lcom/cyanogenmod/trebuchet/Workspace$State;->NORMAL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSwitchStateAfterFirstLayout:Z

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimatingViewIntoPlace:Z

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsDragOccuring:Z

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenLayersEnabled:Z

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    new-instance v5, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-direct {v5}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    iput-object v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempRect:Landroid/graphics/Rect;

    new-array v5, v6, [I

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempXY:[I

    iput v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverscrollFade:F

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mExternalDragOutlinePaint:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMatrix:Landroid/graphics/Matrix;

    new-instance v5, Landroid/graphics/Camera;

    invoke-direct {v5}, Landroid/graphics/Camera;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCamera:Landroid/graphics/Camera;

    new-array v5, v6, [F

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempFloat2:[F

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mUpdateWallpaperOffsetImmediately:Z

    new-instance v5, Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-direct {v5}, Lcom/cyanogenmod/trebuchet/Alarm;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderCreationAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    iput-object v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    iput-object v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    new-instance v5, Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;

    invoke-direct {v5}, Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;-><init>()V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mZoomInInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;

    new-instance v5, Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-direct {v5, v6}, Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;-><init>(F)V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mZInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    const v6, 0x3f666666    # 0.9f

    invoke-direct {v5, v6}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v6, 0x40800000    # 4.0f

    invoke-direct {v5, v6}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLeftScreenAlphaInterpolator:Landroid/view/animation/DecelerateInterpolator;

    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mContentIsRefreshable:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->setDataIsReady()V

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mHandleFadeInAdjacentScreens:Z

    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v5

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperManager:Landroid/app/WallpaperManager;

    const/16 v2, 0x8

    const/4 v3, 0x2

    sget-object v5, Lcom/konka/avenger/R$styleable;->Workspace:[I

    invoke-virtual {p1, p2, v5, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getCellCountsForLarge(Landroid/content/Context;)[I

    move-result-object v1

    aget v2, v1, v7

    aget v3, v1, v8

    :cond_0
    const v5, 0x7f0b0005    # com.konka.avenger.R.integer.config_workspaceSpringLoadShrinkPercentage

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedShrinkFactor:F

    const v5, 0x7f080002    # com.konka.avenger.R.color.drag_view_multiply_color

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewMultiplyColor:I

    invoke-virtual {v0, v7, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v8, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {p1, v2}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getCellCountX(Landroid/content/Context;I)I

    move-result v2

    invoke-static {p1, v3}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getCellCountY(Landroid/content/Context;I)I

    move-result v3

    :cond_1
    invoke-static {v2, v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateWorkspaceLayoutCells(II)V

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->setHapticFeedbackEnabled(Z)V

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getNumberHomescreens(Landroid/content/Context;)I

    move-result v5

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNumberHomescreens:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0025    # com.konka.avenger.R.integer.default_workspace_page

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-static {p1, v5}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getDefaultHomescreen(Landroid/content/Context;I)I

    move-result v5

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDefaultHomescreen:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDefaultHomescreen:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNumberHomescreens:I

    if-lt v5, v6, :cond_2

    iget v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNumberHomescreens:I

    div-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDefaultHomescreen:I

    :cond_2
    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getScreenPaddingVertical(Landroid/content/Context;)I

    move-result v5

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScreenPaddingVertical:I

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getScreenPaddingHorizontal(Landroid/content/Context;)I

    move-result v5

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScreenPaddingHorizontal:I

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getShowSearchBar(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowSearchBar:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getResizeAnyWidget(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mResizeAnyWidget:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getHideIconLabels(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mHideIconLabels:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen$Scrolling;->getScrollWallpaper(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    const v5, 0x7f0a0001    # com.konka.avenger.R.string.config_workspaceDefaultTransitionEffect

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen$Scrolling;->getTransitionEffect(Landroid/content/Context;Ljava/lang/String;)Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-result-object v5

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    const v5, 0x7f090008    # com.konka.avenger.R.bool.config_workspaceDefualtFadeInAdjacentScreens

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    invoke-static {p1, v5}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen$Scrolling;->getFadeInAdjacentScreens(Landroid/content/Context;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeInAdjacentScreens:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen$Indicator;->getShowScrollingIndicator(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowScrollingIndicator:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen$Indicator;->getFadeScrollingIndicator(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeScrollingIndicator:Z

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen$Indicator;->getShowDockDivider(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowDockDivider:Z

    check-cast p1, Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->initWorkspace()V

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->setMotionEventSplittingEnabled(Z)V

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/Workspace;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/Workspace;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$11(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/Workspace$State;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mStateAfterFirstLayout:Lcom/cyanogenmod/trebuchet/Workspace$State;

    return-object v0
.end method

.method static synthetic access$12(Lcom/cyanogenmod/trebuchet/Workspace;F)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionProgress:F

    return-void
.end method

.method static synthetic access$13(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationXs:[F

    return-object v0
.end method

.method static synthetic access$14(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationXs:[F

    return-object v0
.end method

.method static synthetic access$15(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationYs:[F

    return-object v0
.end method

.method static synthetic access$16(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationYs:[F

    return-object v0
.end method

.method static synthetic access$17(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleXs:[F

    return-object v0
.end method

.method static synthetic access$18(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleXs:[F

    return-object v0
.end method

.method static synthetic access$19(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleYs:[F

    return-object v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    return-object v0
.end method

.method static synthetic access$20(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleYs:[F

    return-object v0
.end method

.method static synthetic access$21(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphas:[F

    return-object v0
.end method

.method static synthetic access$22(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphas:[F

    return-object v0
.end method

.method static synthetic access$23(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphaMultipliers:[F

    return-object v0
.end method

.method static synthetic access$24(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphaMultipliers:[F

    return-object v0
.end method

.method static synthetic access$25(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldAlphas:[F

    return-object v0
.end method

.method static synthetic access$26(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewAlphas:[F

    return-object v0
.end method

.method static synthetic access$27(Lcom/cyanogenmod/trebuchet/Workspace;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->syncChildrenLayersEnabledOnVisiblePages()V

    return-void
.end method

.method static synthetic access$28(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldRotations:[F

    return-object v0
.end method

.method static synthetic access$29(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewRotations:[F

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    return-void
.end method

.method static synthetic access$30(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldRotationYs:[F

    return-object v0
.end method

.method static synthetic access$31(Lcom/cyanogenmod/trebuchet/Workspace;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewRotationYs:[F

    return-object v0
.end method

.method static synthetic access$32(Lcom/cyanogenmod/trebuchet/Workspace;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDelayedResizeRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$33(Lcom/cyanogenmod/trebuchet/Workspace;)[I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    return-object v0
.end method

.method static synthetic access$34(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/DragController;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    return-object v0
.end method

.method static synthetic access$35(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    return-object v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/Workspace;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    return-void
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/Workspace;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    return-void
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/Workspace;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    return v0
.end method

.method static synthetic access$7(Lcom/cyanogenmod/trebuchet/Workspace;Landroid/animation/AnimatorSet;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    return-void
.end method

.method static synthetic access$8(Lcom/cyanogenmod/trebuchet/Workspace;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    return-void
.end method

.method static synthetic access$9(Lcom/cyanogenmod/trebuchet/Workspace;)Landroid/app/WallpaperManager;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperManager:Landroid/app/WallpaperManager;

    return-object v0
.end method

.method private animateBackgroundGradient(FZ)V
    .locals 4
    .param p1    # F
    .param p2    # Z

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackground:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeInAnimation:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeInAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeInAnimation:Landroid/animation/ValueAnimator;

    :cond_2
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    :cond_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getBackgroundAlpha()F

    move-result v0

    cmpl-float v1, p1, v0

    if-eqz v1, :cond_0

    if-eqz p2, :cond_4

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v2, 0x1

    aput p1, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/cyanogenmod/trebuchet/Workspace$3;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/Workspace$3;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundFadeOutAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->setBackgroundAlpha(F)V

    goto :goto_0
.end method

.method private cancelFolderCreation()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->animateToNaturalState()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderCreationAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    return-void
.end method

.method private centerWallpaperOffset()V
    .locals 4

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {v0, v3, v2}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v2}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    return-void
.end method

.method private cleanupFolderCreation(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->animateToNaturalState()V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDragExit(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderCreationAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    return-void
.end method

.method private computeWallpaperScrollRatio(I)V
    .locals 6
    .param p1    # I

    const/high16 v5, 0x3f800000    # 1.0f

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLayoutScale:F

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildOffset(I)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getRelativeChildOffset(I)I

    move-result v4

    sub-int v1, v3, v4

    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLayoutScale:F

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildOffset(I)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getRelativeChildOffset(I)I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v2, v3

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLayoutScale:F

    if-lez v1, :cond_0

    mul-float v3, v5, v2

    int-to-float v4, v1

    div-float/2addr v3, v4

    iput v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperScrollRatio:F

    :goto_0
    return-void

    :cond_0
    iput v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperScrollRatio:F

    goto :goto_0
.end method

.method private createDragOutline(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIILandroid/graphics/Paint;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f080007    # com.konka.avenger.R.color.konka_orange

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    new-instance v12, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v12, v2, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    sub-int v2, p4, p3

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    sub-int v4, p5, p3

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v4, v6

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v9

    float-to-int v11, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v9

    float-to-int v10, v2

    new-instance v8, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {v8, v2, v4, v11, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    sub-int v2, p4, v11

    div-int/lit8 v2, v2, 0x2

    sub-int v4, p5, v10

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v8, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    const/4 v2, 0x0

    invoke-virtual {p2, p1, v12, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    move-object v4, p2

    move v6, v5

    move-object/from16 v7, p6

    invoke-virtual/range {v2 .. v7}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->applyMediumExpensiveOutlineWithBlur(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V

    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    return-object v3
.end method

.method private createDragOutline(Landroid/view/View;Landroid/graphics/Canvas;I)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080007    # com.konka.avenger.R.color.konka_orange

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v2, p3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, p3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, p3, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->drawDragView(Landroid/view/View;Landroid/graphics/Canvas;IZ)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {v2, v0, p2, v1, v1}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->applyMediumExpensiveOutlineWithBlur(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private createExternalDragOutline(Landroid/graphics/Canvas;I)Landroid/graphics/Bitmap;
    .locals 12
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f080007    # com.konka.avenger.R.color.konka_orange

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const v7, 0x7f0c000a    # com.konka.avenger.R.dimen.workspace_cell_width

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v7, 0x7f0c000b    # com.konka.avenger.R.dimen.workspace_cell_height

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v7, 0x7f0c005b    # com.konka.avenger.R.dimen.external_drop_icon_rect_radius

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v7

    int-to-float v7, v7

    const v8, 0x3e4ccccd    # 0.2f

    mul-float/2addr v7, v8

    float-to-int v3, v7

    add-int v7, v2, p2

    add-int v8, v1, p2

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    new-instance v7, Landroid/graphics/RectF;

    int-to-float v8, v3

    int-to-float v9, v3

    sub-int v10, v2, v3

    int-to-float v10, v10

    sub-int v11, v1, v3

    int-to-float v11, v11

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    int-to-float v8, v6

    int-to-float v9, v6

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mExternalDragOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {v7, v0, p1, v4, v4}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->applyMediumExpensiveOutlineWithBlur(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private deleteAppWidget(Lcom/cyanogenmod/trebuchet/ItemInfo;)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object v2, p1

    check-cast v2, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    invoke-virtual {v3, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->removeAppWidget(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v2, p1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    move-object v1, p1

    check-cast v1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getAppWidgetHost()Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/cyanogenmod/trebuchet/Workspace$14;

    const-string v3, "deleteAppWidgetId"

    invoke-direct {v2, p0, v3, v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace$14;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Ljava/lang/String;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace$14;->start()V

    :cond_0
    return-void
.end method

.method private doDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->cleanupFolderCreation(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->onResetScrollArea()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragExit()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedDragController:Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->cancel()V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsPageMoving:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->hideOutlines()V

    :cond_1
    return-void
.end method

.method private drawDragView(Landroid/view/View;Landroid/graphics/Canvas;IZ)V
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # I
    .param p4    # Z

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    instance-of v4, p1, Landroid/widget/TextView;

    if-eqz v4, :cond_1

    if-eqz p4, :cond_1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aget-object v1, v4, v7

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v4, p3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v5, p3

    invoke-virtual {v0, v6, v6, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    div-int/lit8 v4, p3, 0x2

    int-to-float v4, v4

    div-int/lit8 v5, p3, 0x2

    int-to-float v5, v5

    invoke-virtual {p2, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_1
    instance-of v4, p1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mHideIconLabels:Z

    if-nez v4, :cond_2

    move-object v4, p1

    check-cast v4, Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getTextVisible()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v4, p1

    check-cast v4, Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-virtual {v4, v6}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTextVisible(Z)V

    const/4 v2, 0x1

    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v5, p3, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v5

    neg-int v5, v5

    div-int/lit8 v6, p3, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p2, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    sget-object v4, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p2, v0, v4}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    invoke-virtual {p1, p2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mHideIconLabels:Z

    if-nez v4, :cond_0

    if-eqz v2, :cond_0

    check-cast p1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-virtual {p1, v7}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTextVisible(Z)V

    goto :goto_0

    :cond_3
    instance-of v4, p1, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v4, :cond_4

    move-object v3, p1

    check-cast v3, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getExtendedPaddingTop()I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineTop(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    :cond_4
    instance-of v4, p1, Landroid/widget/TextView;

    if-eqz v4, :cond_2

    move-object v3, p1

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getExtendedPaddingTop()I

    move-result v4

    invoke-virtual {v3}, Landroid/widget/TextView;->getCompoundDrawablePadding()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineTop(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1
.end method

.method private enableHwLayersOnVisiblePages()V
    .locals 8

    const v7, 0x3caaaaab

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenLayersEnabled:Z

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v4

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempVisiblePagesRange:[I

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->getVisiblePages([I)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempVisiblePagesRange:[I

    const/4 v6, 0x0

    aget v2, v5, v6

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempVisiblePagesRange:[I

    const/4 v6, 0x1

    aget v3, v5, v6

    if-ne v2, v3, :cond_0

    add-int/lit8 v5, v4, -0x1

    if-ge v3, v5, :cond_2

    add-int/lit8 v3, v3, 0x1

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-lt v0, v4, :cond_3

    const/4 v0, 0x0

    :goto_2
    if-lt v0, v4, :cond_6

    :cond_1
    return-void

    :cond_2
    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-gt v2, v0, :cond_4

    if-gt v0, v3, :cond_4

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getVisibility()I

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getAlpha()F

    move-result v5

    cmpl-float v5, v5, v7

    if-gtz v5, :cond_5

    :cond_4
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->disableHardwareLayers()V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-gt v2, v0, :cond_7

    if-gt v0, v3, :cond_7

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getVisibility()I

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getAlpha()F

    move-result v5

    cmpl-float v5, v5, v7

    if-lez v5, :cond_7

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->enableHardwareLayers()V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private findMatchingPageForDragOver(Lcom/cyanogenmod/trebuchet/DragView;FFZ)Lcom/cyanogenmod/trebuchet/CellLayout;
    .locals 10
    .param p1    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p2    # F
    .param p3    # F
    .param p4    # Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v5

    const/4 v0, 0x0

    const v6, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_1

    move-object v2, v0

    :cond_0
    return-object v2

    :cond_1
    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v8, 0x2

    new-array v7, v8, [F

    const/4 v8, 0x0

    aput p2, v7, v8

    const/4 v8, 0x1

    aput p3, v7, v8

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempInverseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v9}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempInverseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2, v7, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    const/4 v8, 0x0

    aget v8, v7, v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_2

    const/4 v8, 0x0

    aget v8, v7, v8

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getWidth()I

    move-result v9

    int-to-float v9, v9

    cmpg-float v8, v8, v9

    if-gtz v8, :cond_2

    const/4 v8, 0x1

    aget v8, v7, v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_2

    const/4 v8, 0x1

    aget v8, v7, v8

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getHeight()I

    move-result v9

    int-to-float v9, v9

    cmpg-float v8, v8, v9

    if-lez v8, :cond_0

    :cond_2
    if-nez p4, :cond_3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempCellLayoutCenterCoordinates:[F

    const/4 v8, 0x0

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    aput v9, v1, v8

    const/4 v8, 0x1

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    aput v9, v1, v8

    invoke-virtual {p0, v2, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromChildToSelf(Landroid/view/View;[F)V

    const/4 v8, 0x0

    aput p2, v7, v8

    const/4 v8, 0x1

    aput p3, v7, v8

    invoke-static {v7, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->squaredDistance([F[F)F

    move-result v3

    cmpg-float v8, v3, v6

    if-gez v8, :cond_3

    move v6, v3

    move-object v0, v2

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private findNearestArea(IIIILcom/cyanogenmod/trebuchet/CellLayout;[I)[I
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p6    # [I

    move-object v0, p5

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestArea(IIII[I)[I

    move-result-object v0

    return-object v0
.end method

.method private findNearestVacantArea(IIIILandroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[I)[I
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/view/View;
    .param p6    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p7    # [I

    move-object v0, p6

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestVacantArea(IIIILandroid/view/View;[I)[I

    move-result-object v0

    return-object v0
.end method

.method public static getCellCountsForLarge(Landroid/content/Context;)[I
    .locals 11
    .param p0    # Landroid/content/Context;

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v7, 0x2

    new-array v2, v7, [I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v7, v9, [I

    const v8, 0x10102eb    # android.R.attr.actionBarSize

    aput v8, v7, v10

    invoke-virtual {p0, v7}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v7, 0x0

    invoke-virtual {v1, v10, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    const v7, 0x7f0c0034    # com.konka.avenger.R.dimen.status_bar_height

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v7

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v5, v7

    aput v9, v2, v10

    :goto_0
    aget v7, v2, v10

    add-int/lit8 v7, v7, 0x1

    invoke-static {v3, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->widthInPortrait(Landroid/content/res/Resources;I)I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v7, v5

    if-lez v7, :cond_0

    aput v9, v2, v9

    :goto_1
    aget v7, v2, v9

    add-int/lit8 v7, v7, 0x1

    invoke-static {v3, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->heightInLandscape(Landroid/content/res/Resources;I)I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v7, v0

    sub-float v8, v4, v6

    cmpg-float v7, v7, v8

    if-lez v7, :cond_1

    return-object v2

    :cond_0
    aget v7, v2, v10

    add-int/lit8 v7, v7, 0x1

    aput v7, v2, v10

    goto :goto_0

    :cond_1
    aget v7, v2, v9

    add-int/lit8 v7, v7, 0x1

    aput v7, v2, v9

    goto :goto_1
.end method

.method private getDragViewVisualCenter(IIIILcom/cyanogenmod/trebuchet/DragView;[F)[F
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p6    # [F

    if-nez p6, :cond_0

    const/4 v3, 0x2

    new-array v1, v3, [F

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0036    # com.konka.avenger.R.dimen.dragViewOffsetX

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr p1, v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0037    # com.konka.avenger.R.dimen.dragViewOffsetY

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr p2, v3

    sub-int v0, p1, p3

    sub-int v2, p2, p4

    const/4 v3, 0x0

    invoke-virtual {p5}, Lcom/cyanogenmod/trebuchet/DragView;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v0

    int-to-float v4, v4

    aput v4, v1, v3

    const/4 v3, 0x1

    invoke-virtual {p5}, Lcom/cyanogenmod/trebuchet/DragView;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v2

    int-to-float v4, v4

    aput v4, v1, v3

    return-object v1

    :cond_0
    move-object v1, p6

    goto :goto_0
.end method

.method private getScrollRange()I
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildOffset(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildOffset(I)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private hitsPage(IFF)Z
    .locals 7
    .param p1    # I
    .param p2    # F
    .param p3    # F

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v4, 0x2

    new-array v0, v4, [F

    aput p2, v0, v3

    aput p3, v0, v2

    invoke-virtual {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[F)V

    aget v4, v0, v3

    cmpl-float v4, v4, v6

    if-ltz v4, :cond_0

    aget v4, v0, v3

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    aget v4, v0, v2

    cmpl-float v4, v4, v6

    if-ltz v4, :cond_0

    aget v4, v0, v2

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method private initAnimationArrays()V
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationXs:[F

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationXs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationYs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleXs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleYs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphas:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphaMultipliers:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldAlphas:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldRotations:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldRotationYs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationXs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationYs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleXs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleYs:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphas:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphaMultipliers:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewAlphas:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewRotations:[F

    new-array v1, v0, [F

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewRotationYs:[F

    goto :goto_0
.end method

.method private initQuickActionMenu()V
    .locals 13

    const/4 v12, 0x2

    const v11, 0x7f0a00a4    # com.konka.avenger.R.string.quick_action_delete

    const v10, 0x7f0a00a3    # com.konka.avenger.R.string.quick_action_move

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v4, Lnet/londatiga/android/ActionItem;

    const v6, 0x7f0a009f    # com.konka.avenger.R.string.quick_action_run

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v8, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v1, Lnet/londatiga/android/ActionItem;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v9, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v5, Lnet/londatiga/android/ActionItem;

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v12, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v4}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v1}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v5}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, p0}, Lnet/londatiga/android/QuickAction;->setOnActionItemClickListener(Lnet/londatiga/android/QuickAction$OnActionItemClickListener;)V

    :goto_0
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v1, Lnet/londatiga/android/ActionItem;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v8, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v5, Lnet/londatiga/android/ActionItem;

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v9, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v1}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v5}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, p0}, Lnet/londatiga/android/QuickAction;->setOnActionItemClickListener(Lnet/londatiga/android/QuickAction$OnActionItemClickListener;)V

    :goto_1
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v4, Lnet/londatiga/android/ActionItem;

    const v6, 0x7f0a009f    # com.konka.avenger.R.string.quick_action_run

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v8, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v3, Lnet/londatiga/android/ActionItem;

    const v6, 0x7f0a00a6    # com.konka.avenger.R.string.quick_action_rename

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v9, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v1, Lnet/londatiga/android/ActionItem;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v12, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v5, Lnet/londatiga/android/ActionItem;

    const/4 v6, 0x3

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v4}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v3}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v1}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, v5}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v6, p0}, Lnet/londatiga/android/QuickAction;->setOnActionItemClickListener(Lnet/londatiga/android/QuickAction$OnActionItemClickListener;)V

    :goto_2
    return-void

    :cond_0
    const-string v6, "Launcher.Workspace"

    const-string v7, "the mIconQuickAction has not been initialized!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    const-string v6, "Launcher.Workspace"

    const-string v7, "the mWidgetQuickAction has not been initialized!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v6, "Launcher.Workspace"

    const-string v7, "the mFolderQuickAction has not been initialized!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private isDragWidget(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isExternalDragWidget(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    if-eq v0, p0, :cond_0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->isDragWidget(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onDropExternal([ILjava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;Z)V
    .locals 6
    .param p1    # [I
    .param p2    # Ljava/lang/Object;
    .param p3    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p4    # Z

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/Workspace;->onDropExternal([ILjava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;ZLcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    return-void
.end method

.method private onDropExternal([ILjava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;ZLcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 35
    .param p1    # [I
    .param p2    # Ljava/lang/Object;
    .param p3    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p4    # Z
    .param p5    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    new-instance v28, Lcom/cyanogenmod/trebuchet/Workspace$11;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace$11;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    move-object/from16 v30, p2

    check-cast v30, Lcom/cyanogenmod/trebuchet/ItemInfo;

    move-object/from16 v0, v30

    iget v5, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    move-object/from16 v0, v30

    iget v6, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v5, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v6, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, -0x65

    :goto_0
    int-to-long v10, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    if-eq v12, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    sget-object v3, Lcom/cyanogenmod/trebuchet/Workspace$State;->SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Workspace$State;

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    :cond_1
    move-object/from16 v0, v30

    instance-of v2, v0, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    if-eqz v2, :cond_a

    move-object/from16 v15, p2

    check-cast v15, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    if-eqz p1, :cond_7

    const/16 v29, 0x1

    :goto_1
    if-eqz v29, :cond_8

    iget v2, v15, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->itemType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    const/4 v2, 0x0

    aget v3, p1, v2

    const/4 v2, 0x1

    aget v4, p1, v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v2, p0

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestArea(IIIILcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    if-eqz p5, :cond_3

    move-object/from16 v0, p5

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v2, Lcom/cyanogenmod/trebuchet/ItemInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->willCreateUserFolder(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;[IZ)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p5

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v2, Lcom/cyanogenmod/trebuchet/ItemInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->willAddToExistingUserFolder(Ljava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;[I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/16 v29, 0x0

    :cond_3
    if-eqz v29, :cond_4

    const/4 v2, 0x0

    aget v3, p1, v2

    const/4 v2, 0x1

    aget v4, p1, v2

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v2, p0

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestVacantArea(IIIILandroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    :cond_4
    :goto_2
    new-instance v7, Lcom/cyanogenmod/trebuchet/Workspace$12;

    move-object/from16 v8, p0

    move-object v9, v15

    move-object/from16 v13, p3

    invoke-direct/range {v7 .. v13}, Lcom/cyanogenmod/trebuchet/Workspace$12;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;JILcom/cyanogenmod/trebuchet/CellLayout;)V

    if-eqz p5, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v3, 0x0

    aget v16, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v3, 0x1

    aget v17, v2, v3

    move-object/from16 v13, p0

    move-object/from16 v14, p3

    move/from16 v18, v5

    move/from16 v19, v6

    invoke-virtual/range {v13 .. v19}, Lcom/cyanogenmod/trebuchet/Workspace;->estimateItemPosition(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/ItemInfo;IIII)Landroid/graphics/RectF;

    move-result-object v33

    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v31, v0

    const/4 v2, 0x0

    move-object/from16 v0, v33

    iget v3, v0, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    aput v3, v31, v2

    const/4 v2, 0x1

    move-object/from16 v0, v33

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    aput v3, v31, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setFinalTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    move-object/from16 v0, p3

    move-object/from16 v1, v31

    invoke-virtual {v2, v0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantCoordRelativeToSelf(Landroid/view/View;[I)F

    move-result v26

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->resetTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    invoke-virtual/range {v33 .. v33}, Landroid/graphics/RectF;->width()F

    move-result v2

    move-object/from16 v0, p5

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual/range {v33 .. v33}, Landroid/graphics/RectF;->height()F

    move-result v3

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v27

    const/4 v2, 0x0

    aget v3, v31, v2

    int-to-float v3, v3

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {v33 .. v33}, Landroid/graphics/RectF;->width()F

    move-result v8

    mul-float v8, v8, v26

    sub-float/2addr v4, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v4, v8

    sub-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v31, v2

    const/4 v2, 0x1

    aget v3, v31, v2

    int-to-float v3, v3

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {v33 .. v33}, Landroid/graphics/RectF;->height()F

    move-result v8

    mul-float v8, v8, v26

    sub-float/2addr v4, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v4, v8

    sub-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v31, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    move-object/from16 v0, p5

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    mul-float v4, v27, v26

    move-object/from16 v0, v31

    invoke-virtual {v2, v3, v0, v4, v7}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;[IFLjava/lang/Runnable;)V

    move-object/from16 v9, v30

    :cond_5
    :goto_3
    return-void

    :cond_6
    const/16 v2, -0x64

    goto/16 :goto_0

    :cond_7
    const/16 v29, 0x0

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v5, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    goto/16 :goto_2

    :cond_9
    const-string v2, "Launcher.Workspace"

    const-string v3, "directly call onAnimationCompleteRunnable"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v9, v30

    goto :goto_3

    :cond_a
    move-object/from16 v0, v30

    iget v2, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    packed-switch v2, :pswitch_data_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown item type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    iget v4, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_0
    move-object/from16 v0, v30

    iget-wide v2, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v13, -0x1

    cmp-long v2, v2, v13

    if-nez v2, :cond_e

    move-object/from16 v0, v30

    instance-of v2, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    if-eqz v2, :cond_e

    new-instance v9, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    check-cast v30, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    move-object/from16 v0, v30

    invoke-direct {v9, v0}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;-><init>(Lcom/cyanogenmod/trebuchet/ApplicationInfo;)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const v4, 0x7f030004    # com.konka.avenger.R.layout.application

    move-object v2, v9

    check-cast v2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->createShortcut(ILandroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;

    move-result-object v34

    :goto_5
    if-eqz p1, :cond_b

    const/4 v2, 0x0

    aget v17, p1, v2

    const/4 v2, 0x1

    aget v18, p1, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v22, v0

    move-object/from16 v16, p0

    move/from16 v19, v5

    move/from16 v20, v6

    move-object/from16 v21, p3

    invoke-direct/range {v16 .. v22}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestArea(IIIILcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v0, v28

    move-object/from16 v1, p5

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->postAnimationRunnable:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p5

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v23, v0

    move-object/from16 v0, p5

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->postAnimationRunnable:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    move-object/from16 v16, p0

    move-object/from16 v17, v34

    move-wide/from16 v18, v10

    move-object/from16 v20, p3

    invoke-virtual/range {v16 .. v24}, Lcom/cyanogenmod/trebuchet/Workspace;->createUserFolderIfNecessary(Landroid/view/View;JLcom/cyanogenmod/trebuchet/CellLayout;[IZLcom/cyanogenmod/trebuchet/DragView;Ljava/lang/Runnable;)Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v19, v0

    const/16 v21, 0x1

    move-object/from16 v16, p0

    move-object/from16 v17, v34

    move-object/from16 v18, p3

    move-object/from16 v20, p5

    invoke-virtual/range {v16 .. v21}, Lcom/cyanogenmod/trebuchet/Workspace;->addToExistingFolderIfNecessary(Landroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[ILcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_b
    if-eqz p1, :cond_c

    const/4 v2, 0x0

    aget v17, p1, v2

    const/4 v2, 0x1

    aget v18, p1, v2

    const/16 v19, 0x1

    const/16 v20, 0x1

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v23, v0

    move-object/from16 v16, p0

    move-object/from16 v22, p3

    invoke-direct/range {v16 .. v23}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestVacantArea(IIIILandroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v3, 0x0

    aget v21, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v3, 0x1

    aget v22, v2, v3

    iget v0, v9, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    move/from16 v23, v0

    iget v0, v9, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    move/from16 v24, v0

    move-object/from16 v16, p0

    move-object/from16 v17, v34

    move-wide/from16 v18, v10

    move/from16 v20, v12

    move/from16 v25, p4

    invoke-virtual/range {v16 .. v25}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    move-object/from16 v0, p3

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDropChild(Landroid/view/View;)V

    invoke-virtual/range {v34 .. v34}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v32

    check-cast v32, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual/range {p3 .. p3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->measureChild(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v0, v32

    iget v13, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move-object/from16 v0, v32

    iget v14, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    invoke-static/range {v8 .. v14}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    if-eqz p5, :cond_5

    move-object/from16 v0, p5

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setFinalTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    move-object/from16 v0, p5

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v0, v34

    move-object/from16 v1, v28

    invoke-virtual {v2, v3, v0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;Ljava/lang/Runnable;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->resetTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    goto/16 :goto_3

    :pswitch_1
    const v3, 0x7f030010    # com.konka.avenger.R.layout.folder_icon

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v2, v30

    check-cast v2, Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    move-object/from16 v0, p3

    invoke-static {v3, v4, v0, v2, v8}, Lcom/cyanogenmod/trebuchet/FolderIcon;->fromXml(ILcom/cyanogenmod/trebuchet/Launcher;Landroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/FolderInfo;Lcom/cyanogenmod/trebuchet/IconCache;)Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-result-object v34

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mHideIconLabels:Z

    if-eqz v2, :cond_d

    move-object/from16 v2, v34

    check-cast v2, Lcom/cyanogenmod/trebuchet/FolderIcon;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTextVisible(Z)V

    move-object/from16 v9, v30

    goto/16 :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    goto/16 :goto_6

    :cond_d
    move-object/from16 v9, v30

    goto/16 :goto_5

    :cond_e
    move-object/from16 v9, v30

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onResetScrollArea()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    :cond_0
    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    return-void
.end method

.method private screenScrolledCube(IZ)V
    .locals 7
    .param p1    # I
    .param p2    # Z

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v5

    if-lt v2, v5, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    if-eqz p2, :cond_3

    const/high16 v5, 0x42b40000    # 90.0f

    :goto_1
    mul-float v3, v5, v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v0, v5, v6

    if-eqz p2, :cond_1

    iget v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDensity:F

    sget v6, Lcom/cyanogenmod/trebuchet/Workspace;->CAMERA_DISTANCE:F

    mul-float/2addr v5, v6

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setCameraDistance(F)V

    :cond_1
    const/4 v5, 0x0

    cmpg-float v5, v4, v5

    if-gez v5, :cond_4

    const/4 v5, 0x0

    :goto_2
    int-to-float v5, v5

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotX(F)V

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotY(F)V

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const/high16 v5, -0x3d4c0000    # -90.0f

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v5

    goto :goto_2
.end method

.method private screenScrolledRotate(IZ)V
    .locals 11
    .param p1    # I
    .param p2    # Z

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v7

    if-lt v2, v7, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v5

    if-eqz p2, :cond_3

    const/high16 v7, 0x41480000    # 12.5f

    :goto_1
    mul-float v4, v7, v5

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float v6, v7, v5

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v10

    const-wide/high16 v8, 0x4019000000000000L    # 6.25

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    double-to-float v8, v8

    div-float v3, v7, v8

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v10

    invoke-virtual {v1, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotX(F)V

    if-eqz p2, :cond_4

    neg-float v7, v3

    invoke-virtual {v1, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotY(F)V

    :goto_2
    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotation(F)V

    invoke-virtual {v1, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    iget-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeInAdjacentScreens:Z

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v7

    if-nez v7, :cond_1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v8

    sub-float v0, v7, v8

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    :cond_1
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const/high16 v7, -0x3eb80000    # -12.5f

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v7, v3

    invoke-virtual {v1, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotY(F)V

    goto :goto_2
.end method

.method private screenScrolledStack(I)V
    .locals 11
    .param p1    # I

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v8

    if-lt v2, v8, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v5

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mZInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;

    invoke-static {v5, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    invoke-virtual {v8, v9}, Lcom/cyanogenmod/trebuchet/Workspace$ZInterpolator;->getInterpolation(F)F

    move-result v3

    sub-float v8, v7, v3

    const v9, 0x3f428f5c    # 0.76f

    mul-float/2addr v9, v3

    add-float v4, v8, v9

    invoke-static {v10, v5}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v9

    int-to-float v9, v9

    mul-float v6, v8, v9

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v8

    if-eqz v8, :cond_1

    cmpg-float v8, v5, v10

    if-gez v8, :cond_5

    :cond_1
    cmpg-float v8, v5, v10

    if-gez v8, :cond_4

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAlphaInterpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float v9, v7, v9

    invoke-virtual {v8, v9}, Landroid/view/animation/AccelerateInterpolator;->getInterpolation(F)F

    move-result v0

    :goto_1
    invoke-virtual {v1, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleX(F)V

    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleY(F)V

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    cmpg-float v8, v0, v10

    if-gtz v8, :cond_6

    const/4 v8, 0x4

    invoke-virtual {v1, v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->setVisibility(I)V

    :cond_2
    :goto_2
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    move v0, v7

    goto :goto_1

    :cond_5
    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLeftScreenAlphaInterpolator:Landroid/view/animation/DecelerateInterpolator;

    sub-float v9, v7, v5

    invoke-virtual {v8, v9}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method private screenScrolledStandard(I)V
    .locals 6
    .param p1    # I

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v4

    if-lt v2, v4, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v3

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeInAdjacentScreens:Z

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v4

    if-nez v4, :cond_1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    sub-float v0, v4, v5

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private screenScrolledTablet(I)V
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v6

    if-lt v2, v6, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    const/high16 v6, 0x41480000    # 12.5f

    mul-float v3, v6, v4

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getWidth()I

    move-result v6

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getHeight()I

    move-result v7

    invoke-virtual {p0, v3, v6, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->getOffsetXForRotation(FII)F

    move-result v5

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeInAdjacentScreens:Z

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v6

    if-nez v6, :cond_1

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v7

    sub-float v0, v6, v7

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    :cond_1
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private screenScrolledZoom(IZ)V
    .locals 10
    .param p1    # I
    .param p2    # Z

    const/high16 v9, 0x3f800000    # 1.0f

    const v7, 0x3dcccccd    # 0.1f

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v6

    if-lt v2, v6, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    if-eqz p2, :cond_4

    const v6, -0x41b33333    # -0.2f

    :goto_1
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v8

    mul-float/2addr v6, v8

    add-float v3, v9, v6

    if-nez p2, :cond_1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v7

    neg-float v8, v4

    mul-float v5, v6, v8

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    :cond_1
    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleX(F)V

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleY(F)V

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeInAdjacentScreens:Z

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v0, v9, v6

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    :cond_2
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    move v6, v7

    goto :goto_1
.end method

.method private static squaredDistance([F[F)F
    .locals 5
    .param p0    # [F
    .param p1    # [F

    const/4 v4, 0x1

    const/4 v3, 0x0

    aget v2, p0, v3

    aget v3, p1, v3

    sub-float v0, v2, v3

    aget v2, p1, v4

    aget v3, p1, v4

    sub-float v1, v2, v3

    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    return v2
.end method

.method private syncChildrenLayersEnabledOnVisiblePages()V
    .locals 7

    const/4 v6, -0x1

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenLayersEnabled:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempVisiblePagesRange:[I

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->getVisiblePages([I)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempVisiblePagesRange:[I

    const/4 v5, 0x0

    aget v1, v4, v5

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempVisiblePagesRange:[I

    const/4 v5, 0x1

    aget v3, v4, v5

    if-eq v1, v6, :cond_0

    if-eq v3, v6, :cond_0

    move v0, v1

    :goto_0
    if-le v0, v3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getAlpha()F

    move-result v4

    const v5, 0x3caaaaab

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->enableHardwareLayers()V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private syncWallpaperOffsetWithScroll()V
    .locals 2

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->wallpaperOffsetForCurrentScroll()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->setFinalX(F)V

    :cond_0
    return-void
.end method

.method private updateChildrenLayersEnabled(Z)V
    .locals 7
    .param p1    # Z

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v6

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v6, :cond_1

    move v3, v4

    :goto_0
    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimatingViewIntoPlace:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsDragOccuring:Z

    if-nez v6, :cond_2

    move v0, v4

    :goto_1
    if-nez v3, :cond_3

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isPageMoving()Z

    move-result v6

    if-nez v6, :cond_3

    move v1, v4

    :goto_2
    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenLayersEnabled:Z

    if-eq v1, v4, :cond_0

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenLayersEnabled:Z

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageCount()I

    move-result v4

    if-lt v2, v4, :cond_4

    :cond_0
    return-void

    :cond_1
    move v3, v5

    goto :goto_0

    :cond_2
    move v0, v5

    goto :goto_1

    :cond_3
    move v1, v5

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->disableHardwareLayers()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private updateWallpaperOffsets()V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mUpdateWallpaperOffsetImmediately:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->jumpToFinal()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mUpdateWallpaperOffsetImmediately:Z

    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWindowToken:Landroid/os/IBinder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperManager:Landroid/app/WallpaperManager;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWindowToken:Landroid/os/IBinder;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->getCurrX()F

    move-result v4

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->getCurrY()F

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->computeScrollOffset()Z

    move-result v0

    move v1, v0

    goto :goto_0
.end method

.method private validateDrag(Landroid/view/DragEvent;)Landroid/util/Pair;
    .locals 9
    .param p1    # Landroid/view/DragEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/DragEvent;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;",
            ">;>;"
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Launcher;->getModel()Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    move-object v6, v7

    :goto_1
    return-object v6

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v2

    const-string v6, "com.android.launcher/shortcut"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    new-instance v6, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v6, v8, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->resolveWidgetsForMimeType(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    new-instance v6, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private wallpaperOffsetForCurrentScroll()F
    .locals 10

    const/high16 v9, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperWidth:I

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperTravelWidth:I

    :cond_0
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    int-to-float v8, v8

    div-float v8, v9, v8

    invoke-virtual {v7, v8, v9}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLayoutScale:F

    iput v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLayoutScale:F

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollRange()I

    move-result v5

    const/4 v7, 0x0

    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMaxScrollX:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    int-to-float v0, v7

    iget v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperScrollRatio:F

    mul-float/2addr v0, v7

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLayoutScale:F

    int-to-float v7, v5

    div-float v4, v0, v7

    int-to-float v7, v6

    mul-float/2addr v7, v4

    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperWidth:I

    sub-int/2addr v8, v6

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float v3, v7, v8

    iget v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperWidth:I

    int-to-float v7, v7

    div-float v2, v3, v7

    return v2
.end method

.method private wallpaperTravelToScreenWidthRatio(II)F
    .locals 9
    .param p1    # I
    .param p2    # I

    int-to-float v7, p1

    int-to-float v8, p2

    div-float v4, v7, v8

    const v0, 0x3fcccccd    # 1.6f

    const/high16 v1, 0x3f200000    # 0.625f

    const/high16 v2, 0x3fc00000    # 1.5f

    const v3, 0x3f99999a    # 1.2f

    const v5, 0x3e9d89d7

    const v6, 0x3f80fc10

    const v7, 0x3e9d89d7

    mul-float/2addr v7, v4

    const v8, 0x3f80fc10

    add-float/2addr v7, v8

    return v7
.end method


# virtual methods
.method public acceptDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z
    .locals 14
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    if-eq v0, p0, :cond_9

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-nez v0, :cond_0

    move v0, v11

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->transitionStateShouldAllowDrop()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v11

    goto :goto_0

    :cond_1
    iget v1, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    iget v2, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    iget v3, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->xOffset:I

    iget v4, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->yOffset:I

    iget-object v5, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Workspace;->getDragViewVisualCenter(IIIILcom/cyanogenmod/trebuchet/DragView;[F)[F

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToSibling(Landroid/view/View;[F)V

    :goto_1
    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v0, :cond_3

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v3, v7, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    iget v4, v7, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    iget-object v10, v7, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    :goto_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    aget v0, v0, v11

    float-to-int v1, v0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    aget v0, v0, v12

    float-to-int v2, v0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestArea(IIIILcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->checkDropPermission()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v11

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    invoke-virtual {p0, v0, v1, v13}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    goto :goto_1

    :cond_3
    iget-object v8, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v8, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v3, v8, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    iget v4, v8, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    goto :goto_2

    :cond_4
    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v0, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    invoke-virtual {p0, v0, v1, v2, v12}, Lcom/cyanogenmod/trebuchet/Workspace;->willCreateUserFolder(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;[IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v12

    goto :goto_0

    :cond_5
    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v0, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->willAddToExistingUserFolder(Ljava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;[I)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v12

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isDefaultHomeScreen(Lcom/cyanogenmod/trebuchet/CellLayout;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showNotAllowedAddMessage()V

    move v0, v11

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, v13, v3, v4, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpanIgnoring([IIILandroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v9

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    aget v0, v0, v11

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    aget v1, v1, v12

    invoke-virtual {v9, v0, v1}, Lcom/cyanogenmod/trebuchet/Hotseat;->getOrderInHotseat(II)I

    move-result v0

    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Hotseat;->isAllAppsButtonRank(I)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v11

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    move v0, v11

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->checkDropPermission()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v11

    goto/16 :goto_0

    :cond_a
    move v0, v12

    goto/16 :goto_0
.end method

.method addApplicationShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Lcom/cyanogenmod/trebuchet/CellLayout;JIIIZII)V
    .locals 13
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Z
    .param p9    # I
    .param p10    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const v2, 0x7f030004    # com.konka.avenger.R.layout.application

    invoke-virtual {v0, v2, p2, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->createShortcut(ILandroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;

    move-result-object v12

    const/4 v0, 0x2

    new-array v1, v0, [I

    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object v0, p2

    move/from16 v4, p9

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpanThatIntersects([IIIII)Z

    const/4 v0, 0x0

    aget v7, v1, v0

    const/4 v0, 0x1

    aget v8, v1, v0

    const/4 v9, 0x1

    const/4 v10, 0x1

    move-object v2, p0

    move-object v3, v12

    move-wide/from16 v4, p3

    move/from16 v6, p5

    move/from16 v11, p8

    invoke-virtual/range {v2 .. v11}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v0, 0x0

    aget v7, v1, v0

    const/4 v0, 0x1

    aget v8, v1, v0

    move-object v3, p1

    move-wide/from16 v4, p3

    move/from16 v6, p5

    invoke-static/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    return-void
.end method

.method public addExternalItemToScreen(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;)Z
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v0, 0x0

    invoke-virtual {p0, p2}, Lcom/cyanogenmod/trebuchet/Workspace;->isDefaultHomeScreen(Lcom/cyanogenmod/trebuchet/CellLayout;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->showNotAllowedAddMessage()V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempEstimate:[I

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    iget v3, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    invoke-virtual {p2, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->dropPos:[I

    invoke-direct {p0, v1, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->onDropExternal([ILjava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;Z)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    goto :goto_0
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 2
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->isAllAppsVisible()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Lcom/cyanogenmod/trebuchet/Folder;->addFocusables(Ljava/util/ArrayList;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/cyanogenmod/trebuchet/PagedView;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0
.end method

.method addInScreen(Landroid/view/View;JIIIII)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v9}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    return-void
.end method

.method addInScreen(Landroid/view/View;JIIIIIZ)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Z

    const-wide/16 v1, -0x64

    cmp-long v1, p2, v1

    if-nez v1, :cond_2

    if-ltz p4, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v1

    if-lt p4, v1, :cond_2

    :cond_0
    const-string v1, "Launcher.Workspace"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "The screen must be >= 0 and < "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " (was "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "); skipping child"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-wide/16 v1, -0x65

    cmp-long v1, p2, v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mHideIconLabels:Z

    if-nez v1, :cond_3

    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v1, :cond_7

    move-object v1, p1

    check-cast v1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTextVisible(Z)V

    :cond_3
    :goto_1
    if-gez p4, :cond_8

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Lcom/cyanogenmod/trebuchet/Hotseat;->getOrderInHotseat(II)I

    move-result p4

    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    if-nez v4, :cond_c

    new-instance v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-direct {v4, p5, p6, p7, p8}, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;-><init>(IIII)V

    :goto_3
    if-gez p7, :cond_4

    if-gez p8, :cond_4

    const/4 v1, 0x0

    iput-boolean v1, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->isLockedToGrid:Z

    :cond_4
    invoke-static/range {p2 .. p8}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellLayoutChildId(JIIIII)I

    move-result v3

    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/Folder;

    if-eqz v1, :cond_d

    const/4 v5, 0x0

    :goto_4
    if-eqz p9, :cond_e

    const/4 v2, 0x0

    :goto_5
    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->addViewToCellLayout(Landroid/view/View;IILcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;Z)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "Launcher.Workspace"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Failed to add to item at ("

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ") to CellLayout"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/Folder;

    if-nez v1, :cond_6

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_6
    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    check-cast p1, Lcom/cyanogenmod/trebuchet/DropTarget;

    invoke-virtual {v1, p1}, Lcom/cyanogenmod/trebuchet/DragController;->addDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    goto/16 :goto_0

    :cond_7
    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v1, :cond_3

    move-object v1, p1

    check-cast v1, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setTextVisible(Z)V

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/cyanogenmod/trebuchet/Hotseat;->getCellXFromOrder(I)I

    move-result p5

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/cyanogenmod/trebuchet/Hotseat;->getCellYFromOrder(I)I

    move-result p6

    goto/16 :goto_2

    :cond_9
    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mHideIconLabels:Z

    if-nez v1, :cond_a

    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v1, :cond_b

    move-object v1, p1

    check-cast v1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTextVisible(Z)V

    :cond_a
    :goto_6
    invoke-virtual {p0, p4}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    new-instance v1, Lcom/cyanogenmod/trebuchet/IconKeyEventListener;

    invoke-direct {v1}, Lcom/cyanogenmod/trebuchet/IconKeyEventListener;-><init>()V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto/16 :goto_2

    :cond_b
    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v1, :cond_a

    move-object v1, p1

    check-cast v1, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setTextVisible(Z)V

    goto :goto_6

    :cond_c
    iput p5, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iput p6, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iput p7, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    iput p8, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    goto/16 :goto_3

    :cond_d
    const/4 v5, 0x1

    goto/16 :goto_4

    :cond_e
    const/4 v2, -0x1

    goto/16 :goto_5
.end method

.method addToExistingFolderIfNecessary(Landroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[ILcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p3    # [I
    .param p4    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;
    .param p5    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    aget v4, p3, v3

    aget v5, p3, v2

    invoke-virtual {p2, v4, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v0

    instance-of v4, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v4, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    iget-object v4, p4, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/FolderIcon;->acceptDrop(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, p4}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    if-nez p5, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v4, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->requestFocus()Z

    :goto_0
    return v2

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method public allowLongPress()Z
    .locals 1

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->allowLongPress()Z

    move-result v0

    return v0
.end method

.method backgroundAlphaInterpolator(F)F
    .locals 4
    .param p1    # F

    const v0, 0x3dcccccd    # 0.1f

    const v1, 0x3ecccccd    # 0.4f

    cmpg-float v2, p1, v0

    if-gez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    cmpl-float v2, p1, v1

    if-lez v2, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_1
    sub-float v2, p1, v0

    sub-float v3, v1, v0

    div-float/2addr v2, v3

    goto :goto_0
.end method

.method public beginDragShared(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;)V
    .locals 24
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/cyanogenmod/trebuchet/DragSource;

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    sget v13, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3}, Landroid/graphics/Canvas;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3, v13}, Lcom/cyanogenmod/trebuchet/Workspace;->createDragBitmap(Landroid/view/View;Landroid/graphics/Canvas;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempXY:[I

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v7}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocationInDragLayer(Landroid/view/View;[I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempXY:[I

    const/4 v7, 0x0

    aget v3, v3, v7

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v7

    sub-int/2addr v7, v14

    div-int/lit8 v7, v7, 0x2

    add-int v5, v3, v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempXY:[I

    const/4 v7, 0x1

    aget v3, v3, v7

    div-int/lit8 v7, v13, 0x2

    sub-int v6, v3, v7

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;

    if-eqz v3, :cond_3

    :cond_0
    const v3, 0x7f0c0022    # com.konka.avenger.R.dimen.app_icon_size

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    const v3, 0x7f0c001a    # com.konka.avenger.R.dimen.app_icon_padding_top

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getPaddingTop()I

    move-result v23

    sub-int v3, v14, v18

    div-int/lit8 v19, v3, 0x2

    add-int v22, v19, v18

    add-int v15, v23, v18

    add-int v6, v6, v23

    new-instance v10, Landroid/graphics/Point;

    neg-int v3, v13

    div-int/lit8 v3, v3, 0x2

    div-int/lit8 v7, v13, 0x2

    sub-int v7, v17, v7

    invoke-direct {v10, v3, v7}, Landroid/graphics/Point;-><init>(II)V

    new-instance v11, Landroid/graphics/Rect;

    move/from16 v0, v19

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-direct {v11, v0, v1, v2, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    :cond_1
    :goto_0
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v3, :cond_2

    move-object/from16 v16, p1

    check-cast v16, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-virtual/range {v16 .. v16}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->clearPressedOrFocusedBackground()V

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    sget v9, Lcom/cyanogenmod/trebuchet/DragController;->DRAG_ACTION_MOVE:I

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v12

    move-object/from16 v7, p2

    invoke-virtual/range {v3 .. v12}, Lcom/cyanogenmod/trebuchet/DragController;->startDrag(Landroid/graphics/Bitmap;IILcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;Z)V

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    return-void

    :cond_3
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v3, :cond_1

    const v3, 0x7f0c003e    # com.konka.avenger.R.dimen.folder_preview_size

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    new-instance v11, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v8

    move/from16 v0, v20

    invoke-direct {v11, v3, v7, v8, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public buildPageHardwareLayers()V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v0, :cond_1

    :cond_0
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    return-void

    :cond_1
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->buildChildrenLayer()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/Workspace$State;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;Z)V

    return-void
.end method

.method changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;Z)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/Workspace$State;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;ZI)V

    return-void
.end method

.method changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;ZI)V
    .locals 28
    .param p1    # Lcom/cyanogenmod/trebuchet/Workspace$State;
    .param p2    # Z
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mFirstLayout:Z

    move/from16 v25, v0

    if-eqz v25, :cond_1

    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/cyanogenmod/trebuchet/Workspace;->mSwitchStateAfterFirstLayout:Z

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/Workspace;->mStateAfterFirstLayout:Lcom/cyanogenmod/trebuchet/Workspace$State;

    goto :goto_0

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->initAnimationArrays()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v25, v0

    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_2
    new-instance v25, Landroid/animation/AnimatorSet;

    invoke-direct/range {v25 .. v25}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    move/from16 v25, v0

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    move/from16 v25, v0

    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setCurrentPage(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    sget-object v25, Lcom/cyanogenmod/trebuchet/Workspace$State;->NORMAL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-object/from16 v0, v25

    if-ne v13, v0, :cond_7

    const/4 v14, 0x1

    :goto_2
    sget-object v25, Lcom/cyanogenmod/trebuchet/Workspace$State;->SMALL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-object/from16 v0, v25

    if-ne v13, v0, :cond_8

    const/4 v15, 0x1

    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    sget-object v25, Lcom/cyanogenmod/trebuchet/Workspace$State;->NORMAL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_9

    const/16 v19, 0x1

    :goto_4
    sget-object v25, Lcom/cyanogenmod/trebuchet/Workspace$State;->SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_a

    const/16 v21, 0x1

    :goto_5
    sget-object v25, Lcom/cyanogenmod/trebuchet/Workspace$State;->SMALL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_b

    const/16 v20, 0x1

    :goto_6
    const/high16 v10, 0x3f800000    # 1.0f

    if-eqz v21, :cond_c

    const/high16 v9, 0x3f800000    # 1.0f

    :goto_7
    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x1

    sget-object v25, Lcom/cyanogenmod/trebuchet/Workspace$State;->NORMAL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedShrinkFactor:F

    move/from16 v26, v0

    if-eqz v20, :cond_d

    const v25, 0x3dcccccd    # 0.1f

    :goto_8
    sub-float v10, v26, v25

    if-eqz v14, :cond_e

    if-eqz v20, :cond_e

    const/16 v24, 0x0

    if-eqz p2, :cond_3

    const/16 v25, 0x0

    const/16 v26, 0x96

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->hideScrollingIndicator(ZI)V

    :cond_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/cyanogenmod/trebuchet/Workspace;->setLayoutScale(F)V

    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    :goto_9
    if-eqz v24, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0004    # com.konka.avenger.R.integer.config_workspaceUnshrinkTime

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    :goto_a
    const/4 v11, 0x0

    :goto_b
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v25

    move/from16 v0, v25

    if-lt v11, v0, :cond_11

    if-eqz p2, :cond_5

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [F

    move-object/from16 v25, v0

    fill-array-data v25, :array_0

    invoke-static/range {v25 .. v25}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v25

    int-to-long v0, v6

    move-wide/from16 v26, v0

    invoke-virtual/range {v25 .. v27}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v4

    if-eqz v24, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mZoomInInterpolator:Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_4
    new-instance v25, Lcom/cyanogenmod/trebuchet/Workspace$5;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move/from16 v2, v19

    invoke-direct {v0, v1, v2, v15}, Lcom/cyanogenmod/trebuchet/Workspace$5;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;ZZ)V

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v25, Lcom/cyanogenmod/trebuchet/Workspace$6;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace$6;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [F

    move-object/from16 v25, v0

    fill-array-data v25, :array_1

    invoke-static/range {v25 .. v25}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v25

    int-to-long v0, v6

    move-wide/from16 v26, v0

    invoke-virtual/range {v25 .. v27}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v17

    new-instance v25, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v26, 0x40000000    # 2.0f

    invoke-direct/range {v25 .. v26}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v25, Lcom/cyanogenmod/trebuchet/Workspace$7;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace$7;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v4, v26, v27

    const/16 v27, 0x1

    aput-object v17, v26, v27

    invoke-virtual/range {v25 .. v26}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v25, v0

    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v26, v0

    invoke-virtual/range {v25 .. v27}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mChangeStateAnimationListener:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/animation/AnimatorSet;->start()V

    :cond_5
    if-eqz v21, :cond_2b

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const/high16 v26, 0x7f0b0000    # com.konka.avenger.R.integer.config_appsCustomizeSpringLoadedBgAlpha

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x42c80000    # 100.0f

    div-float v25, v25, v26

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->animateBackgroundGradient(FZ)V

    :goto_c
    invoke-direct/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->syncChildrenLayersEnabledOnVisiblePages()V

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v25, v0

    goto/16 :goto_1

    :cond_7
    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_8
    const/4 v15, 0x0

    goto/16 :goto_3

    :cond_9
    const/16 v19, 0x0

    goto/16 :goto_4

    :cond_a
    const/16 v21, 0x0

    goto/16 :goto_5

    :cond_b
    const/16 v20, 0x0

    goto/16 :goto_6

    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_7

    :cond_d
    const/16 v25, 0x0

    goto/16 :goto_8

    :cond_e
    const/high16 v9, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/cyanogenmod/trebuchet/Workspace;->setLayoutScale(F)V

    goto/16 :goto_9

    :cond_f
    const/high16 v25, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setLayoutScale(F)V

    goto/16 :goto_9

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f0b0010    # com.konka.avenger.R.integer.config_appsCustomizeWorkspaceShrinkTime

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    goto/16 :goto_a

    :cond_11
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    const/16 v16, 0x0

    const/16 v18, 0x0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getAlpha()F

    move-result v12

    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeInAdjacentScreens:Z

    move/from16 v25, v0

    if-eqz v25, :cond_12

    if-nez v21, :cond_12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-ne v11, v0, :cond_24

    :cond_12
    const/high16 v7, 0x3f800000    # 1.0f

    :goto_d
    if-eqz v15, :cond_13

    if-nez v19, :cond_14

    :cond_13
    if-eqz v14, :cond_16

    if-eqz v20, :cond_16

    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-eq v11, v0, :cond_15

    if-nez p2, :cond_25

    :cond_15
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    :cond_16
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->Tablet:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_17

    if-nez v20, :cond_17

    if-eqz v21, :cond_18

    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v11, v0, :cond_26

    const/high16 v18, 0x41480000    # 12.5f

    :cond_18
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->Stack:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_1a

    if-nez v20, :cond_19

    if-eqz v21, :cond_27

    :cond_19
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setVisibility(I)V

    :cond_1a
    :goto_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->Tablet:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_1b

    if-nez v19, :cond_1c

    :cond_1b
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v25

    if-eqz v25, :cond_1d

    if-nez v20, :cond_1c

    if-eqz v21, :cond_1d

    :cond_1c
    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getWidth()I

    move-result v25

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getHeight()I

    move-result v26

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getOffsetXForRotation(FII)F

    move-result v22

    :cond_1d
    if-eqz v19, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateUp:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateDown:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_1f

    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateUp:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_29

    const/high16 v25, 0x41480000    # 12.5f

    :goto_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v26, v0

    sub-int v26, v26, v11

    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(I)I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    mul-float v16, v25, v26

    :cond_1f
    if-nez v20, :cond_20

    if-eqz v21, :cond_23

    :cond_20
    const/high16 v25, 0x44a00000    # 1280.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDensity:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setCameraDistance(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateUp:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v25, v0

    sget-object v26, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateDown:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_22

    :cond_21
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    :cond_22
    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f000000    # 0.5f

    mul-float v25, v25, v26

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotX(F)V

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredHeight()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3f000000    # 0.5f

    mul-float v25, v25, v26

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotY(F)V

    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldAlphas:[F

    move-object/from16 v25, v0

    aput v12, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewAlphas:[F

    move-object/from16 v25, v0

    aput v7, v25, v11

    if-eqz p2, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationXs:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTranslationX()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationYs:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTranslationY()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleXs:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getScaleX()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleYs:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getScaleY()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphas:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getBackgroundAlpha()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphaMultipliers:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getBackgroundAlphaMultiplier()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldRotations:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getRotation()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mOldRotationYs:[F

    move-object/from16 v25, v0

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getRotationY()F

    move-result v26

    aput v26, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationXs:[F

    move-object/from16 v25, v0

    aput v22, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationYs:[F

    move-object/from16 v25, v0

    aput v23, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleXs:[F

    move-object/from16 v25, v0

    aput v10, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleYs:[F

    move-object/from16 v25, v0

    aput v10, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphas:[F

    move-object/from16 v25, v0

    aput v9, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphaMultipliers:[F

    move-object/from16 v25, v0

    aput v8, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewRotations:[F

    move-object/from16 v25, v0

    aput v16, v25, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewRotationYs:[F

    move-object/from16 v25, v0

    aput v18, v25, v11

    :goto_12
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_b

    :cond_24
    const/4 v7, 0x0

    goto/16 :goto_d

    :cond_25
    const/4 v12, 0x0

    const/4 v7, 0x0

    goto/16 :goto_e

    :cond_26
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-le v11, v0, :cond_18

    const/high16 v18, -0x3eb80000    # -12.5f

    goto/16 :goto_f

    :cond_27
    if-eqz v19, :cond_1a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-gt v11, v0, :cond_28

    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setVisibility(I)V

    goto/16 :goto_10

    :cond_28
    const/16 v25, 0x8

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setVisibility(I)V

    goto/16 :goto_10

    :cond_29
    const/high16 v25, -0x3eb80000    # -12.5f

    goto/16 :goto_11

    :cond_2a
    move/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationY(F)V

    invoke-virtual {v5, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleX(F)V

    invoke-virtual {v5, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleY(F)V

    invoke-virtual {v5, v9}, Lcom/cyanogenmod/trebuchet/CellLayout;->setBackgroundAlpha(F)V

    invoke-virtual {v5, v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->setBackgroundAlphaMultiplier(F)V

    invoke-virtual {v5, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotation(F)V

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mChangeStateAnimationListener:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-interface/range {v25 .. v26}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    goto :goto_12

    :cond_2b
    const/16 v25, 0x0

    const/16 v26, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->animateBackgroundGradient(FZ)V

    goto/16 :goto_c

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method checkDropPermission()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    aget v3, v3, v1

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->screen:I

    :goto_0
    goto :cond_1
    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showNotAllowedAddMessage()V

    :goto_1
    return v1

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    invoke-virtual {p0, v3, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->isTargetCellLocked(Lcom/cyanogenmod/trebuchet/CellLayout;[I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method clearChildrenCache()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setChildrenDrawnWithCacheEnabled(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isHardwareAccelerated()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setChildrenDrawingCacheEnabled(Z)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method clearDropTargets()V
    .locals 7

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWorkspaceAndHotseatCellLayoutChildren()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    return-void

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {v3, v2}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v6, v4, Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    check-cast v4, Lcom/cyanogenmod/trebuchet/DropTarget;

    invoke-virtual {v6, v4}, Lcom/cyanogenmod/trebuchet/DragController;->removeDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 1

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->computeScroll()V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->syncWallpaperOffsetWithScroll()V

    :cond_0
    return-void
.end method

.method public createDragBitmap(Landroid/view/View;Landroid/graphics/Canvas;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Canvas;
    .param p3    # I

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080007    # com.konka.avenger.R.color.konka_orange

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    instance-of v3, p1, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    move-object v3, p1

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aget-object v1, v3, v6

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, p3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v4, p3

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, p1, p2, p3, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->drawDragView(Landroid/view/View;Landroid/graphics/Canvas;IZ)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {v3, v0, p2, v2}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->applyOuterBlur(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;I)V

    iget v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewMultiplyColor:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p2, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, p3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, p3

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method createUserFolderIfNecessary(Landroid/view/View;JLcom/cyanogenmod/trebuchet/CellLayout;[IZLcom/cyanogenmod/trebuchet/DragView;Ljava/lang/Runnable;)Z
    .locals 21
    .param p1    # Landroid/view/View;
    .param p2    # J
    .param p4    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p5    # [I
    .param p6    # Z
    .param p7    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p8    # Ljava/lang/Runnable;

    const/4 v2, 0x0

    aget v2, p5, v2

    const/4 v3, 0x1

    aget v3, p5, v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v9

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v2, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellX:I

    const/4 v3, 0x0

    aget v3, p5, v3

    if-ne v2, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v2, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellY:I

    const/4 v3, 0x1

    aget v3, p5, v3

    if-ne v2, v3, :cond_2

    move-object/from16 v0, v17

    move-object/from16 v1, p4

    if-ne v0, v1, :cond_2

    const/16 v19, 0x1

    :cond_0
    :goto_0
    if-eqz v9, :cond_1

    if-nez v19, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    if-nez v2, :cond_3

    :cond_1
    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_2
    const/16 v19, 0x0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    if-nez p5, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v6, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->screen:I

    :goto_2
    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v15, v2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v0, v2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move/from16 v20, v0

    if-eqz v15, :cond_8

    if-eqz v20, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {v9}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-nez p6, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    :cond_4
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    invoke-virtual {v2, v9, v12}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result v13

    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v3, 0x0

    aget v7, p5, v3

    const/4 v3, 0x1

    aget v8, p5, v3

    move-object/from16 v3, p4

    move-wide/from16 v4, p2

    invoke-virtual/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/Launcher;->addFolder(Lcom/cyanogenmod/trebuchet/CellLayout;JIII)Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-result-object v7

    const/4 v2, -0x1

    move-object/from16 v0, v18

    iput v2, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    const/4 v2, -0x1

    move-object/from16 v0, v18

    iput v2, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    const/4 v2, -0x1

    iput v2, v10, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    const/4 v2, -0x1

    iput v2, v10, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    if-eqz p7, :cond_6

    const/16 v16, 0x1

    :goto_3
    if-eqz v16, :cond_7

    move-object/from16 v8, v18

    move-object/from16 v11, p7

    move-object/from16 v14, p8

    invoke-virtual/range {v7 .. v14}, Lcom/cyanogenmod/trebuchet/FolderIcon;->performCreateAnimation(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Landroid/graphics/Rect;FLjava/lang/Runnable;)V

    :goto_4
    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/FolderIcon;->requestFocus()Z

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v6

    goto/16 :goto_2

    :cond_6
    const/16 v16, 0x0

    goto :goto_3

    :cond_7
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->addItem(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    invoke-virtual {v7, v10}, Lcom/cyanogenmod/trebuchet/FolderIcon;->addItem(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    goto :goto_4

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const v7, 0x3f060a92

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v5

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mXDown:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mYDown:F

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/4 v5, 0x0

    invoke-static {v0, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    div-float v3, v1, v0

    float-to-double v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->atan(D)D

    move-result-wide v5

    double-to-float v4, v5

    iget v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTouchSlop:I

    int-to-float v5, v5

    cmpl-float v5, v0, v5

    if-gtz v5, :cond_2

    iget v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTouchSlop:I

    int-to-float v5, v5

    cmpl-float v5, v1, v5

    if-lez v5, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->cancelCurrentPageLongPress()V

    :cond_3
    const v5, 0x3f860a92

    cmpl-float v5, v4, v5

    if-gtz v5, :cond_0

    cmpl-float v5, v4, v7

    if-lez v5, :cond_4

    sub-float/2addr v4, v7

    div-float v5, v4, v7

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    double-to-float v2, v5

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x40800000    # 4.0f

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    invoke-super {p0, p1, v5}, Lcom/cyanogenmod/trebuchet/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;F)V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method disableBackground()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDrawBackground:Z

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-boolean v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    if-eqz v9, :cond_0

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getHeight()I

    move-result v1

    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v6

    sub-int v9, v1, v6

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mPaddingTop:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mPaddingBottom:I

    sub-int/2addr v9, v10

    div-int/lit8 v3, v9, 0x2

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mPaddingTop:I

    add-int v5, v9, v3

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mPaddingBottom:I

    add-int v4, v9, v3

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getIsDragOverlapping()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02008d    # com.konka.avenger.R.drawable.page_hover_left_holo

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    add-int/2addr v10, v11

    sub-int v11, v1, v4

    invoke-virtual {v0, v9, v5, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getIsDragOverlapping()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02008e    # com.konka.avenger.R.drawable.page_hover_right_holo

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    add-int/2addr v9, v8

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    add-int/2addr v10, v8

    sub-int v11, v1, v4

    invoke-virtual {v0, v9, v5, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->isSearchScreen(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/PagedView;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method enableBackground()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDrawBackground:Z

    return-void
.end method

.method enableChildrenCache(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x1

    if-le p1, p2, :cond_0

    move v3, p1

    move p1, p2

    move p2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v2

    const/4 v4, 0x0

    invoke-static {p1, v4}, Ljava/lang/Math;->max(II)I

    move-result p1

    add-int/lit8 v4, v2, -0x1

    invoke-static {p2, v4}, Ljava/lang/Math;->min(II)I

    move-result p2

    move v0, p1

    :goto_0
    if-le v0, p2, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setChildrenDrawnWithCacheEnabled(Z)V

    invoke-virtual {v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setChildrenDrawingCacheEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public estimateItemPosition(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/ItemInfo;IIII)Landroid/graphics/RectF;
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p2    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    move-object v0, p1

    move v1, p3

    move v2, p4

    move v3, p5

    move v4, p6

    invoke-virtual/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->cellToRect(IIIILandroid/graphics/RectF;)V

    instance-of v0, p2, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    if-eqz v0, :cond_0

    move-object v7, p2

    check-cast v7, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    iget-object v2, v7, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->componentName:Landroid/content/ComponentName;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v6

    iget v0, v5, Landroid/graphics/RectF;->top:F

    iget v1, v6, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, v5, Landroid/graphics/RectF;->top:F

    iget v0, v5, Landroid/graphics/RectF;->left:F

    iget v1, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, v5, Landroid/graphics/RectF;->left:F

    iget v0, v5, Landroid/graphics/RectF;->right:F

    iget v1, v6, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, v5, Landroid/graphics/RectF;->right:F

    iget v0, v5, Landroid/graphics/RectF;->bottom:F

    iget v1, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, v5, Landroid/graphics/RectF;->bottom:F

    :cond_0
    return-object v5
.end method

.method public estimateItemSize(IILcom/cyanogenmod/trebuchet/PendingAddItemInfo;Z)[I
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;
    .param p4    # Z

    const v2, 0x7fffffff

    const/4 v9, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v8, v0, [I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object v0, p0

    move-object v2, p3

    move v4, v3

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Workspace;->estimateItemPosition(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/ItemInfo;IIII)Landroid/graphics/RectF;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    aput v0, v8, v3

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-int v0, v0

    aput v0, v8, v9

    if-eqz p4, :cond_0

    aget v0, v8, v3

    int-to-float v0, v0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedShrinkFactor:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    aput v0, v8, v3

    aget v0, v8, v9

    int-to-float v0, v0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedShrinkFactor:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    aput v0, v8, v9

    :cond_0
    :goto_0
    return-object v8

    :cond_1
    aput v2, v8, v3

    aput v2, v8, v9

    goto :goto_0
.end method

.method public exitWidgetResizeMode()V
    .locals 2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    return-void
.end method

.method protected flashScrollingIndicator(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeScrollingIndicator:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->flashScrollingIndicator(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->showScrollingIndicator(Z)V

    goto :goto_0
.end method

.method public getBackgroundAlpha()F
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundAlpha:F

    return v0
.end method

.method public getChildrenOutlineAlpha()F
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineAlpha:F

    return v0
.end method

.method public getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;
    .locals 2

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    return-object v0

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    goto :goto_0
.end method

.method protected getCurrentPageDescription()Ljava/lang/String;
    .locals 5

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    :goto_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0054    # com.konka.avenger.R.string.workspace_scroll_format

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    add-int/lit8 v4, v0, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    goto :goto_0
.end method

.method public getDescendantFocusability()I
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x60000

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getDescendantFocusability()I

    move-result v0

    goto :goto_0
.end method

.method public getDragInfo()Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    return-object v0
.end method

.method public getDropTargetDelegate(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Lcom/cyanogenmod/trebuchet/DropTarget;
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFolderForTag(Ljava/lang/Object;)Lcom/cyanogenmod/trebuchet/Folder;
    .locals 8
    .param p1    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWorkspaceAndHotseatCellLayoutChildren()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v3, 0x0

    :cond_1
    return-object v3

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    invoke-virtual {v5, v4}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v7, v0, Lcom/cyanogenmod/trebuchet/Folder;

    if-eqz v7, :cond_3

    move-object v3, v0

    check-cast v3, Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Folder;->getInfo()Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v7

    if-ne v7, p1, :cond_3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Folder;->getInfo()Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v7

    iget-boolean v7, v7, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    if-nez v7, :cond_1

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getHitRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1    # Landroid/graphics/Rect;

    const/4 v2, 0x0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayWidth:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayHeight:I

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public getHomeScreenIndex()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getHomeScreenIndex()I

    move-result v0

    return v0
.end method

.method public getHorizontalWallpaperOffset()F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->getCurrX()F

    move-result v0

    return v0
.end method

.method public getLocationInDragLayer([I)V
    .locals 1
    .param p1    # [I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocationInDragLayer(Landroid/view/View;[I)V

    return-void
.end method

.method protected getOffsetXForRotation(FII)F
    .locals 5
    .param p1    # F
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v0}, Landroid/graphics/Camera;->save()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCamera:Landroid/graphics/Camera;

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Camera;->rotateY(F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCamera:Landroid/graphics/Camera;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Camera;->getMatrix(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v0}, Landroid/graphics/Camera;->restore()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMatrix:Landroid/graphics/Matrix;

    neg-int v1, p2

    int-to-float v1, v1

    mul-float/2addr v1, v3

    neg-int v2, p3

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMatrix:Landroid/graphics/Matrix;

    int-to-float v1, p2

    mul-float/2addr v1, v3

    int-to-float v2, p3

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempFloat2:[F

    int-to-float v1, p2

    aput v1, v0, v4

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempFloat2:[F

    const/4 v1, 0x1

    int-to-float v2, p3

    aput v2, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempFloat2:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    int-to-float v0, p2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempFloat2:[F

    aget v1, v1, v4

    sub-float v1, v0, v1

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    mul-float/2addr v0, v1

    return v0

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;
    .locals 6

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/DragLayer;->getChildCount()I

    move-result v1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v1, :cond_1

    const/4 v3, 0x0

    :cond_0
    return-object v3

    :cond_1
    invoke-virtual {v2, v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v5, v0, Lcom/cyanogenmod/trebuchet/Folder;

    if-eqz v5, :cond_2

    move-object v3, v0

    check-cast v3, Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Folder;->getInfo()Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v5

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    if-nez v5, :cond_0

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWorkspaceAndHotseatCellLayouts()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->indexOfChild(Landroid/view/View;)I

    move-result v3

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    goto :goto_0
.end method

.method public getSearchScreen()Lcom/cyanogenmod/trebuchet/CellLayout;
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009    # com.konka.avenger.R.bool.config_searchScreenEnable

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    goto :goto_0
.end method

.method public getSearchScreenIndex()I
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009    # com.konka.avenger.R.bool.config_searchScreenEnable

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getState()Lcom/cyanogenmod/trebuchet/Workspace$State;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    return-object v0
.end method

.method public getTransitionEffect()Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    return-object v0
.end method

.method public getVerticalWallpaperOffset()F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->getCurrY()F

    move-result v0

    return v0
.end method

.method public getViewForTag(Ljava/lang/Object;)Landroid/view/View;
    .locals 7
    .param p1    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWorkspaceAndHotseatCellLayoutChildren()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v0, 0x0

    :cond_1
    return-object v0

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    invoke-virtual {v4, v3}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    if-eq v6, p1, :cond_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getViewLocationRelativeToSelf(Landroid/view/View;[I)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # [I

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0, p2}, Lcom/cyanogenmod/trebuchet/Workspace;->getLocationInWindow([I)V

    aget v2, p2, v5

    aget v3, p2, v6

    invoke-virtual {p1, p2}, Landroid/view/View;->getLocationInWindow([I)V

    aget v0, p2, v5

    aget v1, p2, v6

    sub-int v4, v0, v2

    aput v4, p2, v5

    sub-int v4, v1, v3

    aput v4, p2, v6

    return-void
.end method

.method getWorkspaceAndHotseatCellLayoutChildren()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/CellLayoutChildren;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method getWorkspaceAndHotseatCellLayouts()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/CellLayout;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method hideOutlines()V
    .locals 4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_1
    const-string v0, "childrenOutlineAlpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x177

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_2
    return-void
.end method

.method protected hitsNextPage(FF)Z
    .locals 3
    .param p1    # F
    .param p2    # F

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    :goto_0
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1, p1, p2}, Lcom/cyanogenmod/trebuchet/Workspace;->hitsPage(IFF)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected hitsPreviousPage(FF)Z
    .locals 3
    .param p1    # F
    .param p2    # F

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    :goto_0
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-direct {p0, v1, p1, p2}, Lcom/cyanogenmod/trebuchet/Workspace;->hitsPage(IFF)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected initWorkspace()V
    .locals 13

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDefaultHomescreen:I

    iput v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-static {v8}, Lcom/cyanogenmod/trebuchet/Launcher;->setScreen(I)V

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherApplication;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getIconCache()Lcom/cyanogenmod/trebuchet/IconCache;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mExternalDragOutlinePaint:Landroid/graphics/Paint;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->setWillNotDraw(Z)V

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->setChildrenDrawnWithCacheEnabled(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const/4 v3, 0x0

    :goto_0
    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNumberHomescreens:I

    if-lt v3, v8, :cond_4

    const v8, 0x7f020008    # com.konka.avenger.R.drawable.apps_customize_bg

    :try_start_0
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackground:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowSearchBar:Z

    if-nez v8, :cond_1

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentOrientation()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    const v8, 0x7f0c0005    # com.konka.avenger.R.dimen.qsb_bar_hidden_inset

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v5, v8

    :cond_0
    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPaddingRight()I

    move-result v9

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPaddingBottom()I

    move-result v10

    invoke-virtual {p0, v8, v5, v9, v10}, Lcom/cyanogenmod/trebuchet/Workspace;->setPadding(IIII)V

    :cond_1
    iget-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowScrollingIndicator:Z

    if-nez v8, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->disableScrollingIndicator()V

    :cond_2
    new-instance v8, Lcom/cyanogenmod/trebuchet/Workspace$1;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/Workspace$1;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChangeStateAnimationListener:Landroid/animation/Animator$AnimatorListener;

    const/16 v8, 0x64

    iput v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSnapVelocity:I

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v8

    iput v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayWidth:I

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v8

    iput v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayHeight:I

    iget-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    if-eqz v8, :cond_3

    new-instance v8, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayWidth:I

    int-to-float v8, v8

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayWidth:I

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDisplayHeight:I

    invoke-direct {p0, v9, v10}, Lcom/cyanogenmod/trebuchet/Workspace;->wallpaperTravelToScreenWidthRatio(II)F

    move-result v9

    mul-float/2addr v8, v9

    float-to-int v8, v8

    iput v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperTravelWidth:I

    :cond_3
    new-instance v8, Lnet/londatiga/android/QuickAction;

    const/4 v9, 0x0

    const/4 v10, 0x3

    invoke-direct {v8, v1, v9, v10}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;II)V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    new-instance v8, Lnet/londatiga/android/QuickAction;

    const/4 v9, 0x0

    const/4 v10, 0x3

    invoke-direct {v8, v1, v9, v10}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;II)V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    new-instance v8, Lnet/londatiga/android/QuickAction;

    const/4 v9, 0x0

    const/4 v10, 0x3

    invoke-direct {v8, v1, v9, v10}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;II)V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->initQuickActionMenu()V

    return-void

    :cond_4
    const v8, 0x7f030035    # com.konka.avenger.R.layout.workspace_screen

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v7, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setupGuideView(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v8

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScreenPaddingHorizontal:I

    add-int/2addr v8, v9

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v9

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScreenPaddingVertical:I

    add-int/2addr v9, v10

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingRight()I

    move-result v10

    iget v11, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScreenPaddingHorizontal:I

    add-int/2addr v10, v11

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingBottom()I

    move-result v11

    iget v12, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScreenPaddingVertical:I

    add-int/2addr v11, v12

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPadding(IIII)V

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v8

    goto/16 :goto_1
.end method

.method public isDefaultHomeScreen(I)Z
    .locals 2
    .param p1    # I

    const/4 v1, 0x0
    return v1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getDefaultHomescreen(Landroid/content/Context;)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDefaultHomeScreen(Lcom/cyanogenmod/trebuchet/CellLayout;)Z
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;
    const/4 v1, 0x0
    return v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getDefaultHomescreen(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method isDrawingBackgroundGradient()Z
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundAlpha:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDrawBackground:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDropEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isInSearchScreen()Z
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSearchScreen(I)Z

    move-result v0

    return v0
.end method

.method protected isScrollingIndicatorEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$State;->SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Workspace$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchScreen(I)Z
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009    # com.konka.avenger.R.bool.config_searchScreenEnable

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchScreen(Lcom/cyanogenmod/trebuchet/CellLayout;)Z
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009    # com.konka.avenger.R.bool.config_searchScreenEnable

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSmall()Z
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$State;->SMALL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$State;->SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Workspace$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSwitchingState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    return v0
.end method

.method isTargetCellLocked(Lcom/cyanogenmod/trebuchet/CellLayout;[I)Z
    .locals 6
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p2    # [I

    const/4 v3, 0x1

    const/4 v2, 0x0

    aget v4, p2, v2

    aget v5, p2, v3

    invoke-virtual {p1, v4, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v4, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method isTouchActive()Z
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTouchState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method mapPointFromChildToSelf(Landroid/view/View;[F)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # [F

    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->mapPoints([F)V

    const/4 v0, 0x0

    aget v1, p2, v0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    sub-float/2addr v1, v2

    aput v1, p2, v0

    const/4 v0, 0x1

    aget v1, p2, v0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollY:I

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    sub-float/2addr v1, v2

    aput v1, p2, v0

    return-void
.end method

.method mapPointFromSelfToChild(Landroid/view/View;[F)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # [F

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    return-void
.end method

.method mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # [F
    .param p3    # Landroid/graphics/Matrix;

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempInverseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object p3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempInverseMatrix:Landroid/graphics/Matrix;

    :cond_0
    aget v0, p2, v2

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p2, v2

    aget v0, p2, v3

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollY:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p2, v3

    invoke-virtual {p3, p2}, Landroid/graphics/Matrix;->mapPoints([F)V

    return-void
.end method

.method mapPointFromSelfToSibling(Landroid/view/View;[F)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # [F

    const/4 v3, 0x1

    const/4 v2, 0x0

    aget v0, p2, v2

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p2, v2

    aget v0, p2, v3

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p2, v3

    return-void
.end method

.method moveToDefaultScreen(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDefaultHomescreen:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    :cond_0
    :goto_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDefaultHomescreen:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void

    :cond_1
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDefaultHomescreen:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->setCurrentPage(I)V

    goto :goto_0
.end method

.method protected notifyPageSwitchListener()V
    .locals 1

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->notifyPageSwitchListener()V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->setScreen(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->refreshPageBtn()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWindowToken:Landroid/os/IBinder;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->computeScroll()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWindowToken:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DragController;->setWindowToken(Landroid/os/IBinder;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWindowToken:Landroid/os/IBinder;

    return-void
.end method

.method public onDragEnd()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsDragOccuring:Z

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->unlockScreenOrientationOnLargeUI()V

    return-void
.end method

.method public onDragEnter(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragExit()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragEnter()V

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->showOutlines()V

    :cond_1
    return-void
.end method

.method public onDragEvent(Landroid/view/DragEvent;)Z
    .locals 34
    .param p1    # Landroid/view/DragEvent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v26

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v4, 0x2

    new-array v14, v4, [I

    invoke-virtual {v3, v14}, Lcom/cyanogenmod/trebuchet/CellLayout;->getLocationOnScreen([I)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    const/4 v5, 0x0

    aget v5, v14, v5

    sub-int v6, v4, v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    sub-int v7, v4, v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    invoke-super/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onDragEvent(Landroid/view/DragEvent;)Z

    move-result v4

    :goto_0
    return v4

    :pswitch_0
    invoke-direct/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/Workspace;->validateDrag(Landroid/view/DragEvent;)Landroid/util/Pair;

    move-result-object v32

    if-eqz v32, :cond_1

    move-object/from16 v0, v32

    iget-object v4, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v4, :cond_0

    const/16 v30, 0x1

    :goto_1
    if-eqz v30, :cond_2

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v3, v14, v4, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/16 v30, 0x0

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    const v8, 0x7f0a0015    # com.konka.avenger.R.string.external_drop_widget_error

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    new-instance v25, Landroid/graphics/Canvas;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Canvas;-><init>()V

    sget v23, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->createExternalDragOutline(Landroid/graphics/Canvas;I)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->showOutlines()V

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragEnter()V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->visualizeDropLocation(Landroid/view/View;Landroid/graphics/Bitmap;IIIILandroid/graphics/Point;Landroid/graphics/Rect;)V

    const/4 v4, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->visualizeDropLocation(Landroid/view/View;Landroid/graphics/Bitmap;IIIILandroid/graphics/Point;Landroid/graphics/Rect;)V

    const/4 v4, 0x1

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getModel()Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-result-object v31

    invoke-virtual/range {p1 .. p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v18

    const/4 v4, 0x0

    aput v6, v14, v4

    const/4 v4, 0x1

    aput v7, v14, v4

    invoke-direct/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/Workspace;->validateDrag(Landroid/view/DragEvent;)Landroid/util/Pair;

    move-result-object v32

    if-eqz v32, :cond_3

    move-object/from16 v0, v32

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, v32

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/util/List;

    if-nez v19, :cond_4

    const/16 v30, 0x1

    :goto_2
    invoke-virtual/range {v26 .. v27}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v17

    if-eqz v30, :cond_5

    move-object/from16 v0, v18

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    invoke-virtual/range {v18 .. v18}, Landroid/content/ClipData;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v5

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v4, v1, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel;->infoFromShortcutIntent(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v28

    if-eqz v28, :cond_3

    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v6, v4, v5

    const/4 v5, 0x1

    aput v7, v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v4, v1, v3, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->onDropExternal([ILjava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;Z)V

    :cond_3
    :goto_3
    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_4
    const/16 v30, 0x0

    goto :goto_2

    :cond_5
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;

    iget-object v0, v4, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;->widgetInfo:Landroid/appwidget/AppWidgetProviderInfo;

    move-object/from16 v33, v0

    new-instance v9, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    move-object/from16 v0, v33

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v9, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;-><init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const-wide/16 v10, -0x64

    move-object/from16 v0, p0

    iget v12, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v14}, Lcom/cyanogenmod/trebuchet/Launcher;->addAppWidgetFromDrop(Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;JI[I[I)V

    goto :goto_3

    :cond_6
    new-instance v15, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v21, v0

    move-object/from16 v20, v3

    move-object/from16 v22, v14

    invoke-direct/range {v15 .. v22}, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/lang/String;Landroid/content/ClipData;Ljava/util/List;Lcom/cyanogenmod/trebuchet/CellLayout;I[I)V

    new-instance v24, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v15, v15}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v4, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0016    # com.konka.avenger.R.string.external_drop_widget_pick_title

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f02007a    # com.konka.avenger.R.drawable.ic_no_applications

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {v24 .. v24}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_3

    :pswitch_3
    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragExit()V

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->hideOutlines()V

    const/4 v4, 0x1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->doDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    return-void
.end method

.method public onDragOver(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 20
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v2, :cond_0

    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    const/16 v17, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Lcom/cyanogenmod/trebuchet/ItemInfo;

    move-object/from16 v0, v16

    iget v2, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    if-ltz v2, :cond_2

    move-object/from16 v0, v16

    iget v2, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    if-gez v2, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Improper spans found"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    move-object/from16 v0, p1

    iget v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    move-object/from16 v0, p1

    iget v5, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->xOffset:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->yOffset:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/Workspace;->getDragViewVisualCenter(IIIILcom/cyanogenmod/trebuchet/DragView;[F)[F

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-direct/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/Workspace;->isExternalDragWidget(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/Hotseat;->getHitRect(Landroid/graphics/Rect;)V

    move-object/from16 v0, p1

    iget v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    move-object/from16 v0, p1

    iget v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v17

    :cond_4
    if-nez v17, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    int-to-float v3, v3

    move-object/from16 v0, p1

    iget v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    int-to-float v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->findMatchingPageForDragOver(Lcom/cyanogenmod/trebuchet/DragView;FFZ)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v17

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, v17

    if-eq v0, v2, :cond_7

    invoke-direct/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/Workspace;->cleanupFolderCreation(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragExit()V

    :cond_6
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragEnter()V

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mState:Lcom/cyanogenmod/trebuchet/Workspace$State;

    sget-object v3, Lcom/cyanogenmod/trebuchet/Workspace$State;->SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Workspace$State;

    if-ne v2, v3, :cond_c

    const/4 v14, 0x1

    :goto_2
    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedDragController:Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->cancel()V

    :cond_7
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-nez v2, :cond_12

    const/4 v11, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToSibling(Landroid/view/View;[F)V

    :goto_5
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v13, Lcom/cyanogenmod/trebuchet/ItemInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v3, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v4, 0x1

    aget v2, v2, v4

    float-to-int v4, v2

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestArea(IIIILcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v2, v3, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->willCreateUserFolder(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;[IZ)Z

    move-result v19

    instance-of v15, v12, Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    if-eq v12, v2, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->cancelFolderCreation()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    instance-of v2, v2, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    check-cast v2, Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDragExit(Ljava/lang/Object;)V

    :cond_8
    if-eqz v19, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    if-eq v12, v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderCreationAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    new-instance v3, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/CellLayout;II)V

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Alarm;->setOnAlarmListener(Lcom/cyanogenmod/trebuchet/OnAlarmListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderCreationAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    const-wide/16 v3, 0xfa

    invoke-virtual {v2, v3, v4}, Lcom/cyanogenmod/trebuchet/Alarm;->setAlarm(J)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    if-eq v12, v2, :cond_a

    if-eqz v15, :cond_a

    move-object v2, v12

    check-cast v2, Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDragEnter(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->clearDragOutlines()V

    :cond_a
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    if-nez v2, :cond_0

    if-nez v15, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v5, 0x0

    aget v3, v3, v5

    float-to-int v5, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    float-to-int v6, v3

    move-object/from16 v0, v16

    iget v7, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    move-object/from16 v0, v16

    iget v8, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/DragView;->getDragVisualizeOffset()Landroid/graphics/Point;

    move-result-object v9

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/DragView;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v10

    move-object v3, v11

    invoke-virtual/range {v2 .. v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->visualizeDropLocation(Landroid/view/View;Landroid/graphics/Bitmap;IIIILandroid/graphics/Point;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLastDragOverView:Landroid/view/View;

    goto/16 :goto_1

    :cond_c
    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedDragController:Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->setAlarm(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    goto/16 :goto_3

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-direct/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/Workspace;->isDragWidget(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z

    move-result v2

    if-nez v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/Hotseat;->getHitRect(Landroid/graphics/Rect;)V

    move-object/from16 v0, p1

    iget v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    move-object/from16 v0, p1

    iget v3, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v17

    :cond_f
    if-nez v17, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v17

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, v17

    if-eq v0, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragExit()V

    :cond_11
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragEnter()V

    goto/16 :goto_3

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v11, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    goto/16 :goto_4

    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    goto/16 :goto_5
.end method

.method public onDragStart(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;I)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsDragOccuring:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->lockScreenOrientationOnLargeUI()V

    return-void
.end method

.method public onDragStartedWithItem(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1}, Landroid/graphics/Canvas;-><init>()V

    sget v0, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    invoke-direct {p0, p1, v1, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->createDragOutline(Landroid/view/View;Landroid/graphics/Canvas;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    return-void
.end method

.method public onDragStartedWithItem(Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Landroid/graphics/Paint;

    const/4 v4, 0x0

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    sget v3, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    iget v0, p1, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->spanX:I

    iget v1, p1, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->spanY:I

    invoke-virtual {p0, v0, v1, p1, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->estimateItemSize(IILcom/cyanogenmod/trebuchet/PendingAddItemInfo;Z)[I

    move-result-object v7

    aget v4, v7, v4

    const/4 v0, 0x1

    aget v5, v7, v0

    move-object v0, p0

    move-object v1, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Workspace;->createDragOutline(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIILandroid/graphics/Paint;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    return-void
.end method

.method public onDragStopped(Z)V
    .locals 1
    .param p1    # Z

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->doDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->updateWallpaperOffsets()V

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundAlpha:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDrawBackground:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundAlpha:F

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v0, v1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackground:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    const/4 v3, 0x0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollX:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 39
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    move-object/from16 v0, p1

    iget v7, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->xOffset:I

    move-object/from16 v0, p1

    iget v8, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->yOffset:I

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/cyanogenmod/trebuchet/Workspace;->getDragViewVisualCenter(IIIILcom/cyanogenmod/trebuchet/DragView;[F)[F

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToSibling(Landroid/view/View;[F)V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/16 v38, -0x1

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    move-object/from16 v0, p0

    if-eq v4, v0, :cond_3

    const/4 v4, 0x2

    new-array v5, v4, [I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v8, 0x0

    aget v6, v6, v8

    float-to-int v6, v6

    aput v6, v5, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v8, 0x1

    aget v6, v6, v8

    float-to-int v6, v6

    aput v6, v5, v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v4, p0

    move-object/from16 v9, p1

    invoke-direct/range {v4 .. v9}, Lcom/cyanogenmod/trebuchet/Workspace;->onDropExternal([ILjava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;ZLcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    move-object/from16 v25, v0

    if-eqz v7, :cond_9

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v4

    if-eq v4, v7, :cond_a

    const/16 v31, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v30

    if-eqz v30, :cond_b

    const/16 v4, -0x65

    :goto_3
    int-to-long v15, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v4, v4, v6

    if-gez v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->screen:I

    move/from16 v37, v0

    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v11, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v12, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    float-to-int v9, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v6, 0x1

    aget v4, v4, v6

    float-to-int v10, v4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v8, p0

    move-object v13, v7

    invoke-direct/range {v8 .. v14}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestArea(IIIILcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v13, p0

    move-object/from16 v14, v25

    move-object/from16 v17, v7

    invoke-virtual/range {v13 .. v21}, Lcom/cyanogenmod/trebuchet/Workspace;->createUserFolderIfNecessary(Landroid/view/View;JLcom/cyanogenmod/trebuchet/CellLayout;[IZLcom/cyanogenmod/trebuchet/DragView;Ljava/lang/Runnable;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v20, v0

    const/16 v22, 0x0

    move-object/from16 v17, p0

    move-object/from16 v18, v25

    move-object/from16 v19, v7

    move-object/from16 v21, p1

    invoke-virtual/range {v17 .. v22}, Lcom/cyanogenmod/trebuchet/Workspace;->addToExistingFolderIfNecessary(Landroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[ILcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)Z

    move-result v4

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v6, 0x0

    aget v4, v4, v6

    float-to-int v0, v4

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragViewVisualCenter:[F

    const/4 v6, 0x1

    aget v4, v4, v6

    float-to-int v0, v4

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v24, v0

    move-object/from16 v17, p0

    move-object/from16 v22, v25

    move-object/from16 v23, v7

    invoke-direct/range {v17 .. v24}, Lcom/cyanogenmod/trebuchet/Workspace;->findNearestVacantArea(IIIILandroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[I)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    move/from16 v0, v37

    if-eq v4, v0, :cond_5

    if-nez v30, :cond_5

    move/from16 v38, v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v4, v4, v6

    if-ltz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x1

    aget v4, v4, v6

    if-ltz v4, :cond_9

    if-eqz v31, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v18, v4, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x1

    aget v19, v4, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    move/from16 v21, v0

    move-object/from16 v13, p0

    move-object/from16 v14, v25

    move/from16 v17, v37

    invoke-virtual/range {v13 .. v21}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIII)V

    :cond_6
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v33

    check-cast v33, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v4, v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v8, 0x1

    aget v6, v6, v8

    move-object/from16 v0, v25

    invoke-virtual {v7, v0, v4, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->onMove(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v4, v4, v6

    move-object/from16 v0, v33

    iput v4, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x1

    aget v4, v4, v6

    move-object/from16 v0, v33

    iput v4, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->screen:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v18, v4, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x1

    aget v19, v4, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    move/from16 v21, v0

    invoke-static/range {v15 .. v21}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellLayoutChildId(JIIIII)I

    move-result v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setId(I)V

    const-wide/16 v8, -0x65

    cmp-long v4, v15, v8

    if-eqz v4, :cond_8

    move-object/from16 v0, v25

    instance-of v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    if-eqz v4, :cond_8

    move-object/from16 v26, v7

    move-object/from16 v32, v25

    check-cast v32, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    invoke-virtual/range {v32 .. v32}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x0

    aget v4, v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellX:I

    if-ne v4, v6, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mTargetCell:[I

    const/4 v6, 0x1

    aget v4, v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellY:I

    if-ne v4, v6, :cond_f

    const/16 v29, 0x0

    :goto_7
    move-object/from16 v0, v35

    iget v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    if-nez v4, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mResizeAnyWidget:Z

    if-eqz v4, :cond_8

    :cond_7
    if-nez v29, :cond_8

    if-nez v31, :cond_8

    new-instance v36, Lcom/cyanogenmod/trebuchet/Workspace$8;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    move-object/from16 v2, v32

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v14, v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace$8;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;Lcom/cyanogenmod/trebuchet/CellLayout;)V

    new-instance v4, Lcom/cyanogenmod/trebuchet/Workspace$9;

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v4, v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace$9;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Ljava/lang/Runnable;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->post(Ljava/lang/Runnable;)Z

    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v0, v33

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v18, v0

    move-object/from16 v0, v33

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v19, v0

    move/from16 v17, v37

    invoke-static/range {v13 .. v19}, Lcom/cyanogenmod/trebuchet/LauncherModel;->moveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    :cond_9
    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v34

    check-cast v34, Lcom/cyanogenmod/trebuchet/CellLayout;

    new-instance v27, Lcom/cyanogenmod/trebuchet/Workspace$10;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace$10;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mAnimatingViewIntoPlace:Z

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/DragView;->hasDrawn()Z

    move-result v4

    if-eqz v4, :cond_11

    if-gez v38, :cond_10

    const/16 v28, -0x1

    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setFinalScrollForPageChange(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v0, v25

    move/from16 v1, v28

    move-object/from16 v2, v27

    invoke-virtual {v4, v6, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;ILjava/lang/Runnable;)V

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->resetFinalScrollForPageChange(I)V

    :goto_9
    move-object/from16 v0, v34

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDropChild(Landroid/view/View;)V

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->isInTouchMode()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_1

    :cond_a
    const/16 v31, 0x0

    goto/16 :goto_2

    :cond_b
    const/16 v4, -0x64

    goto/16 :goto_3

    :cond_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v37

    goto/16 :goto_4

    :cond_d
    const/4 v11, 0x1

    goto/16 :goto_5

    :cond_e
    const/4 v12, 0x1

    goto/16 :goto_6

    :cond_f
    const/16 v29, 0x1

    goto/16 :goto_7

    :cond_10
    const/16 v28, 0x12c

    goto :goto_8

    :cond_11
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9
.end method

.method public onDropCompleted(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;
    .param p3    # Z

    const/4 v3, 0x0

    if-eqz p3, :cond_2

    if-eq p1, p0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    instance-of v1, v1, Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    check-cast v1, Lcom/cyanogenmod/trebuchet/DropTarget;

    invoke-virtual {v2, v1}, Lcom/cyanogenmod/trebuchet/DragController;->removeDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    :cond_0
    :goto_0
    iget-boolean v1, p2, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->cancelled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_1
    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    return-void

    :cond_2
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v1, :cond_0

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->doDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDropChild(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->screen:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    goto :goto_1
.end method

.method public onEnterScrollArea(III)Z
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v4

    if-eqz v4, :cond_1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/cyanogenmod/trebuchet/Hotseat;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v6

    :cond_0
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v4, :cond_0

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    iget v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    if-nez p3, :cond_3

    const/4 v4, -0x1

    :goto_1
    add-int v1, v7, v4

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->cancelFolderCreation()V

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragExit()V

    :cond_2
    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    move v4, v5

    goto :goto_1
.end method

.method public onExitScrollArea()Z
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->onDragEnter()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    const/4 v0, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mInScrollArea:Z

    :cond_1
    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    :sswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mXDown:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mYDown:F

    goto :goto_0

    :sswitch_1
    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTouchState:I

    if-nez v1, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->lastDownOnOccupiedCell()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->onWallpaperTap(Landroid/view/MotionEvent;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onItemClick(Lnet/londatiga/android/QuickAction;II)V
    .locals 9
    .param p1    # Lnet/londatiga/android/QuickAction;
    .param p2    # I
    .param p3    # I

    const/4 v8, -0x1

    const/4 v7, -0x2

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    if-ne p1, v5, :cond_3

    packed-switch p3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->onClick(Landroid/view/View;)V

    goto :goto_0

    :pswitch_1
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->startDrag(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V

    goto :goto_0

    :pswitch_2
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-static {v6, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    if-ne p1, v5, :cond_6

    packed-switch p3, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->startDrag(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V

    goto :goto_0

    :pswitch_4
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-direct {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->deleteAppWidget(Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_6
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    if-ne p1, v5, :cond_0

    packed-switch p3, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_5
    sget v5, Lcom/cyanogenmod/trebuchet/Workspace;->STATE_TO_RUN:I

    invoke-static {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->setFolderState(I)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->onClick(Landroid/view/View;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto/16 :goto_0

    :cond_7
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->setFolderState(I)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->onClick(Landroid/view/View;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto/16 :goto_0

    :cond_8
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->startDrag(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto/16 :goto_0

    :cond_9
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v5, v4, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {v2, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00bc    # com.konka.avenger.R.string.folder_delete

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00bd    # com.konka.avenger.R.string.folder_noempty

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000d    # com.konka.avenger.R.string.dialog_positive

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/cyanogenmod/trebuchet/Workspace$15;

    invoke-direct {v6, p0, v4}, Lcom/cyanogenmod/trebuchet/Workspace$15;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    invoke-virtual {v3, v8, v5, v6}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000e    # com.konka.avenger.R.string.dialog_dismiss

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/cyanogenmod/trebuchet/Workspace$16;

    invoke-direct {v6, p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace$16;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v7, v5, v6}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    invoke-virtual {v3, v8}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    invoke-virtual {v1}, Landroid/widget/Button;->clearFocus()V

    goto/16 :goto_0

    :cond_a
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->removeFolder(Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v5, v4}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteFolderContentsFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFirstLayout:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mUpdateWallpaperOffsetImmediately:Z

    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/cyanogenmod/trebuchet/PagedView;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSwitchStateAfterFirstLayout:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSwitchStateAfterFirstLayout:Z

    new-instance v0, Lcom/cyanogenmod/trebuchet/Workspace$4;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/Workspace$4;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method protected onPageBeginMoving()V
    .locals 3

    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPageBeginMoving:::::::::::the current page is ==="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "Launcher"

    const-string v1, "HIDE_TV_WINDOW======onPageBeginMoving"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeTVWindow()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    :goto_0
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->showOutlines()V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNextPage:I

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->enableChildrenCache(II)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->enableChildrenCache(II)V

    goto :goto_0
.end method

.method protected onPageEndMoving()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPageEndMoving:::::::::::the current page is ==="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showTVWindow()V

    :cond_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFadeScrollingIndicator:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->hideScrollingIndicator(Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    :goto_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/DragController;->dragging()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->hideOutlines()V

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollMaxBackgroundAlpha:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollPageIndex:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDelayedResizeRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDelayedResizeRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDelayedResizeRunnable:Ljava/lang/Runnable;

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->updateScrollingIndicator()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->clearChildrenCache()V

    goto :goto_1
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->isAllAppsVisible()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/cyanogenmod/trebuchet/Folder;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/PagedView;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1    # Landroid/os/Parcelable;

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->setScreen(I)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onViewAdded(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onViewAdded(Landroid/view/View;)V

    instance-of v1, p1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "A Workspace can only have CellLayout children."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setClickable(Z)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->enableHardwareLayers()V

    return-void
.end method

.method protected onWallpaperTap(Landroid/view/MotionEvent;)V
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempCell:[I

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v7

    aget v0, v8, v5

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    aput v0, v8, v5

    aget v0, v8, v4

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    aput v0, v8, v4

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v4, :cond_0

    const-string v2, "android.wallpaper.tap"

    :goto_0
    aget v3, v8, v5

    aget v4, v8, v4

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/app/WallpaperManager;->sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)V

    return-void

    :cond_0
    const-string v2, "android.wallpaper.secondaryTap"

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->onWindowVisibilityChanged(I)V

    return-void
.end method

.method protected overScroll(F)V
    .locals 1
    .param p1    # F

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->dampedOverScroll(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->acceleratedOverScroll(F)V

    goto :goto_0
.end method

.method overScrollBackgroundAlphaInterpolator(F)F
    .locals 3
    .param p1    # F

    const v0, 0x3da3d70a    # 0.08f

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollMaxBackgroundAlpha:F

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    iput p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollMaxBackgroundAlpha:F

    :cond_0
    :goto_0
    div-float v1, p1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    return v1

    :cond_1
    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollMaxBackgroundAlpha:F

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    iget p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollMaxBackgroundAlpha:F

    goto :goto_0
.end method

.method overlaps(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/DragView;IILandroid/graphics/Matrix;)Z
    .locals 10
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p2    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempDragCoordinates:[F

    const/4 v7, 0x0

    int-to-float v8, p3

    aput v8, v1, v7

    const/4 v7, 0x1

    int-to-float v8, p4

    aput v8, v1, v7

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTempDragBottomRightCoordinates:[F

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, v1, v8

    invoke-virtual {p2}, Lcom/cyanogenmod/trebuchet/DragView;->getDragRegionWidth()I

    move-result v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    aput v8, v0, v7

    const/4 v7, 0x1

    const/4 v8, 0x1

    aget v8, v1, v8

    invoke-virtual {p2}, Lcom/cyanogenmod/trebuchet/DragView;->getDragRegionHeight()I

    move-result v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    aput v8, v0, v7

    invoke-virtual {p0, p1, v1, p5}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, v1, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v4

    const/4 v7, 0x0

    const/4 v8, 0x1

    aget v8, v1, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getWidth()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v4, v7

    if-gtz v7, :cond_0

    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-ltz v7, :cond_0

    invoke-virtual {p0, p1, v0, p5}, Lcom/cyanogenmod/trebuchet/Workspace;->mapPointFromSelfToChild(Landroid/view/View;[FLandroid/graphics/Matrix;)V

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getWidth()I

    move-result v7

    int-to-float v7, v7

    const/4 v8, 0x0

    aget v8, v0, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getHeight()I

    move-result v7

    int-to-float v7, v7

    const/4 v8, 0x1

    aget v8, v0, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/4 v7, 0x0

    cmpl-float v7, v5, v7

    if-ltz v7, :cond_0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getHeight()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-gtz v7, :cond_0

    sub-float v7, v5, v4

    sub-float v8, v3, v6

    mul-float v2, v7, v8

    const/4 v7, 0x0

    cmpl-float v7, v2, v7

    if-lez v7, :cond_0

    const/4 v7, 0x1

    :goto_0
    return v7

    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public popQuickActionMenu(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V
    .locals 7
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    instance-of v2, v0, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v2, :cond_3

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    iget-boolean v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v4, v2}, Lnet/londatiga/android/QuickAction;->setActionItemAccessible(IZ)Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    iget-boolean v5, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_2

    :goto_1
    invoke-virtual {v2, v6, v3}, Lnet/londatiga/android/QuickAction;->setActionItemAccessible(IZ)Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v2, v0}, Lnet/londatiga/android/QuickAction;->show(Landroid/view/View;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1

    :cond_3
    const-string v2, "Launcher.Workspace"

    const-string v3, "the mIconQuickAction is not initialized"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    instance-of v2, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v2, :cond_7

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    iget-boolean v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v2, :cond_5

    move v2, v3

    :goto_3
    invoke-virtual {v5, v6, v2}, Lnet/londatiga/android/QuickAction;->setActionItemAccessible(IZ)Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    const/4 v5, 0x3

    iget-boolean v6, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v6, :cond_6

    :goto_4
    invoke-virtual {v2, v5, v3}, Lnet/londatiga/android/QuickAction;->setActionItemAccessible(IZ)Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mFolderQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v2, v0}, Lnet/londatiga/android/QuickAction;->show(Landroid/view/View;)V

    goto :goto_2

    :cond_5
    move v2, v4

    goto :goto_3

    :cond_6
    move v3, v4

    goto :goto_4

    :cond_7
    const-string v2, "Launcher.Workspace"

    const-string v3, "the mFolderQuickAction is not initialized"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_8
    instance-of v2, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v2, :cond_b

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    iget-boolean v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v2, :cond_9

    move v2, v3

    :goto_5
    invoke-virtual {v5, v3, v2}, Lnet/londatiga/android/QuickAction;->setActionItemAccessible(IZ)Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    iget-boolean v5, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v5, :cond_a

    :goto_6
    invoke-virtual {v2, v4, v3}, Lnet/londatiga/android/QuickAction;->setActionItemAccessible(IZ)Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWidgetQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v2, v0}, Lnet/londatiga/android/QuickAction;->show(Landroid/view/View;)V

    goto :goto_2

    :cond_9
    move v2, v4

    goto :goto_5

    :cond_a
    move v3, v4

    goto :goto_6

    :cond_b
    const-string v2, "Launcher.Workspace"

    const-string v3, "the mWidgetQuickAction is not initialized"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method removeItems(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWorkspaceAndHotseatCellLayouts()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v1, v6, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v2

    new-instance v0, Lcom/cyanogenmod/trebuchet/Workspace$13;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/Workspace$13;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Landroid/view/ViewGroup;Ljava/util/HashSet;Landroid/appwidget/AppWidgetManager;Lcom/cyanogenmod/trebuchet/CellLayout;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public resetFinalScrollForPageChange(I)V
    .locals 2
    .param p1    # I

    if-ltz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSavedScrollX:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setScrollX(I)V

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSavedTranslationX:F

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSavedRotationY:F

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    :cond_0
    return-void
.end method

.method public resetTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentScaleX:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getScaleY()F

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentScaleY:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTranslationX()F

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentTranslationX:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTranslationY()F

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentTranslationY:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getRotationY()F

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentRotationY:F

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentScaleX:F

    invoke-virtual {p1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleX(F)V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentScaleY:F

    invoke-virtual {p1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleY(F)V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentTranslationX:F

    invoke-virtual {p1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentTranslationY:F

    invoke-virtual {p1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationY(F)V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentRotationY:F

    invoke-virtual {p1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    :cond_0
    return-void
.end method

.method protected screenScrolled(I)V
    .locals 12
    .param p1    # I

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v5, 0x3e800000    # 0.25f

    const/4 v10, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->screenScrolled(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1, v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    const/high16 v5, 0x41480000    # 12.5f

    mul-float v3, v5, v4

    invoke-virtual {v0, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollX:I

    if-ltz v8, :cond_4

    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollX:I

    iget v9, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mMaxScrollX:I

    if-le v8, v9, :cond_b

    :cond_4
    iget v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollX:I

    if-gez v8, :cond_7

    move v2, v6

    :goto_2
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v8

    if-nez v8, :cond_8

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v8

    if-nez v2, :cond_5

    move v6, v7

    :cond_5
    invoke-virtual {v0, v8, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->setOverScrollAmount(FZ)V

    const/high16 v6, -0x3e400000    # -24.0f

    mul-float v3, v6, v4

    iget v6, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDensity:F

    sget v8, Lcom/cyanogenmod/trebuchet/Workspace;->CAMERA_DISTANCE:F

    mul-float/2addr v6, v8

    invoke-virtual {v0, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->setCameraDistance(F)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    if-nez v2, :cond_6

    const/high16 v5, 0x3f400000    # 0.75f

    :cond_6
    mul-float/2addr v5, v6

    invoke-virtual {v0, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotX(F)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v11

    invoke-virtual {v0, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotY(F)V

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    invoke-virtual {v0, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    invoke-virtual {v0, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->setOverscrollTransformsDirty(Z)V

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->setFadeForOverScroll(F)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v8

    add-int/lit8 v2, v8, -0x1

    goto :goto_2

    :cond_8
    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v8

    if-nez v2, :cond_9

    move v6, v7

    :cond_9
    invoke-virtual {v0, v8, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->setOverScrollAmount(FZ)V

    const/high16 v6, -0x3eb80000    # -12.5f

    mul-float v3, v6, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->overScrollBackgroundAlphaInterpolator(F)F

    move-result v6

    invoke-virtual {v0, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->setBackgroundAlphaMultiplier(F)V

    iput v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollPageIndex:I

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    if-nez v2, :cond_a

    const/high16 v5, 0x3f400000    # 0.75f

    :cond_a
    mul-float/2addr v5, v6

    invoke-virtual {v0, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotX(F)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v11

    invoke-virtual {v0, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotY(F)V

    invoke-virtual {v0, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->setOverscrollTransformsDirty(Z)V

    goto/16 :goto_0

    :cond_b
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v1, 0x0

    :goto_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v5

    if-lt v1, v5, :cond_f

    :cond_c
    iget v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverscrollFade:F

    cmpl-float v5, v5, v10

    if-eqz v5, :cond_d

    invoke-virtual {p0, v10}, Lcom/cyanogenmod/trebuchet/Workspace;->setFadeForOverScroll(F)V

    :cond_d
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v5

    if-nez v5, :cond_e

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->resetOverscrollTransforms()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->resetOverscrollTransforms()V

    :cond_e
    invoke-static {}, Lcom/cyanogenmod/trebuchet/Workspace;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$Workspace$TransitionEffect()[I

    move-result-object v5

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionEffect:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->ordinal()I

    move-result v8

    aget v5, v5, v8

    packed-switch v5, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledStandard(I)V

    goto/16 :goto_0

    :cond_f
    iget v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverScrollPageIndex:I

    if-eq v5, v1, :cond_10

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v0, :cond_10

    invoke-virtual {p0, p1, v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollProgress(ILandroid/view/View;I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Workspace;->backgroundAlphaInterpolator(F)F

    move-result v5

    invoke-virtual {v0, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->setBackgroundAlphaMultiplier(F)V

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledTablet(I)V

    goto/16 :goto_0

    :pswitch_2
    invoke-direct {p0, p1, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledZoom(IZ)V

    goto/16 :goto_0

    :pswitch_3
    invoke-direct {p0, p1, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledZoom(IZ)V

    goto/16 :goto_0

    :pswitch_4
    invoke-direct {p0, p1, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledRotate(IZ)V

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0, p1, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledRotate(IZ)V

    goto/16 :goto_0

    :pswitch_6
    invoke-direct {p0, p1, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledCube(IZ)V

    goto/16 :goto_0

    :pswitch_7
    invoke-direct {p0, p1, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledCube(IZ)V

    goto/16 :goto_0

    :pswitch_8
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->screenScrolledStack(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public scrollLeft()V
    .locals 2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v1, :cond_0

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollLeft()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->completeDragExit()V

    :cond_1
    return-void
.end method

.method public scrollRight()V
    .locals 2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v1, :cond_0

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollRight()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->completeDragExit()V

    :cond_1
    return-void
.end method

.method public scrollTo(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollTo(II)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->syncChildrenLayersEnabledOnVisiblePages()V

    return-void
.end method

.method public setBackgroundAlpha(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundAlpha:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mBackgroundAlpha:F

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    :cond_0
    return-void
.end method

.method public setChildrenOutlineAlpha(F)V
    .locals 3
    .param p1    # F

    iput p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineAlpha:F

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setBackgroundAlpha(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method setFadeForOverScroll(F)V
    .locals 8
    .param p1    # F

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isScrollingIndicatorEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mOverscrollFade:F

    sub-float v5, v7, p1

    mul-float/2addr v5, v6

    add-float v3, v6, v5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v5, 0x7f0d0034    # com.konka.avenger.R.id.qsb_divider

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v5, 0x7f0d0035    # com.konka.avenger.R.id.dock_divider

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollingIndicator()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->cancelScrollingIndicatorAnimations()V

    if-eqz v2, :cond_2

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowSearchBar:Z

    if-eqz v5, :cond_2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_2
    if-eqz v0, :cond_3

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowDockDivider:Z

    if-eqz v5, :cond_3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_3
    if-eqz v4, :cond_0

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mShowScrollingIndicator:Z

    if-eqz v5, :cond_0

    sub-float v5, v7, p1

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public setFinalScrollForPageChange(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollX()I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSavedScrollX:I

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTranslationX()F

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSavedTranslationX:F

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getRotationY()F

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSavedRotationY:F

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildOffset(I)I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->getRelativeChildOffset(I)I

    move-result v3

    sub-int v1, v2, v3

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setScrollX(I)V

    invoke-virtual {v0, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    invoke-virtual {v0, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    :cond_0
    return-void
.end method

.method public setFinalTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getScaleX()F

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentScaleX:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getScaleY()F

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentScaleY:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTranslationX()F

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentTranslationX:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTranslationY()F

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentTranslationY:F

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getRotationY()F

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentRotationY:F

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleXs:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleX(F)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleYs:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleY(F)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationXs:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationYs:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationY(F)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mNewRotationYs:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    :cond_0
    return-void
.end method

.method public setHorizontalWallpaperOffset(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->setFinalX(F)V

    return-void
.end method

.method public setVerticalWallpaperOffset(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->setFinalY(F)V

    return-void
.end method

.method protected setWallpaperDimension()V
    .locals 5

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v3

    if-eqz v3, :cond_0

    int-to-float v3, v1

    invoke-direct {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->wallpaperTravelToScreenWidthRatio(II)F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperWidth:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperHeight:I

    :goto_0
    new-instance v3, Lcom/cyanogenmod/trebuchet/Workspace$2;

    const-string v4, "setWallpaperDimension"

    invoke-direct {v3, p0, v4}, Lcom/cyanogenmod/trebuchet/Workspace$2;-><init>(Lcom/cyanogenmod/trebuchet/Workspace;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace$2;->start()V

    return-void

    :cond_0
    int-to-float v3, v2

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperWidth:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperHeight:I

    goto :goto_0
.end method

.method setup(Lcom/cyanogenmod/trebuchet/DragController;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/DragController;

    new-instance v0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mSpringLoadedDragController:Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->setWallpaperDimension()V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->centerWallpaperOffset()V

    :cond_0
    return-void
.end method

.method showOutlines()V
    .locals 4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsSwitchingState:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeOutAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_1
    const-string v0, "childrenOutlineAlpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mChildrenOutlineFadeInAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_2
    return-void
.end method

.method public showOutlinesTemporarily()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mIsPageMoving:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isTouchActive()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    :cond_0
    return-void
.end method

.method protected snapToPage(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->computeWallpaperScrollRatio(I)V

    :cond_0
    return-void
.end method

.method startDrag(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setPressed(Z)V

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1}, Landroid/graphics/Canvas;-><init>()V

    sget v0, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    invoke-direct {p0, v2, v1, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->createDragOutline(Landroid/view/View;Landroid/graphics/Canvas;I)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragOutline:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2, p0}, Lcom/cyanogenmod/trebuchet/Workspace;->beginDragShared(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;)V

    return-void
.end method

.method public syncPageItems(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    return-void
.end method

.method public syncPages()V
    .locals 0

    return-void
.end method

.method public transitionStateShouldAllowDrop()Z
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mTransitionProgress:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected updateCurrentPageScroll()V
    .locals 1

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->updateCurrentPageScroll()V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCurrentPage:I

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->computeWallpaperScrollRatio(I)V

    :cond_0
    return-void
.end method

.method updateShortcuts(Ljava/util/ArrayList;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Workspace;->getWorkspaceAndHotseatCellLayoutChildren()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_1

    return-void

    :cond_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v6

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v6, :cond_0

    invoke-virtual {v11, v10}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v13

    instance-of v15, v13, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-eqz v15, :cond_3

    move-object v8, v13

    check-cast v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v9, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v9}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v12

    iget v15, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->itemType:I

    if-nez v15, :cond_3

    const-string v15, "android.intent.action.MAIN"

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    if-eqz v12, :cond_3

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_4

    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v15, v4, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v15, v12}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    iget-object v0, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/cyanogenmod/trebuchet/IconCache;->getIcon(Landroid/content/Intent;)Landroid/graphics/Bitmap;

    move-result-object v15

    invoke-virtual {v8, v15}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->setIcon(Landroid/graphics/Bitmap;)V

    move-object v15, v14

    check-cast v15, Landroid/widget/TextView;

    const/16 v18, 0x0

    new-instance v19, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->getIcon(Lcom/cyanogenmod/trebuchet/IconCache;)Landroid/graphics/Bitmap;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public updateWallpaperOffsetImmediately()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mUpdateWallpaperOffsetImmediately:Z

    return-void
.end method

.method willAddToExistingUserFolder(Ljava/lang/Object;Lcom/cyanogenmod/trebuchet/CellLayout;[I)Z
    .locals 6
    .param p1    # Ljava/lang/Object;
    .param p2    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p3    # [I

    const/4 v2, 0x1

    const/4 v3, 0x0

    aget v4, p3, v3

    aget v5, p3, v2

    invoke-virtual {p2, v4, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v0

    instance-of v4, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v4, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-virtual {v1, p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->acceptDrop(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method willCreateUserFolder(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;[IZ)Z
    .locals 9
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p3    # [I
    .param p4    # Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    aget v7, p3, v6

    aget v8, p3, v5

    invoke-virtual {p2, v7, v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v7, v7, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Workspace;->getParentCellLayoutForView(Landroid/view/View;)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v1

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v7, v7, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellX:I

    aget v8, p3, v6

    if-ne v7, v8, :cond_2

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mDragInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget v7, v7, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellY:I

    aget v8, p3, v5

    if-ne v7, v8, :cond_2

    if-ne v1, p2, :cond_2

    move v3, v5

    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    if-eqz p4, :cond_3

    iget-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Workspace;->mCreateUserFolderOnDrop:Z

    if-nez v7, :cond_3

    :cond_1
    :goto_1
    return v6

    :cond_2
    move v3, v6

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    instance-of v0, v7, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget v7, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    if-eqz v7, :cond_4

    iget v7, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    if-eq v7, v5, :cond_4

    move v4, v6

    :goto_2
    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    move v6, v5

    goto :goto_1

    :cond_4
    move v4, v5

    goto :goto_2
.end method
