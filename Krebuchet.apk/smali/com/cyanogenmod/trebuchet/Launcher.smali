.class public final Lcom/cyanogenmod/trebuchet/Launcher;
.super Landroid/app/Activity;
.source "Launcher.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/Launcher$AppWidgetResetObserver;,
        Lcom/cyanogenmod/trebuchet/Launcher$CloseSystemDialogsIntentReceiver;,
        Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;,
        Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;,
        Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;,
        Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;,
        Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;,
        Lcom/cyanogenmod/trebuchet/Launcher$State;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$SortMode:[I = null

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I = null

.field static final APPWIDGET_HOST_ID:I = 0x400

.field static final DEBUG_WIDGETS:Z = false

.field static final DEFAULT_SCREEN:I = 0x1

.field static final DIALOG_CREATE_SHORTCUT:I = 0x1

.field static final DIALOG_RENAME_FOLDER:I = 0x2

.field private static final DISMISS_CLING_DURATION:I = 0xfa

.field private static final EXIT_SPRINGLOADED_MODE_LONG_TIMEOUT:I = 0x258

.field private static final EXIT_SPRINGLOADED_MODE_SHORT_TIMEOUT:I = 0x12c

.field static final EXTRA_SHORTCUT_DUPLICATE:Ljava/lang/String; = "duplicate"

.field static final HOME_SCREEN:I = 0x1

.field static final LOGD:Z = false

.field static final MAX_SCREEN_COUNT:I = 0x7

.field private static final MENU_GROUP_MARKET:I = 0x2

.field private static final MENU_GROUP_WALLPAPER:I = 0x1

.field private static final MENU_HELP:I = 0x7

.field private static final MENU_MANAGE_APPS:I = 0x3

.field private static final MENU_MARKET:I = 0x4

.field private static final MENU_PREFERENCES:I = 0x5

.field private static final MENU_SYSTEM_SETTINGS:I = 0x6

.field private static final MENU_WALLPAPER_SETTINGS:I = 0x2

.field private static final PREFERENCES:Ljava/lang/String; = "launcher.preferences"

.field static final PROFILE_STARTUP:Z = false

.field private static final REQUEST_CREATE_APPWIDGET:I = 0x5

.field private static final REQUEST_CREATE_SHORTCUT:I = 0x1

.field private static final REQUEST_PICK_APPLICATION:I = 0x6

.field private static final REQUEST_PICK_APPWIDGET:I = 0x9

.field private static final REQUEST_PICK_SHORTCUT:I = 0x7

.field private static final REQUEST_PICK_WALLPAPER:I = 0xa

.field private static final RUNTIME_STATE:Ljava/lang/String; = "launcher.state"

.field private static final RUNTIME_STATE_CURRENT_SCREEN:Ljava/lang/String; = "launcher.current_screen"

.field private static final RUNTIME_STATE_PENDING_ADD_CELL_X:Ljava/lang/String; = "launcher.add_cell_x"

.field private static final RUNTIME_STATE_PENDING_ADD_CELL_Y:Ljava/lang/String; = "launcher.add_cell_y"

.field private static final RUNTIME_STATE_PENDING_ADD_CONTAINER:Ljava/lang/String; = "launcher.add_container"

.field private static final RUNTIME_STATE_PENDING_ADD_SCREEN:Ljava/lang/String; = "launcher.add_screen"

.field private static final RUNTIME_STATE_PENDING_FOLDER_RENAME:Ljava/lang/String; = "launcher.rename_folder"

.field private static final RUNTIME_STATE_PENDING_FOLDER_RENAME_ID:Ljava/lang/String; = "launcher.rename_folder_id"

.field private static final SHOW_CLING_DURATION:I = 0x226

.field static final TAG:Ljava/lang/String; = "Launcher"

.field public static final TIME_DELAY_MUTE_TV_VIDEO:I = 0x7d0

.field public static final TIME_DELAY_SHOW_TV_FROM_WORKSPACE:I = 0x12c

.field private static final TIME_FOR_TEAC:I = 0x1b7740

.field private static final TIP_ONE:Ljava/lang/String; = "Please select the type of use for this TV.\n[Home Use] optimizes the picture recommended for normal home use and is energy efficient.\n[Store Demo] optimises the picture for store display."

.field private static final TIP_TWO:Ljava/lang/String; = "We strongly recommend you select Home use. \nStore demo is intended only for store use. \nIf you choose Store demo, the picture settings will be reset every 30 minutes."

.field private static final TITLE:Ljava/lang/String; = "First Time Setup:"

.field private static final TOOLBAR_ICON_METADATA_NAME:Ljava/lang/String; = "com.android.launcher.toolbar_icon"

.field static folderState:I = 0x0

.field private static heightPixels:I = 0x0

.field private static isFinish:Z = false

.field public static final mTVWindowEnable:Z = true

.field static final sDumpLogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sFolders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/cyanogenmod/trebuchet/FolderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static sGlobalSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

.field private static sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

.field private static final sLock:Ljava/lang/Object;

.field private static sPendingAddList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;",
            ">;"
        }
    .end annotation
.end field

.field private static sScreen:I

.field private static sVoiceSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

.field private static widthPixels:I


# instance fields
.field private final ADVANCE_MSG:I

.field private final MSG_STR_OK:I

.field private final TV_WINDOW_PADDING:I

.field alertDialogF:Landroid/app/AlertDialog$Builder;

.field private final mAdvanceInterval:I

.field private final mAdvanceStagger:I

.field private mAllAppsButton:Landroid/view/View;

.field private mAppMarketIntent:Landroid/content/Intent;

.field private mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

.field private mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

.field private mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

.field private mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

.field private mAttached:Z

.field private mAutoAdvanceRunning:Z

.field private mAutoAdvanceSentTime:J

.field private mAutoAdvanceTimeLeft:J

.field private mAutoRotate:Z

.field private final mBuildLayersRunnable:Ljava/lang/Runnable;

.field private final mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

.field private mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private mConnectivityServiceBinder:Lcom/cyanogenmod/trebuchet/ConnectivityService;

.field private mConnectivityServiceConnection:Landroid/content/ServiceConnection;

.field private mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

.field private mDividerAnimator:Landroid/animation/AnimatorSet;

.field private mDockDivider:Landroid/view/View;

.field private mDragController:Lcom/cyanogenmod/trebuchet/DragController;

.field private mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

.field private mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

.field private final mHandler:Landroid/os/Handler;

.field private mHideIconLabels:Z

.field private mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

.field private mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

.field private mNextPageBtn:Landroid/view/View;

.field private mOnResumeNeedsLoad:Z

.field private mPaused:Z

.field private final mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

.field private mPrevPageBtn:Landroid/view/View;

.field private mQsbDivider:Landroid/view/View;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field mRegisterTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mRestoreScreenOrientationDelay:I

.field private mRestoring:Z

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSavedState:Landroid/os/Bundle;

.field private mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

.field private mShowDockDivider:Z

.field private mShowSearchBar:Z

.field private mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

.field private mStateAnimation:Landroid/animation/AnimatorSet;

.field private mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

.field private final mTmpAddItemCellCoordinates:[I

.field private mUserPresent:Z

.field private mVisible:Z

.field private mWaitingForResult:Z

.field private mWaitingForResume:Lcom/cyanogenmod/trebuchet/BubbleTextView;

.field private final mWidgetObserver:Landroid/database/ContentObserver;

.field private mWidgetsToAdvance:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

.field private mWorkspaceLoading:Z

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private tvView:Lcom/konka/avenger/tv/TVView;

.field private wdigetThread:Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;


# direct methods
.method static synthetic $SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$SortMode()[I
    .locals 3

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$SortMode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->values()[Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->KKDefault:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$SortMode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I
    .locals 3

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_d
    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_d

    :catch_1
    move-exception v1

    goto :goto_c

    :catch_2
    move-exception v1

    goto :goto_b

    :catch_3
    move-exception v1

    goto :goto_a

    :catch_4
    move-exception v1

    goto :goto_9

    :catch_5
    move-exception v1

    goto :goto_8

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_6

    :catch_8
    move-exception v1

    goto :goto_5

    :catch_9
    move-exception v1

    goto :goto_4

    :catch_a
    move-exception v1

    goto :goto_3

    :catch_b
    move-exception v1

    goto/16 :goto_2

    :catch_c
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    sput v1, Lcom/cyanogenmod/trebuchet/Launcher;->folderState:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sLock:Ljava/lang/Object;

    sput v1, Lcom/cyanogenmod/trebuchet/Launcher;->sScreen:I

    const/4 v0, 0x0

    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    new-array v0, v2, [Landroid/graphics/drawable/Drawable$ConstantState;

    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sGlobalSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    new-array v0, v2, [Landroid/graphics/drawable/Drawable$ConstantState;

    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sVoiceSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sDumpLogs:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sPendingAddList:Ljava/util/ArrayList;

    sput-boolean v1, Lcom/cyanogenmod/trebuchet/Launcher;->isFinish:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$CloseSystemDialogsIntentReceiver;

    invoke-direct {v0, p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher$CloseSystemDialogsIntentReceiver;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Launcher$CloseSystemDialogsIntentReceiver;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$AppWidgetResetObserver;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/Launcher$AppWidgetResetObserver;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/ItemInfo;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    new-array v0, v4, [I

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTmpAddItemCellCoordinates:[I

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceRunning:Z

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPaused:Z

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mUserPresent:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mVisible:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAttached:Z

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppMarketIntent:Landroid/content/Intent;

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->ADVANCE_MSG:I

    const/16 v0, 0x4e20

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAdvanceInterval:I

    const/16 v0, 0xfa

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAdvanceStagger:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceTimeLeft:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoreScreenOrientationDelay:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->TV_WINDOW_PADDING:I

    iput v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->MSG_STR_OK:I

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/Launcher$1;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mBuildLayersRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$2;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/Launcher$2;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$3;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/Launcher$3;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private SetPropertyForSTR(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "Launcher"

    const-string v2, "Unable to find IWindowManger interface."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "mstar.str.suspending"

    invoke-static {v1, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private acceptFilter()Z
    .locals 2

    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/Workspace;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/Launcher;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mUserPresent:Z

    return-void
.end method

.method static synthetic access$10(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setupTVWindow()V

    return-void
.end method

.method static synthetic access$11(Lcom/cyanogenmod/trebuchet/Launcher;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    return-void
.end method

.method static synthetic access$12()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$13(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/FolderInfo;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    return-object v0
.end method

.method static synthetic access$14(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/FolderInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    return-void
.end method

.method static synthetic access$15(Lcom/cyanogenmod/trebuchet/Launcher;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    return v0
.end method

.method static synthetic access$16(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/LauncherModel;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    return-object v0
.end method

.method static synthetic access$17(Lcom/cyanogenmod/trebuchet/Launcher;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    return-void
.end method

.method static synthetic access$18(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->startWallpaper()V

    return-void
.end method

.method static synthetic access$19(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->onAppWidgetReset()V

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/DragLayer;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    return-object v0
.end method

.method static synthetic access$20(Z)V
    .locals 0

    sput-boolean p0, Lcom/cyanogenmod/trebuchet/Launcher;->isFinish:Z

    return-void
.end method

.method static synthetic access$21(Lcom/cyanogenmod/trebuchet/Launcher;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$22(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->readConfiguration(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V

    return-void
.end method

.method static synthetic access$23(Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V
    .locals 0

    sput-object p0, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    return-void
.end method

.method static synthetic access$24(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->checkForLocaleChange()V

    return-void
.end method

.method static synthetic access$25(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->writeConfiguration(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V

    return-void
.end method

.method static synthetic access$26(Lcom/cyanogenmod/trebuchet/Launcher;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mBuildLayersRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$27(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    return-object v0
.end method

.method static synthetic access$28(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/ConnectivityService;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceBinder:Lcom/cyanogenmod/trebuchet/ConnectivityService;

    return-void
.end method

.method static synthetic access$29(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/ConnectivityService;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceBinder:Lcom/cyanogenmod/trebuchet/ConnectivityService;

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V

    return-void
.end method

.method static synthetic access$30(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    return-object v0
.end method

.method static synthetic access$32(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeView;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    return-object v0
.end method

.method static synthetic access$33(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->syncOrientation()V

    return-void
.end method

.method static synthetic access$35(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->alertDialogTwo()V

    return-void
.end method

.method static synthetic access$36(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setToHomeMode()V

    return-void
.end method

.method static synthetic access$37(Lcom/cyanogenmod/trebuchet/Launcher;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->setTvModeVal(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$38(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->threadToProcessPictureMode()V

    return-void
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    return-object v0
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/ItemInfo;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    return-object v0
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/Launcher;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$7(Lcom/cyanogenmod/trebuchet/Launcher;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->sendAdvanceMessage(J)V

    return-void
.end method

.method static synthetic access$8(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->SetPropertyForSTR(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$9(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/konka/avenger/tv/TVWindowManager;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    return-object v0
.end method

.method private alertDialogTwo()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "We strongly recommend you select Home use. \nStore demo is intended only for store use. \nIf you choose Store demo, the picture settings will be reset every 30 minutes."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Yes"

    new-instance v3, Lcom/cyanogenmod/trebuchet/Launcher$30;

    invoke-direct {v3, p0}, Lcom/cyanogenmod/trebuchet/Launcher$30;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "No"

    new-instance v3, Lcom/cyanogenmod/trebuchet/Launcher$31;

    invoke-direct {v3, p0}, Lcom/cyanogenmod/trebuchet/Launcher$31;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private bindConnectivityService()V
    .locals 3

    new-instance v1, Lcom/cyanogenmod/trebuchet/Launcher$12;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/Launcher$12;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceConnection:Landroid/content/ServiceConnection;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/cyanogenmod/trebuchet/ConnectivityService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private checkForLocaleChange()V
    .locals 11

    const/4 v2, 0x0

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    if-nez v9, :cond_1

    new-instance v9, Lcom/cyanogenmod/trebuchet/Launcher$4;

    invoke-direct {v9, p0}, Lcom/cyanogenmod/trebuchet/Launcher$4;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    new-array v10, v2, [Ljava/lang/Void;

    invoke-virtual {v9, v10}, Lcom/cyanogenmod/trebuchet/Launcher$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    iget-object v6, v9, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->locale:Ljava/lang/String;

    iget-object v9, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    iget v7, v9, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mcc:I

    iget v4, v0, Landroid/content/res/Configuration;->mcc:I

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    iget v8, v9, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mnc:I

    iget v5, v0, Landroid/content/res/Configuration;->mnc:I

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    if-ne v4, v7, :cond_2

    if-ne v5, v8, :cond_2

    :goto_1
    if-eqz v2, :cond_0

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    iput-object v1, v9, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->locale:Ljava/lang/String;

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    iput v4, v9, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mcc:I

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    iput v5, v9, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mnc:I

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {v9}, Lcom/cyanogenmod/trebuchet/IconCache;->flush()V

    sget-object v3, Lcom/cyanogenmod/trebuchet/Launcher;->sLocaleConfiguration:Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    new-instance v9, Lcom/cyanogenmod/trebuchet/Launcher$5;

    const-string v10, "WriteLocaleConfiguration"

    invoke-direct {v9, p0, v10, v3}, Lcom/cyanogenmod/trebuchet/Launcher$5;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/lang/String;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V

    invoke-virtual {v9}, Lcom/cyanogenmod/trebuchet/Launcher$5;->start()V

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private checkInputSource()V
    .locals 5

    const-string v2, "activity"

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "the top activity=========="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current screen:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",current state:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.konka.avenger"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.konka.tvsettings"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const-string v2, "Launcher"

    const-string v3, "========checkInputSource,try to switch source"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->tvView:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v2}, Lcom/konka/avenger/tv/TVView;->setStorageInput()V

    :cond_0
    return-void
.end method

.method private checkSystemAutoTime()V
    .locals 6

    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    :try_start_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_time"

    invoke-static {v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    :goto_0
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v4, :cond_0

    if-ne v2, v5, :cond_0

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_time"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v3, "Launcher"

    const-string v4, "=======check set auto time to 1"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method private clearTypedText()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    return-void
.end method

.method private completeAdd(Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;)Z
    .locals 9
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;

    const/4 v8, 0x0

    iget v0, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->requestCode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->resetAddInfo()V

    return v8

    :pswitch_1
    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->intent:Landroid/content/Intent;

    iget-wide v2, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->container:J

    iget v4, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->screen:I

    iget v5, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->cellX:I

    iget v6, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->cellY:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Launcher;->completeAddApplication(Landroid/content/Intent;JIII)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->processShortcut(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->intent:Landroid/content/Intent;

    iget-wide v2, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->container:J

    iget v4, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->screen:I

    iget v5, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->cellX:I

    iget v6, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->cellY:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Launcher;->completeAddShortcut(Landroid/content/Intent;JIII)V

    const/4 v8, 0x1

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->addAppWidgetFromPick(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->intent:Landroid/content/Intent;

    const-string v1, "appWidgetId"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iget-wide v0, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->container:J

    iget v2, p1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->screen:I

    invoke-direct {p0, v7, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->completeAddAppWidget(IJI)V

    const/4 v8, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private completeAddAppWidget(IJI)V
    .locals 27
    .param p1    # I
    .param p2    # J
    .param p4    # I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isDefaultHomeScreen(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showNotAllowedAddMessage()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getCellLayout(JI)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/appwidget/AppWidgetProviderInfo;[I)[I

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mTmpAddItemCellCoordinates:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v0, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->dropPos:[I

    move-object/from16 v26, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    if-ltz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v5, v5, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    if-ltz v5, :cond_3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v6, v6, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    aput v6, v9, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v6, v6, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    aput v6, v9, v5

    const/16 v23, 0x1

    :goto_1
    if-nez v23, :cond_6

    const/4 v5, -0x1

    move/from16 v0, p1

    if-eq v0, v5, :cond_2

    new-instance v5, Lcom/cyanogenmod/trebuchet/Launcher$10;

    const-string v6, "deleteAppWidgetId"

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v5, v0, v6, v1}, Lcom/cyanogenmod/trebuchet/Launcher$10;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/lang/String;I)V

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher$10;->start()V

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    goto :goto_0

    :cond_3
    if-eqz v26, :cond_5

    const/4 v5, 0x0

    aget v5, v26, v5

    const/4 v6, 0x1

    aget v6, v26, v6

    const/4 v7, 0x0

    aget v7, v25, v7

    const/4 v8, 0x1

    aget v8, v25, v8

    invoke-virtual/range {v4 .. v9}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestVacantArea(IIII[I)[I

    move-result-object v24

    if-eqz v24, :cond_4

    const/16 v23, 0x1

    :goto_2
    goto :goto_1

    :cond_4
    const/16 v23, 0x0

    goto :goto_2

    :cond_5
    const/4 v5, 0x0

    aget v5, v25, v5

    const/4 v6, 0x1

    aget v6, v25, v6

    invoke-virtual {v4, v9, v5, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v23

    goto :goto_1

    :cond_6
    new-instance v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    move/from16 v0, p1

    invoke-direct {v11, v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;-><init>(I)V

    const/4 v5, 0x0

    aget v5, v25, v5

    iput v5, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanX:I

    const/4 v5, 0x1

    aget v5, v25, v5

    iput v5, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanY:I

    const/4 v5, 0x0

    aget v15, v9, v5

    const/4 v5, 0x1

    aget v16, v9, v5

    const/16 v17, 0x0

    move-object/from16 v10, p0

    move-wide/from16 v12, p2

    move/from16 v14, p4

    invoke-static/range {v10 .. v17}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIIIZ)V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v22

    invoke-virtual {v5, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v5

    iput-object v5, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    iget-object v5, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    move/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    iget-object v5, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v5, v11}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v13, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    const/4 v5, 0x0

    aget v17, v9, v5

    const/4 v5, 0x1

    aget v18, v9, v5

    iget v0, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanX:I

    move/from16 v19, v0

    iget v0, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanY:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceLocked()Z

    move-result v21

    move-wide/from16 v14, p2

    move/from16 v16, p4

    invoke-virtual/range {v12 .. v21}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    iget-object v5, v11, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v5, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->addWidgetToAutoAdvanceIfNeeded(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V

    goto/16 :goto_0
.end method

.method private completeAddShortcut(Landroid/content/Intent;JIII)V
    .locals 25
    .param p1    # Landroid/content/Intent;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mTmpAddItemCellCoordinates:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v0, v4, Lcom/cyanogenmod/trebuchet/ItemInfo;->dropPos:[I

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getCellLayout(JI)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v8

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isDefaultHomeScreen(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showNotAllowedAddMessage()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v4, v0, v1, v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->infoFromShortcutIntent(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v22

    if-eqz v22, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->createShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;

    move-result-object v5

    if-ltz p5, :cond_2

    if-ltz p6, :cond_2

    const/4 v4, 0x0

    aput p5, v9, v4

    const/4 v4, 0x1

    aput p6, v9, v4

    const/16 v21, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-wide/from16 v6, p2

    invoke-virtual/range {v4 .. v12}, Lcom/cyanogenmod/trebuchet/Workspace;->createUserFolderIfNecessary(Landroid/view/View;JLcom/cyanogenmod/trebuchet/CellLayout;[IZLcom/cyanogenmod/trebuchet/DragView;Ljava/lang/Runnable;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-direct {v10}, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;-><init>()V

    move-object/from16 v0, v22

    iput-object v0, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v11, 0x1

    move-object v7, v5

    invoke-virtual/range {v6 .. v11}, Lcom/cyanogenmod/trebuchet/Workspace;->addToExistingFolderIfNecessary(Landroid/view/View;Lcom/cyanogenmod/trebuchet/CellLayout;[ILcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)Z

    move-result v4

    if-nez v4, :cond_0

    :goto_1
    if-nez v21, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    goto :goto_0

    :cond_2
    if-eqz v24, :cond_4

    const/4 v4, 0x0

    aget v12, v24, v4

    const/4 v4, 0x1

    aget v13, v24, v4

    const/4 v14, 0x1

    const/4 v15, 0x1

    move-object v11, v8

    move-object/from16 v16, v9

    invoke-virtual/range {v11 .. v16}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestVacantArea(IIII[I)[I

    move-result-object v23

    if-eqz v23, :cond_3

    const/16 v21, 0x1

    :goto_2
    goto :goto_1

    :cond_3
    const/16 v21, 0x0

    goto :goto_2

    :cond_4
    const/4 v4, 0x1

    const/4 v6, 0x1

    invoke-virtual {v8, v9, v4, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v21

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    aget v16, v9, v4

    const/4 v4, 0x1

    aget v17, v9, v4

    const/16 v18, 0x0

    move-object/from16 v11, p0

    move-object/from16 v12, v22

    move-wide/from16 v13, p2

    move/from16 v15, p4

    invoke-static/range {v11 .. v18}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIIIZ)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v4, 0x0

    aget v16, v9, v4

    const/4 v4, 0x1

    aget v17, v9, v4

    const/16 v18, 0x1

    const/16 v19, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceLocked()Z

    move-result v20

    move-object v12, v5

    move-wide/from16 v13, p2

    move/from16 v15, p4

    invoke-virtual/range {v11 .. v20}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    goto/16 :goto_0
.end method

.method private dismissCling(Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;I)V
    .locals 5
    .param p1    # Lcom/cyanogenmod/trebuchet/Cling;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Cling;->dismiss()V

    const-string v1, "alpha"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    int-to-long v1, p3

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/cyanogenmod/trebuchet/Launcher$25;

    invoke-direct {v1, p0, p1, p2}, Lcom/cyanogenmod/trebuchet/Launcher$25;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void
.end method

.method private getCurInputsoureFromDB()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 8

    const/4 v2, 0x0

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    const-string v2, "enInputSourceType"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v7, v1, v2

    :cond_0
    return-object v7
.end method

.method private getCurrentOrientationIndexForGlobalIcons()I
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentOrientation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private getExternalPackageToolbarIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p1    # Landroid/content/ComponentName;

    :try_start_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/16 v6, 0x80

    invoke-virtual {v4, p1, v6}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v6

    iget-object v2, v6, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    const-string v6, "com.android.launcher.toolbar_icon"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v4, p1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    :goto_0
    return-object v6

    :catch_0
    move-exception v0

    const-string v6, "Launcher"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to load toolbar icon; "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " not found"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    const/4 v6, 0x0

    goto :goto_0

    :catch_1
    move-exception v3

    const-string v6, "Launcher"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to load toolbar icon from "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static getFolderState()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/Launcher;->folderState:I

    return v0
.end method

.method public static getHeightPixels()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/Launcher;->heightPixels:I

    return v0
.end method

.method private getMicStatusFromDB()I
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "******In db mic status is :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "bMicOn"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "bMicOn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const-string v1, "Launcher"

    const-string v2, "Fetch db fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getRealTVWindowRect()Landroid/graphics/Rect;
    .locals 15

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getTVWindowRect()Landroid/graphics/Rect;

    move-result-object v7

    const-string v8, "Launcher"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "getRealTVWindowRect after , x ="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",y = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",widht = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",height = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, v7, Landroid/graphics/Rect;->left:I

    if-ltz v8, :cond_0

    iget v8, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v11

    add-int/2addr v8, v11

    const/16 v11, 0x780

    if-le v8, v11, :cond_1

    :cond_0
    const-string v8, "Launcher"

    const-string v11, "For test force value left,rect.right 1203,1203 + 654"

    invoke-static {v8, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v8, 0x4b3

    iput v8, v7, Landroid/graphics/Rect;->left:I

    const/16 v8, 0x741

    iput v8, v7, Landroid/graphics/Rect;->right:I

    :cond_1
    iget v8, v7, Landroid/graphics/Rect;->top:I

    if-ltz v8, :cond_2

    iget v8, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v11

    add-int/2addr v8, v11

    const/16 v11, 0x438

    if-le v8, v11, :cond_3

    :cond_2
    const-string v8, "Launcher"

    const-string v11, "For test force value rect.top,rect.bottom 108,108 + 426"

    invoke-static {v8, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v8, 0x6c

    iput v8, v7, Landroid/graphics/Rect;->top:I

    const/16 v8, 0x216

    iput v8, v7, Landroid/graphics/Rect;->bottom:I

    :cond_3
    const/4 v6, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :cond_4
    :goto_0
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v8, v6, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    int-to-double v11, v8

    iget v8, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v13, v8

    div-double v3, v11, v13

    const-string v8, "Launcher"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "panelProperty.width = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v6, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",dm.widthPixels = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",fResolutionRatio = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",dm.density = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v8, 0x40c00000    # 6.0f

    iget v11, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v11

    float-to-int v5, v8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget v11, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-interface {v0, v8}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_6

    move v8, v9

    :goto_1
    sub-int v8, v5, v8

    add-int/2addr v8, v11

    iput v8, v7, Landroid/graphics/Rect;->left:I

    iget v8, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v8, v5

    iput v8, v7, Landroid/graphics/Rect;->top:I

    iget v8, v7, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-interface {v0, v11}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_7

    :goto_2
    sub-int v9, v5, v9

    sub-int/2addr v8, v9

    iput v8, v7, Landroid/graphics/Rect;->right:I

    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, v5

    iput v8, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-interface {v0, v8}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_5

    iget v8, v7, Landroid/graphics/Rect;->left:I

    int-to-double v8, v8

    mul-double/2addr v8, v3

    double-to-int v8, v8

    iput v8, v7, Landroid/graphics/Rect;->left:I

    iget v8, v7, Landroid/graphics/Rect;->top:I

    int-to-double v8, v8

    mul-double/2addr v8, v3

    double-to-int v8, v8

    iput v8, v7, Landroid/graphics/Rect;->top:I

    iget v8, v7, Landroid/graphics/Rect;->right:I

    int-to-double v8, v8

    mul-double/2addr v8, v3

    double-to-int v8, v8

    iput v8, v7, Landroid/graphics/Rect;->right:I

    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    int-to-double v8, v8

    mul-double/2addr v8, v3

    double-to-int v8, v8

    iput v8, v7, Landroid/graphics/Rect;->bottom:I

    :cond_5
    const-string v8, "Launcher"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getRealTVWindowRect before , x ="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",right="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",y = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",widht = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",height = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v7

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_6
    move v8, v10

    goto/16 :goto_1

    :cond_7
    move v9, v10

    goto/16 :goto_2
.end method

.method static getScreen()I
    .locals 2

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher;->sLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/cyanogenmod/trebuchet/Launcher;->sScreen:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getTvModeVal()I
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    iget v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ModeVal:I

    return v1
.end method

.method private getTypedText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWidthPixels()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/Launcher;->widthPixels:I

    return v0
.end method

.method private getWindowDPI()V
    .locals 2

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    sput v1, Lcom/cyanogenmod/trebuchet/Launcher;->widthPixels:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sput v1, Lcom/cyanogenmod/trebuchet/Launcher;->heightPixels:I

    return-void
.end method

.method private growAndFadeOutFolderIcon(Lcom/cyanogenmod/trebuchet/FolderIcon;)V
    .locals 13
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderIcon;

    const/high16 v10, 0x3fc00000    # 1.5f

    const/4 v12, 0x1

    const/4 v11, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v7, "alpha"

    new-array v8, v12, [F

    const/4 v9, 0x0

    aput v9, v8, v11

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    const-string v7, "scaleX"

    new-array v8, v12, [F

    aput v10, v8, v11

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    const-string v7, "scaleY"

    new-array v8, v12, [F

    aput v10, v8, v11

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v7, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->container:J

    const-wide/16 v9, -0x65

    cmp-long v7, v7, v9

    if-nez v7, :cond_1

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    iget v7, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v8, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    invoke-virtual {v1, v7, v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->setFolderLeaveBehindCell(II)V

    :cond_1
    const/4 v7, 0x3

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v7, v11

    aput-object v5, v7, v12

    const/4 v8, 0x2

    aput-object v6, v7, v8

    invoke-static {p1, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b001b    # com.konka.avenger.R.integer.config_folderAnimDuration

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v4, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private handleFolderClick(Lcom/cyanogenmod/trebuchet/FolderIcon;[I)V
    .locals 7
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderIcon;
    .param p2    # [I

    const/4 v6, 0x0

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getFolderForTag(Ljava/lang/Object;)Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v2

    iget-boolean v3, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    if-eqz v3, :cond_0

    if-nez v2, :cond_0

    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Folder info marked as open, but associated folder is not open. Screen: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->screen:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellX:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellY:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v6, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    :cond_0
    iget-boolean v3, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    iget-object v3, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ltz v3, :cond_2

    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->openFolder(Lcom/cyanogenmod/trebuchet/FolderIcon;[I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v3, "\u76ee\u5f55\u6682\u65e0\u5185\u5bb9\uff0c\u8bf7\u5c06\u5e94\u7528\u62d6\u5165\u76ee\u5f55\u3002"

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageForView(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder(Lcom/cyanogenmod/trebuchet/Folder;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v3

    if-eq v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->openFolder(Lcom/cyanogenmod/trebuchet/FolderIcon;[I)V

    goto :goto_0
.end method

.method private hideAppsCustomizeHelper(ZZ)V
    .locals 13
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    move-object v8, p0

    const v0, 0x7f0b000c    # com.konka.avenger.R.integer.config_appsCustomizeZoomOutTime

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    const v0, 0x7f0b000d    # com.konka.avenger.R.integer.config_appsCustomizeZoomScaleFactor

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v4, v0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-direct {p0, v2, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->setPivotsForZoom(Landroid/view/View;F)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateWallpaperVisibility(Z)V

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->showHotseat(Z)V

    if-eqz p1, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getScaleX()F

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getScaleY()F

    move-result v5

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    int-to-long v11, v7

    invoke-virtual {v0, v11, v12}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v10

    new-instance v0, Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/Workspace$ZoomInInterpolator;-><init>()V

    invoke-virtual {v10, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$18;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/Launcher$18;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;FFF)V

    invoke-virtual {v10, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v6

    const v0, 0x7f0b000f    # com.konka.avenger.R.integer.config_appsCustomizeFadeOutTime

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v6, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$19;

    invoke-direct {v0, p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher$19;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    instance-of v0, v2, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    if-eqz v0, :cond_1

    move-object v0, v2

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    const/4 v1, 0x1

    invoke-interface {v0, v8, v6, v1}, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;->onLauncherTransitionStart(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)Z

    :cond_1
    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$20;

    invoke-direct {v0, p0, v2, v8, v6}, Lcom/cyanogenmod/trebuchet/Launcher$20;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v11, 0x0

    aput-object v10, v1, v11

    const/4 v11, 0x1

    aput-object v6, v1, v11

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :goto_0
    return-void

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    instance-of v0, v2, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    if-eqz v0, :cond_3

    move-object v0, v2

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    const/4 v1, 0x0

    const/4 v11, 0x1

    invoke-interface {v0, v8, v1, v11}, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;->onLauncherTransitionStart(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)Z

    check-cast v2, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {v2, v8, v0, v1}, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;->onLauncherTransitionEnd(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)V

    :cond_3
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->hideScrollingIndicator(Z)V

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private initCling(I[IZI)Lcom/cyanogenmod/trebuchet/Cling;
    .locals 4
    .param p1    # I
    .param p2    # [I
    .param p3    # Z
    .param p4    # I

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Cling;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0, p2}, Lcom/cyanogenmod/trebuchet/Cling;->init(Lcom/cyanogenmod/trebuchet/Launcher;[I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Cling;->setVisibility(I)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Cling;->setLayerType(ILandroid/graphics/Paint;)V

    if-eqz p3, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Cling;->buildLayer()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Cling;->setAlpha(F)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Cling;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x226

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, p4

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/Cling;->setAlpha(F)V

    goto :goto_0
.end method

.method private static intToState(I)Lcom/cyanogenmod/trebuchet/Launcher$State;
    .locals 4
    .param p0    # I

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher$State;->values()[Lcom/cyanogenmod/trebuchet/Launcher$State;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_0

    :goto_1
    return-object v1

    :cond_0
    aget-object v3, v2, v0

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher$State;->ordinal()I

    move-result v3

    if-ne v3, p0, :cond_1

    aget-object v1, v2, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private invalidatePressedFocusedStates(Landroid/view/View;Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;

    instance-of v2, p1, Lcom/cyanogenmod/trebuchet/HolographicLinearLayout;

    if-eqz v2, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/cyanogenmod/trebuchet/HolographicLinearLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/HolographicLinearLayout;->invalidatePressedFocusedStates()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v2, p2, Lcom/cyanogenmod/trebuchet/HolographicImageView;

    if-eqz v2, :cond_0

    move-object v1, p2

    check-cast v1, Lcom/cyanogenmod/trebuchet/HolographicImageView;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/HolographicImageView;->invalidatePressedFocusedStates()V

    goto :goto_0
.end method

.method private isClingsEnabled()Z
    .locals 2

    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090004    # com.konka.avenger.R.bool.config_clingEnable

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_0
.end method

.method private isGuideEnabled()Z
    .locals 2

    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090005    # com.konka.avenger.R.bool.config_guideEnable

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_0
.end method

.method private isLaunchBySTR()Z
    .locals 4

    const-string v1, "mstar.str.suspending"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mstar.str.suspending===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private mapConfigurationOriActivityInfoOri(I)I
    .locals 7
    .param p1    # I

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_0
    const/4 v6, 0x4

    new-array v3, v6, [I

    const/4 v6, 0x0

    aput v4, v3, v6

    const/16 v4, 0x9

    aput v4, v3, v5

    const/4 v4, 0x3

    const/16 v6, 0x8

    aput v6, v3, v4

    const/4 v1, 0x0

    if-ne v2, v5, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v4

    add-int/2addr v4, v1

    rem-int/lit8 v4, v4, 0x4

    aget v4, v3, v4

    return v4

    :pswitch_0
    move v2, p1

    goto :goto_0

    :pswitch_1
    if-ne p1, v5, :cond_1

    move v2, v4

    :goto_1
    goto :goto_0

    :cond_1
    move v2, v5

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onAppWidgetReset()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->startListening()V

    :cond_0
    return-void
.end method

.method private static readConfiguration(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    const-string v2, "launcher.preferences"

    invoke-virtual {p0, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->locale:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p1, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mcc:I

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p1, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mnc:I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    move-object v0, v1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v2

    :goto_1
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0

    :catch_2
    move-exception v2

    :goto_2
    if-eqz v0, :cond_0

    :try_start_4
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0

    :catch_3
    move-exception v2

    goto :goto_0

    :catchall_0
    move-exception v2

    :goto_3
    if-eqz v0, :cond_1

    :try_start_5
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_1
    :goto_4
    throw v2

    :catch_4
    move-exception v2

    move-object v0, v1

    goto :goto_0

    :catch_5
    move-exception v3

    goto :goto_4

    :catchall_1
    move-exception v2

    move-object v0, v1

    goto :goto_3

    :catch_6
    move-exception v2

    move-object v0, v1

    goto :goto_2

    :catch_7
    move-exception v2

    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private registerContentObservers()V
    .locals 4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherProvider;->CONTENT_APPWIDGET_RESET_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method private removeCling(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$26;

    invoke-direct {v2, p0, v1, v0}, Lcom/cyanogenmod/trebuchet/Launcher$26;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private resetAddInfo()V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput v3, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput v3, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    iput v3, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput v3, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    iput v3, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->dropPos:[I

    return-void
.end method

.method private restoreState(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v10, "launcher.state"

    sget-object v11, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Launcher$State;->ordinal()I

    move-result v11

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/Launcher;->intToState(I)Lcom/cyanogenmod/trebuchet/Launcher$State;

    move-result-object v9

    sget-object v10, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v9, v10, :cond_2

    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/cyanogenmod/trebuchet/Launcher;->showAllApps(Z)V

    :cond_2
    const-string v10, "launcher.current_screen"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v10, -0x1

    if-le v2, v10, :cond_3

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v10, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->setCurrentPage(I)V

    :cond_3
    const-string v10, "launcher.add_container"

    const-wide/16 v11, -0x1

    invoke-virtual {p1, v10, v11, v12}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    const-string v10, "launcher.add_screen"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    const-wide/16 v10, -0x1

    cmp-long v10, v5, v10

    if-eqz v10, :cond_4

    const/4 v10, -0x1

    if-le v7, v10, :cond_4

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput-wide v5, v10, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput v7, v10, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const-string v11, "launcher.add_cell_x"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    iput v11, v10, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const-string v11, "launcher.add_cell_y"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    iput v11, v10, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    :cond_4
    const-string v10, "launcher.rename_folder"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_5

    const-string v10, "launcher.rename_folder_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    sget-object v11, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v10, p0, v11, v3, v4}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getFolderById(Landroid/content/Context;Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v10

    iput-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    :cond_5
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    if-eqz v10, :cond_0

    const-string v10, "apps_customize_currentTab"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v11, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getContentTypeForTabTag(Ljava/lang/String;)Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->setContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v10, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    :cond_6
    const-string v10, "apps_customize_currentIndex"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v10, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->restore(I)V

    goto/16 :goto_0
.end method

.method private sendAdvanceMessage(J)V
    .locals 3
    .param p1    # J

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceSentTime:J

    return-void
.end method

.method public static setFolderState(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/cyanogenmod/trebuchet/Launcher;->folderState:I

    return-void
.end method

.method public static setHeightPixels(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/cyanogenmod/trebuchet/Launcher;->heightPixels:I

    return-void
.end method

.method private setInputSource2KTV()V
    .locals 6

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;ZZZ)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "Launcher"

    const-string v2, "Do not need to switch to MM, because it is already here!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setInputSource2Storage()V
    .locals 6

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getMicStatusFromDB()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    if-eq v2, v1, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;ZZZ)Z

    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Force (mic) switch to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    goto :goto_0

    :cond_2
    const-string v2, "Launcher"

    const-string v3, "Do not need to switch to MM, because it is already here!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method private setNextPageBtnVisiable(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mNextPageBtn:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mNextPageBtn:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private setPivotsForZoom(Landroid/view/View;F)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # F

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotY(F)V

    return-void
.end method

.method private setPrevPageBtnVisiable(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPrevPageBtn:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPrevPageBtn:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method static setScreen(I)V
    .locals 2
    .param p0    # I

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher;->sLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sput p0, Lcom/cyanogenmod/trebuchet/Launcher;->sScreen:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setTVMode()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->alertDialogF:Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->alertDialogF:Landroid/app/AlertDialog$Builder;

    const-string v1, "First Time Setup:"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Please select the type of use for this TV.\n[Home Use] optimizes the picture recommended for normal home use and is energy efficient.\n[Store Demo] optimises the picture for store display."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Store Demo"

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$28;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/Launcher$28;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Home Use"

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$29;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/Launcher$29;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private setToHomeMode()V
    .locals 3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v0

    const/16 v2, 0x3c

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetBacklight(S)Z

    return-void
.end method

.method private setTvModeVal(I)Z
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/SettingDesk;->setModeVal(I)Z

    const/4 v1, 0x1

    return v1
.end method

.method public static setWidthPixels(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/cyanogenmod/trebuchet/Launcher;->widthPixels:I

    return-void
.end method

.method private setupTVWindow()V
    .locals 8

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getRealTVWindowRect()Landroid/graphics/Rect;

    move-result-object v7

    new-instance v0, Lcom/konka/avenger/tv/TVWindowManager;

    iget v2, v7, Landroid/graphics/Rect;->left:I

    iget v3, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v5

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->tvView:Lcom/konka/avenger/tv/TVView;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/avenger/tv/TVWindowManager;-><init>(Landroid/content/Context;IIIILcom/konka/avenger/tv/TVView;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVWindowManager;->start()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    sget-object v1, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->OPEN:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVWindowManager;->setState(Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;)V

    :cond_0
    return-void
.end method

.method private setupViews()V
    .locals 8

    const v7, 0x7f0d0035    # com.konka.avenger.R.id.dock_divider

    const v6, 0x7f0d0034    # com.konka.avenger.R.id.qsb_divider

    const/16 v5, 0x8

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    const v3, 0x7f0d0032    # com.konka.avenger.R.id.drag_layer

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/DragLayer;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    const v4, 0x7f0d0033    # com.konka.avenger.R.id.workspace

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/Workspace;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    const v3, 0x7f0d003b    # com.konka.avenger.R.id.tv_window

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/konka/avenger/tv/TVView;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->tvView:Lcom/konka/avenger/tv/TVView;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v3, p0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->setup(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/DragController;)V

    const v3, 0x7f0d0036    # com.konka.avenger.R.id.hotseat

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/Hotseat;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v3, p0}, Lcom/cyanogenmod/trebuchet/Hotseat;->setup(Lcom/cyanogenmod/trebuchet/Launcher;)V

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->setHapticFeedbackEnabled(Z)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3, p0}, Lcom/cyanogenmod/trebuchet/Workspace;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->setup(Lcom/cyanogenmod/trebuchet/DragController;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/DragController;->addDragListener(Lcom/cyanogenmod/trebuchet/DragController$DragListener;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    const v4, 0x7f0d0037    # com.konka.avenger.R.id.qsb_bar

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowSearchBar:Z

    if-nez v3, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowDockDivider:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const v3, 0x7f0d0038    # com.konka.avenger.R.id.apps_customize_pane

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    const v4, 0x7f0d0019    # com.konka.avenger.R.id.apps_customize_pane_content

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v3, p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->setup(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/DragController;)V

    const v3, 0x7f0d0055    # com.konka.avenger.R.id.all_apps_button

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAllAppsButton:Landroid/view/View;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAllAppsButton:Landroid/view/View;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAllAppsButton:Landroid/view/View;

    new-instance v4, Lcom/cyanogenmod/trebuchet/Launcher$7;

    invoke-direct {v4, p0}, Lcom/cyanogenmod/trebuchet/Launcher$7;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_3
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/DragController;->setDragScoller(Lcom/cyanogenmod/trebuchet/DragScroller;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/DragController;->setScrollView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/DragController;->setMoveTarget(Landroid/view/View;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/DragController;->addDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v3, p0, v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->setup(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/DragController;)V

    :cond_4
    const v3, 0x7f0d006d    # com.konka.avenger.R.id.workspace_prev_page_button

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPrevPageBtn:Landroid/view/View;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPrevPageBtn:Landroid/view/View;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPrevPageBtn:Landroid/view/View;

    new-instance v4, Lcom/cyanogenmod/trebuchet/Launcher$8;

    invoke-direct {v4, p0}, Lcom/cyanogenmod/trebuchet/Launcher$8;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const v3, 0x7f0d006e    # com.konka.avenger.R.id.workspace_next_page_button

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mNextPageBtn:Landroid/view/View;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mNextPageBtn:Landroid/view/View;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mNextPageBtn:Landroid/view/View;

    new-instance v4, Lcom/cyanogenmod/trebuchet/Launcher$9;

    invoke-direct {v4, p0}, Lcom/cyanogenmod/trebuchet/Launcher$9;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    return-void
.end method

.method private showAddDialog()V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->resetAddInfo()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const-wide/16 v1, -0x64

    iput-wide v1, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v1

    iput v1, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->showDialog(I)V

    return-void
.end method

.method private showAppsCustomizeHelper(ZZ)V
    .locals 20
    .param p1    # Z
    .param p2    # Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v5, p0

    const v2, 0x7f0b000b    # com.konka.avenger.R.integer.config_appsCustomizeZoomInTime

    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v11

    const v2, 0x7f0b000e    # com.konka.avenger.R.integer.config_appsCustomizeFadeInTime

    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    const v2, 0x7f0b000d    # com.konka.avenger.R.integer.config_appsCustomizeZoomScaleFactor

    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v15, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    const v2, 0x7f0b0012    # com.konka.avenger.R.integer.config_workspaceAppsCustomizeAnimationStagger

    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v16

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v15}, Lcom/cyanogenmod/trebuchet/Launcher;->setPivotsForZoom(Landroid/view/View;F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getTransitionEffect()Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-result-object v17

    sget-object v2, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateUp:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v17

    if-eq v0, v2, :cond_4

    sget-object v2, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateDown:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-object/from16 v0, v17

    if-eq v0, v2, :cond_4

    const/4 v9, 0x1

    :goto_0
    if-eqz v9, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    sget-object v3, Lcom/cyanogenmod/trebuchet/Workspace$State;->SMALL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    move/from16 v0, p1

    invoke-virtual {v2, v3, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;Z)V

    :cond_1
    if-eqz p1, :cond_5

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    int-to-long v0, v11

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v6

    new-instance v2, Lcom/cyanogenmod/trebuchet/Workspace$ZoomOutInterpolator;

    invoke-direct {v2}, Lcom/cyanogenmod/trebuchet/Workspace$ZoomOutInterpolator;-><init>()V

    invoke-virtual {v6, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$15;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v15}, Lcom/cyanogenmod/trebuchet/Launcher$15;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;F)V

    invoke-virtual {v6, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/view/View;->setAlpha(F)V

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_1

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    int-to-long v0, v12

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v8

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$16;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/cyanogenmod/trebuchet/Launcher$16;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;)V

    invoke-virtual {v8, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move/from16 v0, v16

    int-to-long v2, v0

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$17;

    move-object/from16 v3, p0

    move/from16 v7, p2

    invoke-direct/range {v2 .. v7}, Lcom/cyanogenmod/trebuchet/Launcher$17;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/ValueAnimator;Z)V

    invoke-virtual {v6, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    const/4 v10, 0x0

    instance-of v2, v4, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    if-eqz v2, :cond_2

    move-object v13, v4

    check-cast v13, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    const/4 v3, 0x0

    invoke-interface {v13, v5, v2, v3}, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;->onLauncherTransitionStart(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)Z

    move-result v10

    :cond_2
    if-nez v10, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mStateAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    :cond_3
    :goto_1
    return-void

    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/view/View;->setTranslationX(F)V

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/view/View;->setTranslationY(F)V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v4, v2}, Landroid/view/View;->setScaleX(F)V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v4, v2}, Landroid/view/View;->setScaleY(F)V

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4}, Landroid/view/View;->bringToFront()V

    instance-of v2, v4, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    if-eqz v2, :cond_6

    move-object v2, v4

    check-cast v2, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-interface {v2, v5, v3, v7}, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;->onLauncherTransitionStart(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)Z

    check-cast v4, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v4, v5, v2, v3}, Lcom/cyanogenmod/trebuchet/LauncherTransitionable;->onLauncherTransitionEnd(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)V

    if-nez p2, :cond_6

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v2

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->hideScrollingIndicator(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Launcher;->hideDockDivider()V

    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->updateWallpaperVisibility(Z)V

    goto :goto_1

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private showOrHideRecentAppsDialog(Landroid/content/Context;Z)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const-string v1, "Launcher"

    const-string v2, "showOrHideRecentAppsDialog"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.systemui.action.SHOW_RECENT_DIALOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "isInTouchMode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private shrinkAndFadeInFolderIcon(Lcom/cyanogenmod/trebuchet/FolderIcon;)V
    .locals 13
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderIcon;

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/4 v11, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v7, "alpha"

    new-array v8, v12, [F

    aput v9, v8, v11

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    const-string v7, "scaleX"

    new-array v8, v12, [F

    aput v9, v8, v11

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    const-string v7, "scaleY"

    new-array v8, v12, [F

    aput v9, v8, v11

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/FolderInfo;

    const/4 v1, 0x0

    iget-wide v7, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->container:J

    const-wide/16 v9, -0x65

    cmp-long v7, v7, v9

    if-nez v7, :cond_1

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    invoke-interface {v7}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    :cond_1
    move-object v3, v1

    const/4 v7, 0x3

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v7, v11

    aput-object v5, v7, v12

    const/4 v8, 0x2

    aput-object v6, v7, v8

    invoke-static {p1, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b001b    # com.konka.avenger.R.integer.config_folderAnimDuration

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v4, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v7, Lcom/cyanogenmod/trebuchet/Launcher$14;

    invoke-direct {v7, p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher$14;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/CellLayout;)V

    invoke-virtual {v4, v7}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private startWallpaper()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showWorkspace(Z)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SET_WALLPAPER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v2, 0x7f0a0008    # com.konka.avenger.R.string.chooser_wallpaper

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {p0, v0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private strShowTV(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isLaunchBySTR()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Launcher"

    const-string v1, "starting STRTVWdigetThread.........."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVWindowManager;->showBackground()V

    sget-boolean v0, Lcom/cyanogenmod/trebuchet/Launcher;->isFinish:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->wdigetThread:Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->wdigetThread:Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;->start()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/cyanogenmod/trebuchet/Launcher;->isFinish:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    sget-object v1, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->OPEN:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVWindowManager;->setState(Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;)V

    goto :goto_0
.end method

.method private syncOrientation()V
    .locals 3

    const-string v1, "Launcher"

    const-string v2, "+++++++++++++++++++++++++++syncOrientation"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "uimode"

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoRotate:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->setRequestedOrientation(I)V

    const-string v1, "Launcher"

    const-string v2, "+++++++++++++++++++++++++++syncOrientation 1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method private takePicture()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cyanogenmod/trebuchet/Launcher$27;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/Launcher$27;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private threadToProcessPictureMode()V
    .locals 3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_DYNAMIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v0, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    const/16 v2, 0x64

    invoke-interface {v0, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetBacklight(S)Z

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$32;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/Launcher$32;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private unbindWorkspaceAndHotseatItems()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->unbindWorkspaceItems()V

    :cond_0
    return-void
.end method

.method private updateAppLatestButton()V
    .locals 2

    const v1, 0x7f0d0018    # com.konka.avenger.R.id.applist_latest

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private updateAppMemSize()V
    .locals 9

    const v6, 0x7f0d0017    # com.konka.avenger.R.id.memsize_info

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Lcom/konka/avenger/utilities/Storage;

    invoke-direct {v3}, Lcom/konka/avenger/utilities/Storage;-><init>()V

    invoke-virtual {v3}, Lcom/konka/avenger/utilities/Storage;->getInternalStorage()Lcom/konka/avenger/utilities/StorageInfor;

    move-result-object v6

    iget-wide v4, v6, Lcom/konka/avenger/utilities/StorageInfor;->mUsedStorage:J

    invoke-virtual {v3}, Lcom/konka/avenger/utilities/Storage;->getInternalStorage()Lcom/konka/avenger/utilities/StorageInfor;

    move-result-object v6

    iget-wide v1, v6, Lcom/konka/avenger/utilities/StorageInfor;->mFreeStrorage:J

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a00b9    # com.konka.avenger.R.string.supersky_used

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v4, v5}, Lcom/konka/avenger/utilities/Tools;->sizeToAutoUnit(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p0, v1, v2}, Lcom/konka/avenger/utilities/Tools;->sizeToAutoUnit(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a00ba    # com.konka.avenger.R.string.supersky_canused

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private updateButtonWithDrawable(ILandroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private updateButtonWithIconFromExternalActivity(ILandroid/content/ComponentName;I)Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/content/ComponentName;
    .param p3    # I

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->getExternalPackageToolbarIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private updateGlobalSearchIcon(Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable$ConstantState;

    const v3, 0x7f0d005e    # com.konka.avenger.R.id.search_button

    const v2, 0x7f0d0061    # com.konka.avenger.R.id.search_button_container

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v3, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->updateButtonWithDrawable(ILandroid/graphics/drawable/Drawable$ConstantState;)V

    invoke-direct {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->invalidatePressedFocusedStates(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method private updateGlobalSearchIcon()Z
    .locals 12

    const v11, 0x7f0d005e    # com.konka.avenger.R.id.search_button

    const/4 v8, 0x0

    const/16 v10, 0x8

    const v9, 0x7f0d0061    # com.konka.avenger.R.id.search_button_container

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v11}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v9, 0x7f0d005f    # com.konka.avenger.R.id.search_divider

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v9, 0x7f0d0062    # com.konka.avenger.R.id.voice_button_container

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v9, 0x7f0d0060    # com.konka.avenger.R.id.voice_button

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const-string v9, "search"

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/SearchManager;

    invoke-virtual {v5}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentOrientationIndexForGlobalIcons()I

    move-result v1

    sget-object v9, Lcom/cyanogenmod/trebuchet/Launcher;->sGlobalSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    const v10, 0x7f020067    # com.konka.avenger.R.drawable.ic_home_search_normal_holo

    invoke-direct {p0, v11, v0, v10}, Lcom/cyanogenmod/trebuchet/Launcher;->updateButtonWithIconFromExternalActivity(ILandroid/content/ComponentName;I)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v10

    aput-object v10, v9, v1

    if-eqz v4, :cond_0

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0, v3, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->invalidatePressedFocusedStates(Landroid/view/View;Landroid/view/View;)V

    const/4 v8, 0x1

    :goto_0
    return v8

    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    if-eqz v7, :cond_5

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateRunning()V
    .locals 11

    const-wide/16 v1, 0x4e20

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mVisible:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mUserPresent:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    move v0, v3

    :goto_0
    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceRunning:Z

    if-eq v0, v5, :cond_0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceRunning:Z

    if-eqz v0, :cond_3

    iget-wide v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceTimeLeft:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    :goto_1
    invoke-direct {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->sendAdvanceMessage(J)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v4

    goto :goto_0

    :cond_2
    iget-wide v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceTimeLeft:J

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    const-wide/16 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceSentTime:J

    sub-long/2addr v7, v9

    sub-long v7, v1, v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoAdvanceTimeLeft:J

    :cond_4
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_2
.end method

.method private updateTextButtonWithDrawable(ILandroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/graphics/drawable/Drawable$ConstantState;

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private updateTextButtonWithIconFromExternalActivity(ILandroid/content/ComponentName;I)Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/content/ComponentName;
    .param p3    # I

    const/4 v8, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->getExternalPackageToolbarIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c001d    # com.konka.avenger.R.dimen.toolbar_external_icon_width

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v6, 0x7f0c001e    # com.konka.avenger.R.dimen.toolbar_external_icon_height

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v3, :cond_1

    const-string v6, "Launcher"

    const-string v7, "toolbarIcon is null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, v8, v8, v4, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    const-string v6, "Launcher"

    const-string v7, "toolbarIcon is not null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3, v8, v8, v4, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    if-eqz v0, :cond_2

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_2
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v5

    goto :goto_0
.end method

.method private updateVoiceSearchIcon(Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable$ConstantState;

    const v3, 0x7f0d0060    # com.konka.avenger.R.id.voice_button

    const v2, 0x7f0d0062    # com.konka.avenger.R.id.voice_button_container

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v3, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->updateButtonWithDrawable(ILandroid/graphics/drawable/Drawable$ConstantState;)V

    invoke-direct {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->invalidatePressedFocusedStates(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method private updateVoiceSearchIcon(Z)Z
    .locals 10
    .param p1    # Z

    const v9, 0x7f0d0060    # com.konka.avenger.R.id.voice_button

    const/16 v8, 0x8

    const/4 v6, 0x0

    const v7, 0x7f0d005f    # com.konka.avenger.R.id.search_divider

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v7, 0x7f0d0062    # com.konka.avenger.R.id.voice_button_container

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.speech.action.WEB_SEARCH"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentOrientationIndexForGlobalIcons()I

    move-result v1

    sget-object v7, Lcom/cyanogenmod/trebuchet/Launcher;->sVoiceSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    const v8, 0x7f02006e    # com.konka.avenger.R.drawable.ic_home_voice_search_holo

    invoke-direct {p0, v9, v0, v8}, Lcom/cyanogenmod/trebuchet/Launcher;->updateButtonWithIconFromExternalActivity(ILandroid/content/ComponentName;I)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v8

    aput-object v8, v7, v1

    if-eqz v3, :cond_0

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    if-eqz v5, :cond_1

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v5, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->invalidatePressedFocusedStates(Landroid/view/View;Landroid/view/View;)V

    const/4 v6, 0x1

    :goto_0
    return v6

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private static writeConfiguration(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/DataOutputStream;

    const-string v3, "launcher.preferences"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p1, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->locale:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget v3, p1, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mcc:I

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v3, p1, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;->mnc:I

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    move-object v1, v2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v3

    :goto_1
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0

    :catch_2
    move-exception v0

    :goto_2
    :try_start_4
    const-string v3, "launcher.preferences"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    :catch_3
    move-exception v3

    goto :goto_0

    :catchall_0
    move-exception v3

    :goto_3
    if-eqz v1, :cond_1

    :try_start_6
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_1
    :goto_4
    throw v3

    :catch_4
    move-exception v3

    move-object v1, v2

    goto :goto_0

    :catch_5
    move-exception v4

    goto :goto_4

    :catchall_1
    move-exception v3

    move-object v1, v2

    goto :goto_3

    :catch_6
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_7
    move-exception v3

    move-object v1, v2

    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public ClearTVManager_RestorSTR()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->finalizeAllManager()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method addAppWidgetFromDrop(Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;JI[I[I)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;
    .param p2    # J
    .param p4    # I
    .param p5    # [I
    .param p6    # [I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->resetAddInfo()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput-wide p2, p1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->container:J

    iput-wide p2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput p4, p1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->screen:I

    iput p4, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput-object p6, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->dropPos:[I

    if-eqz p5, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v2, 0x0

    aget v2, p5, v2

    iput v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v2, 0x1

    aget v2, p5, v2

    iput v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getAppWidgetHost()Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->allocateAppWidgetId()I

    move-result v0

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v1, v0, v2}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetId(ILandroid/content/ComponentName;)V

    invoke-virtual {p0, v0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->addAppWidgetImpl(ILcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;)V

    return-void
.end method

.method addAppWidgetFromPick(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const-string v1, "appWidgetId"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->addAppWidgetImpl(ILcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;)V

    return-void
.end method

.method addAppWidgetImpl(ILcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;)V
    .locals 15
    .param p1    # I
    .param p2    # Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget-object v12, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    iget-object v12, v1, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v12, :cond_5

    new-instance v5, Landroid/content/Intent;

    const-string v12, "android.appwidget.action.APPWIDGET_CONFIGURE"

    invoke-direct {v5, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v12, v1, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    invoke-virtual {v5, v12}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v12, "appWidgetId"

    move/from16 v0, p1

    invoke-virtual {v5, v12, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    if-eqz v12, :cond_0

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    const-string v12, "com.android.launcher.extra.widget.CONFIGURATION_DATA_MIME_TYPE"

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->mimeType:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->configurationData:Landroid/os/Parcelable;

    check-cast v2, Landroid/content/ClipData;

    invoke-virtual {v2}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v3

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v3}, Landroid/content/ClipDescription;->getMimeTypeCount()I

    move-result v12

    if-lt v4, v12, :cond_1

    :cond_0
    :goto_1
    const/4 v12, 0x5

    invoke-virtual {p0, v5, v12}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivityForResultSafely(Landroid/content/Intent;I)V

    :goto_2
    return-void

    :cond_1
    invoke-virtual {v3, v4}, Landroid/content/ClipDescription;->getMimeType(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual {v2, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v8, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    if-eqz v11, :cond_2

    const-string v12, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    invoke-virtual {v5, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1

    :cond_2
    if-eqz v6, :cond_3

    const-string v12, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    invoke-virtual {v5, v12, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    if-eqz v10, :cond_0

    const-string v12, "com.android.launcher.extra.widget.CONFIGURATION_DATA"

    invoke-virtual {v5, v12, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    goto :goto_1

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->container:J

    move-object/from16 v0, p2

    iget v14, v0, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->screen:I

    move/from16 v0, p1

    invoke-direct {p0, v0, v12, v13, v14}, Lcom/cyanogenmod/trebuchet/Launcher;->completeAddAppWidget(IJI)V

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-virtual {p0, v12, v13}, Lcom/cyanogenmod/trebuchet/Launcher;->exitSpringLoadedDragModeDelayed(ZZ)V

    goto :goto_2
.end method

.method addExternalItemToScreen(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v0, p1, p2}, Lcom/cyanogenmod/trebuchet/Workspace;->addExternalItemToScreen(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    :cond_0
    return-void
.end method

.method addFolder(Lcom/cyanogenmod/trebuchet/CellLayout;JIII)Lcom/cyanogenmod/trebuchet/FolderIcon;
    .locals 12
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    new-instance v1, Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-direct {v1}, Lcom/cyanogenmod/trebuchet/FolderInfo;-><init>()V

    const v0, 0x7f0a0007    # com.konka.avenger.R.string.folder_name

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v2, p2

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-static/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIIIZ)V

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    iget-wide v4, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v0, 0x7f030010    # com.konka.avenger.R.layout.folder_icon

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-static {v0, p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->fromXml(ILcom/cyanogenmod/trebuchet/Launcher;Landroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/FolderInfo;Lcom/cyanogenmod/trebuchet/IconCache;)Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-result-object v3

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHideIconLabels:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTextVisible(Z)V

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceLocked()Z

    move-result v11

    move-wide v4, p2

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    invoke-virtual/range {v2 .. v11}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    return-object v3
.end method

.method addWidgetToAutoAdvanceIfNeeded(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz p2, :cond_0

    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/Advanceable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Landroid/widget/Advanceable;

    invoke-interface {v0}, Landroid/widget/Advanceable;->fyiWillBeAdvancedByHostKThx()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V

    goto :goto_0
.end method

.method public bindAllApplications(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    const v3, 0x7f0d001f    # com.konka.avenger.R.id.apps_customize_progress_bar

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    new-instance v3, Lcom/cyanogenmod/trebuchet/Launcher$22;

    invoke-direct {v3, p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher$22;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    new-instance v3, Lcom/cyanogenmod/trebuchet/Launcher$23;

    invoke-direct {v3, p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher$23;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public bindAppWidget(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V
    .locals 14
    .param p1    # Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setLoadOnResume()Z

    const-wide/16 v12, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget v10, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->appWidgetId:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v1, v10}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v11

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    invoke-virtual {v1, p0, v10, v11}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v1

    iput-object v1, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v1, v10, v11}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v1, p1}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    iget-wide v2, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->container:J

    iget v4, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->screen:I

    iget v5, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->cellX:I

    iget v6, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->cellY:I

    iget v7, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanX:I

    iget v8, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanY:I

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {p0, v1, v11}, Lcom/cyanogenmod/trebuchet/Launcher;->addWidgetToAutoAdvanceIfNeeded(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->requestLayout()V

    return-void
.end method

.method public bindAppsAdded(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setLoadOnResume()Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->removeDialog(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->addApps(Ljava/util/ArrayList;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->addApps(Ljava/util/ArrayList;)V

    return-void
.end method

.method public bindAppsRemoved(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;Z)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->removeDialog(I)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->removeItems(Ljava/util/ArrayList;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->removeApps(Ljava/util/ArrayList;)V

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->removeApps(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v0, p1, p0}, Lcom/cyanogenmod/trebuchet/DragController;->onAppsRemoved(Ljava/util/ArrayList;Landroid/content/Context;)V

    return-void
.end method

.method public bindAppsUpdated(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setLoadOnResume()Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->removeDialog(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->updateShortcuts(Ljava/util/ArrayList;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->updateApps(Ljava/util/ArrayList;)V

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->updateApps(Ljava/util/ArrayList;)V

    return-void
.end method

.method public bindFolders(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/cyanogenmod/trebuchet/FolderInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setLoadOnResume()Z

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public bindItems(Ljava/util/ArrayList;II)V
    .locals 15
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ItemInfo;",
            ">;II)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setLoadOnResume()Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    move/from16 v13, p2

    :goto_0
    move/from16 v0, p3

    if-lt v13, v0, :cond_0

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->requestLayout()V

    return-void

    :cond_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-wide v5, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v7, -0x65

    cmp-long v3, v5, v7

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    if-nez v3, :cond_1

    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_1
    iget v3, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    packed-switch v3, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    move-object v3, v14

    check-cast v3, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->createShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;

    move-result-object v2

    iget-wide v3, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iget v5, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iget v6, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v7, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual/range {v1 .. v10}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    goto :goto_1

    :pswitch_1
    const v6, 0x7f030010    # com.konka.avenger.R.layout.folder_icon

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    move-object v5, v14

    check-cast v5, Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-static {v6, p0, v3, v5, v7}, Lcom/cyanogenmod/trebuchet/FolderIcon;->fromXml(ILcom/cyanogenmod/trebuchet/Launcher;Landroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/FolderInfo;Lcom/cyanogenmod/trebuchet/IconCache;)Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-result-object v4

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHideIconLabels:Z

    if-nez v3, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTextVisible(Z)V

    :cond_2
    iget-wide v5, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iget v7, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iget v8, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v9, v14, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v12}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIIIZ)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bindPackagesUpdated()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->onPackagesUpdated()V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onPackagesUpdated()V

    return-void
.end method

.method public bindSearchablesChanged()V
    .locals 3

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateGlobalSearchIcon()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateVoiceSearchIcon(Z)Z

    move-result v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v2, v0, v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onSearchPackagesChanged(ZZ)V

    return-void
.end method

.method public close3D()V
    .locals 8

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v0

    const-string v4, "Launcher"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "====================================get current 3d mode: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v4, v2, :cond_0

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1

    if-eq v0, v4, :cond_1

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    const-string v4, "Launcher"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "====================================mute video for source:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    const/4 v5, 0x1

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    :try_start_2
    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I

    move-result-object v4

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-eq v0, v4, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_1
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    goto :goto_1

    :pswitch_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public closeFolder()V
    .locals 2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder(Lcom/cyanogenmod/trebuchet/Folder;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissFolderCling(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method closeFolder(Lcom/cyanogenmod/trebuchet/Folder;)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Folder;->getInfo()Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v2

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v3, p1, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getViewForTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->shrinkAndFadeInFolderIcon(Lcom/cyanogenmod/trebuchet/FolderIcon;)V

    :cond_0
    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Folder;->animateClosed()V

    return-void
.end method

.method public closePIP()V
    .locals 5

    const/4 v4, 0x0

    const-string v2, "Launcher"

    const-string v3, "close PIP start----------->"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    if-nez v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    :cond_0
    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->getIsPipOn()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disablePip"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "Launcher"

    const-string v3, "close pip!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-virtual {v1, v4}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_1
    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->getIsPopOn()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disablePop"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "Launcher"

    const-string v3, "close pop!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    invoke-virtual {v1, v4}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_2
    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->getIsDualViewOn()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disable3dDualView"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "Launcher"

    const-string v3, "For little Tao==>close dual view!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z

    invoke-virtual {v1, v4}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    :goto_0
    const-string v2, "Launcher"

    const-string v3, "----------->close PIP end"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    const-string v2, "Launcher"

    const-string v3, "For little Tao==>the dual view is not enabled."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method closeSystemDialogs()V
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->closeAllPanels()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    return-void
.end method

.method public closeTVWindow()V
    .locals 2

    const-string v0, "Launcher"

    const-string v1, "close TV Window"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    sget-object v1, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->CLOSE:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVWindowManager;->setState(Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;)V

    :cond_0
    return-void
.end method

.method completeAddApplication(Landroid/content/Intent;JIII)V
    .locals 15
    .param p1    # Landroid/content/Intent;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    iget-object v14, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTmpAddItemCellCoordinates:[I

    move-wide/from16 v0, p2

    move/from16 v2, p4

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getCellLayout(JI)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v5

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isDefaultHomeScreen(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showNotAllowedAddMessage()V

    :goto_0
    return-void

    :cond_0
    if-ltz p5, :cond_2

    if-ltz p6, :cond_2

    const/4 v3, 0x0

    aput p5, v14, v3

    const/4 v3, 0x1

    aput p6, v14, v3

    :cond_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v3, v6, v0, p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getShortcutInfo(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    const/high16 v6, 0x10200000

    invoke-virtual {v4, v3, v6}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->setActivity(Landroid/content/ComponentName;I)V

    const-wide/16 v6, -0x1

    iput-wide v6, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->container:J

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v6, 0x0

    aget v9, v14, v6

    const/4 v6, 0x1

    aget v10, v14, v6

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceLocked()Z

    move-result v11

    move-wide/from16 v6, p2

    move/from16 v8, p4

    move/from16 v12, p5

    move/from16 v13, p6

    invoke-virtual/range {v3 .. v13}, Lcom/cyanogenmod/trebuchet/Workspace;->addApplicationShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Lcom/cyanogenmod/trebuchet/CellLayout;JIIIZII)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    const/4 v6, 0x1

    invoke-virtual {v5, v14, v3, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    goto :goto_0

    :cond_3
    const-string v3, "Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Couldn\'t find ActivityInfo for selected application: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method createShortcut(ILandroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v1, p1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {v0, p3, v1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->applyFromShortcutInfo(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Lcom/cyanogenmod/trebuchet/IconCache;)V

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHideIconLabels:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setTextVisible(Z)V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method createShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const v1, 0x7f030004    # com.konka.avenger.R.layout.application

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v1, v0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->createShortcut(ILandroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public destroyTVWindow()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVWindowManager;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    :cond_0
    return-void
.end method

.method public dismissAllAppsCling(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const v1, 0x7f0d001c    # com.konka.avenger.R.id.all_apps_cling

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Cling;

    const-string v1, "cling.allapps.dismissed"

    const/16 v2, 0xfa

    invoke-direct {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissCling(Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;I)V

    return-void
.end method

.method public dismissAllAppsSortCling(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const v1, 0x7f0d001d    # com.konka.avenger.R.id.all_apps_sort_cling

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Cling;

    const-string v1, "cling.allappssort.dismissed"

    const/16 v2, 0xfa

    invoke-direct {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissCling(Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;I)V

    return-void
.end method

.method public dismissFolderCling(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const v1, 0x7f0d003a    # com.konka.avenger.R.id.folder_cling

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Cling;

    const-string v1, "cling.folder.dismissed"

    const/16 v2, 0xfa

    invoke-direct {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissCling(Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;I)V

    return-void
.end method

.method public dismissTVMute()V
    .locals 7

    const/4 v6, 0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dismiss the mute,the current inputsource =="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->GetCecStatus()Lcom/mstar/android/tvapi/common/vo/CecSetting;

    move-result-object v0

    iget-short v3, v0, Lcom/mstar/android/tvapi/common/vo/CecSetting;->arcStatus:S

    if-ne v3, v6, :cond_0

    iget-short v3, v0, Lcom/mstar/android/tvapi/common/vo/CecSetting;->cecStatus:S

    if-ne v3, v6, :cond_0

    const-string v3, "Launcher"

    const-string v4, "dismiss the mute,arc is open"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->getIsBootByRecord()Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "Launcher"

    const-string v4, "dismiss the mute,not pvr standby recording"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    const/16 v4, 0xe0

    invoke-virtual {v3, v4, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setMuteStatus(ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    const/16 v4, 0xf0

    invoke-virtual {v3, v4, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setMuteStatus(ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    const-string v3, "Launcher"

    const-string v4, "dismiss the mute!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v3, "Launcher"

    const-string v4, "skip dismiss the mute,pvr standby recording"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public dismissWorkspaceCling(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const v1, 0x7f0d0039    # com.konka.avenger.R.id.workspace_cling

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Cling;

    const-string v1, "cling.workspace.dismissed"

    const/16 v2, 0xfa

    invoke-direct {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissCling(Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;I)V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_1
    :pswitch_0
    :sswitch_0
    return v0

    :sswitch_1
    const-string v1, "debug.launcher2.dumpstate"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->dumpState()V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x19 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/FileDescriptor;
    .param p3    # Ljava/io/PrintWriter;
    .param p4    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v1, "Debug logs: "

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher;->sDumpLogs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher;->sDumpLogs:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public dumpState()V
    .locals 3

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BEGIN launcher2 dump state for launcher "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mSavedState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedState:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mWorkspaceLoading="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mRestoring="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mWaitingForResult="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mSavedInstanceState="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sFolders.size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->dumpState()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->dumpState()V

    :cond_0
    const-string v0, "Launcher"

    const-string v1, "END launcher2 dump state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method enterSpringLoadedDragMode()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Workspace$State;->SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Workspace$State;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;)V

    invoke-direct {p0, v2, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->hideAppsCustomizeHelper(ZZ)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->hideDockDivider()V

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE_SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Launcher$State;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    :cond_0
    return-void
.end method

.method exitSpringLoadedDragMode()V
    .locals 5

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v3, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE_SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v4, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->showAppsCustomizeHelper(ZZ)V

    sget-object v2, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    :cond_0
    return-void
.end method

.method exitSpringLoadedDragModeDelayed(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE_SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/cyanogenmod/trebuchet/Launcher$21;

    invoke-direct {v2, p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher$21;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    if-eqz p2, :cond_1

    const/16 v0, 0x258

    :goto_1
    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    const/16 v0, 0x12c

    goto :goto_1
.end method

.method public finishBindingItems()V
    .locals 8

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setLoadOnResume()Z

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedState:Landroid/os/Bundle;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->hasFocus()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :cond_0
    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedState:Landroid/os/Bundle;

    :cond_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-super {p0, v3}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedInstanceState:Landroid/os/Bundle;

    :cond_2
    iput-boolean v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setupTVWindow()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissTVMute()V

    const/4 v2, 0x0

    :goto_0
    sget-object v3, Lcom/cyanogenmod/trebuchet/Launcher;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_5

    sget-object v3, Lcom/cyanogenmod/trebuchet/Launcher;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateAppMemSize()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateAppLatestButton()V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mBuildLayersRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v3

    if-ne v3, v6, :cond_3

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v4, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v3, v4, :cond_3

    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "focus to the tv widget"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3, v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v7}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v6}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {v0, v6}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_3
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v4, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v3, v4, :cond_4

    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "focus to the APPS_CUSTOMIZE"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->onResume()V

    :cond_4
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->refreshAddGuide()V

    return-void

    :cond_5
    sget-object v3, Lcom/cyanogenmod/trebuchet/Launcher;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->completeAdd(Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;)Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method

.method public getAppWidgetHost()Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    return-object v0
.end method

.method getCellLayout(JI)Lcom/cyanogenmod/trebuchet/CellLayout;
    .locals 2
    .param p1    # J
    .param p3    # I

    const-wide/16 v0, -0x65

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v0, p3}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    goto :goto_0
.end method

.method public getCurrentOrientation()I
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method public getCurrentWorkspaceScreen()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/ComponentName;
    .param p3    # Landroid/graphics/Rect;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-le v2, v3, :cond_0

    invoke-static {p1, p2, p3}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "!!!!get the rect====="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0038    # com.konka.avenger.R.dimen.app_widget_padding_left

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->left:I

    const v2, 0x7f0c0039    # com.konka.avenger.R.dimen.app_widget_padding_right

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    const v2, 0x7f0c003a    # com.konka.avenger.R.dimen.app_widget_padding_top

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    const v2, 0x7f0c003b    # com.konka.avenger.R.dimen.app_widget_padding_bottom

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get the rect====="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    goto :goto_0
.end method

.method public getDragController()Lcom/cyanogenmod/trebuchet/DragController;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    return-object v0
.end method

.method public getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    return-object v0
.end method

.method public getHomeScreenIndex()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    return-object v0
.end method

.method getMinResizeSpanForWidget(Landroid/appwidget/AppWidgetProviderInfo;[I)[I
    .locals 3
    .param p1    # Landroid/appwidget/AppWidgetProviderInfo;
    .param p2    # [I

    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/content/ComponentName;II[I)[I

    move-result-object v0

    return-object v0
.end method

.method public getModel()Lcom/cyanogenmod/trebuchet/LauncherModel;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    return-object v0
.end method

.method getSearchBar()Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    return-object v0
.end method

.method getSpanForWidget(Landroid/appwidget/AppWidgetProviderInfo;[I)[I
    .locals 3
    .param p1    # Landroid/appwidget/AppWidgetProviderInfo;
    .param p2    # [I

    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/content/ComponentName;II[I)[I

    move-result-object v0

    return-object v0
.end method

.method getSpanForWidget(Landroid/content/ComponentName;II[I)[I
    .locals 6
    .param p1    # Landroid/content/ComponentName;
    .param p2    # I
    .param p3    # I
    .param p4    # [I

    const/4 v5, 0x0

    if-nez p4, :cond_0

    const/4 v3, 0x2

    new-array p4, v3, [I

    :cond_0
    invoke-virtual {p0, p0, p1, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, p2

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int v2, v3, v4

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, p3

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    add-int v1, v3, v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v2, v1, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->rectToCell(Landroid/content/res/Resources;II[I)[I

    move-result-object v3

    return-object v3
.end method

.method getSpanForWidget(Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;[I)[I
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;
    .param p2    # [I

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->componentName:Landroid/content/ComponentName;

    iget v1, p1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->minWidth:I

    iget v2, p1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->minHeight:I

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/content/ComponentName;II[I)[I

    move-result-object v0

    return-object v0
.end method

.method public getTVWindowRect()Landroid/graphics/Rect;
    .locals 9

    const/4 v8, 0x1

    const/4 v6, 0x0

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v4, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x5

    invoke-virtual {v1, v4, v6}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v4, 0x2

    new-array v2, v4, [I

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v4, v2, v6

    aget v5, v2, v8

    aget v6, v2, v6

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    aget v7, v2, v8

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    return-object v3
.end method

.method getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    return-object v0
.end method

.method public hideAddGuide()V
    .locals 4

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->hideGuideView()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method hideDockDivider()V
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowSearchBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowDockDivider:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method hideHotseat(Z)V
    .locals 4
    .param p1    # Z

    const/4 v2, 0x0

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getTransitionOutDuration()I

    move-result v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/Hotseat;->setAlpha(F)V

    goto :goto_0
.end method

.method public isAllAppsCustomizeOpen()Z
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAllAppsVisible()Z
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFolderClingVisible()Z
    .locals 3

    const/4 v1, 0x0

    const v2, 0x7f0d003a    # com.konka.avenger.R.id.folder_cling

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Cling;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Cling;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method isHotseatLayout(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Hotseat;->getLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWorkspaceLocked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isWorkspaceVisible()Z
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method lockAllApps()V
    .locals 0

    return-void
.end method

.method public lockScreenOrientationOnLargeUI()V
    .locals 1

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentOrientation()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->mapConfigurationOriActivityInfoOri(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->setRequestedOrientation(I)V

    :cond_0
    return-void
.end method

.method public newIntentForStartActivity(Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v3, 0x0

    const/4 v8, -0x1

    const/4 v2, 0x0

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    if-ne p2, v8, :cond_3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-wide v4, v4, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    new-instance v1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;-><init>(Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;)V

    iput p1, v1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->requestCode:I

    iput-object p3, v1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->intent:Landroid/content/Intent;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-wide v4, v4, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iput-wide v4, v1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->container:J

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v4, v4, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iput v4, v1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->screen:I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v4, v4, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iput v4, v1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->cellX:I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v4, v4, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    iput v4, v1, Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;->cellY:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceLocked()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/cyanogenmod/trebuchet/Launcher;->sPendingAddList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {p0, v3, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->exitSpringLoadedDragModeDelayed(ZZ)V

    return-void

    :cond_2
    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->completeAdd(Lcom/cyanogenmod/trebuchet/Launcher$PendingAddArguments;)Z

    move-result v2

    goto :goto_0

    :cond_3
    const/16 v4, 0x9

    if-eq p1, v4, :cond_4

    const/4 v4, 0x5

    if-ne p1, v4, :cond_0

    :cond_4
    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    const-string v4, "appWidgetId"

    invoke-virtual {p3, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v8, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    invoke-virtual {v4, v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->deleteAppWidgetId(I)V

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.konka.launcher.action.SHOW_ALL_APP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAttached:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mVisible:Z

    return-void
.end method

.method public onBackPressed()V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v2, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v1, v2, :cond_0

    const-string v1, "Launcher"

    const-string v2, "onBackPressed ========"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->showWorkspace(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showTVWindow()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->exitWidgetResizeMode()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v1

    if-ne v1, v3, :cond_3

    const-string v1, "Launcher"

    const-string v2, "the back press is not response in the home page!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->showOutlinesTemporarily()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1    # Landroid/view/View;

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v11, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    const-string v5, "Launcher"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onClick"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    instance-of v5, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-eqz v5, :cond_2

    move-object v5, v4

    check-cast v5, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v1, v5, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    new-array v2, v9, [I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    new-instance v5, Landroid/graphics/Rect;

    aget v6, v2, v8

    aget v7, v2, v11

    aget v8, v2, v8

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    aget v9, v2, v11

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v1, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    instance-of v5, p1, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v5, :cond_0

    check-cast p1, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResume:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResume:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-virtual {v5, v11}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setStayPressed(Z)V

    goto :goto_0

    :cond_2
    instance-of v5, v4, Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eqz v5, :cond_3

    new-array v2, v9, [I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[wjx]"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    instance-of v5, p1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v5, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    sget v6, Lcom/cyanogenmod/trebuchet/Launcher;->folderState:I

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/Folder;->setmFolderState(I)V

    invoke-direct {p0, v0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->handleFolderClick(Lcom/cyanogenmod/trebuchet/FolderIcon;[I)V

    sput v11, Lcom/cyanogenmod/trebuchet/Launcher;->folderState:I

    goto/16 :goto_0

    :cond_3
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAllAppsButton:Landroid/view/View;

    if-ne p1, v5, :cond_0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v6, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v5, v6, :cond_4

    invoke-virtual {p0, v11}, Lcom/cyanogenmod/trebuchet/Launcher;->showWorkspace(Z)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->onClickAllAppsButton(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public onClickAllAppsButton(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showAllApps(Z)V

    return-void
.end method

.method public onClickAppLatestButton(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const-string v0, "Launcher"

    const-string v1, "onClickAppLatestButton"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    invoke-direct {p0, p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showOrHideRecentAppsDialog(Landroid/content/Context;Z)V

    return-void
.end method

.method public onClickKKSearchButton(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.android.vending"

    const-string v3, "com.android.vending.AssetBrowserActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->newIntentForStartActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KKSearch"

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    return-void
.end method

.method public onClickNetworkStatusIndicator(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onClickNetworkStatusIndicator(Landroid/view/View;)V

    return-void
.end method

.method public onClickOverflowMenuButton(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    new-instance v1, Landroid/widget/PopupMenu;

    invoke-direct {v1, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method

.method public onClickSearchButton(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->onSearchRequested()Z

    return-void
.end method

.method public onClickSettingsButton(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.konka.systemsetting"

    const-string v3, "com.konka.systemsetting.MainActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->newIntentForStartActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "Settings"

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    return-void
.end method

.method public onClickUsbStatusIndicator(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.konka.mm"

    const-string v3, "com.konka.mm.filemanager.FileDiskActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->newIntentForStartActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "Usb Status Indicator"

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    return-void
.end method

.method public onClickUserCenterButton(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.konka.passport"

    const-string v3, "com.konka.passport.LoginCheckActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->newIntentForStartActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "UserCenter"

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    return-void
.end method

.method public onClickVoiceButton(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v8, "Launcher"

    const-string v9, "onCreate==="

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowDPI()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherApplication;

    invoke-virtual {v0, p0}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->setLauncher(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getIconCache()Lcom/cyanogenmod/trebuchet/IconCache;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    new-instance v8, Lcom/cyanogenmod/trebuchet/DragController;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/DragController;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    new-instance v8, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    const/16 v9, 0x400

    invoke-direct {v8, p0, v9}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;-><init>(Landroid/content/Context;I)V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->startListening()V

    invoke-static {p0}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getShowSearchBar(Landroid/content/Context;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowSearchBar:Z

    invoke-static {p0}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen$Indicator;->getShowDockDivider(Landroid/content/Context;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowDockDivider:Z

    invoke-static {p0}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getHideIconLabels(Landroid/content/Context;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHideIconLabels:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090002    # com.konka.avenger.R.bool.config_defaultAutoRotate

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    invoke-static {p0, v8}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$General;->getAutoRotate(Landroid/content/Context;Z)Z

    move-result v8

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAutoRotate:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->checkForLocaleChange()V

    const v8, 0x7f030016    # com.konka.avenger.R.layout.launcher

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/Launcher;->setContentView(I)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setupViews()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showFirstRunWorkspaceCling()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->bindConnectivityService()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->registerContentObservers()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->lockAllApps()V

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedState:Landroid/os/Bundle;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedState:Landroid/os/Bundle;

    invoke-direct {p0, v8}, Lcom/cyanogenmod/trebuchet/Launcher;->restoreState(Landroid/os/Bundle;)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->onPackagesUpdated()V

    :cond_0
    iget-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    const/4 v9, 0x1

    invoke-virtual {v8, p0, v9}, Lcom/cyanogenmod/trebuchet/LauncherModel;->startLoader(Landroid/content/Context;Z)V

    :cond_1
    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/LauncherModel;->isAllAppsLoaded()Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f030007    # com.konka.avenger.R.layout.apps_customize_progressbar

    invoke-virtual {v8, v9, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :cond_2
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v8, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v3, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentOrientationIndexForGlobalIcons()I

    move-result v2

    sget-object v8, Lcom/cyanogenmod/trebuchet/Launcher;->sGlobalSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v8, v8, v2

    if-eqz v8, :cond_3

    sget-object v8, Lcom/cyanogenmod/trebuchet/Launcher;->sVoiceSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v8, v8, v2

    if-nez v8, :cond_4

    :cond_3
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateGlobalSearchIcon()Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->updateVoiceSearchIcon(Z)Z

    move-result v7

    :cond_4
    sget-object v8, Lcom/cyanogenmod/trebuchet/Launcher;->sGlobalSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v8, v8, v2

    if-eqz v8, :cond_5

    sget-object v8, Lcom/cyanogenmod/trebuchet/Launcher;->sGlobalSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v8, v8, v2

    invoke-direct {p0, v8}, Lcom/cyanogenmod/trebuchet/Launcher;->updateGlobalSearchIcon(Landroid/graphics/drawable/Drawable$ConstantState;)V

    const/4 v4, 0x1

    :cond_5
    sget-object v8, Lcom/cyanogenmod/trebuchet/Launcher;->sVoiceSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v8, v8, v2

    if-eqz v8, :cond_6

    sget-object v8, Lcom/cyanogenmod/trebuchet/Launcher;->sVoiceSearchIcon:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v8, v8, v2

    invoke-direct {p0, v8}, Lcom/cyanogenmod/trebuchet/Launcher;->updateVoiceSearchIcon(Landroid/graphics/drawable/Drawable$ConstantState;)V

    const/4 v7, 0x1

    :cond_6
    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v8, v4, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onSearchPackagesChanged(ZZ)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->syncOrientation()V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->tvView:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v8}, Lcom/konka/avenger/tv/TVView;->startInputSourceThread()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v8

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v8

    iget-object v8, v8, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    const-string v9, "teac"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v8

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/SettingDesk;->GetInstallationFlag()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->setTVMode()V

    :cond_7
    :goto_0
    return-void

    :cond_8
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getTvModeVal()I

    move-result v6

    const/4 v8, 0x2

    if-ne v6, v8, :cond_7

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->threadToProcessPictureMode()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;

    invoke-direct {v0, p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->createDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;

    invoke-direct {v0, p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->createDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 12
    .param p1    # Landroid/view/Menu;

    const v11, 0x1080049    # android.R.drawable.ic_menu_preferences

    const/4 v10, 0x2

    const/4 v6, 0x1

    const/high16 v9, 0x10800000

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090006    # com.konka.avenger.R.bool.config_optionMenuEnable

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceLocked()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.settings.MANAGE_ALL_APPLICATIONS_SETTINGS"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const-class v8, Lcom/cyanogenmod/trebuchet/preference/Preferences;

    invoke-virtual {v7, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.settings.SETTINGS"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v7, 0x10200000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const v7, 0x7f0a0040    # com.konka.avenger.R.string.help_url

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v0, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const v7, 0x7f0a0038    # com.konka.avenger.R.string.menu_wallpaper

    invoke-interface {p1, v6, v10, v5, v7}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    const v8, 0x108003f    # android.R.drawable.ic_menu_gallery

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v7

    const/16 v8, 0x57

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    const/4 v7, 0x4

    const v8, 0x7f0a0037    # com.konka.avenger.R.string.menu_market

    invoke-interface {p1, v10, v7, v5, v8}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    const/16 v8, 0x41

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v7

    new-instance v8, Lcom/cyanogenmod/trebuchet/Launcher$11;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/Launcher$11;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090003    # com.konka.avenger.R.bool.config_cyanogenmod

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x5

    const v8, 0x7f0a003c    # com.konka.avenger.R.string.menu_preferences

    invoke-interface {p1, v5, v7, v5, v8}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v11}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v3}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v7

    const/16 v8, 0x4f

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    :cond_2
    const/4 v7, 0x6

    const v8, 0x7f0a003b    # com.konka.avenger.R.string.menu_settings

    invoke-interface {p1, v5, v7, v5, v8}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v11}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v7

    const/16 v8, 0x50

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    const/4 v7, 0x7

    const v8, 0x7f0a003d    # com.konka.avenger.R.string.menu_help

    invoke-interface {p1, v5, v7, v5, v8}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v5

    const v7, 0x1080040    # android.R.drawable.ic_menu_help

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v5

    const/16 v7, 0x48

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    :cond_3
    move v5, v6

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v2, "Launcher"

    const-string v3, "onDestroy: destroying TVWindow"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->destroyTVWindow()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mBuildLayersRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->removeCallbacks(Ljava/lang/Runnable;)Z

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceBinder:Lcom/cyanogenmod/trebuchet/ConnectivityService;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherApplication;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->stopLoader()V

    invoke-virtual {v0, v4}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->setLauncher(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/LauncherModel;

    :try_start_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->stopListening()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppWidgetHost:Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/method/TextKeyListener;->release()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->unbindWorkspaceAndHotseatItems()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->removeAllViews()V

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->tvView:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v2}, Lcom/konka/avenger/tv/TVView;->stopInputSourceThread()V

    return-void

    :catch_0
    move-exception v1

    const-string v2, "Launcher"

    const-string v3, "problem while stopping AppWidgetHost during Launcher destruction"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onDetachedFromWindow()V

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mVisible:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAttached:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAttached:Z

    :cond_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x1

    const-string v7, "Launcher"

    const-string v8, "onKeyDown"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v5

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-lez v5, :cond_1

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v7

    if-nez v7, :cond_1

    move v4, v6

    :goto_0
    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->acceptFilter()Z

    move-result v7

    if-eqz v7, :cond_2

    if-eqz v4, :cond_2

    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v7

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v7, v8, v9, p1, p2}, Landroid/text/method/TextKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDefaultKeySsb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->onSearchRequested()Z

    move-result v6

    :cond_0
    :goto_1
    return v6

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    const/16 v7, 0x52

    if-ne p1, v7, :cond_3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v7

    if-nez v7, :cond_0

    :cond_3
    const/4 v7, 0x4

    if-ne p1, v7, :cond_4

    const-string v7, "====>leedebug"

    const-string v8, "KeyEvent.KEYCODE_BACK Launcher"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const/16 v7, 0x54

    if-ne p1, v7, :cond_5

    new-instance v0, Landroid/content/ComponentName;

    const-string v7, "com.android.vending"

    const-string v8, "com.android.vending.AssetBrowserActivity"

    invoke-direct {v0, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->newIntentForStartActivity(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    const-string v7, "KKSearch"

    invoke-virtual {p0, v3, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->isInSearchScreen()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->getSearchScreen()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->hasFocus()Z

    move-result v7

    if-eqz v7, :cond_6

    if-nez v2, :cond_6

    const/4 v6, 0x0

    sparse-switch p1, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->requestFocus()Z

    move-result v6

    goto :goto_1

    :sswitch_1
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->scrollLeft()V

    const/4 v6, 0x1

    goto :goto_1

    :sswitch_2
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->scrollRight()V

    const/4 v6, 0x1

    goto :goto_1

    :sswitch_3
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->getScrollingIndicator()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    move-result v6

    goto :goto_1

    :cond_6
    move v6, v2

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_3
        0x5c -> :sswitch_1
        0x5d -> :sswitch_2
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 8
    .param p1    # Landroid/view/View;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v7, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-eq v6, v7, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceLocked()Z

    move-result v6

    if-nez v6, :cond_0

    instance-of v6, p1, Lcom/cyanogenmod/trebuchet/CellLayout;

    if-nez v6, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    :cond_2
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->resetAddInfo()V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    if-nez v3, :cond_3

    move v4, v5

    goto :goto_0

    :cond_3
    iget-object v1, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->isHotseatLayout(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Workspace;->allowLongPress()Z

    move-result v6

    if-nez v6, :cond_5

    move v0, v4

    :goto_1
    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/DragController;->isDragging()Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v1, :cond_4

    instance-of v4, v1, Lcom/cyanogenmod/trebuchet/Folder;

    if-nez v4, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v4, v2, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v4, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->startDrag(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V

    :cond_4
    move v4, v5

    goto :goto_0

    :cond_5
    move v0, v5

    goto :goto_1
.end method

.method public onLongClickAppsTab(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x1

    new-instance v1, Landroid/widget/PopupMenu;

    invoke-direct {v1, p0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissAllAppsSortCling(Landroid/view/View;)V

    const/high16 v3, 0x7f0f0000    # com.konka.avenger.R.menu.apps_tab

    invoke-virtual {v1, v3}, Landroid/widget/PopupMenu;->inflate(I)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->getSortMode()Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    move-result-object v2

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->$SWITCH_TABLE$com$cyanogenmod$trebuchet$AppsCustomizeView$SortMode()[I

    move-result-object v3

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    new-instance v3, Lcom/cyanogenmod/trebuchet/Launcher$13;

    invoke-direct {v3, p0}, Lcom/cyanogenmod/trebuchet/Launcher$13;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    invoke-virtual {v1, v3}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    return-void

    :pswitch_0
    const v3, 0x7f0d006f    # com.konka.avenger.R.id.apps_sort_title

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_1
    const v3, 0x7f0d0070    # com.konka.avenger.R.id.apps_sort_install_date

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/high16 v9, 0x400000

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    const-string v7, "Launcher"

    const-string v8, "====================onNewIntent"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeSystemDialogs()V

    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v7

    and-int/2addr v7, v9

    if-eq v7, v9, :cond_4

    move v0, v5

    :goto_0
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v3

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->exitWidgetResizeMode()V

    const-string v7, "goHomePage"

    invoke-virtual {p1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v7, "Launcher"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "====================isGoHomePage==="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  The state==="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->exitSpringLoadedDragMode()V

    invoke-direct {p0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->strShowTV(Z)V

    if-eqz v0, :cond_5

    if-nez v2, :cond_5

    move v7, v5

    :goto_1
    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/Launcher;->showWorkspace(Z)V

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v8, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v7, v8, :cond_1

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/Workspace;->isTouchActive()Z

    move-result v7

    if-nez v7, :cond_1

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v7

    if-eq v7, v5, :cond_1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/Workspace;->moveToDefaultScreen(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    if-eqz v5, :cond_2

    const-string v5, "input_method"

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    invoke-virtual {v1, v5, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_2
    if-nez v0, :cond_3

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->reset()V

    :cond_3
    return-void

    :cond_4
    move v0, v6

    goto/16 :goto_0

    :cond_5
    move v7, v6

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->startWallpaper()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->checkInputSource()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "Launcher"

    const-string v1, "======onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPaused:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->onPause()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4
    .param p1    # I
    .param p2    # Landroid/app/Dialog;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eqz v2, :cond_0

    const v2, 0x7f0d005d    # com.konka.avenger.R.id.folder_name

    invoke-virtual {p2, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->isTransitioning()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    move v1, v2

    :goto_2
    invoke-interface {p1, v3, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    const/4 v1, 0x2

    if-eqz v0, :cond_1

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppMarketIntent:Landroid/content/Intent;

    if-eqz v4, :cond_1

    move v2, v3

    :cond_1
    invoke-interface {p1, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    move v2, v3

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSavedInstanceState:Landroid/os/Bundle;

    return-void
.end method

.method protected onResume()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v6, "Launcher"

    const-string v7, "======onResume"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "auto_time"

    invoke-static {v6, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurInputsoureFromDB()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v6, :cond_6

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "auto_time"

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v6, "Launcher"

    const-string v7, "=======check set auto time to 0"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPaused:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->preferencesChanged()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v6

    invoke-static {v6}, Landroid/os/Process;->killProcess(I)V

    :cond_1
    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mOnResumeNeedsLoad:Z

    if-eqz v6, :cond_3

    :cond_2
    iput-boolean v9, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v6, p0, v9}, Lcom/cyanogenmod/trebuchet/LauncherModel;->startLoader(Landroid/content/Context;Z)V

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mRestoring:Z

    iput-boolean v8, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mOnResumeNeedsLoad:Z

    :cond_3
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResume:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResume:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-virtual {v6, v8}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setStayPressed(Z)V

    :cond_4
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->onResume()V

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Workspace;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    new-instance v6, Lcom/cyanogenmod/trebuchet/Launcher$6;

    invoke-direct {v6, p0, v5, v2}, Lcom/cyanogenmod/trebuchet/Launcher$6;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Workspace;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v2, v6}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :cond_5
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->clearTypedText()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    return-void

    :catch_0
    move-exception v0

    const/4 v3, 0x0

    goto :goto_0

    :cond_6
    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v6, :cond_0

    if-ne v4, v9, :cond_0

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "auto_time"

    invoke-static {v6, v7, v9}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v6, "Launcher"

    const-string v7, "=======check set auto time to 1"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->stopLoader()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->surrender()V

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const-string v2, "launcher.current_screen"

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v2, "launcher.state"

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher$State;->ordinal()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-wide v2, v2, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v2, v2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    if-eqz v2, :cond_0

    const-string v2, "launcher.add_container"

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-wide v3, v3, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    invoke-virtual {p1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v2, "launcher.add_screen"

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v3, v3, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "launcher.add_cell_x"

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v3, v3, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "launcher.add_cell_y"

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v3, v3, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    if-eqz v2, :cond_1

    const-string v2, "launcher.rename_folder"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "launcher.rename_folder_id"

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v3, v3, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    invoke-virtual {p1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v2, "apps_customize_currentTab"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->getSaveInstanceStateIndex()I

    move-result v0

    const-string v2, "apps_customize_currentIndex"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "Launcher"

    const-string v1, "========onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closePIP()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->close3D()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    const-string v0, "Launcher"

    const-string v1, "========onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->checkSystemAutoTime()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showWorkspace(Z)V

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchDownAllAppsButton(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .param p1    # I

    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->onTrimMemory()V

    :cond_0
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x1

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mVisible:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v0

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_2

    const-string v0, "Launcher"

    const-string v1, "SHOW_TV_WINDOW======onWindowVisibilityChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->showTVWindow()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v0, "Launcher"

    const-string v1, "HIDE_TV_WINDOW======onWindowVisibilityChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeTVWindow()V

    goto :goto_1
.end method

.method public openFolder(Lcom/cyanogenmod/trebuchet/FolderIcon;[I)V
    .locals 5
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderIcon;
    .param p2    # [I

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/Folder;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/DragController;->addDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    :goto_0
    invoke-virtual {v0, v0, p2}, Lcom/cyanogenmod/trebuchet/Folder;->animateOpen(Lcom/cyanogenmod/trebuchet/Folder;[I)V

    return-void

    :cond_0
    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Opening folder ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") which already has a parent ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public preferencesChanged()Z
    .locals 5

    const/4 v4, 0x0

    const-string v3, "com.cyanogenmod.trebuchet_preferences"

    invoke-virtual {p0, v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "preferences_changed"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "preferences_changed"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return v1
.end method

.method processShortcut(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a001c    # com.konka.avenger.R.string.group_applications

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    const/4 v5, 0x0

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.PICK_ACTIVITY"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "android.intent.extra.INTENT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v4, "android.intent.extra.TITLE"

    const v5, 0x7f0a0027    # com.konka.avenger.R.string.title_select_application

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    const/4 v4, 0x6

    invoke-virtual {p0, v2, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivityForResultSafely(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivityForResultSafely(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method processShortcutFromDrop(Landroid/content/ComponentName;JI[I[I)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # J
    .param p4    # I
    .param p5    # [I
    .param p6    # [I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->resetAddInfo()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput-wide p2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput p4, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput-object p6, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->dropPos:[I

    if-eqz p5, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v2, 0x0

    aget v2, p5, v2

    iput v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPendingAddInfo:Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v2, 0x1

    aget v2, p5, v2

    iput v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->processShortcut(Landroid/content/Intent;)V

    return-void
.end method

.method processWallpaper(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public refreshAddGuide()V
    .locals 4

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v3, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->refreshGuideView()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public refreshPageBtn()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->refreshPageBtn(II)V

    return-void
.end method

.method public refreshPageBtn(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->setPrevPageBtnVisiable(Z)V

    add-int/lit8 v0, p2, -0x1

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->setNextPageBtnVisiable(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public removeAppWidget(Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->removeWidgetToAutoAdvance(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    return-void
.end method

.method removeFolder(Lcom/cyanogenmod/trebuchet/FolderInfo;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderInfo;

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;

    iget-wide v1, p1, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method removeWidgetToAutoAdvance(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V

    :cond_0
    return-void
.end method

.method public setLoadOnResume()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mPaused:Z

    if-eqz v1, :cond_0

    const-string v1, "Launcher"

    const-string v2, "setLoadOnResume"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mOnResumeNeedsLoad:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method showAllApps(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "Launcher"

    const-string v1, "HIDE_TV_WINDOW======showAllApps"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeTVWindow()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->tvView:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVView;->setStorageInput()V

    invoke-direct {p0, p1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showAppsCustomizeHelper(ZZ)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getAppsCustomizePagedView()Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getFocusOnFistItem()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->hideSearchBar(Z)V

    sget-object v0, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mUserPresent:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method showDockDivider(Z)V
    .locals 8
    .param p1    # Z

    const/4 v7, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowSearchBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowDockDivider:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowSearchBar:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    :cond_3
    if-eqz p1, :cond_5

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowSearchBar:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowDockDivider:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    const-string v3, "alpha"

    new-array v4, v7, [F

    aput v6, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    const-string v3, "alpha"

    new-array v4, v7, [F

    aput v6, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getTransitionInDuration()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_5
    return-void

    :cond_6
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowSearchBar:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mQsbDivider:Landroid/view/View;

    const-string v2, "alpha"

    new-array v3, v7, [F

    aput v6, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    :cond_7
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mShowDockDivider:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDividerAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mDockDivider:Landroid/view/View;

    const-string v2, "alpha"

    new-array v3, v7, [F

    aput v6, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0
.end method

.method public showFirstRunAllAppsCling([I)V
    .locals 4
    .param p1    # [I

    const v3, 0x7f0d001c    # com.konka.avenger.R.id.all_apps_cling

    const/4 v2, 0x0

    const-string v1, "com.cyanogenmod.trebuchet_preferences"

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isClingsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "cling.allapps.dismissed"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v3, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->initCling(I[IZI)Lcom/cyanogenmod/trebuchet/Cling;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->removeCling(I)V

    goto :goto_0
.end method

.method public showFirstRunAllAppsSortCling()V
    .locals 5

    const v4, 0x7f0d001d    # com.konka.avenger.R.id.all_apps_sort_cling

    const/4 v3, 0x0

    const-string v1, "com.cyanogenmod.trebuchet_preferences"

    invoke-virtual {p0, v1, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isClingsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "cling.allappssort.dismissed"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v4, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->initCling(I[IZI)Lcom/cyanogenmod/trebuchet/Cling;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->removeCling(I)V

    goto :goto_0
.end method

.method public showFirstRunFoldersCling()Lcom/cyanogenmod/trebuchet/Cling;
    .locals 6

    const v5, 0x7f0d003a    # com.konka.avenger.R.id.folder_cling

    const/4 v4, 0x0

    const-string v2, "com.cyanogenmod.trebuchet_preferences"

    invoke-virtual {p0, v2, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isClingsEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "cling.folder.dismissed"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v5, v2, v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->initCling(I[IZI)Lcom/cyanogenmod/trebuchet/Cling;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->removeCling(I)V

    goto :goto_0
.end method

.method public showFirstRunWorkspaceCling()V
    .locals 4

    const v3, 0x7f0d0039    # com.konka.avenger.R.id.workspace_cling

    const/4 v2, 0x0

    const-string v1, "com.cyanogenmod.trebuchet_preferences"

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->isClingsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "cling.workspace.dismissed"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v3, v1, v2, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->initCling(I[IZI)Lcom/cyanogenmod/trebuchet/Cling;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->removeCling(I)V

    goto :goto_0
.end method

.method showHotseat(Z)V
    .locals 4
    .param p1    # Z

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getTransitionInDuration()I

    move-result v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/Hotseat;->setAlpha(F)V

    goto :goto_0
.end method

.method showNotAllowedAddMessage()V
    .locals 2

    const v0, 0x7f0a0021    # com.konka.avenger.R.string.not_allowed_add

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method showOutOfSpaceMessage()V
    .locals 2

    const v0, 0x7f0a0020    # com.konka.avenger.R.string.out_of_space

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method showRenameDialog(Lcom/cyanogenmod/trebuchet/FolderInfo;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderInfo;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showDialog(I)V

    return-void
.end method

.method public showTVWindow()V
    .locals 3

    const-string v0, "Launcher"

    const-string v1, "show TV Window"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",getCurrentWorkspaceScreen = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v1, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getCurrentWorkspaceScreen()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mTVWindowManager:Lcom/konka/avenger/tv/TVWindowManager;

    sget-object v1, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->OPEN:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVWindowManager;->setState(Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Launcher"

    const-string v1, "it\'s not time to show tv window"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method showWorkspace(Z)V
    .locals 8
    .param p1    # Z

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0b0011    # com.konka.avenger.R.integer.config_appsCustomizeWorkspaceAnimationStagger

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Workspace;->getTransitionEffect()Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    move-result-object v3

    sget-object v6, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateUp:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    if-eq v3, v6, :cond_0

    sget-object v6, Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;->RotateDown:Lcom/cyanogenmod/trebuchet/Workspace$TransitionEffect;

    if-ne v3, v6, :cond_4

    :cond_0
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Workspace;->getState()Lcom/cyanogenmod/trebuchet/Workspace$State;

    move-result-object v6

    sget-object v7, Lcom/cyanogenmod/trebuchet/Workspace$State;->SMALL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    if-ne v6, v7, :cond_4

    move v0, v4

    :goto_0
    if-eqz v0, :cond_1

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    sget-object v7, Lcom/cyanogenmod/trebuchet/Workspace$State;->NORMAL:Lcom/cyanogenmod/trebuchet/Workspace$State;

    invoke-virtual {v6, v7, p1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;ZI)V

    :cond_1
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v7, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-eq v6, v7, :cond_3

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v6, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->setVisibility(I)V

    invoke-direct {p0, p1, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->hideAppsCustomizeHelper(ZZ)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v6, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->showSearchBar(Z)V

    if-eqz p1, :cond_2

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    sget-object v7, Lcom/cyanogenmod/trebuchet/Launcher$State;->APPS_CUSTOMIZE_SPRING_LOADED:Lcom/cyanogenmod/trebuchet/Launcher$State;

    if-ne v6, v7, :cond_2

    move v4, v5

    :cond_2
    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->showDockDivider(Z)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAllAppsButton:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mAllAppsButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    :cond_3
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v4, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->flashScrollingIndicator(Z)V

    sget-object v4, Lcom/cyanogenmod/trebuchet/Launcher$State;->WORKSPACE:Lcom/cyanogenmod/trebuchet/Launcher$State;

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mState:Lcom/cyanogenmod/trebuchet/Launcher$State;

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mUserPresent:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->updateRunning()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->sendAccessibilityEvent(I)V

    return-void

    :cond_4
    move v0, v5

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    if-ltz p2, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWaitingForResult:Z

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method startActivityForResultSafely(Landroid/content/Intent;I)V
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const v2, 0x7f0a000b    # com.konka.avenger.R.string.activity_not_found

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Launcher does not have the permission to launch "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "or use the exported attribute for this activity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Landroid/content/Intent;
    .param p2    # Ljava/lang/Object;

    const v3, 0x7f0a000b    # com.konka.avenger.R.string.activity_not_found

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-virtual {p1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-static {p0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to launch. tag="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {p0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Launcher does not have the permission to launch "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Make sure to create a MAIN intent-filter for the corresponding activity "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "or use the exported attribute for this activity. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "tag="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method startApplicationDetailsActivity(Landroid/content/ComponentName;)V
    .locals 5
    .param p1    # Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    const-string v3, "package"

    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v2, 0x10800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method startApplicationUninstallActivity(Lcom/cyanogenmod/trebuchet/ApplicationInfo;)V
    .locals 6
    .param p1    # Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget v4, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_0

    const v2, 0x7f0a0050    # com.konka.avenger.R.string.uninstall_system_app_text

    const/4 v4, 0x0

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.DELETE"

    const-string v5, "package"

    invoke-static {v5, v3, v0}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v4, 0x10800000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public startBinding()V
    .locals 5

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Workspace;->clearDropTargets()V

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mWidgetsToAdvance:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHotseat:Lcom/cyanogenmod/trebuchet/Hotseat;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Hotseat;->resetLayout()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v3, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeAllViewsInLayout()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Landroid/os/Bundle;
    .param p4    # Z

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->showWorkspace(Z)V

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getTypedText()Ljava/lang/String;

    move-result-object p1

    :cond_0
    if-nez p3, :cond_1

    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    const-string v1, "source"

    const-string v2, "launcher-search"

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getSearchBarBounds()Landroid/graphics/Rect;

    move-result-object v6

    const-string v1, "search"

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;ZLandroid/graphics/Rect;)V

    return-void
.end method

.method startShortcutUninstallActivity(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v4, v6, v7}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    iget-object v6, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_0

    const v2, 0x7f0a0050    # com.konka.avenger.R.string.uninstall_system_app_text

    invoke-static {p0, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.DELETE"

    const-string v7, "package"

    invoke-static {v7, v3, v0}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v6, 0x10800000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method unlockAllApps()V
    .locals 0

    return-void
.end method

.method public unlockScreenOrientationOnLargeUI()V
    .locals 4

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/cyanogenmod/trebuchet/Launcher$24;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/Launcher$24;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method updateWallpaperVisibility(Z)V
    .locals 4
    .param p1    # Z

    const/high16 v2, 0x100000

    if-eqz p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int v0, v3, v2

    if-eq v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Landroid/view/Window;->setFlags(II)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method warnTargetisLocked()V
    .locals 2

    const v0, 0x7f0a00b5    # com.konka.avenger.R.string.target_is_locked

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
