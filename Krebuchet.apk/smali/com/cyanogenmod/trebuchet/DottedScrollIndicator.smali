.class public Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;
.super Landroid/widget/LinearLayout;
.source "DottedScrollIndicator.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/cyanogenmod/trebuchet/ScrollIndicator;


# instance fields
.field private mActiveDot:I

.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mDots:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mPaddingLeft:I

.field private mPaddingRight:I

.field private mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mActiveDot:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mActiveDot:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    iput v2, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mActiveDot:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->init()V

    sget-object v1, Lcom/konka/avenger/R$styleable;->PagedView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPaddingLeft:I

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPaddingRight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private addIndicator()V
    .locals 7

    const/4 v6, -0x2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mInflater:Landroid/view/LayoutInflater;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->getPageIconRes(I)I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2, v1}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private getPageIconRes(I)I
    .locals 4
    .param p1    # I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    instance-of v3, v3, Lcom/cyanogenmod/trebuchet/Workspace;

    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    check-cast v2, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getHomeScreenIndex()I

    move-result v0

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getSearchScreenIndex()I

    move-result v1

    if-ne p1, v1, :cond_0

    const v3, 0x7f030028    # com.konka.avenger.R.layout.search_indicator_item

    :goto_0
    return v3

    :cond_0
    if-ne p1, v0, :cond_1

    const v3, 0x7f030013    # com.konka.avenger.R.layout.home_indicator_item

    goto :goto_0

    :cond_1
    const v3, 0x7f03000b    # com.konka.avenger.R.layout.dotted_indicator_item

    goto :goto_0
.end method

.method private init()V
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private updateScrollingIndicator()V
    .locals 7

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v4

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageNearestToCenterOfScreen()I

    move-result v0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v1, v4, :cond_4

    const/4 v3, 0x1

    move v2, v1

    :goto_0
    add-int/lit8 v1, v2, 0x1

    if-lt v2, v4, :cond_3

    :cond_0
    iget v5, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mActiveDot:I

    if-eq v5, v0, :cond_1

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    iget v6, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mActiveDot:I

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mActiveDot:I

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->invalidate()V

    :cond_2
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->addIndicator()V

    move v2, v1

    goto :goto_0

    :cond_4
    if-le v1, v4, :cond_0

    const/4 v3, 0x1

    move v2, v1

    :goto_1
    add-int/lit8 v1, v2, -0x1

    if-le v2, v4, :cond_0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public cancelAnimations()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method public hide(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->updateScrollingIndicator()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->cancelAnimations()V

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "alpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator$1;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator$1;-><init>(Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public init(Lcom/cyanogenmod/trebuchet/PagedView;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/PagedView;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->addIndicator()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isElasticScrollIndicator()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mDots:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mActiveDot:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    :cond_0
    return-void
.end method

.method public show(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->updateScrollingIndicator()V

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->cancelAnimations()V

    if-eqz p1, :cond_0

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "alpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    aput v2, v1, v3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public update()V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DottedScrollIndicator;->updateScrollingIndicator()V

    return-void
.end method
