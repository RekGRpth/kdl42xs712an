.class final Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;
.super Landroid/os/Handler;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "NetworkMonitorHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;-><init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveInterface:I
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$2(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthHWConnected:Z
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$3(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$4(Lcom/cyanogenmod/trebuchet/NetworkMonitor;I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$5(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$6(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$7(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$7(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # invokes: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->getCurrentConnectivityInfo()Landroid/os/Bundle;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$8(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;->onUpdateNetworkConnectivity(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method
