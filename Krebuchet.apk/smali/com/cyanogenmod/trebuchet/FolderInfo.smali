.class Lcom/cyanogenmod/trebuchet/FolderInfo;
.super Lcom/cyanogenmod/trebuchet/ItemInfo;
.source "FolderInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;
    }
.end annotation


# instance fields
.field contents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ShortcutInfo;",
            ">;"
        }
    .end annotation
.end field

.field listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;",
            ">;"
        }
    .end annotation
.end field

.field opened:Z

.field title:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/ItemInfo;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    const/4 v0, 0x2

    iput v0, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->itemType:I

    return-void
.end method


# virtual methods
.method public add(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderInfo;->itemsChanged()V

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;

    invoke-interface {v0, p1}, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;->onAdd(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    goto :goto_0
.end method

.method addListener(Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method itemsChanged()V
    .locals 3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;->onItemsChanged()V

    goto :goto_0
.end method

.method onAddToDatabase(Landroid/content/ContentValues;)V
    .locals 2
    .param p1    # Landroid/content/ContentValues;

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/ItemInfo;->onAddToDatabase(Landroid/content/ContentValues;)V

    const-string v0, "title"

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public remove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderInfo;->itemsChanged()V

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;

    invoke-interface {v0, p1}, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;->onRemove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    goto :goto_0
.end method

.method removeListener(Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;

    invoke-interface {v0, p1}, Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;->onTitleChanged(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method unbind()V
    .locals 1

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/ItemInfo;->unbind()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderInfo;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method
