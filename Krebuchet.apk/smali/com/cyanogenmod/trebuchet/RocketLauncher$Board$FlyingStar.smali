.class public Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingStar;
.super Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;
.source "RocketLauncher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FlyingStar"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/util/AttributeSet;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingStar;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-direct {p0, p1, p2, p3}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;-><init>(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public randomize()V
    .locals 2

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->randomize()V

    const v0, 0x443b8000    # 750.0f

    const/high16 v1, 0x44fa0000    # 2000.0f

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->randfrange(FF)F

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingStar;->v:F

    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->randfrange(FF)F

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingStar;->endscale:F

    return-void
.end method

.method public randomizeIcon()V
    .locals 1

    const v0, 0x7f0200d8    # com.konka.avenger.R.drawable.widget_resize_handle_bottom

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingStar;->setImageResource(I)V

    return-void
.end method
