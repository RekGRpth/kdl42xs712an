.class Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;
.super Ljava/lang/Object;
.source "DragLayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/DragLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowHintRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/DragLayer;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/DragLayer;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/DragLayer;Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$0(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintText:Ljava/lang/String;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$1(Lcom/cyanogenmod/trebuchet/DragLayer;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$0(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v1, v1}, Landroid/widget/TextView;->measure(II)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$0(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$0(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$2(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$2(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$2(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$2(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintAnchor:Landroid/view/View;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$3(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V

    :goto_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$4(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHideHintRunnalbe:Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$5(Lcom/cyanogenmod/trebuchet/DragLayer;)Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;

    move-result-object v4

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintDuration:J
    invoke-static {v5}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$6(Lcom/cyanogenmod/trebuchet/DragLayer;)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$2(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintAnchor:Landroid/view/View;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$3(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4, v2, v0}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V

    goto :goto_0
.end method
