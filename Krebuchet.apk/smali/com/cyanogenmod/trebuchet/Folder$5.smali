.class Lcom/cyanogenmod/trebuchet/Folder$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Folder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Folder;->animateOpen(Lcom/cyanogenmod/trebuchet/Folder;[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Folder;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Folder;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1    # Landroid/animation/Animator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/cyanogenmod/trebuchet/Folder;->access$5(Lcom/cyanogenmod/trebuchet/Folder;I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showFirstRunFoldersCling()Lcom/cyanogenmod/trebuchet/Cling;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Cling;->bringToFront()V

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0007    # com.konka.avenger.R.string.folder_name

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    # getter for: Lcom/cyanogenmod/trebuchet/Folder;->mFolderState:I
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Folder;->access$6(Lcom/cyanogenmod/trebuchet/Folder;)I

    move-result v2

    # getter for: Lcom/cyanogenmod/trebuchet/Folder;->STATE_TO_EDIT:I
    invoke-static {}, Lcom/cyanogenmod/trebuchet/Folder;->access$7()I

    move-result v3

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    # invokes: Lcom/cyanogenmod/trebuchet/Folder;->setFocusOnEditText()V
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Folder;->access$8(Lcom/cyanogenmod/trebuchet/Folder;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    # getter for: Lcom/cyanogenmod/trebuchet/Folder;->STATE_TO_RUN:I
    invoke-static {}, Lcom/cyanogenmod/trebuchet/Folder;->access$9()I

    move-result v3

    invoke-static {v2, v3}, Lcom/cyanogenmod/trebuchet/Folder;->access$10(Lcom/cyanogenmod/trebuchet/Folder;I)V

    :goto_0
    return-void

    :cond_3
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Folder;->setFocusOnFirstChild()V

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 7
    .param p1    # Landroid/animation/Animator;

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    # getter for: Lcom/cyanogenmod/trebuchet/Folder;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Folder;->access$3(Lcom/cyanogenmod/trebuchet/Folder;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0062    # com.konka.avenger.R.string.folder_opened

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v4, v4, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountY()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/cyanogenmod/trebuchet/Folder;->sendCustomAccessibilityEvent(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Folder;->access$4(Lcom/cyanogenmod/trebuchet/Folder;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder$5;->this$0:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-static {v0, v6}, Lcom/cyanogenmod/trebuchet/Folder;->access$5(Lcom/cyanogenmod/trebuchet/Folder;I)V

    return-void
.end method
