.class public Lcom/cyanogenmod/trebuchet/PagedViewWidget;
.super Landroid/widget/LinearLayout;
.source "PagedViewWidget.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field static final TAG:Ljava/lang/String; = "PagedViewWidgetLayout"

.field private static sDeletePreviewsWhenDetachedFromWindow:Z


# instance fields
.field private mAlpha:I

.field private mCheckedAlpha:F

.field private mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

.field private mCheckedFadeInDuration:I

.field private mCheckedFadeOutDuration:I

.field private mDimensionsFormatString:Ljava/lang/String;

.field private mHolographicAlpha:I

.field private mHolographicOutline:Landroid/graphics/Bitmap;

.field private mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

.field private mIsChecked:Z

.field private final mPaint:Landroid/graphics/Paint;

.field private mPreviewImageView:Landroid/widget/ImageView;

.field private final mTmpScaleRect:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->sDeletePreviewsWhenDetachedFromWindow:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v3, 0x7f0b0003    # com.konka.avenger.R.integer.config_dragAppsCustomizeIconFadeAlpha

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mTmpScaleRect:Landroid/graphics/RectF;

    const/16 v2, 0xff

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mAlpha:I

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlpha:F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x43800000    # 256.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlpha:F

    const v2, 0x7f0b0001    # com.konka.avenger.R.integer.config_dragAppsCustomizeIconFadeInDuration

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedFadeInDuration:I

    const v2, 0x7f0b0002    # com.konka.avenger.R.integer.config_dragAppsCustomizeIconFadeOutDuration

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedFadeOutDuration:I

    :cond_0
    const v2, 0x7f0a0013    # com.konka.avenger.R.string.widget_dims_format

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mDimensionsFormatString:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setWillNotDraw(Z)V

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setClipToPadding(Z)V

    return-void
.end method

.method private setChildrenAlpha(F)V
    .locals 3
    .param p1    # F

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setAlpha(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static setDeletePreviewsWhenDetachedFromWindow(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->sDeletePreviewsWhenDetachedFromWindow:Z

    return-void
.end method


# virtual methods
.method public applyFromAppWidgetProviderInfo(Landroid/appwidget/AppWidgetProviderInfo;I[ILcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V
    .locals 8
    .param p1    # Landroid/appwidget/AppWidgetProviderInfo;
    .param p2    # I
    .param p3    # [I
    .param p4    # Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    const/4 v7, 0x1

    const/4 v6, 0x0

    iput-object p4, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    const v3, 0x7f0d0022    # com.konka.avenger.R.id.widget_preview

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v3, -0x1

    if-le p2, v3, :cond_0

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setMaxWidth(I)V

    :cond_0
    iget-object v3, p1, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPreviewImageView:Landroid/widget/ImageView;

    const v3, 0x7f0d0020    # com.konka.avenger.R.id.widget_name

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p1, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0d0021    # com.konka.avenger.R.id.widget_dims

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mDimensionsFormatString:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aget v5, p3, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aget v5, p3, v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public applyFromResolveInfo(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V
    .locals 9
    .param p1    # Landroid/content/pm/PackageManager;
    .param p2    # Landroid/content/pm/ResolveInfo;
    .param p3    # Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    const/4 v8, 0x1

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {p2, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    const v4, 0x7f0d0022    # com.konka.avenger.R.id.widget_preview

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPreviewImageView:Landroid/widget/ImageView;

    const v4, 0x7f0d0020    # com.konka.avenger.R.id.widget_name

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0d0021    # com.konka.avenger.R.id.widget_dims

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mDimensionsFormatString:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method applyPreview(Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    const v1, 0x7f0d0022    # com.konka.avenger.R.id.widget_preview

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/PagedViewWidgetImageView;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewWidgetImageView;->mAllowRequestLayout:Z

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWidgetImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewWidgetImageView;->setAlpha(F)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewWidgetImageView;->mAllowRequestLayout:Z

    :cond_0
    return-void
.end method

.method public getPreviewSize()[I
    .locals 5

    const v2, 0x7f0d0022    # com.konka.avenger.R.id.widget_preview

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v2, 0x2

    new-array v1, v2, [I

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    aput v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    aput v3, v1, v2

    return-object v1
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mIsChecked:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    sget-boolean v2, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->sDeletePreviewsWhenDetachedFromWindow:Z

    if-eqz v2, :cond_1

    const v2, 0x7f0d0022    # com.konka.avenger.R.id.widget_preview

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mAlpha:I

    if-lez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicOutline:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicAlpha:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mTmpScaleRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPreviewImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mTmpScaleRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mTmpScaleRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mTmpScaleRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicOutline:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPreviewImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLeft()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPreviewImageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return-void
.end method

.method protected onSetAlpha(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setAlpha(F)V
    .locals 6
    .param p1    # F

    const/high16 v5, 0x437f0000    # 255.0f

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->viewAlphaInterpolator(F)F

    move-result v3

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->highlightAlphaInterpolator(F)F

    move-result v0

    mul-float v4, v3, v5

    float-to-int v2, v4

    mul-float v4, v0, v5

    float-to-int v1, v4

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mAlpha:I

    if-ne v4, v2, :cond_0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicAlpha:I

    if-eq v4, v1, :cond_1

    :cond_0
    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mAlpha:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicAlpha:I

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setChildrenAlpha(F)V

    invoke-super {p0, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    :cond_1
    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setChecked(ZZ)V

    return-void
.end method

.method setChecked(ZZ)V
    .locals 6
    .param p1    # Z
    .param p2    # Z

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mIsChecked:Z

    if-eq v2, p1, :cond_1

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mIsChecked:Z

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mIsChecked:Z

    if-eqz v2, :cond_2

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlpha:F

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedFadeInDuration:I

    :goto_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    if-eqz p2, :cond_3

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->getAlpha()F

    move-result v5

    aput v5, v3, v4

    const/4 v4, 0x1

    aput v0, v3, v4

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v3, v1

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    :goto_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->invalidate()V

    :cond_1
    return-void

    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mCheckedFadeOutDuration:I

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setAlpha(F)V

    goto :goto_1
.end method

.method public setHolographicOutline(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mHolographicOutline:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->invalidate()V

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->mIsChecked:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
