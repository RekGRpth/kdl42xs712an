.class public Lcom/cyanogenmod/trebuchet/LauncherModel;
.super Landroid/content/BroadcastReceiver;
.source "LauncherModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;,
        Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;,
        Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;,
        Lcom/cyanogenmod/trebuchet/LauncherModel$ShortcutNameComparator;,
        Lcom/cyanogenmod/trebuchet/LauncherModel$WidgetAndShortcutNameComparator;
    }
.end annotation


# static fields
.field public static final APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final APP_KONKA_DEFAULT_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final APP_NAME_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final DEBUG_LOADERS:Z = true

.field private static final ITEMS_CHUNK:I = 0xa

.field static final TAG:Ljava/lang/String; = "Launcher.Model"

.field public static final WIDGET_NAME_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static mCellCountX:I

.field private static mCellCountY:I

.field static final sAppWidgets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCollator:Ljava/text/Collator;

.field static final sDbIconCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "[B>;"
        }
    .end annotation
.end field

.field static final sFolders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/cyanogenmod/trebuchet/FolderInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final sItemsIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/cyanogenmod/trebuchet/ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final sWorker:Landroid/os/Handler;

.field private static final sWorkerThread:Landroid/os/HandlerThread;

.field static final sWorkspaceItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ItemInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;

.field private mAllAppsLoaded:Z

.field private final mApp:Lcom/cyanogenmod/trebuchet/LauncherApplication;

.field private final mAppsCanBeOnExternalStorage:Z

.field private mCallbacks:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultIcon:Landroid/graphics/Bitmap;

.field private mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;

.field private mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

.field private mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

.field private final mLock:Ljava/lang/Object;

.field protected mPreviousConfigMcc:I

.field private mWorkspaceLoaded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "launcher-loader"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sItemsIdMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sAppWidgets:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sFolders:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sDbIconCache:Ljava/util/HashMap;

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sCollator:Ljava/text/Collator;

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherModel$1;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$1;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_NAME_COMPARATOR:Ljava/util/Comparator;

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherModel$2;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$2;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherModel$3;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$3;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_KONKA_DEFAULT_COMPARATOR:Ljava/util/Comparator;

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherModel$4;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$4;-><init>()V

    sput-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->WIDGET_NAME_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(Lcom/cyanogenmod/trebuchet/LauncherApplication;Lcom/cyanogenmod/trebuchet/IconCache;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/LauncherApplication;
    .param p2    # Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;

    new-instance v2, Lcom/cyanogenmod/trebuchet/DeferredHandler;

    invoke-direct {v2}, Lcom/cyanogenmod/trebuchet/DeferredHandler;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;

    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAppsCanBeOnExternalStorage:Z

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mApp:Lcom/cyanogenmod/trebuchet/LauncherApplication;

    new-instance v2, Lcom/cyanogenmod/trebuchet/AllAppsList;

    invoke-direct {v2, p2}, Lcom/cyanogenmod/trebuchet/AllAppsList;-><init>(Lcom/cyanogenmod/trebuchet/IconCache;)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/IconCache;->getFullResDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/cyanogenmod/trebuchet/Utilities;->createIconBitmap(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mDefaultIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v2, v0, Landroid/content/res/Configuration;->mcc:I

    iput v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mPreviousConfigMcc:I

    return-void

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method static synthetic access$0()Ljava/text/Collator;
    .locals 1

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sCollator:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/LauncherModel;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mWorkspaceLoaded:Z

    return v0
.end method

.method static synthetic access$10(Lcom/cyanogenmod/trebuchet/LauncherModel;Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .locals 1

    invoke-direct/range {p0 .. p7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getShortcutInfo(Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$11(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->findOrMakeFolder(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->unbindWorkspaceItemsOnMainThread()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13(Lcom/cyanogenmod/trebuchet/LauncherModel;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsLoaded:Z

    return v0
.end method

.method static synthetic access$14(Lcom/cyanogenmod/trebuchet/LauncherModel;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsLoaded:Z

    return-void
.end method

.method static synthetic access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;

    return-object v0
.end method

.method static synthetic access$16(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/IconCache;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    return-object v0
.end method

.method static synthetic access$17(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/LauncherApplication;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mApp:Lcom/cyanogenmod/trebuchet/LauncherApplication;

    return-object v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/LauncherModel;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mWorkspaceLoaded:Z

    return-void
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    return-object v0
.end method

.method static synthetic access$7(Lcom/cyanogenmod/trebuchet/LauncherModel;Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    return-void
.end method

.method static synthetic access$8()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountX:I

    return v0
.end method

.method static synthetic access$9()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountY:I

    return v0
.end method

.method static addItemToDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIIIZ)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Z

    iput-wide p2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iput p5, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iput p6, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    instance-of v4, p0, Lcom/cyanogenmod/trebuchet/Launcher;

    if-eqz v4, :cond_0

    if-gez p4, :cond_0

    const-wide/16 v4, -0x65

    cmp-long v4, p2, v4

    if-nez v4, :cond_0

    move-object v4, p0

    check-cast v4, Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v4

    invoke-virtual {v4, p5, p6}, Lcom/cyanogenmod/trebuchet/Hotseat;->getOrderInHotseat(II)I

    move-result v4

    iput v4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    :goto_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p1, v3}, Lcom/cyanogenmod/trebuchet/ItemInfo;->onAddToDatabase(Landroid/content/ContentValues;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherApplication;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getLauncherProvider()Lcom/cyanogenmod/trebuchet/LauncherProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/LauncherProvider;->generateNewId()J

    move-result-wide v4

    iput-wide v4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->id:J

    const-string v4, "_id"

    iget-wide v5, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->id:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget v4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v5, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    invoke-virtual {p1, v3, v4, v5}, Lcom/cyanogenmod/trebuchet/ItemInfo;->updateValuesWithCoordinates(Landroid/content/ContentValues;II)V

    new-instance v2, Lcom/cyanogenmod/trebuchet/LauncherModel$8;

    invoke-direct {v2, v1, p7, v3, p1}, Lcom/cyanogenmod/trebuchet/LauncherModel$8;-><init>(Landroid/content/ContentResolver;ZLandroid/content/ContentValues;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v4

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v5

    if-ne v4, v5, :cond_1

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    :goto_1
    return-void

    :cond_0
    iput p4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method static addOrMoveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    iget-wide v0, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIIIZ)V

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p0 .. p6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->moveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    goto :goto_0
.end method

.method static deleteFolderContentsFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/FolderInfo;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$10;

    invoke-direct {v1, v0, p1}, Lcom/cyanogenmod/trebuchet/LauncherModel$10;-><init>(Landroid/content/ContentResolver;Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v3, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->id:J

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->getContentUri(JZ)Landroid/net/Uri;

    move-result-object v2

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$9;

    invoke-direct {v1, v0, v2, p1}, Lcom/cyanogenmod/trebuchet/LauncherModel$9;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    sget-object v3, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v3

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    sget-object v3, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private static findOrMakeFolder(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;
    .locals 2
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/cyanogenmod/trebuchet/FolderInfo;",
            ">;J)",
            "Lcom/cyanogenmod/trebuchet/FolderInfo;"
        }
    .end annotation

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/FolderInfo;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private forceReload()V
    .locals 2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->stopLoaderLocked()Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsLoaded:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mWorkspaceLoaded:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->startLoaderFromBackground()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static getCellCountX()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountX:I

    return v0
.end method

.method static getCellCountY()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountY:I

    return v0
.end method

.method static getCellLayoutChildId(JIIIII)I
    .locals 2
    .param p0    # J
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    long-to-int v0, p0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    and-int/lit16 v1, p2, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, p3, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, p4, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method static getComponentNameFromResolveInfo(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;
    .locals 3
    .param p0    # Landroid/content/pm/ResolveInfo;

    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static getItemsInLocalCoordinates(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 19
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ItemInfo;",
            ">;"
        }
    .end annotation

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->CONTENT_URI:Landroid/net/Uri;

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "itemType"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "container"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "screen"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "cellX"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "cellY"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "spanX"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "spanY"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "lock"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const-string v2, "itemType"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    const-string v2, "container"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    const-string v2, "screen"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    const-string v2, "cellX"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    const-string v2, "cellY"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    const-string v2, "spanX"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v17

    const-string v2, "spanY"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v18

    const-string v2, "lock"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    :goto_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v14

    :cond_0
    :try_start_1
    new-instance v12, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-direct {v12}, Lcom/cyanogenmod/trebuchet/ItemInfo;-><init>()V

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    move/from16 v0, v17

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    move/from16 v0, v18

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    invoke-interface {v7, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    move/from16 v0, v16

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    invoke-interface {v7, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_2
    iput-boolean v2, v12, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v11

    :try_start_2
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_1
    const/4 v2, 0x1

    goto :goto_2

    :catchall_0
    move-exception v2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method private getShortcutInfo(Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .locals 11
    .param p1    # Landroid/database/Cursor;
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    const/4 v1, 0x0

    new-instance v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-direct {v4}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;-><init>()V

    const/4 v9, 0x1

    iput v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->itemType:I

    move/from16 v0, p7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    invoke-interface {p1, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getFallbackIcon()Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v9, 0x1

    iput-boolean v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->usingFallbackIcon:Z

    const/4 v9, 0x0

    iput-boolean v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->customIcon:Z

    :cond_0
    :goto_0
    invoke-virtual {v4, v1}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->setIcon(Landroid/graphics/Bitmap;)V

    return-object v4

    :pswitch_0
    invoke-interface {p1, p4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move/from16 v0, p5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v9, 0x0

    iput-boolean v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->customIcon:Z

    :try_start_0
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v8

    if-eqz v8, :cond_1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v7, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {v9, v8, v3}, Lcom/cyanogenmod/trebuchet/IconCache;->getFullResIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-static {v9, p2}, Lcom/cyanogenmod/trebuchet/Utilities;->createIconBitmap(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_1
    :goto_1
    if-nez v1, :cond_2

    move/from16 v0, p6

    invoke-virtual {p0, p1, v0, p2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getIconFromCursor(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getFallbackIcon()Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v9, 0x1

    iput-boolean v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->usingFallbackIcon:Z

    goto :goto_0

    :pswitch_1
    move/from16 v0, p6

    invoke-virtual {p0, p1, v0, p2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getIconFromCursor(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getFallbackIcon()Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v9, 0x0

    iput-boolean v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->customIcon:Z

    const/4 v9, 0x1

    iput-boolean v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->usingFallbackIcon:Z

    goto :goto_0

    :cond_3
    const/4 v9, 0x1

    iput-boolean v9, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->customIcon:Z

    goto :goto_0

    :catch_0
    move-exception v9

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static moveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    iput-wide p2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    iput p5, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iput p6, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    instance-of v1, p0, Lcom/cyanogenmod/trebuchet/Launcher;

    if-eqz v1, :cond_0

    if-gez p4, :cond_0

    const-wide/16 v1, -0x65

    cmp-long v1, p2, v1

    if-nez v1, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getHotseat()Lcom/cyanogenmod/trebuchet/Hotseat;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Lcom/cyanogenmod/trebuchet/Hotseat;->getOrderInHotseat(II)I

    move-result v1

    iput v1, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "container"

    iget-wide v2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "cellX"

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "cellY"

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "screen"

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "moveItemInDatabase"

    invoke-static {p0, v0, p1, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateItemInDatabaseHelper(Landroid/content/Context;Landroid/content/ContentValues;Lcom/cyanogenmod/trebuchet/ItemInfo;Ljava/lang/String;)V

    return-void

    :cond_0
    iput p4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    goto :goto_0
.end method

.method static resizeItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;IIII)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput p4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    iput p5, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    iput p2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iput p3, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "container"

    iget-wide v2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "spanX"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "spanY"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "cellX"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "cellY"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "resizeItemInDatabase"

    invoke-static {p0, v0, p1, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateItemInDatabaseHelper(Landroid/content/Context;Landroid/content/ContentValues;Lcom/cyanogenmod/trebuchet/ItemInfo;Ljava/lang/String;)V

    return-void
.end method

.method static shortcutExists(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Z
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Intent;

    const/4 v4, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "title"

    aput-object v3, v2, v5

    const-string v3, "intent"

    aput-object v3, v2, v8

    const-string v3, "title=? and intent=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-virtual {p2, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7

    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private stopLoaderLocked()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->isLaunching()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->stopLocked()V

    :cond_1
    return v0
.end method

.method private unbindWorkspaceItemsOnMainThread()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ItemInfo;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sAppWidgets:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;

    new-instance v3, Lcom/cyanogenmod/trebuchet/LauncherModel$6;

    invoke-direct {v3, p0, v1, v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$6;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    return-object v1
.end method

.method static updateItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {p1, v0}, Lcom/cyanogenmod/trebuchet/ItemInfo;->onAddToDatabase(Landroid/content/ContentValues;)V

    iget v1, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/ItemInfo;->updateValuesWithCoordinates(Landroid/content/ContentValues;II)V

    const-string v1, "updateItemInDatabase"

    invoke-static {p0, v0, p1, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateItemInDatabaseHelper(Landroid/content/Context;Landroid/content/ContentValues;Lcom/cyanogenmod/trebuchet/ItemInfo;Ljava/lang/String;)V

    return-void
.end method

.method static updateItemInDatabaseHelper(Landroid/content/Context;Landroid/content/ContentValues;Lcom/cyanogenmod/trebuchet/ItemInfo;Ljava/lang/String;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/ContentValues;
    .param p2    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p3    # Ljava/lang/String;

    iget-wide v4, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->id:J

    const/4 v3, 0x0

    invoke-static {v4, v5, v3}, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->getContentUri(JZ)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;

    move-object v3, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/LauncherModel$7;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;JLcom/cyanogenmod/trebuchet/ItemInfo;Ljava/lang/String;)V

    sget-object v3, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v3

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v6

    if-ne v3, v6, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    sget-object v3, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static updateWorkspaceLayoutCells(II)V
    .locals 0
    .param p0    # I
    .param p1    # I

    sput p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountX:I

    sput p1, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountY:I

    return-void
.end method


# virtual methods
.method addShortcut(Landroid/content/Context;Landroid/content/Intent;JIIIZ)Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->infoFromShortcutIntent(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    move-object v0, p1

    move-wide v2, p3

    move v4, p5

    move v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-static/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addItemToDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIIIZ)V

    goto :goto_0
.end method

.method public dumpState()V
    .locals 3

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mCallbacks="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.data"

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/AllAppsList;->data:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->dumpApplicationInfoList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.added"

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/AllAppsList;->added:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->dumpApplicationInfoList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.removed"

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/AllAppsList;->removed:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->dumpApplicationInfoList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.modified"

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/AllAppsList;->modified:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->dumpApplicationInfoList(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->dumpState()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Launcher.Model"

    const-string v1, "mLoaderTask=null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method enqueuePackageUpdated(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method findAppWidgetProviderInfoWithComponent(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/appwidget/AppWidgetProviderInfo;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/ComponentName;

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v3, p2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0
.end method

.method public getFallbackIcon()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mDefaultIcon:Landroid/graphics/Bitmap;

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method getFolderById(Landroid/content/Context;Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;
    .locals 18
    .param p1    # Landroid/content/Context;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/cyanogenmod/trebuchet/FolderInfo;",
            ">;J)",
            "Lcom/cyanogenmod/trebuchet/FolderInfo;"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "_id=? and (itemType=? or itemType=?)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v6, v7

    const/4 v7, 0x1

    const/16 v17, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "itemType"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    const-string v3, "title"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    const-string v3, "container"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    const-string v3, "screen"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    const-string v3, "cellX"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    const-string v3, "cellY"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    const-string v3, "lock"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    const/4 v12, 0x0

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    move/from16 v0, v16

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    move-wide/from16 v0, p3

    iput-wide v0, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    invoke-interface {v8, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v3, v3

    iput-wide v3, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->container:J

    invoke-interface {v8, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->screen:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellX:I

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellY:I

    invoke-interface {v8, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_1
    iput-boolean v3, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->isLock:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_2
    return-object v12

    :pswitch_0
    :try_start_1
    invoke-static/range {p2 .. p4}, Lcom/cyanogenmod/trebuchet/LauncherModel;->findOrMakeFolder(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    goto :goto_1

    :catchall_0
    move-exception v3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    const/4 v12, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method getIconFromCursor(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # Landroid/content/Context;

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    array-length v3, v0

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2, p3}, Lcom/cyanogenmod/trebuchet/Utilities;->createIconBitmap(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getShortcutInfo(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .locals 8
    .param p1    # Landroid/content/pm/PackageManager;
    .param p2    # Landroid/content/Intent;
    .param p3    # Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, v5

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getShortcutInfo(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v0

    return-object v0
.end method

.method public getShortcutInfo(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .locals 11
    .param p1    # Landroid/content/pm/PackageManager;
    .param p2    # Landroid/content/Intent;
    .param p3    # Landroid/content/Context;
    .param p4    # Landroid/database/Cursor;
    .param p5    # I
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Landroid/content/Intent;",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "II",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/CharSequence;",
            ">;)",
            "Lcom/cyanogenmod/trebuchet/ShortcutInfo;"
        }
    .end annotation

    const/4 v3, 0x0

    new-instance v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-direct {v4}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;-><init>()V

    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    :try_start_0
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v8, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v8, v8, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v8, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v8, "Launcher.Model"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getPackInfo failed for package "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v8, 0x0

    invoke-virtual {p1, p2, v8}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    if-eqz v7, :cond_2

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    move-object/from16 v0, p7

    invoke-virtual {v8, v1, v7, v0}, Lcom/cyanogenmod/trebuchet/IconCache;->getIcon(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)Landroid/graphics/Bitmap;

    move-result-object v3

    :cond_2
    if-nez v3, :cond_3

    if-eqz p4, :cond_3

    move/from16 v0, p5

    invoke-virtual {p0, p4, v0, p3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getIconFromCursor(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v3

    :cond_3
    if-nez v3, :cond_4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getFallbackIcon()Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v8, 0x1

    iput-boolean v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->usingFallbackIcon:Z

    :cond_4
    invoke-virtual {v4, v3}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->setIcon(Landroid/graphics/Bitmap;)V

    if-eqz v7, :cond_5

    invoke-static {v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getComponentNameFromResolveInfo(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v5

    if-eqz p7, :cond_8

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    move-object/from16 v0, p7

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    iput-object v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    :cond_5
    :goto_1
    iget-object v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    if-nez v8, :cond_6

    if-eqz p4, :cond_6

    move/from16 v0, p6

    invoke-interface {p4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    :cond_6
    iget-object v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    if-nez v8, :cond_7

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    :cond_7
    const/4 v8, 0x0

    iput v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->itemType:I

    goto/16 :goto_0

    :cond_8
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v8, p1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    iput-object v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    if-eqz p7, :cond_5

    iget-object v8, v4, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    move-object/from16 v0, p7

    invoke-virtual {v0, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method infoFromShortcutIntent(Landroid/content/Context;Landroid/content/Intent;Landroid/graphics/Bitmap;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .locals 16
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;
    .param p3    # Landroid/graphics/Bitmap;

    const-string v13, "android.intent.extra.shortcut.INTENT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/content/Intent;

    const-string v13, "android.intent.extra.shortcut.NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v13, "android.intent.extra.shortcut.ICON"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v9, :cond_0

    const-string v13, "Launcher.Model"

    const-string v14, "Can\'t construct ShorcutInfo with null intent"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    :goto_0
    return-object v8

    :cond_0
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x0

    if-eqz v1, :cond_3

    instance-of v13, v1, Landroid/graphics/Bitmap;

    if-eqz v13, :cond_3

    new-instance v13, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-direct {v13, v1}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/cyanogenmod/trebuchet/Utilities;->createIconBitmap(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v5

    const/4 v2, 0x1

    :cond_1
    :goto_1
    new-instance v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-direct {v8}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;-><init>()V

    if-nez v5, :cond_2

    if-eqz p3, :cond_4

    move-object/from16 v5, p3

    :cond_2
    :goto_2
    invoke-virtual {v8, v5}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->setIcon(Landroid/graphics/Bitmap;)V

    iput-object v10, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    iput-object v9, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    iput-boolean v2, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->customIcon:Z

    iput-object v6, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->iconResource:Landroid/content/Intent$ShortcutIconResource;

    goto :goto_0

    :cond_3
    const-string v13, "android.intent.extra.shortcut.ICON_RESOURCE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    if-eqz v4, :cond_1

    instance-of v13, v4, Landroid/content/Intent$ShortcutIconResource;

    if-eqz v13, :cond_1

    :try_start_0
    move-object v0, v4

    check-cast v0, Landroid/content/Intent$ShortcutIconResource;

    move-object v6, v0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    iget-object v13, v6, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v11, v13}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v12

    iget-object v13, v6, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {v13, v12, v7}, Lcom/cyanogenmod/trebuchet/IconCache;->getFullResIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/cyanogenmod/trebuchet/Utilities;->createIconBitmap(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_1

    :catch_0
    move-exception v3

    const-string v13, "Launcher.Model"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "Could not load shortcut icon: "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getFallbackIcon()Landroid/graphics/Bitmap;

    move-result-object v5

    const/4 v13, 0x1

    iput-boolean v13, v8, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->usingFallbackIcon:Z

    goto :goto_2
.end method

.method public initialize(Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isAllAppsLoaded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsLoaded:Z

    return v0
.end method

.method public isUpdateWorkspace([Ljava/lang/String;)Z
    .locals 4
    .param p1    # [Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    array-length v2, p1

    if-gtz v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const-string v2, "com.facebook.katana"

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "Launcher.Model"

    const-string v2, " not update workspace ======= ========"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-string v7, "Launcher.Model"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "onReceive intent="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    const-string v7, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const/4 v3, 0x0

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v3, 0x2

    :cond_3
    :goto_1
    if-eqz v3, :cond_1

    new-instance v7, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

    new-array v8, v11, [Ljava/lang/String;

    aput-object v4, v8, v10

    invoke-direct {v7, p0, v3, v8}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel;I[Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->enqueuePackageUpdated(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;)V

    goto :goto_0

    :cond_4
    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    if-nez v6, :cond_3

    const/4 v3, 0x3

    goto :goto_1

    :cond_5
    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    if-nez v6, :cond_6

    const/4 v3, 0x1

    goto :goto_1

    :cond_6
    const/4 v3, 0x2

    goto :goto_1

    :cond_7
    const-string v7, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "android.intent.extra.changed_package_list"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    new-instance v7, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

    invoke-direct {v7, p0, v11, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel;I[Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->enqueuePackageUpdated(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->startLoaderFromBackground()V

    goto :goto_0

    :cond_8
    const-string v7, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    const-string v7, "android.intent.extra.changed_package_list"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    new-instance v7, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

    const/4 v8, 0x4

    invoke-direct {v7, p0, v8, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel;I[Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/LauncherModel;->enqueuePackageUpdated(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;)V

    goto :goto_0

    :cond_9
    const-string v7, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->forceReload()V

    goto :goto_0

    :cond_a
    const-string v7, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v7, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mPreviousConfigMcc:I

    iget v8, v2, Landroid/content/res/Configuration;->mcc:I

    if-eq v7, v8, :cond_b

    const-string v7, "Launcher.Model"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Reload apps on config change. curr_mcc:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v2, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " prevmcc:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mPreviousConfigMcc:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->forceReload()V

    :cond_b
    iget v7, v2, Landroid/content/res/Configuration;->mcc:I

    iput v7, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mPreviousConfigMcc:I

    goto/16 :goto_0

    :cond_c
    const-string v7, "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, "android.search.action.SEARCHABLES_CHANGED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_d
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;->bindSearchablesChanged()V

    goto/16 :goto_0
.end method

.method queueIconToBeChecked(Ljava/util/HashMap;Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/database/Cursor;I)Z
    .locals 2
    .param p2    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "[B>;",
            "Lcom/cyanogenmod/trebuchet/ShortcutInfo;",
            "Landroid/database/Cursor;",
            "I)Z"
        }
    .end annotation

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mAppsCanBeOnExternalStorage:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->customIcon:Z

    if-nez v1, :cond_0

    iget-boolean v1, p2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->usingFallbackIcon:Z

    if-nez v1, :cond_0

    invoke-interface {p3, p4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method resolveWidgetsForMimeType(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Landroid/content/Intent;

    const-string v9, "com.android.launcher.action.SUPPORTS_CLIPDATA_MIMETYPE"

    invoke-direct {v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v8

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    const/high16 v9, 0x10000

    invoke-virtual {v5, v7, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    return-object v6

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v10, v3, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    invoke-virtual {v2, v10, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v1, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v4, Landroid/content/ComponentName;

    iget-object v9, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v11, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v9, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    new-instance v11, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct {v11, v3, v9}, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;-><init>(Landroid/content/pm/ResolveInfo;Landroid/appwidget/AppWidgetProviderInfo;)V

    invoke-interface {v6, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public startLoader(Landroid/content/Context;Z)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startLoader isLaunching="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->stopLoaderLocked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 p2, 0x0

    :goto_0
    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel;Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkerThread:Landroid/os/HandlerThread;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/os/HandlerThread;->setPriority(I)V

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    const/4 p2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startLoaderFromBackground()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;->setLoadOnResume()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mApp:Lcom/cyanogenmod/trebuchet/LauncherApplication;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->startLoader(Landroid/content/Context;Z)V

    :cond_1
    return-void
.end method

.method public stopLoader()V
    .locals 2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->stopLocked()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unbindWorkspaceItems()V
    .locals 2

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorker:Landroid/os/Handler;

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$5;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$5;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method updateSavedIcon(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ShortcutInfo;[B)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .param p3    # [B

    const/4 v4, 0x0

    const/4 v2, 0x0

    if-eqz p3, :cond_2

    const/4 v5, 0x0

    :try_start_0
    array-length v6, p3

    invoke-static {p3, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {p2, v5}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->getIcon(Lcom/cyanogenmod/trebuchet/IconCache;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    move v2, v4

    :goto_0
    if-eqz v2, :cond_0

    const-string v4, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "going to save icon bitmap for info="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v2, 0x1

    goto :goto_0
.end method
