.class Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;
.super Landroid/os/AsyncTask;
.source "AppsCustomizePagedView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;",
        "Ljava/lang/Void;",
        "Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;",
        ">;"
    }
.end annotation


# instance fields
.field dataType:Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

.field page:I

.field pageContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

.field threadPriority:I


# direct methods
.method constructor <init>(ILcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;
    .param p3    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->pageContentType:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    const/4 v0, 0x0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->threadPriority:I

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->dataType:Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;
    .locals 3
    .param p1    # [Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    const/4 v2, 0x0

    array-length v0, p1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    aget-object v0, p1, v2

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->doInBackgroundCallback:Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;

    aget-object v1, p1, v2

    invoke-interface {v0, p0, v1}, Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;->run(Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V

    aget-object v0, p1, v2

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->doInBackground([Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->postExecuteCallback:Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;

    invoke-interface {v0, p0, p1}, Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;->run(Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->onPostExecute(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V

    return-void
.end method

.method setThreadPriority(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->threadPriority:I

    return-void
.end method

.method syncThreadPriority()V
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->threadPriority:I

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    return-void
.end method
