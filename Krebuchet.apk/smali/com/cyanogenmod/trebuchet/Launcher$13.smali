.class Lcom/cyanogenmod/trebuchet/Launcher$13;
.super Ljava/lang/Object;
.source "Launcher.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Launcher;->onLongClickAppsTab(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$13;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$13;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->access$32(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    move-result-object v0

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->setSortMode(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$13;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeContent:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->access$32(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    move-result-object v0

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->setSortMode(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0d006f
        :pswitch_0    # com.konka.avenger.R.id.apps_sort_title
        :pswitch_1    # com.konka.avenger.R.id.apps_sort_install_date
    .end packed-switch
.end method
