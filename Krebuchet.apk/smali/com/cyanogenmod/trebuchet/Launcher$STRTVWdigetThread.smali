.class Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;
.super Ljava/lang/Thread;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "STRTVWdigetThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$20(Z)V

    :goto_0
    const-string v2, "mstar.str.suspending"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "STRTVWdigetThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ",STRTVWdigetThread suspending str suspending = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$21(Lcom/cyanogenmod/trebuchet/Launcher;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$STRTVWdigetThread;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$21(Lcom/cyanogenmod/trebuchet/Launcher;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$20(Z)V

    return-void

    :cond_1
    const-string v2, "0"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
