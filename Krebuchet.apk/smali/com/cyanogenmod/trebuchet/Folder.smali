.class public Lcom/cyanogenmod/trebuchet/Folder;
.super Landroid/widget/LinearLayout;
.source "Folder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/cyanogenmod/trebuchet/DragSource;
.implements Lcom/cyanogenmod/trebuchet/DropTarget;
.implements Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;
.implements Lnet/londatiga/android/QuickAction$OnActionItemClickListener;


# static fields
.field private static final FULL_GROW:I = 0x0

.field private static final ID_QUICKACTION_APP_DELETE:I = 0x2

.field private static final ID_QUICKACTION_APP_MOVE:I = 0x1

.field private static final ID_QUICKACTION_APP_RUN:I = 0x0

.field private static ITEM_ONE:I = 0x0

.field private static final ON_EXIT_CLOSE_DELAY:I = 0x320

.field private static final PARTIAL_GROW:I = 0x1

.field private static final REORDER_ANIMATION_DURATION:I = 0xe6

.field static final STATE_ANIMATING:I = 0x1

.field static final STATE_NONE:I = -0x1

.field static final STATE_OPEN:I = 0x2

.field static final STATE_SMALL:I = 0x0

.field private static STATE_TO_EDIT:I = 0x0

.field private static STATE_TO_RUN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Launcher.Folder"

.field private static sDefaultFolderName:Ljava/lang/String;

.field private static sHintText:Ljava/lang/String;


# instance fields
.field private folderSurface:Landroid/widget/LinearLayout;

.field private mActionModeCallback:Landroid/view/ActionMode$Callback;

.field private final mAddQuickAction:Lcom/konka/folder/OpenAction;

.field private mAppQuickAction:Lnet/londatiga/android/QuickAction;

.field private mArrowDown:Landroid/widget/ImageView;

.field private mArrowUp:Landroid/widget/ImageView;

.field protected mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

.field private mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

.field private mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

.field private mCurrentDragView:Landroid/view/View;

.field private mDeleteFolderOnDropCompleted:Z

.field protected mDragController:Lcom/cyanogenmod/trebuchet/DragController;

.field private mDragInProgress:Z

.field private mEmptyCell:[I

.field private mExpandDuration:I

.field private mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

.field private mFolderLocationX:I

.field private mFolderLocationY:I

.field mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

.field private mFolderNameHeight:I

.field private mFolderState:I

.field private final mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

.field private mIconDrawable:Landroid/graphics/drawable/Drawable;

.field private mIconRect:Landroid/graphics/Rect;

.field private final mInflater:Landroid/view/LayoutInflater;

.field protected mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

.field private mInitAddQuickAction:Z

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mIsEditingName:Z

.field private mItemAddedBackToSelfViaIcon:Z

.field private mItemsInReadingOrder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mItemsInvalidated:Z

.field protected mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mMaxCountX:I

.field private mMaxCountY:I

.field private mMaxNumItems:I

.field private mMode:I

.field private mNewSize:Landroid/graphics/Rect;

.field private mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

.field mOnExitAlarmListener:Lcom/cyanogenmod/trebuchet/OnAlarmListener;

.field private mPreviousTargetCell:[I

.field private mRearrangeOnClose:Z

.field private mReorderAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

.field mReorderAlarmListener:Lcom/cyanogenmod/trebuchet/OnAlarmListener;

.field private mState:I

.field private mSuppressFolderDeletion:Z

.field mSuppressOnAdd:Z

.field private mTargetCell:[I

.field private mTempRect:Landroid/graphics/Rect;

.field mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/cyanogenmod/trebuchet/Folder;->ITEM_ONE:I

    sput v0, Lcom/cyanogenmod/trebuchet/Folder;->STATE_TO_RUN:I

    const/4 v0, 0x0

    sput v0, Lcom/cyanogenmod/trebuchet/Folder;->STATE_TO_EDIT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mState:I

    iput v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMode:I

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mRearrangeOnClose:Z

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mNewSize:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInReadingOrder:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressOnAdd:Z

    new-array v1, v2, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    new-array v1, v2, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mPreviousTargetCell:[I

    new-array v1, v2, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    new-instance v1, Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-direct {v1}, Lcom/cyanogenmod/trebuchet/Alarm;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mReorderAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    new-instance v1, Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-direct {v1}, Lcom/cyanogenmod/trebuchet/Alarm;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTempRect:Landroid/graphics/Rect;

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragInProgress:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDeleteFolderOnDropCompleted:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressFolderDeletion:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemAddedBackToSelfViaIcon:Z

    sget v1, Lcom/cyanogenmod/trebuchet/Folder;->STATE_TO_RUN:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderState:I

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIsEditingName:Z

    new-instance v1, Lcom/cyanogenmod/trebuchet/Folder$1;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/Folder$1;-><init>(Lcom/cyanogenmod/trebuchet/Folder;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    new-instance v1, Lcom/cyanogenmod/trebuchet/Folder$2;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/Folder$2;-><init>(Lcom/cyanogenmod/trebuchet/Folder;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mReorderAlarmListener:Lcom/cyanogenmod/trebuchet/OnAlarmListener;

    new-instance v1, Lcom/cyanogenmod/trebuchet/Folder$3;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/Folder$3;-><init>(Lcom/cyanogenmod/trebuchet/Folder;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarmListener:Lcom/cyanogenmod/trebuchet/OnAlarmListener;

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Folder;->setAlwaysDrawnWithCacheEnabled(Z)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/LauncherApplication;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getIconCache()Lcom/cyanogenmod/trebuchet/IconCache;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    new-instance v1, Lcom/konka/folder/OpenAction;

    invoke-direct {v1, p1, v3, v5}, Lcom/konka/folder/OpenAction;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAddQuickAction:Lcom/konka/folder/OpenAction;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0020    # com.konka.avenger.R.integer.folder_max_count_x

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxCountX:I

    const v1, 0x7f0b0021    # com.konka.avenger.R.integer.folder_max_count_y

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxCountY:I

    const v1, 0x7f0b0022    # com.konka.avenger.R.integer.folder_max_num_items

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxNumItems:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    const v1, 0x7f0b001b    # com.konka.avenger.R.integer.config_folderAnimDuration

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mExpandDuration:I

    sget-object v1, Lcom/cyanogenmod/trebuchet/Folder;->sDefaultFolderName:Ljava/lang/String;

    if-nez v1, :cond_0

    const v1, 0x7f0a0007    # com.konka.avenger.R.string.folder_name

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/cyanogenmod/trebuchet/Folder;->sDefaultFolderName:Ljava/lang/String;

    :cond_0
    sget-object v1, Lcom/cyanogenmod/trebuchet/Folder;->sHintText:Ljava/lang/String;

    if-nez v1, :cond_1

    const v1, 0x7f0a0052    # com.konka.avenger.R.string.folder_hint_text

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/cyanogenmod/trebuchet/Folder;->sHintText:Ljava/lang/String;

    :cond_1
    move-object v1, p1

    check-cast v1, Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/Folder;->setFocusableInTouchMode(Z)V

    new-instance v1, Lnet/londatiga/android/QuickAction;

    invoke-direct {v1, p1, v3, v5}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->initQuickActionMenu()V

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/Folder;)[I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/Folder;)[I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    return-object v0
.end method

.method static synthetic access$10(Lcom/cyanogenmod/trebuchet/Folder;I)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderState:I

    return-void
.end method

.method static synthetic access$11(Lcom/cyanogenmod/trebuchet/Folder;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->onCloseComplete()V

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/Folder;[I[I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/Folder;->realTimeReorder([I[I)V

    return-void
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/Folder;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/Folder;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/Folder;->sendCustomAccessibilityEvent(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/Folder;I)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mState:I

    return-void
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/Folder;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderState:I

    return v0
.end method

.method static synthetic access$7()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/Folder;->STATE_TO_EDIT:I

    return v0
.end method

.method static synthetic access$8(Lcom/cyanogenmod/trebuchet/Folder;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->setFocusOnEditText()V

    return-void
.end method

.method static synthetic access$9()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/Folder;->STATE_TO_RUN:I

    return v0
.end method

.method private arrangeChildren(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v0, 0x2

    new-array v12, v0, [I

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemsInReadingOrder()Ljava/util/ArrayList;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeAllViews()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    return-void

    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, v12, v10, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->getVacantCell([III)Z

    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    aget v0, v12, v4

    iput v0, v9, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    aget v0, v12, v10

    iput v0, v9, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    invoke-virtual {v11}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v0, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    aget v2, v12, v4

    if-ne v0, v2, :cond_2

    iget v0, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    aget v2, v12, v10

    if-eq v0, v2, :cond_3

    :cond_2
    aget v0, v12, v4

    iput v0, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    aget v0, v12, v10

    iput v0, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v2, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    iget v5, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v6, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    invoke-static/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    :cond_3
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v7, -0x1

    iget-wide v2, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->id:J

    long-to-int v8, v2

    move-object v6, v11

    invoke-virtual/range {v5 .. v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->addViewToCellLayout(Landroid/view/View;IILcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;Z)Z

    goto :goto_0
.end method

.method private centerAboutIcon()V
    .locals 27

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    const/high16 v22, 0x44a00000    # 1280.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    const/high16 v23, 0x44a00000    # 1280.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x43c30000    # 390.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    const/high16 v23, 0x44a00000    # 1280.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v22, v0

    const v23, 0x7f0d0032    # com.konka.avenger.R.id.drag_layer

    invoke-virtual/range {v22 .. v23}, Lcom/cyanogenmod/trebuchet/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Lcom/cyanogenmod/trebuchet/DragLayer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->centerY()I

    move-result v8

    div-int/lit8 v22, v21, 0x2

    sub-int v9, v7, v22

    div-int/lit8 v22, v16, 0x2

    sub-int v22, v8, v22

    move/from16 v0, v22

    add-int/lit16 v10, v0, 0x13b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v11

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v5

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v5, v6}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I

    move-result v22

    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int v23, v23, v24

    sub-int v23, v23, v21

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(II)I

    move-result v17

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v22

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v23, v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v24

    add-int v23, v23, v24

    sub-int v23, v23, v16

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(II)I

    move-result v20

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_0

    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v22, v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v23

    sub-int v23, v23, v21

    div-int/lit8 v23, v23, 0x2

    add-int v17, v22, v23

    :cond_0
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v22

    move/from16 v0, v16

    move/from16 v1, v22

    if-lt v0, v1, :cond_1

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v23

    sub-int v23, v23, v16

    div-int/lit8 v23, v23, 0x2

    add-int v20, v22, v23

    :cond_1
    div-int/lit8 v22, v21, 0x2

    sub-int v23, v9, v17

    add-int v14, v22, v23

    div-int/lit8 v22, v16, 0x2

    sub-int v23, v10, v20

    add-int v15, v22, v23

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->setPivotX(F)V

    int-to-float v0, v15

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->setPivotY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getMeasuredWidth()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x3f800000    # 1.0f

    int-to-float v0, v14

    move/from16 v24, v0

    mul-float v23, v23, v24

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v23, v23, v24

    mul-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v12, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getMeasuredHeight()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x3f800000    # 1.0f

    int-to-float v0, v15

    move/from16 v24, v0

    mul-float v23, v23, v24

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v23, v23, v24

    mul-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v13, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v22, v0

    int-to-float v0, v12

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setPivotX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    move-object/from16 v22, v0

    int-to-float v0, v13

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setPivotY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowDown:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderLocationX:I

    move/from16 v23, v0

    const/high16 v24, 0x42200000    # 40.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v24, v24, v25

    const/high16 v25, 0x44a00000    # 1280.0f

    div-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    add-int v23, v23, v24

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {v22 .. v26}, Landroid/widget/ImageView;->setPadding(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowUp:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderLocationX:I

    move/from16 v23, v0

    const/high16 v24, 0x42200000    # 40.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v24, v24, v25

    const/high16 v25, 0x44a00000    # 1280.0f

    div-float v24, v24, v25

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    add-int v23, v23, v24

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    invoke-virtual/range {v22 .. v26}, Landroid/widget/ImageView;->setPadding(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderLocationY:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x43b40000    # 360.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v23, v23, v24

    const/high16 v24, 0x44a00000    # 1280.0f

    div-float v23, v23, v24

    cmpg-float v22, v22, v23

    if-gtz v22, :cond_2

    const/high16 v22, 0x42860000    # 67.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    const/high16 v23, 0x44a00000    # 1280.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    add-int v20, v8, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowDown:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowUp:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->folderSurface:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    const v23, 0x7f020034    # com.konka.avenger.R.drawable.file_popmenu_bj

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mMode:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    move/from16 v0, v21

    move-object/from16 v1, v18

    iput v0, v1, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->width:I

    move/from16 v0, v16

    move-object/from16 v1, v18

    iput v0, v1, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->height:I

    move/from16 v0, v17

    move-object/from16 v1, v18

    iput v0, v1, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    :goto_1
    return-void

    :cond_2
    const v22, 0x43e38000    # 455.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v22, v22, v23

    const/high16 v23, 0x44a00000    # 1280.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    sub-int v20, v8, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowDown:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowUp:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->folderSurface:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    const v23, 0x7f020031    # com.konka.avenger.R.drawable.file_popmenu1_bj

    invoke-virtual/range {v22 .. v23}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Folder;->mNewSize:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    add-int v23, v17, v21

    add-int v24, v20, v16

    move-object/from16 v0, v22

    move/from16 v1, v17

    move/from16 v2, v20

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method static fromXml(Landroid/content/Context;)Lcom/cyanogenmod/trebuchet/Folder;
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002c    # com.konka.avenger.R.layout.user_folder

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/Folder;

    return-object v0
.end method

.method private getDragViewVisualCenter(IIIILcom/cyanogenmod/trebuchet/DragView;[F)[F
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p6    # [F

    if-nez p6, :cond_0

    const/4 v3, 0x2

    new-array v1, v3, [F

    :goto_0
    sub-int v0, p1, p3

    sub-int v2, p2, p4

    const/4 v3, 0x0

    invoke-virtual {p5}, Lcom/cyanogenmod/trebuchet/DragView;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v0

    int-to-float v4, v4

    aput v4, v1, v3

    const/4 v3, 0x1

    invoke-virtual {p5}, Lcom/cyanogenmod/trebuchet/DragView;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v2

    int-to-float v4, v4

    aput v4, v1, v3

    return-object v1

    :cond_0
    move-object v1, p6

    goto :goto_0
.end method

.method private getViewForInfo(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountY()I

    move-result v3

    if-lt v1, v3, :cond_1

    const/4 v2, 0x0

    :cond_0
    return-object v2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v3

    if-lt v0, v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3, v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-eq v3, p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private initQuickActionMenu()V
    .locals 7

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Lnet/londatiga/android/ActionItem;

    const/4 v5, 0x0

    const v6, 0x7f0a009f    # com.konka.avenger.R.string.quick_action_run

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v1, Lnet/londatiga/android/ActionItem;

    const/4 v5, 0x1

    const v6, 0x7f0a00a3    # com.konka.avenger.R.string.quick_action_move

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    new-instance v4, Lnet/londatiga/android/ActionItem;

    const/4 v5, 0x2

    const v6, 0x7f0a00a4    # com.konka.avenger.R.string.quick_action_delete

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, v3}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, v1}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, v4}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v5, p0}, Lnet/londatiga/android/QuickAction;->setOnActionItemClickListener(Lnet/londatiga/android/QuickAction$OnActionItemClickListener;)V

    :goto_0
    return-void

    :cond_0
    const-string v5, "Launcher.Folder"

    const-string v6, "the mIconQuickAction has not been initialized!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onCloseComplete()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragLayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v1, p0}, Lcom/cyanogenmod/trebuchet/DragController;->removeDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->clearFocus()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->requestFocus()Z

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mRearrangeOnClose:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->setupContentForNumItems(I)V

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mRearrangeOnClose:Z

    :cond_1
    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressFolderDeletion:Z

    return-void
.end method

.method private positionAndSizeAsIcon()V
    .locals 5

    const v3, 0x3f4ccccd    # 0.8f

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Lcom/cyanogenmod/trebuchet/DragLayer;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Folder;->setScaleX(F)V

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Folder;->setScaleY(F)V

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/Folder;->setAlpha(F)V

    :goto_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mState:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->width:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->height:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    goto :goto_1
.end method

.method private realTimeReorder([I[I)V
    .locals 18
    .param p1    # [I
    .param p2    # [I

    const/4 v8, 0x0

    const/high16 v9, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Folder;->readingOrderGreaterThan([I[I)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    aget v3, p1, v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v3, v5, :cond_1

    const/4 v13, 0x1

    :goto_0
    if-eqz v13, :cond_2

    const/4 v3, 0x1

    aget v3, p1, v3

    add-int/lit8 v12, v3, 0x1

    :goto_1
    move v15, v12

    :goto_2
    const/4 v3, 0x1

    aget v3, p2, v3

    if-le v15, v3, :cond_3

    :cond_0
    return-void

    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    aget v12, p1, v3

    goto :goto_1

    :cond_3
    const/4 v3, 0x1

    aget v3, p1, v3

    if-ne v15, v3, :cond_4

    const/4 v3, 0x0

    aget v3, p1, v3

    add-int/lit8 v11, v3, 0x1

    :goto_3
    const/4 v3, 0x1

    aget v3, p2, v3

    if-ge v15, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v3

    add-int/lit8 v10, v3, -0x1

    :goto_4
    move v14, v11

    :goto_5
    if-le v14, v10, :cond_6

    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    :cond_4
    const/4 v11, 0x0

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    aget v10, p2, v3

    goto :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3, v14, v15}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v5, 0x0

    aget v5, p1, v5

    const/4 v6, 0x1

    aget v6, p1, v6

    const/16 v7, 0xe6

    invoke-virtual/range {v3 .. v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->animateChildToPosition(Landroid/view/View;IIII)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    aput v14, p1, v3

    const/4 v3, 0x1

    aput v15, p1, v3

    int-to-float v3, v8

    add-float/2addr v3, v9

    float-to-int v8, v3

    float-to-double v5, v9

    const-wide v16, 0x3feccccccccccccdL    # 0.9

    mul-double v5, v5, v16

    double-to-float v9, v5

    :cond_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    :cond_8
    const/4 v3, 0x0

    aget v3, p1, v3

    if-nez v3, :cond_9

    const/4 v13, 0x1

    :goto_6
    if-eqz v13, :cond_a

    const/4 v3, 0x1

    aget v3, p1, v3

    add-int/lit8 v12, v3, -0x1

    :goto_7
    move v15, v12

    :goto_8
    const/4 v3, 0x1

    aget v3, p2, v3

    if-lt v15, v3, :cond_0

    const/4 v3, 0x1

    aget v3, p1, v3

    if-ne v15, v3, :cond_b

    const/4 v3, 0x0

    aget v3, p1, v3

    add-int/lit8 v11, v3, -0x1

    :goto_9
    const/4 v3, 0x1

    aget v3, p2, v3

    if-le v15, v3, :cond_c

    const/4 v10, 0x0

    :goto_a
    move v14, v11

    :goto_b
    if-ge v14, v10, :cond_d

    add-int/lit8 v15, v15, -0x1

    goto :goto_8

    :cond_9
    const/4 v13, 0x0

    goto :goto_6

    :cond_a
    const/4 v3, 0x1

    aget v12, p1, v3

    goto :goto_7

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v3

    add-int/lit8 v11, v3, -0x1

    goto :goto_9

    :cond_c
    const/4 v3, 0x0

    aget v10, p2, v3

    goto :goto_a

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3, v14, v15}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v5, 0x0

    aget v5, p1, v5

    const/4 v6, 0x1

    aget v6, p1, v6

    const/16 v7, 0xe6

    invoke-virtual/range {v3 .. v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->animateChildToPosition(Landroid/view/View;IIII)Z

    move-result v3

    if-eqz v3, :cond_e

    const/4 v3, 0x0

    aput v14, p1, v3

    const/4 v3, 0x1

    aput v15, p1, v3

    int-to-float v3, v8

    add-float/2addr v3, v9

    float-to-int v8, v3

    float-to-double v5, v9

    const-wide v16, 0x3feccccccccccccdL    # 0.9

    mul-double v5, v5, v16

    double-to-float v9, v5

    :cond_e
    add-int/lit8 v14, v14, -0x1

    goto :goto_b
.end method

.method private replaceFolderWithFinalItem()V
    .locals 12

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v4, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->container:J

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v2, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->screen:I

    invoke-virtual {v0, v4, v5, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getCellLayout(JI)Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v11

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-virtual {v11, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    check-cast v0, Lcom/cyanogenmod/trebuchet/DropTarget;

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/DragController;->removeDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->removeFolder(Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v2, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->container:J

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v4, v4, Lcom/cyanogenmod/trebuchet/FolderInfo;->screen:I

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v5, v5, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellX:I

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v6, v6, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellY:I

    invoke-static/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    :cond_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-static {v0, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const v2, 0x7f030004    # com.konka.avenger.R.layout.application

    check-cast v1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {v0, v2, v11, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->createShortcut(ILandroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v4, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->container:J

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v6, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->screen:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v7, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellX:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v8, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellY:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v9, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->spanX:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget v10, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->spanY:I

    invoke-virtual/range {v2 .. v10}, Lcom/cyanogenmod/trebuchet/Workspace;->addInScreen(Landroid/view/View;JIIIII)V

    :cond_3
    return-void
.end method

.method private sendCustomAccessibilityEvent(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    :cond_0
    return-void
.end method

.method private setFocusOnEditText()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/FolderEditText;->requestFocus()Z

    return-void
.end method

.method private setupContentDimensions(I)V
    .locals 9
    .param p1    # I

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemsInReadingOrder()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v0

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountY()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/16 v7, 0x8

    const/4 v8, 0x2

    invoke-virtual {v6, v7, v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->setGridSize(II)V

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/Folder;->arrangeChildren(Ljava/util/ArrayList;)V

    return-void

    :cond_0
    move v4, v0

    move v5, v1

    mul-int v7, v0, v1

    if-ge v7, p1, :cond_5

    if-le v0, v1, :cond_1

    iget v7, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxCountY:I

    if-ne v1, v7, :cond_4

    :cond_1
    iget v7, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxCountX:I

    if-ge v0, v7, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_2
    :goto_1
    if-nez v1, :cond_3

    add-int/lit8 v1, v1, 0x1

    :cond_3
    :goto_2
    if-ne v0, v4, :cond_7

    if-ne v1, v5, :cond_7

    const/4 v2, 0x1

    :goto_3
    goto :goto_0

    :cond_4
    iget v7, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxCountY:I

    if-ge v1, v7, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    add-int/lit8 v7, v1, -0x1

    mul-int/2addr v7, v0

    if-lt v7, p1, :cond_6

    if-lt v1, v0, :cond_6

    add-int/lit8 v7, v1, -0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_2

    :cond_6
    add-int/lit8 v7, v0, -0x1

    mul-int/2addr v7, v1

    if-lt v7, p1, :cond_3

    add-int/lit8 v7, v0, -0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_7
    move v2, v6

    goto :goto_3
.end method

.method private setupContentForNumItems(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Folder;->setupContentDimensions(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    if-nez v0, :cond_0

    new-instance v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;-><init>(II)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->customPosition:Z

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->centerAboutIcon()V

    return-void
.end method

.method private updateItemLocationsInDatabase()V
    .locals 10

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemsInReadingOrder()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v2, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    const/4 v4, 0x0

    iget v5, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v6, v1, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    invoke-static/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->moveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    goto :goto_0
.end method

.method private updateTextViewFocus()V
    .locals 3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->getItemAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->getItemAt(I)Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setNextFocusDownId(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setNextFocusRightId(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setNextFocusLeftId(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setNextFocusUpId(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public acceptDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v2, 0x1

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v0, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget v1, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    if-eqz v1, :cond_0

    if-ne v1, v2, :cond_1

    :cond_0
    sget v3, Lcom/cyanogenmod/trebuchet/Folder;->ITEM_ONE:I

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/Folder;->checkFolderAdd(I)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public animateClosed()V
    .locals 14

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    instance-of v10, v10, Lcom/cyanogenmod/trebuchet/DragLayer;

    if-nez v10, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMode:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    const-string v10, "alpha"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    const-string v10, "scaleX"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const v13, 0x3f666666    # 0.9f

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    const-string v10, "scaleY"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const v13, 0x3f666666    # 0.9f

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    const/4 v10, 0x3

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    const/4 v11, 0x1

    aput-object v5, v10, v11

    const/4 v11, 0x2

    aput-object v6, v10, v11

    invoke-static {p0, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    :goto_1
    new-instance v10, Lcom/cyanogenmod/trebuchet/Folder$7;

    invoke-direct {v10, p0}, Lcom/cyanogenmod/trebuchet/Folder$7;-><init>(Lcom/cyanogenmod/trebuchet/Folder;)V

    invoke-virtual {v4, v10}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mExpandDuration:I

    int-to-long v10, v10

    invoke-virtual {v4, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    const-string v10, "width"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v13

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    const-string v10, "height"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v13

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const-string v10, "x"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v8

    const-string v10, "y"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    const/4 v10, 0x4

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    const/4 v11, 0x2

    aput-object v8, v10, v11

    const/4 v11, 0x3

    aput-object v9, v10, v11

    invoke-static {v3, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v10, Lcom/cyanogenmod/trebuchet/Folder$6;

    invoke-direct {v10, p0}, Lcom/cyanogenmod/trebuchet/Folder$6;-><init>(Lcom/cyanogenmod/trebuchet/Folder;)V

    invoke-virtual {v4, v10}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const-string v10, "alpha"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/animation/PropertyValuesHolder;

    const/4 v12, 0x0

    aput-object v0, v11, v12

    invoke-static {v10, v11}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mExpandDuration:I

    int-to-long v10, v10

    invoke-virtual {v1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v10, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v11, 0x40000000    # 2.0f

    invoke-direct {v10, v11}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_1
.end method

.method public animateOpen(Lcom/cyanogenmod/trebuchet/Folder;[I)V
    .locals 14
    .param p1    # Lcom/cyanogenmod/trebuchet/Folder;
    .param p2    # [I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->positionAndSizeAsIcon()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    instance-of v10, v10, Lcom/cyanogenmod/trebuchet/DragLayer;

    if-nez v10, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    const/4 v10, 0x0

    aget v10, p2, v10

    iput v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderLocationX:I

    const/4 v10, 0x1

    aget v10, p2, v10

    iput v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderLocationY:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->centerAboutIcon()V

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMode:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    const-string v10, "alpha"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    const-string v10, "scaleX"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    const-string v10, "scaleY"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    const/4 v10, 0x3

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    const/4 v11, 0x1

    aput-object v5, v10, v11

    const/4 v11, 0x2

    aput-object v6, v10, v11

    invoke-static {p0, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    :goto_1
    new-instance v10, Lcom/cyanogenmod/trebuchet/Folder$5;

    invoke-direct {v10, p0}, Lcom/cyanogenmod/trebuchet/Folder$5;-><init>(Lcom/cyanogenmod/trebuchet/Folder;)V

    invoke-virtual {v4, v10}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mExpandDuration:I

    int-to-long v10, v10

    invoke-virtual {v4, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    :cond_1
    const-string v10, "width"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mNewSize:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->width()I

    move-result v13

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    const-string v10, "height"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mNewSize:Landroid/graphics/Rect;

    invoke-virtual {v13}, Landroid/graphics/Rect;->height()I

    move-result v13

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const-string v10, "x"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mNewSize:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v8

    const-string v10, "y"

    const/4 v11, 0x1

    new-array v11, v11, [I

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/cyanogenmod/trebuchet/Folder;->mNewSize:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    const/4 v10, 0x4

    new-array v10, v10, [Landroid/animation/PropertyValuesHolder;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    const/4 v11, 0x2

    aput-object v8, v10, v11

    const/4 v11, 0x3

    aput-object v9, v10, v11

    invoke-static {v3, v10}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v10, Lcom/cyanogenmod/trebuchet/Folder$4;

    invoke-direct {v10, p0}, Lcom/cyanogenmod/trebuchet/Folder$4;-><init>(Lcom/cyanogenmod/trebuchet/Folder;)V

    invoke-virtual {v4, v10}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const-string v10, "alpha"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    aput v13, v11, v12

    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/animation/PropertyValuesHolder;

    const/4 v12, 0x0

    aput-object v0, v11, v12

    invoke-static {v10, v11}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget v10, p0, Lcom/cyanogenmod/trebuchet/Folder;->mExpandDuration:I

    int-to-long v10, v10

    invoke-virtual {v1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v10, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v11, 0x40000000    # 2.0f

    invoke-direct {v10, v11}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_1
.end method

.method bind(Lcom/cyanogenmod/trebuchet/FolderInfo;)V
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderInfo;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/cyanogenmod/trebuchet/Folder;->setupContentForNumItems(I)V

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0, v2}, Lcom/cyanogenmod/trebuchet/Folder;->setupContentForNumItems(I)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->updateTextViewFocus()V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v5, p0}, Lcom/cyanogenmod/trebuchet/FolderInfo;->addListener(Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;)V

    sget-object v5, Lcom/cyanogenmod/trebuchet/Folder;->sDefaultFolderName:Ljava/lang/String;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->createAndAddShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v6, v3}, Lcom/cyanogenmod/trebuchet/FolderInfo;->remove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v6, v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0007    # com.konka.avenger.R.string.folder_name

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public checkFolderAdd(I)Z
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v1

    add-int/2addr v1, p1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxNumItems:I

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00bb    # com.konka.avenger.R.string.folder_noroom

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v1

    add-int/2addr v1, p1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mMaxNumItems:I

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public completeDragExit()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressOnAdd:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mRearrangeOnClose:Z

    return-void
.end method

.method protected createAndAddShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Z
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v5, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030004    # com.konka.avenger.R.layout.application

    invoke-virtual {v2, v3, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {p1, v3}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->getIcon(Lcom/cyanogenmod/trebuchet/IconCache;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v6, v2, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget v3, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    iget v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    invoke-virtual {v2, v3, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    if-ltz v2, :cond_0

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    if-ltz v2, :cond_0

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountY()I

    move-result v3

    if-lt v2, v3, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Folder;->findAndSetEmptyCells(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Z

    move-result v2

    if-nez v2, :cond_1

    move v5, v0

    :goto_0
    return v5

    :cond_1
    new-instance v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    iget v0, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    iget v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    iget v3, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->spanX:I

    iget v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->spanY:I

    invoke-direct {v4, v0, v2, v3, v6}, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;-><init>(IIII)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/FolderKeyEventListener;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/FolderKeyEventListener;-><init>()V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v2, -0x1

    iget-wide v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->id:J

    long-to-int v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->addViewToCellLayout(Landroid/view/View;IILcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;Z)Z

    goto :goto_0
.end method

.method public dismissEditingName()V
    .locals 3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0007    # com.konka.avenger.R.string.folder_name

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->doneEditingFolderName(Z)V

    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public doneEditingFolderName(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    sget-object v2, Lcom/cyanogenmod/trebuchet/Folder;->sHintText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getHideIconLabels(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0007    # com.konka.avenger.R.string.folder_name

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/FolderInfo;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-static {v1, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    if-eqz p1, :cond_2

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0066    # com.konka.avenger.R.string.folder_renamed

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/Folder;->sendCustomAccessibilityEvent(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->requestFocus()Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v4, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->setFocusOnFirstChild()V

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIsEditingName:Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    :cond_3
    return-void
.end method

.method protected findAndSetEmptyCells(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Z
    .locals 6
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v0, v3, [I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget v4, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->spanX:I

    iget v5, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->spanY:I

    invoke-virtual {v3, v0, v4, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForFolderSpan([III)Z

    move-result v3

    if-eqz v3, :cond_0

    aget v2, v0, v2

    iput v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    aget v2, v0, v1

    iput v2, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public getDragDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDropTargetDelegate(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Lcom/cyanogenmod/trebuchet/DropTarget;
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEditTextRegion()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    return-object v0
.end method

.method getInfo()Lcom/cyanogenmod/trebuchet/FolderInfo;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    return-object v0
.end method

.method public getItemAt(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getItemsInReadingOrder()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemsInReadingOrder(Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getItemsInReadingOrder(Z)Ljava/util/ArrayList;
    .locals 5
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInReadingOrder:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountY()I

    move-result v4

    if-lt v2, v4, :cond_1

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    :cond_0
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInReadingOrder:Ljava/util/ArrayList;

    return-object v4

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v4

    if-lt v0, v4, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v4, v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-ne v1, v4, :cond_3

    if-eqz p1, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInReadingOrder:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getLocationInDragLayer([I)V
    .locals 1
    .param p1    # [I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocationInDragLayer(Landroid/view/View;[I)V

    return-void
.end method

.method public getmFolderState()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderState:I

    return v0
.end method

.method public isDropEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEditingName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIsEditingName:Z

    return v0
.end method

.method notifyDataSetChanged()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeAllViewsInLayout()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->bind(Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    return-void
.end method

.method public notifyDrop()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragInProgress:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemAddedBackToSelfViaIcon:Z

    :cond_0
    return-void
.end method

.method public onAdd(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 7
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressOnAdd:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Folder;->findAndSetEmptyCells(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->setupContentForNumItems(I)V

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Folder;->findAndSetEmptyCells(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Z

    :cond_1
    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Folder;->createAndAddShortcut(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v2, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    const/4 v4, 0x0

    iget v5, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    iget v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-eqz v3, :cond_0

    move-object v0, v2

    check-cast v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v3, 0x2

    new-array v1, v3, [I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    new-instance v4, Landroid/graphics/Rect;

    aget v5, v1, v7

    aget v6, v1, v9

    aget v7, v1, v7

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    aget v8, v1, v9

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v3, v4, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivitySafely(Landroid/content/Intent;Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public onDragEnter(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mPreviousTargetCell:[I

    const/4 v1, 0x0

    aput v2, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mPreviousTargetCell:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    return-void
.end method

.method public onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-boolean v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragComplete:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarmListener:Lcom/cyanogenmod/trebuchet/OnAlarmListener;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Alarm;->setOnAlarmListener(Lcom/cyanogenmod/trebuchet/OnAlarmListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    const-wide/16 v1, 0x320

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Alarm;->setAlarm(J)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mReorderAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    return-void
.end method

.method public onDragOver(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 10
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget v1, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    iget v2, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    iget v3, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->xOffset:I

    iget v4, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->yOffset:I

    iget-object v5, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Folder;->getDragViewVisualCenter(IIIILcom/cyanogenmod/trebuchet/DragView;[F)[F

    move-result-object v7

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    aget v1, v7, v9

    float-to-int v1, v1

    aget v2, v7, v8

    float-to-int v2, v2

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    move v3, v8

    move v4, v8

    invoke-virtual/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestArea(IIII[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    aget v0, v0, v9

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mPreviousTargetCell:[I

    aget v1, v1, v9

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    aget v0, v0, v8

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mPreviousTargetCell:[I

    aget v1, v1, v8

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mReorderAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mReorderAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mReorderAlarmListener:Lcom/cyanogenmod/trebuchet/OnAlarmListener;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Alarm;->setOnAlarmListener(Lcom/cyanogenmod/trebuchet/OnAlarmListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mReorderAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/Alarm;->setAlarm(J)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mPreviousTargetCell:[I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    aget v1, v1, v9

    aput v1, v0, v9

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mPreviousTargetCell:[I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mTargetCell:[I

    aget v1, v1, v8

    aput v1, v0, v8

    :cond_1
    return-void
.end method

.method public onDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 11
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v10, 0x0

    const/4 v5, 0x1

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->makeShortcut()Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v6

    iput v5, v6, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->spanX:I

    iput v5, v6, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->spanY:I

    :goto_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-ne v6, v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    aget v0, v0, v10

    iput v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iput v0, v7, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    aget v0, v0, v5

    iput v0, v4, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iput v0, v7, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    const/4 v2, -0x1

    iget-wide v8, v6, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->id:J

    long-to-int v3, v8

    invoke-virtual/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->addViewToCellLayout(Landroid/view/View;IILcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;Z)Z

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/DragView;->hasDrawn()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v0

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;)V

    :goto_1
    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/Folder;->setupContentDimensions(I)V

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressOnAdd:Z

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v0, v6}, Lcom/cyanogenmod/trebuchet/FolderInfo;->add(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void

    :cond_2
    iget-object v6, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v6, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onDropCompleted(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;
    .param p3    # Z

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p3, :cond_2

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDeleteFolderOnDropCompleted:Z

    if-eqz v0, :cond_0

    :cond_0
    :goto_0
    if-eq p1, p0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->alarmPending()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->completeDragExit()V

    :cond_1
    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDeleteFolderOnDropCompleted:Z

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragInProgress:Z

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemAddedBackToSelfViaIcon:Z

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressOnAdd:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->updateItemLocationsInDatabase()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    invoke-virtual {v0, p2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mOnExitAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->alarmPending()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mSuppressFolderDeletion:Z

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 5

    const/16 v4, 0x8

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v1, 0x7f0d0024    # com.konka.avenger.R.id.arrow_up

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowUp:Landroid/widget/ImageView;

    const v1, 0x7f0d0025    # com.konka.avenger.R.id.arrow_down

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mArrowDown:Landroid/widget/ImageView;

    const v1, 0x7f0d0065    # com.konka.avenger.R.id.folder_content

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v4, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setGridSize(II)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->setMotionEventSplittingEnabled(Z)V

    const v1, 0x7f0d005d    # com.konka.avenger.R.id.folder_name

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/FolderEditText;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1, p0}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setFolder(Lcom/cyanogenmod/trebuchet/Folder;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1, p0}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1, v0, v0}, Lcom/cyanogenmod/trebuchet/FolderEditText;->measure(II)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderNameHeight:I

    const v1, 0x7f0d0064    # com.konka.avenger.R.id.folder_surface

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->folderSurface:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1, p0}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setSelectAllOnFocus(Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getInputType()I

    move-result v2

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    or-int/lit16 v2, v2, 0x2000

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setInputType(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getHideIconLabels(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1, v4}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getPaddingBottom()I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderNameHeight:I

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1, p0}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->startEditingFolderName()V

    :cond_0
    return-void
.end method

.method public onItemClick(Lnet/londatiga/android/QuickAction;II)V
    .locals 7
    .param p1    # Lnet/londatiga/android/QuickAction;
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    if-ne p1, v3, :cond_0

    packed-switch p3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v4, v4, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->onClick(Landroid/view/View;)V

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v3, v3, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v2, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-eqz v3, :cond_2

    move-object v0, v1

    check-cast v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissFolderCling(Landroid/view/View;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->onDragStartedWithItem(Landroid/view/View;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v3

    invoke-virtual {v3, v2, p0}, Lcom/cyanogenmod/trebuchet/Workspace;->beginDragShared(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;)V

    move-object v3, v2

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aget-object v3, v3, v5

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    iget v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    aput v4, v3, v6

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    iget v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    aput v4, v3, v5

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/FolderInfo;->remove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragInProgress:Z

    iput-boolean v6, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemAddedBackToSelfViaIcon:Z

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->setFocusOnFirstChild()V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-boolean v3, v3, Lcom/cyanogenmod/trebuchet/ItemInfo;->isLock:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->warnTargetisLocked()V

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {v4, v3}, Lcom/cyanogenmod/trebuchet/FolderInfo;->remove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->setFocusOnFirstChild()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onItemsChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Folder;->updateTextViewFocus()V

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    sparse-switch p2, :sswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getSelectionStart()I

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getSelectionStart()I

    move-result v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v1, v2, :cond_0

    :cond_5
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    goto/16 :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x42 -> :sswitch_3
    .end sparse-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissFolderCling(Landroid/view/View;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/cyanogenmod/trebuchet/Workspace;->onDragStartedWithItem(Landroid/view/View;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v2

    invoke-virtual {v2, p1, p0}, Lcom/cyanogenmod/trebuchet/Workspace;->beginDragShared(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;)V

    move-object v2, p1

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aget-object v2, v2, v4

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    iget v5, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    aput v5, v2, v3

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mEmptyCell:[I

    iget v5, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    aput v5, v2, v4

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragView:Landroid/view/View;

    invoke-virtual {v2, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {v2, v5}, Lcom/cyanogenmod/trebuchet/FolderInfo;->remove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragInProgress:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemAddedBackToSelfViaIcon:Z

    :cond_1
    move v2, v4

    goto :goto_0
.end method

.method public onRemove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mItemsInvalidated:Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurrentDragInfo:Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-ne p1, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Folder;->getViewForInfo(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mState:I

    if-ne v1, v2, :cond_1

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/Folder;->mRearrangeOnClose:Z

    :goto_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->setupContentForNumItems(I)V

    goto :goto_1
.end method

.method public onTitleChanged(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public popQuickActionMenu(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    instance-of v1, v0, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    if-eqz v1, :cond_1

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mAppQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v1, v0}, Lnet/londatiga/android/QuickAction;->show(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "Launcher.Folder"

    const-string v2, "the mIconQuickAction is not initialized"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method readingOrderGreaterThan([I[I)Z
    .locals 4
    .param p1    # [I
    .param p2    # [I

    const/4 v0, 0x0

    const/4 v1, 0x1

    aget v2, p1, v1

    aget v3, p2, v1

    if-gt v2, v3, :cond_1

    aget v2, p1, v1

    aget v3, p2, v1

    if-ne v2, v3, :cond_0

    aget v2, p1, v0

    aget v3, p2, v0

    if-gt v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public setDragController(Lcom/cyanogenmod/trebuchet/DragController;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/DragController;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    return-void
.end method

.method public setFocusOnFirstChild()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mContent:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1, v2, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method

.method setFolderIcon(Lcom/cyanogenmod/trebuchet/FolderIcon;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderIcon;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderIcon:Lcom/cyanogenmod/trebuchet/FolderIcon;

    return-void
.end method

.method public setmFolderState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderState:I

    return-void
.end method

.method public startEditingFolderName()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/FolderEditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/Folder;->mIsEditingName:Z

    return-void
.end method
