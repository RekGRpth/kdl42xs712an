.class Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;
.super Ljava/lang/Object;
.source "DragController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/DragController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScrollRunnable"
.end annotation


# instance fields
.field private mDirection:I

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/DragController;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/DragController;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    # getter for: Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/DragController;->access$0(Lcom/cyanogenmod/trebuchet/DragController;)Lcom/cyanogenmod/trebuchet/DragScroller;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->mDirection:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    # getter for: Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/DragController;->access$0(Lcom/cyanogenmod/trebuchet/DragController;)Lcom/cyanogenmod/trebuchet/DragScroller;

    move-result-object v0

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/DragScroller;->scrollLeft()V

    :goto_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/DragController;->access$1(Lcom/cyanogenmod/trebuchet/DragController;I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/DragController;->access$2(Lcom/cyanogenmod/trebuchet/DragController;I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    # getter for: Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/DragController;->access$0(Lcom/cyanogenmod/trebuchet/DragController;)Lcom/cyanogenmod/trebuchet/DragScroller;

    move-result-object v0

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/DragScroller;->onExitScrollArea()Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    # getter for: Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/DragController;->access$0(Lcom/cyanogenmod/trebuchet/DragController;)Lcom/cyanogenmod/trebuchet/DragScroller;

    move-result-object v0

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/DragScroller;->scrollRight()V

    goto :goto_0
.end method

.method setDirection(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->mDirection:I

    return-void
.end method
