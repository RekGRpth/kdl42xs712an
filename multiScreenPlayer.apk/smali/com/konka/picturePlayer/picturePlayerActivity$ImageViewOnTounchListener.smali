.class final Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;
.super Ljava/lang/Object;
.source "picturePlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ImageViewOnTounchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v5, 0x2

    const/high16 v4, 0x41200000    # 10.0f

    const/4 v6, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    return v6

    :pswitch_1
    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->start:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iput v6, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const/4 v3, 0x0

    iput v3, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # invokes: Lcom/konka/picturePlayer/picturePlayerActivity;->spacing(Landroid/view/MotionEvent;)F
    invoke-static {v3, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$9(Lcom/konka/picturePlayer/picturePlayerActivity;Landroid/view/MotionEvent;)F

    move-result v3

    iput v3, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->oldDist:F

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->oldDist:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->mid:Landroid/graphics/PointF;

    # invokes: Lcom/konka/picturePlayer/picturePlayerActivity;->midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    invoke-static {v2, v3, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$10(Lcom/konka/picturePlayer/picturePlayerActivity;Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iput v5, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    if-ne v2, v6, :cond_1

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v4, v4, Lcom/konka/picturePlayer/picturePlayerActivity;->start:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v5, v5, Lcom/konka/picturePlayer/picturePlayerActivity;->start:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    if-ne v2, v5, :cond_0

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # invokes: Lcom/konka/picturePlayer/picturePlayerActivity;->spacing(Landroid/view/MotionEvent;)F
    invoke-static {v2, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$9(Lcom/konka/picturePlayer/picturePlayerActivity;Landroid/view/MotionEvent;)F

    move-result v0

    cmpl-float v2, v0, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->oldDist:F

    div-float v1, v0, v2

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->mid:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v4, v4, Lcom/konka/picturePlayer/picturePlayerActivity;->mid:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
