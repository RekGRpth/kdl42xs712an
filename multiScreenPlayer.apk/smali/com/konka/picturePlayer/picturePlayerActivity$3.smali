.class Lcom/konka/picturePlayer/picturePlayerActivity$3;
.super Landroid/os/Handler;
.source "picturePlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->hideController()V

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/konka/picturePlayer/ImageViewTouch;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/picturePlayer/ImageViewTouch;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->hideController()V

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$3;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->stateDrawable:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$6(Lcom/konka/picturePlayer/picturePlayerActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
