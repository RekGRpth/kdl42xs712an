.class Lcom/konka/musicPlayer/musicPlayerActivity$5;
.super Ljava/lang/Object;
.source "musicPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 7
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "media player is prepared111111111:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$4(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$4(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$4(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->cancelProgressDlg()V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v2, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isMusicPlay:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v2, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$18(Lcom/konka/musicPlayer/musicPlayerActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&ma"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$18(Lcom/konka/musicPlayer/musicPlayerActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicPathTemp:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$19(Lcom/konka/musicPlayer/musicPlayerActivity;)Ljava/lang/String;

    move-result-object v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicPathTemp:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$19(Lcom/konka/musicPlayer/musicPlayerActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$20(Lcom/konka/musicPlayer/musicPlayerActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$5;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$21(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method
