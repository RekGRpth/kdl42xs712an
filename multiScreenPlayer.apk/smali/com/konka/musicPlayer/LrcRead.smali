.class public Lcom/konka/musicPlayer/LrcRead;
.super Ljava/lang/Object;
.source "LrcRead.java"


# instance fields
.field private LyricList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/musicPlayer/LyricContent;",
            ">;"
        }
    .end annotation
.end field

.field private mLyricContent:Lcom/konka/musicPlayer/LyricContent;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/konka/musicPlayer/LyricContent;

    invoke-direct {v0}, Lcom/konka/musicPlayer/LyricContent;-><init>()V

    iput-object v0, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/musicPlayer/LrcRead;->LyricList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public GetLyricContent()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/konka/musicPlayer/LyricContent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/musicPlayer/LrcRead;->LyricList:Ljava/util/List;

    return-object v0
.end method

.method public Read(Ljava/lang/String;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v10, 0x1

    const-string v0, ""

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v6, Ljava/io/InputStreamReader;

    const-string v8, "GBK"

    invoke-direct {v6, v5, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->LyricList:Ljava/util/List;

    invoke-virtual {p0, v8}, Lcom/konka/musicPlayer/LrcRead;->sortList(Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v6}, Ljava/io/InputStreamReader;->close()V

    return-void

    :cond_1
    const-string v8, "["

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v8, "]"

    const-string v9, "@"

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v8, "@"

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    if-le v8, v10, :cond_0

    array-length v8, v7

    const/4 v9, 0x2

    if-le v8, v9, :cond_2

    const/4 v2, 0x0

    :goto_1
    array-length v8, v7

    add-int/lit8 v8, v8, -0x1

    if-ge v2, v8, :cond_0

    iget-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    array-length v9, v7

    add-int/lit8 v9, v9, -0x1

    aget-object v9, v7, v9

    invoke-virtual {v8, v9}, Lcom/konka/musicPlayer/LyricContent;->setLyric(Ljava/lang/String;)V

    aget-object v8, v7, v2

    invoke-virtual {p0, v8}, Lcom/konka/musicPlayer/LrcRead;->TimeStr(Ljava/lang/String;)I

    move-result v1

    iget-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v8, v1}, Lcom/konka/musicPlayer/LyricContent;->setLyricTime(I)V

    iget-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->LyricList:Ljava/util/List;

    iget-object v9, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/konka/musicPlayer/LyricContent;

    invoke-direct {v8}, Lcom/konka/musicPlayer/LyricContent;-><init>()V

    iput-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    aget-object v9, v7, v10

    invoke-virtual {v8, v9}, Lcom/konka/musicPlayer/LyricContent;->setLyric(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-object v8, v7, v8

    invoke-virtual {p0, v8}, Lcom/konka/musicPlayer/LrcRead;->TimeStr(Ljava/lang/String;)I

    move-result v1

    iget-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v8, v1}, Lcom/konka/musicPlayer/LyricContent;->setLyricTime(I)V

    iget-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->LyricList:Ljava/util/List;

    iget-object v9, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/konka/musicPlayer/LyricContent;

    invoke-direct {v8}, Lcom/konka/musicPlayer/LyricContent;-><init>()V

    iput-object v8, p0, Lcom/konka/musicPlayer/LrcRead;->mLyricContent:Lcom/konka/musicPlayer/LyricContent;

    goto :goto_0
.end method

.method public TimeStr(Ljava/lang/String;)I
    .locals 7
    .param p1    # Ljava/lang/String;

    const-string v5, ":"

    const-string v6, "."

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v5, "."

    const-string v6, "@"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v5, "@"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v5, 0x1

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v5, 0x2

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit8 v5, v2, 0x3c

    add-int/2addr v5, v3

    mul-int/lit16 v5, v5, 0x3e8

    mul-int/lit8 v6, v1, 0xa

    add-int v0, v5, v6

    return v0
.end method

.method public sortList(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/konka/musicPlayer/LyricContent;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v6, v5, -0x1

    if-lt v3, v6, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    :goto_1
    sub-int v6, v5, v3

    add-int/lit8 v6, v6, -0x1

    if-lt v4, v6, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/musicPlayer/LyricContent;

    add-int/lit8 v6, v4, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v7

    if-le v6, v7, :cond_3

    new-instance v0, Lcom/konka/musicPlayer/LyricContent;

    invoke-direct {v0}, Lcom/konka/musicPlayer/LyricContent;-><init>()V

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/konka/musicPlayer/LyricContent;->setLyric(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/konka/musicPlayer/LyricContent;->setLyricTime(I)V

    invoke-virtual {v1}, Lcom/konka/musicPlayer/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/musicPlayer/LyricContent;->setLyric(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/konka/musicPlayer/LyricContent;->setLyricTime(I)V

    invoke-virtual {v0}, Lcom/konka/musicPlayer/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/konka/musicPlayer/LyricContent;->setLyric(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/konka/musicPlayer/LyricContent;->setLyricTime(I)V

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
