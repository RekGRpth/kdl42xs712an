.class Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;
.super Landroid/content/BroadcastReceiver;
.source "musicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "playerDataReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const v7, 0x7f0900a2    # com.konka.mediaSharePlayer.R.string.mediafile_buffering

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v3, "mediastop"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    const-string v2, "media stop"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "music---------------------mediastop: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget v4, v4, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "diskexist"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v2, v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$8(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v3, "haveNextFlag"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$24(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v3, "musicPath"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$25(Lcom/konka/musicPlayer/musicPlayerActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v3, "mediastop"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v3, "mobileIp"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$26(Lcom/konka/musicPlayer/musicPlayerActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v3, "resumePos"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$22(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    const-string v2, "noob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "music----resumePos: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$4(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    new-instance v3, Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {v3}, Lcom/konka/musicPlayer/MusicInfo;-><init>()V

    invoke-static {v2, v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$27(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/MusicInfo;)V

    const-string v2, "media stop"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "info.getData()11111------:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicPathTemp:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$19(Lcom/konka/musicPlayer/musicPlayerActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/musicPlayer/MusicInfo;->setData(Ljava/lang/String;)V

    const-string v2, "media stop"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "info.getData()2222------: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/musicPlayer/MusicInfo;->setDesplayName(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/musicPlayer/MusicInfo;->setTitle(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-boolean v2, v2, Lcom/konka/musicPlayer/musicPlayerActivity;->isMusicPlay:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->showProgressDialog(II)V
    invoke-static {v2, v7, v7}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$1(Lcom/konka/musicPlayer/musicPlayerActivity;II)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$21(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
