.class public Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;
.super Ljava/lang/Object;
.source "DataBaseDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/DataBaseDesk;


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND:[I

.field private static dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;


# instance fields
.field private final TAG:Ljava/lang/String;

.field public astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

.field blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

.field colorParaEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private cr:Landroid/content/ContentResolver;

.field customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

.field private factoryCusSchema:Ljava/lang/String;

.field private factorySchema:Ljava/lang/String;

.field mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

.field mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

.field mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

.field mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

.field m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_bADCAutoTune:Z

.field m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

.field m_stCISet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

.field m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

.field m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

.field m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

.field m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

.field m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

.field soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

.field stCECPara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

.field stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

.field stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

.field stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

.field stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

.field stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

.field private tableDirtyFlags:[Ljava/lang/Boolean;

.field private userSettingSchema:Ljava/lang/String;

.field videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_COLOR_TEMP_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_COMMAND_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_VIDEO_ADC_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_USER_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/16 v4, 0x58

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "com.konka.kkimplements.tv.mstar.DataBaseDeskImpl"

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->TAG:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "content://mstar.tv.usersetting"

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    const-string v1, "content://mstar.tv.factory"

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factorySchema:Ljava/lang/String;

    const-string v1, "content://konka.tv.factory"

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\n context  \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\n getContentResolver cr  \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\n getInstance   com \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarPicture()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->InitSettingVar()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initCECVar()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarSound()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarFactory()Z

    new-array v1, v4, [Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v4, :cond_0

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->openDB()V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->loadEssentialDataFromDB()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private InitSettingVar()Z
    .locals 11

    const-wide/16 v4, 0x0

    const/4 v3, 0x2

    const/4 v10, 0x1

    const/16 v1, 0x80

    const/4 v9, 0x0

    const-string v0, "TvService"

    const-string v2, "SettingServiceImpl InitVar!!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-direct {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const v2, 0xffff

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->checkSum:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v10, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CDSMATV:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->EN_SATEPF_HDPLUS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-wide v4, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;->MS_SUPER_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v10, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bTeletext:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-wide v4, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_BLACKSCREEN:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;->MS_OFFLINE_DET_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eOffDetMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bBlueScreen:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ePWR_Music:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;->MS_POWERON_LOGO_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ePWR_Logo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoOperation:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoSignal:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->screenSaveMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8SystemAutoTimeType:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;-><init>(SSSSSS)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v7, 0x0

    :goto_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    if-lt v7, v0, :cond_0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-direct {v0, v1, v2, v9, v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;-><init>(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;ZZ)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    invoke-direct {v0, v9, v9, v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;-><init>(III)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return v10

    :cond_0
    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;-><init>(IIIIII)V

    aput-object v0, v8, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public static getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    if-nez v0, :cond_0

    const-string v0, "KKJAVAAPI"

    const-string v1, "create databasedesk now"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    const-string v0, "KKJAVAAPI"

    const-string v1, "DataBaseDeskImpl "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->openDB()V

    const-string v0, "KKJAVAAPI"

    const-string v1, "open DB"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    return-object v0
.end method

.method private initCECVar()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

    const v1, 0xffff

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;-><init>(ISSSS)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stCECPara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method private initVarFactory()Z
    .locals 12

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_bADCAutoTune:Z

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v8, :cond_0

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_2

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v2, 0x0

    :goto_2
    if-lt v2, v8, :cond_4

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v3, 0x0

    :goto_3
    if-lt v3, v8, :cond_6

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stCISet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    const/4 v10, 0x1

    return v10

    :cond_0
    const/4 v4, 0x0

    :goto_4
    if-lt v4, v9, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v0

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_2
    const/4 v5, 0x0

    :goto_5
    if-lt v5, v9, :cond_3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_4
    const/4 v6, 0x0

    :goto_6
    if-lt v6, v9, :cond_5

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v2

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    :cond_6
    const/4 v7, 0x0

    :goto_7
    if-lt v7, v9, :cond_7

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v3

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_7
.end method

.method private initVarPicture()Z
    .locals 24

    const/4 v2, 0x7

    new-array v0, v2, [[S

    move-object/from16 v21, v0

    const/4 v2, 0x0

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_0

    aput-object v3, v21, v2

    const/4 v2, 0x1

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_1

    aput-object v3, v21, v2

    const/4 v2, 0x2

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_2

    aput-object v3, v21, v2

    const/4 v2, 0x3

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_3

    aput-object v3, v21, v2

    const/4 v2, 0x4

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_4

    aput-object v3, v21, v2

    const/4 v2, 0x5

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_5

    aput-object v3, v21, v2

    const/4 v2, 0x6

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_6

    aput-object v3, v21, v2

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-direct {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const v3, 0xffff

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->CheckSum:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move/from16 v0, v19

    new-array v3, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    const/16 v20, 0x0

    :goto_0
    move/from16 v0, v20

    move/from16 v1, v19

    if-lt v0, v1, :cond_0

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move/from16 v0, v19

    new-array v3, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    const/16 v20, 0x0

    :goto_1
    move/from16 v0, v20

    move/from16 v1, v19

    if-lt v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const v9, 0xffff

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v3, v9, v10, v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;-><init>(ISS)V

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_FULL_HD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->MAPI_VIDEO_OUT_VE_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->E_MAPI_VIDEOSTANDARD_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;->E_AUDIOMODE_MONO_:Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;->MS_FILM_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v9, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;->DB_ThreeD_Video_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    sget-object v11, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    sget-object v12, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    sget-object v13, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    sget-object v14, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->DB_ThreeD_Video_3DDEPTH_LEVEL_15:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    sget-object v15, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->DB_ThreeD_Video_3DOFFSET_LEVEL_15:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    sget-object v16, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->DB_ThreeD_Video_AUTOSTART_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    sget-object v17, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->DB_ThreeD_Video_3DOUTPUTASPECT_FULLSCREEN:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    sget-object v18, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-direct/range {v9 .. v18}, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;-><init>(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)V

    iput-object v9, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v3, v9, v10, v11, v12}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;-><init>(SSSS)V

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->DISPLAY_TVFORMAT_16TO9HD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->SKIN_TONE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->skinTone:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->detailEnhance:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->DNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    new-instance v9, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/16 v10, 0x80

    const/16 v11, 0x80

    const/16 v12, 0x80

    const/16 v13, 0x80

    const/16 v14, 0x80

    const/16 v15, 0x80

    invoke-direct/range {v9 .. v15}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;-><init>(IIIIII)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->colorParaEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v2, 0x1

    return v2

    :cond_0
    const/16 v22, 0x0

    aget-object v2, v21, v20

    add-int/lit8 v23, v22, 0x1

    aget-short v4, v2, v22

    aget-object v2, v21, v20

    add-int/lit8 v22, v23, 0x1

    aget-short v5, v2, v23

    aget-object v2, v21, v20

    add-int/lit8 v23, v22, 0x1

    aget-short v6, v2, v22

    aget-object v2, v21, v20

    add-int/lit8 v22, v23, 0x1

    aget-short v7, v2, v23

    aget-object v2, v21, v20

    aget-short v8, v2, v22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v14, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    const/16 v3, 0x64

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v11, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v12, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v13, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-direct/range {v2 .. v13}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;-><init>(SSSSSSLcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;)V

    aput-object v2, v14, v20

    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    new-instance v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-direct {v3, v9, v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;-><init>(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)V

    aput-object v3, v2, v20

    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_1

    :array_0
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_1
    .array-data 2
        0x3cs
        0x37s
        0x3cs
        0x3cs
        0x32s
    .end array-data

    nop

    :array_2
    .array-data 2
        0x28s
        0x2ds
        0x2ds
        0x28s
        0x32s
    .end array-data

    nop

    :array_3
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_4
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_5
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_6
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data
.end method

.method private initVarSound()Z
    .locals 15

    const/16 v14, 0x1e

    const/4 v13, 0x1

    const/16 v12, 0x3c

    const/16 v11, 0x28

    const/16 v1, 0x32

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-direct {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const v2, 0xffff

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->u16CheckSum:I

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/16 v3, 0x46

    const/16 v5, 0x46

    move v4, v11

    move v6, v12

    move v7, v1

    move v8, v1

    move v9, v11

    invoke-direct/range {v2 .. v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v13

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v10, 0x2

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/16 v7, 0x2d

    move v3, v12

    move v4, v14

    move v5, v12

    move v6, v1

    move v8, v11

    move v9, v14

    invoke-direct/range {v2 .. v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v10

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v10, 0x3

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/16 v4, 0x50

    const/16 v6, 0x2d

    const/16 v9, 0x50

    move v3, v11

    move v5, v11

    move v7, v1

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v10

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x4

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x5

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x6

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    return v13
.end method

.method private static openDB()V
    .locals 0

    return-void
.end method

.method private readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v2, p2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public IsSystemLocked()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    iget v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysLockMode:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public SyncUserSettingDB()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    return-void
.end method

.method public UpdateDB()V
    .locals 2

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    return-void
.end method

.method public closeDB()V
    .locals 0

    return-void
.end method

.method public deleteEpg(I)I
    .locals 7
    .param p1    # I

    :try_start_0
    const-string v3, " u32StartTime = ? "

    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/epgtimer"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAdcSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    return-object v0
.end method

.method public getCECVar()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stCECPara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

    return-object v0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    return-object v0
.end method

.method public getCustomerCfgMiscSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    return-object v0
.end method

.method public getDTVCity()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/chinadvbcsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    move-result-object v0

    const-string v1, "eDVBCRegion"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getDVBCNetTableFrequency()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/chinadvbcsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u32NITFreq"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public getDefinitionOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
    .locals 8

    const/4 v2, 0x0

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/srssetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v0

    const-string v1, "bDefinitionOn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getDialogClarityOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
    .locals 8

    const/4 v2, 0x0

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/srssetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v0

    const-string v1, "bDialogClarityOn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getDynamicBLMode()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/systemsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enLocalDimm"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const-string v0, "enLocalDimm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "value======="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v7
.end method

.method public getFactoryExt()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    return-object v0
.end method

.method public getLocationSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return-object v0
.end method

.method public getNoStandSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    return-object v0
.end method

.method public getNoStandVifSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    return-object v0
.end method

.method public getParentalControlRating()I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->parentalControl:I

    return v0
.end method

.method public getSRSOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
    .locals 8

    const/4 v2, 0x0

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/srssetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v0

    const-string v1, "bSRSOn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    return-object v0
.end method

.method public getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    return-object v0
.end method

.method public getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;
    .locals 2
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getSpdifMode()Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;
    .locals 1

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSPDIFMODE:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    return-object v0
.end method

.method public getSscSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    return-object v0
.end method

.method public getSubtitleSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    return-object v0
.end method

.method public getSystemLockPassword()I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysPassword:I

    return v0
.end method

.method public getTableDirtyFlags(I)Ljava/lang/Boolean;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getTruebaseOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
    .locals 8

    const/4 v2, 0x0

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/srssetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v0

    const-string v1, "bTruebassOn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    return-object v0
.end method

.method public getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "DataBaseServiceImpl getVideo!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    return-object v0
.end method

.method public getVideoTemp()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    return-object v0
.end method

.method public getVideoTempEx()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->colorParaEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    return-object v0
.end method

.method public insertEpg(Landroid/content/ContentValues;I)J
    .locals 8
    .param p1    # Landroid/content/ContentValues;
    .param p2    # I

    const-wide/16 v1, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/epgtimer"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "u32StartTime = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, p1, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_0
    return-wide v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public isPCTimingNew()Z
    .locals 4

    const/4 v0, 0x0

    const-string v1, "DataBaseDeskImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "c video info ModeIndex is :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    const-string v1, "DataBaseDeskImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "~method(isPCTimingNew)~~~~~~~ id is :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "||||||   ModeIndex is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v1

    int-to-short v1, v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v2

    iget-short v2, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    if-ne v1, v2, :cond_1

    const-string v1, "DataBaseDeskImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "~~~~~~~~isPCTimingNew id is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public loadEssentialDataFromDB()V
    .locals 8

    const/4 v2, 0x0

    const/16 v6, 0x22

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/systemsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enInputSourceType"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    invoke-virtual {p0, v6}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryColorTempData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryADCAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryExtern()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNoStandSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySSCAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserLocSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUsrColorTmpData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSubtitleSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUsrColorTmpExData()[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundModeSettings()[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySRSAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryCustomerCfgMiscSetting()V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryBlockSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    return-void
.end method

.method public queryADCAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/adcadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    return-object v0

    :cond_1
    add-int/lit8 v0, v8, -0x1

    if-gt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redgain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greengain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->bluegain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redoffset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greenoffset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->blueoffset:I

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;
    .locals 22
    .param p1    # I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/videosetting/inputsrc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    :goto_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    const-string v4, " InputSrcType = ? "

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/picmode_setting"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const-string v6, "PictureModeType"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    array-length v0, v1

    move/from16 v19, v0

    :goto_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_7

    :cond_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    const-string v4, " InputSrcType = ? "

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v2, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/nrmode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const-string v6, "NRMode"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    array-length v0, v1

    move/from16 v20, v0

    :goto_2
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_8

    :cond_1
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/threedvideomode/inputsrc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    move-result-object v2

    const-string v3, "eThreeDVideo"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    move-result-object v2

    const-string v3, "eThreeDVideo3DDepth"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    move-result-object v2

    const-string v3, "eThreeDVideo3DOffset"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    move-result-object v2

    const-string v3, "eThreeDVideoAutoStart"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    move-result-object v2

    const-string v3, "eThreeDVideo3DOutputAspect"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    move-result-object v2

    const-string v3, "eThreeDVideoLRViewSwitch"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    :cond_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/threedvideomode/inputsrc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v2

    const-string v3, "eThreeDVideoSelfAdaptiveDetect"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    :cond_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/useroverscanmode/inputsrc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanHposition"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHposition:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanVposition"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVposition:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanHRatio"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHRatio:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanVRatio"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVRatio:S

    :cond_4
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    return-object v1

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v2

    const-string v3, "ePicture"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v2

    const-string v3, "enARCType"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-result-object v2

    const-string v3, "fOutput_RES"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-result-object v2

    const-string v3, "tvsys"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v2

    const-string v3, "LastVideoStandardMode"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-result-object v2

    const-string v3, "LastAudioStandardMode"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-result-object v2

    const-string v3, "eDynamic_Contrast"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    move-result-object v2

    const-string v3, "eFilm"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    move-result-object v2

    const-string v3, "eTvFormat"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    move-result-object v2

    const-string v3, "skinTone"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->skinTone:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const-string v1, "detailEnhance"

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    :goto_3
    iput-boolean v1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->detailEnhance:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v2

    const-string v3, "DNR"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->DNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const-string v2, "u8SubBrightness"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubBrightness:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const-string v2, "u8SubContrast"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubContrast:S

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x1

    goto :goto_3

    :cond_7
    add-int/lit8 v1, v19, -0x1

    move/from16 v0, v21

    if-gt v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    const-string v2, "u8Backlight"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    const-string v2, "u8Contrast"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    const-string v2, "u8Brightness"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    const-string v2, "u8Saturation"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    const-string v2, "u8Sharpness"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    const-string v2, "u8Hue"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v2

    const-string v3, "eColorTemp"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "eVibrantColour"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eVibrantColour:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "ePerfectClear"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->ePerfectClear:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "eDynamicContrast"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicContrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v21

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "eDynamicBacklight"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicBacklight:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v1, v20, -0x1

    if-gt v12, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v1, v1, v12

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v2

    const-string v3, "eNR"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v1, v1, v12

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    move-result-object v2

    const-string v3, "eMPEG_NR"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2
.end method

.method public queryAutoMHLSwitch()I
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bAutoMHLSwitch"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryAutoTimeMode()I
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/timesetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bClockMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryBlockSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/blocksyssetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const-string v4, "u8BlockSysLockMode"

    aput-object v4, v2, v7

    const-string v4, "u8UnratedLoack"

    aput-object v4, v2, v8

    const-string v4, "u8VideoBlockMode"

    aput-object v4, v2, v9

    const-string v4, "u8BlockSysPWSetStatus"

    aput-object v4, v2, v10

    const/4 v4, 0x5

    const-string v5, "u8ParentalControl"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "u8EnterLockPage"

    aput-object v5, v2, v4

    const/4 v4, 0x7

    const-string v5, "u16BlockSysPassword"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysLockMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->unrateLock:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->videoBlockMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysPWSetStatus:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v1, 0x5

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->parentalControl:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v1, 0x6

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->enterLockPage:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v1, 0x7

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysPassword:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    return-object v0
.end method

.method public queryColorTemp(II)I
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/picmode_setting/inputsrc/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/picmode/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "PictureModeType"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v7, 0x0

    const-string v0, "eColorTemp"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryCurCountry()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/systemsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Country"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryCurInputSrc()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/systemsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enInputSourceType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryCustomerCfgMiscSetting()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/miscsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "getColumnIndex=%x\n"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "EnergyEnable"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "getInt=%x\n"

    new-array v2, v8, [Ljava/lang/Object;

    const-string v3, "EnergyEnable"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    const-string v0, "EnergyEnable"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyEnable:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    const-string v1, "EnergyPercent"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyPercent:S

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-void

    :cond_1
    move v0, v8

    goto :goto_0
.end method

.method public queryEpg(ZI)Landroid/database/Cursor;
    .locals 12
    .param p1    # Z
    .param p2    # I

    const/4 v2, 0x0

    const/4 v11, 0x0

    if-eqz p1, :cond_0

    const-string v3, "u32StartTime = ? "

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/epgtimer"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    :goto_0
    return-object v11

    :cond_0
    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/epgtimer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    goto :goto_0
.end method

.method public queryEventName(S)Ljava/lang/String;
    .locals 8
    .param p1    # S

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "content://mstar.tv.usersetting/epgtimer/"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7}, Ljava/lang/String;-><init>()V

    const-string v0, "sEventName"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\n=====>>eventName "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " @epgTimerIndex "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryFactoryColorTempData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;
    .locals 9

    const/4 v2, 0x0

    const-string v0, "content://mstar.tv.factory/factorycolortemp"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "ColorTemperatureID"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    return-object v0

    :cond_1
    add-int/lit8 v0, v8, -0x1

    if-gt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v2, "u8RedGain"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v2, "u8GreenGain"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v2, "u8BlueGain"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v2, "u8RedOffset"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v2, "u8GreenOffset"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v2, "u8BlueOffset"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0
.end method

.method public queryFactoryColorTempExData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;
    .locals 9

    const-string v3, ""

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v8, 0x0

    :goto_0
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v0

    if-lt v8, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    return-object v0

    :cond_0
    const-string v3, " InputSourceID = ? "

    const/4 v0, 0x0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/factorycolortempex"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v5, "ColorTemperatureID"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const/4 v6, 0x0

    :goto_1
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    if-lt v6, v0, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v8

    const-string v1, "u16RedGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v8

    const-string v1, "u16GreenGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v8

    const-string v1, "u16BlueGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v8

    const-string v1, "u16RedOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v8

    const-string v1, "u16GreenOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v8

    const-string v1, "u16BlueOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1
.end method

.method public queryFactoryExtern()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/factoryextern"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SoftWareVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    const-string v0, "BoardType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    const-string v0, "PanelType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    const-string v0, "CompileTime"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "TestPatternMode"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "stPowerMode"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "DtvAvAbnormalDelay"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "FactoryPreSetFeature"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "PanelSwing"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "AudioPrescale"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "vdDspVersion"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "eHidevMode"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "audioNrThr"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "audioSifThreshold"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "audioDspVersion"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v1, "bBurnIn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    :goto_1
    iput-boolean v7, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->bBurnIn:Z

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    return-object v0

    :cond_1
    move v0, v8

    goto/16 :goto_0

    :cond_2
    move v7, v8

    goto :goto_1
.end method

.method public queryNoStandSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_D4"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D4:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_D5_Bit2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D5_Bit2:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_D8_Bit3210"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D8_Bit3210:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_D9_Bit0"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D9_Bit0:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_D7_LOW_BOUND"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_LOW_BOUND:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_D7_HIGH_BOUND"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_HIGH_BOUND:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_A0"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A0:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_A1"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A1:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_66_Bit76"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_66_Bit76:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_6E_Bit7654"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit7654:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_6E_Bit3210"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit3210:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_44"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_44:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v1, "u8AFEC_CB"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_CB:S

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    return-object v0
.end method

.method public queryNoStandVifSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifTop"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifVgaMaximum"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifCrKp"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifCrKi"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifCrKp1"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifCrKi1"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifCrKp2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifCrKi2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "VifAsiaSignalOption"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "VifCrKpKiAdjust"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    if-nez v0, :cond_2

    move v0, v7

    :goto_1
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifOverModulation"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_3

    :goto_2
    iput-boolean v7, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifClampgainGainOvNegative"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "ChinaDescramblerBox"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifDelayReduce"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifCrThr"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifVersion"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "VifACIAGCREF"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v1, "GainDistributionThr"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    return-object v0

    :cond_1
    move v0, v8

    goto/16 :goto_0

    :cond_2
    move v0, v8

    goto/16 :goto_1

    :cond_3
    move v7, v8

    goto :goto_2
.end method

.method public queryNonLinearAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;
    .locals 9

    const-string v3, " InputSrcType = ? "

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/nonlinearadjust"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v5, "CurveTypeIndex"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    return-object v0

    :cond_1
    add-int/lit8 v0, v8, -0x1

    if-gt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V0"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V0:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V25"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V25:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V50"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V50:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V75"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V75:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V100"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V100:S

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;
    .locals 19
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    const-string v1, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v4, " FactoryOverScanType = ? "

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const-string v6, "_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v11

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v12

    const/4 v9, 0x0

    :goto_1
    if-lt v9, v11, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    :goto_2
    if-lt v10, v12, :cond_1

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16H_CapStart"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16V_CapStart"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Left"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Right"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Up"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Down"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    :pswitch_1
    const-string v1, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v4, " FactoryOverScanType = ? "

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const-string v6, "_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v13

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v14

    const/4 v9, 0x0

    :goto_3
    if-lt v9, v13, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto/16 :goto_0

    :cond_3
    const/4 v10, 0x0

    :goto_4
    if-lt v10, v14, :cond_4

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16H_CapStart"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16V_CapStart"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Left"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Right"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Up"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Down"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_4

    :pswitch_2
    const-string v1, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v4, " FactoryOverScanType = ? "

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const-string v6, "_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v17

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v18

    const/4 v9, 0x0

    :goto_5
    move/from16 v0, v17

    if-lt v9, v0, :cond_6

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto/16 :goto_0

    :cond_6
    const/4 v10, 0x0

    :goto_6
    move/from16 v0, v18

    if-lt v10, v0, :cond_7

    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_7
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16H_CapStart"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16V_CapStart"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Left"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Right"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Up"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Down"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_6

    :pswitch_3
    const-string v1, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v4, " FactoryOverScanType = ? "

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const-string v6, "_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v15

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v16

    const/4 v9, 0x0

    :goto_7
    if-lt v9, v15, :cond_9

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto/16 :goto_0

    :cond_9
    const/4 v10, 0x0

    :goto_8
    move/from16 v0, v16

    if-lt v10, v0, :cond_a

    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    :cond_a
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16H_CapStart"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u16V_CapStart"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Left"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8HCrop_Right"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Up"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v1, v1, v9

    aget-object v1, v1, v10

    const-string v3, "u8VCrop_Down"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_b
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public queryPCClock()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-lt v7, v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u16UI_Clock"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_1
    invoke-virtual {p0, v7}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v1

    iget-short v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "~~~~~~~~clock id is "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPCHPos()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-lt v7, v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u16UI_HorizontalStart"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_1
    invoke-virtual {p0, v7}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v1

    iget-short v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "~~~~~~~~Hpos id is "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPCModeIndex(I)I
    .locals 9
    .param p1    # I

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v8, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u8ModeIndex"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v8
.end method

.method public queryPCPhase()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "video info ModeIndex is :"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-lt v7, v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u16UI_Phase"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_1
    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "~~~~~~~~ id is :"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "||||||   ModeIndex is:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v7}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v7}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v1

    iget-short v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "~~~~~~~~Phase id is "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0
.end method

.method public queryPCVPos()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-lt v7, v0, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u16UI_VorizontalStart"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_1
    invoke-virtual {p0, v7}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v1

    iget-short v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "~~~~~~~~Vpos id is "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPEQAdjust(I)Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    new-instance v7, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    invoke-direct {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "content://mstar.tv.factory/peqadjust/"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Band"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Band:I

    const-string v0, "Gain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    const-string v0, "Foh"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    const-string v0, "Fol"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    const-string v0, "QValue"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryPEQAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/peqadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    return-object v0

    :cond_1
    add-int/lit8 v0, v8, -0x1

    if-gt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Band"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Band:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Gain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Foh"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Fol"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "QValue"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPicMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    const-string v0, "ePicture"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryPicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/picmode_setting/inputsrc/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/picmode/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "PictureModeType"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v7, 0x0

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7

    :pswitch_0
    const-string v0, "u8Brightness"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_1
    const-string v0, "u8Contrast"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_2
    const-string v0, "u8Hue"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_3
    const-string v0, "u8Saturation"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_4
    const-string v0, "u8Sharpness"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_5
    const-string v0, "u8Backlight"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public querySRSAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/srsadjust"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v1, "iInputGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v1, "iSurrLevelCtl"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v1, "iSpeakerAudio"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v1, "iSpeakerAnalysis"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v1, "iTrubassCtl"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v1, "iDCCtl"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v1, "iDefinitionCtl"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    return-object v0
.end method

.method public querySSCAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/sscadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v0, "Miu_SscEnable"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Lvds_SscEnable"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    :goto_1
    iput-boolean v7, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Lvds_SscSpan"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Lvds_SscStep"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Miu_SscSpan"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Miu_SscStep"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Miu1_SscSpan"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Miu1_SscStep"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Miu2_SscSpan"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v1, "Miu2_SscStep"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    return-object v0

    :cond_1
    move v0, v8

    goto/16 :goto_0

    :cond_2
    move v7, v8

    goto :goto_1
.end method

.method public queryServiceName(S)Ljava/lang/String;
    .locals 8
    .param p1    # S

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "content://mstar.tv.usersetting/epgtimer/"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7}, Ljava/lang/String;-><init>()V

    const-string v0, "sServiceName"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\n=====>>serviceName "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " @epgTimerIndex "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public querySoundMode()I
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/soundsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    const-string v0, "SoundMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public querySoundModeSettings()[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/soundmodesetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    return-object v0

    :cond_1
    add-int/lit8 v0, v8, -0x1

    if-gt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "Bass"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "Treble"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand1"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand3"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand4"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand5"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand6"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand6:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand7"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand7:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v0, v7

    const-string v0, "UserMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->UserMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "Balance"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Balance:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    move-result-object v1

    const-string v2, "enSoundAudioChannel"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->enSoundAudioChannel:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
    .locals 15

    const/4 v14, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/soundsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v1

    const-string v2, "SoundMode"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    move-result-object v1

    const-string v2, "AudysseyDynamicVolume"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyDynamicVolume:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    move-result-object v1

    const-string v2, "AudysseyEQ"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyEQ:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    move-result-object v1

    const-string v2, "SurroundSoundMode"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundSoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    move-result-object v1

    const-string v2, "Surround"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v0, "bEnableAVC"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v13

    :goto_0
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Volume"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "HPVolume"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->HPVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Balance"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Primary_Flag"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Primary_Flag:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "enSoundAudioLan1"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v8, v0, v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "enSoundAudioLan2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v9, v0, v1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_2
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "MUTE_Flag"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->MUTE_Flag:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    move-result-object v1

    const-string v2, "enSoundAudioChannel"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioChannel:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "bEnableAD"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_8

    :goto_3
    iput-boolean v13, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAD:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "ADVolume"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    move-result-object v1

    const-string v2, "ADOutput"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADOutput:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "SPDIF_Delay"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SPDIF_Delay:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Speaker_Delay"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Speaker_Delay:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v1

    const-string v2, "hdmi1AudioSource"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi1AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v1

    const-string v2, "hdmi2AudioSource"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi2AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v1

    const-string v2, "hdmi3AudioSource"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi3AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v1

    const-string v2, "hdmi4AudioSource"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi4AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    return-object v0

    :cond_1
    move v0, v14

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_3
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "enSoundAudioLan1"

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v10, -0x1

    :try_start_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/soundsetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v12, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v10, v0

    goto/16 :goto_1

    :catch_0
    move-exception v7

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_6
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_7

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "enSoundAudioLan2"

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v10, -0x1

    :try_start_1
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/soundsetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v12, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    int-to-long v10, v0

    goto/16 :goto_2

    :catch_1
    move-exception v7

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_8
    move v13, v14

    goto/16 :goto_3
.end method

.method public querySourceIdent()I
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bSourceDetectEnable"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public querySourceSwit()I
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bAutoSourceSwitch"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryThreeDVideo3DTo2D(I)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v0

    const-string v1, "eThreeDVideo3DTo2D"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryThreeDVideoDisplayFormat(I)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v0

    const-string v1, "eThreeDVideoDisplayFormat"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryUserLocSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/userlocationsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v1, "u16LocationNo"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mLocationNo:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v1, "s16ManualLongitude"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLongitude:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v1, "s16ManualLatitude"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLatitude:I

    goto :goto_0
.end method

.method public queryUserSubtitleSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
    .locals 15

    const/4 v14, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/subtitlesetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "SubtitleDefaultLanguage"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v8, v0, v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "SubtitleDefaultLanguage_2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v9, v0, v1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const-string v0, "fHardOfHearing"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v13

    :goto_2
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fHardOfHearing:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const-string v1, "fEnableSubTitle"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_8

    :goto_3
    iput-boolean v13, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fEnableSubTitle:Z

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "SubtitleDefaultLanguage"

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v10, -0x1

    :try_start_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/subtitlesetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v12, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v10, v0

    goto/16 :goto_0

    :catch_0
    move-exception v7

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "SubtitleDefaultLanguage_2"

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v10, -0x1

    :try_start_1
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/subtitlesetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v12, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    int-to-long v10, v0

    goto/16 :goto_1

    :catch_1
    move-exception v7

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    move v0, v14

    goto/16 :goto_2

    :cond_8
    move v13, v14

    goto/16 :goto_3
.end method

.method public queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
    .locals 15

    const/4 v2, 0x0

    const/4 v14, 0x1

    const/4 v13, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/systemsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const-string v6, "CN"

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "fRunInstallationGuide"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v13

    :goto_0
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "fNoChannel"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v13

    :goto_1
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bDisableSiAutoUpdate"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v13

    :goto_2
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    const-string v2, "enInputSourceType"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v1

    const-string v2, "Country"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    move-result-object v1

    const-string v2, "enCableOperators"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    move-result-object v1

    const-string v2, "enSatellitePlatform"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "Language"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v9, v0, v1

    const-string v0, "/system/build.prop"

    const-string v1, "persist.sys.country"

    invoke-direct {p0, v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "CN"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_3
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    move-result-object v1

    const-string v2, "enSPDIFMODE"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSPDIFMODE:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fSoftwareUpdate"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fSoftwareUpdate:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "U8OADTime"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fOADScanAfterWakeup"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fAutoVolume"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fDcPowerOFFMode"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "DtvRoute"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "ScartOutRGB"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "U8Transparency"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u32MenuTimeOut"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "AudioOnly"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "bEnableWDT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8FavoriteRegion"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8Bandwidth"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8TimeShiftSizeType"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fOadScan"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "bEnablePVRRecordAll"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8ColorRangeMode"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8HDMIAudioSource"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "bEnableAlwaysTimeshift"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    move-result-object v1

    const-string v2, "eSUPER"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bUartBus"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v13

    :goto_4
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bTeletext"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_9

    move v0, v13

    :goto_5
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bTeletext:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "m_AutoZoom"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bOverScan"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_a

    move v0, v13

    :goto_6
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "m_u8BrazilVideoStandardType"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "m_u8SoftwareUpdateMode"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "OSD_Active_Time"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "m_MessageBoxExist"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_b

    move v0, v13

    :goto_7
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u16LastOADVersion"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bEnableAutoChannelUpdate"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_c

    move v0, v13

    :goto_8
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "standbyNoOperation"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoOperation:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "standbyNoSignal"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_d

    move v0, v13

    :goto_9
    iput-boolean v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoSignal:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "screenSaveMode"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_e

    :goto_a
    iput-boolean v13, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->screenSaveMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "U8SystemAutoTimeType"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8SystemAutoTimeType:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v1

    const-string v2, "smartEnergySaving"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    move-result-object v1

    const-string v2, "colorWheelMode"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    return-object v0

    :cond_1
    move v0, v14

    goto/16 :goto_0

    :cond_2
    move v0, v14

    goto/16 :goto_1

    :cond_3
    move v0, v14

    goto/16 :goto_2

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v9, v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "Language"

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v10, -0x1

    :try_start_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/systemsetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v12, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v10, v0

    goto/16 :goto_3

    :catch_0
    move-exception v8

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_7
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-object v9, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_8
    move v0, v14

    goto/16 :goto_4

    :cond_9
    move v0, v14

    goto/16 :goto_5

    :cond_a
    move v0, v14

    goto/16 :goto_6

    :cond_b
    move v0, v14

    goto/16 :goto_7

    :cond_c
    move v0, v14

    goto/16 :goto_8

    :cond_d
    move v0, v14

    goto/16 :goto_9

    :cond_e
    move v13, v14

    goto/16 :goto_a
.end method

.method public queryUsrColorTmpData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/usercolortemp"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8RedGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8GreenGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8BlueGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8RedOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8GreenOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8BlueOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryUsrColorTmpExData()[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/usercolortempex"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryVideo3DSelfAdaptiveDetectMode(I)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .locals 9
    .param p1    # I

    const/4 v2, 0x0

    sget-object v8, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "eThreeDVideoSelfAdaptiveDetect"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v0, "DEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "===========================index============================"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v0

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v8, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v8
.end method

.method public queryVideoArcMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    const-string v0, "enARCType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public restoreUsrDB(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;)Z
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "restoreUsrDB : E_FACTORY_RESTORE_DEFAULT!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarPicture()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->InitSettingVar()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initCECVar()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarSound()Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "restoreUsrDB : E_FACTORY_RESTORE_DEFAULT!!"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public saveInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-object p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x0

    return v0
.end method

.method public setAdcSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setAutoTimeMode(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "bClockMode"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/timesetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "DataBaseDeskImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "update bClockMode:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v2

    const/16 v3, 0x1b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setBlockSysPassword(I)V
    .locals 8
    .param p1    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16BlockSysPassword"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/blocksyssetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    int-to-long v1, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    iput p1, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysPassword:I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update block system password  failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setCECVar(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stCECPara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setCustomerCfgMiscSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    const/4 v0, 0x1

    return v0
.end method

.method public setDTVCity(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V
    .locals 8
    .param p1    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "eDVBCRegion"

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "\n########### update \n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/chinadvbcsetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "\n########### update failed \n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setDVBCNetTableFrequency(I)V
    .locals 8
    .param p1    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u32NITFreq"

    mul-int/lit16 v5, p1, 0x3e8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/chinadvbcsetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setDefinitionOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "bDefinitionOn"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srssetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSSetting error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDialogClarityOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "bDialogClarityOn"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srssetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSSetting error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDynamicBLMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "enLocalDimm"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/systemsetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setFactoryExt(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setLocationSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setNoStandSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const/4 v0, 0x1

    return v0
.end method

.method public setNoStandVifSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const/4 v0, 0x1

    return v0
.end method

.method public setParentalControl(I)V
    .locals 8
    .param p1    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8ParentalControl"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/blocksyssetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    int-to-long v1, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    iput p1, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->parentalControl:I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update parental control  failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setSRSOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "bSRSOn"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srssetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSSetting error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;
    .param p2    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_INPUTGAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p2, v2, :cond_1

    const-string v2, "iInputGain"

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srsadjust"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    :cond_0
    :goto_1
    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSAdjust error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_SURRLEVEL_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p2, v2, :cond_2

    const-string v2, "iSurrLevelCtl"

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srsadjust"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSAdjust error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_SPEAKERAUDIO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p2, v2, :cond_3

    const-string v2, "iSpeakerAudio"

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_2
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srsadjust"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSAdjust error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_3
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_SPEAKERANALYSIS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p2, v2, :cond_4

    const-string v2, "iSpeakerAnalysis"

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_3
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srsadjust"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    goto/16 :goto_1

    :catch_3
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSAdjust error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_4
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_TRUBASS_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p2, v2, :cond_5

    const-string v2, "iTrubassCtl"

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_4
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srsadjust"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    goto/16 :goto_1

    :catch_4
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSAdjust error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_5
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_DC_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p2, v2, :cond_6

    const-string v2, "iDCCtl"

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_5
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srsadjust"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    goto/16 :goto_1

    :catch_5
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSAdjust error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_6
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_DEFINITION_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p2, v2, :cond_0

    const-string v2, "iDefinitionCtl"

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_6
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srsadjust"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    iget v3, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    goto/16 :goto_1

    :catch_6
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSAdjust error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7
.end method

.method public setSound(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v0, 0x0

    return v0
.end method

.method public setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z
    .locals 7
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "SoundMode"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v1, -0x1

    :try_start_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/soundsetting"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    const/4 v3, 0x0

    :goto_1
    return v3

    :catch_0
    move-exception v0

    const-string v3, "DataBaseDeskImpl"

    const-string v4, "update failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;)Z
    .locals 2
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;
    .param p2    # Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aput-object p2, v1, v0

    const/4 v1, 0x1

    return v1
.end method

.method public setSpdifMode(Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;)V
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-object p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSPDIFMODE:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    return-void
.end method

.method public setSscSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const/4 v0, 0x1

    return v0
.end method

.method public setSubtitleSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setSystemLock(I)V
    .locals 8
    .param p1    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8BlockSysLockMode"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/blocksyssetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    int-to-long v1, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    iput p1, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysLockMode:I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update system lock  failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setTableDirty(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, p1

    return-void
.end method

.method public setTruebaseOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "bTruebassOn"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/srssetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "KKJAVAAPI"

    const-string v3, "update tbl_SRSSetting error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setUsrData(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setVideo(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const/4 v0, 0x1

    return v0
.end method

.method public setVideoTemp(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    const/4 v0, 0x1

    return v0
.end method

.method public setVideoTempEx(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->colorParaEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v0, 0x1

    return v0
.end method

.method public syncDirtyDataOnResume()V
    .locals 8

    const/16 v7, 0x17

    const/16 v6, 0x16

    const/16 v5, 0x14

    const/16 v4, 0xe

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x22

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1a

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x20

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x22

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1a

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x20

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    :cond_1
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x25

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x25

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryColorTempData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    :cond_2
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x24

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x24

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryADCAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    :cond_3
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x2a

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x2a

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    :cond_4
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x27

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x27

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryExtern()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    :cond_5
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x28

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x28

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNoStandSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    :cond_6
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x29

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x29

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySSCAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    :cond_7
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x2b

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x2b

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    :cond_8
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x2c

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x2c

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    :cond_9
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1e

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1e

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserLocSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    :cond_a
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1c

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1c

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUsrColorTmpData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    :cond_b
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x19

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x19

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    :cond_c
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x18

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x18

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSubtitleSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    :cond_d
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1d

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x1d

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUsrColorTmpExData()[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    :cond_e
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundModeSettings()[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    :cond_f
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v7

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundModeSettings()[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    :cond_10
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x26

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x26

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    :cond_11
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x33

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/16 v1, 0x33

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySRSAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    :cond_12
    return-void
.end method

.method public updateADCAdjust(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16RedGain"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redgain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenGain"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greengain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueGain"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->bluegain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16RedOffset"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenOffset"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greenoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueOffset"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->blueoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "content://mstar.tv.factory/adcadjust/sourceid/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x24

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateAutoMHLSwitch(S)V
    .locals 7
    .param p1    # S

    const-string v3, "Charles"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "updateAutoMHLSwitch curStatue : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "bAutoMHLSwitch"

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :goto_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_SystemSetting ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v3

    const/16 v4, 0x19

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public updateChannelNameAndEventName(Landroid/content/ContentValues;I)V
    .locals 8
    .param p1    # Landroid/content/ContentValues;
    .param p2    # I

    const-wide/16 v1, -0x1

    :try_start_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "\n########### update \n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "u32StartTime = ? "

    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/epgtimer"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, p1, v4, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v1, v5

    :goto_0
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_0

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "\n########### update failed\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update block system password  failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateCurCountry(I)V
    .locals 7
    .param p1    # I

    const-wide/16 v0, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "Country"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v4, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-long v0, v3

    :goto_0
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_SystemSetting field Country ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public updateCurInputSrc(I)V
    .locals 8
    .param p1    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "enInputSourceType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/systemsetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateCustomerCfgMiscSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "EnergyEnable"

    iget-boolean v4, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyEnable:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "EnergyPercent"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyPercent:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/miscsetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public updateEventName(Ljava/lang/String;S)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # S

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\n====>>updateEventName--eventName "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " @epgTimerIndex "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "sEventName"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "content://mstar.tv.usersetting/epgtimer/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_EpgTimer field sEventName ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateFactoryColorTempData(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;
    .param p2    # I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "u8RedGain"

    iget-short v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8GreenGain"

    iget-short v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8BlueGain"

    iget-short v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8RedOffset"

    iget-short v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8GreenOffset"

    iget-short v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8BlueOffset"

    iget-short v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "content://mstar.tv.factory/factorycolortemp/colortemperatureid/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v1, v5

    :goto_0
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x25

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateFactoryColorTempExData(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;II)V
    .locals 9
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .param p2    # I
    .param p3    # I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "u16RedGain"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "u16GreenGain"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "u16BlueGain"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "u16RedOffset"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "u16GreenOffset"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "u16BlueOffset"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v5, " InputSourceID = ? and ColorTemperatureID = ? "

    const/4 v6, 0x2

    new-array v3, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/factorycolortempex"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7, v4, v5, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    int-to-long v1, v6

    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v1, v6

    if-nez v6, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v6, "DataBaseDeskImpl"

    const-string v7, "update failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v6

    const/16 v7, 0x26

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateFactoryExtern(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "SoftWareVersion"

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "BoardType"

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PanelType"

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "CompileTime"

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "TestPatternMode"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "stPowerMode"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "DtvAvAbnormalDelay"

    iget-boolean v4, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "FactoryPreSetFeature"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "PanelSwing"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "AudioPrescale"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "vdDspVersion"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "eHidevMode"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "audioNrThr"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "audioSifThreshold"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "audioDspVersion"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "bBurnIn"

    iget-boolean v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->bBurnIn:Z

    if-eqz v7, :cond_2

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "content://mstar.tv.factory/factoryextern"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_2
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_3

    :cond_0
    :goto_3
    return-void

    :cond_1
    move v4, v6

    goto/16 :goto_0

    :cond_2
    move v5, v6

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x27

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method public updateNonLinearAdjust(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;I)V
    .locals 9
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;
    .param p2    # I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "u8OSD_V0"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V0:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8OSD_V25"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V25:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8OSD_V50"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V50:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8OSD_V75"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V75:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8OSD_V100"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V100:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v5, " CurveTypeIndex = ? and InputSrcType = ? "

    const/4 v6, 0x2

    new-array v3, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/nonlinearadjust"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7, v4, v5, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    int-to-long v1, v6

    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v1, v6

    if-nez v6, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v6, "DataBaseDeskImpl"

    const-string v7, "update failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v6

    const/16 v7, 0x2a

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateNonStandardAdjust(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8AFEC_D4"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D4:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D5_Bit2"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D5_Bit2:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D8_Bit3210"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D8_Bit3210:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D9_Bit0"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D9_Bit0:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D7_LOW_BOUND"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_LOW_BOUND:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D7_HIGH_BOUND"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_HIGH_BOUND:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_A0"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A0:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_A1"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A1:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_66_Bit76"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_66_Bit76:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_6E_Bit7654"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit7654:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_6E_Bit3210"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit3210:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_43"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_43:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_44"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_44:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_CB"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_CB:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "content://mstar.tv.facory/nonstandardadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateNonStandardAdjust(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "VifTop"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifVgaMaximum"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "VifCrKp"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKi"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKp1"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKi1"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKp2"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKi2"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "VifAsiaSignalOption"

    iget-boolean v4, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "VifCrKpKiAdjust"

    iget-boolean v4, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    if-eqz v4, :cond_2

    move v4, v5

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "VifOverModulation"

    iget-boolean v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    if-eqz v7, :cond_3

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "VifClampgainGainOvNegative"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "ChinaDescramblerBox"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifDelayReduce"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrThr"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifVersion"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifACIAGCREF"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "GainDistributionThr"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "content://mstar.tv.facory/nonstandardadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_3
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_4

    :cond_0
    :goto_4
    return-void

    :cond_1
    move v4, v6

    goto/16 :goto_0

    :cond_2
    move v4, v6

    goto :goto_1

    :cond_3
    move v5, v6

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_4
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4
.end method

.method public updateOverscanAdjust(I[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 13
    .param p1    # I
    .param p2    # [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v5

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_1
    if-lt v2, v4, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v10

    const/16 v11, 0x2b

    invoke-virtual {v10, v11}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_2
    return-void

    :pswitch_0
    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v4

    goto :goto_0

    :pswitch_1
    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v4

    goto :goto_0

    :pswitch_2
    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v4

    goto :goto_0

    :pswitch_3
    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v4

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_3
    if-lt v3, v5, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v10, "u16H_CapStart"

    aget-object v11, p2, v2

    aget-object v11, v11, v3

    iget v11, v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "u16V_CapStart"

    aget-object v11, p2, v2

    aget-object v11, v11, v3

    iget v11, v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "u8HCrop_Left"

    aget-object v11, p2, v2

    aget-object v11, v11, v3

    iget-short v11, v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v11}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v10, "u8HCrop_Right"

    aget-object v11, p2, v2

    aget-object v11, v11, v3

    iget-short v11, v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v11}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v10, "u8VCrop_Up"

    aget-object v11, p2, v2

    aget-object v11, v11, v3

    iget-short v11, v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v11}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v10, "u8VCrop_Down"

    aget-object v11, p2, v2

    aget-object v11, v11, v3

    iget-short v11, v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v11}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    mul-int v10, v2, v5

    add-int v0, v10, v3

    const-wide/16 v6, -0x1

    :try_start_1
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "content://mstar.tv.factory/overscanadjust/factoryoverscantype/"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/_id/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v8, v9, v11, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v10

    int-to-long v6, v10

    :goto_4
    const-wide/16 v10, -0x1

    cmp-long v10, v6, v10

    if-eqz v10, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :catch_0
    move-exception v1

    const-string v10, "DataBaseDeskImpl"

    const-string v11, "update failed"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public updatePEQAdjust(Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;
    .param p2    # I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "Band"

    iget v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Band:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "Gain"

    iget v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "Foh"

    iget v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "Fol"

    iget v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "QValue"

    iget v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "content://mstar.tv.factory/peqadjust/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v1, v5

    :goto_0
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updatePicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;III)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM()[I

    move-result-object v4

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :goto_0
    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/picmode_setting/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picmode/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    :cond_0
    return-void

    :pswitch_0
    const-string v4, "u8Brightness"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_1
    const-string v4, "u8Contrast"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_2
    const-string v4, "u8Hue"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_3
    const-string v4, "u8Saturation"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_4
    const-string v4, "u8Sharpness"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_5
    const-string v4, "u8Backlight"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public updateSSCAdjust(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "Miu_SscEnable"

    iget-boolean v4, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Lvds_SscEnable"

    iget-boolean v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    if-eqz v7, :cond_2

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Lvds_SscSpan"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Lvds_SscStep"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu_SscSpan"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu_SscStep"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu1_SscSpan"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu1_SscStep"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu2_SscSpan"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu2_SscStep"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "content://mstar.tv.factory/sscadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_2
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_3

    :cond_0
    :goto_3
    return-void

    :cond_1
    move v4, v6

    goto/16 :goto_0

    :cond_2
    move v5, v6

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x29

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method public updateServiceName(Ljava/lang/String;S)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # S

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\n====>>updateServiceName--serviceName "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " @epgTimerIndex "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "sServiceName"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "content://mstar.tv.usersetting/epgtimer/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_EpgTimer field sServiceName ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "Bass"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "Treble"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand1"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand2"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand3"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand4"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand5"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand6"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand6:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand7"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand7:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "UserMode"

    iget-boolean v4, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->UserMode:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Balance"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Balance:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "enSoundAudioChannel"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->enSoundAudioChannel:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/soundmodesetting/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_2

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x16

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method public updateSoundSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)V
    .locals 10
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "SoundMode"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "AudysseyDynamicVolume"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyDynamicVolume:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "AudysseyEQ"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyEQ:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "SurroundSoundMode"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundSoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "Surround"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "bEnableAVC"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    if-eqz v6, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "Volume"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "HPVolume"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->HPVolume:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "Balance"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "Primary_Flag"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Primary_Flag:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_4

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_2
    const-string v6, "enSoundAudioLan1"

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "enSoundAudioLan2"

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "MUTE_Flag"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->MUTE_Flag:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "enSoundAudioChannel"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioChannel:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "bEnableAD"

    iget-boolean v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAD:Z

    if-eqz v9, :cond_6

    :goto_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "ADVolume"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADVolume:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "ADOutput"

    iget-object v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADOutput:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "SPDIF_Delay"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SPDIF_Delay:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "Speaker_Delay"

    iget-short v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Speaker_Delay:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "hdmi1AudioSource"

    iget-object v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi1AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "hdmi2AudioSource"

    iget-object v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi2AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "hdmi3AudioSource"

    iget-object v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi3AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "hdmi4AudioSource"

    iget-object v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi4AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v3, -0x1

    :try_start_0
    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/soundsetting"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v5, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    int-to-long v3, v6

    :goto_4
    const-wide/16 v6, -0x1

    cmp-long v6, v3, v6

    if-nez v6, :cond_7

    :cond_0
    :goto_5
    return-void

    :cond_1
    move v6, v8

    goto/16 :goto_0

    :cond_2
    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_4
    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_5
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_6
    move v7, v8

    goto/16 :goto_3

    :catch_0
    move-exception v0

    const-string v6, "DataBaseDeskImpl"

    const-string v7, "update failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_7
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v6

    const/16 v7, 0x17

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5
.end method

.method public updateSourceIdent(S)V
    .locals 7
    .param p1    # S

    const/4 v1, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "bSourceDetectEnable"

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :goto_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_SystemSetting ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v3

    const/16 v4, 0x19

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public updateSourceSwit(S)V
    .locals 7
    .param p1    # S

    const/4 v1, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "bAutoSourceSwitch"

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :goto_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_SystemSetting ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v3

    const/16 v4, 0x19

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public updateUserLocSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16LocationNo"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mLocationNo:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "s16ManualLongitude"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLongitude:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "s16ManualLatitude"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLatitude:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/userlocationsetting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1e

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateUserSubtitleSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;)V
    .locals 10
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_0
    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_3

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    const-string v6, "SubtitleDefaultLanguage"

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "SubtitleDefaultLanguage_2"

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "fHardOfHearing"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fHardOfHearing:Z

    if-eqz v6, :cond_5

    move v6, v7

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "fEnableSubTitle"

    iget-boolean v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fEnableSubTitle:Z

    if-eqz v9, :cond_6

    :goto_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v3, -0x1

    :try_start_0
    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/subtitlesetting"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v5, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    int-to-long v3, v6

    :goto_4
    const-wide/16 v6, -0x1

    cmp-long v6, v3, v6

    if-nez v6, :cond_7

    :cond_0
    :goto_5
    return-void

    :cond_1
    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_3
    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_4

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_1

    :cond_5
    move v6, v8

    goto :goto_2

    :cond_6
    move v7, v8

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v6, "DataBaseDeskImpl"

    const-string v7, "update failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_7
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v6

    const/16 v7, 0x18

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5
.end method

.method public updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V
    .locals 11
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string v0, "CN"

    const-string v6, "/system/build.prop"

    const-string v9, "persist.sys.country"

    invoke-direct {p0, v6, v9}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "fRunInstallationGuide"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    if-eqz v6, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "fNoChannel"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    if-eqz v6, :cond_2

    move v6, v7

    :goto_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "bDisableSiAutoUpdate"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    if-eqz v6, :cond_3

    move v6, v7

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "enInputSourceType"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "Country"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "enCableOperators"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "enSatellitePlatform"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "CN"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_4

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_3
    const-string v6, "Language"

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "enSPDIFMODE"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSPDIFMODE:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "fSoftwareUpdate"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fSoftwareUpdate:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "U8OADTime"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "fOADScanAfterWakeup"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "fAutoVolume"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "fDcPowerOFFMode"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "DtvRoute"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "ScartOutRGB"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "U8Transparency"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u32MenuTimeOut"

    iget-wide v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "AudioOnly"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "bEnableWDT"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8FavoriteRegion"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8Bandwidth"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8TimeShiftSizeType"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "fOadScan"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "bEnablePVRRecordAll"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8ColorRangeMode"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "u8HDMIAudioSource"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "bEnableAlwaysTimeshift"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "eSUPER"

    iget-object v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "bUartBus"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    if-eqz v6, :cond_7

    move v6, v7

    :goto_4
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "bTeletext"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bTeletext:Z

    if-eqz v6, :cond_8

    move v6, v7

    :goto_5
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "m_AutoZoom"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v9, "bOverScan"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    if-eqz v6, :cond_9

    move v6, v7

    :goto_6
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "m_u8BrazilVideoStandardType"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "m_u8SoftwareUpdateMode"

    iget-short v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "OSD_Active_Time"

    iget-wide v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "m_MessageBoxExist"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    if-eqz v6, :cond_a

    move v6, v7

    :goto_7
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "u16LastOADVersion"

    iget v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "bEnableAutoChannelUpdate"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    if-eqz v6, :cond_b

    move v6, v7

    :goto_8
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "standbyNoOperation"

    iget v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoOperation:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "standbyNoSignal"

    iget-boolean v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoSignal:Z

    if-eqz v6, :cond_c

    move v6, v7

    :goto_9
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "screenSaveMode"

    iget-boolean v9, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->screenSaveMode:Z

    if-eqz v9, :cond_d

    :goto_a
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "U8SystemAutoTimeType"

    iget v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8SystemAutoTimeType:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "smartEnergySaving"

    iget-object v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "colorWheelMode"

    iget-object v7, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v3, -0x1

    :try_start_0
    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/systemsetting"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v5, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    int-to-long v3, v6

    :goto_b
    const-wide/16 v6, -0x1

    cmp-long v6, v3, v6

    if-nez v6, :cond_e

    :cond_0
    :goto_c
    return-void

    :cond_1
    move v6, v8

    goto/16 :goto_0

    :cond_2
    move v6, v8

    goto/16 :goto_1

    :cond_3
    move v6, v8

    goto/16 :goto_2

    :cond_4
    iget-object v6, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_5
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_6
    iget-object v2, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_7
    move v6, v8

    goto/16 :goto_4

    :cond_8
    move v6, v8

    goto/16 :goto_5

    :cond_9
    move v6, v8

    goto/16 :goto_6

    :cond_a
    move v6, v8

    goto/16 :goto_7

    :cond_b
    move v6, v8

    goto/16 :goto_8

    :cond_c
    move v6, v8

    goto/16 :goto_9

    :cond_d
    move v7, v8

    goto/16 :goto_a

    :catch_0
    move-exception v1

    const-string v6, "DataBaseDeskImpl"

    const-string v7, "update failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    :cond_e
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v6

    const/16 v7, 0x19

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_c

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_c
.end method

.method public updateUsrColorTmpData(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8RedGain"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8GreenGain"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8BlueGain"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8RedOffset"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8GreenOffset"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8BlueOffset"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/usercolortemp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1c

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateUsrColorTmpExData(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16RedGain"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenGain"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueGain"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16RedOffset"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenOffset"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueOffset"

    iget v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/usercolortempex/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1d

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateVideo3DAdaptiveDetectMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "eThreeDVideoSelfAdaptiveDetect"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/threedvideomode/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1a

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateVideo3DDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "eThreeDVideoDisplayFormat"

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/threedvideomode/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1a

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "eThreeDVideo"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoSelfAdaptiveDetect"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoDisplayFormat"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DTo2D"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DDepth"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DOffset"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoAutoStart"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DOutputAspect"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoLRViewSwitch"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/threedvideomode/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1a

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateVideoAstPicture(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;II)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;
    .param p2    # I
    .param p3    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8Backlight"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Contrast"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Brightness"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Saturation"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Sharpness"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Hue"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "eColorTemp"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eVibrantColour"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eVibrantColour:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "ePerfectClear"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->ePerfectClear:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eDynamicContrast"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicContrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eDynamicBacklight"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicBacklight:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/picmode_setting/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picmode/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateVideoAstSubColor(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8SubBrightness"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubBrightness:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8SubContrast"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubContrast:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/videosetting/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "ePicture"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "enARCType"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "fOutput_RES"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "tvsys"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "LastVideoStandardMode"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "LastAudioStandardMode"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eDynamic_Contrast"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eFilm"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eTvFormat"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "skinTone"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->skinTone:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "detailEnhance"

    iget-boolean v4, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->detailEnhance:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "DNR"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->DNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/videosetting/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_2

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method public updateVideoNRMode(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;II)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;
    .param p2    # I
    .param p3    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "eNR"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eMPEG_NR"

    iget-object v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/nrmode/nrmode/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateVideoUserOverScanMode(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;I)V
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;
    .param p2    # I

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "OverScanHposition"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHposition:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "OverScanVposition"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVposition:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "OverScanHRatio"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHRatio:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "OverScanVRatio"

    iget-short v5, p1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVRatio:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/useroverscanmode/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "DataBaseDeskImpl"

    const-string v5, "update failed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
