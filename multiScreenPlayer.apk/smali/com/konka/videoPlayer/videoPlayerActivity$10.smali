.class Lcom/konka/videoPlayer/videoPlayerActivity$10;
.super Ljava/lang/Object;
.source "videoPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;->initVideoPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 8
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput-boolean v7, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSend:Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput v7, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    const-string v0, "weikan"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoView is completion once!!! nextFlag = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$32(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$32(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_STOP:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&mv"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$11(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$52(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    :goto_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$49(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "media player is completed-------2"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$32(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$12(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$24(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    const-string v0, "weikan"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoView is completion once!!! nextFlag = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$12(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$13(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$12(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I
    invoke-static {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$13(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v1

    if-ge v0, v1, :cond_1

    const-string v0, "weikan"

    const-string v1, "VideoView is begin next one!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$35(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$36(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&mv"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$11(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v7, v7, v7}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$49(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "have no next file!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&mv"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$11(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$10;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v7, v7, v7}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0
.end method
