.class public Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "videoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "closeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method public constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    const-string v0, "closePlayer"

    invoke-virtual {v10, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v1, "mediastop"

    invoke-virtual {v10, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v1, "resumePos"

    invoke-virtual {v10, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->resumeAsk:I

    const-string v0, "media stop"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-closeReceiver-------------video--closePlayer: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "media stop"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-closeReceiver-------------video--mediastop: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v2, v2, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "media resume"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-resumeAsk-------------video--resumeAsk: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v2, v2, Lcom/konka/videoPlayer/videoPlayerActivity;->resumeAsk:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v11, v6, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&mv"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->cancelProgressDlg()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerStop()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$9(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_STOP:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&mv"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerStop()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$9(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    :cond_1
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->resumeAsk:I

    if-ne v0, v6, :cond_2

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v5, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_RESUME:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v6

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v7

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v8

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerStop()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$9(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    :cond_2
    return-void
.end method
