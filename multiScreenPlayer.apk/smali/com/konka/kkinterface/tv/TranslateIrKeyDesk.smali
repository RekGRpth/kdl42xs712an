.class public Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;
.super Ljava/lang/Object;
.source "TranslateIrKeyDesk.java"


# static fields
.field static keyinstance:Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;


# instance fields
.field final mstar_ir_keymap:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->keyinstance:Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [[I

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/16 v2, 0xff

    aput v2, v1, v3

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->mstar_ir_keymap:[[I

    return-void
.end method

.method public static getInstance()Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;
    .locals 1

    sget-object v0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->keyinstance:Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    invoke-direct {v0}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;-><init>()V

    sput-object v0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->keyinstance:Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    :cond_0
    sget-object v0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->keyinstance:Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    return-object v0
.end method


# virtual methods
.method public translateIRKey(I)I
    .locals 5
    .param p1    # I

    move v2, p1

    iget-object v3, p0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->mstar_ir_keymap:[[I

    array-length v1, v3

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    :goto_1
    return v2

    :cond_0
    iget-object v3, p0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->mstar_ir_keymap:[[I

    aget-object v3, v3, v0

    const/4 v4, 0x0

    aget v3, v3, v4

    if-ne p1, v3, :cond_1

    iget-object v3, p0, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->mstar_ir_keymap:[[I

    aget-object v3, v3, v0

    const/4 v4, 0x1

    aget v2, v3, v4

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
