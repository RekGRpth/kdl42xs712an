.class public final Lcom/konka/mediaSharePlayer/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mediaSharePlayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final app_icon_file:I = 0x7f020000

.field public static final app_icon_image:I = 0x7f020001

.field public static final app_icon_music:I = 0x7f020002

.field public static final app_icon_share:I = 0x7f020003

.field public static final back:I = 0x7f020004

.field public static final bigger:I = 0x7f020005

.field public static final changshow:I = 0x7f020006

.field public static final com_bg:I = 0x7f020007

.field public static final com_bg1:I = 0x7f020008

.field public static final com_bg_bottom:I = 0x7f020009

.field public static final com_bg_bottom_list:I = 0x7f02000a

.field public static final com_bg_top:I = 0x7f02000b

.field public static final com_button_arrange_s:I = 0x7f02000c

.field public static final com_button_arrange_uns:I = 0x7f02000d

.field public static final com_button_select_s1:I = 0x7f02000e

.field public static final com_button_select_s2:I = 0x7f02000f

.field public static final com_button_select_uns1:I = 0x7f020010

.field public static final com_button_select_uns2:I = 0x7f020011

.field public static final com_button_sortord_s:I = 0x7f020012

.field public static final com_button_sortord_uns:I = 0x7f020013

.field public static final com_default_nextpage_images:I = 0x7f020014

.field public static final com_pause_sel:I = 0x7f020015

.field public static final com_pause_uns:I = 0x7f020016

.field public static final com_play_next_s:I = 0x7f020017

.field public static final com_play_next_uns:I = 0x7f020018

.field public static final com_play_pause_s:I = 0x7f020019

.field public static final com_play_pause_uns:I = 0x7f02001a

.field public static final com_play_prev_s:I = 0x7f02001b

.field public static final com_play_prev_sel:I = 0x7f02001c

.field public static final com_play_prev_uns:I = 0x7f02001d

.field public static final com_play_sel:I = 0x7f02001e

.field public static final com_play_stop_s:I = 0x7f02001f

.field public static final com_play_stop_uns:I = 0x7f020020

.field public static final com_play_uns:I = 0x7f020021

.field public static final com_play_voice2_s:I = 0x7f020022

.field public static final com_play_voice2_uns:I = 0x7f020023

.field public static final com_play_voice_s:I = 0x7f020024

.field public static final com_play_voice_s1:I = 0x7f020025

.field public static final com_play_voice_s2:I = 0x7f020026

.field public static final com_play_voice_uns:I = 0x7f020027

.field public static final com_play_voice_uns1:I = 0x7f020028

.field public static final com_play_voice_uns2:I = 0x7f020029

.field public static final com_popmenu_bg_delete:I = 0x7f02002a

.field public static final com_progressbar_arrived_button_s:I = 0x7f02002b

.field public static final com_progressbar_arrived_button_uns:I = 0x7f02002c

.field public static final com_progressbar_arrived_s2:I = 0x7f02002d

.field public static final com_progressbar_default2:I = 0x7f02002e

.field public static final com_progressbar_default_all:I = 0x7f02002f

.field public static final com_select_rim:I = 0x7f020030

.field public static final com_separator1:I = 0x7f020031

.field public static final com_separator2:I = 0x7f020032

.field public static final com_separator3:I = 0x7f020033

.field public static final com_set_s:I = 0x7f020034

.field public static final com_set_uns:I = 0x7f020035

.field public static final com_touming_bg:I = 0x7f020036

.field public static final cycle_selector:I = 0x7f020037

.field public static final end_focus_btn:I = 0x7f020038

.field public static final end_nofoucs_btn:I = 0x7f020039

.field public static final file_batton_confirm_s:I = 0x7f02003a

.field public static final file_batton_confirm_uns:I = 0x7f02003b

.field public static final file_button_sortord_uns:I = 0x7f02003c

.field public static final file_icon_delete_s:I = 0x7f02003d

.field public static final file_icon_delete_uns:I = 0x7f02003e

.field public static final file_icon_deletemark_s:I = 0x7f02003f

.field public static final file_manager_selector:I = 0x7f020040

.field public static final file_popmenu_bg1:I = 0x7f020041

.field public static final file_popmenu_bg2:I = 0x7f020042

.field public static final file_popmenu_button_confirm_s:I = 0x7f020043

.field public static final file_popmenu_button_confirm_uns:I = 0x7f020044

.field public static final file_select_button:I = 0x7f020045

.field public static final file_select_rim:I = 0x7f020046

.field public static final file_tooltip:I = 0x7f020047

.field public static final file_tooltip1:I = 0x7f020048

.field public static final file_unselect_button:I = 0x7f020049

.field public static final filetord_s:I = 0x7f02004a

.field public static final first_page:I = 0x7f02004b

.field public static final frame_gallery_thumb:I = 0x7f02004c

.field public static final fsm_host_bg_selector:I = 0x7f02004d

.field public static final fullcream:I = 0x7f02004e

.field public static final gallery_pic_select:I = 0x7f02004f

.field public static final gallery_selector:I = 0x7f020050

.field public static final greenbutton_greenbutton_empty:I = 0x7f020051

.field public static final gridview_image_selector:I = 0x7f020052

.field public static final gridview_music_selector:I = 0x7f020053

.field public static final ic_launcher:I = 0x7f020054

.field public static final icon:I = 0x7f020055

.field public static final infos:I = 0x7f020056

.field public static final item_left_arrow_state:I = 0x7f020057

.field public static final item_right_arrow_state:I = 0x7f020058

.field public static final item_selector:I = 0x7f020059

.field public static final last_page:I = 0x7f02005a

.field public static final leftrotation:I = 0x7f02005b

.field public static final login:I = 0x7f02005c

.field public static final login_:I = 0x7f02005d

.field public static final lrc_selector:I = 0x7f02005e

.field public static final media_default_file_uns:I = 0x7f02005f

.field public static final media_default_images_uns:I = 0x7f020060

.field public static final media_default_music_uns:I = 0x7f020061

.field public static final media_default_video_uns:I = 0x7f020062

.field public static final media_disk_s:I = 0x7f020063

.field public static final media_disk_s_03:I = 0x7f020064

.field public static final media_disk_uns:I = 0x7f020065

.field public static final media_disk_uns_06:I = 0x7f020066

.field public static final menu_btn_selector:I = 0x7f020067

.field public static final menu_btn_textcolor_selector:I = 0x7f020068

.field public static final minstars:I = 0x7f020069

.field public static final more_bg:I = 0x7f02006a

.field public static final music_bg:I = 0x7f02006b

.field public static final music_icon_big:I = 0x7f02006c

.field public static final music_icon_cycle_s:I = 0x7f02006d

.field public static final music_icon_cycle_uns:I = 0x7f02006e

.field public static final music_icon_listbox_s:I = 0x7f02006f

.field public static final music_icon_lyric_s:I = 0x7f020070

.field public static final music_icon_lyric_uns:I = 0x7f020071

.field public static final music_icon_shuffle_s:I = 0x7f020072

.field public static final music_icon_shuffle_uns:I = 0x7f020073

.field public static final music_icon_single_cycle_s:I = 0x7f020074

.field public static final music_icon_single_cycle_uns:I = 0x7f020075

.field public static final music_icon_small:I = 0x7f020076

.field public static final music_icon_uncycle_s:I = 0x7f020077

.field public static final music_icon_uncycle_uns:I = 0x7f020078

.field public static final music_item_bg:I = 0x7f020079

.field public static final music_list:I = 0x7f02007a

.field public static final music_list_selector:I = 0x7f02007b

.field public static final music_listbox_bg:I = 0x7f02007c

.field public static final music_lrc:I = 0x7f02007d

.field public static final music_lyric_s:I = 0x7f02007e

.field public static final music_lyric_uns:I = 0x7f02007f

.field public static final music_main_bg:I = 0x7f020080

.field public static final music_next_s:I = 0x7f020081

.field public static final music_next_uns:I = 0x7f020082

.field public static final music_pause_s:I = 0x7f020083

.field public static final music_pause_uns:I = 0x7f020084

.field public static final music_play_s:I = 0x7f020085

.field public static final music_play_uns:I = 0x7f020086

.field public static final music_prev_s:I = 0x7f020087

.field public static final music_prev_uns:I = 0x7f020088

.field public static final musicplayerbg:I = 0x7f020089

.field public static final musicplayericon:I = 0x7f02008a

.field public static final musics_selector:I = 0x7f02008b

.field public static final next:I = 0x7f02008c

.field public static final next_focus_btn:I = 0x7f02008d

.field public static final next_nof_btn:I = 0x7f02008e

.field public static final next_selector:I = 0x7f02008f

.field public static final path_text_selector:I = 0x7f020090

.field public static final pause_selector:I = 0x7f020091

.field public static final pic_icon_anticlockwise_s:I = 0x7f020092

.field public static final pic_icon_anticlockwise_uns:I = 0x7f020093

.field public static final pic_icon_clockwise_s:I = 0x7f020094

.field public static final pic_icon_clockwise_uns:I = 0x7f020095

.field public static final pic_icon_default_list:I = 0x7f020096

.field public static final pic_icon_descale12_s:I = 0x7f020097

.field public static final pic_icon_descale2_s:I = 0x7f020098

.field public static final pic_icon_descale4_s:I = 0x7f020099

.field public static final pic_icon_descale8_s:I = 0x7f02009a

.field public static final pic_icon_descale_s:I = 0x7f02009b

.field public static final pic_icon_descale_uns:I = 0x7f02009c

.field public static final pic_icon_enlarge12_s:I = 0x7f02009d

.field public static final pic_icon_enlarge2_s:I = 0x7f02009e

.field public static final pic_icon_enlarge4_s:I = 0x7f02009f

.field public static final pic_icon_enlarge8_s:I = 0x7f0200a0

.field public static final pic_icon_enlarge_s:I = 0x7f0200a1

.field public static final pic_icon_enlarge_uns:I = 0x7f0200a2

.field public static final pic_icon_fullscreen_s:I = 0x7f0200a3

.field public static final pic_icon_fullscreen_uns:I = 0x7f0200a4

.field public static final pic_icon_list_select_rim:I = 0x7f0200a5

.field public static final pic_setting_state:I = 0x7f0200a6

.field public static final pkg:I = 0x7f0200a7

.field public static final play:I = 0x7f0200a8

.field public static final play_selector:I = 0x7f0200a9

.field public static final playstate1:I = 0x7f0200aa

.field public static final playstate2:I = 0x7f0200ab

.field public static final playstate3:I = 0x7f0200ac

.field public static final playstate4:I = 0x7f0200ad

.field public static final pre_f_btn:I = 0x7f0200ae

.field public static final pre_nof_btn:I = 0x7f0200af

.field public static final pre_selector:I = 0x7f0200b0

.field public static final radio_btn_background_selector:I = 0x7f0200b1

.field public static final radio_btn_selector:I = 0x7f0200b2

.field public static final radio_item:I = 0x7f0200b3

.field public static final redbutton_delete:I = 0x7f0200b4

.field public static final rightrotation:I = 0x7f0200b5

.field public static final run_focus_btn:I = 0x7f0200b6

.field public static final run_nof_btn:I = 0x7f0200b7

.field public static final second_page:I = 0x7f0200b8

.field public static final seekbar_n:I = 0x7f0200b9

.field public static final seekbar_p:I = 0x7f0200ba

.field public static final seekbar_style:I = 0x7f0200bb

.field public static final serviceicon:I = 0x7f0200bc

.field public static final setting:I = 0x7f0200bd

.field public static final share_bg:I = 0x7f0200be

.field public static final share_choose_btn:I = 0x7f0200bf

.field public static final share_icon_button_search_s:I = 0x7f0200c0

.field public static final share_icon_button_search_uns:I = 0x7f0200c1

.field public static final share_icon_s:I = 0x7f0200c2

.field public static final share_icon_search_s:I = 0x7f0200c3

.field public static final share_icon_search_uns:I = 0x7f0200c4

.field public static final share_icon_uns:I = 0x7f0200c5

.field public static final shuffle_selector:I = 0x7f0200c6

.field public static final single_cycle:I = 0x7f0200c7

.field public static final smaller:I = 0x7f0200c8

.field public static final sort_btn:I = 0x7f0200c9

.field public static final sort_selector:I = 0x7f0200ca

.field public static final sound:I = 0x7f0200cb

.field public static final sound_dec:I = 0x7f0200cc

.field public static final sound_inc:I = 0x7f0200cd

.field public static final stop:I = 0x7f0200ce

.field public static final text_color_samba_selector:I = 0x7f0200cf

.field public static final text_color_selector:I = 0x7f0200d0

.field public static final text_selector:I = 0x7f0200d1

.field public static final thumb:I = 0x7f0200d2

.field public static final uncycle_selector:I = 0x7f0200d3

.field public static final update:I = 0x7f0200d4

.field public static final update_s:I = 0x7f0200d5

.field public static final update_uns:I = 0x7f0200d6

.field public static final video_icon_default_list:I = 0x7f0200d7

.field public static final video_icon_list_select_rim:I = 0x7f0200d8

.field public static final video_icon_set_bg:I = 0x7f0200d9

.field public static final video_icon_set_model_cycle_s:I = 0x7f0200da

.field public static final video_icon_set_model_image_s:I = 0x7f0200db

.field public static final video_icon_set_model_scale_s:I = 0x7f0200dc

.field public static final video_icon_set_model_subtitles_s:I = 0x7f0200dd

.field public static final video_icon_set_model_track_s:I = 0x7f0200de

.field public static final video_icon_set_select_bar:I = 0x7f0200df

.field public static final video_icon_set_select_button_s:I = 0x7f0200e0

.field public static final video_icon_set_select_button_uns:I = 0x7f0200e1

.field public static final video_seekbar_style:I = 0x7f0200e2

.field public static final voice_down_selector:I = 0x7f0200e3

.field public static final voice_no_selector:I = 0x7f0200e4

.field public static final voice_selector:I = 0x7f0200e5

.field public static final voice_up_selector:I = 0x7f0200e6


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
