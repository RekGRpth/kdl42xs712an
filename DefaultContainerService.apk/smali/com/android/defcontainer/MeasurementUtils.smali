.class public Lcom/android/defcontainer/MeasurementUtils;
.super Ljava/lang/Object;
.source "MeasurementUtils.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "defcontainer_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static measureDirectory(Ljava/lang/String;)J
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/android/defcontainer/MeasurementUtils;->native_measureDirectory(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static native native_measureDirectory(Ljava/lang/String;)J
.end method
