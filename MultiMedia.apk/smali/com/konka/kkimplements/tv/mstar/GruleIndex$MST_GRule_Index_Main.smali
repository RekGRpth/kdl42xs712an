.class public final enum Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;
.super Ljava/lang/Enum;
.source "GruleIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkimplements/tv/mstar/GruleIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MST_GRule_Index_Main"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_COLOR_LEVEL_STRTECH_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_COLOR_ROLL_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_DETAILS_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_DYNAMIC_CONTRAST_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_FILM_MODE_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_MPEG_NR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_NR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_NR_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_OSD_BW_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_PTP_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_SKIN_TONE_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

.field public static final enum PQ_GRule_ULTRAT_CLEAR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_NR_Main"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_NR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_OSD_BW_Main"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_OSD_BW_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_MPEG_NR_Main"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_MPEG_NR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_FILM_MODE_Main"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_FILM_MODE_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_DYNAMIC_CONTRAST_Main"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_DYNAMIC_CONTRAST_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_ULTRAT_CLEAR_Main"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_ULTRAT_CLEAR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_PTP_Main"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_PTP_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_NR_Main_Color"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_NR_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_COLOR_ROLL_Main_Color"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_COLOR_ROLL_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_DETAILS_Main_Color"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_DETAILS_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_SKIN_TONE_Main_Color"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_SKIN_TONE_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const-string v1, "PQ_GRule_COLOR_LEVEL_STRTECH_Main_Color"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_COLOR_LEVEL_STRTECH_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_NR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_OSD_BW_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_MPEG_NR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_FILM_MODE_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_DYNAMIC_CONTRAST_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_ULTRAT_CLEAR_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_PTP_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_NR_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_COLOR_ROLL_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_DETAILS_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_SKIN_TONE_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_COLOR_LEVEL_STRTECH_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;
    .locals 1

    const-class v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
