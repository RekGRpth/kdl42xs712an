.class public final enum Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;
.super Ljava/lang/Enum;
.source "DeskCiEventListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

.field public static final enum EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

.field public static final enum EV_CIMMI_UI_CARD_INSERTED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

.field public static final enum EV_CIMMI_UI_CARD_REMOVED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

.field public static final enum EV_CIMMI_UI_CLOSEMMI:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

.field public static final enum EV_CIMMI_UI_DATA_READY:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    const-string v1, "EV_CIMMI_UI_DATA_READY"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_DATA_READY:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    const-string v1, "EV_CIMMI_UI_CLOSEMMI"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CLOSEMMI:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    const-string v1, "EV_CIMMI_UI_CARD_INSERTED"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_INSERTED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    const-string v1, "EV_CIMMI_UI_CARD_REMOVED"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_REMOVED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    const-string v1, "EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_DATA_READY:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CLOSEMMI:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_INSERTED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_REMOVED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;
    .locals 1

    const-class v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
