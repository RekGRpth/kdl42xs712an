.class public Lcom/konka/mm/FamilyShareHostActivity;
.super Landroid/app/Activity;
.source "FamilyShareHostActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;
    }
.end annotation


# instance fields
.field HostAdapter:Lcom/konka/mm/adapters/FamilyShareHostListAdapter;

.field private HostItem1:Landroid/widget/ImageButton;

.field private HostItem2:Landroid/widget/ImageButton;

.field private HostItem3:Landroid/widget/ImageButton;

.field private HostItem4:Landroid/widget/ImageButton;

.field private HostItem5:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem1:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem2:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem3:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem4:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem5:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f0b0006    # com.konka.mm.R.id.FSMHostItem1

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareHostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem1:Landroid/widget/ImageButton;

    const v0, 0x7f0b0008    # com.konka.mm.R.id.FSMHostItem2

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareHostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem2:Landroid/widget/ImageButton;

    const v0, 0x7f0b000a    # com.konka.mm.R.id.FSMHostItem3

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareHostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem3:Landroid/widget/ImageButton;

    const v0, 0x7f0b000c    # com.konka.mm.R.id.FSMHostItem4

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareHostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem4:Landroid/widget/ImageButton;

    const v0, 0x7f0b000e    # com.konka.mm.R.id.FSMHostItem5

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareHostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostItem5:Landroid/widget/ImageButton;

    return-void
.end method

.method private setListeners()V
    .locals 0

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030002    # com.konka.mm.R.layout.family_share_hosts

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareHostActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/mm/FamilyShareHostActivity;->findViews()V

    invoke-direct {p0}, Lcom/konka/mm/FamilyShareHostActivity;->setListeners()V

    new-instance v0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;

    invoke-static {}, Lcom/konka/mm/model/FamilyShareModel;->getHosts()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity;->HostAdapter:Lcom/konka/mm/adapters/FamilyShareHostListAdapter;

    return-void
.end method
