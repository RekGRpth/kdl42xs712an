.class public Lcom/konka/mm/services/CopyFileService;
.super Landroid/app/Service;
.source "CopyFileService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/services/CopyFileService$CopyFileBinder;,
        Lcom/konka/mm/services/CopyFileService$CopyThread;
    }
.end annotation


# static fields
.field private static final BUFF_SIZE:I = 0x2000

.field private static final SUCCESS:I = 0x3e7

.field private static final TAG:Ljava/lang/String; = "FileDialog"


# instance fields
.field public allDoSame:Z

.field private cBinder:Lcom/konka/mm/services/CopyFileService$CopyFileBinder;

.field private checkFile:Z

.field private context:Landroid/content/Context;

.field private copyLength:J

.field copyThread:Ljava/lang/Thread;

.field copyedLength:J

.field private deleAfterCopy:Z

.field private destroyAfterCopy:Z

.field private destroyed:Z

.field dialog:Landroid/app/ProgressDialog;

.field doIsFile:Z

.field fIn:Ljava/io/FileInputStream;

.field fOut:Ljava/io/FileOutputStream;

.field fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

.field private from:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field in:Ljava/io/BufferedInputStream;

.field isCancel:Z

.field public isCancle:Z

.field public isCopying:Z

.field public isCut:Z

.field public isHidden:Z

.field largeFile:Z

.field linux:Ljava/lang/Runtime;

.field moveProcess:Ljava/lang/Process;

.field out:Ljava/io/BufferedOutputStream;

.field public pasteToPath:Ljava/lang/String;

.field private perSize:I

.field public root:Z

.field private rootEBR:Ljava/io/BufferedReader;

.field private rootOS:Ljava/io/DataOutputStream;

.field private rootProcess:Ljava/lang/Process;

.field public selection:I

.field private tmpCutFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private toFile:Ljava/lang/String;

.field private toParentPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->from:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->toParentPath:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->destroyAfterCopy:Z

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->isCopying:Z

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->isCancle:Z

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->deleAfterCopy:Z

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->root:Z

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/mm/services/CopyFileService;->pasteToPath:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/services/CopyFileService;->selection:I

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->allDoSame:Z

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->isHidden:Z

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->largeFile:Z

    iput v2, p0, Lcom/konka/mm/services/CopyFileService;->perSize:I

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->checkFile:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->in:Ljava/io/BufferedInputStream;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->out:Ljava/io/BufferedOutputStream;

    iput-wide v3, p0, Lcom/konka/mm/services/CopyFileService;->copyLength:J

    iput-wide v3, p0, Lcom/konka/mm/services/CopyFileService;->copyedLength:J

    iput-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->isCancel:Z

    new-instance v0, Lcom/konka/mm/services/CopyFileService$CopyFileBinder;

    invoke-direct {v0, p0}, Lcom/konka/mm/services/CopyFileService$CopyFileBinder;-><init>(Lcom/konka/mm/services/CopyFileService;)V

    iput-object v0, p0, Lcom/konka/mm/services/CopyFileService;->cBinder:Lcom/konka/mm/services/CopyFileService$CopyFileBinder;

    return-void
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)I
    .locals 15
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    const/16 v9, 0x3e7

    :try_start_0
    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->root:Z

    if-nez v11, :cond_9

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_9

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->checkFile:Z

    if-eqz v11, :cond_9

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/konka/mm/services/CopyFileService;->multFile(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    const/4 v11, 0x2

    if-ne v9, v11, :cond_3

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v11, :cond_0

    move v11, v9

    :goto_0
    return v11

    :cond_0
    :try_start_1
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_1
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    move v11, v9

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    const/4 v11, 0x2

    goto :goto_0

    :cond_3
    const/4 v11, 0x3

    if-ne v9, v11, :cond_8

    :cond_4
    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v11, :cond_5

    move v11, v9

    goto :goto_0

    :cond_5
    :try_start_2
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v11, :cond_6

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_6
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_7

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_7
    move v11, v9

    goto :goto_0

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    const/4 v11, 0x3

    goto :goto_0

    :cond_8
    :try_start_3
    new-instance v3, Ljava/io/File;

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-direct {v3, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 p2, v3

    :cond_9
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isFile()Z

    move-result v11

    if-eqz v11, :cond_11

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    new-instance v11, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    new-instance v11, Ljava/io/BufferedInputStream;

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    const/16 v13, 0x2000

    invoke-direct {v11, v12, v13}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v11, p0, Lcom/konka/mm/services/CopyFileService;->in:Ljava/io/BufferedInputStream;

    new-instance v11, Ljava/io/BufferedOutputStream;

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    const/16 v13, 0x2000

    invoke-direct {v11, v12, v13}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v11, p0, Lcom/konka/mm/services/CopyFileService;->out:Ljava/io/BufferedOutputStream;

    const/16 v11, 0x2000

    new-array v1, v11, [B

    const/4 v10, 0x0

    :cond_a
    :goto_1
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->in:Ljava/io/BufferedInputStream;

    invoke-virtual {v11, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v7

    const/4 v11, -0x1

    if-ne v7, v11, :cond_c

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->out:Ljava/io/BufferedOutputStream;

    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_b
    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v11, :cond_1f

    move v11, v9

    goto/16 :goto_0

    :cond_c
    :try_start_4
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->out:Ljava/io/BufferedOutputStream;

    const/4 v12, 0x0

    invoke-virtual {v11, v1, v12, v7}, Ljava/io/BufferedOutputStream;->write([BII)V

    iget-wide v11, p0, Lcom/konka/mm/services/CopyFileService;->copyedLength:J

    int-to-long v13, v7

    add-long/2addr v11, v13

    iput-wide v11, p0, Lcom/konka/mm/services/CopyFileService;->copyedLength:J

    add-int/2addr v10, v7

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->isCancel:Z

    if-eqz v11, :cond_10

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v9, 0x2

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v11, :cond_d

    move v11, v9

    goto/16 :goto_0

    :cond_d
    :try_start_5
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v11, :cond_e

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_e
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_f

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_f
    move v11, v9

    goto/16 :goto_0

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    move v11, v9

    goto/16 :goto_0

    :cond_10
    :try_start_6
    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->isHidden:Z

    if-nez v11, :cond_a

    iget v11, p0, Lcom/konka/mm/services/CopyFileService;->perSize:I

    if-lt v10, v11, :cond_a

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->deleAfterCopy:Z

    if-nez v11, :cond_a

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v12, 0x15

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    iput v10, v8, Landroid/os/Message;->arg1:I

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    invoke-virtual {v11, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catch_3
    move-exception v4

    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v11, :cond_16

    move v11, v9

    goto/16 :goto_0

    :cond_11
    :try_start_8
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->mkdirs()Z

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    array-length v2, v5

    const/4 v6, 0x0

    :goto_2
    if-ge v6, v2, :cond_b

    aget-object v11, v5, v6

    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v5, v6

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v11, v12}, Lcom/konka/mm/services/CopyFileService;->copyFile(Ljava/io/File;Ljava/io/File;)I
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v9

    const/4 v11, 0x2

    if-ne v9, v11, :cond_15

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v11, :cond_12

    move v11, v9

    goto/16 :goto_0

    :cond_12
    :try_start_9
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v11, :cond_13

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_13
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_14

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    :cond_14
    move v11, v9

    goto/16 :goto_0

    :catch_4
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    const/4 v11, 0x2

    goto/16 :goto_0

    :cond_15
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_16
    :try_start_a
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v11, :cond_17

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_17
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_18

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :cond_18
    move v11, v9

    goto/16 :goto_0

    :catch_5
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    :goto_3
    move v11, v9

    goto/16 :goto_0

    :catch_6
    move-exception v4

    :try_start_b
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    iget-boolean v11, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v11, :cond_19

    move v11, v9

    goto/16 :goto_0

    :cond_19
    :try_start_c
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v11, :cond_1a

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_1a
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_1b

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    :cond_1b
    move v11, v9

    goto/16 :goto_0

    :catch_7
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catchall_0
    move-exception v11

    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    if-nez v12, :cond_1c

    move v11, v9

    goto/16 :goto_0

    :cond_1c
    :try_start_d
    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v12, :cond_1d

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    :cond_1d
    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v12, :cond_1e

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    :cond_1e
    move v11, v9

    goto/16 :goto_0

    :catch_8
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    throw v11

    :cond_1f
    :try_start_e
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    if-eqz v11, :cond_20

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fIn:Ljava/io/FileInputStream;

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_20
    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_21

    iget-object v11, p0, Lcom/konka/mm/services/CopyFileService;->fOut:Ljava/io/FileOutputStream;

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    :cond_21
    move v11, v9

    goto/16 :goto_0

    :catch_9
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method private doCancel()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/mm/services/CopyFileService;->doFinish(Z)V

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private doDelete()V
    .locals 10

    const/4 v9, 0x0

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    iget-object v6, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    :try_start_0
    iget-boolean v6, p0, Lcom/konka/mm/services/CopyFileService;->root:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    const/4 v7, 0x0

    const-string v8, "rm"

    invoke-virtual {v6, v7, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v6, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    const/4 v7, 0x1

    const-string v8, "-r"

    invoke-virtual {v6, v7, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/konka/mm/services/CopyFileService;->linux:Ljava/lang/Runtime;

    iget-object v6, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    add-int/lit8 v8, v5, 0x2

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Process;->waitFor()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    move-object v0, v1

    :cond_2
    iput-object v9, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    goto :goto_0

    :catch_0
    move-exception v2

    :goto_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-object v9, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    goto :goto_0

    :catch_1
    move-exception v2

    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iput-object v9, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    goto :goto_0

    :catchall_0
    move-exception v6

    :goto_3
    iput-object v9, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    :cond_3
    throw v6

    :catchall_1
    move-exception v6

    move-object v0, v1

    goto :goto_3

    :catch_2
    move-exception v2

    move-object v0, v1

    goto :goto_2

    :catch_3
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method private doFailure()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/mm/services/CopyFileService;->doFinish(Z)V

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v1, 0x1d

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private doFinish(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->isCopying:Z

    iput-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->allDoSame:Z

    iget-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->destroyAfterCopy:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/services/CopyFileService;->stopSelf()V

    :cond_0
    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->from:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->toParentPath:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private doSuccess()V
    .locals 2

    iget-boolean v1, p0, Lcom/konka/mm/services/CopyFileService;->deleAfterCopy:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/konka/mm/services/CopyFileService;->doDelete()V

    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/konka/mm/services/CopyFileService;->doFinish(Z)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/services/CopyFileService;->out:Ljava/io/BufferedOutputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/services/CopyFileService;->out:Ljava/io/BufferedOutputStream;

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/services/CopyFileService;->in:Ljava/io/BufferedInputStream;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/services/CopyFileService;->in:Ljava/io/BufferedInputStream;

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private multFile(Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v3, p0, Lcom/konka/mm/services/CopyFileService;->context:Landroid/content/Context;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lcom/konka/mm/services/CopyFileService;->allDoSame:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v4, 0x17

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "mf"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "sf"

    iget-boolean v4, p0, Lcom/konka/mm/services/CopyFileService;->doIsFile:Z

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v2, p0, Lcom/konka/mm/services/CopyFileService;->context:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    :cond_0
    iget v2, p0, Lcom/konka/mm/services/CopyFileService;->selection:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    monitor-exit v3

    const/4 v2, 0x2

    :goto_0
    return v2

    :pswitch_1
    const-string v2, "(2)"

    invoke-static {p1, v2}, Lcom/konka/mm/tools/FileTool;->fileNameAppend(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    monitor-exit v3

    const/4 v2, 0x0

    goto :goto_0

    :pswitch_2
    iput-object p1, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    monitor-exit v3

    const/4 v2, 0x1

    goto :goto_0

    :pswitch_3
    monitor-exit v3

    const/4 v2, 0x3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public cancelCopy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    invoke-direct {p0}, Lcom/konka/mm/services/CopyFileService;->doDelete()V

    iput-object v1, p0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->isCancel:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->isCopying:Z

    return-void
.end method

.method public isDestroyAfterCopy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->destroyed:Z

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->cBinder:Lcom/konka/mm/services/CopyFileService$CopyFileBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->destroyed:Z

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/services/CopyFileService;->linux:Ljava/lang/Runtime;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->destroyed:Z

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->from:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/services/CopyFileService$CopyThread;

    iget-object v2, p0, Lcom/konka/mm/services/CopyFileService;->from:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/konka/mm/services/CopyFileService;->toParentPath:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3}, Lcom/konka/mm/services/CopyFileService$CopyThread;-><init>(Lcom/konka/mm/services/CopyFileService;Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/mm/services/CopyFileService;->copyThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService;->copyThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/mm/services/CopyFileService;->context:Landroid/content/Context;

    check-cast p1, Lcom/konka/mm/filemanager/FileListActivity;

    iput-object p1, p0, Lcom/konka/mm/services/CopyFileService;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    return-void
.end method

.method public setDestroyAfterCopy(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/mm/services/CopyFileService;->destroyAfterCopy:Z

    return-void
.end method

.method public setFrom(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/services/CopyFileService;->from:Ljava/util/ArrayList;

    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    return-void
.end method

.method public setHidden(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/mm/services/CopyFileService;->isHidden:Z

    return-void
.end method

.method public setToParentPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/services/CopyFileService;->toParentPath:Ljava/lang/String;

    return-void
.end method

.method public startCopy(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 14
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->isCopying:Z

    if-eqz v12, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->root:Z

    if-nez v12, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->canWrite()Z

    move-result v12

    if-nez v12, :cond_2

    invoke-direct {p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v12

    if-eqz v12, :cond_0

    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    iput-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->deleAfterCopy:Z

    const/4 v10, -0x1

    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->checkFile:Z

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v11, 0x0

    const/4 v7, 0x0

    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    if-eqz v12, :cond_3

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v13, 0x1e

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-virtual {p1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/ArrayList;

    iput-object v12, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    :cond_3
    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    if-eqz v12, :cond_e

    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->root:Z

    if-eqz v12, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->canWrite()Z

    move-result v12

    if-eqz v12, :cond_e

    :cond_4
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v1

    :goto_1
    if-lt v9, v3, :cond_5

    :try_start_0
    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    if-eqz v12, :cond_d

    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->deleAfterCopy:Z

    invoke-direct {p0}, Lcom/konka/mm/services/CopyFileService;->doSuccess()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    goto :goto_0

    :cond_5
    :try_start_1
    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Ljava/lang/String;

    move-object v8, v0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v8}, Lcom/konka/mm/tools/FileTool;->getPathName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    new-instance v5, Ljava/io/File;

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_9

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/konka/mm/services/CopyFileService;->multFile(Ljava/lang/String;)I

    move-result v10

    const/4 v12, 0x2

    if-ne v10, v12, :cond_6

    invoke-direct {p0}, Lcom/konka/mm/services/CopyFileService;->doCancel()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v2

    :goto_2
    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    goto/16 :goto_0

    :cond_6
    const/4 v12, 0x3

    if-ne v10, v12, :cond_8

    :try_start_2
    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-object v1, v2

    :cond_7
    :goto_3
    add-int/lit8 v9, v9, 0x1

    move-object v2, v1

    goto :goto_1

    :cond_8
    new-instance v5, Ljava/io/File;

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_9
    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-virtual {v12, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_a

    move-object v1, v2

    goto :goto_3

    :cond_a
    const/4 v12, 0x3

    new-array v1, v12, [Ljava/lang/String;

    const/4 v12, 0x0

    const-string v13, "mv"

    aput-object v13, v1, v12

    const/4 v12, 0x1

    aput-object v8, v1, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    aput-object v13, v1, v12
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->linux:Ljava/lang/Runtime;

    invoke-virtual {v12, v1}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    iput-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->waitFor()I

    move-result v10

    if-eqz v10, :cond_7

    iget-boolean v12, p0, Lcom/konka/mm/services/CopyFileService;->root:Z

    if-nez v12, :cond_b

    invoke-virtual {v4}, Ljava/io/File;->canWrite()Z

    move-result v12

    if-nez v12, :cond_b

    invoke-direct {p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v6

    :goto_4
    :try_start_4
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    goto/16 :goto_0

    :cond_b
    :try_start_5
    invoke-virtual/range {p0 .. p2}, Lcom/konka/mm/services/CopyFileService;->startDoCopy(Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    :catch_1
    move-exception v6

    :goto_5
    :try_start_6
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    goto/16 :goto_0

    :catchall_0
    move-exception v12

    move-object v1, v2

    :goto_6
    iget-object v13, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz v13, :cond_c

    iget-object v13, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v13}, Ljava/lang/Process;->destroy()V

    :cond_c
    throw v12

    :cond_d
    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    if-eqz v12, :cond_f

    iget-object v12, p0, Lcom/konka/mm/services/CopyFileService;->moveProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    move-object v1, v2

    goto/16 :goto_0

    :cond_e
    invoke-virtual/range {p0 .. p2}, Lcom/konka/mm/services/CopyFileService;->startDoCopy(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v12

    goto :goto_6

    :catch_2
    move-exception v6

    move-object v1, v2

    goto :goto_5

    :catch_3
    move-exception v6

    move-object v1, v2

    goto :goto_4

    :cond_f
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public startDoCopy(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 17
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/konka/mm/services/CopyFileService;->copyLength:J

    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/konka/mm/services/CopyFileService;->copyedLength:J

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->isCopying:Z

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->isCancel:Z

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->allDoSame:Z

    const/4 v12, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/konka/mm/services/CopyFileService;->selection:I

    const/4 v9, 0x0

    :try_start_0
    const-string v12, "/sdcard"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v7, 0x0

    :goto_0
    if-lt v7, v3, :cond_3

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->deleAfterCopy:Z

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v13, 0x14

    invoke-virtual {v12, v13}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/konka/mm/services/CopyFileService;->copyLength:J

    long-to-int v12, v12

    iput v12, v8, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/konka/mm/services/CopyFileService;->copyLength:J

    long-to-int v12, v12

    div-int/lit8 v12, v12, 0x64

    move-object/from16 v0, p0

    iput v12, v0, Lcom/konka/mm/services/CopyFileService;->perSize:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    invoke-virtual {v12, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->root:Z

    if-eqz v12, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v12, v12, Lcom/konka/mm/filemanager/FileListActivity;->linux:Lcom/konka/mm/utils/LinuxFileCommand;

    iget-object v12, v12, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    const-string v13, "su"

    invoke-virtual {v12, v13}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    new-instance v12, Ljava/io/DataOutputStream;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v13}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    new-instance v12, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v14}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v12, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const/4 v7, 0x0

    :goto_2
    if-lt v7, v3, :cond_6

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    if-eqz v12, :cond_2

    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    const-string v13, "exit\n"

    invoke-virtual {v12, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->waitFor()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_d

    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    :cond_2
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    if-eqz v9, :cond_1a

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doSuccess()V

    :goto_4
    return-void

    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-wide v13, v0, Lcom/konka/mm/services/CopyFileService;->copyLength:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static {v12}, Lcom/konka/mm/filemanager/FileOperation;->getDirectorySize(Ljava/lang/String;)J

    move-result-wide v15

    add-long v12, v13, v15

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/konka/mm/services/CopyFileService;->copyLength:J

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_4
    const-wide/16 v12, 0x64

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/konka/mm/services/CopyFileService;->copyLength:J
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v2

    :try_start_3
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    if-eqz v12, :cond_5

    :try_start_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    const-string v13, "exit\n"

    invoke-virtual {v12, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->waitFor()I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_7

    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    :cond_5
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    if-eqz v9, :cond_17

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doSuccess()V

    goto :goto_4

    :cond_6
    :try_start_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v5}, Lcom/konka/mm/tools/FileTool;->getPathName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-direct {v4, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->checkFile:Z

    if-eqz v12, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    const-string v13, "(2)"

    invoke-static {v12, v13}, Lcom/konka/mm/tools/FileTool;->pathNameAppend(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    :goto_6
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-direct {v4, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_a

    :cond_7
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    if-nez v12, :cond_8

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v13, 0x1b

    invoke-virtual {v12, v13}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v12, "fp"

    invoke-virtual {v1, v12, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "tp"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    invoke-virtual {v12, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->root:Z

    if-eqz v12, :cond_13

    const-string v11, ""

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_10

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/konka/mm/services/CopyFileService;->multFile(Ljava/lang/String;)I

    move-result v10

    const/4 v12, 0x2

    if-ne v10, v12, :cond_d

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doCancel()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    if-eqz v12, :cond_9

    :try_start_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    const-string v13, "exit\n"

    invoke-virtual {v12, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->waitFor()I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_3

    :goto_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    :cond_9
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    if-eqz v9, :cond_c

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doSuccess()V

    goto/16 :goto_4

    :cond_a
    :try_start_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    const-string v13, "(2)"

    invoke-static {v12, v13}, Lcom/konka/mm/tools/FileTool;->pathNameAppend(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_6

    :catch_1
    move-exception v2

    :try_start_8
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    if-eqz v12, :cond_b

    :try_start_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    const-string v13, "exit\n"

    invoke-virtual {v12, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->waitFor()I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_9

    :goto_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    :cond_b
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    if-eqz v9, :cond_18

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doSuccess()V

    goto/16 :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_7

    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V

    goto/16 :goto_4

    :cond_d
    const/4 v12, 0x3

    if-ne v10, v12, :cond_f

    :try_start_a
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->tmpCutFiles:Ljava/util/ArrayList;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_e
    :goto_9
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    :cond_f
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->toFile:Ljava/lang/String;

    invoke-direct {v4, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    if-nez v12, :cond_12

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "cp -fpr \""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\" \""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\"\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/DataOutputStream;->write([B)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    invoke-virtual {v12}, Ljava/io/BufferedReader;->ready()Z
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_9

    :catchall_0
    move-exception v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    if-eqz v13, :cond_11

    :try_start_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    const-string v14, "exit\n"

    invoke-virtual {v13, v14}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v13}, Ljava/lang/Process;->waitFor()I
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_b

    :goto_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v13}, Ljava/lang/Process;->destroy()V

    :cond_11
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    if-eqz v9, :cond_19

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doSuccess()V

    :goto_c
    throw v12

    :cond_12
    :try_start_c
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "mv -r \""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\" \""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\"\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_a

    :cond_13
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v4}, Lcom/konka/mm/services/CopyFileService;->copyFile(Ljava/io/File;Ljava/io/File;)I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_16

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/konka/mm/services/CopyFileService;->isCancel:Z

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doCancel()V
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    if-eqz v12, :cond_14

    :try_start_d
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootOS:Ljava/io/DataOutputStream;

    const-string v13, "exit\n"

    invoke-virtual {v12, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->waitFor()I
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_5

    :goto_d
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    :cond_14
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootProcess:Ljava/lang/Process;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/mm/services/CopyFileService;->rootEBR:Ljava/io/BufferedReader;

    if-eqz v9, :cond_15

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doSuccess()V

    goto/16 :goto_4

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_d

    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V

    goto/16 :goto_4

    :cond_16
    :try_start_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/konka/mm/services/CopyFileService;->handler:Landroid/os/Handler;

    const/16 v13, 0x16

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_9

    :catch_6
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    :catch_7
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_5

    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V

    goto/16 :goto_4

    :catch_8
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_8

    :catch_9
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_8

    :cond_18
    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V

    goto/16 :goto_4

    :catch_a
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_b

    :catch_b
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_b

    :cond_19
    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V

    goto/16 :goto_c

    :catch_c
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    :catch_d
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_3

    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/services/CopyFileService;->doFailure()V

    goto/16 :goto_4
.end method

.method public successOrCancle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/services/CopyFileService;->isCancle:Z

    return v0
.end method
