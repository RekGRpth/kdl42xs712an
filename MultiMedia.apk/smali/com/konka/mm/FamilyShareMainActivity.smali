.class public Lcom/konka/mm/FamilyShareMainActivity;
.super Landroid/app/Activity;
.source "FamilyShareMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;
    }
.end annotation


# instance fields
.field private CancelBtn:Landroid/widget/Button;

.field private SearchBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->SearchBtn:Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->CancelBtn:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/FamilyShareMainActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->SearchBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/FamilyShareMainActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->CancelBtn:Landroid/widget/Button;

    return-object v0
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f0b0010    # com.konka.mm.R.id.FSMSearchBtn

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->SearchBtn:Landroid/widget/Button;

    const v0, 0x7f0b0011    # com.konka.mm.R.id.FSMCancelBtn

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->CancelBtn:Landroid/widget/Button;

    return-void
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->SearchBtn:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;

    invoke-direct {v1, p0}, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;-><init>(Lcom/konka/mm/FamilyShareMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/FamilyShareMainActivity;->CancelBtn:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;

    invoke-direct {v1, p0}, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;-><init>(Lcom/konka/mm/FamilyShareMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030004    # com.konka.mm.R.layout.family_share_main_1280x720

    invoke-virtual {p0, v0}, Lcom/konka/mm/FamilyShareMainActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/mm/FamilyShareMainActivity;->findViews()V

    invoke-direct {p0}, Lcom/konka/mm/FamilyShareMainActivity;->setListeners()V

    return-void
.end method
