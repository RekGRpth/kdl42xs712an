.class Lcom/konka/mm/modules/ModulesActivity$SortThread;
.super Ljava/lang/Object;
.source "ModulesActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/modules/ModulesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortThread"
.end annotation


# instance fields
.field private sortType:I

.field final synthetic this$0:Lcom/konka/mm/modules/ModulesActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/modules/ModulesActivity;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->sortType:I

    iput p2, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->sortType:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const v10, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    const v9, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    const/high16 v7, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    const/4 v8, 0x0

    const/4 v6, -0x1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-boolean v4, v4, Lcom/konka/mm/modules/ModulesActivity;->isUsbStateChange:Z

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v4, v4, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-ne v4, v6, :cond_4

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v1, v4, :cond_2

    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iput-boolean v8, v4, Lcom/konka/mm/modules/ModulesActivity;->isUsbStateChange:Z

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-static {v4}, Lcom/konka/mm/tools/MusicTool;->array2Files(Ljava/util/ArrayList;)[Ljava/io/File;

    move-result-object v0

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->sortType:I

    # invokes: Lcom/konka/mm/modules/ModulesActivity;->sortFiles([Ljava/io/File;I)[Ljava/io/File;
    invoke-static {v4, v0, v5}, Lcom/konka/mm/modules/ModulesActivity;->access$12(Lcom/konka/mm/modules/ModulesActivity;[Ljava/io/File;I)[Ljava/io/File;

    move-result-object v0

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->Files2array([Ljava/io/File;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->gridhandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$13(Lcom/konka/mm/modules/ModulesActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v5, v5, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/konka/mm/tools/MusicTool;->files2Strings(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->gridhandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$13(Lcom/konka/mm/modules/ModulesActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_2
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v6, v6, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v4, v10, v6}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_4
    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v6, v6, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v6, v6, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v4, v10, v6}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_5
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_APK:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v4, v4, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-ne v4, v6, :cond_7

    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const v6, 0x7f070003    # com.konka.mm.R.array.fileEndingApk

    iget-object v7, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v7, v7, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v5, v4, v6, v7}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_6

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_6

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v6, v6, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const v6, 0x7f070003    # com.konka.mm.R.array.fileEndingApk

    iget-object v7, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v7, v7, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v5, v4, v6, v7}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_8
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_TXT:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_b

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v4, v4, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-ne v4, v6, :cond_a

    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const v6, 0x7f070004    # com.konka.mm.R.array.fileEndingTxt

    iget-object v7, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v7, v7, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v5, v4, v6, v7}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_9

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_9

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_a
    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v6, v6, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const v6, 0x7f070004    # com.konka.mm.R.array.fileEndingTxt

    iget-object v7, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v7, v7, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v5, v4, v6, v7}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_b
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_10

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v4, v4, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-ne v4, v6, :cond_e

    const/4 v1, 0x0

    :goto_4
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v6, v6, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v4, v7, v6}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_c

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_c

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-gt v4, v5, :cond_c

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-le v4, v5, :cond_d

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v6, v6, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v2, v8, v5}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_c
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_d
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_e
    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v6, v6, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v6, v6, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v4, v7, v6}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-le v4, v5, :cond_f

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    invoke-virtual {v2, v8, v5}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_f
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_10
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v4, v4, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-ne v4, v6, :cond_12

    const/4 v1, 0x0

    :goto_6
    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    if-ge v4, v5, :cond_1

    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v6, v6, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v4, v9, v6}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v4, :cond_11

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_11

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_12
    iget-object v5, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/modules/ModulesActivity;->access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v6, v6, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v6, v6, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v4, v9, v6}, Lcom/konka/mm/tools/FileTool;->getAllModulesList(Lcom/konka/mm/modules/ModulesActivity;Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity$SortThread;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1
.end method
