.class public Lcom/konka/mm/receivers/ScanSdReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ScanSdReceiver.java"


# instance fields
.field private activity:Lcom/konka/mm/IActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/IActivity;)V
    .locals 0
    .param p1    # Lcom/konka/mm/IActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/receivers/ScanSdReceiver;->activity:Lcom/konka/mm/IActivity;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const v2, 0x7f090061    # com.konka.mm.R.string.MEDIA_LOADING

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/receivers/ScanSdReceiver;->activity:Lcom/konka/mm/IActivity;

    invoke-interface {v1, v2, v2}, Lcom/konka/mm/IActivity;->showProgressDialog(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "onReceive....."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/receivers/ScanSdReceiver;->activity:Lcom/konka/mm/IActivity;

    invoke-interface {v1}, Lcom/konka/mm/IActivity;->broweToRoot()V

    iget-object v1, p0, Lcom/konka/mm/receivers/ScanSdReceiver;->activity:Lcom/konka/mm/IActivity;

    invoke-interface {v1}, Lcom/konka/mm/IActivity;->cancelProgressDlg()V

    goto :goto_0
.end method
