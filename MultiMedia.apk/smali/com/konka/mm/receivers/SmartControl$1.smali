.class Lcom/konka/mm/receivers/SmartControl$1;
.super Landroid/content/BroadcastReceiver;
.source "SmartControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/receivers/SmartControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType:[I


# instance fields
.field final synthetic this$0:Lcom/konka/mm/receivers/SmartControl;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I
    .locals 3

    sget-object v0, Lcom/konka/mm/receivers/SmartControl$1;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->values()[Lcom/konka/mm/receivers/SmartControl$PlayerType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->MUSIC:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-virtual {v1}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->PHOTO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-virtual {v1}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->VIDEO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-virtual {v1}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/mm/receivers/SmartControl$1;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/konka/mm/receivers/SmartControl;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "konka.voice.control.action.PLAYER_CONTROL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "CmdName"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "Player_Play"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/konka/mm/receivers/SmartControl$1;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;
    invoke-static {v3}, Lcom/konka/mm/receivers/SmartControl;->access$0(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/receivers/SmartControl$PlayerType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v2}, Lcom/konka/mm/receivers/SmartControl;->access$1(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->play()V

    goto :goto_0

    :cond_1
    const-string v2, "Player_Pause"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/konka/mm/receivers/SmartControl$1;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;
    invoke-static {v3}, Lcom/konka/mm/receivers/SmartControl;->access$0(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/receivers/SmartControl$PlayerType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v2}, Lcom/konka/mm/receivers/SmartControl;->access$1(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->pause()V

    goto :goto_0

    :cond_2
    const-string v2, "Player_Stop"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/konka/mm/receivers/SmartControl$1;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;
    invoke-static {v3}, Lcom/konka/mm/receivers/SmartControl;->access$0(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/receivers/SmartControl$PlayerType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v2}, Lcom/konka/mm/receivers/SmartControl;->access$1(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->pause()V

    goto :goto_0

    :cond_3
    const-string v2, "Player_Setting"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Player_Prev"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/konka/mm/receivers/SmartControl$1;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;
    invoke-static {v3}, Lcom/konka/mm/receivers/SmartControl;->access$0(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/receivers/SmartControl$PlayerType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_3

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v2}, Lcom/konka/mm/receivers/SmartControl;->access$1(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->prev()V

    goto/16 :goto_0

    :cond_4
    const-string v2, "Player_Next"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/konka/mm/receivers/SmartControl$1;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;
    invoke-static {v3}, Lcom/konka/mm/receivers/SmartControl;->access$0(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/receivers/SmartControl$PlayerType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/konka/mm/receivers/SmartControl$1;->this$0:Lcom/konka/mm/receivers/SmartControl;

    # getter for: Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v2}, Lcom/konka/mm/receivers/SmartControl;->access$1(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->next()V

    goto/16 :goto_0

    :cond_5
    const-string v2, "Player_Jump_A1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "Args"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget-object v2, v1, v2

    goto/16 :goto_0

    :cond_6
    const-string v2, "Player_PlayList"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
