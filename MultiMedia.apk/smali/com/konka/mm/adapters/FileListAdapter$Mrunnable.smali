.class Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;
.super Ljava/lang/Object;
.source "FileListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/adapters/FileListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Mrunnable"
.end annotation


# instance fields
.field file:Ljava/io/File;

.field mHandler:Landroid/os/Handler;

.field picList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/konka/mm/adapters/FileListAdapter;


# direct methods
.method public constructor <init>(Lcom/konka/mm/adapters/FileListAdapter;Ljava/util/List;Ljava/io/File;Landroid/os/Handler;)V
    .locals 0
    .param p3    # Ljava/io/File;
    .param p4    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->picList:Ljava/util/List;

    iput-object p3, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->file:Ljava/io/File;

    iput-object p4, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    :goto_0
    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->picList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v8, v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    invoke-virtual {v0, v9}, Lcom/konka/mm/adapters/FileListAdapter;->getBitmapResult(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v11

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v10

    iput-object v11, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->picList:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    iget-object v2, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    iget-object v3, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    # getter for: Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;
    invoke-static {v3}, Lcom/konka/mm/adapters/FileListAdapter;->access$0(Lcom/konka/mm/adapters/FileListAdapter;)Lcom/konka/mm/filemanager/FileListActivity;

    move-result-object v3

    const/high16 v4, 0x41d00000    # 26.0f

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v2

    int-to-double v2, v2

    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    iget-object v5, p0, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    # getter for: Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;
    invoke-static {v5}, Lcom/konka/mm/adapters/FileListAdapter;->access$0(Lcom/konka/mm/adapters/FileListAdapter;)Lcom/konka/mm/filemanager/FileListActivity;

    move-result-object v5

    const/high16 v12, 0x42300000    # 44.0f

    invoke-virtual {v4, v5, v12}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v4

    int-to-double v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/konka/mm/adapters/FileListAdapter;->fileToBitmap(Ljava/lang/String;DD)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method
