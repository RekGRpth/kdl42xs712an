.class Lcom/konka/mm/adapters/FileListAdapter$2$1;
.super Ljava/lang/Object;
.source "FileListAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/adapters/FileListAdapter$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/mm/adapters/FileListAdapter$2;

.field private final synthetic val$icon:Landroid/widget/ImageView;

.field private final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/konka/mm/adapters/FileListAdapter$2;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/adapters/FileListAdapter$2$1;->this$1:Lcom/konka/mm/adapters/FileListAdapter$2;

    iput-object p2, p0, Lcom/konka/mm/adapters/FileListAdapter$2$1;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/mm/adapters/FileListAdapter$2$1;->val$icon:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/konka/mm/adapters/FileListAdapter$2$1;->this$1:Lcom/konka/mm/adapters/FileListAdapter$2;

    # getter for: Lcom/konka/mm/adapters/FileListAdapter$2;->this$0:Lcom/konka/mm/adapters/FileListAdapter;
    invoke-static {v1}, Lcom/konka/mm/adapters/FileListAdapter$2;->access$0(Lcom/konka/mm/adapters/FileListAdapter$2;)Lcom/konka/mm/adapters/FileListAdapter;

    move-result-object v1

    # getter for: Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;
    invoke-static {v1}, Lcom/konka/mm/adapters/FileListAdapter;->access$0(Lcom/konka/mm/adapters/FileListAdapter;)Lcom/konka/mm/filemanager/FileListActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/adapters/FileListAdapter$2$1;->val$url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/konka/mm/tools/FileTool;->getThumbBitmap(Landroid/app/Activity;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/konka/mm/adapters/FileListAdapter$2$1;->this$1:Lcom/konka/mm/adapters/FileListAdapter$2;

    # getter for: Lcom/konka/mm/adapters/FileListAdapter$2;->this$0:Lcom/konka/mm/adapters/FileListAdapter;
    invoke-static {v1}, Lcom/konka/mm/adapters/FileListAdapter$2;->access$0(Lcom/konka/mm/adapters/FileListAdapter$2;)Lcom/konka/mm/adapters/FileListAdapter;

    move-result-object v1

    # getter for: Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;
    invoke-static {v1}, Lcom/konka/mm/adapters/FileListAdapter;->access$0(Lcom/konka/mm/adapters/FileListAdapter;)Lcom/konka/mm/filemanager/FileListActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/adapters/FileListAdapter$2$1;->val$icon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->gc()V

    return-void
.end method
