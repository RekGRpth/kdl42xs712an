.class public Lcom/konka/mm/adapters/MusicListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MusicListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private activity:Lcom/konka/mm/music/MusicListActivity;

.field private pageIndex:I

.field private pageNum:I

.field private perPageCount:I


# direct methods
.method public constructor <init>(Lcom/konka/mm/music/MusicListActivity;III)V
    .locals 0
    .param p1    # Lcom/konka/mm/music/MusicListActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/adapters/MusicListAdapter;->activity:Lcom/konka/mm/music/MusicListActivity;

    iput p2, p0, Lcom/konka/mm/adapters/MusicListAdapter;->pageIndex:I

    iput p3, p0, Lcom/konka/mm/adapters/MusicListAdapter;->pageNum:I

    iput p4, p0, Lcom/konka/mm/adapters/MusicListAdapter;->perPageCount:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/adapters/MusicListAdapter;->pageNum:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v6, 0x0

    const/4 v3, 0x0

    if-nez p2, :cond_1

    new-instance v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;

    invoke-direct {v3, v6}, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;-><init>(Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;)V

    sget v4, Lcom/konka/mm/music/MusicDiskActivity;->List_Mode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/konka/mm/adapters/MusicListAdapter;->activity:Lcom/konka/mm/music/MusicListActivity;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030011    # com.konka.mm.R.layout.item_template

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v4, 0x7f0b0042    # com.konka.mm.R.id.img_item_icon

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v4, 0x7f0b0044    # com.konka.mm.R.id.tv_item_name

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v4, v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v5, 0x7f02006b    # com.konka.mm.R.drawable.media_default_music_uns

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    const-string v2, ""

    :try_start_0
    sget-object v4, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/mm/adapters/MusicListAdapter;->pageIndex:I

    iget v6, p0, Lcom/konka/mm/adapters/MusicListAdapter;->perPageCount:I

    mul-int/2addr v5, v6

    add-int/2addr v5, p1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    iget-object v4, v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-object p2

    :cond_0
    iget-object v4, p0, Lcom/konka/mm/adapters/MusicListAdapter;->activity:Lcom/konka/mm/music/MusicListActivity;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030012    # com.konka.mm.R.layout.listadapter

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v4, 0x7f0b0026    # com.konka.mm.R.id.picitem

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v4, 0x7f0b0028    # com.konka.mm.R.id.textitem

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v4, v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v5, 0x7f020084    # com.konka.mm.R.drawable.music_listadapter

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/adapters/MusicListAdapter$ViewHolder;

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method
