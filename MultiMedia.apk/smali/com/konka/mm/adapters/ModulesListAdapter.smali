.class public Lcom/konka/mm/adapters/ModulesListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ModulesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private activity:Lcom/konka/mm/modules/ModulesActivity;

.field private pageIndex:I

.field private pageNum:I

.field private perPageCount:I


# direct methods
.method public constructor <init>(Lcom/konka/mm/modules/ModulesActivity;III)V
    .locals 0
    .param p1    # Lcom/konka/mm/modules/ModulesActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iput p2, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->pageIndex:I

    iput p3, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->pageNum:I

    iput p4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->perPageCount:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->pageNum:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v6, 0x0

    const/4 v3, 0x0

    if-nez p2, :cond_7

    new-instance v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;

    invoke-direct {v3, v6}, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;-><init>(Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;)V

    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget v4, v4, Lcom/konka/mm/modules/ModulesActivity;->Item_Show_Mode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030011    # com.konka.mm.R.layout.item_template

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v4, 0x7f0b0042    # com.konka.mm.R.id.img_item_icon

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v4, 0x7f0b0044    # com.konka.mm.R.id.tv_item_name

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    :goto_0
    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_2

    iget-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v5, 0x7f02006c    # com.konka.mm.R.drawable.media_default_video_uns

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_1
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_2
    const-string v2, ""

    :try_start_0
    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->pageIndex:I

    iget v6, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->perPageCount:I

    mul-int/2addr v5, v6

    add-int/2addr v5, p1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    iget-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    return-object p2

    :cond_1
    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030012    # com.konka.mm.R.layout.listadapter

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v4, 0x7f0b0026    # com.konka.mm.R.id.picitem

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v4, 0x7f0b0028    # com.konka.mm.R.id.textitem

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_TXT:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_3

    iget-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v5, 0x7f0200f9    # com.konka.mm.R.drawable.txt_icon

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_APK:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_4

    iget-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const/high16 v5, 0x7f020000    # com.konka.mm.R.drawable.apk_install_icon

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_5

    iget-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v5, 0x7f020084    # com.konka.mm.R.drawable.music_listadapter

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v5, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v4, v5, :cond_0

    iget-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v5, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->pageIndex:I

    iget v6, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->perPageCount:I

    mul-int/2addr v5, v6

    add-int/2addr v5, p1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-static {v4, v5}, Lcom/konka/mm/photo/PhotoThumbLoader;->remove(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    :cond_6
    new-instance v4, Lcom/konka/mm/photo/ThumbFileDownloadHandler;

    iget-object v5, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-direct {v4, v5}, Lcom/konka/mm/photo/ThumbFileDownloadHandler;-><init>(Landroid/widget/ImageView;)V

    iput-object v4, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    iget-object v4, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->activity:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v4, v4, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->pageIndex:I

    iget v6, p0, Lcom/konka/mm/adapters/ModulesListAdapter;->perPageCount:I

    mul-int/2addr v5, v6

    add-int/2addr v5, p1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-static {v4, v5}, Lcom/konka/mm/photo/PhotoThumbLoader;->start(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/adapters/ModulesListAdapter$ViewHolder;

    goto/16 :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3
.end method
