.class public Lcom/konka/mm/adapters/FamilyShareHostListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FamilyShareHostListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;
    }
.end annotation


# instance fields
.field private Items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/FamilyShareHostItem;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private lf:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/FamilyShareHostItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;->Items:Ljava/util/List;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;->lf:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;->Items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;->Items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    if-nez p2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;->lf:Landroid/view/LayoutInflater;

    const v3, 0x7f03000a    # com.konka.mm.R.layout.host_item

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;

    invoke-direct {v0}, Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;-><init>()V

    const v2, 0x7f0b002a    # com.konka.mm.R.id.hosticon

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, v0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;->ibIcon:Landroid/widget/ImageButton;

    const v2, 0x7f0b002b    # com.konka.mm.R.id.hostname

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;->tvHostName:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, p0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter;->Items:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/FamilyShareHostItem;

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;->ibIcon:Landroid/widget/ImageButton;

    iget v3, v1, Lcom/konka/mm/FamilyShareHostItem;->Icon:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v2, v0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;->tvHostName:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/konka/mm/FamilyShareHostItem;->getHostName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/adapters/FamilyShareHostListAdapter$HostItemViewHolder;

    goto :goto_0
.end method
