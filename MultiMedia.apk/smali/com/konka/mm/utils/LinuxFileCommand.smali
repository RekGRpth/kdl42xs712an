.class public Lcom/konka/mm/utils/LinuxFileCommand;
.super Ljava/lang/Object;
.source "LinuxFileCommand.java"


# instance fields
.field public final shell:Ljava/lang/Runtime;


# direct methods
.method public constructor <init>(Ljava/lang/Runtime;)V
    .locals 0
    .param p1    # Ljava/lang/Runtime;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    return-void
.end method


# virtual methods
.method public final copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "cp"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-r"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final createDirectory(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "mkdir"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final createFile(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "touch"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final delete(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rm"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-r"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final deleteDirectory(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rm"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-r"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final deleteFile(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "rm"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final deleteMult([Ljava/lang/String;)Ljava/lang/Process;
    .locals 1
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v0, p1}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    return-object v0
.end method

.method public final du_s(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "du"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-s"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final linkFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ln"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-l"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final ls_Directory(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, ""

    :cond_0
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ls"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-a"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final ls_lhd(Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ls"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-l"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method

.method public final moveFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Process;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "mv"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    iget-object v1, p0, Lcom/konka/mm/utils/LinuxFileCommand;->shell:Ljava/lang/Runtime;

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    return-object v1
.end method
