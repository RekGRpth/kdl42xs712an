.class Lcom/konka/mm/filemanager/FileListActivity$8;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$8;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :sswitch_0
    const-string v0, "FileListActivity"

    const-string v1, "PAGE UP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$8;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_1
    const-string v0, "FileListActivity"

    const-string v1, "PAGE DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$8;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$8;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$24(Lcom/konka/mm/filemanager/FileListActivity;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$8;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v0, v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$25(Lcom/konka/mm/filemanager/FileListActivity;Z)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$8;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x132 -> :sswitch_1
        0x134 -> :sswitch_0
    .end sparse-switch
.end method
