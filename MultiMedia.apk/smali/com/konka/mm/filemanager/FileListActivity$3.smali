.class Lcom/konka/mm/filemanager/FileListActivity$3;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 10
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v7, 0x5

    const v6, 0x7f09005e    # com.konka.mm.R.string.SORT

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3}, Lcom/konka/mm/tools/FileTool;->usbIsExists(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3, v5}, Lcom/konka/mm/filemanager/FileListActivity;->access$7(Lcom/konka/mm/filemanager/FileListActivity;Z)V

    :cond_0
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    :cond_3
    new-instance v3, Lcom/konka/mm/filemanager/FileManagerFilter;

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {v3, v4}, Lcom/konka/mm/filemanager/FileManagerFilter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    sput v9, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v4, v4, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-virtual {v3, v4, v8}, Lcom/konka/mm/filemanager/FileListActivity;->intiGridView(Ljava/util/List;I)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v5, v3, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    goto :goto_0

    :sswitch_1
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3, v5}, Lcom/konka/mm/filemanager/FileListActivity;->access$14(Lcom/konka/mm/filemanager/FileListActivity;I)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3, v5}, Lcom/konka/mm/filemanager/FileListActivity;->access$15(Lcom/konka/mm/filemanager/FileListActivity;I)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3, v6, v6}, Lcom/konka/mm/filemanager/FileListActivity;->showProgressDialog(II)V

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I
    invoke-static {v6}, Lcom/konka/mm/filemanager/FileListActivity;->access$16(Lcom/konka/mm/filemanager/FileListActivity;)I

    move-result v6

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;-><init>(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;II)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :sswitch_2
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3, v9}, Lcom/konka/mm/filemanager/FileListActivity;->access$14(Lcom/konka/mm/filemanager/FileListActivity;I)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3, v6, v6}, Lcom/konka/mm/filemanager/FileListActivity;->showProgressDialog(II)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3, v5}, Lcom/konka/mm/filemanager/FileListActivity;->access$15(Lcom/konka/mm/filemanager/FileListActivity;I)V

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I
    invoke-static {v6}, Lcom/konka/mm/filemanager/FileListActivity;->access$16(Lcom/konka/mm/filemanager/FileListActivity;)I

    move-result v6

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;-><init>(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;II)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_3
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/konka/mm/filemanager/FileListActivity;->access$14(Lcom/konka/mm/filemanager/FileListActivity;I)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3, v6, v6}, Lcom/konka/mm/filemanager/FileListActivity;->showProgressDialog(II)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3, v5}, Lcom/konka/mm/filemanager/FileListActivity;->access$15(Lcom/konka/mm/filemanager/FileListActivity;I)V

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I
    invoke-static {v6}, Lcom/konka/mm/filemanager/FileListActivity;->access$16(Lcom/konka/mm/filemanager/FileListActivity;)I

    move-result v6

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;-><init>(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;II)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_4
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/konka/mm/filemanager/FileListActivity;->access$14(Lcom/konka/mm/filemanager/FileListActivity;I)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3, v6, v6}, Lcom/konka/mm/filemanager/FileListActivity;->showProgressDialog(II)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3, v5}, Lcom/konka/mm/filemanager/FileListActivity;->access$15(Lcom/konka/mm/filemanager/FileListActivity;I)V

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I
    invoke-static {v6}, Lcom/konka/mm/filemanager/FileListActivity;->access$16(Lcom/konka/mm/filemanager/FileListActivity;)I

    move-result v6

    invoke-direct {v4, v5, v1, v6, v7}, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;-><init>(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;II)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_5
    const/4 v3, 0x2

    sput v3, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v4, v4, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-virtual {v3, v4, v8}, Lcom/konka/mm/filemanager/FileListActivity;->intiGridView(Ljava/util/List;I)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v5, v3, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$3;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v3}, Lcom/konka/mm/finals/CommonFinals;->sdCardNoFound(Landroid/app/Activity;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0b00b4 -> :sswitch_0    # com.konka.mm.R.id.list_small_style
        0x7f0b00b5 -> :sswitch_5    # com.konka.mm.R.id.list_big_style
        0x7f0b00fe -> :sswitch_2    # com.konka.mm.R.id.rb_sort_by_filename
        0x7f0b00ff -> :sswitch_3    # com.konka.mm.R.id.rb_sort_by_filesize
        0x7f0b0100 -> :sswitch_4    # com.konka.mm.R.id.rb_sort_by_filetype
        0x7f0b0101 -> :sswitch_1    # com.konka.mm.R.id.rb_sort_by_default
    .end sparse-switch
.end method
