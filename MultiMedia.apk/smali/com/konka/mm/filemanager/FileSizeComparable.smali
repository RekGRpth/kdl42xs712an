.class public Lcom/konka/mm/filemanager/FileSizeComparable;
.super Ljava/lang/Object;
.source "FileSizeComparable.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/io/File;Ljava/io/File;)I
    .locals 9
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    const-wide/16 v7, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/konka/mm/filemanager/FileSizeComparable;->getFileSiez(Ljava/io/File;)J

    move-result-wide v2

    invoke-virtual {p0, p2}, Lcom/konka/mm/filemanager/FileSizeComparable;->getFileSiez(Ljava/io/File;)J

    move-result-wide v4

    sub-long v0, v4, v2

    cmp-long v6, v0, v7

    if-lez v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    return v6

    :cond_0
    cmp-long v6, v0, v7

    if-gez v6, :cond_1

    const/4 v6, -0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/io/File;

    check-cast p2, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/filemanager/FileSizeComparable;->compare(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    return v0
.end method

.method public getFileSiez(Ljava/io/File;)J
    .locals 4
    .param p1    # Ljava/io/File;

    const-wide/16 v1, 0x0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getFileSize(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :cond_0
    :goto_0
    return-wide v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_1
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getFileSizes(Ljava/io/File;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
