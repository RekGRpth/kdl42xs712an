.class public Lcom/konka/mm/filemanager/ItemMenuDialog;
.super Landroid/app/Dialog;
.source "ItemMenuDialog.java"

# interfaces
.implements Landroid/content/DialogInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;
    }
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

.field filePath:Ljava/lang/String;

.field gv:Landroid/widget/GridView;

.field height:I

.field layout:Landroid/widget/LinearLayout;

.field menuNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->height:I

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/filemanager/ItemMenuDialog;->requestWindowFeature(I)Z

    iput-object p1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->context:Landroid/content/Context;

    move-object v1, p1

    check-cast v1, Lcom/konka/mm/filemanager/FileListActivity;

    iput-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v1}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070009    # com.konka.mm.R.array.longClickListMenu

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->menuNames:[Ljava/lang/String;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030009    # com.konka.mm.R.layout.gridview_menu

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->layout:Landroid/widget/LinearLayout;

    const v2, 0x7f0b0029    # com.konka.mm.R.id.gridview

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->gv:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->gv:Landroid/widget/GridView;

    new-instance v2, Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;

    invoke-direct {v2, p0}, Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;-><init>(Lcom/konka/mm/filemanager/ItemMenuDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->gv:Landroid/widget/GridView;

    new-instance v2, Lcom/konka/mm/filemanager/ItemMenuDialog$1;

    invoke-direct {v2, p0}, Lcom/konka/mm/filemanager/ItemMenuDialog$1;-><init>(Lcom/konka/mm/filemanager/ItemMenuDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/konka/mm/filemanager/ItemMenuDialog;->setContentView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    iget v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->height:I

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->height:I

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v0, v1

    if-ltz v0, :cond_1

    iget v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->height:I

    if-le v0, v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/ItemMenuDialog;->dismiss()V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Dialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public selectedFile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog;->filePath:Ljava/lang/String;

    return-void
.end method
