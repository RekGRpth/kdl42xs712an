.class public Lcom/konka/mm/filemanager/FileSizeComparable1;
.super Ljava/lang/Object;
.source "FileSizeComparable1.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/filemanager/FileSizeComparable1;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-wide/16 v9, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v5, 0x0

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/konka/mm/filemanager/FileSizeComparable1;->getFileSiez(Ljava/io/File;)J

    move-result-wide v2

    invoke-virtual {p0, v7}, Lcom/konka/mm/filemanager/FileSizeComparable1;->getFileSiez(Ljava/io/File;)J

    move-result-wide v5

    sub-long v0, v5, v2

    cmp-long v8, v0, v9

    if-lez v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    return v8

    :cond_0
    cmp-long v8, v0, v9

    if-gez v8, :cond_1

    const/4 v8, -0x1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public getFileSiez(Ljava/io/File;)J
    .locals 4
    .param p1    # Ljava/io/File;

    const-wide/16 v1, 0x0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getFileSize(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :cond_0
    :goto_0
    return-wide v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_1
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getFileSizes(Ljava/io/File;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
