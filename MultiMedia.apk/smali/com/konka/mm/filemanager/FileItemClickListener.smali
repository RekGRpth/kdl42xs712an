.class public Lcom/konka/mm/filemanager/FileItemClickListener;
.super Ljava/lang/Object;
.source "FileItemClickListener.java"


# static fields
.field public static final BUNDLE_FROM_PATH:Ljava/lang/String; = "fp"

.field public static final BUNDLE_IS_FILE:Ljava/lang/String; = "sf"

.field public static final BUNDLE_MULT_FILE_PATH:Ljava/lang/String; = "mf"

.field public static final BUNDLE_TO_PATH:Ljava/lang/String; = "tp"

.field public static final COPY_APPEND:I = 0x0

.field public static final COPY_CANCEL:I = 0x2

.field public static final COPY_REPLACED:I = 0x1

.field public static final COPY_SKIP:I = 0x3

.field public static final FILE_TYPE_DIR:Ljava/lang/String; = "dir"

.field public static final HANDLER_COPY_CANCEL:I = 0x1f

.field public static final HANDLER_COPY_FAILURE:I = 0x1d

.field public static final HANDLER_COPY_FINISHED:I = 0x18

.field public static final HANDLER_CUT_FINISH:I = 0x21

.field public static final HANDLER_FILE_SIZE:I = 0x20

.field public static final HANDLER_INCREMENT_COPY_PROGRESS:I = 0x15

.field public static final HANDLER_PROCESS_SET_MAX:I = 0x19

.field public static final HANDLER_PROCESS_SET_MESSAGE:I = 0x1b

.field public static final HANDLER_PROCESS_SET_VALUE:I = 0x1a

.field public static final HANDLER_REFRESH_LIST:I = 0x16

.field public static final HANDLER_SHOW_COPY_PROGRESS_DIALOG:I = 0x14

.field public static final HANDLER_SHOW_COPY_WARN:I = 0x17

.field public static final HANDLER_SHOW_CUT_DIALOG:I = 0x1e

.field public static final HANDLER_SHOW_DELETE_ERROR:I = 0x1c

.field public static final OPEN_APK:I = 0x4

.field public static final OPEN_AUDIO:I = 0x2

.field public static final OPEN_IMAGE:I = 0x1

.field public static final OPEN_TEXT:I = 0x0

.field public static final OPEN_VIDEO:I = 0x3

.field private static final PER_NULL:I = 0x0

.field private static final PER_ONLY_EXEC:I = 0x2

.field private static final PER_ONLY_READ:I = 0x1

.field private static final PER_READ_EXEC:I = 0x4

.field private static final PER_READ_WRITE:I = 0x3

.field private static final PER_WRITE_EXEC:I = 0x5


# instance fields
.field private TAG:Ljava/lang/String;

.field public absPath:Ljava/lang/String;

.field public allDoLikeThis:Z

.field public bTmp:Ljava/lang/String;

.field public br:Ljava/io/BufferedReader;

.field private copyFileService:Lcom/konka/mm/services/CopyFileService;

.field public copyProgressDialog:Landroid/app/ProgressDialog;

.field public copyWarningDialog:Landroid/app/AlertDialog$Builder;

.field public copyWarningSelection:I

.field public creatDirectory:Z

.field public createFileDialog:Landroid/app/AlertDialog;

.field public createFileEdit:Landroid/widget/EditText;

.field deleProgress:Ljava/lang/Process;

.field public deleteDialog:Landroid/app/AlertDialog;

.field private dirSize:Ljava/lang/String;

.field doDeleDialog:Landroid/app/ProgressDialog;

.field private doSelected:Z

.field fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

.field public inputEdit:Landroid/widget/EditText;

.field private isCopying:Z

.field public isCut:Z

.field listener:Landroid/content/DialogInterface$OnClickListener;

.field multPaste:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public openMannerDialog:Landroid/app/AlertDialog;

.field public pastePath:Ljava/lang/String;

.field public position:I

.field public renameDialog:Landroid/app/AlertDialog;

.field public renamePath:Ljava/lang/String;

.field public renameWarningDialog:Landroid/app/AlertDialog;

.field private selectView:Landroid/view/View;

.field public type:Ljava/lang/String;

.field private xHandler:Landroid/os/Handler;

.field private xSConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 3
    .param p1    # Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "FileDialog"

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->TAG:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->dirSize:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->selectView:Landroid/view/View;

    iput v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->position:I

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCut:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->allDoLikeThis:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->creatDirectory:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCopying:Z

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->deleProgress:Ljava/lang/Process;

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->doSelected:Z

    new-instance v0, Lcom/konka/mm/filemanager/FileItemClickListener$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileItemClickListener$1;-><init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->xSConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/konka/mm/filemanager/FileItemClickListener$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileItemClickListener$2;-><init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->xHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/filemanager/FileItemClickListener$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileItemClickListener$3;-><init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->listener:Landroid/content/DialogInterface$OnClickListener;

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileItemClickListener;->init()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/filemanager/FileItemClickListener;Lcom/konka/mm/services/CopyFileService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/filemanager/FileItemClickListener;)Lcom/konka/mm/services/CopyFileService;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/filemanager/FileItemClickListener;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->xHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/filemanager/FileItemClickListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileItemClickListener;->initCopyProgressDialog()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/filemanager/FileItemClickListener;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/filemanager/FileItemClickListener;->dealCopyBack(I)V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/filemanager/FileItemClickListener;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCopying:Z

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/filemanager/FileItemClickListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileItemClickListener;->doRename()V

    return-void
.end method

.method private clearPaste()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->multPaste:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private dealCopyBack(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    const-string v0, ""

    sparse-switch p1, :sswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->pastePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, v1, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    iget-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCut:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileItemClickListener;->clearPaste()V

    :cond_2
    iput-boolean v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCopying:Z

    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v2, 0x7f0900bf    # com.konka.mm.R.string.cancel_copy

    invoke-virtual {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v2, 0x7f0900c0    # com.konka.mm.R.string.fail_to_copy

    invoke-virtual {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v2, 0x7f0900c1    # com.konka.mm.R.string.done_copy

    invoke-virtual {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_2
        0x1d -> :sswitch_1
        0x1f -> :sswitch_0
        0x21 -> :sswitch_2
    .end sparse-switch
.end method

.method private doRename()V
    .locals 8

    const/4 v0, 0x0

    const/4 v4, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileListActivity;->linux:Lcom/konka/mm/utils/LinuxFileCommand;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->absPath:Ljava/lang/String;

    iget-object v7, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/konka/mm/utils/LinuxFileCommand;->moveFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v4}, Ljava/lang/Process;->waitFor()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v6, 0x7f0900a7    # com.konka.mm.R.string.RENAME_FAILED

    const/4 v7, 0x1

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V

    move-object v0, v1

    :goto_1
    return-void

    :cond_0
    :try_start_2
    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    const/16 v5, 0xd

    iput v5, v3, Landroid/os/Message;->what:I

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    iput-object v5, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v5, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v2

    move-object v0, v1

    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V

    goto :goto_1

    :catch_1
    move-exception v2

    :goto_3
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V

    goto :goto_1

    :catchall_0
    move-exception v5

    :goto_4
    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V

    throw v5

    :catchall_1
    move-exception v5

    move-object v0, v1

    goto :goto_4

    :catch_2
    move-exception v2

    move-object v0, v1

    goto :goto_3

    :catch_3
    move-exception v2

    goto :goto_2
.end method

.method private initCopyProgressDialog()V
    .locals 4

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v2, 0x7f0900b7    # com.konka.mm.R.string.copyFile

    invoke-virtual {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, -0x3

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v3, 0x7f0900c3    # com.konka.mm.R.string.hide

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, -0x2

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090029    # com.konka.mm.R.string.cancel

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->listener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final doBindCopyService()V
    .locals 4

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const-class v3, Lcom/konka/mm/services/CopyFileService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->xSConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method public doCopy()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->multPaste:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->multPaste:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->absPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public doPaste()V
    .locals 3

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->multPaste:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const-string v1, "\u526a\u5207\u677f\u4e3a\u7a7a"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->multPaste:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, v1, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCut:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/konka/mm/filemanager/FileItemClickListener;->startCopyService(Ljava/util/ArrayList;Ljava/lang/String;Z)V

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCut:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->multPaste:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public getFile()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->absPath:Ljava/lang/String;

    return-object v0
.end method

.method public init()V
    .locals 4

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->absPath:Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->type:Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->bTmp:Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->multPaste:Ljava/util/ArrayList;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->inputEdit:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->inputEdit:Landroid/widget/EditText;

    invoke-static {v1, v2, v3}, Lcom/konka/mm/utils/XDialog;->createInputDialog(Landroid/content/Context;Landroid/widget/TextView;Landroid/widget/EditText;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v2, 0x7f0900bd    # com.konka.mm.R.string.rename

    invoke-virtual {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v3, 0x7f09002a    # com.konka.mm.R.string.ok

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/konka/mm/filemanager/FileItemClickListener$4;

    invoke-direct {v3, p0}, Lcom/konka/mm/filemanager/FileItemClickListener$4;-><init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v3, 0x7f090029    # com.konka.mm.R.string.cancel

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/konka/mm/filemanager/FileItemClickListener$5;

    invoke-direct {v3, p0}, Lcom/konka/mm/filemanager/FileItemClickListener$5;-><init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->renameDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public onClick(Landroid/view/View;I)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->selectView:Landroid/view/View;

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->absPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "dir"

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->type:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->inputEdit:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->renameDialog:Landroid/app/AlertDialog;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const v4, 0x7f0900bd    # com.konka.mm.R.string.rename

    invoke-virtual {v3, v4}, Lcom/konka/mm/filemanager/FileListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->renameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->type:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->inputEdit:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v1, v1, Lcom/konka/mm/filemanager/FileListActivity;->multFile:Z

    if-nez v1, :cond_0

    iput-boolean v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCut:Z

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileItemClickListener;->doCopy()V

    goto :goto_0

    :pswitch_3
    iget-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCopying:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const-string v2, "\u6b63\u5728\u590d\u5236\u4e2d"

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->isCopying:Z

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileItemClickListener;->doPaste()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->absPath:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->name:Ljava/lang/String;

    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->position:I

    return-void
.end method

.method public startCopyService(Ljava/util/ArrayList;Ljava/lang/String;Z)V
    .locals 4
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->pastePath:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->allDoLikeThis:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyWarningSelection:I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;

    iput-boolean p3, v0, Lcom/konka/mm/services/CopyFileService;->isCut:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;

    invoke-virtual {v0, p1}, Lcom/konka/mm/services/CopyFileService;->setFrom(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;

    invoke-virtual {v0, p2}, Lcom/konka/mm/services/CopyFileService;->setToParentPath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const-class v3, Lcom/konka/mm/services/CopyFileService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public stopCopyService()V
    .locals 4

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const-class v3, Lcom/konka/mm/services/CopyFileService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->stopService(Landroid/content/Intent;)Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener;->xSConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method
