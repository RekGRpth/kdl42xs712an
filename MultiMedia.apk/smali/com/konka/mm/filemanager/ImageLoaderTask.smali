.class public Lcom/konka/mm/filemanager/ImageLoaderTask;
.super Landroid/os/AsyncTask;
.source "ImageLoaderTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private bmp:Landroid/graphics/Bitmap;

.field private context:Landroid/content/Context;

.field private image:Landroid/widget/ImageView;

.field private path:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/ImageView;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->image:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->path:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/mm/filemanager/ImageLoaderTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Thread Name: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->path:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/konka/mm/tools/FileTool;->getThumbBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->bmp:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->bmp:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->bmp:Landroid/graphics/Bitmap;

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/ImageLoaderTask;->publishProgress([Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getBmp()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->bmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getImage()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->image:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->path:Ljava/lang/String;

    return-object v0
.end method

.method public bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/mm/filemanager/ImageLoaderTask;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method public varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 2
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->image:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->bmp:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setBmp(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->bmp:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setImage(Landroid/widget/ImageView;)V
    .locals 0
    .param p1    # Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->image:Landroid/widget/ImageView;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/filemanager/ImageLoaderTask;->path:Ljava/lang/String;

    return-void
.end method
