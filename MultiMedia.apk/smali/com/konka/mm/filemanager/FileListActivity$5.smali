.class Lcom/konka/mm/filemanager/FileListActivity$5;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput p3, v2, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;
    invoke-static {v2, p3}, Lcom/konka/mm/filemanager/FileListActivity;->access$18(Lcom/konka/mm/filemanager/FileListActivity;I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-virtual {v2, v1}, Lcom/konka/mm/filemanager/FileItemClickListener;->setFile(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v3, v3, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v4, v4, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    mul-int/2addr v3, v4

    add-int/2addr v3, p3

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileItemClickListener;->setPosition(I)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;
    invoke-static {v3, p3}, Lcom/konka/mm/filemanager/FileListActivity;->access$18(Lcom/konka/mm/filemanager/FileListActivity;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileItemClickListener;->setName(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/mm/filemanager/ItemMenuDialog;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$5;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {v0, v2}, Lcom/konka/mm/filemanager/ItemMenuDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/ItemMenuDialog;->selectedFile(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/filemanager/ItemMenuDialog;->show()V

    const/4 v2, 0x1

    return v2
.end method
