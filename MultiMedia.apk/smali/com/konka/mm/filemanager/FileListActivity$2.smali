.class Lcom/konka/mm/filemanager/FileListActivity$2;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/filemanager/FileListActivity$2;)Lcom/konka/mm/filemanager/FileListActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/16 v4, 0x113

    const/4 v1, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupView:Landroid/view/View;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$3(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x96

    const/16 v4, 0xb6

    invoke-direct {v1, v2, v3, v4, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    invoke-static {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$4(Lcom/konka/mm/filemanager/FileListActivity;Landroid/widget/PopupWindow;)V

    :goto_1
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    new-instance v1, Lcom/konka/mm/filemanager/FileListActivity$2$1;

    invoke-direct {v1, p0}, Lcom/konka/mm/filemanager/FileListActivity$2$1;-><init>(Lcom/konka/mm/filemanager/FileListActivity$2;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->isPopup:Z
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$9(Lcom/konka/mm/filemanager/FileListActivity;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->sort:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$10(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/Button;

    move-result-object v1

    const/16 v2, -0x3d

    invoke-virtual {v0, v1, v2, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    :goto_2
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v0, v6}, Lcom/konka/mm/filemanager/FileListActivity;->access$7(Lcom/konka/mm/filemanager/FileListActivity;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v1}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f090000    # com.konka.mm.R.string.str_sys_language

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$5(Lcom/konka/mm/filemanager/FileListActivity;Ljava/lang/String;)V

    const-string v0, "zh"

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$6(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupView:Landroid/view/View;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$3(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0xc8

    invoke-direct {v1, v2, v3, v4, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    invoke-static {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$4(Lcom/konka/mm/filemanager/FileListActivity;Landroid/widget/PopupWindow;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupView:Landroid/view/View;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$3(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x140

    invoke-direct {v1, v2, v3, v4, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    invoke-static {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$4(Lcom/konka/mm/filemanager/FileListActivity;Landroid/widget/PopupWindow;)V

    goto/16 :goto_1

    :cond_3
    const-string v0, "zh"

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$6(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->sort:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$10(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/Button;

    move-result-object v1

    const/16 v2, -0x4b

    invoke-virtual {v0, v1, v2, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->sort:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$10(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/Button;

    move-result-object v1

    const/16 v2, -0xbe

    invoke-virtual {v0, v1, v2, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v0, v5}, Lcom/konka/mm/filemanager/FileListActivity;->access$7(Lcom/konka/mm/filemanager/FileListActivity;Z)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v6, v0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput v5, v0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v6}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v5, v0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v5, v0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput v1, v0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v5}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->deleteFile()V
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$11(Lcom/konka/mm/filemanager/FileListActivity;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->deleteAllFile()V
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$12(Lcom/konka/mm/filemanager/FileListActivity;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v0}, Lcom/konka/mm/filemanager/FileListActivity;->renameFile()V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$2;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v0, v6}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0013 -> :sswitch_0    # com.konka.mm.R.id.img_file_sort
        0x7f0b001a -> :sswitch_1    # com.konka.mm.R.id.img_file_manager
        0x7f0b001d -> :sswitch_2    # com.konka.mm.R.id.img_file_delete
        0x7f0b0020 -> :sswitch_3    # com.konka.mm.R.id.img_file_delete_all
        0x7f0b0023 -> :sswitch_4    # com.konka.mm.R.id.img_file_rename
        0x7f0b002f -> :sswitch_5    # com.konka.mm.R.id.btn_list_left
        0x7f0b0034 -> :sswitch_6    # com.konka.mm.R.id.btn_list_right
    .end sparse-switch
.end method
