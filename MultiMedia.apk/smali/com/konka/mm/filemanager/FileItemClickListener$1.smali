.class Lcom/konka/mm/filemanager/FileItemClickListener$1;
.super Ljava/lang/Object;
.source "FileItemClickListener.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileItemClickListener;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener$1;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener$1;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    check-cast p2, Lcom/konka/mm/services/CopyFileService$CopyFileBinder;

    invoke-virtual {p2}, Lcom/konka/mm/services/CopyFileService$CopyFileBinder;->getService()Lcom/konka/mm/services/CopyFileService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$0(Lcom/konka/mm/filemanager/FileItemClickListener;Lcom/konka/mm/services/CopyFileService;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener$1;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    # getter for: Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$1(Lcom/konka/mm/filemanager/FileItemClickListener;)Lcom/konka/mm/services/CopyFileService;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener$1;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    # getter for: Lcom/konka/mm/filemanager/FileItemClickListener;->xHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$2(Lcom/konka/mm/filemanager/FileItemClickListener;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/mm/services/CopyFileService;->setHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener$1;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    # getter for: Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$1(Lcom/konka/mm/filemanager/FileItemClickListener;)Lcom/konka/mm/services/CopyFileService;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileItemClickListener$1;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v1, v1, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/services/CopyFileService;->setContext(Landroid/content/Context;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;

    return-void
.end method
