.class public Lcom/konka/mm/filemanager/FileDiskActivity;
.super Landroid/app/Activity;
.source "FileDiskActivity.java"

# interfaces
.implements Lcom/konka/mm/IActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;
    }
.end annotation


# static fields
.field public static List_Mode:I = 0x0

.field private static final SWITCHINPUTSOURCEMSG:Ljava/lang/String; = "com.konka.service.switchinputsourcemsg"

.field private static diskRoot:Ljava/lang/String;


# instance fields
.field private adapter:Lcom/konka/mm/adapters/FileDiskListAdapter;

.field private curDiskName:Ljava/lang/String;

.field private diskDetail:Landroid/widget/TextView;

.field private diskName:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fileDiskList:Landroid/widget/GridView;

.field private is3dKeyDown:Z

.field private isEnterOtherAct:Z

.field private isRemove:Z

.field private itemListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private pipSkin:Lcom/mstar/tv/service/skin/PipSkin;

.field private usbReceiver:Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;

.field private usbs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private versionNum:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskRoot:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskName:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->isEnterOtherAct:Z

    iput-boolean v2, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->is3dKeyDown:Z

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->curDiskName:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->isRemove:Z

    new-instance v0, Lcom/konka/mm/filemanager/FileDiskActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileDiskActivity$1;-><init>(Lcom/konka/mm/filemanager/FileDiskActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->itemListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/filemanager/FileDiskActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->isEnterOtherAct:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskRoot:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskRoot:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskName:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/mm/filemanager/FileDiskActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->curDiskName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->curDiskName:Ljava/lang/String;

    return-object v0
.end method

.method private findViews()V
    .locals 6

    const v3, 0x7f0b0015    # com.konka.mm.R.id.file_disk_list_gridView

    invoke-virtual {p0, v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    iput-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->fileDiskList:Landroid/widget/GridView;

    const v3, 0x7f0b0016    # com.konka.mm.R.id.tv_file_disk_info

    invoke-virtual {p0, v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskDetail:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskDetail:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const v3, 0x7f0b0014    # com.konka.mm.R.id.tv_version_num

    invoke-virtual {p0, v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->versionNum:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v1, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v2, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->versionNum:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const v5, 0x7f09000e    # com.konka.mm.R.string.Version

    invoke-virtual {p0, v5}, Lcom/konka/mm/filemanager/FileDiskActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".00 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f09000f    # com.konka.mm.R.string.Build

    invoke-virtual {p0, v5}, Lcom/konka/mm/filemanager/FileDiskActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->findViews()V

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->setListeners()V

    new-instance v1, Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;

    invoke-direct {v1, p0}, Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;-><init>(Lcom/konka/mm/filemanager/FileDiskActivity;)V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbReceiver:Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->broweToRoot()V

    const/4 v1, 0x1

    sput v1, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.konka.service.switchinputsourcemsg"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbReceiver:Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/mm/filemanager/FileDiskActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private initGrid(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/konka/mm/adapters/FileDiskListAdapter;

    invoke-direct {v0, p0, p1}, Lcom/konka/mm/adapters/FileDiskListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->adapter:Lcom/konka/mm/adapters/FileDiskListAdapter;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->fileDiskList:Landroid/widget/GridView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->fileDiskList:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->adapter:Lcom/konka/mm/adapters/FileDiskListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->fileDiskList:Landroid/widget/GridView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    return-void
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->fileDiskList:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->itemListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public broweToRoot()V
    .locals 2

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->getUsbs(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;

    invoke-static {v0}, Lcom/konka/mm/tools/FileTool;->changeDiskName(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskName:Ljava/util/List;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskName:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->curDiskName:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->curDiskName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileDiskActivity;->initDiskInfo(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskName:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/konka/mm/filemanager/FileDiskActivity;->initGrid(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->adapter:Lcom/konka/mm/adapters/FileDiskListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->adapter:Lcom/konka/mm/adapters/FileDiskListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/mm/adapters/FileDiskListAdapter;->setInfos(Ljava/util/List;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->adapter:Lcom/konka/mm/adapters/FileDiskListAdapter;

    invoke-virtual {v0}, Lcom/konka/mm/adapters/FileDiskListAdapter;->notifyDataSetChanged()V

    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileDiskActivity;->initDiskInfo(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/konka/mm/finals/CommonFinals;->sdCardNoFound(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public cancelProgressDlg()V
    .locals 0

    return-void
.end method

.method public initDiskInfo(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskDetail:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->diskDetail:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-object v3, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Liapp/eric/utils/base/Trace;->setTag(Ljava/lang/String;)V

    const v3, 0x7f030005    # com.konka.mm.R.layout.file_disk_1280x720

    invoke-virtual {p0, v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "showwhat"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get showwhat is:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Liapp/eric/utils/base/Trace;->Warning(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/GlobalData;

    invoke-virtual {v1, v2}, Lcom/konka/mm/GlobalData;->setShowWhat(I)V

    :cond_0
    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->init()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "\ufffd\ufffd\ufffd\ufffd"

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbReceiver:Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->usbReceiver:Lcom/konka/mm/filemanager/FileDiskActivity$UsbReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileDiskActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileDiskActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09001a    # com.konka.mm.R.string.MM_FILE_MANAGER

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/konka/mm/finals/CommonFinals;->quitActivity(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->is3dKeyDown:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0xce -> :sswitch_1
        0x205 -> :sswitch_1
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    invoke-static {p0}, Lcom/konka/mm/tools/VersionTool;->VersionShow(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->isEnterOtherAct:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->is3dKeyDown:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->is3dKeyDown:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onRestart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileDiskActivity;->isEnterOtherAct:Z

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    return-void
.end method

.method public showProgressDialog(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method
