.class Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;
.super Landroid/os/Handler;
.source "AsycLoadImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/AsycLoadImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/AsycLoadImage;


# direct methods
.method public constructor <init>(Lcom/konka/mm/filemanager/AsycLoadImage;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;->this$0:Lcom/konka/mm/filemanager/AsycLoadImage;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/konka/mm/filemanager/AsycLoadImage;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;->this$0:Lcom/konka/mm/filemanager/AsycLoadImage;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    :try_start_0
    invoke-static {}, Ljava/lang/System;->gc()V

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/util/HashMap;

    const-string v5, "path"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "icon"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;->this$0:Lcom/konka/mm/filemanager/AsycLoadImage;

    # getter for: Lcom/konka/mm/filemanager/AsycLoadImage;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/konka/mm/filemanager/AsycLoadImage;->access$0(Lcom/konka/mm/filemanager/AsycLoadImage;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-static {v5, v4}, Lcom/konka/mm/tools/FileTool;->getThumbBitmap(Landroid/app/Activity;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const v5, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
