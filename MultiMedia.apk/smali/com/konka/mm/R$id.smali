.class public final Lcom/konka/mm/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final FSMCancelBtn:I = 0x7f0b0011

.field public static final FSMHostItem1:I = 0x7f0b0006

.field public static final FSMHostItem2:I = 0x7f0b0008

.field public static final FSMHostItem3:I = 0x7f0b000a

.field public static final FSMHostItem4:I = 0x7f0b000c

.field public static final FSMHostItem5:I = 0x7f0b000e

.field public static final FSMHostItemName1:I = 0x7f0b0007

.field public static final FSMHostItemName2:I = 0x7f0b0009

.field public static final FSMHostItemName3:I = 0x7f0b000b

.field public static final FSMHostItemName4:I = 0x7f0b000d

.field public static final FSMHostItemName5:I = 0x7f0b000f

.field public static final FSMModuleName:I = 0x7f0b0005

.field public static final FSMSearchBtn:I = 0x7f0b0010

.field public static final RelativeLayout_Item:I = 0x7f0b0040

.field public static final SelectDiskIdPath:I = 0x7f0b00bc

.field public static final SelectDiskListRow:I = 0x7f0b00bf

.field public static final SelectDiskPathRow:I = 0x7f0b00bd

.field public static final SelectDiskPathText:I = 0x7f0b00be

.field public static final SelectDiskStatus:I = 0x7f0b00c5

.field public static final TableLayout1:I = 0x7f0b004d

.field public static final TableLayout2:I = 0x7f0b0051

.field public static final TableRow1:I = 0x7f0b004e

.field public static final TableRow2:I = 0x7f0b0052

.field public static final VideoBrowser2ModeSwitch:I = 0x7f0b0105

.field public static final VideoBrowser2ModuleName:I = 0x7f0b0104

.field public static final VideoBrowser2PrevDown:I = 0x7f0b0107

.field public static final VideoBrowser2PrevUp:I = 0x7f0b0106

.field public static final VideoBrowserFullName:I = 0x7f0b0109

.field public static final VideoBrowserGridView:I = 0x7f0b0103

.field public static final VideoBrowserThumbList:I = 0x7f0b0108

.field public static final album_image:I = 0x7f0b0068

.field public static final album_picture:I = 0x7f0b0064

.field public static final albumlist_item_album_name:I = 0x7f0b0001

.field public static final albumlist_item_album_year:I = 0x7f0b0000

.field public static final appmenu:I = 0x7f0b00d3

.field public static final approw1:I = 0x7f0b00d4

.field public static final approw2:I = 0x7f0b00d5

.field public static final bSelectDisk1:I = 0x7f0b00c0

.field public static final bSelectDisk2:I = 0x7f0b00c1

.field public static final bSelectDisk3:I = 0x7f0b00c2

.field public static final bSelectDisk4:I = 0x7f0b00c3

.field public static final bSelectDisk5:I = 0x7f0b00c4

.field public static final bt_music_cycle:I = 0x7f0b0072

.field public static final bt_music_next:I = 0x7f0b006f

.field public static final bt_music_pause:I = 0x7f0b006e

.field public static final bt_music_play:I = 0x7f0b006d

.field public static final bt_music_pre:I = 0x7f0b006c

.field public static final bt_music_repeat_one:I = 0x7f0b0071

.field public static final bt_music_sequence:I = 0x7f0b0070

.field public static final bt_music_shuffle:I = 0x7f0b0073

.field public static final btn_list_left:I = 0x7f0b002f

.field public static final btn_list_left_music:I = 0x7f0b003d

.field public static final btn_list_right:I = 0x7f0b0034

.field public static final btn_list_right_music:I = 0x7f0b003e

.field public static final btn_menu_delete_cancle:I = 0x7f0b0059

.field public static final btn_menu_delete_sure:I = 0x7f0b0058

.field public static final btn_music_cycle:I = 0x7f0b008c

.field public static final btn_music_list:I = 0x7f0b007e

.field public static final btn_music_lrc:I = 0x7f0b007f

.field public static final btn_music_next:I = 0x7f0b0086

.field public static final btn_music_pause:I = 0x7f0b0084

.field public static final btn_music_play:I = 0x7f0b0083

.field public static final btn_music_pre:I = 0x7f0b0081

.field public static final btn_music_repeat_one:I = 0x7f0b008b

.field public static final btn_music_sequence:I = 0x7f0b008a

.field public static final btn_music_shuffle:I = 0x7f0b008d

.field public static final btn_music_stop:I = 0x7f0b0088

.field public static final btn_music_voice:I = 0x7f0b0091

.field public static final btn_music_voice_down:I = 0x7f0b0093

.field public static final btn_music_voice_no:I = 0x7f0b0092

.field public static final btn_music_voice_up:I = 0x7f0b0094

.field public static final btn_popup_no:I = 0x7f0b00a7

.field public static final btn_popup_yes:I = 0x7f0b00a6

.field public static final btn_search_lrc:I = 0x7f0b008f

.field public static final btn_update:I = 0x7f0b005b

.field public static final buttomitem0:I = 0x7f0b00de

.field public static final buttomitem1:I = 0x7f0b00e0

.field public static final buttomitem10:I = 0x7f0b00f5

.field public static final buttomitem11:I = 0x7f0b00eb

.field public static final buttomitem12:I = 0x7f0b00ec

.field public static final buttomitem13:I = 0x7f0b00e5

.field public static final buttomitem2:I = 0x7f0b00e2

.field public static final buttomitem3:I = 0x7f0b00e4

.field public static final buttomitem4:I = 0x7f0b00e7

.field public static final buttomitem5:I = 0x7f0b00e9

.field public static final buttomitem6:I = 0x7f0b00ed

.field public static final buttomitem7:I = 0x7f0b00ef

.field public static final buttomitem8:I = 0x7f0b00f1

.field public static final buttomitem9:I = 0x7f0b00f3

.field public static final buttomitems:I = 0x7f0b0115

.field public static final buttoms_set:I = 0x7f0b00dc

.field public static final cancle_btn:I = 0x7f0b00b2

.field public static final control_timer_current:I = 0x7f0b0112

.field public static final control_timer_total:I = 0x7f0b0114

.field public static final disk_name:I = 0x7f0b0004

.field public static final file_disk_list_gridView:I = 0x7f0b0015

.field public static final file_layout:I = 0x7f0b0017

.field public static final file_list_path:I = 0x7f0b0018

.field public static final filegrid:I = 0x7f0b00d2

.field public static final filelist:I = 0x7f0b00d1

.field public static final fl_item_template:I = 0x7f0b0025

.field public static final gridview:I = 0x7f0b0029

.field public static final hosticon:I = 0x7f0b002a

.field public static final hostname:I = 0x7f0b002b

.field public static final hs_music:I = 0x7f0b005e

.field public static final ibAudio:I = 0x7f0b004f

.field public static final ibFileManager:I = 0x7f0b0054

.field public static final ibImage:I = 0x7f0b0053

.field public static final ibShare:I = 0x7f0b0055

.field public static final ibVideo:I = 0x7f0b0050

.field public static final ibWidget:I = 0x7f0b0056

.field public static final imageSurfaceView:I = 0x7f0b0102

.field public static final imageView:I = 0x7f0b00d8

.field public static final img_file_delete:I = 0x7f0b001d

.field public static final img_file_delete_all:I = 0x7f0b0020

.field public static final img_file_disk_manager:I = 0x7f0b0012

.field public static final img_file_manager:I = 0x7f0b001a

.field public static final img_file_rename:I = 0x7f0b0023

.field public static final img_file_sort:I = 0x7f0b0013

.field public static final img_item_delete_icon:I = 0x7f0b0027

.field public static final img_item_icon:I = 0x7f0b0042

.field public static final img_item_rename_icon:I = 0x7f0b0043

.field public static final img_left_down:I = 0x7f0b0039

.field public static final img_left_up:I = 0x7f0b0038

.field public static final img_play_item_icon:I = 0x7f0b00a0

.field public static final img_playbar_icon:I = 0x7f0b0096

.field public static final img_popup_icon:I = 0x7f0b00a3

.field public static final img_right_down:I = 0x7f0b003c

.field public static final img_right_up:I = 0x7f0b003b

.field public static final img_sort:I = 0x7f0b005c

.field public static final inc_music_list:I = 0x7f0b0062

.field public static final item_image:I = 0x7f0b0041

.field public static final item_text:I = 0x7f0b00c6

.field public static final layout1:I = 0x7f0b002c

.field public static final layout_gap:I = 0x7f0b00dd

.field public static final list_big_style:I = 0x7f0b00b5

.field public static final list_btn:I = 0x7f0b0019

.field public static final list_gridView:I = 0x7f0b0036

.field public static final list_small_style:I = 0x7f0b00b4

.field public static final ll_file_linearlayout_1:I = 0x7f0b001c

.field public static final ll_file_linearlayout_2:I = 0x7f0b001f

.field public static final ll_file_linearlayout_3:I = 0x7f0b0022

.field public static final ll_list_left:I = 0x7f0b0037

.field public static final ll_list_right:I = 0x7f0b003a

.field public static final lrc_cancle:I = 0x7f0b004b

.field public static final lrc_index:I = 0x7f0b0046

.field public static final lrc_listview:I = 0x7f0b0045

.field public static final lrc_load:I = 0x7f0b004c

.field public static final lrc_no_found:I = 0x7f0b00bb

.field public static final lrc_search:I = 0x7f0b00b9

.field public static final lrc_search_layout:I = 0x7f0b00b6

.field public static final lrc_searching:I = 0x7f0b00ba

.field public static final lrc_singer:I = 0x7f0b0048

.field public static final lrc_singer_edit:I = 0x7f0b0049

.field public static final lrc_song:I = 0x7f0b0047

.field public static final lrc_song_edit:I = 0x7f0b004a

.field public static final main_window:I = 0x7f0b010f

.field public static final mini_lrc_txt:I = 0x7f0b0061

.field public static final modules_list_item_info:I = 0x7f0b005d

.field public static final music_gridView:I = 0x7f0b005f

.field public static final music_list_gridView:I = 0x7f0b0074

.field public static final music_list_layout:I = 0x7f0b0063

.field public static final music_listitem_linearlayout:I = 0x7f0b0076

.field public static final music_listitemcontent:I = 0x7f0b0079

.field public static final music_listitemname:I = 0x7f0b0078

.field public static final music_listview:I = 0x7f0b0066

.field public static final music_lrctext:I = 0x7f0b0067

.field public static final music_num:I = 0x7f0b0077

.field public static final music_relativelayout:I = 0x7f0b0065

.field public static final optgallery:I = 0x7f0b00c7

.field public static final opthistory:I = 0x7f0b00ca

.field public static final optmenu:I = 0x7f0b00cd

.field public static final optmultfile:I = 0x7f0b00cb

.field public static final optrefresh:I = 0x7f0b00cc

.field public static final opttag:I = 0x7f0b00c8

.field public static final optup:I = 0x7f0b00c9

.field public static final page1:I = 0x7f0b0031

.field public static final page2:I = 0x7f0b0032

.field public static final page3:I = 0x7f0b0033

.field public static final page_linearlayout:I = 0x7f0b002e

.field public static final photo_title:I = 0x7f0b009a

.field public static final photoitem:I = 0x7f0b0099

.field public static final pic_main:I = 0x7f0b009b

.field public static final pic_show_gallery:I = 0x7f0b00f8

.field public static final picitem:I = 0x7f0b0026

.field public static final playbar_time:I = 0x7f0b0069

.field public static final playbar_time_all:I = 0x7f0b006b

.field public static final progress:I = 0x7f0b0113

.field public static final queue_list_artistname:I = 0x7f0b00fd

.field public static final queue_list_songduration:I = 0x7f0b00fb

.field public static final queue_list_songname:I = 0x7f0b00fc

.field public static final rb_sort_by_default:I = 0x7f0b0101

.field public static final rb_sort_by_filename:I = 0x7f0b00fe

.field public static final rb_sort_by_filesize:I = 0x7f0b00ff

.field public static final rb_sort_by_filetype:I = 0x7f0b0100

.field public static final relativeLayout1:I = 0x7f0b009c

.field public static final rg_sort_btns:I = 0x7f0b00b3

.field public static final samba_cancel:I = 0x7f0b00ad

.field public static final samba_choose_name:I = 0x7f0b00a9

.field public static final samba_icon:I = 0x7f0b00ae

.field public static final samba_ip:I = 0x7f0b00af

.field public static final samba_login:I = 0x7f0b00ac

.field public static final samba_user_name:I = 0x7f0b00aa

.field public static final samba_user_pass:I = 0x7f0b00ab

.field public static final scale_btn:I = 0x7f0b00db

.field public static final search_btn:I = 0x7f0b00b1

.field public static final seek_bar:I = 0x7f0b006a

.field public static final seekbar:I = 0x7f0b007c

.field public static final selectedbtn:I = 0x7f0b00d0

.field public static final selectededit:I = 0x7f0b00cf

.field public static final selectedlayout:I = 0x7f0b00ce

.field public static final setting_choose:I = 0x7f0b0124

.field public static final setting_img:I = 0x7f0b0121

.field public static final setting_img2:I = 0x7f0b0123

.field public static final setting_img3:I = 0x7f0b0125

.field public static final setting_text:I = 0x7f0b0122

.field public static final showview:I = 0x7f0b002d

.field public static final singer_name_edit:I = 0x7f0b00b8

.field public static final song_name_edit:I = 0x7f0b00b7

.field public static final songlist_item_song_duration:I = 0x7f0b00f9

.field public static final songlist_item_song_name:I = 0x7f0b00fa

.field public static final sort_btn:I = 0x7f0b009e

.field public static final state_image:I = 0x7f0b00da

.field public static final surfaceView4k2k:I = 0x7f0b00d9

.field public static final switch_image:I = 0x7f0b00f7

.field public static final textView1:I = 0x7f0b009d

.field public static final text_sound:I = 0x7f0b011e

.field public static final textitem:I = 0x7f0b0028

.field public static final textview:I = 0x7f0b009f

.field public static final textview1:I = 0x7f0b00d7

.field public static final textview_item0:I = 0x7f0b00df

.field public static final textview_item1:I = 0x7f0b00e1

.field public static final textview_item10:I = 0x7f0b00f6

.field public static final textview_item2:I = 0x7f0b00e3

.field public static final textview_item3:I = 0x7f0b00e6

.field public static final textview_item4:I = 0x7f0b00e8

.field public static final textview_item5:I = 0x7f0b00ea

.field public static final textview_item6:I = 0x7f0b00ee

.field public static final textview_item7:I = 0x7f0b00f0

.field public static final textview_item8:I = 0x7f0b00f2

.field public static final textview_item9:I = 0x7f0b00f4

.field public static final titil_layout:I = 0x7f0b00d6

.field public static final tv_file_delete:I = 0x7f0b001e

.field public static final tv_file_delete_all:I = 0x7f0b0021

.field public static final tv_file_disk_info:I = 0x7f0b0016

.field public static final tv_file_info:I = 0x7f0b001b

.field public static final tv_file_rename:I = 0x7f0b0024

.field public static final tv_item_name:I = 0x7f0b0044

.field public static final tv_lrcText:I = 0x7f0b0060

.field public static final tv_menu_delete_info:I = 0x7f0b0057

.field public static final tv_music_info:I = 0x7f0b007a

.field public static final tv_music_list_info:I = 0x7f0b0075

.field public static final tv_page_info:I = 0x7f0b0035

.field public static final tv_page_info_music:I = 0x7f0b003f

.field public static final tv_play_item_artist:I = 0x7f0b00a2

.field public static final tv_play_item_name:I = 0x7f0b00a1

.field public static final tv_playbar_artist:I = 0x7f0b0098

.field public static final tv_playbar_songname:I = 0x7f0b0097

.field public static final tv_playbar_time:I = 0x7f0b007b

.field public static final tv_playbar_time_all:I = 0x7f0b007d

.field public static final tv_popup_content:I = 0x7f0b00a5

.field public static final tv_popup_title:I = 0x7f0b00a4

.field public static final tv_version_num:I = 0x7f0b0014

.field public static final tv_voice_count:I = 0x7f0b0095

.field public static final txt_load_lrc:I = 0x7f0b0090

.field public static final txt_lrc:I = 0x7f0b0080

.field public static final txt_next:I = 0x7f0b0087

.field public static final txt_play:I = 0x7f0b0085

.field public static final txt_play_order:I = 0x7f0b008e

.field public static final txt_pre:I = 0x7f0b0082

.field public static final txt_stop:I = 0x7f0b0089

.field public static final update_btn:I = 0x7f0b00a8

.field public static final usbs_list:I = 0x7f0b0003

.field public static final version:I = 0x7f0b0002

.field public static final version_text:I = 0x7f0b00b0

.field public static final video_galleryshow:I = 0x7f0b0116

.field public static final video_gridview:I = 0x7f0b010e

.field public static final video_list_display:I = 0x7f0b0120

.field public static final video_list_gridviews:I = 0x7f0b010d

.field public static final video_list_img:I = 0x7f0b010a

.field public static final video_list_title:I = 0x7f0b010b

.field public static final video_list_view:I = 0x7f0b010c

.field public static final video_name_display:I = 0x7f0b011f

.field public static final video_next:I = 0x7f0b0118

.field public static final video_play:I = 0x7f0b0110

.field public static final video_pre:I = 0x7f0b0117

.field public static final video_setting:I = 0x7f0b011a

.field public static final video_stop:I = 0x7f0b0119

.field public static final video_suspension_layout:I = 0x7f0b0111

.field public static final video_volume:I = 0x7f0b011b

.field public static final video_volume_dec:I = 0x7f0b011c

.field public static final video_volume_inc:I = 0x7f0b011d

.field public static final viewFlipper:I = 0x7f0b0030

.field public static final which_module_txt:I = 0x7f0b005a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
