.class public Lcom/konka/mm/service/MediaPrepareService;
.super Landroid/app/Service;
.source "MediaPrepareService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;
    }
.end annotation


# static fields
.field public static final PREPARE_READY:Ljava/lang/String; = "com.mstar.mediaprepare.prepareready"

.field private static final TAG:Ljava/lang/String; = "MediaPrepareService"


# instance fields
.field private audioChanged:Ljava/lang/Boolean;

.field private imageChanged:Ljava/lang/Boolean;

.field private imagesObserver:Landroid/database/ContentObserver;

.field private volatile mServiceHandler:Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private videoChanged:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/service/MediaPrepareService;->imageChanged:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/service/MediaPrepareService;->audioChanged:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/service/MediaPrepareService;->videoChanged:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/mm/service/MediaPrepareService$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/konka/mm/service/MediaPrepareService$1;-><init>(Lcom/konka/mm/service/MediaPrepareService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/konka/mm/service/MediaPrepareService;->imagesObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/service/MediaPrepareService;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/service/MediaPrepareService;->imageChanged:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/service/MediaPrepareService;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/service/MediaPrepareService;->imageChanged:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    const/4 v5, 0x1

    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/konka/mm/service/MediaPrepareService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v2, "MediaPrepareService"

    invoke-virtual {v0, v5, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/service/MediaPrepareService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    new-instance v1, Ljava/lang/Thread;

    const/4 v2, 0x0

    const-string v3, "MediaPrepareService"

    invoke-direct {v1, v2, p0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    invoke-virtual {p0}, Lcom/konka/mm/service/MediaPrepareService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/konka/mm/service/MediaPrepareService;->imagesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-static {}, Lcom/konka/mm/data/DataGB;->getInstance()Lcom/konka/mm/data/DataGB;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/data/DataGB;->create()V

    return-void
.end method

.method public run()V
    .locals 0

    return-void
.end method
