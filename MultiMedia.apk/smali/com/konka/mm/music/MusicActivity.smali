.class public Lcom/konka/mm/music/MusicActivity;
.super Landroid/app/Activity;
.source "MusicActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/konka/mm/IActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/music/MusicActivity$EthStateReceiver;,
        Lcom/konka/mm/music/MusicActivity$LrcHolder;,
        Lcom/konka/mm/music/MusicActivity$LrcListAdapter;
    }
.end annotation


# static fields
.field public static final ALBUM_GET_IMAGE:I = 0x3

.field private static final CYCLE:I = 0x2

.field public static final LRC_GET_CONTENT:I = 0x2

.field public static final LRC_LIST_CHANGE:I = 0x1

.field public static final LRC_WRITE_FAILED:I = 0x4

.field public static final MUSIC_CHANGER_FLAG:I = 0x21

.field public static final NETWORK_CONNECTED_FAILED:I = 0x5

.field public static final PAGE_SIZE:F = 12.0f

.field private static final REPEAT:I = 0x1

.field private static final SEQUENCE:I = 0x0

.field private static final SHUFFLE:I = 0x3

.field private static final SWITCHINPUTSOURCEMSG:Ljava/lang/String; = "com.konka.service.switchinputsourcemsg"

.field public static final TAG:Ljava/lang/String; = "MusicActivity"

.field private static final perPageCount:F = 12.0f

.field protected static sourceFileComeFrom:Ljava/lang/String;


# instance fields
.field private IsMusicNotSurport:Z

.field private NotFormatToast:Landroid/widget/Toast;

.field protected PageCount:I

.field private abroadCustomer:Ljava/lang/String;

.field public albumHandler:Landroid/os/Handler;

.field albumthumbHandler:Landroid/os/Handler;

.field private artist:Landroid/widget/TextView;

.field private bTeacCustomer:Z

.field private clickListener:Landroid/view/View$OnClickListener;

.field private completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private curIndex:I

.field private curPage:I

.field private curPlay:I

.field private curScreen:I

.field private currentIp:Ljava/lang/String;

.field protected currentPage:I

.field private cycleBtn:Landroid/widget/Button;

.field private dm:Landroid/util/DisplayMetrics;

.field private errorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private fristSongToast:Landroid/widget/Toast;

.field private gridOnKey:Landroid/view/View$OnKeyListener;

.field public handler:Landroid/os/Handler;

.field private img_playbar_icon:Landroid/widget/ImageView;

.field private intentFilter:Landroid/content/IntentFilter;

.field private is3dKeyDown:Z

.field private isChangePlayBtn:Z

.field private isEnterOtherAct:Z

.field private isFocusInBtns:Z

.field public isFocuseBtns:Z

.field private isLrc:Z

.field private isNeedKillServiceWhenQuit:Z

.field private isPlay:Z

.field private isPlayStop:Z

.field private isRegistReceiver:Z

.field private isdispatchKeyDown:Z

.field private isfocusInGrid:Z

.field private lastSongToast:Landroid/widget/Toast;

.field private listBtn:Landroid/widget/Button;

.field private listView:Landroid/view/View;

.field private lrcBtn:Landroid/widget/Button;

.field private lrcHandler:Landroid/os/Handler;

.field lrc_cancle_btn:Landroid/widget/Button;

.field lrc_search_btn:Landroid/widget/Button;

.field lrc_search_layout:Landroid/view/View;

.field public lyricView:Lcom/konka/mm/music/LyricView;

.field private mAlbumArtImporter:Lcom/konka/mm/music/AlbumArtImporter;

.field mEdit_lrc_singer:Landroid/widget/EditText;

.field mEdit_lrc_song:Landroid/widget/EditText;

.field private mEthStateReceiver:Lcom/konka/mm/music/MusicActivity$EthStateReceiver;

.field mLoadLrcListView:Landroid/widget/ListView;

.field mLrcAdapter:Lcom/konka/mm/music/MusicActivity$LrcListAdapter;

.field mLrcList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/music/SearchResult;",
            ">;"
        }
    .end annotation
.end field

.field mLrcSearchDlg:Landroid/app/Dialog;

.field private mPushLeftInAnim:Landroid/view/animation/Animation;

.field private mPushLeftOutAnim:Landroid/view/animation/Animation;

.field private mPustRightInAnim:Landroid/view/animation/Animation;

.field private mPustRightOutAnim:Landroid/view/animation/Animation;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRootPath:Ljava/lang/String;

.field public mSearchDlg:Landroid/app/ProgressDialog;

.field mTxtNofoundLrc:Landroid/widget/TextView;

.field private m_SmartControl:Lcom/konka/mm/receivers/SmartControl;

.field private miniLrcTxt:Landroid/widget/TextView;

.field private musicListTxt:Landroid/widget/TextView;

.field public musicPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private musicPlayService:Lcom/konka/mm/services/MusicPlayService;

.field private musicSize:I

.field private music_list_layout:Landroid/widget/LinearLayout;

.field private muteFlag:Z

.field n:I

.field private nextBtn:Landroid/widget/Button;

.field private nextPage:Landroid/widget/ImageButton;

.field private nextTxt:Landroid/widget/TextView;

.field private notifyPageChange:Ljava/lang/Runnable;

.field private numColumns:I

.field private onFocusChange:Landroid/view/View$OnFocusChangeListener;

.field private pageCount:I

.field private pageGridView:[Landroid/widget/GridView;

.field private pageHandler:Landroid/os/Handler;

.field private pageInfomation:Landroid/widget/TextView;

.field private pageLeft:Landroid/widget/Button;

.field private pageRight:Landroid/widget/Button;

.field private pauseBtn:Landroid/widget/Button;

.field private perColumnWidth:I

.field public pingNetRunnable:Ljava/lang/Runnable;

.field private playBtn:Landroid/widget/Button;

.field private playPauseTxt:Landroid/widget/TextView;

.field private playTiem:Landroid/widget/TextView;

.field private player:Landroid/media/MediaPlayer;

.field private preBtn:Landroid/widget/Button;

.field private prePage:Landroid/widget/ImageButton;

.field private preTxt:Landroid/widget/TextView;

.field private proDlg:Landroid/app/ProgressDialog;

.field private progressSpeed:I

.field private repeatBtn:Landroid/widget/Button;

.field private result:Lcom/konka/mm/music/SearchResult;

.field private screenHeight:I

.field private screenHeight_1080p:I

.field private screenHeight_720p:I

.field private screenWidth:I

.field private screenWidth_1080p:I

.field private screenWidth_720p:I

.field private searchLrcBtn:Landroid/widget/Button;

.field private searchLrcTxt:Landroid/widget/TextView;

.field private seekBar:Landroid/widget/SeekBar;

.field private seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private seekbarOnKey:Landroid/view/View$OnKeyListener;

.field private sequenceBtn:Landroid/widget/Button;

.field private sequenceTxt:Landroid/widget/TextView;

.field private service:Landroid/content/Intent;

.field private shuffleBtn:Landroid/widget/Button;

.field private songname:Landroid/widget/TextView;

.field private sortType:I

.field startX:F

.field private stopBtn:Landroid/widget/Button;

.field private stopTxt:Landroid/widget/TextView;

.field private time:Landroid/widget/TextView;

.field private toast:Landroid/widget/Toast;

.field private toast_file_not_exist:Landroid/widget/Toast;

.field private viewFilpper:Landroid/widget/ViewFlipper;

.field private voice:Landroid/widget/Button;

.field private voiceCount:Landroid/widget/TextView;

.field private voiceDown:Landroid/widget/Button;

.field private voiceNo:Landroid/widget/Button;

.field private voiceTxt:Landroid/widget/TextView;

.field private voiceUp:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x500

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-array v0, v4, [Landroid/widget/GridView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isfocusInGrid:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isFocusInBtns:Z

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isChangePlayBtn:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isdispatchKeyDown:Z

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->mSearchDlg:Landroid/app/ProgressDialog;

    const/16 v0, 0x3a98

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isFocuseBtns:Z

    iput v5, p0, Lcom/konka/mm/music/MusicActivity;->screenWidth_720p:I

    const/16 v0, 0x2d0

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->screenHeight_720p:I

    const/16 v0, 0x780

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->screenWidth_1080p:I

    iput v5, p0, Lcom/konka/mm/music/MusicActivity;->screenHeight_1080p:I

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isLrc:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->muteFlag:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->IsMusicNotSurport:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isNeedKillServiceWhenQuit:Z

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->m_SmartControl:Lcom/konka/mm/receivers/SmartControl;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$1;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$2;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$3;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->albumHandler:Landroid/os/Handler;

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->intentFilter:Landroid/content/IntentFilter;

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;

    const/16 v0, 0x1c2

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->perColumnWidth:I

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->mRootPath:Ljava/lang/String;

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->musicSize:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->pageCount:I

    iput v4, p0, Lcom/konka/mm/music/MusicActivity;->numColumns:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPage:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->sortType:I

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isRegistReceiver:Z

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->is3dKeyDown:Z

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->n:I

    new-instance v0, Lcom/konka/mm/music/MusicActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$4;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$5;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->albumthumbHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$6;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$7;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$8;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$8;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$9;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$9;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$10;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$10;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->notifyPageChange:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$11;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$11;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->seekbarOnKey:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$12;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$12;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$13;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$13;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/music/MusicActivity$14;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicActivity$14;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pingNetRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/music/MusicActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/music/MusicActivity;Landroid/app/ProgressDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$10(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/SearchResult;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->result:Lcom/konka/mm/music/SearchResult;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/AlbumArtImporter;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mAlbumArtImporter:Lcom/konka/mm/music/AlbumArtImporter;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/mm/music/MusicActivity;Lcom/konka/mm/music/SearchResult;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity;->result:Lcom/konka/mm/music/SearchResult;

    return-void
.end method

.method static synthetic access$13(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    return-void
.end method

.method static synthetic access$14(Lcom/konka/mm/music/MusicActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    return v0
.end method

.method static synthetic access$15(Lcom/konka/mm/music/MusicActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/music/MusicActivity;->initBarInfo(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$16(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/music/MusicActivity;Landroid/media/MediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;

    return-void
.end method

.method static synthetic access$20(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$21(Lcom/konka/mm/music/MusicActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    return-void
.end method

.method static synthetic access$22(Lcom/konka/mm/music/MusicActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicActivity;->isLrc:Z

    return-void
.end method

.method static synthetic access$23(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->music_list_layout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$24(Lcom/konka/mm/music/MusicActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$25(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$26(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$27(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$28(Lcom/konka/mm/music/MusicActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$29(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->img_playbar_icon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$30(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNextMode()V

    return-void
.end method

.method static synthetic access$31(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->NotFormatToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$32(Lcom/konka/mm/music/MusicActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/music/MusicActivity;->isdispatchKeyDown:Z

    return v0
.end method

.method static synthetic access$33(Lcom/konka/mm/music/MusicActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicActivity;->isChangePlayBtn:Z

    return-void
.end method

.method static synthetic access$34(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$35(Lcom/konka/mm/music/MusicActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicActivity;->isFocusInBtns:Z

    return-void
.end method

.method static synthetic access$36(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$37(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$38(Lcom/konka/mm/music/MusicActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    return v0
.end method

.method static synthetic access$39(Lcom/konka/mm/music/MusicActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    return v0
.end method

.method static synthetic access$4(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$40(Lcom/konka/mm/music/MusicActivity;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$41(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V

    return-void
.end method

.method static synthetic access$43(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->preTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$44(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->nextTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$45(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->stopTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$46(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$47(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$48(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageInfomation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$49(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageLeft:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$50(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageRight:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$51(Lcom/konka/mm/music/MusicActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/music/MusicActivity;->onReceiveSdCardBroadCast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$52(Lcom/konka/mm/music/MusicActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    return-void
.end method

.method static synthetic access$53(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$54(Lcom/konka/mm/music/MusicActivity;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/music/MusicActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/music/MusicActivity;->IsMusicNotSurport:Z

    return v0
.end method

.method static synthetic access$7(Lcom/konka/mm/music/MusicActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicActivity;->IsMusicNotSurport:Z

    return-void
.end method

.method static synthetic access$8(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->time:Landroid/widget/TextView;

    return-object v0
.end method

.method private dismissMuteFlag()V
    .locals 3

    const-string v1, "MusicActivity"

    const-string v2, "dissmissMute"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.tv.hotkey.UNMUTE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private findView()V
    .locals 2

    const/16 v1, 0x8

    const v0, 0x7f0b0081    # com.konka.mm.R.id.btn_music_pre

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->preBtn:Landroid/widget/Button;

    const v0, 0x7f0b0083    # com.konka.mm.R.id.btn_music_play

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    const v0, 0x7f0b0084    # com.konka.mm.R.id.btn_music_pause

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    const v0, 0x7f0b0086    # com.konka.mm.R.id.btn_music_next

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->nextBtn:Landroid/widget/Button;

    const v0, 0x7f0b0088    # com.konka.mm.R.id.btn_music_stop

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->stopBtn:Landroid/widget/Button;

    const v0, 0x7f0b007e    # com.konka.mm.R.id.btn_music_list

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;

    const v0, 0x7f0b007f    # com.konka.mm.R.id.btn_music_lrc

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    const v0, 0x7f0b0091    # com.konka.mm.R.id.btn_music_voice

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voice:Landroid/widget/Button;

    const v0, 0x7f0b0092    # com.konka.mm.R.id.btn_music_voice_no

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceNo:Landroid/widget/Button;

    const v0, 0x7f0b0094    # com.konka.mm.R.id.btn_music_voice_up

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceUp:Landroid/widget/Button;

    const v0, 0x7f0b0093    # com.konka.mm.R.id.btn_music_voice_down

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceDown:Landroid/widget/Button;

    const v0, 0x7f0b008b    # com.konka.mm.R.id.btn_music_repeat_one

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    const v0, 0x7f0b008a    # com.konka.mm.R.id.btn_music_sequence

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;

    const v0, 0x7f0b008c    # com.konka.mm.R.id.btn_music_cycle

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;

    const v0, 0x7f0b008d    # com.konka.mm.R.id.btn_music_shuffle

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;

    const v0, 0x7f0b0097    # com.konka.mm.R.id.tv_playbar_songname

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->songname:Landroid/widget/TextView;

    const v0, 0x7f0b0096    # com.konka.mm.R.id.img_playbar_icon

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->img_playbar_icon:Landroid/widget/ImageView;

    const v0, 0x7f0b0098    # com.konka.mm.R.id.tv_playbar_artist

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->artist:Landroid/widget/TextView;

    const v0, 0x7f0b008f    # com.konka.mm.R.id.btn_search_lrc

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->initTextViews()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->preBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->nextBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->stopBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceUp:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceDown:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voice:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceNo:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageLeft:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageRight:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsOnFocusListeners()V

    return-void
.end method

.method private findViews()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v0, 0x7f0b0030    # com.konka.mm.R.id.viewFlipper

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0031    # com.konka.mm.R.id.page1

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0032    # com.konka.mm.R.id.page2

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v4

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0033    # com.konka.mm.R.id.page3

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v5

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v4

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v5

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f040005    # com.konka.mm.R.anim.photo_push_left_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    const v0, 0x7f040009    # com.konka.mm.R.anim.push_left_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    const v0, 0x7f040006    # com.konka.mm.R.anim.photo_push_right_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    const v0, 0x7f04000a    # com.konka.mm.R.anim.push_right_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    const v0, 0x7f0b007c    # com.konka.mm.R.id.seekbar

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    const v0, 0x7f0b007d    # com.konka.mm.R.id.tv_playbar_time_all

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->time:Landroid/widget/TextView;

    const v0, 0x7f0b007b    # com.konka.mm.R.id.tv_playbar_time

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;

    const v0, 0x7f0b0060    # com.konka.mm.R.id.tv_lrcText

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/music/LyricView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    const v0, 0x7f0b0063    # com.konka.mm.R.id.music_list_layout

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->music_list_layout:Landroid/widget/LinearLayout;

    const v0, 0x7f0b0095    # com.konka.mm.R.id.tv_voice_count

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceCount:Landroid/widget/TextView;

    const v0, 0x7f0b0062    # com.konka.mm.R.id.inc_music_list

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listView:Landroid/view/View;

    const v0, 0x7f0b003d    # com.konka.mm.R.id.btn_list_left_music

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageLeft:Landroid/widget/Button;

    const v0, 0x7f0b003e    # com.konka.mm.R.id.btn_list_right_music

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageRight:Landroid/widget/Button;

    const v0, 0x7f0b003f    # com.konka.mm.R.id.tv_page_info_music

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageInfomation:Landroid/widget/TextView;

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mSearchDlg:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mSearchDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007c    # com.konka.mm.R.string.lrc_searching

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->findView()V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->initMyView()V

    return-void
.end method

.method private getIndexFromList(Ljava/util/List;Lcom/konka/mm/model/MusicInfo;)I
    .locals 5
    .param p2    # Lcom/konka/mm/model/MusicInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/MusicInfo;",
            ">;",
            "Lcom/konka/mm/model/MusicInfo;",
            ")I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/MusicInfo;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Lcom/konka/mm/model/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/konka/mm/model/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0
.end method

.method private getInfosFromFiles([Ljava/io/File;)Ljava/util/List;
    .locals 6
    .param p1    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/MusicInfo;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_1

    :cond_0
    return-object v2

    :cond_1
    aget-object v0, p1, v3

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/konka/mm/tools/MusicTool;->getInfoFromSDByName(Landroid/app/Activity;Ljava/lang/String;)Lcom/konka/mm/model/MusicInfo;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private getIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->intentFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->intentFilter:Landroid/content/IntentFilter;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "konka.mm.lrc.action"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->intentFilter:Landroid/content/IntentFilter;

    return-object v0
.end method

.method private init()V
    .locals 3

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->findViews()V

    new-instance v1, Lcom/konka/mm/services/MusicPlayService;

    invoke-direct {v1}, Lcom/konka/mm/services/MusicPlayService;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPlayService:Lcom/konka/mm/services/MusicPlayService;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPlayService:Lcom/konka/mm/services/MusicPlayService;

    invoke-virtual {v1}, Lcom/konka/mm/services/MusicPlayService;->getMediaPlayer()Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekbarOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032    # com.konka.mm.R.string.MM_CONPUTER_UNCONNECT

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->toast:Landroid/widget/Toast;

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->dm:Landroid/util/DisplayMetrics;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->dm:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->dm:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->screenWidth:I

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->dm:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->screenHeight:I

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->broweToRoot()V

    sget-object v1, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.konka.service.switchinputsourcemsg"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v1, Lcom/konka/mm/music/MusicActivity$15;

    invoke-direct {v1, p0}, Lcom/konka/mm/music/MusicActivity$15;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/mm/music/MusicActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    sget-object v1, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->registEthernetReceiver()V

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pingNetRunnable:Ljava/lang/Runnable;

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLoadLrcListView:Landroid/widget/ListView;

    new-instance v2, Lcom/konka/mm/music/MusicActivity$16;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicActivity$16;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private initBarInfo(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->songname:Landroid/widget/TextView;

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->artist:Landroid/widget/TextView;

    const-string v2, "unknown"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/mm/music/MusicActivity$17;

    invoke-direct {v2, p0, v0}, Lcom/konka/mm/music/MusicActivity$17;-><init>(Lcom/konka/mm/music/MusicActivity;Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->img_playbar_icon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02007f    # com.konka.mm.R.drawable.music_icon_small

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->songname:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->artist:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private initSeekBar()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private initTextViews()V
    .locals 1

    const v0, 0x7f0b0080    # com.konka.mm.R.id.txt_lrc

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;

    const v0, 0x7f0b0082    # com.konka.mm.R.id.txt_pre

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->preTxt:Landroid/widget/TextView;

    const v0, 0x7f0b0085    # com.konka.mm.R.id.txt_play

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;

    const v0, 0x7f0b0087    # com.konka.mm.R.id.txt_next

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->nextTxt:Landroid/widget/TextView;

    const v0, 0x7f0b0089    # com.konka.mm.R.id.txt_stop

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->stopTxt:Landroid/widget/TextView;

    const v0, 0x7f0b008e    # com.konka.mm.R.id.txt_play_order

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;

    const v0, 0x7f0b0090    # com.konka.mm.R.id.txt_load_lrc

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcTxt:Landroid/widget/TextView;

    const v0, 0x7f0b0061    # com.konka.mm.R.id.mini_lrc_txt

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->miniLrcTxt:Landroid/widget/TextView;

    return-void
.end method

.method private intiGridView()V
    .locals 9

    const/4 v6, 0x0

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-nez v7, :cond_1

    const/4 v7, 0x0

    iput v7, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    const/4 v6, 0x0

    :goto_0
    iget-object v7, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-gtz v7, :cond_3

    :cond_0
    return-void

    :cond_1
    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v7, v7, 0x1

    rem-int/lit8 v7, v7, 0xc

    if-nez v7, :cond_2

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v7, v7, 0x1

    div-int/lit8 v7, v7, 0xc

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    const/16 v6, 0xb

    goto :goto_0

    :cond_2
    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v7, v7, 0x1

    div-int/lit8 v7, v7, 0xc

    iput v7, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v7, v7, 0x1

    rem-int/lit8 v7, v7, 0xc

    add-int/lit8 v6, v7, -0x1

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    int-to-float v7, v4

    const/high16 v8, 0x41400000    # 12.0f

    div-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v7, v7

    iput v7, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setPageInfo()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    if-nez v7, :cond_4

    const/4 v1, 0x0

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    const/4 v8, 0x3

    if-lt v7, v8, :cond_7

    const/4 v2, 0x3

    :cond_4
    :goto_1
    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    if-lez v7, :cond_5

    const/4 v1, -0x1

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v7, v7, -0x1

    iget v8, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    if-ne v7, v8, :cond_8

    const/4 v2, 0x2

    :cond_5
    :goto_2
    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    :goto_3
    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    add-int/2addr v7, v2

    if-ge v3, v7, :cond_0

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    add-int/2addr v7, v3

    iget v8, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    sub-int/2addr v7, v8

    add-int v5, v7, v1

    iget v7, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v7, v7, -0x1

    if-ge v5, v7, :cond_9

    const/16 v0, 0xc

    :cond_6
    :goto_4
    iget-object v7, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v8, v3, v1

    add-int/lit8 v8, v8, 0x3

    rem-int/lit8 v8, v8, 0x3

    aget-object v7, v7, v8

    new-instance v8, Lcom/konka/mm/adapters/MusicAdapter;

    invoke-direct {v8, p0, v5, v0}, Lcom/konka/mm/adapters/MusicAdapter;-><init>(Lcom/konka/mm/music/MusicActivity;II)V

    invoke-virtual {v7, v8}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v7, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v7}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setFocusable(Z)V

    iget-object v7, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v7}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    iget-object v7, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget v8, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    aget-object v7, v7, v8

    invoke-virtual {v7, v6}, Landroid/widget/GridView;->setSelection(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    goto :goto_1

    :cond_8
    const/4 v2, 0x3

    goto :goto_2

    :cond_9
    iget-object v7, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    iget v8, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v8, v8, -0x1

    mul-int/lit8 v8, v8, 0xc

    sub-int v0, v7, v8

    const/16 v7, 0xc

    if-lt v0, v7, :cond_6

    const/16 v0, 0xc

    goto :goto_4
.end method

.method private onReceiveSdCardBroadCast(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mRootPath:Ljava/lang/String;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mRootPath:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/konka/mm/tools/FileTool;->checkUsbExist(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->finish()V

    goto :goto_0

    :cond_3
    const-string v1, "com.konka.service.switchinputsourcemsg"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "MusicActivity catch the msg --->com.konka.service.switchinputsourcemsg"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    goto :goto_0
.end method

.method private playMusicService(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/mm/services/MusicPlayService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    const-string v1, "MSG"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    const-string v1, "MUSIC_PATH"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0}, Lcom/konka/mm/services/MusicPlayService;->addActivity(Lcom/konka/mm/music/MusicActivity;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private playNext()V
    .locals 6

    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090010    # com.konka.mm.R.string.file_not_exist

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x3

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/konka/mm/music/MusicActivity;->initBarInfo(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    int-to-float v2, v2

    const/high16 v3, 0x41400000    # 12.0f

    rem-float/2addr v2, v3

    float-to-int v1, v2

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    aget-object v2, v2, v3

    invoke-virtual {v2, v1}, Landroid/widget/GridView;->setSelection(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {p0, v2, v3}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090066    # com.konka.mm.R.string.pauseTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private playNextMode()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-gt v1, v0, :cond_1

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v2, v0, 0x1

    if-lt v1, v2, :cond_0

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->initSeekBar()V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isShown()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v5, v1}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/mm/services/MusicPlayService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    const-string v2, "MSG"

    const/4 v3, 0x7

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0}, Lcom/konka/mm/services/MusicPlayService;->addActivity(Lcom/konka/mm/music/MusicActivity;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090065    # com.konka.mm.R.string.playTxt

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v5, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->listView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->music_list_layout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    invoke-virtual {p0, v1, v2}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063    # com.konka.mm.R.string.lrcTxt

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :pswitch_2
    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-gt v1, v0, :cond_2

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :cond_2
    iput v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    if-nez v0, :cond_3

    iput v4, p0, Lcom/konka/mm/music/MusicActivity;->curPage:I

    :goto_1
    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :cond_3
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private playPos(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    const-string v1, "MSG"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0}, Lcom/konka/mm/services/MusicPlayService;->addActivity(Lcom/konka/mm/music/MusicActivity;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private playPreMode()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-gez v1, :cond_1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    :cond_1
    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-ltz v1, :cond_2

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :cond_2
    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    if-nez v0, :cond_3

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPage:I

    :goto_1
    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    iput v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setBtnsOnFocusListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->preBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->nextBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->stopBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceUp:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceDown:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voice:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->voiceNo:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private setTextViewVisible(Landroid/widget/TextView;Z)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private showPlayBtns(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090069    # com.konka.mm.R.string.repeatTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006a    # com.konka.mm.R.string.cycleTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006b    # com.konka.mm.R.string.shuffleTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private sortMusicList(I[Ljava/io/File;)[Ljava/io/File;
    .locals 1
    .param p1    # I
    .param p2    # [Ljava/io/File;

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    move-object v0, p2

    goto :goto_0

    :pswitch_1
    invoke-static {p2}, Lcom/konka/mm/tools/FileTool;->getSortFilesByName([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p2}, Lcom/konka/mm/tools/FileTool;->getSortFilesBySize([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p2}, Lcom/konka/mm/tools/FileTool;->getSortFilesByType([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public broweToRoot()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/GlobalData;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/music/MusicActivity;->mRootPath:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    const-string v3, "com.konka.mm.file.where.come.from"

    const-string v4, "com.konka.mm.file.come.from.other"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.is.need.kill.music.service.when.quit"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isNeedKillServiceWhenQuit:Z

    sget-object v3, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.konka.mm.samba.current.ip"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/music/MusicActivity;->currentIp:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v2}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    :goto_0
    const-string v3, "playMode"

    invoke-static {p0, v3}, Lcom/konka/mm/tools/SharedPreferencesTool;->getInt(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    const/4 v3, -0x1

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    if-eq v3, v4, :cond_3

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    invoke-direct {p0, v3}, Lcom/konka/mm/music/MusicActivity;->showPlayBtns(I)V

    :goto_1
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/mm/music/MusicActivity;->initBarInfo(Ljava/lang/String;)V

    sget-object v3, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.com.from.camera"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    :goto_2
    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->intiGridView()V

    return-void

    :cond_0
    sget-object v3, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    invoke-virtual {v2}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    goto :goto_0

    :cond_2
    const-string v3, "com.konka.mm.file.all.lists"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iput v5, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    goto :goto_1

    :cond_4
    const/4 v4, 0x2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v4, v3}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public cancelProgressDlg()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public changePalyState(Z)V
    .locals 4
    .param p1    # Z

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsFocuse()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public changeRepeatState(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    const v1, 0x7f02007d    # com.konka.mm.R.drawable.music_icon_single_cycle_s

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    const v1, 0x7f02007e    # com.konka.mm.R.drawable.music_icon_single_cycle_uns

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/music/MusicActivity;->isdispatchKeyDown:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public initMyView()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v1, Landroid/app/Dialog;

    const v2, 0x7f0a0006    # com.konka.mm.R.style.dialog

    invoke-direct {v1, p0, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f03002e    # com.konka.mm.R.layout.search_lrc_layout

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    const v2, 0x7f0b0045    # com.konka.mm.R.id.lrc_listview

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLoadLrcListView:Landroid/widget/ListView;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLrcList:Ljava/util/List;

    new-instance v1, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;

    invoke-direct {v1, p0}, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLrcAdapter:Lcom/konka/mm/music/MusicActivity$LrcListAdapter;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLoadLrcListView:Landroid/widget/ListView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mLoadLrcListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->mLrcAdapter:Lcom/konka/mm/music/MusicActivity$LrcListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    const v2, 0x7f0b00b8    # com.konka.mm.R.id.singer_name_edit

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_singer:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    const v2, 0x7f0b00b7    # com.konka.mm.R.id.song_name_edit

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_song:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    const v2, 0x7f0b004b    # com.konka.mm.R.id.lrc_cancle

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_cancle_btn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    const v2, 0x7f0b00b9    # com.konka.mm.R.id.lrc_search

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_btn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_cancle_btn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_btn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lrc_search_layout:Landroid/view/View;

    const v2, 0x7f0b00bb    # com.konka.mm.R.id.lrc_no_found

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    new-instance v1, Lcom/konka/mm/music/AlbumArtImporter;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v4, v2}, Lcom/konka/mm/music/AlbumArtImporter;-><init>(Lcom/konka/mm/music/MusicActivity;Landroid/os/Handler;Z)V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mAlbumArtImporter:Lcom/konka/mm/music/AlbumArtImporter;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090010    # com.konka.mm.R.string.file_not_exist

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090011    # com.konka.mm.R.string.not_support_song_format

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->NotFormatToast:Landroid/widget/Toast;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007d    # com.konka.mm.R.string.last_song

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lastSongToast:Landroid/widget/Toast;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007e    # com.konka.mm.R.string.frist_song

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->fristSongToast:Landroid/widget/Toast;

    return-void
.end method

.method public isCanPing()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->currentIp:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->currentIp:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method public next()V
    .locals 2

    sget-object v0, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v1, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_1

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lastSongToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03001a    # com.konka.mm.R.layout.music_1280x720

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->init()V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->abroadCustomer:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->abroadCustomer:Ljava/lang/String;

    const-string v2, "teac"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->bTeacCustomer:Z

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setpicGridViewStatus()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isdispatchKeyDown:Z

    new-instance v1, Lcom/konka/mm/receivers/SmartControl;

    sget-object v2, Lcom/konka/mm/receivers/SmartControl$PlayerType;->MUSIC:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-direct {v1, p0, v2}, Lcom/konka/mm/receivers/SmartControl;-><init>(Landroid/content/Context;Lcom/konka/mm/receivers/SmartControl$PlayerType;)V

    iput-object v1, p0, Lcom/konka/mm/music/MusicActivity;->m_SmartControl:Lcom/konka/mm/receivers/SmartControl;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/GlobalData;

    invoke-virtual {v0, p0}, Lcom/konka/mm/GlobalData;->setMa(Lcom/konka/mm/music/MusicActivity;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v1, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    :cond_1
    iget-boolean v0, p0, Lcom/konka/mm/music/MusicActivity;->isNeedKillServiceWhenQuit:Z

    if-eqz v0, :cond_3

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "quit the MusicPlay====================isNeedKillServiceWhenQuit : true"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    :goto_0
    sget-object v0, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v1, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->unregistEthernetReceiver()V

    :cond_2
    return-void

    :cond_3
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "quit the MusicPlay====================isNeedKillServiceWhenQuit : false"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    :try_start_0
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->bTeacCustomer:Z

    if-nez v2, :cond_8

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2

    :sswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsUnFocuse()V

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :sswitch_2
    :try_start_2
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsUnFocuse()V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsFocuse()V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsFocuse()V

    goto :goto_0

    :sswitch_5
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->is3dKeyDown:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    goto :goto_0

    :sswitch_6
    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-ltz v2, :cond_2

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :sswitch_7
    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_4

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :sswitch_8
    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    if-eqz v2, :cond_6

    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_5
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {p0, v2, v3}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {p0, v2, v3}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    goto/16 :goto_0

    :sswitch_9
    const-string v2, "======="

    const-string v3, "REVEAL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    sub-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/konka/mm/music/MusicActivity;->playPos(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_a
    const-string v2, "======="

    const-string v3, "UPDATE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    if-le v2, v3, :cond_7

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNextMode()V

    goto/16 :goto_0

    :cond_7
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    add-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/konka/mm/music/MusicActivity;->playPos(I)V

    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_b
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->finish()V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsFocuse()V

    goto/16 :goto_0

    :sswitch_d
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsUnFocuse()V

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsUnFocuse()V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setBtnsFocuse()V

    goto/16 :goto_0

    :sswitch_10
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->is3dKeyDown:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    goto/16 :goto_0

    :sswitch_11
    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_9
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-ltz v2, :cond_a

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :sswitch_12
    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_b
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-eqz v2, :cond_c

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt v2, v3, :cond_c

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :cond_c
    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto/16 :goto_0

    :sswitch_13
    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    if-eqz v2, :cond_e

    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_d
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {p0, v2, v3}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {p0, v2, v3}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    goto/16 :goto_0

    :sswitch_14
    const-string v2, "======="

    const-string v3, "REVEAL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    sub-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/konka/mm/music/MusicActivity;->playPos(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_15
    const-string v2, "======="

    const-string v3, "UPDATE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    if-le v2, v3, :cond_f

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNextMode()V

    goto/16 :goto_0

    :cond_f
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->progressSpeed:I

    add-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/konka/mm/music/MusicActivity;->playPos(I)V

    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_16
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->finish()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x7 -> :sswitch_0
        0x13 -> :sswitch_4
        0x14 -> :sswitch_3
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x55 -> :sswitch_8
        0x57 -> :sswitch_7
        0x58 -> :sswitch_6
        0xce -> :sswitch_5
        0x106 -> :sswitch_7
        0x12e -> :sswitch_8
        0x12f -> :sswitch_b
        0x130 -> :sswitch_a
        0x131 -> :sswitch_9
        0x133 -> :sswitch_6
        0x205 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x4 -> :sswitch_0
        0x7 -> :sswitch_0
        0x13 -> :sswitch_c
        0x14 -> :sswitch_f
        0x15 -> :sswitch_d
        0x16 -> :sswitch_e
        0x56 -> :sswitch_16
        0x7e -> :sswitch_13
        0xce -> :sswitch_10
        0x12e -> :sswitch_12
        0x12f -> :sswitch_14
        0x132 -> :sswitch_11
        0x133 -> :sswitch_15
        0x205 -> :sswitch_10
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;

    const/16 v6, 0x8

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/GlobalData;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->mRootPath:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    const-string v2, "com.konka.mm.file.where.come.from"

    const-string v3, "com.konka.mm.file.come.from.other"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.is.need.kill.music.service.when.quit"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isNeedKillServiceWhenQuit:Z

    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.konka.mm.samba.current.ip"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->currentIp:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    :goto_0
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/konka/mm/music/MusicActivity;->initBarInfo(Ljava/lang/String;)V

    const/4 v3, 0x2

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->intiGridView()V

    return-void

    :cond_0
    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {v1}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    goto :goto_0

    :cond_2
    const-string v2, "com.konka.mm.file.all.lists"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 5

    const/4 v4, 0x1

    const-string v2, "-----------------------onPause"

    invoke-static {v2}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->is3dKeyDown:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    :cond_0
    iget-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    :cond_1
    const-string v2, "activity"

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "XXX==========>>>MusicTop2ndAct:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Launcher"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "XXX==========>>>StopMusicAndBackToLauncher"

    invoke-static {v2}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    :cond_2
    iput-boolean v4, p0, Lcom/konka/mm/music/MusicActivity;->isEnterOtherAct:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->is3dKeyDown:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "-----------------------OnResume"

    invoke-static {v0}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->m_SmartControl:Lcom/konka/mm/receivers/SmartControl;

    invoke-virtual {v0}, Lcom/konka/mm/receivers/SmartControl;->Start()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    const-string v0, "-----------------------OnStop"

    invoke-static {v0}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->m_SmartControl:Lcom/konka/mm/receivers/SmartControl;

    invoke-virtual {v0}, Lcom/konka/mm/receivers/SmartControl;->End()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/high16 v3, 0x42480000    # 50.0f

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p2}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->startX:F

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->startX:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->startX:F

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->startX:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->startX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public pause()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {p0, v0, v1}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090065    # com.konka.mm.R.string.playTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    return-void
.end method

.method public play()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v1, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlayStop:Z

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {p0, v0, v1}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090066    # com.konka.mm.R.string.pauseTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    goto :goto_0
.end method

.method public prev()V
    .locals 2

    sget-object v0, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v1, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    if-ltz v0, :cond_1

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->fristSongToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curPlay:I

    invoke-direct {p0}, Lcom/konka/mm/music/MusicActivity;->playNext()V

    goto :goto_0
.end method

.method public varargs refresh([Ljava/lang/Object;)V
    .locals 5
    .param p1    # [Ljava/lang/Object;

    const/4 v0, 0x0

    :try_start_0
    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->n:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/mm/music/MusicActivity;->n:I

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iget-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isChangePlayBtn:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicActivity;->isChangePlayBtn:Z

    :cond_0
    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->n:I

    rem-int/lit8 v3, v3, 0xa

    iput v3, p0, Lcom/konka/mm/music/MusicActivity;->n:I

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->n:I

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->time:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    invoke-virtual {v3, v0}, Lcom/konka/mm/music/LyricView;->SetNowPlayIndex(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public registEthernetReceiver()V
    .locals 3

    new-instance v2, Lcom/konka/mm/music/MusicActivity$EthStateReceiver;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicActivity$EthStateReceiver;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iput-object v2, p0, Lcom/konka/mm/music/MusicActivity;->mEthStateReceiver:Lcom/konka/mm/music/MusicActivity$EthStateReceiver;

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->mEthStateReceiver:Lcom/konka/mm/music/MusicActivity$EthStateReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/konka/mm/music/MusicActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 1
    .param p1    # Landroid/widget/Button;
    .param p2    # Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method public setBtnsFocuse()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->nextBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->stopBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->preBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isFocuseBtns:Z

    return-void
.end method

.method public setBtnsUnFocuse()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->nextBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->stopBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->preBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->searchLrcBtn:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicActivity;->isFocuseBtns:Z

    return-void
.end method

.method public setMiniLRCText(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->miniLrcTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPageInfo()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageInfomation:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09009a    # com.konka.mm.R.string.page

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPlayMode(Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 2
    .param p1    # Landroid/widget/Button;
    .param p2    # Landroid/widget/Button;

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    :cond_0
    const-string v0, "playMode"

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curIndex:I

    invoke-static {p0, v0, v1}, Lcom/konka/mm/tools/SharedPreferencesTool;->saveInt(Landroid/app/Activity;Ljava/lang/String;I)V

    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setFocusable(Z)V

    invoke-virtual {p2}, Landroid/widget/Button;->requestFocus()Z

    return-void
.end method

.method public setPreOrNextScreen(I)V
    .locals 5
    .param p1    # I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    add-int/2addr v2, p1

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v1, v2, 0x3

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v1

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->numColumns:I

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setNumColumns(I)V

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_3

    const/16 v0, 0xc

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v1

    new-instance v3, Lcom/konka/mm/adapters/MusicAdapter;

    iget v4, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    add-int/2addr v4, p1

    invoke-direct {v3, p0, v4, v0}, Lcom/konka/mm/adapters/MusicAdapter;-><init>(Lcom/konka/mm/music/MusicActivity;II)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v3, v3, -0x1

    mul-int/lit8 v3, v3, 0xc

    sub-int v0, v2, v3

    const/16 v2, 0xc

    if-lt v0, v2, :cond_2

    const/16 v0, 0xc

    goto :goto_1
.end method

.method public setpicGridViewStatus()V
    .locals 8

    const/16 v7, 0x32

    const/16 v6, 0x28

    const/16 v5, 0xf

    const/4 v4, 0x0

    const/16 v3, 0x46

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->numColumns:I

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->screenWidth:I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->screenWidth_720p:I

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->screenHeight:I

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->screenHeight_720p:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v7, v5, v7, v5}, Landroid/widget/GridView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->perColumnWidth:I

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setColumnWidth(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    const/16 v2, 0x1b

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    const/high16 v2, 0x42780000    # 62.0f

    invoke-virtual {v1, v2}, Lcom/konka/mm/music/LyricView;->setTextHigh(F)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    const/high16 v2, 0x42040000    # 33.0f

    invoke-virtual {v1, v2}, Lcom/konka/mm/music/LyricView;->setTextSize(F)V

    :goto_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    new-instance v2, Lcom/konka/mm/music/MusicActivity$18;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicActivity$18;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    new-instance v2, Lcom/konka/mm/music/MusicActivity$19;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicActivity$19;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    const/16 v2, 0x3c

    invoke-virtual {v1, v3, v6, v2, v4}, Landroid/widget/GridView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/konka/mm/music/MusicActivity;->perColumnWidth:I

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setColumnWidth(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v6}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    const/high16 v2, 0x42a00000    # 80.0f

    invoke-virtual {v1, v2}, Lcom/konka/mm/music/LyricView;->setTextHigh(F)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {v1, v2}, Lcom/konka/mm/music/LyricView;->setTextSize(F)V

    goto :goto_1
.end method

.method public showProgressDialog(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method public snapScreen(I)V
    .locals 4
    .param p1    # I

    const/4 v3, -0x1

    const/4 v2, 0x1

    if-ne p1, v2, :cond_3

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicActivity;->setPreOrNextScreen(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/GridView;->requestFocus()Z

    if-ne p1, v2, :cond_4

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    :goto_2
    invoke-virtual {p0}, Lcom/konka/mm/music/MusicActivity;->setPageInfo()V

    goto :goto_0

    :cond_3
    if-ne p1, v3, :cond_2

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    invoke-virtual {p0, v3}, Lcom/konka/mm/music/MusicActivity;->setPreOrNextScreen(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicActivity;->curScreen:I

    aget-object v0, v0, v1

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_2
.end method

.method public stopMusicService()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->stopService(Landroid/content/Intent;)Z

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicActivity;->isPlay:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->gc()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public unregistEthernetReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mEthStateReceiver:Lcom/konka/mm/music/MusicActivity$EthStateReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity;->mEthStateReceiver:Lcom/konka/mm/music/MusicActivity$EthStateReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
