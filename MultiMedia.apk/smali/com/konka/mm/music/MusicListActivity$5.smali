.class Lcom/konka/mm/music/MusicListActivity$5;
.super Ljava/lang/Object;
.source "MusicListActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicListActivity$5;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    const-string v0, "MusicListActivity"

    const-string v1, "PAGE UP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$5;->this$0:Lcom/konka/mm/music/MusicListActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V

    goto :goto_0

    :pswitch_1
    const-string v0, "MusicListActivity"

    const-string v1, "PAGE DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$5;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->currentPage:I
    invoke-static {v0}, Lcom/konka/mm/music/MusicListActivity;->access$6(Lcom/konka/mm/music/MusicListActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$5;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->pageCount:I
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$7(Lcom/konka/mm/music/MusicListActivity;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$5;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-static {v0, v2}, Lcom/konka/mm/music/MusicListActivity;->access$8(Lcom/konka/mm/music/MusicListActivity;Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$5;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
