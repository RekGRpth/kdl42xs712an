.class Lcom/konka/mm/music/MusicActivity$7;
.super Ljava/lang/Object;
.source "MusicActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    const-string v2, "MusicActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "the song play error! --> "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v0, v0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->curPlay:I
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$14(Lcom/konka/mm/music/MusicActivity;)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v0, v1}, Lcom/konka/mm/music/MusicActivity;->access$7(Lcom/konka/mm/music/MusicActivity;Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->NotFormatToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$31(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "MusicActivity"

    const-string v2, "================player.reset()==============="

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->isdispatchKeyDown:Z
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$32(Lcom/konka/mm/music/MusicActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$7;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
