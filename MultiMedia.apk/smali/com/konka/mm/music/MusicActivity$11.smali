.class Lcom/konka/mm/music/MusicActivity$11;
.super Ljava/lang/Object;
.source "MusicActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private playPos(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$40(Lcom/konka/mm/music/MusicActivity;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$40(Lcom/konka/mm/music/MusicActivity;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "MSG"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$40(Lcom/konka/mm/music/MusicActivity;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v0}, Lcom/konka/mm/services/MusicPlayService;->addActivity(Lcom/konka/mm/music/MusicActivity;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->service:Landroid/content/Intent;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$40(Lcom/konka/mm/music/MusicActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/mm/music/MusicActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$8(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    move v1, v2

    :cond_1
    :goto_1
    return v1

    :sswitch_0
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v3, v2}, Lcom/konka/mm/music/MusicActivity;->access$21(Lcom/konka/mm/music/MusicActivity;Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->finish()V

    goto :goto_1

    :sswitch_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v1, v2}, Lcom/konka/mm/music/MusicActivity;->access$35(Lcom/konka/mm/music/MusicActivity;Z)V

    move v1, v2

    goto :goto_1

    :sswitch_2
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$36(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->isShown()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$36(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$36(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->requestFocus()Z

    :goto_2
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v3, v1}, Lcom/konka/mm/music/MusicActivity;->access$35(Lcom/konka/mm/music/MusicActivity;Z)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$37(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$37(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_2

    :sswitch_3
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->progressSpeed:I
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$38(Lcom/konka/mm/music/MusicActivity;)I

    move-result v2

    sub-int/2addr v0, v2

    if-gtz v0, :cond_3

    const/4 v0, 0x0

    :cond_3
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->isPlay:Z
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$39(Lcom/konka/mm/music/MusicActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v2, v1}, Lcom/konka/mm/music/MusicActivity;->access$33(Lcom/konka/mm/music/MusicActivity;Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$8(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$34(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :sswitch_4
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->progressSpeed:I
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$38(Lcom/konka/mm/music/MusicActivity;)I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$8(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getMax()I

    move-result v4

    if-le v3, v4, :cond_4

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # invokes: Lcom/konka/mm/music/MusicActivity;->playNextMode()V
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$30(Lcom/konka/mm/music/MusicActivity;)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->progressSpeed:I
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$38(Lcom/konka/mm/music/MusicActivity;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->isPlay:Z
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$39(Lcom/konka/mm/music/MusicActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v2, v1}, Lcom/konka/mm/music/MusicActivity;->access$33(Lcom/konka/mm/music/MusicActivity;Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$8(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$11;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playTiem:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$34(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
    .end sparse-switch
.end method
