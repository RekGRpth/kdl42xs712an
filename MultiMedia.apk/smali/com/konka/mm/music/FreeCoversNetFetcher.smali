.class public Lcom/konka/mm/music/FreeCoversNetFetcher;
.super Ljava/lang/Object;
.source "FreeCoversNetFetcher.java"


# instance fields
.field final FREE_COVERS_API_URL:Ljava/lang/String;

.field final TAG:Ljava/lang/String;

.field mSaxParser:Ljavax/xml/parsers/SAXParser;

.field mSaxParserFactory:Ljavax/xml/parsers/SAXParserFactory;

.field mXmlHandler:Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

.field mXmlReader:Lorg/xml/sax/XMLReader;


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "FreeCoversNetFetcher"

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->TAG:Ljava/lang/String;

    const-string v1, "http://www.freecovers.net/api/search/"

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->FREE_COVERS_API_URL:Ljava/lang/String;

    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mSaxParserFactory:Ljavax/xml/parsers/SAXParserFactory;

    iput-object v2, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mSaxParser:Ljavax/xml/parsers/SAXParser;

    iput-object v2, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlReader:Lorg/xml/sax/XMLReader;

    new-instance v1, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

    invoke-direct {v1}, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlHandler:Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mSaxParserFactory:Ljavax/xml/parsers/SAXParserFactory;

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mSaxParser:Ljavax/xml/parsers/SAXParser;

    iget-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mSaxParser:Ljavax/xml/parsers/SAXParser;

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlReader:Lorg/xml/sax/XMLReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public fetch(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    :try_start_0
    new-instance v2, Ljava/net/URL;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "http://www.freecovers.net/api/search/"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "+"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    new-instance v6, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v6}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const/16 v8, 0x1d4c

    invoke-static {v6, v8}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    new-instance v5, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iget-object v8, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlHandler:Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

    invoke-virtual {v8}, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->reset()V

    iget-object v8, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlHandler:Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->setSearchTarget(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlReader:Lorg/xml/sax/XMLReader;

    iget-object v9, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlHandler:Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

    invoke-interface {v8, v9}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    iget-object v8, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlReader:Lorg/xml/sax/XMLReader;

    new-instance v9, Lorg/xml/sax/InputSource;

    invoke-direct {v9, v5}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v8, v9}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    const-string v8, "FreeCoversNetFetcher"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "fetching: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlHandler:Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

    iget-object v10, v10, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/konka/mm/music/FreeCoversNetFetcher;->mXmlHandler:Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;

    iget-object v8, v8, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    invoke-static {v8}, Lcom/konka/mm/music/AlbumArtUtils;->fetchBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    :goto_0
    return-object v8

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v8, 0x0

    goto :goto_0
.end method
