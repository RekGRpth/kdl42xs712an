.class Lcom/konka/mm/music/MusicActivity$4;
.super Ljava/lang/Object;
.source "MusicActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/music/MusicActivity$4;)Lcom/konka/mm/music/MusicActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$16(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$17(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/music/MusicActivity;->setPlayMode(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006a    # com.konka.mm.R.string.cycleTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$19(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->repeatBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$16(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/music/MusicActivity;->setPlayMode(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090069    # com.konka.mm.R.string.repeatTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->cycleBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$17(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$20(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/music/MusicActivity;->setPlayMode(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006b    # com.konka.mm.R.string.shuffleTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->shuffleBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$20(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$19(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/music/MusicActivity;->setPlayMode(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090068    # com.konka.mm.R.string.sequenceTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->prev()V

    goto/16 :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->play()V

    goto/16 :goto_0

    :sswitch_6
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->pause()V

    goto/16 :goto_0

    :sswitch_7
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->next()V

    goto/16 :goto_0

    :sswitch_8
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v2, v4}, Lcom/konka/mm/music/MusicActivity;->access$21(Lcom/konka/mm/music/MusicActivity;Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->finish()V

    goto/16 :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v2, v4}, Lcom/konka/mm/music/MusicActivity;->access$22(Lcom/konka/mm/music/MusicActivity;Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->music_list_layout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$23(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->listView:Landroid/view/View;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$24(Lcom/konka/mm/music/MusicActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$25(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$26(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$26(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$26(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$27(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090063    # com.konka.mm.R.string.lrcTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v2, v5}, Lcom/konka/mm/music/MusicActivity;->access$22(Lcom/konka/mm/music/MusicActivity;Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->music_list_layout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$23(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->listView:Landroid/view/View;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$24(Lcom/konka/mm/music/MusicActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$26(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$25(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$25(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->listBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$25(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$27(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090062    # com.konka.mm.R.string.musicListTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_song:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_singer:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->curPlay:I
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$14(Lcom/konka/mm/music/MusicActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/mm/finals/MusicFinals;->ALBUM_PIC_PATH:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_song:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_song:Landroid/widget/EditText;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_song:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mLrcList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mLrcAdapter:Lcom/konka/mm/music/MusicActivity$LrcListAdapter;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->notifyDataSetChanged()V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0

    :sswitch_d
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mSearchDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/konka/mm/music/MusicActivity$4$1;

    invoke-direct {v3, p0}, Lcom/konka/mm/music/MusicActivity$4$1;-><init>(Lcom/konka/mm/music/MusicActivity$4;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_e
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2, v5}, Lcom/konka/mm/music/MusicActivity;->snapScreen(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0b003d -> :sswitch_e    # com.konka.mm.R.id.btn_list_left_music
        0x7f0b003e -> :sswitch_f    # com.konka.mm.R.id.btn_list_right_music
        0x7f0b004b -> :sswitch_c    # com.konka.mm.R.id.lrc_cancle
        0x7f0b007e -> :sswitch_9    # com.konka.mm.R.id.btn_music_list
        0x7f0b007f -> :sswitch_a    # com.konka.mm.R.id.btn_music_lrc
        0x7f0b0081 -> :sswitch_4    # com.konka.mm.R.id.btn_music_pre
        0x7f0b0083 -> :sswitch_5    # com.konka.mm.R.id.btn_music_play
        0x7f0b0084 -> :sswitch_6    # com.konka.mm.R.id.btn_music_pause
        0x7f0b0086 -> :sswitch_7    # com.konka.mm.R.id.btn_music_next
        0x7f0b0088 -> :sswitch_8    # com.konka.mm.R.id.btn_music_stop
        0x7f0b008a -> :sswitch_1    # com.konka.mm.R.id.btn_music_sequence
        0x7f0b008b -> :sswitch_0    # com.konka.mm.R.id.btn_music_repeat_one
        0x7f0b008c -> :sswitch_2    # com.konka.mm.R.id.btn_music_cycle
        0x7f0b008d -> :sswitch_3    # com.konka.mm.R.id.btn_music_shuffle
        0x7f0b008f -> :sswitch_b    # com.konka.mm.R.id.btn_search_lrc
        0x7f0b00b9 -> :sswitch_d    # com.konka.mm.R.id.lrc_search
    .end sparse-switch
.end method
