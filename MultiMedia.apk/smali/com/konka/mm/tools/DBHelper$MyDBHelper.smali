.class Lcom/konka/mm/tools/DBHelper$MyDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DBHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/tools/DBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyDBHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/tools/DBHelper;


# direct methods
.method public constructor <init>(Lcom/konka/mm/tools/DBHelper;Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    iput-object p1, p0, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->this$0:Lcom/konka/mm/tools/DBHelper;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "onCreate...."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->this$0:Lcom/konka/mm/tools/DBHelper;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {v0, v1}, Lcom/konka/mm/tools/DBHelper;->access$0(Lcom/konka/mm/tools/DBHelper;Ljava/lang/StringBuffer;)V

    iget-object v0, p0, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->this$0:Lcom/konka/mm/tools/DBHelper;

    # getter for: Lcom/konka/mm/tools/DBHelper;->tableCreate:Ljava/lang/StringBuffer;
    invoke-static {v0}, Lcom/konka/mm/tools/DBHelper;->access$1(Lcom/konka/mm/tools/DBHelper;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "create table "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "_id integer primary key autoincrement,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "DATA text,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "SIZE text,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "TITLE text,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "TYPE text,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "PARENTPATH text,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "ROOTPATH text,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "THUMBPATH text"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->this$0:Lcom/konka/mm/tools/DBHelper;

    # getter for: Lcom/konka/mm/tools/DBHelper;->tableCreate:Ljava/lang/StringBuffer;
    invoke-static {v1}, Lcom/konka/mm/tools/DBHelper;->access$1(Lcom/konka/mm/tools/DBHelper;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->this$0:Lcom/konka/mm/tools/DBHelper;

    # getter for: Lcom/konka/mm/tools/DBHelper;->tableCreate:Ljava/lang/StringBuffer;
    invoke-static {v0}, Lcom/konka/mm/tools/DBHelper;->access$1(Lcom/konka/mm/tools/DBHelper;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "onUpgrade...."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, "drop table if exists image"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->this$0:Lcom/konka/mm/tools/DBHelper;

    # getter for: Lcom/konka/mm/tools/DBHelper;->myDBHelper:Lcom/konka/mm/tools/DBHelper$MyDBHelper;
    invoke-static {v1}, Lcom/konka/mm/tools/DBHelper;->access$2(Lcom/konka/mm/tools/DBHelper;)Lcom/konka/mm/tools/DBHelper$MyDBHelper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
