.class Lcom/konka/mm/tools/ImageFileCache$FileLastModifSort;
.super Ljava/lang/Object;
.source "ImageFileCache.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/tools/ImageFileCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileLastModifSort"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/tools/ImageFileCache;


# direct methods
.method private constructor <init>(Lcom/konka/mm/tools/ImageFileCache;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/tools/ImageFileCache$FileLastModifSort;->this$0:Lcom/konka/mm/tools/ImageFileCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/mm/tools/ImageFileCache;Lcom/konka/mm/tools/ImageFileCache$FileLastModifSort;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/tools/ImageFileCache$FileLastModifSort;-><init>(Lcom/konka/mm/tools/ImageFileCache;)V

    return-void
.end method


# virtual methods
.method public compare(Ljava/io/File;Ljava/io/File;)I
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/io/File;

    check-cast p2, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/tools/ImageFileCache$FileLastModifSort;->compare(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    return v0
.end method
