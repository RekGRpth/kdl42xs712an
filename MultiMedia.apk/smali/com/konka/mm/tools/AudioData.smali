.class public Lcom/konka/mm/tools/AudioData;
.super Ljava/lang/Object;
.source "AudioData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x30f0aca0fb1cafceL


# instance fields
.field private album:Ljava/lang/String;

.field private albumid:J

.field private artist:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private duration:I

.field private id:I

.field private mimeType:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private size:Ljava/lang/Long;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAlbum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioData;->album:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumid()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/tools/AudioData;->albumid:J

    return-wide v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioData;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioData;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/tools/AudioData;->duration:I

    return v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/tools/AudioData;->id:I

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioData;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioData;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioData;->size:Ljava/lang/Long;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/tools/AudioData;->album:Ljava/lang/String;

    return-void
.end method

.method public setAlbumid(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/tools/AudioData;->albumid:J

    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/tools/AudioData;->artist:Ljava/lang/String;

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/tools/AudioData;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/tools/AudioData;->duration:I

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/tools/AudioData;->id:I

    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/tools/AudioData;->mimeType:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/tools/AudioData;->path:Ljava/lang/String;

    return-void
.end method

.method public setSize(Ljava/lang/Long;)V
    .locals 0
    .param p1    # Ljava/lang/Long;

    iput-object p1, p0, Lcom/konka/mm/tools/AudioData;->size:Ljava/lang/Long;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/tools/AudioData;->title:Ljava/lang/String;

    return-void
.end method
