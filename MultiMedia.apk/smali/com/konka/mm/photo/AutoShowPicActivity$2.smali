.class Lcom/konka/mm/photo/AutoShowPicActivity$2;
.super Landroid/os/Handler;
.source "AutoShowPicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/AutoShowPicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/AutoShowPicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v0, v0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideController()V

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/ImageViewTouch;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/ImageViewTouch;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideController()V

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_2

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v1, v1, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$2;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$5(Lcom/konka/mm/photo/AutoShowPicActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
