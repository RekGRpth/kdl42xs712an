.class Lcom/konka/mm/photo/PicMainActivity$sortThread;
.super Ljava/lang/Object;
.source "PicMainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "sortThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicMainActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/PicMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->setting_state:I
    invoke-static {v1}, Lcom/konka/mm/photo/PicMainActivity;->access$7(Lcom/konka/mm/photo/PicMainActivity;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/photo/PicMainActivity;->access$9(Lcom/konka/mm/photo/PicMainActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/photo/PicMainActivity;->access$9(Lcom/konka/mm/photo/PicMainActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v1, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;
    invoke-static {v2}, Lcom/konka/mm/photo/PicMainActivity;->access$8(Lcom/konka/mm/photo/PicMainActivity;)Lcom/konka/mm/tools/DBHelper1;

    move-result-object v2

    sget-object v3, Lcom/konka/mm/photo/PicMainActivity;->mRootPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/konka/mm/tools/DBHelper1;->getImageSortByType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v1, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;
    invoke-static {v2}, Lcom/konka/mm/photo/PicMainActivity;->access$8(Lcom/konka/mm/photo/PicMainActivity;)Lcom/konka/mm/tools/DBHelper1;

    move-result-object v2

    sget-object v3, Lcom/konka/mm/photo/PicMainActivity;->mRootPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/konka/mm/tools/DBHelper1;->getImageSortByTitle(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v1, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$sortThread;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;
    invoke-static {v2}, Lcom/konka/mm/photo/PicMainActivity;->access$8(Lcom/konka/mm/photo/PicMainActivity;)Lcom/konka/mm/tools/DBHelper1;

    move-result-object v2

    sget-object v3, Lcom/konka/mm/photo/PicMainActivity;->mRootPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/konka/mm/tools/DBHelper1;->getImageSortBySize(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
