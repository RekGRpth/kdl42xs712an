.class Lcom/konka/mm/photo/PhotoDiskActivity$3;
.super Ljava/lang/Object;
.source "PhotoDiskActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/photo/PhotoDiskActivity;->setAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PhotoDiskActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PhotoDiskActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PhotoDiskActivity$3;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    if-nez p3, :cond_1

    sget-object v4, Lcom/konka/mm/modules/ModulesActivity;->instance:Lcom/konka/mm/modules/ModulesActivity;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/konka/mm/modules/ModulesActivity;->instance:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v4}, Lcom/konka/mm/modules/ModulesActivity;->finish()V

    :cond_0
    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$3;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/konka/mm/photo/PhotoDiskActivity;->startMudulesActivity(I)V

    :goto_0
    return-void

    :cond_1
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$3;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    # getter for: Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/photo/PhotoDiskActivity;->access$1(Lcom/konka/mm/photo/PhotoDiskActivity;)Ljava/util/List;

    move-result-object v4

    add-int/lit8 v6, p3, -0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/temp.txt"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$3;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    # getter for: Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/photo/PhotoDiskActivity;->access$1(Lcom/konka/mm/photo/PhotoDiskActivity;)Ljava/util/List;

    move-result-object v4

    add-int/lit8 v5, p3, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    # getter for: Lcom/konka/mm/photo/PhotoDiskActivity;->diskRoot:Ljava/lang/String;
    invoke-static {}, Lcom/konka/mm/photo/PhotoDiskActivity;->access$2()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/konka/mm/photo/PicMainActivity;->instance:Lcom/konka/mm/photo/PicMainActivity;

    if-eqz v4, :cond_2

    sget-object v4, Lcom/konka/mm/photo/PicMainActivity;->instance:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v4}, Lcom/konka/mm/photo/PicMainActivity;->finish()V

    :cond_2
    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$3;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    # getter for: Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/mm/photo/PhotoDiskActivity;->access$1(Lcom/konka/mm/photo/PhotoDiskActivity;)Ljava/util/List;

    move-result-object v4

    add-int/lit8 v5, p3, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/konka/mm/photo/PhotoDiskActivity;->access$3(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "enter the picture list which include SQL"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$3;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    add-int/lit8 v5, p3, -0x1

    invoke-virtual {v4, v5}, Lcom/konka/mm/photo/PhotoDiskActivity;->startPicMainActivity(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "enter the picture list which don\'t include SQL"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$3;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    add-int/lit8 v5, p3, -0x1

    invoke-virtual {v4, v5}, Lcom/konka/mm/photo/PhotoDiskActivity;->startMudulesActivity(I)V

    goto :goto_0
.end method
