.class Lcom/konka/mm/photo/AutoShowPicActivity$1;
.super Landroid/os/Handler;
.source "AutoShowPicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/AutoShowPicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/AutoShowPicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const v3, 0x50000001

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :sswitch_0
    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->settextViewName()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v0, v0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v0, v0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->mDelayed_time:I
    invoke-static {v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$0(Lcom/konka/mm/photo/AutoShowPicActivity;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    const/16 v1, 0x1b58

    invoke-static {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$1(Lcom/konka/mm/photo/AutoShowPicActivity;I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # invokes: Lcom/konka/mm/photo/AutoShowPicActivity;->set4K2KPicture()V
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$2(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setPhotoView()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$1;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # invokes: Lcom/konka/mm/photo/AutoShowPicActivity;->playFirstPhoto()V
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$3(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_3
        0xa1 -> :sswitch_1
        0xa2 -> :sswitch_2
        0x50000001 -> :sswitch_0
    .end sparse-switch
.end method
