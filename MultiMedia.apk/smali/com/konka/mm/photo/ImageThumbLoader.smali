.class public Lcom/konka/mm/photo/ImageThumbLoader;
.super Landroid/os/AsyncTask;
.source "ImageThumbLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/konka/mm/photo/ImageThumbLoader;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 9
    .param p1    # [Ljava/lang/Object;

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    aget-object v0, p1, v6

    check-cast v0, Landroid/app/Activity;

    aget-object v2, p1, v7

    check-cast v2, Landroid/widget/ImageView;

    aget-object v3, p1, v8

    check-cast v3, Ljava/lang/String;

    const v4, 0x1fa400

    invoke-static {v3, v4}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, " doInBackground============================="

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-nez v1, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "bitmap =============================null"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    new-array v4, v8, [Ljava/lang/Object;

    aput-object v2, v4, v6

    aput-object v1, v4, v7

    invoke-virtual {p0, v4}, Lcom/konka/mm/photo/ImageThumbLoader;->publishProgress([Ljava/lang/Object;)V

    const/4 v4, 0x0

    return-object v4

    :cond_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "bitmap =============================is ont null"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 2
    .param p1    # [Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, p1, v1

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x1

    aget-object v1, p1, v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
