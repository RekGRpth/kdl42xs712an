.class public Lcom/konka/mm/photo/ThumbFileDownloadHandler;
.super Lcom/konka/mm/photo/ImageThumbHandler;
.source "ThumbFileDownloadHandler.java"


# instance fields
.field protected mImageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 1
    .param p1    # Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/konka/mm/photo/ThumbFileDownloadHandler;-><init>(Lcom/konka/mm/photo/ImageThumbHandler;Landroid/widget/ImageView;)V

    return-void
.end method

.method public constructor <init>(Lcom/konka/mm/photo/ImageThumbHandler;Landroid/widget/ImageView;)V
    .locals 0
    .param p1    # Lcom/konka/mm/photo/ImageThumbHandler;
    .param p2    # Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/konka/mm/photo/ImageThumbHandler;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/photo/ThumbFileDownloadHandler;->mImageView:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/konka/mm/photo/ImageThumbHandler;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v2, p1, Lcom/konka/mm/photo/ThumbFileDownloadHandler;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/konka/mm/photo/ThumbFileDownloadHandler;

    iget-object v1, v0, Lcom/konka/mm/photo/ThumbFileDownloadHandler;->mImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/mm/photo/ThumbFileDownloadHandler;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Lcom/konka/mm/photo/ImageThumbHandler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/ThumbFileDownloadHandler;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public hashCode()I
    .locals 3

    invoke-super {p0}, Lcom/konka/mm/photo/ImageThumbHandler;->hashCode()I

    move-result v0

    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/konka/mm/photo/ThumbFileDownloadHandler;->mImageView:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    return v0

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/photo/ThumbFileDownloadHandler;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public preHandleMessage()V
    .locals 0

    invoke-super {p0}, Lcom/konka/mm/photo/ImageThumbHandler;->preHandleMessage()V

    return-void
.end method
