.class public Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AutoShowPicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/AutoShowPicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EthStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/AutoShowPicActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-static {v0}, Lcom/konka/mm/model/FamilyShareModel;->isConneted2Network(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v1, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finishActivity()V

    :cond_0
    return-void
.end method
