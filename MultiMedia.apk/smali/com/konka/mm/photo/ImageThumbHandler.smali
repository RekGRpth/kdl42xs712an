.class public Lcom/konka/mm/photo/ImageThumbHandler;
.super Landroid/os/Handler;
.source "ImageThumbHandler.java"


# instance fields
.field protected mComponent:Lcom/konka/mm/photo/ImageThumbHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    return-void
.end method

.method public constructor <init>(Lcom/konka/mm/photo/ImageThumbHandler;)V
    .locals 1
    .param p1    # Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    instance-of v1, p1, Lcom/konka/mm/photo/ImageThumbHandler;

    if-nez v1, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/konka/mm/photo/ImageThumbHandler;

    iget-object v1, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    iget-object v4, v0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    if-eqz v4, :cond_4

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-eqz v1, :cond_5

    move v2, v3

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v4, v3

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    iget-object v2, v0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-virtual {v1, v2}, Lcom/konka/mm/photo/ImageThumbHandler;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    :cond_0
    return-void
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public preHandleMessage()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/ImageThumbHandler;->mComponent:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-virtual {v0}, Lcom/konka/mm/photo/ImageThumbHandler;->preHandleMessage()V

    :cond_0
    return-void
.end method
