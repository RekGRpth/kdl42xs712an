.class public Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhotoDiskActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PhotoDiskActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UsbReceiver"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UsbReceiver"


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PhotoDiskActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/PhotoDiskActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->cancel()V

    const/4 v3, 0x0

    sput-object v3, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    :cond_0
    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "UsbReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Mounted:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    invoke-virtual {v4}, Lcom/konka/mm/photo/PhotoDiskActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090094    # com.konka.mm.R.string.new_usb_mount

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    invoke-virtual {v3}, Lcom/konka/mm/photo/PhotoDiskActivity;->broweToRoot()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "UsbReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unmounted:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    iget-object v4, p0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    invoke-virtual {v4}, Lcom/konka/mm/photo/PhotoDiskActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090095    # com.konka.mm.R.string.usb_remove

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;->this$0:Lcom/konka/mm/photo/PhotoDiskActivity;

    invoke-virtual {v3}, Lcom/konka/mm/photo/PhotoDiskActivity;->broweToRoot()V

    goto :goto_0
.end method
