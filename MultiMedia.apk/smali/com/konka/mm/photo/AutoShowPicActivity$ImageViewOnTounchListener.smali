.class final Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;
.super Ljava/lang/Object;
.source "AutoShowPicActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/AutoShowPicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ImageViewOnTounchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/AutoShowPicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/high16 v5, 0x41200000    # 10.0f

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v7, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    return v7

    :pswitch_1
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->savedMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->start:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iput v7, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->mode:I

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->mode:I

    if-ne v3, v7, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->start:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float v0, v3, v4

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "event.getX() - start.x="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageViewTouch;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    float-to-double v3, v3

    cmpg-double v3, v3, v8

    if-gtz v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->start:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->start:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    const/high16 v4, -0x3d380000    # -100.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v3, v7}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iput v4, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->mode:I

    goto/16 :goto_0

    :pswitch_4
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # invokes: Lcom/konka/mm/photo/AutoShowPicActivity;->spacing(Landroid/view/MotionEvent;)F
    invoke-static {v4, p2}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$6(Lcom/konka/mm/photo/AutoShowPicActivity;Landroid/view/MotionEvent;)F

    move-result v4

    iput v4, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->oldDist:F

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iput-boolean v7, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->moveflag:Z

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->oldDist:F

    cmpl-float v3, v3, v5

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->savedMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->mid:Landroid/graphics/PointF;

    # invokes: Lcom/konka/mm/photo/AutoShowPicActivity;->midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    invoke-static {v3, v4, p2}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$7(Lcom/konka/mm/photo/AutoShowPicActivity;Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iput v6, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->mode:I

    goto/16 :goto_0

    :pswitch_5
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->mode:I

    if-ne v3, v7, :cond_2

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageViewTouch;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    float-to-double v3, v3

    cmpg-double v3, v3, v8

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->savedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v5, v5, Lcom/konka/mm/photo/AutoShowPicActivity;->start:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v6, v6, Lcom/konka/mm/photo/AutoShowPicActivity;->start:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->mode:I

    if-ne v3, v6, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # invokes: Lcom/konka/mm/photo/AutoShowPicActivity;->spacing(Landroid/view/MotionEvent;)F
    invoke-static {v3, p2}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$6(Lcom/konka/mm/photo/AutoShowPicActivity;Landroid/view/MotionEvent;)F

    move-result v1

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-boolean v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->moveflag:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iput v1, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->oldDist:F

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iput-boolean v4, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->moveflag:Z

    :cond_3
    cmpl-float v3, v1, v5

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-object v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->savedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v3, v3, Lcom/konka/mm/photo/AutoShowPicActivity;->oldDist:F

    div-float v2, v1, v3

    float-to-double v3, v2

    cmpl-double v3, v3, v8

    if-ltz v3, :cond_4

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageViewTouch;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    float-to-double v3, v3

    const-wide/high16 v5, 0x4008000000000000L    # 3.0

    cmpg-double v3, v3, v5

    if-gez v3, :cond_4

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->screenWidth:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v5, v5, Lcom/konka/mm/photo/AutoShowPicActivity;->screenHeight:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v3, v2, v2, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto/16 :goto_0

    :cond_4
    float-to-double v3, v2

    cmpg-double v3, v3, v8

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageViewTouch;->getScale(Landroid/graphics/Matrix;)F

    move-result v3

    float-to-double v3, v3

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    cmpl-double v3, v3, v5

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v4, v4, Lcom/konka/mm/photo/AutoShowPicActivity;->screenWidth:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget v5, v5, Lcom/konka/mm/photo/AutoShowPicActivity;->screenHeight:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v3, v2, v2, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
