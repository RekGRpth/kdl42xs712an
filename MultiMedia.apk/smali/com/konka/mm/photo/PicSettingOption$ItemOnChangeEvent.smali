.class Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;
.super Ljava/lang/Object;
.source "PicSettingOption.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicSettingOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemOnChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicSettingOption;


# direct methods
.method private constructor <init>(Lcom/konka/mm/photo/PicSettingOption;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/mm/photo/PicSettingOption;Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;-><init>(Lcom/konka/mm/photo/PicSettingOption;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    # getter for: Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/mm/photo/PicSettingOption;->access$0(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    # getter for: Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/mm/photo/PicSettingOption;->access$1(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    # getter for: Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/photo/PicSettingOption;->access$2(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/TextView;

    move-result-object v0

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    # getter for: Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/photo/PicSettingOption;->access$2(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    # getter for: Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/mm/photo/PicSettingOption;->access$1(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    # getter for: Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/photo/PicSettingOption;->access$2(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/TextView;

    move-result-object v0

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;->this$0:Lcom/konka/mm/photo/PicSettingOption;

    # getter for: Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/photo/PicSettingOption;->access$2(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/TextView;

    move-result-object v0

    const v1, -0x7c7c78

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
