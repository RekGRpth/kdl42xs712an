.class Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;
.super Ljava/lang/Object;
.source "FamilyShareMainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/FamilyShareMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FamilyShareListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/FamilyShareMainActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/FamilyShareMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;->this$0:Lcom/konka/mm/FamilyShareMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;->this$0:Lcom/konka/mm/FamilyShareMainActivity;

    # getter for: Lcom/konka/mm/FamilyShareMainActivity;->SearchBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/mm/FamilyShareMainActivity;->access$0(Lcom/konka/mm/FamilyShareMainActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v3, "SearchBtnSearchBtnSearchBtnSearchBtn"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;->this$0:Lcom/konka/mm/FamilyShareMainActivity;

    invoke-static {v2}, Lcom/konka/mm/model/FamilyShareModel;->SearchAllHost(Landroid/content/Context;)Ljava/util/ArrayList;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;->this$0:Lcom/konka/mm/FamilyShareMainActivity;

    const-class v3, Lcom/konka/mm/FamilyShareHostActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;->this$0:Lcom/konka/mm/FamilyShareMainActivity;

    invoke-virtual {v2, v1}, Lcom/konka/mm/FamilyShareMainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "android.content.ActivityNotFoundException"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/konka/mm/FamilyShareMainActivity$FamilyShareListener;->this$0:Lcom/konka/mm/FamilyShareMainActivity;

    # getter for: Lcom/konka/mm/FamilyShareMainActivity;->CancelBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/mm/FamilyShareMainActivity;->access$1(Lcom/konka/mm/FamilyShareMainActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v3, "CancelBtnCancelBtnCancelBtnCancelBtnCancelBtn"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v3, "unknown button clicked!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
