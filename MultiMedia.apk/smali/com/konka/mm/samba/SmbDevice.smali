.class public Lcom/konka/mm/samba/SmbDevice;
.super Ljava/lang/Object;
.source "SmbDevice.java"


# static fields
.field private static final localpath:Ljava/lang/String; = "/mnt/samba/"


# instance fields
.field private SmbURL:Ljava/lang/String;

.field private auth:Lcom/konka/mm/samba/SmbAuthentication;

.field protected folderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbShareFolder;",
            ">;"
        }
    .end annotation
.end field

.field private ip:Ljava/lang/String;

.field private mMountedFlag:Z

.field private mountPointByIp:Z

.field private onRecvMsg:Lcom/konka/mm/samba/OnRecvMsg;

.field root:Ljcifs/smb/SmbFile;

.field private sambaBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

.field private stm:Lcom/mstar/android/storage/MStorageManager;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->onRecvMsg:Lcom/konka/mm/samba/OnRecvMsg;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    iput-boolean v2, p0, Lcom/konka/mm/samba/SmbDevice;->mountPointByIp:Z

    iput-boolean v2, p0, Lcom/konka/mm/samba/SmbDevice;->mMountedFlag:Z

    iput-object p1, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->onRecvMsg:Lcom/konka/mm/samba/OnRecvMsg;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    iput-boolean v2, p0, Lcom/konka/mm/samba/SmbDevice;->mountPointByIp:Z

    iput-boolean v2, p0, Lcom/konka/mm/samba/SmbDevice;->mMountedFlag:Z

    iput-object p1, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    iput-object p2, p0, Lcom/konka/mm/samba/SmbDevice;->sambaBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    return-void
.end method

.method static convertToSambaFolder(Lcom/konka/mm/samba/SmbShareFolder;Ljcifs/smb/SmbFile;)V
    .locals 0
    .param p0    # Lcom/konka/mm/samba/SmbShareFolder;
    .param p1    # Ljcifs/smb/SmbFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/samba/SmbShareFolder;->file:Ljcifs/smb/SmbFile;

    return-void
.end method

.method private getServerName()Ljava/lang/String;
    .locals 6

    :try_start_0
    iget-object v5, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-static {v5}, Ljcifs/netbios/NbtAddress;->getByName(Ljava/lang/String;)Ljcifs/netbios/NbtAddress;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljcifs/netbios/NbtAddress;->isActive()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v4}, Ljcifs/netbios/NbtAddress;->getAllByAddress(Ljcifs/netbios/NbtAddress;)[Ljcifs/netbios/NbtAddress;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_1

    :cond_0
    :goto_1
    const/4 v5, 0x0

    :goto_2
    return-object v5

    :cond_1
    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->isGroupAddress()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->getNameType()I

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->getHostName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->getHostName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    return-object v0
.end method

.method public getAuth()Lcom/konka/mm/samba/SmbAuthentication;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    return-object v0
.end method

.method public getHostName()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/konka/mm/samba/SmbDevice;->getServerName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " hostName : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-object v1
.end method

.method public getSharefolderList()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbShareFolder;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "smb://"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    invoke-virtual {v12}, Lcom/konka/mm/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    invoke-virtual {v12}, Lcom/konka/mm/samba/SmbAuthentication;->getPassword()Ljava/lang/String;

    move-result-object v8

    const-string v12, "\\"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_1

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    invoke-virtual {v12}, Lcom/konka/mm/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v12

    add-int/lit8 v13, v5, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    invoke-virtual {v12}, Lcom/konka/mm/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :goto_0
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, " NtlmPasswordAuthentication = "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v1, Ljcifs/smb/NtlmPasswordAuthentication;

    invoke-direct {v1, v3, v11, v8}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    if-nez v12, :cond_2

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "SmbURL == null"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v12, 0x0

    :goto_1
    return-object v12

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    if-nez v12, :cond_4

    new-instance v12, Ljcifs/smb/SmbFile;

    iget-object v13, p0, Lcom/konka/mm/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    invoke-direct {v12, v13}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    iput-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->root:Ljcifs/smb/SmbFile;

    :goto_2
    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->root:Ljcifs/smb/SmbFile;

    invoke-virtual {v12}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v0

    const/4 v6, 0x0

    :goto_3
    array-length v12, v0

    if-lt v6, v12, :cond_5

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-gtz v12, :cond_3

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->onRecvMsg:Lcom/konka/mm/samba/OnRecvMsg;

    const/16 v13, 0x36

    invoke-interface {v12, v13}, Lcom/konka/mm/samba/OnRecvMsg;->onRecvMsg(I)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_3
    :goto_4
    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    goto :goto_1

    :cond_4
    :try_start_1
    new-instance v12, Ljcifs/smb/SmbFile;

    iget-object v13, p0, Lcom/konka/mm/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    invoke-direct {v12, v13, v1}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    iput-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->root:Ljcifs/smb/SmbFile;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_4

    :cond_5
    :try_start_2
    aget-object v12, v0, v6

    invoke-virtual {p0, v12}, Lcom/konka/mm/samba/SmbDevice;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v12

    if-eqz v12, :cond_6

    new-instance v10, Lcom/konka/mm/samba/SmbShareFolder;

    invoke-direct {v10}, Lcom/konka/mm/samba/SmbShareFolder;-><init>()V

    invoke-virtual {v10, p0}, Lcom/konka/mm/samba/SmbShareFolder;->setDevice(Lcom/konka/mm/samba/SmbDevice;)V

    aget-object v12, v0, v6

    invoke-static {v10, v12}, Lcom/konka/mm/samba/SmbDevice;->convertToSambaFolder(Lcom/konka/mm/samba/SmbShareFolder;Ljcifs/smb/SmbFile;)V

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    aget-object v12, v0, v6

    invoke-virtual {v12}, Ljcifs/smb/SmbFile;->getType()I
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v12

    const/4 v13, 0x2

    if-eq v12, v13, :cond_8

    :cond_7
    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_8
    :try_start_3
    aget-object v12, v0, v6

    invoke-virtual {v12}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v2

    const/4 v9, 0x0

    :goto_6
    array-length v12, v2

    if-ge v9, v12, :cond_7

    aget-object v12, v2, v9

    invoke-virtual {p0, v12}, Lcom/konka/mm/samba/SmbDevice;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v12

    if-nez v12, :cond_9

    :goto_7
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    :cond_9
    new-instance v10, Lcom/konka/mm/samba/SmbShareFolder;

    invoke-direct {v10}, Lcom/konka/mm/samba/SmbShareFolder;-><init>()V

    invoke-virtual {v10, p0}, Lcom/konka/mm/samba/SmbShareFolder;->setDevice(Lcom/konka/mm/samba/SmbDevice;)V

    aget-object v12, v2, v9

    invoke-static {v10, v12}, Lcom/konka/mm/samba/SmbDevice;->convertToSambaFolder(Lcom/konka/mm/samba/SmbShareFolder;Ljcifs/smb/SmbFile;)V

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljcifs/smb/SmbException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_7

    :catch_1
    move-exception v4

    :try_start_4
    invoke-virtual {v4}, Ljcifs/smb/SmbException;->getNtStatus()I

    move-result v7

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "workgroup : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_5

    :catch_2
    move-exception v4

    iget-object v12, p0, Lcom/konka/mm/samba/SmbDevice;->onRecvMsg:Lcom/konka/mm/samba/OnRecvMsg;

    const/16 v13, 0x33

    invoke-interface {v12, v13}, Lcom/konka/mm/samba/OnRecvMsg;->onRecvMsg(I)V

    invoke-virtual {v4}, Ljcifs/smb/SmbException;->printStackTrace()V

    goto/16 :goto_4
.end method

.method public hasPassword()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isHost(Ljcifs/smb/SmbFile;)Z
    .locals 3
    .param p1    # Ljcifs/smb/SmbFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p1}, Ljcifs/smb/SmbFile;->getType()I

    move-result v0

    invoke-virtual {p1}, Ljcifs/smb/SmbFile;->isHidden()Z

    move-result v2

    if-nez v2, :cond_1

    if-eq v0, v1, :cond_0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMounted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/samba/SmbDevice;->mMountedFlag:Z

    return v0
.end method

.method public localPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/mnt/samba/"

    return-object v0
.end method

.method public mount(Lcom/konka/mm/samba/SmbAuthentication;)V
    .locals 10
    .param p1    # Lcom/konka/mm/samba/SmbAuthentication;

    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/mm/samba/SmbShareFolder;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " mount "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/storage/MStorageManager;->mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "mount ok"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    :goto_1
    if-nez v8, :cond_4

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, " mount jcifs fail! "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v0, "\\"

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-lez v7, :cond_3

    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/konka/mm/samba/SmbAuthentication;->getPassword()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/storage/MStorageManager;->mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/samba/SmbDevice;->mMountedFlag:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/samba/SmbDevice;->mountPointByIp:Z

    goto/16 :goto_0
.end method

.method public remotePath()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "//"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAuth(Lcom/konka/mm/samba/SmbAuthentication;)V
    .locals 0
    .param p1    # Lcom/konka/mm/samba/SmbAuthentication;

    iput-object p1, p0, Lcom/konka/mm/samba/SmbDevice;->auth:Lcom/konka/mm/samba/SmbAuthentication;

    return-void
.end method

.method public setOnRecvMsg(Lcom/konka/mm/samba/OnRecvMsg;)V
    .locals 0
    .param p1    # Lcom/konka/mm/samba/OnRecvMsg;

    iput-object p1, p0, Lcom/konka/mm/samba/SmbDevice;->onRecvMsg:Lcom/konka/mm/samba/OnRecvMsg;

    return-void
.end method

.method public setStorageManager(Lcom/mstar/android/storage/MStorageManager;)V
    .locals 0
    .param p1    # Lcom/mstar/android/storage/MStorageManager;

    iput-object p1, p0, Lcom/konka/mm/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    return-void
.end method

.method public unmount(Landroid/app/Activity;)V
    .locals 7
    .param p1    # Landroid/app/Activity;

    const/4 v6, 0x1

    iget-object v2, p0, Lcom/konka/mm/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    check-cast p1, Lcom/konka/mm/samba/SambaBrowserActivity;

    iput-object p1, p0, Lcom/konka/mm/samba/SmbDevice;->sambaBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, p0, Lcom/konka/mm/samba/SmbDevice;->sambaBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->scanHandler:Landroid/os/Handler;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/samba/SmbShareFolder;

    iget-boolean v3, p0, Lcom/konka/mm/samba/SmbDevice;->mountPointByIp:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/mm/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/mm/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lcom/mstar/android/storage/MStorageManager;->unmountSamba(Ljava/lang/String;Z)Z

    move-result v1

    :goto_1
    if-nez v1, :cond_2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, " umount jcifs fail! "

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/mm/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6}, Lcom/mstar/android/storage/MStorageManager;->unmountSamba(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/mm/samba/SmbDevice;->mMountedFlag:Z

    goto :goto_0
.end method
