.class public Lcom/konka/mm/samba/SambaBrowserActivity;
.super Landroid/app/Activity;
.source "SambaBrowserActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/samba/SambaBrowserActivity$BtnFileDialogListener;,
        Lcom/konka/mm/samba/SambaBrowserActivity$BtnScanDialogListener;,
        Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;,
        Lcom/konka/mm/samba/SambaBrowserActivity$LoginRunnable;,
        Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;,
        Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;
    }
.end annotation


# static fields
.field protected static final BROWERS_FILE:I = 0x5

.field protected static final DIALOG_LOADING_FILE:I = 0x12

.field protected static final DIALOG_SHOW_FILE:I = 0x1

.field protected static final DIALOG_SHOW_LOADING:I = 0x3

.field protected static final DIALOG_SHOW_LOGIN:I = 0x1

.field private static final DIALOG_SHOW_SCAN:I = 0x4

.field protected static final DIALOG_SHOW_UPDATE:I = 0x2

.field protected static final DIALOG_SHOW_UPDATE_MOUNT:I = 0x11

.field public static final LIST_KEY:Ljava/lang/String; = "com.mstar.arraylist"

.field public static final MSG_HOST_LOGIN_FAILED:I = 0x33

.field public static final MSG_HOST_LOGIN_OK:I = 0x34

.field public static final MSG_SHARE_FILE_EMPTY:I = 0x36

.field public static final MSG_UNMOUNT_COMPLETE:I = 0x32

.field public static final MSG_UPDATE_DATA_COMPLETE:I = 0x31

.field public static final MSG_WRONG_PASSWORK:I = 0x35

.field private static final NOT_FIND_FILE:I = 0x2

.field public static final SCAN_FILE:I = 0x1

.field public static final SCAN_HOST:I = 0x0

.field private static final SCAN_HOST_ERROR:I = 0x3

.field public static final SCREEN_1080P:I = 0x1

.field public static final SCREEN_720P:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SambaBrowserActivity"


# instance fields
.field public final BACK_FOLDER_FLAG:I

.field public final ENTER_FOLDER_FLAG:I

.field public List_Mode:I

.field protected NumColumns:I

.field protected PageCount:I

.field public ThreadState:I

.field public final UPDATE_FOLDER_FLAG:I

.field private bShowScanDlg:Z

.field private bt_cancel:Landroid/widget/Button;

.field private bt_login:Landroid/widget/Button;

.field protected bt_update:Landroid/widget/Button;

.field private changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private clickListener:Landroid/view/View$OnClickListener;

.field public curItemNum:I

.field protected curScreen:I

.field protected currentHost:I

.field protected currentPage:I

.field private currentShare:I

.field private edit_pass:Landroid/widget/EditText;

.field private edit_user:Landroid/widget/EditText;

.field private gridOnKey:Landroid/view/View$OnKeyListener;

.field protected gxdbhelper:Lcom/konka/mm/samba/GXdbHelper;

.field private hostIp:Ljava/lang/String;

.field protected hosts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ip:Ljava/net/InetAddress;

.field private is3dKeyDown:Z

.field public isEnterOtherAct:Z

.field private isPopup:Z

.field private isSortPopup:Z

.field public isUpdate:Z

.field public keyResponse:Z

.field protected list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/samba/SambaListItem;",
            ">;"
        }
    .end annotation
.end field

.field private listItem:Lcom/konka/mm/samba/SambaListItem;

.field protected list_btn:Landroid/widget/Button;

.field protected listener:Landroid/widget/AdapterView$OnItemClickListener;

.field protected mCountPerPage:I

.field private mDialogFile:Landroid/app/ProgressDialog;

.field private mDialogLoadFile:Landroid/app/ProgressDialog;

.field private mDialogLoading:Landroid/app/ProgressDialog;

.field private mDialogLogin:Landroid/app/ProgressDialog;

.field private mDialogScan:Landroid/app/ProgressDialog;

.field private mDialogUpdate:Landroid/app/ProgressDialog;

.field private mDialogUpdateMount:Landroid/app/ProgressDialog;

.field private mEthStateReceiver:Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;

.field private mPushLeftInAnim:Landroid/view/animation/Animation;

.field private mPushLeftOutAnim:Landroid/view/animation/Animation;

.field private mPustRightInAnim:Landroid/view/animation/Animation;

.field private mPustRightOutAnim:Landroid/view/animation/Animation;

.field protected mScreen_mode:I

.field private menuPopupView:Landroid/view/View;

.field private needListening:Z

.field protected pageGridView:[Landroid/widget/GridView;

.field private pageHandler:Landroid/os/Handler;

.field private pageInfomation:Landroid/widget/TextView;

.field protected pageInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/PageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private pageLeft:Landroid/widget/Button;

.field private pageRight:Landroid/widget/Button;

.field protected page_linearLayout:Landroid/widget/LinearLayout;

.field private parentPath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SambaFile;",
            ">;"
        }
    .end annotation
.end field

.field private passWord:Ljava/lang/String;

.field private popupView:Landroid/view/View;

.field private popupWindow:Landroid/widget/PopupWindow;

.field protected scanHandler:Landroid/os/Handler;

.field private screenHeight:I

.field private screenHeight_720p:I

.field private screenWidth:I

.field private screenWidth_720p:I

.field private selectFile:Lcom/konka/mm/samba/SmbShareFolder;

.field private sortBtns:Landroid/widget/RadioGroup;

.field private sortHandler:Landroid/os/Handler;

.field private sortPopupWindow:Landroid/widget/PopupWindow;

.field private sortType:I

.field startX:F

.field private textEntryView:Landroid/view/View;

.field protected textview_name:Landroid/widget/TextView;

.field private userName:Ljava/lang/String;

.field protected viewFilpper:Landroid/widget/ViewFlipper;

.field private viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isEnterOtherAct:Z

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->is3dKeyDown:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->listItem:Lcom/konka/mm/samba/SambaListItem;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curItemNum:I

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLogin:Landroid/app/ProgressDialog;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdate:Landroid/app/ProgressDialog;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoading:Landroid/app/ProgressDialog;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdateMount:Landroid/app/ProgressDialog;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoadFile:Landroid/app/ProgressDialog;

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isSortPopup:Z

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/GridView;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    const/16 v0, 0x500

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenWidth_720p:I

    const/16 v0, 0x2d0

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenHeight_720p:I

    iput v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mScreen_mode:I

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    const/16 v0, 0xb

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ENTER_FOLDER_FLAG:I

    const/16 v0, 0x16

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->BACK_FOLDER_FLAG:I

    const/16 v0, 0x21

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->UPDATE_FOLDER_FLAG:I

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isUpdate:Z

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentShare:I

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentHost:I

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->needListening:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->hosts:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list:Ljava/util/List;

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortType:I

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ip:Ljava/net/InetAddress;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->hostIp:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->parentPath:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$1;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->scanHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$2;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$3;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->clickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$4;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$5;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$6;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$7;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/mm/samba/SambaBrowserActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortType:I

    return v0
.end method

.method static synthetic access$14(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findHosts()V

    return-void
.end method

.method static synthetic access$16(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_user:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_pass:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/mm/samba/SambaBrowserActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/samba/SambaBrowserActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z

    return v0
.end method

.method static synthetic access$4(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/mm/samba/SambaBrowserActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isSortPopup:Z

    return-void
.end method

.method static synthetic access$8(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfomation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;

    return-object v0
.end method

.method private findHosts()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    new-instance v1, Lcom/konka/mm/samba/SmbClient;

    invoke-direct {v1}, Lcom/konka/mm/samba/SmbClient;-><init>()V

    iput-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mHost:Lcom/konka/mm/samba/SmbClient;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mHost:Lcom/konka/mm/samba/SmbClient;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$17;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$17;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/mm/samba/SmbClient;->setOnRecvMsgListener(Lcom/konka/mm/samba/OnRecvMsgListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mHost:Lcom/konka/mm/samba/SmbClient;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SmbClient;->updateSmbDeviceList()V

    return-void
.end method

.method private sortFiles([Ljava/io/File;)[Ljava/io/File;
    .locals 2
    .param p1    # [Ljava/io/File;

    const/4 v0, 0x0

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortType:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    move-object v0, p1

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByName([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesBySize([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByType([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public checkScreenMode()V
    .locals 3

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenWidth:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenHeight:I

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenWidth:I

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenWidth_720p:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenHeight:I

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->screenHeight_720p:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mScreen_mode:I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mScreen_mode:I

    goto :goto_0
.end method

.method public deleteDirectory(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    :goto_0
    return v5

    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_4

    :cond_3
    :goto_2
    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_4
    aget-object v6, v1, v3

    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_6

    aget-object v6, v1, v3

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aget-object v6, v1, v3

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->deleteFile(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    aget-object v6, v1, v3

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->deleteDirectory(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_2
.end method

.method public deleteFile(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    iput-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isEnterOtherAct:Z

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-virtual {v1}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->returnToPreviousFolder()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isEnterOtherAct:Z

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090005    # com.konka.mm.R.string.LaunchShare

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/konka/mm/finals/CommonFinals;->quitActivity(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public findInputDlg()V
    .locals 3

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002a    # com.konka.mm.R.layout.samba_input

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    const v1, 0x7f0b00aa    # com.konka.mm.R.id.samba_user_name

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_user:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    const v1, 0x7f0b00ab    # com.konka.mm.R.id.samba_user_pass

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_pass:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    const v1, 0x7f0b00ac    # com.konka.mm.R.id.samba_login

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_login:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    const v1, 0x7f0b00ad    # com.konka.mm.R.id.samba_cancel

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_cancel:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_login:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$14;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$14;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_cancel:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$15;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$15;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public getHostIP()V
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ip:Ljava/net/InetAddress;

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->hostIp:Ljava/lang/String;

    return-void

    :cond_2
    if-nez v0, :cond_1

    :try_start_1
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/net/InetAddress;

    iput-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ip:Ljava/net/InetAddress;

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ip:Ljava/net/InetAddress;

    invoke-virtual {v6}, Ljava/net/InetAddress;->isSiteLocalAddress()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ip:Ljava/net/InetAddress;

    invoke-virtual {v6}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ip:Ljava/net/InetAddress;

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_1
.end method

.method protected hideDialog(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    :cond_0
    if-ne p1, v2, :cond_1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    :cond_1
    if-ne p1, v2, :cond_2

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLogin:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLogin:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdate:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdate:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoading:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoading:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    :cond_4
    const/16 v0, 0x11

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdateMount:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdateMount:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    :cond_5
    const/16 v0, 0x12

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoadFile:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoadFile:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-boolean v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    :cond_6
    return-void
.end method

.method public hideInputDlg()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z

    return-void
.end method

.method public isBtnFocuse()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_update:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocusable()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list_btn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public messageShow(I)V
    .locals 6
    .param p1    # I

    const/16 v5, 0x3e8

    const/16 v4, 0x11

    const/4 v3, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002b    # com.konka.mm.R.string.MM_NOFOUND_SHARE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0, v5}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002c    # com.konka.mm.R.string.MM_NET_EXCEPTION

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0, v5}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030029    # com.konka.mm.R.layout.samba_browser

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->setContentView(I)V

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserViewHolder;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->checkScreenMode()V

    invoke-static {}, Lcom/konka/mm/samba/SmbClient;->initSamba()V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-static {p0}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v1

    iput-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mStorageManager:Lcom/mstar/android/storage/MStorageManager;

    const v0, 0x7f0b00a9    # com.konka.mm.R.id.samba_choose_name

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textview_name:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002d    # com.konka.mm.R.layout.samba_popup_window_template

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupView:Landroid/view/View;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupView:Landroid/view/View;

    const v1, 0x7f0b00b3    # com.konka.mm.R.id.rg_sort_btns

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortBtns:Landroid/widget/RadioGroup;

    const v0, 0x7f0b002e    # com.konka.mm.R.id.page_linearlayout

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0b0030    # com.konka.mm.R.id.viewFlipper

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0031    # com.konka.mm.R.id.page1

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v5

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0032    # com.konka.mm.R.id.page2

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v3

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0033    # com.konka.mm.R.id.page3

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v4

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v5

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v4

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f040005    # com.konka.mm.R.anim.photo_push_left_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    const v0, 0x7f040009    # com.konka.mm.R.anim.push_left_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    const v0, 0x7f040006    # com.konka.mm.R.anim.photo_push_right_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    const v0, 0x7f04000a    # com.konka.mm.R.anim.push_right_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    iput v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isEnterOtherAct:Z

    const v0, 0x7f0b002f    # com.konka.mm.R.id.btn_list_left

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;

    const v0, 0x7f0b0034    # com.konka.mm.R.id.btn_list_right

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;

    const v0, 0x7f0b0035    # com.konka.mm.R.id.tv_page_info

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfomation:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findInputDlg()V

    const v0, 0x7f0b0019    # com.konka.mm.R.id.list_btn

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list_btn:Landroid/widget/Button;

    const v0, 0x7f0b00a8    # com.konka.mm.R.id.update_btn

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_update:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_update:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list_btn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortBtns:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_update:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$8;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$8;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list_btn:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$9;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$9;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupView:Landroid/view/View;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$10;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$10;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectDiskReads()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectDiskWrites()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    new-instance v0, Lcom/konka/mm/samba/GXdbHelper;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/GXdbHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->gxdbhelper:Lcom/konka/mm/samba/GXdbHelper;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->showDialog(I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mEthStateReceiver:Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->registEthernetReceiver()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090033    # com.konka.mm.R.string.MM_SCAN_FILE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/samba/SambaBrowserActivity$BtnFileDialogListener;

    invoke-direct {v2, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$BtnFileDialogListener;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogFile:Landroid/app/ProgressDialog;

    goto :goto_0

    :sswitch_1
    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090034    # com.konka.mm.R.string.MM_SCAN_SAMBA

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/samba/SambaBrowserActivity$BtnScanDialogListener;

    invoke-direct {v2, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$BtnScanDialogListener;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogScan:Landroid/app/ProgressDialog;

    goto :goto_0

    :sswitch_2
    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdate:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdate:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090035    # com.konka.mm.R.string.MM_UPDATE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdate:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    :sswitch_3
    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoading:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoading:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090036    # com.konka.mm.R.string.MM_LOADING

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoading:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    :sswitch_4
    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdateMount:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdateMount:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090037    # com.konka.mm.R.string.MM_UNMOUNT

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogUpdateMount:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    :sswitch_5
    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bShowScanDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoadFile:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoadFile:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090038    # com.konka.mm.R.string.MM_LOADING_FILE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mDialogLoadFile:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_1
        0x11 -> :sswitch_4
        0x12 -> :sswitch_5
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->unregistEthernetReceiver()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    return v0

    :sswitch_0
    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->isBtnFocuse()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_1
    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->isBtnFocuse()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->setBtnFocuseFlag(Z)V

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->setBtnFocuseFlag(Z)V

    goto :goto_0

    :sswitch_4
    iput-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->is3dKeyDown:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0xce -> :sswitch_4
        0x205 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isEnterOtherAct:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->is3dKeyDown:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->is3dKeyDown:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isEnterOtherAct:Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/high16 v3, 0x42480000    # 50.0f

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p2}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->startX:F

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->startX:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->startX:F

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->snapScreen(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->startX:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->startX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->snapScreen(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public registEthernetReceiver()V
    .locals 3

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mEthStateReceiver:Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected sambaLogin(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x1

    const-string v2, "SambaBrowserActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "login start ip =========== "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v4, v4, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v5, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v5, :cond_0

    new-instance v0, Lcom/konka/mm/samba/SmbAuthentication;

    invoke-direct {v0, p1, p2}, Lcom/konka/mm/samba/SmbAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v2, v0}, Lcom/konka/mm/samba/SmbDevice;->setAuth(Lcom/konka/mm/samba/SmbAuthentication;)V

    :goto_0
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v3, v3, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mStorageManager:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/SmbDevice;->setStorageManager(Lcom/mstar/android/storage/MStorageManager;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    new-instance v3, Lcom/konka/mm/samba/SambaBrowserActivity$18;

    invoke-direct {v3, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$18;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/SmbDevice;->setOnRecvMsg(Lcom/konka/mm/samba/OnRecvMsg;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v3, v3, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v3}, Lcom/konka/mm/samba/SmbDevice;->getSharefolderList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SmbDevice;->getAuth()Lcom/konka/mm/samba/SmbAuthentication;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v2, v1}, Lcom/konka/mm/samba/SmbDevice;->mount(Lcom/konka/mm/samba/SmbAuthentication;)V

    iput-boolean v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->scanHandler:Landroid/os/Handler;

    const/16 v3, 0x34

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v5

    :cond_0
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/SmbDevice;->setAuth(Lcom/konka/mm/samba/SmbAuthentication;)V

    goto :goto_0
.end method

.method public setBtnFocuseFlag(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_update:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list_btn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->bt_update:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    return-void
.end method

.method public setChoosedName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textview_name:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPageInfo()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfomation:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09009a    # com.konka.mm.R.string.page

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPreOrNextScreen(I)V
    .locals 9
    .param p1    # I

    const/16 v3, 0x14

    const/4 v8, 0x0

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x3

    rem-int/lit8 v6, v0, 0x3

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v6

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->NumColumns:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v6

    invoke-virtual {v0, v3, v8, v3, v8}, Landroid/widget/GridView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v7, v0, v6

    new-instance v0, Lcom/konka/mm/samba/MySambaAdapter;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    add-int v3, v1, p1

    iget v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/mm/samba/MySambaAdapter;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/util/List;III)V

    invoke-virtual {v7, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v6

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v8}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v6

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$11;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$11;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    sub-int v4, v0, v1

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    if-lt v4, v0, :cond_2

    iget v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    goto :goto_1
.end method

.method public showInputDlg()V
    .locals 6

    const/16 v5, 0x12c

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mScreen_mode:I

    if-nez v0, :cond_2

    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v5, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    :goto_0
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$16;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$16;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_user:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_pass:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_user:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->edit_user:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, v4, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z

    :cond_1
    return-void

    :cond_2
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->textEntryView:Landroid/view/View;

    const/16 v2, 0x1c2

    invoke-direct {v0, v1, v2, v5, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupWindow:Landroid/widget/PopupWindow;

    goto :goto_0
.end method

.method public snapScreen(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->setBtnFocuseFlag(Z)V

    if-ne p1, v2, :cond_3

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    invoke-virtual {p0, v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->setPreOrNextScreen(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/GridView;->requestFocus()Z

    if-ne p1, v2, :cond_4

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setSelection(I)V

    :goto_2
    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaBrowserActivity;->setPageInfo()V

    goto :goto_0

    :cond_3
    if-ne p1, v3, :cond_2

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    invoke-virtual {p0, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->setPreOrNextScreen(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->NumColumns:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_2
.end method

.method public sortBtnOnKeyOrOnClickEvent()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mScreen_mode:I

    if-nez v0, :cond_1

    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupView:Landroid/view/View;

    const/16 v2, 0x96

    const/16 v3, 0xb9

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    :goto_0
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Lcom/konka/mm/samba/SambaBrowserActivity$12;

    invoke-direct {v1, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$12;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isSortPopup:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mScreen_mode:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list_btn:Landroid/widget/Button;

    const/16 v2, -0x3d

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    :goto_1
    iput-boolean v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isSortPopup:Z

    :goto_2
    return-void

    :cond_1
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->popupView:Landroid/view/View;

    const/16 v2, 0x140

    const/16 v3, 0x113

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->list_btn:Landroid/widget/Button;

    const/16 v2, -0xbe

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iput-boolean v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isSortPopup:Z

    goto :goto_2
.end method

.method public unregistEthernetReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mEthStateReceiver:Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->mEthStateReceiver:Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public updataBtnOnKeyOrOnClickEvent()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->isUpdate:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-nez v1, :cond_1

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->showDialog(I)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;

    invoke-direct {v2, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-virtual {v1}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isCanPingHost()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    if-eqz v1, :cond_0

    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->showDialog(I)V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "onItemClick viewHolder.selectDevice.unmount"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/mm/samba/SambaBrowserActivity$13;

    invoke-direct {v2, p0}, Lcom/konka/mm/samba/SambaBrowserActivity$13;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    if-nez v1, :cond_3

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-virtual {v1}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isCanPingHost()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->showDialog(I)V

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v3, v3, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->getSelectFolder(Ljava/util/ArrayList;)Lcom/konka/mm/samba/SmbShareFolder;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    const/16 v3, 0x21

    invoke-virtual {v1, v2, v3}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->FillShareFileView(Lcom/konka/mm/samba/SmbShareFolder;I)Z
    :try_end_0
    .catch Lcom/konka/mm/samba/SambaException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/konka/mm/samba/SambaException;->printStackTrace()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-virtual {v1}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isCanPingHost()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->showDialog(I)V

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-virtual {v1}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->updateFolder()V

    goto :goto_0
.end method
