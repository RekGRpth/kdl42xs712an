.class public Lcom/konka/mm/samba/SambaBrowserViewHolder;
.super Ljava/lang/Object;
.source "SambaBrowserViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;
    }
.end annotation


# instance fields
.field protected DataMng:Lcom/konka/mm/data/sambaDataManager;

.field private final SCAN_FILE:I

.field private final SCAN_HOST:I

.field public final TAG:Ljava/lang/String;

.field protected adapter:Lcom/konka/mm/samba/MySambaAdapter;

.field protected currentFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbShareFolder;",
            ">;"
        }
    .end annotation
.end field

.field protected enterFolderLevel:I

.field protected fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

.field private gridOnKey:Landroid/view/View$OnKeyListener;

.field private hostNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected isStartFindHostName:Z

.field private isThreadfinish:Z

.field private isfristSearch:Z

.field protected list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/samba/SambaListItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mHost:Lcom/konka/mm/samba/SmbClient;

.field protected mStorageManager:Lcom/mstar/android/storage/MStorageManager;

.field protected nameList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected parentPath:Ljava/lang/String;

.field protected selectDevice:Lcom/konka/mm/samba/SmbDevice;

.field protected selectDir:Lcom/konka/mm/samba/SmbShareFolder;

.field protected seletedItemPosition:I

.field protected seletedItemPositionTarget:I

.field protected toast_login_error:Landroid/widget/Toast;

.field protected toast_mount_failed:Landroid/widget/Toast;

.field protected toast_nofound_file:Landroid/widget/Toast;

.field protected toast_noshare_file:Landroid/widget/Toast;

.field protected toast_null_pass:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 4
    .param p1    # Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "SambaBrowserViewHolder"

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->TAG:Ljava/lang/String;

    iput v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->SCAN_HOST:I

    iput v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->SCAN_FILE:I

    iput v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mHost:Lcom/konka/mm/samba/SmbClient;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mStorageManager:Lcom/mstar/android/storage/MStorageManager;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    iput v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPosition:I

    iput v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPositionTarget:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->nameList:Ljava/util/ArrayList;

    iput-boolean v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isStartFindHostName:Z

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->adapter:Lcom/konka/mm/samba/MySambaAdapter;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_nofound_file:Landroid/widget/Toast;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_mount_failed:Landroid/widget/Toast;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_login_error:Landroid/widget/Toast;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_null_pass:Landroid/widget/Toast;

    iput-object v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_noshare_file:Landroid/widget/Toast;

    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isThreadfinish:Z

    iput-boolean v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isfristSearch:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->hostNameMap:Ljava/util/Map;

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserViewHolder$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/samba/SambaBrowserViewHolder$1;-><init>(Lcom/konka/mm/samba/SambaBrowserViewHolder;)V

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->gridOnKey:Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {p1}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09008e    # com.konka.mm.R.string.directory_empty

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_nofound_file:Landroid/widget/Toast;

    invoke-virtual {p1}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090039    # com.konka.mm.R.string.MOUNT_FAILED

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_mount_failed:Landroid/widget/Toast;

    invoke-virtual {p1}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002f    # com.konka.mm.R.string.MM_ERROR_PASSWORD

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_login_error:Landroid/widget/Toast;

    invoke-virtual {p1}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002e    # com.konka.mm.R.string.MM_NULL_PASSWORD

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_null_pass:Landroid/widget/Toast;

    invoke-virtual {p1}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090030    # com.konka.mm.R.string.MM_NO_SHARE

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_noshare_file:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/samba/SambaBrowserViewHolder;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->hostNameMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/samba/SambaBrowserViewHolder;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isThreadfinish:Z

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/samba/SambaBrowserViewHolder;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isfristSearch:Z

    return v0
.end method

.method static synthetic access$3(Lcom/konka/mm/samba/SambaBrowserViewHolder;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isfristSearch:Z

    return-void
.end method


# virtual methods
.method protected FillFolderDataView(I)Z
    .locals 17
    .param p1    # I

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v14, v14, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-ne v13, v14, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v14, 0x0

    iput v14, v13, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    const/4 v13, 0x0

    :goto_0
    return v13

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x1

    if-lt v13, v14, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->getSambaFolderData(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->clear()V

    const-string v12, "file"

    const-string v8, ""

    const-string v6, ""

    const-string v7, ""

    const-string v9, ""

    const/4 v1, 0x0

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lt v4, v13, :cond_2

    invoke-virtual/range {p0 .. p1}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v14, 0x1

    iput v14, v13, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    new-instance v14, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v15}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09003a    # com.konka.mm.R.string.request_ip

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, "  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v15}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/konka/mm/samba/SambaBrowserActivity;->setChoosedName(Ljava/lang/String;)V

    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/konka/mm/samba/SmbShareFolder;

    :try_start_0
    invoke-virtual {v11}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11}, Lcom/konka/mm/samba/SmbShareFolder;->localPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11}, Lcom/konka/mm/samba/SmbShareFolder;->remotePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11}, Lcom/konka/mm/samba/SmbShareFolder;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11}, Lcom/konka/mm/samba/SmbShareFolder;->canRead()Z

    move-result v1

    invoke-virtual {v11}, Lcom/konka/mm/samba/SmbShareFolder;->isDirectory()Z

    move-result v13

    if-eqz v13, :cond_3

    const-string v12, "dir"
    :try_end_0
    .catch Lcom/konka/mm/samba/SambaException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_2
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    new-instance v5, Lcom/konka/mm/samba/SambaListItem;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v5, v10, v12, v13}, Lcom/konka/mm/samba/SambaListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v13, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/konka/mm/samba/SambaException;->printStackTrace()V

    goto :goto_2
.end method

.method protected FillHostDataView(I)Z
    .locals 10
    .param p1    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mHost:Lcom/konka/mm/samba/SmbClient;

    invoke-virtual {p0, v7}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->getSambaHostData(Lcom/konka/mm/samba/SmbClient;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_1

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v8}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v8}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090005    # com.konka.mm.R.string.LaunchShare

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/mm/finals/CommonFinals;->quitDialog(Landroid/app/Activity;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v4, :cond_2

    invoke-virtual {p0, p1}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const-string v8, ""

    invoke-virtual {v7, v8}, Lcom/konka/mm/samba/SambaBrowserActivity;->setChoosedName(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v6, :cond_0

    move v5, v6

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Lcom/konka/mm/samba/SambaListItem;

    invoke-direct {v2, v0}, Lcom/konka/mm/samba/SambaListItem;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected FillShareFileView(Lcom/konka/mm/samba/SmbShareFolder;I)Z
    .locals 10
    .param p1    # Lcom/konka/mm/samba/SmbShareFolder;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/konka/mm/samba/SambaException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/konka/mm/samba/SmbShareFolder;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v7, Lcom/konka/mm/data/sambaDataManager;

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v7, v8}, Lcom/konka/mm/data/sambaDataManager;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    new-instance v4, Ljava/io/File;

    invoke-virtual {p1}, Lcom/konka/mm/samba/SmbShareFolder;->localPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v7, v4, v8}, Lcom/konka/mm/data/sambaDataManager;->browseTo(Ljava/io/File;Landroid/content/Context;)Z

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    invoke-virtual {v7}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataCount()I

    move-result v0

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    invoke-virtual {v7}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v3, :cond_1

    invoke-virtual {p0, p2}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v7, v7, Lcom/konka/mm/samba/SambaBrowserActivity;->scanHandler:Landroid/os/Handler;

    const/16 v8, 0x31

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const/4 v7, 0x0

    return v7

    :cond_1
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/data/OtherData;

    new-instance v6, Lcom/konka/mm/samba/SambaListItem;

    invoke-virtual {v1}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/konka/mm/data/OtherData;->getType()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/mm/samba/SambaListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/konka/mm/samba/SambaListItem;->setFileName(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/mm/data/OtherData;->getFormat()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/konka/mm/samba/SambaListItem;->setFormat(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/konka/mm/samba/SambaListItem;->setPath(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected PlayFile()V
    .locals 6

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    if-eqz v3, :cond_0

    iget v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPosition:I

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    invoke-virtual {v3}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/OtherData;

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    array-length v3, v3

    if-gtz v3, :cond_1

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090031    # com.konka.mm.R.string.MM_NO_FILE

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->isDir()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->hasMultiMediaFile(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_nofound_file:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_nofound_file:Landroid/widget/Toast;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_nofound_file:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->cancel()V

    :cond_3
    iget v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPosition:I

    invoke-virtual {p0, v3}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolder(I)V

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPosition:I

    invoke-virtual {p0, v3}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->processListItem(I)V

    goto :goto_0
.end method

.method protected enterFolder(I)V
    .locals 10
    .param p1    # I

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "enterFolderLevel = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    invoke-virtual {v7}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/konka/mm/data/OtherData;

    invoke-virtual {v7}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v6

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v7, v3, v8}, Lcom/konka/mm/data/sambaDataManager;->browseTo(Ljava/io/File;Landroid/content/Context;)Z

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    invoke-virtual {v7}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v2, :cond_0

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0xb

    invoke-virtual {p0, v7}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    return-void

    :cond_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/OtherData;

    new-instance v5, Lcom/konka/mm/samba/SambaListItem;

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getType()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v5, v7, v8, v9}, Lcom/konka/mm/samba/SambaListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/konka/mm/samba/SambaListItem;->setFileName(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFormat()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/konka/mm/samba/SambaListItem;->setFormat(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/konka/mm/samba/SambaListItem;->setPath(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getListCurPos(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getSambaFolderData(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbShareFolder;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbShareFolder;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/samba/SmbShareFolder;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected getSambaHostData(Lcom/konka/mm/samba/SmbClient;)Ljava/util/ArrayList;
    .locals 7
    .param p1    # Lcom/konka/mm/samba/SmbClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/konka/mm/samba/SmbClient;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/konka/mm/samba/SmbClient;->getSmbDeviceList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v1, :cond_1

    iget-boolean v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isThreadfinish:Z

    if-eqz v6, :cond_0

    new-instance v2, Ljava/lang/Thread;

    new-instance v6, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;

    invoke-direct {v6, p0, v0, v1}, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;-><init>(Lcom/konka/mm/samba/SambaBrowserViewHolder;Ljava/util/ArrayList;I)V

    invoke-direct {v2, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isThreadfinish:Z

    :cond_0
    return-object v5

    :cond_1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->hostNameMap:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->hostNameMap:Ljava/util/Map;

    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected getSelectFolder(Ljava/util/ArrayList;)Lcom/konka/mm/samba/SmbShareFolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbShareFolder;",
            ">;)",
            "Lcom/konka/mm/samba/SmbShareFolder;"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPositionTarget:I

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/samba/SmbShareFolder;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getSelectSmbDevice()Lcom/konka/mm/samba/SmbDevice;
    .locals 3

    iget v1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPosition:I

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mHost:Lcom/konka/mm/samba/SmbClient;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SmbClient;->getSmbDeviceList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/samba/SmbDevice;

    iput-object v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    return-object v2
.end method

.method public hasMultiMediaFile(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const/4 v2, 0x0

    :goto_1
    array-length v5, v1

    if-ge v2, v5, :cond_0

    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    goto :goto_0

    :cond_2
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public isCanPingHost()Z
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->mHost:Lcom/konka/mm/samba/SmbClient;

    if-nez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SmbDevice;->remotePath()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090032    # com.konka.mm.R.string.MM_CONPUTER_UNCONNECT

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method protected processListItem(I)V
    .locals 30
    .param p1    # I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/konka/mm/samba/SambaListItem;

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SambaListItem;->getPath()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/konka/mm/samba/SambaListItem;

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SambaListItem;->getFormat()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/konka/mm/samba/SambaListItem;

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SambaListItem;->getFileName()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SmbDevice;->remotePath()Ljava/lang/String;

    move-result-object v24

    const/16 v27, 0x2

    move-object/from16 v0, v24

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const/high16 v28, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v23

    const/4 v4, 0x0

    const/16 v25, 0x0

    const/16 v21, 0x0

    const/4 v13, 0x0

    :goto_0
    array-length v0, v6

    move/from16 v27, v0

    move/from16 v0, v27

    if-lt v13, v0, :cond_3

    :goto_1
    if-nez v4, :cond_0

    if-nez v21, :cond_0

    const/4 v13, 0x0

    :goto_2
    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    if-lt v13, v0, :cond_5

    :cond_0
    :goto_3
    if-nez v25, :cond_1

    if-nez v4, :cond_1

    const/4 v13, 0x0

    :goto_4
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    if-lt v13, v0, :cond_7

    :cond_1
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/samba/SambaBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    check-cast v18, Lcom/konka/mm/GlobalData;

    if-eqz v4, :cond_b

    new-instance v15, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    const-class v28, Lcom/konka/mm/music/MusicActivity;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v13, 0x0

    :goto_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-lt v13, v0, :cond_9

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/konka/mm/GlobalData;->setmMMFileList(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->getListCurPos(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v14

    const-string v27, "com.konka.mm.file.index.posstion"

    move-object/from16 v0, v27

    invoke-virtual {v7, v0, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v27, "com.konka.mm.file.where.come.from"

    const-string v28, "com.konka.mm.file.come.from.samba"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v27, "com.konka.mm.samba.current.ip"

    move-object/from16 v0, v27

    invoke-virtual {v7, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Lcom/konka/mm/samba/SambaBrowserActivity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    :goto_7
    return-void

    :cond_3
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "."

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    aget-object v28, v6, v13

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    const/4 v4, 0x1

    const/16 v25, 0x0

    const/16 v21, 0x0

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    :cond_5
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "."

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    aget-object v28, v26, v13

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    const/4 v4, 0x0

    const/16 v25, 0x1

    const/16 v21, 0x0

    goto/16 :goto_3

    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    :cond_7
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "."

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    aget-object v28, v23, v13

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    const/16 v21, 0x1

    const/4 v4, 0x0

    const/16 v25, 0x0

    goto/16 :goto_5

    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_4

    :cond_9
    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/mm/data/OtherData;

    invoke-virtual {v9}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_a

    invoke-virtual {v9}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_6

    :cond_b
    if-eqz v25, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v17

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    const/4 v13, 0x0

    :goto_8
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-lt v13, v0, :cond_c

    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v28, "the movie play list:"

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v16, 0x0

    :goto_9
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v16

    move/from16 v1, v27

    if-lt v0, v1, :cond_e

    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "current playing movie path: "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    const-string v27, "com.konka.kkvideoplayer"

    const-string v28, "com.konka.kkvideoplayer.VideoPlayerMain"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v27, "host_ip"

    move-object/from16 v0, v27

    invoke-virtual {v7, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v27, "videofile"

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v27, "com.konka.mm.movie.list"

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v15, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v27, "KONKA_MM"

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "------>goto movie theater["

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "]"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Lcom/konka/mm/samba/SambaBrowserActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_7

    :catch_0
    move-exception v10

    const-string v27, "KONKA_MM"

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    :cond_c
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/mm/data/OtherData;

    invoke-virtual {v9}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const v29, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_d

    invoke-virtual {v9}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_8

    :cond_e
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v27, Ljava/lang/StringBuilder;

    add-int/lit8 v29, v16, 0x1

    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v29, ":  "

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_9

    :cond_f
    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput-boolean v0, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->isEnterOtherAct:Z

    new-instance v15, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    const-class v28, Lcom/konka/mm/photo/AutoShowPicActivity;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v3

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    const/4 v13, 0x0

    :goto_a
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-lt v13, v0, :cond_10

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/konka/mm/GlobalData;->setmMMFileList(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->getListCurPos(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v14

    const-string v27, "com.konka.mm.file.index.posstion"

    move-object/from16 v0, v27

    invoke-virtual {v7, v0, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v27, "com.konka.mm.file.where.come.from"

    const-string v28, "com.konka.mm.file.come.from.samba"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v27, "com.konka.mm.samba.current.ip"

    move-object/from16 v0, v27

    invoke-virtual {v7, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Lcom/konka/mm/samba/SambaBrowserActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_7

    :cond_10
    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/mm/data/OtherData;

    invoke-virtual {v9}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v28

    const/high16 v29, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual/range {v28 .. v29}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_11

    invoke-virtual {v9}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_11
    add-int/lit8 v13, v13, 0x1

    goto :goto_a
.end method

.method protected returnToPreviousFolder()V
    .locals 11

    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v10, 0x0

    const/16 v9, 0x16

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v6, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-ne v7, v6, :cond_1

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-static {v6}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0, v9}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->FillHostDataView(I)Z

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput v10, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v6, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-ne v8, v6, :cond_2

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0, v9}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->FillFolderDataView(I)Z

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput v7, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    goto :goto_0

    :cond_2
    iget v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    if-lez v6, :cond_0

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v6, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-ne v8, v6, :cond_0

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "parentPath "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6, v5, v7}, Lcom/konka/mm/data/sambaDataManager;->browseTo(Ljava/io/File;Landroid/content/Context;)Z

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    invoke-virtual {v6}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_1
    if-lt v3, v2, :cond_3

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0, v9}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    iget v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->enterFolderLevel:I

    goto :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/OtherData;

    new-instance v4, Lcom/konka/mm/samba/SambaListItem;

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getType()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v4, v6, v7, v8}, Lcom/konka/mm/samba/SambaListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/konka/mm/samba/SambaListItem;->setFileName(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFormat()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/konka/mm/samba/SambaListItem;->setFormat(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/konka/mm/samba/SambaListItem;->setPath(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public setAdapter(I)V
    .locals 16
    .param p1    # I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-boolean v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->isUpdate:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v2, 0x0

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0xb

    move/from16 v0, p1

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-boolean v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->isUpdate:Z

    if-nez v1, :cond_1

    new-instance v12, Lcom/konka/mm/model/PageInfo;

    invoke-direct {v12}, Lcom/konka/mm/model/PageInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    mul-int/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->curItemNum:I

    add-int v14, v1, v2

    invoke-virtual {v12, v14}, Lcom/konka/mm/model/PageInfo;->setPos(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v2, 0x0

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/16 v2, 0x21

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v2, 0x3

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->NumColumns:I

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    int-to-float v2, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v3, v3, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    const/4 v2, 0x3

    if-gt v1, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v8, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    :goto_1
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x16

    move/from16 v0, p1

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/konka/mm/model/PageInfo;

    invoke-virtual {v12}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    div-int v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput v10, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    if-nez v10, :cond_2

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_8

    const/4 v8, 0x3

    :cond_2
    :goto_2
    const/4 v1, 0x1

    if-lt v10, v1, :cond_3

    const/4 v7, -0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ne v1, v10, :cond_9

    const/4 v8, 0x2

    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x21

    move/from16 v0, p1

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_5

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->returnToPreviousFolder()V

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v9, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    add-int/2addr v1, v8

    if-lt v9, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v1, 0x16

    move/from16 v0, p1

    if-ne v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/konka/mm/model/PageInfo;

    :goto_5
    invoke-virtual {v12}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    div-int v11, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    aget-object v1, v1, v2

    invoke-virtual {v12}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v3, v3, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    rem-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelection(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v12}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v3, v3, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    div-int/2addr v2, v3

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->setPageInfo()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->isUpdate:Z

    return-void

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/16 v2, 0x12

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v2, 0x6

    iput v2, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->NumColumns:I

    goto/16 :goto_0

    :cond_7
    const/4 v8, 0x3

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v8, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-le v1, v10, :cond_3

    const/4 v8, 0x3

    goto/16 :goto_3

    :cond_a
    add-int v1, v10, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->curScreen:I

    sub-int/2addr v1, v2

    add-int v4, v1, v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ge v4, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v5, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    :cond_b
    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    const/16 v2, 0x14

    const/4 v3, 0x0

    const/16 v6, 0x14

    const/4 v15, 0x0

    invoke-virtual {v1, v2, v3, v6, v15}, Landroid/widget/GridView;->setPadding(IIII)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mScreen_mode:I

    if-nez v1, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    const/16 v2, -0x14

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v15, 0x0

    invoke-virtual {v1, v2, v3, v6, v15}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :goto_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->NumColumns:I

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->gridOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v15, v1, v2

    new-instance v1, Lcom/konka/mm/samba/MySambaAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v6, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    invoke-direct/range {v1 .. v6}, Lcom/konka/mm/samba/MySambaAdapter;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/util/List;III)V

    invoke-virtual {v15, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setFocusable(Z)V

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    sub-int v5, v13, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    if-lt v5, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v5, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    goto/16 :goto_7

    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    const/16 v2, -0x1e

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    goto/16 :goto_8

    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v2, v9, v7

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x3

    aget-object v1, v1, v2

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    const/16 v3, 0x14

    const/4 v6, 0x0

    const/4 v15, 0x0

    invoke-virtual {v1, v2, v3, v6, v15}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto/16 :goto_9

    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/konka/mm/model/PageInfo;

    goto/16 :goto_5

    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_6
.end method

.method protected updateFolder()V
    .locals 9

    new-instance v3, Ljava/io/File;

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->parentPath:Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6, v3, v7}, Lcom/konka/mm/data/sambaDataManager;->browseTo(Ljava/io/File;Landroid/content/Context;)Z

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->DataMng:Lcom/konka/mm/data/sambaDataManager;

    invoke-virtual {v6}, Lcom/konka/mm/data/sambaDataManager;->getSambaDataList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v2, :cond_0

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0x21

    invoke-virtual {p0, v6}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v6, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->scanHandler:Landroid/os/Handler;

    const/16 v7, 0x31

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/OtherData;

    new-instance v5, Lcom/konka/mm/samba/SambaListItem;

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getType()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/mm/samba/SambaListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/konka/mm/samba/SambaListItem;->setFileName(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getFormat()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/konka/mm/samba/SambaListItem;->setFormat(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/konka/mm/samba/SambaListItem;->setPath(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
