.class public Lcom/konka/mm/samba/SambaListItem;
.super Ljava/lang/Object;
.source "SambaListItem.java"


# instance fields
.field public description:Ljava/lang/String;

.field public format:Ljava/lang/String;

.field public icon:I

.field public isChecked:Z

.field private name:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/konka/mm/samba/SambaListItem;->isChecked:Z

    const v1, 0x7f0200d9    # com.konka.mm.R.drawable.share_icon_uns

    iput v1, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    iput-object p1, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v4, p0, Lcom/konka/mm/samba/SambaListItem;->isChecked:Z

    const-string v2, "return"

    if-ne p2, v2, :cond_0

    const v2, 0x7f020098    # com.konka.mm.R.drawable.pic_icon_anticlockwise_s

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    iput-object p1, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    :cond_0
    const-string v2, "dir"

    if-ne p2, v2, :cond_1

    const v2, 0x7f020069    # com.konka.mm.R.drawable.media_default_file_uns

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    iput-object p1, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    :cond_1
    const-string v2, "file"

    if-ne p2, v2, :cond_3

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/konka/mm/samba/SambaListItem;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    :goto_0
    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/samba/SambaListItem;->format:Ljava/lang/String;

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070006    # com.konka.mm.R.array.fileEndingWebText

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/konka/mm/samba/SambaListItem;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x7f02000f    # com.konka.mm.R.drawable.com_button_arrange_uns

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    goto :goto_0

    :cond_5
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070005    # com.konka.mm.R.array.fileEndingPackage

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/konka/mm/samba/SambaListItem;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const v2, 0x7f020015    # com.konka.mm.R.drawable.com_button_sortord_uns

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    goto :goto_0

    :cond_6
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/konka/mm/samba/SambaListItem;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const v2, 0x7f02006b    # com.konka.mm.R.drawable.media_default_music_uns

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    goto :goto_0

    :cond_7
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/konka/mm/samba/SambaListItem;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const v2, 0x7f02006c    # com.konka.mm.R.drawable.media_default_video_uns

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    goto/16 :goto_0

    :cond_8
    const v2, 0x7f020077    # com.konka.mm.R.drawable.music_icon_cycle_uns

    iput v2, p0, Lcom/konka/mm/samba/SambaListItem;->icon:I

    goto/16 :goto_0
.end method

.method static checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    const/4 v1, 0x0

    array-length v3, p1

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    :goto_1
    return v1

    :cond_0
    aget-object v0, p1, v2

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaListItem;->format:Ljava/lang/String;

    return-object v0
.end method

.method public getHostName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SambaListItem;->path:Ljava/lang/String;

    return-object v0
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    return-void
.end method

.method public setFormat(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/samba/SambaListItem;->format:Ljava/lang/String;

    return-void
.end method

.method public setHostName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/samba/SambaListItem;->name:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/samba/SambaListItem;->path:Ljava/lang/String;

    return-void
.end method
