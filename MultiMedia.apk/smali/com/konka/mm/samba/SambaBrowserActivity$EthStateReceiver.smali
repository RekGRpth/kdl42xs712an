.class public Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SambaBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EthStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-static {v0}, Lcom/konka/mm/model/FamilyShareModel;->isConneted2Network(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002d    # com.konka.mm.R.string.MM_NET_UNCONNECT

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$EthStateReceiver;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->finish()V

    :cond_0
    return-void
.end method
