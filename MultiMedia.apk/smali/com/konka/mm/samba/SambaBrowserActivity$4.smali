.class Lcom/konka/mm/samba/SambaBrowserActivity$4;
.super Ljava/lang/Object;
.source "SambaBrowserActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onItemClick ThreadState: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v7, v7, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput p3, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->curItemNum:I

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v5, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->mCountPerPage:I

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v6, v6, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    mul-int/2addr v5, v6

    add-int v2, p3, v5

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iput v2, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPosition:I

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v5, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->getSelectSmbDevice()Lcom/konka/mm/samba/SmbDevice;

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const-string v6, ""

    invoke-static {v5, v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$4(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const-string v6, ""

    invoke-static {v5, v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$5(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput v2, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->currentHost:I

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isCanPingHost()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v6, 0x1

    iput v6, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    new-instance v3, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v3, v5}, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    new-instance v4, Ljava/lang/Thread;

    invoke-direct {v4, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v5, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_7

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-boolean v5, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    if-eqz v5, :cond_7

    :try_start_0
    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iput v2, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->seletedItemPositionTarget:I

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v6

    iget-object v7, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v7}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v7

    iget-object v7, v7, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->getSelectFolder(Ljava/util/ArrayList;)Lcom/konka/mm/samba/SmbShareFolder;

    move-result-object v6

    iput-object v6, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isCanPingHost()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    invoke-virtual {v5}, Lcom/konka/mm/samba/SmbShareFolder;->localPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_3

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_mount_failed:Landroid/widget/Toast;

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/konka/mm/samba/SambaException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/konka/mm/samba/SambaException;->printStackTrace()V

    goto/16 :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_mount_failed:Landroid/widget/Toast;

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    array-length v5, v5

    if-gtz v5, :cond_5

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090031    # com.konka.mm.R.string.MM_NO_FILE

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v6

    iget-object v6, v6, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    invoke-virtual {v6}, Lcom/konka/mm/samba/SmbShareFolder;->localPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->hasMultiMediaFile(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_mount_failed:Landroid/widget/Toast;

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_6
    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v6}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v6

    iget-object v6, v6, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDir:Lcom/konka/mm/samba/SmbShareFolder;

    const/16 v7, 0xb

    invoke-virtual {v5, v6, v7}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->FillShareFileView(Lcom/konka/mm/samba/SmbShareFolder;I)Z

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v6, 0x5

    iput v6, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I
    :try_end_1
    .catch Lcom/konka/mm/samba/SambaException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_7
    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v5, v5, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->isCanPingHost()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/konka/mm/samba/SambaBrowserActivity$4;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->PlayFile()V

    goto/16 :goto_0
.end method
