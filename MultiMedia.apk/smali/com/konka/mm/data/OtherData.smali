.class public Lcom/konka/mm/data/OtherData;
.super Ljava/lang/Object;
.source "OtherData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/konka/mm/data/OtherData;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/konka/mm/data/OtherData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private description:Ljava/lang/String;

.field private fileName:Ljava/lang/String;

.field private format:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/konka/mm/data/OtherData$1;

    invoke-direct {v0}, Lcom/konka/mm/data/OtherData$1;-><init>()V

    sput-object v0, Lcom/konka/mm/data/OtherData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/data/OtherData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->path:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/data/OtherData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->fileName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/mm/data/OtherData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->type:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/data/OtherData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->format:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/data/OtherData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/konka/mm/data/OtherData;)I
    .locals 3
    .param p1    # Lcom/konka/mm/data/OtherData;

    iget-object v1, p0, Lcom/konka/mm/data/OtherData;->fileName:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/OtherData;->fileName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/mm/data/OtherData;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    return v1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/konka/mm/data/OtherData;

    invoke-virtual {p0, p1}, Lcom/konka/mm/data/OtherData;->compareTo(Lcom/konka/mm/data/OtherData;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->format:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->type:Ljava/lang/String;

    return-object v0
.end method

.method public isDir()Z
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->type:Ljava/lang/String;

    const-string v1, "dir"

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->description:Ljava/lang/String;

    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->fileName:Ljava/lang/String;

    return-void
.end method

.method public setFormat(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->format:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->path:Ljava/lang/String;

    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/OtherData;->type:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->path:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->fileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->format:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/OtherData;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
