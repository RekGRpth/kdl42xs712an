.class public Lcom/konka/mm/data/PhotoManager;
.super Ljava/lang/Object;
.source "PhotoManager.java"


# static fields
.field protected static photoFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/PhotoFodler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method


# virtual methods
.method public addPhotoFolder(Lcom/konka/mm/data/PhotoFodler;)V
    .locals 1
    .param p1    # Lcom/konka/mm/data/PhotoFodler;

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public clearAllFolder()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public getPhotoFolder(I)Lcom/konka/mm/data/PhotoFodler;
    .locals 1
    .param p1    # I

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/PhotoFodler;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPhotoFolder(ILandroid/content/Context;)Lcom/konka/mm/data/PhotoFodler;
    .locals 11
    .param p1    # I
    .param p2    # Landroid/content/Context;

    const/4 v4, 0x0

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/mm/data/PhotoFodler;

    invoke-virtual {v6}, Lcom/konka/mm/data/PhotoFodler;->getPhotoDataCount()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/data/BaseData;->IMAGE_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "bucket_id="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/konka/mm/data/PhotoFodler;->getBucketId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_0

    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    new-instance v9, Lcom/konka/mm/data/PhotoData;

    invoke-direct {v9}, Lcom/konka/mm/data/PhotoData;-><init>()V

    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v9, v0, v1}, Lcom/konka/mm/data/PhotoData;->setId(J)V

    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/PhotoData;->setPath(Ljava/lang/String;)V

    const-string v0, "_size"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v9, v0, v1}, Lcom/konka/mm/data/PhotoData;->setSize(J)V

    const-string v0, "_display_name"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/PhotoData;->setDisplayName(Ljava/lang/String;)V

    const-string v0, "mime_type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/PhotoData;->setMimeType(Ljava/lang/String;)V

    const-string v0, "title"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/PhotoData;->setTitle(Ljava/lang/String;)V

    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_data"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "image_id="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_4

    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    invoke-virtual {v6, v9}, Lcom/konka/mm/data/PhotoFodler;->addPhotoData(Lcom/konka/mm/data/PhotoData;)V

    goto/16 :goto_1
.end method

.method public getPhotoFolderCount()I
    .locals 1

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPhotoFolderData(II)Ljava/util/ArrayList;
    .locals 4
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/PhotoFodler;",
            ">;"
        }
    .end annotation

    add-int/lit8 v3, p2, -0x1

    mul-int v0, p1, v3

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    sget-object v3, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    if-lt v1, p1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    sget-object v3, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/data/PhotoFodler;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isOpened(I)Z
    .locals 1
    .param p1    # I

    sget-object v0, Lcom/konka/mm/data/PhotoManager;->photoFolderList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/PhotoFodler;

    invoke-virtual {v0}, Lcom/konka/mm/data/PhotoFodler;->getPhotoDataCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
