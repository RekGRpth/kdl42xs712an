.class public abstract Lcom/konka/mm/data/BaseData;
.super Ljava/lang/Object;
.source "BaseData.java"

# interfaces
.implements Lcom/konka/mm/data/IData;


# static fields
.field protected static final AUDIO_PROJECTION:[Ljava/lang/String;

.field protected static final IMAGE_PROJECTION:[Ljava/lang/String;

.field protected static final VIDEO_PROJECTION:[Ljava/lang/String;

.field protected static audioManager:Lcom/konka/mm/data/AudioManager;

.field protected static photoManager:Lcom/konka/mm/data/PhotoManager;

.field protected static videoManager:Lcom/konka/mm/data/VideoManager;


# instance fields
.field protected lowMemoryMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_size"

    aput-object v1, v0, v5

    const-string v1, "_display_name"

    aput-object v1, v0, v6

    const-string v1, "mime_type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "date_added"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/data/BaseData;->IMAGE_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_display_name"

    aput-object v1, v0, v5

    const-string v1, "_size"

    aput-object v1, v0, v6

    const-string v1, "mime_type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/data/BaseData;->VIDEO_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "_size"

    aput-object v1, v0, v5

    const-string v1, "_display_name"

    aput-object v1, v0, v6

    const-string v1, "mime_type"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "album_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/data/BaseData;->AUDIO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/data/BaseData;->lowMemoryMode:Z

    return-void
.end method


# virtual methods
.method public clearAudioManager()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/AudioManager;->clearAll()V

    return-void
.end method

.method public clearPhotoManager()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/PhotoManager;->clearAllFolder()V

    return-void
.end method

.method public clearVideoManager()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/VideoManager;->clearAllFolder()V

    return-void
.end method

.method public create()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/mm/data/PhotoManager;

    invoke-direct {v0}, Lcom/konka/mm/data/PhotoManager;-><init>()V

    sput-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    :cond_0
    return-void
.end method

.method public destory()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/PhotoManager;->clearAllFolder()V

    :cond_0
    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/VideoManager;->clearAllFolder()V

    :cond_1
    sget-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/AudioManager;->clearAll()V

    :cond_2
    return-void
.end method

.method public getAllAudioData(Landroid/content/Context;)V
    .locals 11
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/data/BaseData;->AUDIO_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-nez v10, :cond_3

    :goto_0
    return-void

    :cond_0
    new-instance v9, Lcom/konka/mm/data/AudioData;

    invoke-direct {v9}, Lcom/konka/mm/data/AudioData;-><init>()V

    const-string v0, "_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setId(I)V

    const-string v0, "title"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setTitle(Ljava/lang/String;)V

    const-string v0, "duration"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setDuration(I)V

    const-string v0, "artist"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setArtist(Ljava/lang/String;)V

    const-string v0, "_display_name"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setDisplayName(Ljava/lang/String;)V

    const-string v0, "album"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setAlbum(Ljava/lang/String;)V

    const-string v0, "_data"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setPath(Ljava/lang/String;)V

    const-string v0, "_size"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v9, v0, v1}, Lcom/konka/mm/data/AudioData;->setSize(J)V

    const-string v0, "mime_type"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/AudioData;->setMimeType(Ljava/lang/String;)V

    const-string v0, "album_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "album_art"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_2

    :cond_1
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    sget-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    invoke-virtual {v0, v9}, Lcom/konka/mm/data/AudioManager;->addAudioData(Lcom/konka/mm/data/AudioData;)V

    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_4
    const-string v0, "album_art"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v9, v6}, Lcom/konka/mm/data/AudioData;->setAlbumArt(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getAllImageFolder(Landroid/content/Context;)V
    .locals 11
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "distinct bucket_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v9, 0x0

    :cond_1
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    const-string v0, "bucket_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "min(_id)"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bucket_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_1

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_3
    new-instance v9, Lcom/konka/mm/data/PhotoFodler;

    invoke-direct {v9}, Lcom/konka/mm/data/PhotoFodler;-><init>()V

    invoke-virtual {v9, v6}, Lcom/konka/mm/data/PhotoFodler;->setBucketId(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    const-string v0, "_data"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/PhotoFodler;->setPath(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/PhotoFodler;->setName(Ljava/lang/String;)V

    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    invoke-virtual {v0, v9}, Lcom/konka/mm/data/PhotoManager;->addPhotoFolder(Lcom/konka/mm/data/PhotoFodler;)V

    goto :goto_2
.end method

.method public getAllVideoFolder(Landroid/content/Context;)V
    .locals 11
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "distinct bucket_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v9, 0x0

    :cond_1
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    const-string v0, "bucket_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "min(_id)"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_data"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bucket_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_1

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_3
    new-instance v9, Lcom/konka/mm/data/VideoFolder;

    invoke-direct {v9}, Lcom/konka/mm/data/VideoFolder;-><init>()V

    invoke-virtual {v9, v6}, Lcom/konka/mm/data/VideoFolder;->setBucketId(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    const-string v0, "_data"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/VideoFolder;->setPath(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/konka/mm/data/VideoFolder;->setName(Ljava/lang/String;)V

    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    invoke-virtual {v0, v9}, Lcom/konka/mm/data/VideoManager;->addVideoFolder(Lcom/konka/mm/data/VideoFolder;)V

    goto :goto_2
.end method

.method public getAudioDataCount()I
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/AudioManager;->getAudioDataCount()I

    move-result v0

    return v0
.end method

.method public getAudioManager(Landroid/content/Context;)Lcom/konka/mm/data/AudioManager;
    .locals 1
    .param p1    # Landroid/content/Context;

    sget-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/AudioManager;->getAudioDataCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/konka/mm/data/BaseData;->getAllAudioData(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/konka/mm/data/BaseData;->audioManager:Lcom/konka/mm/data/AudioManager;

    return-object v0
.end method

.method public getImageFolderCount()I
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/PhotoManager;->getPhotoFolderCount()I

    move-result v0

    return v0
.end method

.method public getPhotoManager(Landroid/content/Context;)Lcom/konka/mm/data/PhotoManager;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-boolean v0, p0, Lcom/konka/mm/data/BaseData;->lowMemoryMode:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/VideoManager;->clearAllFolder()V

    :cond_0
    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/PhotoManager;->getPhotoFolderCount()I

    move-result v0

    if-gtz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/konka/mm/data/BaseData;->getAllImageFolder(Landroid/content/Context;)V

    :cond_1
    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    return-object v0
.end method

.method public getVideoFolderCount()I
    .locals 1

    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/VideoManager;->getVideoFolderCount()I

    move-result v0

    return v0
.end method

.method public getVideoManager(Landroid/content/Context;)Lcom/konka/mm/data/VideoManager;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-boolean v0, p0, Lcom/konka/mm/data/BaseData;->lowMemoryMode:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/BaseData;->photoManager:Lcom/konka/mm/data/PhotoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/PhotoManager;->clearAllFolder()V

    :cond_0
    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    invoke-virtual {v0}, Lcom/konka/mm/data/VideoManager;->getVideoFolderCount()I

    move-result v0

    if-gtz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/konka/mm/data/BaseData;->getAllVideoFolder(Landroid/content/Context;)V

    :cond_1
    sget-object v0, Lcom/konka/mm/data/BaseData;->videoManager:Lcom/konka/mm/data/VideoManager;

    return-object v0
.end method

.method public setMemoryMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/mm/data/BaseData;->lowMemoryMode:Z

    return-void
.end method
