.class public interface abstract Lcom/konka/mm/data/IData;
.super Ljava/lang/Object;
.source "IData.java"


# virtual methods
.method public abstract create()V
.end method

.method public abstract destory()V
.end method

.method public abstract getAudioManager(Landroid/content/Context;)Lcom/konka/mm/data/AudioManager;
.end method

.method public abstract getPhotoManager(Landroid/content/Context;)Lcom/konka/mm/data/PhotoManager;
.end method

.method public abstract getVideoManager(Landroid/content/Context;)Lcom/konka/mm/data/VideoManager;
.end method

.method public abstract setMemoryMode(Z)V
.end method
