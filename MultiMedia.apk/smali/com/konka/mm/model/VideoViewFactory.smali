.class public Lcom/konka/mm/model/VideoViewFactory;
.super Ljava/lang/Object;
.source "VideoViewFactory.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# instance fields
.field mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/konka/mm/model/VideoViewFactory;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public makeView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/konka/mm/model/VideoViewFactory;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03003a    # com.konka.mm.R.layout.video_browser2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
