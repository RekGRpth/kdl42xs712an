.class public Lcom/konka/mm/model/PathItem;
.super Ljava/lang/Object;
.source "PathItem.java"


# instance fields
.field private btn:Landroid/widget/Button;

.field private path:Ljava/lang/String;

.field private textview:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/widget/Button;Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/widget/Button;
    .param p3    # Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/model/PathItem;->path:Ljava/lang/String;

    iput-object p2, p0, Lcom/konka/mm/model/PathItem;->btn:Landroid/widget/Button;

    iput-object p3, p0, Lcom/konka/mm/model/PathItem;->textview:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public getBtn()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/PathItem;->btn:Landroid/widget/Button;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/PathItem;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getTextview()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/PathItem;->textview:Landroid/widget/TextView;

    return-object v0
.end method

.method public setBtn(Landroid/widget/Button;)V
    .locals 0
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/konka/mm/model/PathItem;->btn:Landroid/widget/Button;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/PathItem;->path:Ljava/lang/String;

    return-void
.end method

.method public setTextview(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/mm/model/PathItem;->textview:Landroid/widget/TextView;

    return-void
.end method
