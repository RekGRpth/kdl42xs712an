.class public Lcom/konka/mm/model/Global;
.super Ljava/lang/Object;
.source "Global.java"


# static fields
.field private static bcursorVideoFileInit:Z

.field private static cursorVideoFile:Landroid/database/Cursor;

.field private static eActiveModule:Lcom/konka/mm/finals/CommonFinals$MMModule;

.field private static eErrorCode:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActiveModule()Lcom/konka/mm/finals/CommonFinals$MMModule;
    .locals 1

    sget-object v0, Lcom/konka/mm/model/Global;->eActiveModule:Lcom/konka/mm/finals/CommonFinals$MMModule;

    return-object v0
.end method

.method public static getErrorCode()Lcom/konka/mm/finals/CommonFinals$MMErrorCode;
    .locals 1

    sget-object v0, Lcom/konka/mm/model/Global;->eErrorCode:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    return-object v0
.end method

.method public static setActiveModule(Lcom/konka/mm/finals/CommonFinals$MMModule;)V
    .locals 0
    .param p0    # Lcom/konka/mm/finals/CommonFinals$MMModule;

    sput-object p0, Lcom/konka/mm/model/Global;->eActiveModule:Lcom/konka/mm/finals/CommonFinals$MMModule;

    return-void
.end method

.method public static setErrorCode(Lcom/konka/mm/finals/CommonFinals$MMErrorCode;)V
    .locals 0
    .param p0    # Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    sput-object p0, Lcom/konka/mm/model/Global;->eErrorCode:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    return-void
.end method
