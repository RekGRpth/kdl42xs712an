.class public Lcom/konka/mm/model/AudioTrackInfo;
.super Ljava/lang/Object;
.source "AudioTrackInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AudioTrackInfo"


# instance fields
.field private album:Ljava/lang/String;

.field private artist:Ljava/lang/String;

.field private biteRate:I

.field private codecID:I

.field private currentPlayTime:I

.field private sampleRate:I

.field private title:Ljava/lang/String;

.field private totalPlayTime:I

.field private year:I


# direct methods
.method public constructor <init>(ZLcom/konka/mm/model/Metadata;)V
    .locals 3
    .param p1    # Z
    .param p2    # Lcom/konka/mm/model/Metadata;

    const/16 v0, 0x21

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_1

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->codecID:I

    const/16 v0, 0x11

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->biteRate:I

    const/16 v0, 0x22

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->year:I

    const/16 v0, 0x14

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->sampleRate:I

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->title:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->album:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->artist:Ljava/lang/String;

    iput v1, p0, Lcom/konka/mm/model/AudioTrackInfo;->totalPlayTime:I

    iput v1, p0, Lcom/konka/mm/model/AudioTrackInfo;->currentPlayTime:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->codecID:I

    const/16 v0, 0xa

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->totalPlayTime:I

    const/16 v0, 0x23

    invoke-virtual {p2, v0}, Lcom/konka/mm/model/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->currentPlayTime:I

    iput v1, p0, Lcom/konka/mm/model/AudioTrackInfo;->biteRate:I

    iput v1, p0, Lcom/konka/mm/model/AudioTrackInfo;->year:I

    iput v1, p0, Lcom/konka/mm/model/AudioTrackInfo;->sampleRate:I

    iput-object v2, p0, Lcom/konka/mm/model/AudioTrackInfo;->title:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/model/AudioTrackInfo;->album:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/model/AudioTrackInfo;->artist:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getAlbum()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->album:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Album!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->album:Ljava/lang/String;

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->artist:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Artist!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getBiteRate()I
    .locals 2

    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->biteRate:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get BiteRate!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->biteRate:I

    return v0
.end method

.method public getCodecID()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->codecID:I

    return v0
.end method

.method public getCurrentPlayTime()I
    .locals 2

    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->currentPlayTime:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only vedio type can get CurrentPlayTime!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->currentPlayTime:I

    return v0
.end method

.method public getSampleRate()I
    .locals 2

    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->sampleRate:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get SampleRate!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->sampleRate:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Title!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalPlayTime()I
    .locals 2

    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->totalPlayTime:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only vedio type can get TotalPlayTime!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->totalPlayTime:I

    return v0
.end method

.method public getYear()I
    .locals 2

    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->year:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Year!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/konka/mm/model/AudioTrackInfo;->year:I

    return v0
.end method
