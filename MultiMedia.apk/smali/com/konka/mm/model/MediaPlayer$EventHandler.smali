.class Lcom/konka/mm/model/MediaPlayer$EventHandler;
.super Landroid/os/Handler;
.source "MediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/model/MediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

.field final synthetic this$0:Lcom/konka/mm/model/MediaPlayer;


# direct methods
.method public constructor <init>(Lcom/konka/mm/model/MediaPlayer;Lcom/konka/mm/model/MediaPlayer;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Lcom/konka/mm/model/MediaPlayer;
    .param p3    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mNativeContext:I
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$0(Lcom/konka/mm/model/MediaPlayer;)I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "MediaPlayer"

    const-string v2, "mediaplayer went away with unhandled events"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    :sswitch_0
    return-void

    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    const-string v1, "MediaPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown message type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnPreparedListener:Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$1(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnPreparedListener:Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$1(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    invoke-interface {v1, v2}, Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;->onPrepared(Lcom/konka/mm/model/MediaPlayer;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$2(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$2(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    invoke-interface {v1, v2}, Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;->onCompletion(Lcom/konka/mm/model/MediaPlayer;)V

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # invokes: Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V
    invoke-static {v1, v5}, Lcom/konka/mm/model/MediaPlayer;->access$3(Lcom/konka/mm/model/MediaPlayer;Z)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnBufferingUpdateListener:Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$4(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnBufferingUpdateListener:Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$4(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v2, v3}, Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;->onBufferingUpdate(Lcom/konka/mm/model/MediaPlayer;I)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnSeekCompleteListener:Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$5(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnSeekCompleteListener:Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$5(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    invoke-interface {v1, v2}, Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;->onSeekComplete(Lcom/konka/mm/model/MediaPlayer;)V

    goto :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnVideoSizeChangedListener:Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$6(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnVideoSizeChangedListener:Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$6(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4}, Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;->onVideoSizeChanged(Lcom/konka/mm/model/MediaPlayer;II)V

    goto/16 :goto_0

    :sswitch_6
    const-string v1, "MediaPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnErrorListener:Lcom/konka/mm/model/MediaPlayer$OnErrorListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$7(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnErrorListener;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnErrorListener:Lcom/konka/mm/model/MediaPlayer$OnErrorListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$7(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnErrorListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4}, Lcom/konka/mm/model/MediaPlayer$OnErrorListener;->onError(Lcom/konka/mm/model/MediaPlayer;II)Z

    move-result v0

    :cond_3
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$2(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    move-result-object v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$2(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    invoke-interface {v1, v2}, Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;->onCompletion(Lcom/konka/mm/model/MediaPlayer;)V

    :cond_4
    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # invokes: Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V
    invoke-static {v1, v5}, Lcom/konka/mm/model/MediaPlayer;->access$3(Lcom/konka/mm/model/MediaPlayer;Z)V

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "MediaPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Info ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnInfoListener:Lcom/konka/mm/model/MediaPlayer$OnInfoListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$8(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnInfoListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->this$0:Lcom/konka/mm/model/MediaPlayer;

    # getter for: Lcom/konka/mm/model/MediaPlayer;->mOnInfoListener:Lcom/konka/mm/model/MediaPlayer$OnInfoListener;
    invoke-static {v1}, Lcom/konka/mm/model/MediaPlayer;->access$8(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnInfoListener;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer$EventHandler;->mMediaPlayer:Lcom/konka/mm/model/MediaPlayer;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4}, Lcom/konka/mm/model/MediaPlayer$OnInfoListener;->onInfo(Lcom/konka/mm/model/MediaPlayer;II)Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x64 -> :sswitch_6
        0xc8 -> :sswitch_7
    .end sparse-switch
.end method
