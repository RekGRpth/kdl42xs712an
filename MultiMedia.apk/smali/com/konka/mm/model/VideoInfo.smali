.class public Lcom/konka/mm/model/VideoInfo;
.super Ljava/lang/Object;
.source "VideoInfo.java"


# instance fields
.field private _duration:J

.field private displayName:Ljava/lang/String;

.field private duration:Ljava/lang/String;

.field private m_dateTaken:J

.field private m_id:Ljava/lang/String;

.field private m_path:Ljava/lang/String;

.field private m_size:D

.field private m_title:Ljava/lang/String;

.field private m_type:Ljava/lang/String;

.field private modifyTime:J

.field private resolution:Ljava/lang/String;

.field private thumbPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/mm/model/VideoInfo;->modifyTime:J

    return-void
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->duration:Ljava/lang/String;

    return-object v0
.end method

.method public getM_dateTaken()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/model/VideoInfo;->m_dateTaken:J

    return-wide v0
.end method

.method public getM_id()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->m_id:Ljava/lang/String;

    return-object v0
.end method

.method public getM_path()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->m_path:Ljava/lang/String;

    return-object v0
.end method

.method public getM_size()D
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/model/VideoInfo;->m_size:D

    return-wide v0
.end method

.method public getM_title()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->m_title:Ljava/lang/String;

    return-object v0
.end method

.method public getM_type()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->m_type:Ljava/lang/String;

    return-object v0
.end method

.method public getModifyTime()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/model/VideoInfo;->modifyTime:J

    return-wide v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->resolution:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/VideoInfo;->thumbPath:Ljava/lang/String;

    return-object v0
.end method

.method public get_duration()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/model/VideoInfo;->_duration:J

    return-wide v0
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setDuration(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->duration:Ljava/lang/String;

    return-void
.end method

.method public setM_dateTaken(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/model/VideoInfo;->m_dateTaken:J

    return-void
.end method

.method public setM_id(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->m_id:Ljava/lang/String;

    return-void
.end method

.method public setM_path(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->m_path:Ljava/lang/String;

    return-void
.end method

.method public setM_size(D)V
    .locals 0
    .param p1    # D

    iput-wide p1, p0, Lcom/konka/mm/model/VideoInfo;->m_size:D

    return-void
.end method

.method public setM_title(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->m_title:Ljava/lang/String;

    return-void
.end method

.method public setM_type(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->m_type:Ljava/lang/String;

    return-void
.end method

.method public setModifyTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/model/VideoInfo;->modifyTime:J

    return-void
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->resolution:Ljava/lang/String;

    return-void
.end method

.method public setThumbPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/model/VideoInfo;->thumbPath:Ljava/lang/String;

    return-void
.end method

.method public set_duration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/model/VideoInfo;->_duration:J

    return-void
.end method
