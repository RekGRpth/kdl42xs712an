.class public Lcom/konka/mm/model/PageInfo;
.super Ljava/lang/Object;
.source "PageInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x5a928118aa60b136L


# instance fields
.field private pate:I

.field private pos:I

.field private position:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/konka/mm/model/PageInfo;->pos:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/konka/mm/model/PageInfo;->pate:I

    iput p2, p0, Lcom/konka/mm/model/PageInfo;->position:I

    return-void
.end method


# virtual methods
.method public getPate()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/model/PageInfo;->pate:I

    return v0
.end method

.method public getPos()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/model/PageInfo;->pos:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/model/PageInfo;->position:I

    return v0
.end method

.method public setPate(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/model/PageInfo;->pate:I

    return-void
.end method

.method public setPos(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/model/PageInfo;->pos:I

    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/model/PageInfo;->position:I

    return-void
.end method
