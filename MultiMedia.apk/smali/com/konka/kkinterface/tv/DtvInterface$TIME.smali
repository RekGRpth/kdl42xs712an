.class public Lcom/konka/kkinterface/tv/DtvInterface$TIME;
.super Ljava/lang/Object;
.source "DtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DtvInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TIME"
.end annotation


# static fields
.field public static final E_TIMER_BOOT_ON_TIMER:I = 0x0

.field public static final E_TIMER_BOOT_RECORDER:I = 0x2

.field public static final E_TIMER_BOOT_REMINDER:I = 0x1


# instance fields
.field public autoSleepFlag:Z

.field public clockMode:Z

.field public eTimeZoneInfo:S

.field public enOnTimeState:S

.field public is12Hour:Z

.field public isAutoSync:Z

.field public isDaylightSaving:Z

.field public offTimeFlag:I

.field public offTimeState:S

.field public offTimerInfoHour:S

.field public offTimerInfoMin:S

.field public offsetTime:I

.field public onTimeFlag:S

.field public onTimeTvSrc:S

.field public onTimerChannel:I

.field public onTimerInfoHour:S

.field public onTimerInfoMin:S

.field public onTimerVolume:S

.field public timeDataCS:I

.field public timerBootMode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
