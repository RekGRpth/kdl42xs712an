.class public final enum Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;
.super Ljava/lang/Enum;
.source "CommonDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/CommonDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumScreenMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_AUDIO_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_CA_NOTIFY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_CH_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_CI_PLUS_AUTHENTICATION:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_COMMON_VIDEO:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_DATA_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_INVALID_PMT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_INVALID_SERVICE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_MAX:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_NO_CI_MODULE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_PARENTAL_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_SCRAMBLED_PROGRAM:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

.field public static final enum MSRV_DTV_SS_UNSUPPORTED_FORMAT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_INVALID_SERVICE"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_INVALID_SERVICE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_NO_CI_MODULE"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_NO_CI_MODULE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_CI_PLUS_AUTHENTICATION"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CI_PLUS_AUTHENTICATION:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_SCRAMBLED_PROGRAM"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_SCRAMBLED_PROGRAM:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_CH_BLOCK"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CH_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_PARENTAL_BLOCK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_PARENTAL_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_AUDIO_ONLY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_AUDIO_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_DATA_ONLY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_DATA_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_COMMON_VIDEO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_COMMON_VIDEO:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_UNSUPPORTED_FORMAT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_UNSUPPORTED_FORMAT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_INVALID_PMT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_INVALID_PMT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_MAX"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_MAX:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const-string v1, "MSRV_DTV_SS_CA_NOTIFY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CA_NOTIFY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_INVALID_SERVICE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_NO_CI_MODULE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CI_PLUS_AUTHENTICATION:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_SCRAMBLED_PROGRAM:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CH_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_PARENTAL_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_AUDIO_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_DATA_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_COMMON_VIDEO:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_UNSUPPORTED_FORMAT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_INVALID_PMT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_MAX:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CA_NOTIFY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
