.class Lcom/mstar/SmbClient$scanHost;
.super Ljava/lang/Object;
.source "SmbClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/SmbClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "scanHost"
.end annotation


# instance fields
.field SmbURL:Ljava/lang/String;

.field auth:Lcom/mstar/NbtAuthority;

.field callback:Lcom/mstar/CallBack;

.field final synthetic this$0:Lcom/mstar/SmbClient;


# direct methods
.method constructor <init>(Lcom/mstar/SmbClient;Ljava/lang/String;Lcom/mstar/NbtAuthority;Lcom/mstar/CallBack;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mstar/NbtAuthority;
    .param p4    # Lcom/mstar/CallBack;

    iput-object p1, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mstar/SmbClient$scanHost;->SmbURL:Ljava/lang/String;

    iput-object p3, p0, Lcom/mstar/SmbClient$scanHost;->auth:Lcom/mstar/NbtAuthority;

    iput-object p4, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v1, 0x0

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->auth:Lcom/mstar/NbtAuthority;

    if-eqz v9, :cond_0

    new-instance v1, Ljcifs/smb/NtlmPasswordAuthentication;

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->auth:Lcom/mstar/NbtAuthority;

    iget-object v9, v9, Lcom/mstar/NbtAuthority;->domain:Ljava/lang/String;

    iget-object v10, p0, Lcom/mstar/SmbClient$scanHost;->auth:Lcom/mstar/NbtAuthority;

    iget-object v10, v10, Lcom/mstar/NbtAuthority;->username:Ljava/lang/String;

    iget-object v11, p0, Lcom/mstar/SmbClient$scanHost;->auth:Lcom/mstar/NbtAuthority;

    iget-object v11, v11, Lcom/mstar/NbtAuthority;->passwd:Ljava/lang/String;

    invoke-direct {v1, v9, v10, v11}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->SmbURL:Ljava/lang/String;

    if-nez v9, :cond_3

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    new-instance v10, Ljcifs/smb/SmbFile;

    const-string v11, "smb://"

    invoke-direct {v10, v11}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    iput-object v10, v9, Lcom/mstar/SmbClient;->root:Ljcifs/smb/SmbFile;

    :goto_0
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    iget-object v9, v9, Lcom/mstar/SmbClient;->root:Ljcifs/smb/SmbFile;

    invoke-virtual {v9}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v0

    sget-boolean v9, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v9, :cond_1

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "list1 number: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v11, v0

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    :goto_1
    array-length v9, v0

    if-lt v5, v9, :cond_5

    :cond_2
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    # getter for: Lcom/mstar/SmbClient;->stoprun:Z
    invoke-static {v9}, Lcom/mstar/SmbClient;->access$0(Lcom/mstar/SmbClient;)Z

    move-result v9

    if-eqz v9, :cond_e

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/mstar/SmbClient;->HostTable:[Lcom/mstar/SambaFile;

    :goto_2
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-interface {v9, v10, v11}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    :goto_3
    return-void

    :cond_3
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->auth:Lcom/mstar/NbtAuthority;

    if-nez v9, :cond_4

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    new-instance v10, Ljcifs/smb/SmbFile;

    iget-object v11, p0, Lcom/mstar/SmbClient$scanHost;->SmbURL:Ljava/lang/String;

    invoke-direct {v10, v11}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    iput-object v10, v9, Lcom/mstar/SmbClient;->root:Ljcifs/smb/SmbFile;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_3

    :cond_4
    :try_start_1
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    new-instance v10, Ljcifs/smb/SmbFile;

    iget-object v11, p0, Lcom/mstar/SmbClient$scanHost;->SmbURL:Ljava/lang/String;

    invoke-direct {v10, v11, v1}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    iput-object v10, v9, Lcom/mstar/SmbClient;->root:Ljcifs/smb/SmbFile;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljcifs/smb/SmbException;->getNtStatus()I

    move-result v6

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9, v6}, Ljava/io/PrintStream;->println(I)V

    sparse-switch v6, :sswitch_data_0

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/4 v10, 0x5

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto :goto_3

    :cond_5
    :try_start_2
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    # getter for: Lcom/mstar/SmbClient;->stoprun:Z
    invoke-static {v9}, Lcom/mstar/SmbClient;->access$0(Lcom/mstar/SmbClient;)Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    aget-object v10, v0, v5

    invoke-virtual {v9, v10}, Lcom/mstar/SmbClient;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v9

    if-eqz v9, :cond_7

    new-instance v8, Lcom/mstar/SambaFile;

    invoke-direct {v8}, Lcom/mstar/SambaFile;-><init>()V

    aget-object v9, v0, v5

    invoke-static {v8, v9}, Lcom/mstar/SmbClient;->convertToSambaFile(Lcom/mstar/SambaFile;Ljcifs/smb/SmbFile;)V

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/4 v10, 0x2

    invoke-interface {v9, v8, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    :cond_6
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    sget-boolean v9, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v9, :cond_8

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "get type :"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v11, v0, v5

    invoke-virtual {v11}, Ljcifs/smb/SmbFile;->getType()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    aget-object v9, v0, v5

    invoke-virtual {v9}, Ljcifs/smb/SmbFile;->getType()I
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v9

    if-ne v9, v13, :cond_a

    :try_start_3
    aget-object v9, v0, v5

    invoke-virtual {v9}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v2

    sget-boolean v9, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v9, :cond_9

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "list2 number: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v11, v2

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_9
    const/4 v7, 0x0

    :goto_4
    array-length v9, v2

    if-lt v7, v9, :cond_b

    :cond_a
    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_b
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    # getter for: Lcom/mstar/SmbClient;->stoprun:Z
    invoke-static {v9}, Lcom/mstar/SmbClient;->access$0(Lcom/mstar/SmbClient;)Z

    move-result v9

    if-nez v9, :cond_a

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    aget-object v10, v2, v7

    invoke-virtual {v9, v10}, Lcom/mstar/SmbClient;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v9

    if-eqz v9, :cond_d

    new-instance v8, Lcom/mstar/SambaFile;

    invoke-direct {v8}, Lcom/mstar/SambaFile;-><init>()V

    aget-object v9, v2, v7

    invoke-static {v8, v9}, Lcom/mstar/SmbClient;->convertToSambaFile(Lcom/mstar/SambaFile;Ljcifs/smb/SmbFile;)V

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    if-eqz v9, :cond_c

    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/4 v10, 0x2

    invoke-interface {v9, v8, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    :cond_c
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljcifs/smb/SmbException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_d
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :catch_2
    move-exception v3

    :try_start_4
    invoke-virtual {v3}, Ljcifs/smb/SmbException;->getNtStatus()I

    move-result v6

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "workgroup :"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_5

    :cond_e
    iget-object v10, p0, Lcom/mstar/SmbClient$scanHost;->this$0:Lcom/mstar/SmbClient;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lcom/mstar/SambaFile;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/mstar/SambaFile;

    iput-object v9, v10, Lcom/mstar/SmbClient;->HostTable:[Lcom/mstar/SambaFile;
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    :sswitch_0
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/4 v10, 0x6

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_1
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/4 v10, 0x7

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_2
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0x8

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_3
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0x9

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_4
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0xa

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_5
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0xb

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_6
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0xc

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_7
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0xd

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_8
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0xe

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_9
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0xf

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_a
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0x10

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_b
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0x11

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    :sswitch_c
    iget-object v9, p0, Lcom/mstar/SmbClient$scanHost;->callback:Lcom/mstar/CallBack;

    const/16 v10, 0x12

    invoke-interface {v9, v12, v10}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3fffffa2 -> :sswitch_5
        -0x3fffff9c -> :sswitch_7
        -0x3fffff96 -> :sswitch_c
        -0x3fffff93 -> :sswitch_3
        -0x3fffff91 -> :sswitch_2
        -0x3fffff8f -> :sswitch_9
        -0x3fffff43 -> :sswitch_0
        -0x3fffff30 -> :sswitch_b
        -0x3fffff21 -> :sswitch_6
        -0x3ffffede -> :sswitch_1
        -0x3ffffea5 -> :sswitch_4
        -0x3ffffddc -> :sswitch_a
        -0x3ffffddb -> :sswitch_8
    .end sparse-switch
.end method
