.class public abstract Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;
.super Landroid/os/Binder;
.source "ITvServiceServerCommon.java"

# interfaces
.implements Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

.field static final TRANSACTION_AllowDetection:I = 0x3

.field static final TRANSACTION_DisableAutoSourceSwitch:I = 0x7

.field static final TRANSACTION_EnableAutoSourceSwitch:I = 0x6

.field static final TRANSACTION_ForbidDetection:I = 0x2

.field static final TRANSACTION_GetCurrentInputSource:I = 0x9

.field static final TRANSACTION_GetInputSourceStatus:I = 0x8

.field static final TRANSACTION_OSD_Set3Dformat:I = 0x1d

.field static final TRANSACTION_SetInputSource:I = 0xa

.field static final TRANSACTION_StartSourceDetection:I = 0x4

.field static final TRANSACTION_StopSourceDetection:I = 0x5

.field static final TRANSACTION_closeSurfaceView:I = 0x11

.field static final TRANSACTION_enterSleepMode:I = 0x1a

.field static final TRANSACTION_getGpioDeviceStatus:I = 0x18

.field static final TRANSACTION_getHPPortStatus:I = 0x20

.field static final TRANSACTION_getOsdLanguage:I = 0x13

.field static final TRANSACTION_getPowerOnSource:I = 0x15

.field static final TRANSACTION_getPresentFollowingEventInfo:I = 0x17

.field static final TRANSACTION_getSourceList:I = 0x1

.field static final TRANSACTION_getVideoInfo:I = 0xc

.field static final TRANSACTION_isSignalStable:I = 0xb

.field static final TRANSACTION_openSurfaceView:I = 0xf

.field static final TRANSACTION_programDown:I = 0xe

.field static final TRANSACTION_programUp:I = 0xd

.field static final TRANSACTION_rebootSystem:I = 0x1b

.field static final TRANSACTION_setGpioDeviceStatus:I = 0x19

.field static final TRANSACTION_setHPPortStatus:I = 0x1f

.field static final TRANSACTION_setHotkeyEnableOrNot:I = 0x1e

.field static final TRANSACTION_setOsdLanguage:I = 0x12

.field static final TRANSACTION_setPowerOnSource:I = 0x14

.field static final TRANSACTION_setSurfaceView:I = 0x10

.field static final TRANSACTION_standbySystem:I = 0x1c

.field static final TRANSACTION_upgrade:I = 0x16


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p0, p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v6, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    :sswitch_0
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->getSourceList()Lcom/mstar/tv/service/aidl/IntArrayList;

    move-result-object v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_0

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3, v6}, Lcom/mstar/tv/service/aidl/IntArrayList;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_2
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->ForbidDetection()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_3
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->AllowDetection()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_4
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->StartSourceDetection()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_5
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->StopSourceDetection()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_6
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->EnableAutoSourceSwitch()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_7
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->DisableAutoSourceSwitch()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_8
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->GetInputSourceStatus()Lcom/mstar/tv/service/aidl/BoolArrayList;

    move-result-object v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_1

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3, v6}, Lcom/mstar/tv/service/aidl/BoolArrayList;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_9
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->GetCurrentInputSource()Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    move-result-object v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_2

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3, v6}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_a
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->SetInputSource(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_b
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->isSignalStable()Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_4

    move v5, v6

    :cond_4
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_c
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->getVideoInfo()Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;

    move-result-object v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_5

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3, v6}, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_d
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->programUp()Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_6

    move v5, v6

    :cond_6
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_e
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->programDown()Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_7

    move v5, v6

    :cond_7
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_f
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->openSurfaceView(IIII)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_10
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->setSurfaceView(IIII)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_11
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->closeSurfaceView()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_12
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_9

    sget-object v7, Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;

    :goto_2
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->setOsdLanguage(Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;)Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_8

    move v5, v6

    :cond_8
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    :sswitch_13
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->getOsdLanguage()Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;

    move-result-object v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_a

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3, v6}, Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_14
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_c

    sget-object v7, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    :goto_3
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->setPowerOnSource(Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;)Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_b

    move v5, v6

    :cond_b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto :goto_3

    :sswitch_15
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->getPowerOnSource()Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    move-result-object v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_d

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3, v6}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_16
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->upgrade(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_e

    move v5, v6

    :cond_e
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_17
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_f

    move v2, v6

    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->getPresentFollowingEventInfo(IIZI)Lcom/mstar/tv/service/aidl/PresentFollowingEventInfo;

    move-result-object v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_10

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3, v6}, Lcom/mstar/tv/service/aidl/PresentFollowingEventInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_f
    move v2, v5

    goto :goto_4

    :cond_10
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_18
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->getGpioDeviceStatus(I)I

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_19
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_12

    move v1, v6

    :goto_5
    invoke-virtual {p0, v0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->setGpioDeviceStatus(IZ)Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_11

    move v5, v6

    :cond_11
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_12
    move v1, v5

    goto :goto_5

    :sswitch_1a
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_13

    move v0, v6

    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_14

    move v1, v6

    :goto_7
    invoke-virtual {p0, v0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->enterSleepMode(ZZ)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_13
    move v0, v5

    goto :goto_6

    :cond_14
    move v1, v5

    goto :goto_7

    :sswitch_1b
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->rebootSystem(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_1c
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->standbySystem(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_1d
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_15

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_ThreeD_OSD_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_OSD_TYPE;

    :goto_8
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->OSD_Set3Dformat(Lcom/mstar/tv/service/aidl/EN_ThreeD_OSD_TYPE;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_15
    const/4 v0, 0x0

    goto :goto_8

    :sswitch_1e
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_16

    move v0, v6

    :goto_9
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->setHotkeyEnableOrNot(Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_16
    move v0, v5

    goto :goto_9

    :sswitch_1f
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_17

    move v0, v6

    :goto_a
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->setHPPortStatus(Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_17
    move v0, v5

    goto :goto_a

    :sswitch_20
    const-string v7, "com.mstar.tv.service.interfaces.ITvServiceServerCommon"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon$Stub;->getHPPortStatus()Z

    move-result v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v4, :cond_18

    move v5, v6

    :cond_18
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
