.class public abstract Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;
.super Landroid/os/Binder;
.source "ITvServiceServerChannel.java"

# interfaces
.implements Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

.field static final TRANSACTION_DtvStopScan:I = 0x40

.field static final TRANSACTION_GetTsStatus:I = 0x2e

.field static final TRANSACTION_addProgramToFavorite:I = 0x8

.field static final TRANSACTION_atvGetCurrentFrequency:I = 0x9

.field static final TRANSACTION_atvGetProgramInfo:I = 0xa

.field static final TRANSACTION_atvGetSoundSystem:I = 0xb

.field static final TRANSACTION_atvGetVideoSystem:I = 0xc

.field static final TRANSACTION_atvSetAutoTuningEnd:I = 0xe

.field static final TRANSACTION_atvSetAutoTuningPause:I = 0xf

.field static final TRANSACTION_atvSetAutoTuningResume:I = 0x10

.field static final TRANSACTION_atvSetAutoTuningStart:I = 0x7

.field static final TRANSACTION_atvSetChannel:I = 0x11

.field static final TRANSACTION_atvSetForceSoundSystem:I = 0x12

.field static final TRANSACTION_atvSetForceVedioSystem:I = 0x13

.field static final TRANSACTION_atvSetFrequency:I = 0x14

.field static final TRANSACTION_atvSetManualTuningEnd:I = 0x6

.field static final TRANSACTION_atvSetManualTuningStart:I = 0x4

.field static final TRANSACTION_atvSetProgramInfo:I = 0x15

.field static final TRANSACTION_changeToFirstService:I = 0x5

.field static final TRANSACTION_closeSubtitle:I = 0x16

.field static final TRANSACTION_deleteProgramFromFavorite:I = 0x17

.field static final TRANSACTION_dtvAutoScan:I = 0x18

.field static final TRANSACTION_dtvChangeManualScanRF:I = 0x19

.field static final TRANSACTION_dtvFullScan:I = 0x1a

.field static final TRANSACTION_dtvGetAntennaType:I = 0x38

.field static final TRANSACTION_dtvGetRFInfo:I = 0x46

.field static final TRANSACTION_dtvManualScanFreq:I = 0x39

.field static final TRANSACTION_dtvManualScanRF:I = 0x3a

.field static final TRANSACTION_dtvPauseScan:I = 0x3b

.field static final TRANSACTION_dtvResumeScan:I = 0x3d

.field static final TRANSACTION_dtvSetAntennaType:I = 0x3e

.field static final TRANSACTION_dtvStartManualScan:I = 0x3f

.field static final TRANSACTION_dtvplayCurrentProgram:I = 0x3c

.field static final TRANSACTION_dvbcgetScanParam:I = 0x43

.field static final TRANSACTION_dvbcsetScanParam:I = 0x44

.field static final TRANSACTION_getAtvStationName:I = 0x1b

.field static final TRANSACTION_getAudioInfo:I = 0x47

.field static final TRANSACTION_getCurrProgramInfo:I = 0x37

.field static final TRANSACTION_getCurrentChannelNumber:I = 0xd

.field static final TRANSACTION_getCurrentLanguageIndex:I = 0x42

.field static final TRANSACTION_getCurrentMuxInfo:I = 0x41

.field static final TRANSACTION_getCurrentProgramSpecificInfo:I = 0x45

.field static final TRANSACTION_getMSrvDtvRoute:I = 0x36

.field static final TRANSACTION_getProgramAttribute:I = 0x2f

.field static final TRANSACTION_getProgramCount:I = 0x1c

.field static final TRANSACTION_getProgramCtrl:I = 0x30

.field static final TRANSACTION_getProgramInfo:I = 0x35

.field static final TRANSACTION_getProgramName:I = 0x31

.field static final TRANSACTION_getSIFMtsMode:I = 0x32

.field static final TRANSACTION_getSubtitleInfo:I = 0x34

.field static final TRANSACTION_getSystemCountry:I = 0x33

.field static final TRANSACTION_getUserScanType:I = 0x2d

.field static final TRANSACTION_getVideoStandard:I = 0x2c

.field static final TRANSACTION_isSignalStabled:I = 0x2b

.field static final TRANSACTION_makeSourceAtv:I = 0x1

.field static final TRANSACTION_makeSourceDtv:I = 0x2

.field static final TRANSACTION_moveProgram:I = 0x2a

.field static final TRANSACTION_openSubtitle:I = 0x29

.field static final TRANSACTION_programDown:I = 0x28

.field static final TRANSACTION_programReturn:I = 0x27

.field static final TRANSACTION_programSel:I = 0x1d

.field static final TRANSACTION_programUp:I = 0x26

.field static final TRANSACTION_sendAtvScaninfo:I = 0x25

.field static final TRANSACTION_sendDtvScaninfo:I = 0x24

.field static final TRANSACTION_setChannelChangeFreezeMode:I = 0x48

.field static final TRANSACTION_setProgramAttribute:I = 0x23

.field static final TRANSACTION_setProgramCtrl:I = 0x22

.field static final TRANSACTION_setProgramName:I = 0x21

.field static final TRANSACTION_setSystemCountry:I = 0x20

.field static final TRANSACTION_setUserScanType:I = 0x1f

.field static final TRANSACTION_switchAudioTrack:I = 0x1e

.field static final TRANSACTION_switchMSrvDtvRouteCmd:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p0, p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v7, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    :sswitch_0
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->makeSourceAtv()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_0

    move v0, v7

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    move v0, v8

    goto :goto_1

    :sswitch_2
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->makeSourceDtv()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_1

    move v8, v7

    :cond_1
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_3
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->switchMSrvDtvRouteCmd(I)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_2

    move v8, v7

    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_4
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_MANUAL_TUNE_MODE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/tv/service/aidl/EN_ATV_MANUAL_TUNE_MODE;

    :goto_2
    invoke-virtual {p0, v1, v2, v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetManualTuningStart(IILcom/mstar/tv/service/aidl/EN_ATV_MANUAL_TUNE_MODE;)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_3

    move v8, v7

    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    :sswitch_5
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_INPUT_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_INPUT_TYPE;

    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    :goto_4
    invoke-virtual {p0, v1, v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->changeToFirstService(Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_INPUT_TYPE;Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_5

    move v8, v7

    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    :sswitch_6
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetManualTuningEnd()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetAutoTuningStart(III)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_8

    move v8, v7

    :cond_8
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;

    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->addProgramToFavorite(Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;III)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    :sswitch_9
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvGetCurrentFrequency()I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvGetProgramInfo(Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;IILjava/lang/String;)I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x0

    goto :goto_6

    :sswitch_b
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvGetSoundSystem()Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_b

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvGetVideoSystem()Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_c

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getCurrentChannelNumber()I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetAutoTuningEnd()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_d

    move v8, v7

    :cond_d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_f
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetAutoTuningPause()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_e

    move v8, v7

    :cond_e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_10
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetAutoTuningResume()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_f

    move v8, v7

    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    move v2, v7

    :goto_7
    invoke-virtual {p0, v1, v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetChannel(IZ)I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_10
    move v2, v8

    goto :goto_7

    :sswitch_12
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    :goto_8
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetForceSoundSystem(Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_11

    move v8, v7

    :cond_11
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_12
    const/4 v1, 0x0

    goto :goto_8

    :sswitch_13
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_14

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;

    :goto_9
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetForceVedioSystem(Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_13

    move v8, v7

    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_14
    const/4 v1, 0x0

    goto :goto_9

    :sswitch_14
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetFrequency(I)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_15

    move v8, v7

    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_15
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_INFO;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_INFO;

    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->atvSetProgramInfo(Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_INFO;IILjava/lang/String;)I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_16
    const/4 v1, 0x0

    goto :goto_a

    :sswitch_16
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->closeSubtitle()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_17

    move v8, v7

    :cond_17
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_17
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_18

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;

    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->deleteProgramFromFavorite(Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;III)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_18
    const/4 v1, 0x0

    goto :goto_b

    :sswitch_18
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvAutoScan()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_19

    move v8, v7

    :cond_19
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_19
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvChangeManualScanRF(I)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_1a

    move v8, v7

    :cond_1a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1a
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvFullScan()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_1b

    move v8, v7

    :cond_1b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1b
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getAtvStationName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1c
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_PROGRAM_COUNT_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_PROGRAM_COUNT_TYPE;

    :goto_c
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getProgramCount(Lcom/mstar/tv/service/aidl/EN_PROGRAM_COUNT_TYPE;)I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_1c
    const/4 v1, 0x0

    goto :goto_c

    :sswitch_1d
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1e

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;

    :goto_d
    invoke-virtual {p0, v1, v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->programSel(ILcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_1d

    move v8, v7

    :cond_1d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_1e
    const/4 v2, 0x0

    goto :goto_d

    :sswitch_1e
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->switchAudioTrack(I)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_1f
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1f

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;

    :goto_e
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->setUserScanType(Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_1f
    const/4 v1, 0x0

    goto :goto_e

    :sswitch_20
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_20

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    :goto_f
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->setSystemCountry(Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_20
    const/4 v1, 0x0

    goto :goto_f

    :sswitch_21
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->setProgramName(IILjava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_22
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_21

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_CTRL;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_CTRL;

    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->setProgramCtrl(Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_CTRL;IILjava/lang/String;)I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_21
    const/4 v1, 0x0

    goto :goto_10

    :sswitch_23
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_22

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;

    :goto_11
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_23

    move v5, v7

    :goto_12
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->setProgramAttribute(Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;IIIZ)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_22
    const/4 v1, 0x0

    goto :goto_11

    :cond_23
    move v5, v8

    goto :goto_12

    :sswitch_24
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->sendDtvScaninfo()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_25
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->sendAtvScaninfo()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_26
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->programUp()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_24

    move v8, v7

    :cond_24
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_27
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->programReturn()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_25

    move v8, v7

    :cond_25
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_28
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->programDown()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_26

    move v8, v7

    :cond_26
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_29
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->openSubtitle(I)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_27

    move v8, v7

    :cond_27
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_2a
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->moveProgram(II)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_2b
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->isSignalStabled()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_28

    move v8, v7

    :cond_28
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_2c
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getVideoStandard()Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_29

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_29
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_2d
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getUserScanType()Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_2a

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_2a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_2e
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->GetTsStatus()Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_2b

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_2b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_2f
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2d

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;

    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2e

    move v5, v7

    :goto_14
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getProgramAttribute(Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;IIIZ)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_2c

    move v8, v7

    :cond_2c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_2d
    const/4 v1, 0x0

    goto :goto_13

    :cond_2e
    move v5, v8

    goto :goto_14

    :sswitch_30
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2f

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_CTRL;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_CTRL;

    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getProgramCtrl(Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_CTRL;IILjava/lang/String;)I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_2f
    const/4 v1, 0x0

    goto :goto_15

    :sswitch_31
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_30

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;

    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getProgramName(ILcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_30
    const/4 v2, 0x0

    goto :goto_16

    :sswitch_32
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getSIFMtsMode()Lcom/mstar/tv/service/aidl/EN_ATV_AUDIO_MODE_TYPE;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_31

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_ATV_AUDIO_MODE_TYPE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_31
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_33
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getSystemCountry()Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_32

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_32
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_34
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getSubtitleInfo()Lcom/mstar/tv/service/aidl/DtvSubtitleInfo;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_33

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/DtvSubtitleInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_33
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_35
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_34

    sget-object v0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;

    :goto_17
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_35

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_PROGRAM_INFO_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/tv/service/aidl/EN_PROGRAM_INFO_TYPE;

    :goto_18
    invoke-virtual {p0, v1, v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getProgramInfo(Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;Lcom/mstar/tv/service/aidl/EN_PROGRAM_INFO_TYPE;)Lcom/mstar/tv/service/aidl/ProgramInfo;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_36

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/ProgramInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_34
    const/4 v1, 0x0

    goto :goto_17

    :cond_35
    const/4 v2, 0x0

    goto :goto_18

    :cond_36
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_36
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getMSrvDtvRoute()I

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_37
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getCurrProgramInfo()Lcom/mstar/tv/service/aidl/ProgramInfo;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_37

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/ProgramInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_37
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_38
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvGetAntennaType()Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_38

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_38
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_39
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvManualScanFreq(I)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_39

    move v8, v7

    :cond_39
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_3a
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvManualScanRF(I)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_3a

    move v8, v7

    :cond_3a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_3b
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvPauseScan()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_3b

    move v8, v7

    :cond_3b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_3c
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvplayCurrentProgram()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_3c

    move v8, v7

    :cond_3c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_3d
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvResumeScan()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_3d

    move v8, v7

    :cond_3d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_3e
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3e

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;

    :goto_19
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvSetAntennaType(Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_3e
    const/4 v1, 0x0

    goto :goto_19

    :sswitch_3f
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvStartManualScan()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_3f

    move v8, v7

    :cond_3f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_40
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->DtvStopScan()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_40

    move v8, v7

    :cond_40
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_41
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getCurrentMuxInfo()Lcom/mstar/tv/service/aidl/DvbMuxInfo;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_41

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_41
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_42
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_42

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_42
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_43
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_44

    sget-object v0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/DvbcScanParam;

    :goto_1a
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dvbcgetScanParam(Lcom/mstar/tv/service/aidl/DvbcScanParam;)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_43

    move v8, v7

    :cond_43
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_44
    const/4 v1, 0x0

    goto :goto_1a

    :sswitch_44
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_46

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;

    :goto_1b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dvbcsetScanParam(ILcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;III)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_45

    move v8, v7

    :cond_45
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_46
    const/4 v2, 0x0

    goto :goto_1b

    :sswitch_45
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_48

    sget-object v0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;

    :goto_1c
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getCurrentProgramSpecificInfo(Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;)Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_47

    move v8, v7

    :cond_47
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_48
    const/4 v1, 0x0

    goto :goto_1c

    :sswitch_46
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_49

    sget-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/tv/service/aidl/EnumInfoType;

    :goto_1d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->dtvGetRFInfo(Lcom/mstar/tv/service/aidl/EnumInfoType;I)Lcom/mstar/tv/service/aidl/RfInfo;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_4a

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/RfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_49
    const/4 v1, 0x0

    goto :goto_1d

    :cond_4a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_47
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->getAudioInfo()Lcom/mstar/tv/service/aidl/DtvAudioInfo;

    move-result-object v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_4b

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v6, p3, v7}, Lcom/mstar/tv/service/aidl/DtvAudioInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_4b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_48
    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerChannel"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4c

    move v1, v7

    :goto_1e
    invoke-virtual {p0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel$Stub;->setChannelChangeFreezeMode(Z)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_4c
    move v1, v8

    goto :goto_1e

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
