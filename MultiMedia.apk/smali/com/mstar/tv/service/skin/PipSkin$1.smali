.class Lcom/mstar/tv/service/skin/PipSkin$1;
.super Ljava/lang/Object;
.source "PipSkin.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/skin/PipSkin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/tv/service/skin/PipSkin;


# direct methods
.method constructor <init>(Lcom/mstar/tv/service/skin/PipSkin;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PipSkin$1;->this$0:Lcom/mstar/tv/service/skin/PipSkin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin$1;->this$0:Lcom/mstar/tv/service/skin/PipSkin;

    invoke-static {p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mstar/tv/service/skin/PipSkin;->access$0(Lcom/mstar/tv/service/skin/PipSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;)V

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin$1;->this$0:Lcom/mstar/tv/service/skin/PipSkin;

    # getter for: Lcom/mstar/tv/service/skin/PipSkin;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/mstar/tv/service/skin/PipSkin;->access$1(Lcom/mstar/tv/service/skin/PipSkin;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin$1;->this$0:Lcom/mstar/tv/service/skin/PipSkin;

    # getter for: Lcom/mstar/tv/service/skin/PipSkin;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/mstar/tv/service/skin/PipSkin;->access$1(Lcom/mstar/tv/service/skin/PipSkin;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const v2, 0x7fffffff

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Index"

    const v3, 0x7ffffffb

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/mstar/tv/service/skin/PipSkin$1;->this$0:Lcom/mstar/tv/service/skin/PipSkin;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mstar/tv/service/skin/PipSkin;->access$0(Lcom/mstar/tv/service/skin/PipSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;)V

    return-void
.end method
