.class public Lcom/mstar/SambaFile;
.super Ljava/lang/Object;
.source "SambaFile.java"


# instance fields
.field file:Ljcifs/smb/SmbFile;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Lcom/mstar/SambaException;
        }
    .end annotation

    const/4 v3, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v2, Lcom/mstar/SambaException;

    invoke-direct {v2, v3}, Lcom/mstar/SambaException;-><init>(I)V

    throw v2

    :cond_0
    new-instance v2, Ljcifs/smb/SmbFile;

    invoke-direct {v2, p1}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    :try_start_0
    iget-object v2, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v2}, Ljcifs/smb/SmbFile;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    :cond_1
    new-instance v2, Lcom/mstar/SambaException;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lcom/mstar/SambaException;-><init>(I)V

    throw v2
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lcom/mstar/SambaException;

    invoke-direct {v2, v0}, Lcom/mstar/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v2

    :cond_2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/mstar/NbtAuthority;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/NbtAuthority;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Lcom/mstar/SambaException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Ljcifs/smb/NtlmPasswordAuthentication;

    iget-object v1, p2, Lcom/mstar/NbtAuthority;->domain:Ljava/lang/String;

    iget-object v2, p2, Lcom/mstar/NbtAuthority;->username:Ljava/lang/String;

    iget-object v3, p2, Lcom/mstar/NbtAuthority;->passwd:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez p1, :cond_1

    new-instance v1, Lcom/mstar/SambaException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/mstar/SambaException;-><init>(I)V

    throw v1

    :cond_1
    new-instance v1, Ljcifs/smb/SmbFile;

    invoke-direct {v1, p1, v0}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    iput-object v1, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDirectory()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/SambaException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->isDirectory()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/SambaException;

    invoke-direct {v1, v0}, Lcom/mstar/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v1
.end method

.method public isFile()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/SambaException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->isFile()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/SambaException;

    invoke-direct {v1, v0}, Lcom/mstar/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v1
.end method

.method public listFiles()[Lcom/mstar/SambaFile;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/SambaException;,
            Ljava/net/MalformedURLException;
        }
    .end annotation

    :try_start_0
    iget-object v5, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v5}, Ljcifs/smb/SmbFile;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v5}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    array-length v5, v1

    if-lt v2, v5, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lcom/mstar/SambaFile;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/mstar/SambaFile;

    :goto_1
    return-object v5

    :cond_0
    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljcifs/smb/SmbFile;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_1

    aget-object v5, v1, v2

    invoke-virtual {v5}, Ljcifs/smb/SmbFile;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    new-instance v4, Lcom/mstar/SambaFile;

    invoke-direct {v4}, Lcom/mstar/SambaFile;-><init>()V

    aget-object v5, v1, v2

    invoke-static {v4, v5}, Lcom/mstar/SmbClient;->convertToSambaFile(Lcom/mstar/SambaFile;Ljcifs/smb/SmbFile;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v5, Lcom/mstar/SambaException;

    invoke-direct {v5, v0}, Lcom/mstar/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v5

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public openFile()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/SambaException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljcifs/smb/SmbFileInputStream;

    iget-object v2, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-direct {v1, v2}, Ljcifs/smb/SmbFileInputStream;-><init>(Ljcifs/smb/SmbFile;)V
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/SambaException;

    invoke-direct {v1, v0}, Lcom/mstar/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_1
.end method
