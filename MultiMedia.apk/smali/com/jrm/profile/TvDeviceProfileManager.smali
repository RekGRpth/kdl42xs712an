.class public Lcom/jrm/profile/TvDeviceProfileManager;
.super Ljava/lang/Object;
.source "TvDeviceProfileManager.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;


# direct methods
.method public constructor <init>(Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;)V
    .locals 3
    .param p1    # Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "TvDeviceProfileManager"

    iput-object v0, p0, Lcom/jrm/profile/TvDeviceProfileManager;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/profile/TvDeviceProfileManager;->mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    const-string v0, "TvDeviceProfileManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mITvDeviceInfo:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/profile/TvDeviceProfileManager;->mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public editTvprofile(Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;)I
    .locals 3
    .param p1    # Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvDeviceProfileManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "profile:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/profile/TvDeviceProfileManager;->mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    invoke-interface {v0, p1}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;->editTvprofile(Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;)I

    move-result v0

    return v0
.end method

.method public getBBNumer()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/profile/TvDeviceProfileManager;->mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    invoke-interface {v0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;->getBBNumer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/profile/TvDeviceProfileManager;->mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    invoke-interface {v0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;->getClientId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMac()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/profile/TvDeviceProfileManager;->mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    invoke-interface {v0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTvProfile()Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/profile/TvDeviceProfileManager;->mITvDeviceInfo:Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    invoke-interface {v0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;->getTvProfile()Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;

    move-result-object v0

    return-object v0
.end method
