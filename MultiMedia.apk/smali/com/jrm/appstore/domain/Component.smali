.class public Lcom/jrm/appstore/domain/Component;
.super Ljava/lang/Object;
.source "Component.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/appstore/domain/Component;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x6c4f9711b51a6c53L


# instance fields
.field private comId:Ljava/lang/String;

.field private compActClass:Ljava/lang/String;

.field private compCharge:Z

.field private compDescribe:Ljava/lang/String;

.field private compDeveloper:Ljava/lang/String;

.field private compDownloadURL:Ljava/lang/String;

.field private compFlag:I

.field private compLogoURL:Ljava/lang/String;

.field private compName:Ljava/lang/String;

.field private compPackage:Ljava/lang/String;

.field private compPrintScreenURL:[Ljava/lang/String;

.field private compSize:I

.field private compStar:I

.field private compType:Ljava/lang/String;

.field private compUpdateTime:Ljava/lang/String;

.field private md5:Ljava/lang/String;

.field private publishTime:Ljava/lang/String;

.field private versionCode:I

.field private versionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/appstore/domain/Component$1;

    invoke-direct {v0}, Lcom/jrm/appstore/domain/Component$1;-><init>()V

    sput-object v0, Lcom/jrm/appstore/domain/Component;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/jrm/appstore/domain/Component;->compPrintScreenURL:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->comId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$10(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compUpdateTime:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$11(Lcom/jrm/appstore/domain/Component;I)V
    .locals 0

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->compStar:I

    return-void
.end method

.method static synthetic access$12(Lcom/jrm/appstore/domain/Component;I)V
    .locals 0

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->compSize:I

    return-void
.end method

.method static synthetic access$13(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->publishTime:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$14(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compDownloadURL:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$15(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compType:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$16(Lcom/jrm/appstore/domain/Component;I)V
    .locals 0

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->compFlag:I

    return-void
.end method

.method static synthetic access$17(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->md5:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$2(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compDeveloper:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compLogoURL:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/jrm/appstore/domain/Component;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compPrintScreenURL:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compPackage:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compActClass:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/jrm/appstore/domain/Component;I)V
    .locals 0

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->versionCode:I

    return-void
.end method

.method static synthetic access$8(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->versionName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compDescribe:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getComId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->comId:Ljava/lang/String;

    return-object v0
.end method

.method public getCompActClass()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compActClass:Ljava/lang/String;

    return-object v0
.end method

.method public getCompDescribe()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compDescribe:Ljava/lang/String;

    return-object v0
.end method

.method public getCompDeveloper()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compDeveloper:Ljava/lang/String;

    return-object v0
.end method

.method public getCompDownloadURL()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compDownloadURL:Ljava/lang/String;

    return-object v0
.end method

.method public getCompFlag()I
    .locals 1

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->compFlag:I

    return v0
.end method

.method public getCompLogoURL()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compLogoURL:Ljava/lang/String;

    return-object v0
.end method

.method public getCompName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compName:Ljava/lang/String;

    return-object v0
.end method

.method public getCompPackage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getCompPrintScreenURL()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compPrintScreenURL:[Ljava/lang/String;

    return-object v0
.end method

.method public getCompSize()I
    .locals 1

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->compSize:I

    return v0
.end method

.method public getCompStar()I
    .locals 1

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->compStar:I

    return v0
.end method

.method public getCompType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compType:Ljava/lang/String;

    return-object v0
.end method

.method public getCompUpdateTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compUpdateTime:Ljava/lang/String;

    return-object v0
.end method

.method public getMd5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->md5:Ljava/lang/String;

    return-object v0
.end method

.method public getPublishTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->publishTime:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->versionCode:I

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->versionName:Ljava/lang/String;

    return-object v0
.end method

.method public isCompCharge()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jrm/appstore/domain/Component;->compCharge:Z

    return v0
.end method

.method public setComId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->comId:Ljava/lang/String;

    return-void
.end method

.method public setCompActClass(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compActClass:Ljava/lang/String;

    return-void
.end method

.method public setCompCharge(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/appstore/domain/Component;->compCharge:Z

    return-void
.end method

.method public setCompDescribe(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compDescribe:Ljava/lang/String;

    return-void
.end method

.method public setCompDeveloper(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compDeveloper:Ljava/lang/String;

    return-void
.end method

.method public setCompDownloadURL(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compDownloadURL:Ljava/lang/String;

    return-void
.end method

.method public setCompFlag(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->compFlag:I

    return-void
.end method

.method public setCompLogoURL(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compLogoURL:Ljava/lang/String;

    return-void
.end method

.method public setCompName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compName:Ljava/lang/String;

    return-void
.end method

.method public setCompPackage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compPackage:Ljava/lang/String;

    return-void
.end method

.method public setCompPrintScreenURL([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compPrintScreenURL:[Ljava/lang/String;

    return-void
.end method

.method public setCompSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->compSize:I

    return-void
.end method

.method public setCompStar(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->compStar:I

    return-void
.end method

.method public setCompType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compType:Ljava/lang/String;

    return-void
.end method

.method public setCompUpdateTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->compUpdateTime:Ljava/lang/String;

    return-void
.end method

.method public setMd5(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->md5:Ljava/lang/String;

    return-void
.end method

.method public setPublishTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->publishTime:Ljava/lang/String;

    return-void
.end method

.method public setVersionCode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/appstore/domain/Component;->versionCode:I

    return-void
.end method

.method public setVersionName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/appstore/domain/Component;->versionName:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Component [comId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->comId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compDeveloper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compDeveloper:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compLogoURL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compLogoURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compPrintScreenURL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compPrintScreenURL:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compPackage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compActClass="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compActClass:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", versionCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/appstore/domain/Component;->versionCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", versionName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->versionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compDescribe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compDescribe:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compUpdateTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compUpdateTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compStar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/appstore/domain/Component;->compStar:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/appstore/domain/Component;->compSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", publishTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->publishTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compDownloadURL="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compDownloadURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/appstore/domain/Component;->compType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compCharge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jrm/appstore/domain/Component;->compCharge:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compFlag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/appstore/domain/Component;->compFlag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->comId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compDeveloper:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compLogoURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compPrintScreenURL:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compPackage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compActClass:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->versionCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->versionName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compDescribe:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compUpdateTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->compStar:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->compSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->publishTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compDownloadURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->compType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/jrm/appstore/domain/Component;->compFlag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/jrm/appstore/domain/Component;->md5:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
