.class Lcom/jrm/appstore/domain/Component$1;
.super Ljava/lang/Object;
.source "Component.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/appstore/domain/Component;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/jrm/appstore/domain/Component;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/jrm/appstore/domain/Component;
    .locals 2
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/jrm/appstore/domain/Component;

    invoke-direct {v0}, Lcom/jrm/appstore/domain/Component;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$0(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$1(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$2(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$3(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$4(Lcom/jrm/appstore/domain/Component;[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$5(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$6(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$7(Lcom/jrm/appstore/domain/Component;I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$8(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$9(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$10(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$11(Lcom/jrm/appstore/domain/Component;I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$12(Lcom/jrm/appstore/domain/Component;I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$13(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$14(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$15(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$16(Lcom/jrm/appstore/domain/Component;I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jrm/appstore/domain/Component;->access$17(Lcom/jrm/appstore/domain/Component;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/appstore/domain/Component$1;->createFromParcel(Landroid/os/Parcel;)Lcom/jrm/appstore/domain/Component;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/jrm/appstore/domain/Component;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/jrm/appstore/domain/Component;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/appstore/domain/Component$1;->newArray(I)[Lcom/jrm/appstore/domain/Component;

    move-result-object v0

    return-object v0
.end method
