.class public Lcom/jrm/core/container/cmps/upgrade/TaskModel;
.super Ljava/lang/Object;
.source "TaskModel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/core/container/cmps/upgrade/TaskModel;",
            ">;"
        }
    .end annotation
.end field

.field public static final STATUS_DOWNLOADING:I = 0x2

.field public static final STATUS_DOWNLOAD_FAILED:I = 0x6

.field public static final STATUS_INPAUSE:I = 0x3

.field public static final STATUS_INSTALLED:I = 0x5

.field public static final STATUS_INSTALL_FAILED:I = 0x7

.field public static final STATUS_PREINSTALL:I = 0x4

.field public static final STATUS_PREPARED:I = 0x1

.field public static final STATUS_PREPARED_FAILED:I = 0x8

.field public static final STATUS_RESUME_FAILED:I = 0x9

.field public static final TYPE_APP:I = 0x1

.field public static final TYPE_CMP:I = 0x2

.field public static final TYPE_UI:I = 0x3


# instance fields
.field private downloadSize:J

.field private downloadUrl:Ljava/lang/String;

.field private localUrl:Ljava/lang/String;

.field private moduleVersion:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private percent:I

.field private status:I

.field private taskId:I

.field private taskName:Ljava/lang/String;

.field private totalSize:J

.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/core/container/cmps/upgrade/TaskModel$1;

    invoke-direct {v0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel$1;-><init>()V

    sput-object v0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->percent:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-ne p1, p0, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;

    iget v3, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->taskId:I

    invoke-virtual {v0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getTaskId()I

    move-result v4

    if-ne v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getDownloadSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->downloadSize:J

    return-wide v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->downloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->localUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getModuleVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->moduleVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPercent()I
    .locals 1

    iget v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->percent:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->status:I

    return v0
.end method

.method public getTaskId()I
    .locals 1

    iget v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->taskId:I

    return v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->taskName:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->totalSize:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->type:I

    return v0
.end method

.method public setDownloadSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->downloadSize:J

    return-void
.end method

.method public setDownloadUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->downloadUrl:Ljava/lang/String;

    return-void
.end method

.method public setLocalUrl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->localUrl:Ljava/lang/String;

    return-void
.end method

.method public setModuleVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->moduleVersion:Ljava/lang/String;

    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->packageName:Ljava/lang/String;

    return-void
.end method

.method public setPercent(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->percent:I

    return-void
.end method

.method public setStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->status:I

    return-void
.end method

.method public setTaskId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->taskId:I

    return-void
.end method

.method public setTaskName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->taskName:Ljava/lang/String;

    return-void
.end method

.method public setTotalSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->totalSize:J

    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->type:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->taskName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->status:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->totalSize:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->percent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->downloadUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getTaskId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getTaskName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getStatus()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getLocalUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getModuleVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getType()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getDownloadUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getDownloadSize()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getTotalSize()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->getPercent()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
