.class Lcom/jrm/core/container/cmps/upgrade/TaskModel$1;
.super Ljava/lang/Object;
.source "TaskModel.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/core/container/cmps/upgrade/TaskModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/jrm/core/container/cmps/upgrade/TaskModel;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/jrm/core/container/cmps/upgrade/TaskModel;
    .locals 3
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/jrm/core/container/cmps/upgrade/TaskModel;

    invoke-direct {v0}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setTaskId(I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setStatus(I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setLocalUrl(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setModuleVersion(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setType(I)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setPackageName(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setDownloadUrl(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setDownloadSize(J)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setTotalSize(J)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->setPercent(I)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel$1;->createFromParcel(Landroid/os/Parcel;)Lcom/jrm/core/container/cmps/upgrade/TaskModel;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/jrm/core/container/cmps/upgrade/TaskModel;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/jrm/core/container/cmps/upgrade/TaskModel;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/core/container/cmps/upgrade/TaskModel$1;->newArray(I)[Lcom/jrm/core/container/cmps/upgrade/TaskModel;

    move-result-object v0

    return-object v0
.end method
