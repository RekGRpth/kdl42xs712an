.class public abstract Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;
.super Landroid/os/Binder;
.source "IUpgradeService.java"

# interfaces
.implements Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.jrm.core.container.cmps.upgrade.IUpgradeService"

.field static final TRANSACTION_cancelUpgradeTask:I = 0x5

.field static final TRANSACTION_createUpgradeTask:I = 0x1

.field static final TRANSACTION_getAllUpgradeTask:I = 0x3

.field static final TRANSACTION_getUpgradeTask:I = 0x2

.field static final TRANSACTION_startUpgradeTask:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-virtual {p0, p0, v0}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v10, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const-string v0, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v10

    goto :goto_0

    :sswitch_1
    const-string v0, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;->createUpgradeTask(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)J

    move-result-wide v8

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    move v0, v10

    goto :goto_0

    :sswitch_2
    const-string v0, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;->getUpgradeTask(J)Lcom/jrm/core/container/cmps/upgrade/TaskModel;

    move-result-object v8

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v8, :cond_0

    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v8, p3, v10}, Lcom/jrm/core/container/cmps/upgrade/TaskModel;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_1
    move v0, v10

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    :sswitch_3
    const-string v0, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;->getAllUpgradeTask(I)[Lcom/jrm/core/container/cmps/upgrade/TaskModel;

    move-result-object v8

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v8, v10}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    move v0, v10

    goto :goto_0

    :sswitch_4
    const-string v0, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;->startUpgradeTask(J)Lcom/jrm/core/container/cmps/upgrade/IUpgradeTask;

    move-result-object v8

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v8, :cond_1

    invoke-interface {v8}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeTask;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    move v0, v10

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :sswitch_5
    const-string v0, "com.jrm.core.container.cmps.upgrade.IUpgradeService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;->cancelUpgradeTask(J)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v10

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
