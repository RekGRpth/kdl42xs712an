.class public abstract Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;
.super Landroid/os/Binder;
.source "ITvDeviceInfo.java"

# interfaces
.implements Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

.field static final TRANSACTION_editTvprofile:I = 0x5

.field static final TRANSACTION_getBBNumer:I = 0x1

.field static final TRANSACTION_getClientId:I = 0x2

.field static final TRANSACTION_getMac:I = 0x3

.field static final TRANSACTION_getTvProfile:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-virtual {p0, p0, v0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    :sswitch_0
    const-string v3, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v3, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;->getBBNumer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    const-string v3, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    const-string v3, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;->getMac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    const-string v3, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;->getTvProfile()Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_0

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v2}, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_5
    const-string v3, "com.jrm.core.container.cmps.authorize.ITvDeviceInfo"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    new-instance v0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;

    invoke-direct {v0}, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;->editTvprofile(Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;)I

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v0, p3, v2}, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
