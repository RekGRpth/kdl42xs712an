.class public abstract Lcom/jrm/service/AbstractJRMServiceAgent;
.super Ljava/lang/Object;
.source "AbstractJRMServiceAgent.java"


# static fields
.field public static final AUTH_SERVICE:Ljava/lang/String; = "jrmsys_IAuthService"

.field public static final DEV_SERVICE:Ljava/lang/String; = "jrmsys_devService"

.field public static final MANAGEMENT_CMPUPGRADE_SERVICE:Ljava/lang/String; = "jrmsys_mgt_upgrade"

.field public static final PROFILE_SERVICE:Ljava/lang/String; = "jrmsys_profileService"

.field public static final SORT_ASC:I = 0x1

.field public static final SORT_DESC:I = 0x2

.field public static final SORT_NAME:Ljava/lang/String; = "dnc"

.field public static final TV_DEVICE_INFO_SERVICE:Ljava/lang/String; = "jrmsys_ITvProfile"

.field public static final UPGRADE_SERVICE:Ljava/lang/String; = "jrmsys_IUpgradeService"

.field public static final version:Ljava/lang/String; = "1.0"


# instance fields
.field protected connection:Lcom/jrm/service/JRMExceptionListener;

.field protected mPkgName:Ljava/lang/String;

.field protected mServiceContainer:Lcom/jrm/core/container/IServiceContainer;


# direct methods
.method public constructor <init>(Lcom/jrm/core/container/IServiceContainer;Lcom/jrm/service/JRMExceptionListener;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/jrm/core/container/IServiceContainer;
    .param p2    # Lcom/jrm/service/JRMExceptionListener;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    iput-object p2, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->connection:Lcom/jrm/service/JRMExceptionListener;

    iput-object p3, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mPkgName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    if-eqz v6, :cond_1

    :try_start_0
    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    iget-object v7, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mPkgName:Ljava/lang/String;

    invoke-interface {v6, p1, p2, v7}, Lcom/jrm/core/container/IServiceContainer;->checkCmp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x2

    if-le v6, v7, :cond_2

    move-object v5, v3

    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    iget-object v7, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mPkgName:Ljava/lang/String;

    invoke-interface {v6, p1, p3, v5, v7}, Lcom/jrm/core/container/IServiceContainer;->getService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    if-eqz v4, :cond_0

    :goto_0
    return-object v4

    :cond_0
    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->connection:Lcom/jrm/service/JRMExceptionListener;

    const/16 v7, 0x5a

    invoke-interface {v6, v7, p1, p2, p3}, Lcom/jrm/service/JRMExceptionListener;->onServiceBindException(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    move-object v4, v0

    goto :goto_0

    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->connection:Lcom/jrm/service/JRMExceptionListener;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->connection:Lcom/jrm/service/JRMExceptionListener;

    invoke-interface {v6, v2, p1, p2, p3}, Lcom/jrm/service/JRMExceptionListener;->onServiceBindException(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/16 v6, 0xb

    if-lt v2, v6, :cond_1

    goto :goto_1

    :cond_4
    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->connection:Lcom/jrm/service/JRMExceptionListener;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->connection:Lcom/jrm/service/JRMExceptionListener;

    const/16 v7, 0x5a

    invoke-interface {v6, v7, p1, p2, p3}, Lcom/jrm/service/JRMExceptionListener;->onServiceBindException(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method protected setConnection(Lcom/jrm/service/JRMExceptionListener;)V
    .locals 0
    .param p1    # Lcom/jrm/service/JRMExceptionListener;

    iput-object p1, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->connection:Lcom/jrm/service/JRMExceptionListener;

    return-void
.end method

.method public setPkgName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/service/AbstractJRMServiceAgent;->mPkgName:Ljava/lang/String;

    return-void
.end method
