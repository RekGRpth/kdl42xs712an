.class Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3$1;
.super Ljava/lang/Object;
.source "JRMServiceManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;


# direct methods
.method constructor <init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3$1;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3$1;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;)Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v0

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3$1;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;)Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v0

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3$1;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;)Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v0

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.jrm.core.container.ServiceContainerService"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    return-void
.end method
