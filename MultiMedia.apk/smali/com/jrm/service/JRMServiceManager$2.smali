.class Lcom/jrm/service/JRMServiceManager$2;
.super Ljava/lang/Object;
.source "JRMServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/service/JRMServiceManager;->aysnConnectToServiceContainer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private time:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    iput v0, p0, Lcom/jrm/service/JRMServiceManager$2;->time:I

    return-void
.end method

.method private connectToSC()V
    .locals 6

    const-string v3, "jrm_sc_1"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/jrm/core/container/IServiceContainer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/IServiceContainer;

    move-result-object v2

    :try_start_0
    # getter for: Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$0()Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/jrm/core/container/IServiceContainer;->registerListener(Lcom/jrm/core/container/IServiceContainerListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v3, Lcom/jrm/service/JRMServiceAgent;

    # getter for: Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$0()Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v4

    # getter for: Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$1()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v2, v4, v5}, Lcom/jrm/service/JRMServiceAgent;-><init>(Lcom/jrm/core/container/IServiceContainer;Lcom/jrm/service/JRMExceptionListener;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager;->access$2(Lcom/jrm/service/JRMServiceAgent;)V

    # getter for: Lcom/jrm/service/JRMServiceManager;->jrmconnector:Lcom/jrm/service/IJRMConnectListener;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$3()Lcom/jrm/service/IJRMConnectListener;

    move-result-object v3

    # getter for: Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$4()Lcom/jrm/service/JRMServiceAgent;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/jrm/service/IJRMConnectListener;->onConnectedSuccess(Lcom/jrm/service/JRMServiceAgent;)V

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "connect success..."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/jrm/service/JRMServiceManager$2;->time:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/jrm/service/JRMServiceManager$2;->time:I

    iget v3, p0, Lcom/jrm/service/JRMServiceManager$2;->time:I

    if-lez v3, :cond_1

    const-wide/16 v3, 0x1f4

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    invoke-direct {p0}, Lcom/jrm/service/JRMServiceManager$2;->connectToSC()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    :cond_1
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "connect failed..."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    # getter for: Lcom/jrm/service/JRMServiceManager;->jrmconnector:Lcom/jrm/service/IJRMConnectListener;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$3()Lcom/jrm/service/IJRMConnectListener;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/jrm/service/IJRMConnectListener;->onConnectedFailed(I)V

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 0

    invoke-direct {p0}, Lcom/jrm/service/JRMServiceManager$2;->connectToSC()V

    return-void
.end method
