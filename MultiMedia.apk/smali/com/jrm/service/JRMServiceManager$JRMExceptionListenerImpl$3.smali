.class Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;
.super Ljava/lang/Object;
.source "JRMServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->onServiceContainerPreRestart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;


# direct methods
.method constructor <init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;)Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    .locals 1

    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "\u5347\u7ea7\u4e86\u540e\u53f0\u7ec4\u4ef6,\u8bf7\u91cd\u542f\u5e94\u7528?"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "\u786e\u5b9a"

    new-instance v4, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3$1;

    invoke-direct {v4, p0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3$1;-><init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
