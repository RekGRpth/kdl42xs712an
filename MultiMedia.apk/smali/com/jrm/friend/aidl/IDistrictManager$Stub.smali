.class public abstract Lcom/jrm/friend/aidl/IDistrictManager$Stub;
.super Landroid/os/Binder;
.source "IDistrictManager.java"

# interfaces
.implements Lcom/jrm/friend/aidl/IDistrictManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/friend/aidl/IDistrictManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/friend/aidl/IDistrictManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.jrm.friend.aidl.IDistrictManager"

.field static final TRANSACTION_addDistrictRefreshListener:I = 0x6

.field static final TRANSACTION_closeManager:I = 0x8

.field static final TRANSACTION_getDistrictById:I = 0x5

.field static final TRANSACTION_getDistricts:I = 0x3

.field static final TRANSACTION_getProvinces:I = 0x2

.field static final TRANSACTION_getProvincesById:I = 0x4

.field static final TRANSACTION_initDistrictManager:I = 0x1

.field static final TRANSACTION_removeDistrictRefreshListener:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p0, p0, v0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/jrm/friend/aidl/IDistrictManager;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.jrm.friend.aidl.IDistrictManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/jrm/friend/aidl/IDistrictManager;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/jrm/friend/aidl/IDistrictManager;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/jrm/friend/aidl/IDistrictManager$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v5, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    :sswitch_0
    const-string v4, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v6, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->initDistrictManager()Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_0

    move v4, v5

    :cond_0
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_2
    const-string v4, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->getProvinces()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    :sswitch_3
    const-string v4, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->getDistricts(I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    :sswitch_4
    const-string v6, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->getProvincesById(I)Lcom/jrm/friend/aidl/Province;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_1

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v5}, Lcom/jrm/friend/aidl/Province;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_5
    const-string v6, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->getDistrictById(I)Lcom/jrm/friend/aidl/District;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_2

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v5}, Lcom/jrm/friend/aidl/District;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_6
    const-string v4, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/friend/aidl/IDistrictDataRefreshListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/friend/aidl/IDistrictDataRefreshListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->addDistrictRefreshListener(Lcom/jrm/friend/aidl/IDistrictDataRefreshListener;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_7
    const-string v4, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/friend/aidl/IDistrictDataRefreshListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/friend/aidl/IDistrictDataRefreshListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->removeDistrictRefreshListener(Lcom/jrm/friend/aidl/IDistrictDataRefreshListener;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_8
    const-string v4, "com.jrm.friend.aidl.IDistrictManager"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->closeManager()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
