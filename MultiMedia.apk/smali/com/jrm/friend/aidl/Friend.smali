.class public Lcom/jrm/friend/aidl/Friend;
.super Ljava/lang/Object;
.source "Friend.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/friend/aidl/Friend;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private friendAddress:Ljava/lang/String;

.field private friendBb:I

.field private friendBirthday:Ljava/lang/String;

.field private friendEmail:Ljava/lang/String;

.field private friendGroupId:I

.field private friendGroupName:Ljava/lang/String;

.field private friendLogo:Ljava/lang/String;

.field private friendMobile:Ljava/lang/String;

.field private friendName:Ljava/lang/String;

.field private friendNickName:Ljava/lang/String;

.field private friendSex:Ljava/lang/String;

.field private friendSignature:Ljava/lang/String;

.field private frienddev:I

.field private remarkName:Ljava/lang/String;

.field private showFlag:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/friend/aidl/Friend$1;

    invoke-direct {v0}, Lcom/jrm/friend/aidl/Friend$1;-><init>()V

    sput-object v0, Lcom/jrm/friend/aidl/Friend;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jrm/friend/aidl/Friend;->friendBb:I

    iput-object p2, p0, Lcom/jrm/friend/aidl/Friend;->friendNickName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getFriendAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendBb()I
    .locals 1

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->friendBb:I

    return v0
.end method

.method public getFriendBirthday()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendBirthday:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendDev()I
    .locals 1

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->frienddev:I

    return v0
.end method

.method public getFriendEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendGroupId()I
    .locals 1

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->friendGroupId:I

    return v0
.end method

.method public getFriendGroupName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendGroupName:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendLogo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendLogo:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendMobile()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendMobile:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendName:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendNickName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendNickName:Ljava/lang/String;

    return-object v0
.end method

.method public getFriendSignature()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendSignature:Ljava/lang/String;

    return-object v0
.end method

.method public getFriend_sex()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendSex:Ljava/lang/String;

    return-object v0
.end method

.method public getRemarkName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->remarkName:Ljava/lang/String;

    return-object v0
.end method

.method public getShowFlag()I
    .locals 1

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->showFlag:I

    return v0
.end method

.method public setFriendAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendAddress:Ljava/lang/String;

    return-void
.end method

.method public setFriendBb(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/friend/aidl/Friend;->friendBb:I

    return-void
.end method

.method public setFriendBirthday(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendBirthday:Ljava/lang/String;

    return-void
.end method

.method public setFriendDeve(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/friend/aidl/Friend;->frienddev:I

    return-void
.end method

.method public setFriendEmail(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendEmail:Ljava/lang/String;

    return-void
.end method

.method public setFriendGroupId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/friend/aidl/Friend;->friendGroupId:I

    return-void
.end method

.method public setFriendGroupName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendGroupName:Ljava/lang/String;

    return-void
.end method

.method public setFriendLogo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendLogo:Ljava/lang/String;

    return-void
.end method

.method public setFriendMobile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendMobile:Ljava/lang/String;

    return-void
.end method

.method public setFriendName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendName:Ljava/lang/String;

    return-void
.end method

.method public setFriendNickName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendNickName:Ljava/lang/String;

    return-void
.end method

.method public setFriendSex(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendSex:Ljava/lang/String;

    return-void
.end method

.method public setFriendSignature(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->friendSignature:Ljava/lang/String;

    return-void
.end method

.method public setRemarkName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/friend/aidl/Friend;->remarkName:Ljava/lang/String;

    return-void
.end method

.method public setShowFlag(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/friend/aidl/Friend;->showFlag:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->friendBb:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendNickName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->remarkName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendGroupName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendSex:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendEmail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendLogo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendMobile:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/Friend;->friendSignature:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->showFlag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->frienddev:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/jrm/friend/aidl/Friend;->friendGroupId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
