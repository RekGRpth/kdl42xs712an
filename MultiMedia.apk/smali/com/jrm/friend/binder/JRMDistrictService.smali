.class public Lcom/jrm/friend/binder/JRMDistrictService;
.super Ljava/lang/Object;
.source "JRMDistrictService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;
    }
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field private listener:Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;

.field private mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;


# direct methods
.method public constructor <init>(Lcom/jrm/friend/aidl/IDistrictManager;)V
    .locals 3
    .param p1    # Lcom/jrm/friend/aidl/IDistrictManager;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    invoke-interface {v1}, Lcom/jrm/friend/aidl/IDistrictManager;->initDistrictManager()Z

    new-instance v1, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;-><init>(Lcom/jrm/friend/binder/JRMDistrictService;Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;)V

    iput-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->listener:Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    iget-object v2, p0, Lcom/jrm/friend/binder/JRMDistrictService;->listener:Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;

    invoke-interface {v1, v2}, Lcom/jrm/friend/aidl/IDistrictManager;->addDistrictRefreshListener(Lcom/jrm/friend/aidl/IDistrictDataRefreshListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/jrm/friend/binder/JRMDistrictService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMDistrictService;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public bindHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->handler:Landroid/os/Handler;

    return-void
.end method

.method public destroyService()V
    .locals 3

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    invoke-interface {v1}, Lcom/jrm/friend/aidl/IDistrictManager;->closeManager()V

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    iget-object v2, p0, Lcom/jrm/friend/binder/JRMDistrictService;->listener:Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;

    invoke-interface {v1, v2}, Lcom/jrm/friend/aidl/IDistrictManager;->removeDistrictRefreshListener(Lcom/jrm/friend/aidl/IDistrictDataRefreshListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDistrictById(I)Lcom/jrm/friend/aidl/District;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    invoke-interface {v1, p1}, Lcom/jrm/friend/aidl/IDistrictManager;->getDistrictById(I)Lcom/jrm/friend/aidl/District;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDistricts(I)Ljava/util/List;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/friend/aidl/District;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    invoke-interface {v1, p1}, Lcom/jrm/friend/aidl/IDistrictManager;->getDistricts(I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProvinces()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/friend/aidl/Province;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    invoke-interface {v1}, Lcom/jrm/friend/aidl/IDistrictManager;->getProvinces()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProvincesById(I)Lcom/jrm/friend/aidl/Province;
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMDistrictService;->mDistrictService:Lcom/jrm/friend/aidl/IDistrictManager;

    invoke-interface {v1, p1}, Lcom/jrm/friend/aidl/IDistrictManager;->getProvincesById(I)Lcom/jrm/friend/aidl/Province;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
