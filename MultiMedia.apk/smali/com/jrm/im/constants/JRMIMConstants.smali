.class public Lcom/jrm/im/constants/JRMIMConstants;
.super Ljava/lang/Object;
.source "JRMIMConstants.java"


# static fields
.field public static final CONNECT_BEGINT:I = 0x10

.field public static final CONNECT_FAILED:I = 0x11

.field public static final CONNECT_SUCCESS:I = 0x12

.field public static final LOGIN_BEGIN:I = 0x20

.field public static final LOGIN_FAILE:I = 0x21

.field public static final LOGIN_SUCCESS:I = 0x22

.field public static final MESSAGE_BODY:Ljava/lang/String; = "body"

.field public static final MESSAGE_OBJECT:Ljava/lang/String; = "msg_object"

.field public static final MESSAGE_SENDER:Ljava/lang/String; = "sender"

.field public static final MESSAGE_TIMESTAMP:Ljava/lang/String; = "time"

.field public static final MESSAGE_TO:Ljava/lang/String; = "to"

.field public static final USER_ADDFRIEND_TAG:Ljava/lang/String; = "fg"

.field public static final USER_BRITHEDAY:Ljava/lang/String; = "bh"

.field public static final USER_CITY:Ljava/lang/String; = "ct"

.field public static final USER_EMAIL:Ljava/lang/String; = "ml"

.field public static final USER_JOG:Ljava/lang/String; = "jb"

.field public static final USER_LOGO:Ljava/lang/String; = "lo"

.field public static final USER_MOBILE:Ljava/lang/String; = "mp"

.field public static final USER_NIKE_NAME:Ljava/lang/String; = "nk"

.field public static final USER_OTHER:Ljava/lang/String; = "ot"

.field public static final USER_PID:Ljava/lang/String; = "id"

.field public static final USER_SEX:Ljava/lang/String; = "sx"

.field public static final USER_SIGNUATURE:Ljava/lang/String; = "sg"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
