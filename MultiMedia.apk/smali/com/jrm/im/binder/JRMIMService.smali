.class public Lcom/jrm/im/binder/JRMIMService;
.super Ljava/lang/Object;
.source "JRMIMService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/im/binder/JRMIMService$AddFriendListener;,
        Lcom/jrm/im/binder/JRMIMService$ChatListener;,
        Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;,
        Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;,
        Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;,
        Lcom/jrm/im/binder/JRMIMService$FriendStatus;
    }
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field private mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;

.field private mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;

.field private mFriendDeviceStateListener:Lcom/jrm/im/listener/OnFriendDeviceStateListener;

.field private mFriendStateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/im/binder/JRMIMService$FriendStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mFriendStateListenerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/im/listener/OnFriendStateChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

.field private mMessageListener:Lcom/jrm/im/listener/OnMessageListener;


# direct methods
.method public constructor <init>(Lcom/jrm/im/aidl/IMManagerService;)V
    .locals 4
    .param p1    # Lcom/jrm/im/aidl/IMManagerService;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    new-instance v2, Lcom/jrm/im/binder/JRMIMService$ChatListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jrm/im/binder/JRMIMService$ChatListener;-><init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$ChatListener;)V

    invoke-interface {v1, v2}, Lcom/jrm/im/aidl/IMManagerService;->setChatMessageListener(Lcom/jrm/im/aidl/IChatMessageListener;)V

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    new-instance v2, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;-><init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;)V

    invoke-interface {v1, v2}, Lcom/jrm/im/aidl/IMManagerService;->setFriendStateChangeListener(Lcom/jrm/im/aidl/IFriendStateChangeListener;)V

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    new-instance v2, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;

    invoke-direct {v2, p0}, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;-><init>(Lcom/jrm/im/binder/JRMIMService;)V

    invoke-interface {v1, v2}, Lcom/jrm/im/aidl/IMManagerService;->setAddFriendListener(Lcom/jrm/im/aidl/IAddFriendRequestListener;)V

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    new-instance v2, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;-><init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;)V

    invoke-interface {v1, v2}, Lcom/jrm/im/aidl/IMManagerService;->setDeviceStateChangeListener(Lcom/jrm/im/aidl/IDeviceChangeListener;)V

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    new-instance v2, Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;-><init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;)V

    invoke-interface {v1, v2}, Lcom/jrm/im/aidl/IMManagerService;->setFriendDeviceStateChangeListener(Lcom/jrm/im/aidl/IFriendDeviceChangeListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnMessageListener;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mMessageListener:Lcom/jrm/im/listener/OnMessageListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/jrm/im/binder/JRMIMService;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lcom/jrm/im/binder/JRMIMService;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnDeviceChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;

    return-object v0
.end method

.method static synthetic access$4(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnFriendDeviceStateListener;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendDeviceStateListener:Lcom/jrm/im/listener/OnFriendDeviceStateListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnAddFriendListener;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;

    return-object v0
.end method


# virtual methods
.method public acceptFriendMessage(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->acceptFriendMessage(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public bindHander(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    return-void
.end method

.method public changeUserState(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->changeUserState(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public closeChat(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->closeChat(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public connect(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/im/aidl/IMManagerService;->connect(ILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public disConnect()V
    .locals 2

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1}, Lcom/jrm/im/aidl/IMManagerService;->disConnect()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    :cond_1
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFriendState(I)I
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v1, 0x64

    :goto_0
    return v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;

    iget v2, v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;->friendBBNo:I

    if-ne v2, p1, :cond_0

    iget v1, v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;->friendStatus:I

    goto :goto_0
.end method

.method public getUserInfo()Lcom/jrm/im/aidl/UserInfo;
    .locals 2

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1}, Lcom/jrm/im/aidl/IMManagerService;->getUserInfo()Lcom/jrm/im/aidl/UserInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public login(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    iget-object v2, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v2, p1}, Lcom/jrm/im/aidl/IMManagerService;->login(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    if-nez v1, :cond_3

    iget-object v2, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    const/16 v3, 0x21

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/jrm/im/binder/JRMIMService;->handler:Landroid/os/Handler;

    const/16 v3, 0x22

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method public modifyUser([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/im/aidl/IMManagerService;->modifyUser([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public openChat(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->openChat(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public pauseChat(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->pauseChat(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public refuseFriendMessage(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->refuseFriendMessage(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public removeAddFriendListener()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;

    return-void
.end method

.method public removeDeviceStateChangeListener()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;

    return-void
.end method

.method public removeFriendStateChangeListener(Lcom/jrm/im/listener/OnFriendStateChangeListener;)V
    .locals 1
    .param p1    # Lcom/jrm/im/listener/OnFriendStateChangeListener;

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public removeMessageListener()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/im/binder/JRMIMService;->mMessageListener:Lcom/jrm/im/listener/OnMessageListener;

    return-void
.end method

.method public sendAddFriendMessage(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->sendAddFriendMessage(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendMessage(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p2, p1}, Lcom/jrm/im/aidl/IMManagerService;->sendMessage(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAddFriendListener(Lcom/jrm/im/listener/OnAddFriendListener;)V
    .locals 0
    .param p1    # Lcom/jrm/im/listener/OnAddFriendListener;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;

    :cond_0
    return-void
.end method

.method public setDeviceStateChangeListener(Lcom/jrm/im/listener/OnDeviceChangeListener;)V
    .locals 0
    .param p1    # Lcom/jrm/im/listener/OnDeviceChangeListener;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService;->mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;

    :cond_0
    return-void
.end method

.method public setDomain(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMManagerService;->setDomain(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFriendDeviceStateChangeListener(Lcom/jrm/im/listener/OnFriendDeviceStateListener;)V
    .locals 0
    .param p1    # Lcom/jrm/im/listener/OnFriendDeviceStateListener;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendDeviceStateListener:Lcom/jrm/im/listener/OnFriendDeviceStateListener;

    :cond_0
    return-void
.end method

.method public setFriendStateChangeListener(Lcom/jrm/im/listener/OnFriendStateChangeListener;)V
    .locals 4
    .param p1    # Lcom/jrm/im/listener/OnFriendStateChangeListener;

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;

    iget v2, v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;->friendBBNo:I

    iget v3, v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;->friendStatus:I

    invoke-interface {p1, v2, v3}, Lcom/jrm/im/listener/OnFriendStateChangeListener;->onFriendStateChange(II)V

    goto :goto_0
.end method

.method public setHostAndPort(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMService;->mIMManagerService:Lcom/jrm/im/aidl/IMManagerService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/im/aidl/IMManagerService;->setHostAndPort(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setMessageListener(Lcom/jrm/im/listener/OnMessageListener;)V
    .locals 0
    .param p1    # Lcom/jrm/im/listener/OnMessageListener;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService;->mMessageListener:Lcom/jrm/im/listener/OnMessageListener;

    :cond_0
    return-void
.end method
