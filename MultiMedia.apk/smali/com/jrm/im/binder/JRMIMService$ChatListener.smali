.class Lcom/jrm/im/binder/JRMIMService$ChatListener;
.super Lcom/jrm/im/aidl/IChatMessageListener$Stub;
.source "JRMIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/im/binder/JRMIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChatListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/im/binder/JRMIMService;


# direct methods
.method private constructor <init>(Lcom/jrm/im/binder/JRMIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService$ChatListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    invoke-direct {p0}, Lcom/jrm/im/aidl/IChatMessageListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$ChatListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jrm/im/binder/JRMIMService$ChatListener;-><init>(Lcom/jrm/im/binder/JRMIMService;)V

    return-void
.end method


# virtual methods
.method public openedChatMessage(ILjava/lang/String;J)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$ChatListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mMessageListener:Lcom/jrm/im/listener/OnMessageListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$0(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnMessageListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$ChatListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mMessageListener:Lcom/jrm/im/listener/OnMessageListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$0(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnMessageListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/jrm/im/listener/OnMessageListener;->OnOpenMessageCome(ILjava/lang/String;J)V

    :cond_0
    return-void
.end method

.method public unOpenedChatMessage(ILjava/lang/String;J)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$ChatListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mMessageListener:Lcom/jrm/im/listener/OnMessageListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$0(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnMessageListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$ChatListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mMessageListener:Lcom/jrm/im/listener/OnMessageListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$0(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnMessageListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/jrm/im/listener/OnMessageListener;->OnUnopenMessageCome(ILjava/lang/String;J)V

    :cond_0
    return-void
.end method
