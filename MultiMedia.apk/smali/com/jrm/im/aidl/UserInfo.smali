.class public Lcom/jrm/im/aidl/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/im/aidl/UserInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private show_flag:S

.field private user_address:Ljava/lang/String;

.field private user_birthday:Ljava/lang/String;

.field private user_email:Ljava/lang/String;

.field private user_id:I

.field private user_imsinumber:Ljava/lang/String;

.field private user_job:Ljava/lang/String;

.field private user_logo:Ljava/lang/String;

.field private user_name:Ljava/lang/String;

.field private user_nickname:Ljava/lang/String;

.field private user_password:Ljava/lang/String;

.field private user_phone:Ljava/lang/String;

.field private user_sex:Ljava/lang/String;

.field private user_slelf_name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/im/aidl/UserInfo$1;

    invoke-direct {v0}, Lcom/jrm/im/aidl/UserInfo$1;-><init>()V

    sput-object v0, Lcom/jrm/im/aidl/UserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getShow_flag()S
    .locals 1

    iget-short v0, p0, Lcom/jrm/im/aidl/UserInfo;->show_flag:S

    return v0
.end method

.method public getUser_address()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_address:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_birthday()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_birthday:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_email()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_email:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_id()I
    .locals 1

    iget v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_id:I

    return v0
.end method

.method public getUser_imsinumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_imsinumber:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_job()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_job:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_logo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_logo:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_name()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_name:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_nickname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_nickname:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_password()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_password:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_phone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_phone:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_sex()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_sex:Ljava/lang/String;

    return-object v0
.end method

.method public getUser_slelf_name()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_slelf_name:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_id:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_name:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_password:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_nickname:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_logo:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_sex:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_address:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_email:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_phone:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_birthday:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_job:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_slelf_name:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_imsinumber:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/jrm/im/aidl/UserInfo;->show_flag:S

    return-void
.end method

.method public setShow_flag(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/jrm/im/aidl/UserInfo;->show_flag:S

    return-void
.end method

.method public setUser_address(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_address:Ljava/lang/String;

    return-void
.end method

.method public setUser_birthday(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_birthday:Ljava/lang/String;

    return-void
.end method

.method public setUser_email(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_email:Ljava/lang/String;

    return-void
.end method

.method public setUser_id(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_id:I

    return-void
.end method

.method public setUser_imsinumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_imsinumber:Ljava/lang/String;

    return-void
.end method

.method public setUser_job(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_job:Ljava/lang/String;

    return-void
.end method

.method public setUser_logo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_logo:Ljava/lang/String;

    return-void
.end method

.method public setUser_name(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_name:Ljava/lang/String;

    return-void
.end method

.method public setUser_nickname(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_nickname:Ljava/lang/String;

    return-void
.end method

.method public setUser_password(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_password:Ljava/lang/String;

    return-void
.end method

.method public setUser_phone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_phone:Ljava/lang/String;

    return-void
.end method

.method public setUser_sex(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_sex:Ljava/lang/String;

    return-void
.end method

.method public setUser_slelf_name(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/im/aidl/UserInfo;->user_slelf_name:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_password:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_nickname:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_logo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_sex:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_address:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_phone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_birthday:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_job:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/im/aidl/UserInfo;->user_slelf_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-short v0, p0, Lcom/jrm/im/aidl/UserInfo;->show_flag:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
