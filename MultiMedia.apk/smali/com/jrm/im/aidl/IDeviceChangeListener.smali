.class public interface abstract Lcom/jrm/im/aidl/IDeviceChangeListener;
.super Ljava/lang/Object;
.source "IDeviceChangeListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/im/aidl/IDeviceChangeListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onCameraDeviceChange(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onMicDeviceChange(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
