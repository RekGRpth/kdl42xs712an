.class public Liapp/eric/utils/metadata/DownloadTaskBR;
.super Ljava/lang/Object;
.source "DownloadTaskBR.java"


# instance fields
.field public blockSize:J

.field public completedSize:J

.field public isFinish:Z

.field public saveFile:Ljava/io/File;

.field public threadId:I

.field public url:Ljava/net/URL;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Liapp/eric/utils/metadata/DownloadTaskBR;->threadId:I

    iput-wide v1, p0, Liapp/eric/utils/metadata/DownloadTaskBR;->blockSize:J

    iput-wide v1, p0, Liapp/eric/utils/metadata/DownloadTaskBR;->completedSize:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Liapp/eric/utils/metadata/DownloadTaskBR;->isFinish:Z

    return-void
.end method
