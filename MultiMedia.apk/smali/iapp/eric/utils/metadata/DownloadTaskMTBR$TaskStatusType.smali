.class public final enum Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;
.super Ljava/lang/Enum;
.source "DownloadTaskMTBR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Liapp/eric/utils/metadata/DownloadTaskMTBR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TaskStatusType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DOWNLOADING:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

.field private static final synthetic ENUM$VALUES:[Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

.field public static final enum IDLE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

.field public static final enum PAUSE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

.field public static final enum WAITING:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->IDLE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    new-instance v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v3}, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->DOWNLOADING:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    new-instance v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v4}, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->WAITING:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    new-instance v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v5}, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->PAUSE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    const/4 v0, 0x4

    new-array v0, v0, [Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    sget-object v1, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->IDLE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    aput-object v1, v0, v2

    sget-object v1, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->DOWNLOADING:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    aput-object v1, v0, v3

    sget-object v1, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->WAITING:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    aput-object v1, v0, v4

    sget-object v1, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->PAUSE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    aput-object v1, v0, v5

    sput-object v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->ENUM$VALUES:[Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;
    .locals 1

    const-class v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    return-object v0
.end method

.method public static values()[Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->ENUM$VALUES:[Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    array-length v1, v0

    new-array v2, v1, [Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
