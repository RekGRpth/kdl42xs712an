.class Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;
.super Ljava/lang/Thread;
.source "DownloadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Liapp/eric/utils/base/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadThreadBR"
.end annotation


# instance fields
.field private task:Liapp/eric/utils/metadata/DownloadTaskBR;

.field final synthetic this$0:Liapp/eric/utils/base/DownloadManager;


# direct methods
.method public constructor <init>(Liapp/eric/utils/base/DownloadManager;Liapp/eric/utils/metadata/DownloadTaskBR;)V
    .locals 0
    .param p2    # Liapp/eric/utils/metadata/DownloadTaskBR;

    iput-object p1, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->this$0:Liapp/eric/utils/base/DownloadManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    return-void
.end method

.method static synthetic access$0(Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;)Liapp/eric/utils/metadata/DownloadTaskBR;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 15

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-wide v10, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->completedSize:J

    iget-object v12, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-wide v12, v12, Liapp/eric/utils/metadata/DownloadTaskBR;->blockSize:J

    cmp-long v10, v10, v12

    if-gez v10, :cond_0

    :try_start_0
    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-object v10, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->url:Ljava/net/URL;

    invoke-virtual {v10}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    const/16 v10, 0x1388

    invoke-virtual {v4, v10}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string v10, "GET"

    invoke-virtual {v4, v10}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v10, "Accept"

    const-string v11, "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*"

    invoke-virtual {v4, v10, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "Accept-Language"

    const-string v11, "zh-CN"

    invoke-virtual {v4, v10, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "Referer"

    iget-object v11, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-object v11, v11, Liapp/eric/utils/metadata/DownloadTaskBR;->url:Ljava/net/URL;

    invoke-virtual {v11}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "Charset"

    const-string v11, "UTF-8"

    invoke-virtual {v4, v10, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-wide v10, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->blockSize:J

    iget-object v12, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget v12, v12, Liapp/eric/utils/metadata/DownloadTaskBR;->threadId:I

    add-int/lit8 v12, v12, -0x1

    int-to-long v12, v12

    mul-long/2addr v10, v12

    iget-object v12, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-wide v12, v12, Liapp/eric/utils/metadata/DownloadTaskBR;->completedSize:J

    add-long v7, v10, v12

    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-wide v10, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->blockSize:J

    iget-object v12, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget v12, v12, Liapp/eric/utils/metadata/DownloadTaskBR;->threadId:I

    int-to-long v12, v12

    mul-long/2addr v10, v12

    const-wide/16 v12, 0x1

    sub-long v2, v10, v12

    const-string v10, "Range"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "bytes="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "User-Agent"

    const-string v11, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)"

    invoke-virtual {v4, v10, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "Connection"

    const-string v11, "Keep-Alive"

    invoke-virtual {v4, v10, v11}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    const/16 v10, 0x400

    new-array v0, v10, [B

    const/4 v6, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Thread "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget v11, v11, Liapp/eric/utils/metadata/DownloadTaskBR;->threadId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " start download from position "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    new-instance v9, Ljava/io/RandomAccessFile;

    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-object v10, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->saveFile:Ljava/io/File;

    const-string v11, "rwd"

    invoke-direct {v9, v10, v11}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9, v7, v8}, Ljava/io/RandomAccessFile;->seek(J)V

    :goto_0
    const/4 v10, 0x0

    const/16 v11, 0x400

    invoke-virtual {v5, v0, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    const/4 v10, -0x1

    if-ne v6, v10, :cond_1

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Thread "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget v11, v11, Liapp/eric/utils/metadata/DownloadTaskBR;->threadId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " download finish"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    const/4 v11, 0x1

    iput-boolean v11, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->isFinish:Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v10, 0x0

    invoke-virtual {v9, v0, v10, v6}, Ljava/io/RandomAccessFile;->write([BII)V

    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget-wide v11, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->completedSize:J

    int-to-long v13, v6

    add-long/2addr v11, v13

    iput-wide v11, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->completedSize:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v10, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    const-wide/16 v11, -0x1

    iput-wide v11, v10, Liapp/eric/utils/metadata/DownloadTaskBR;->completedSize:J

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Thread "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;

    iget v11, v11, Liapp/eric/utils/metadata/DownloadTaskBR;->threadId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    goto :goto_1
.end method
