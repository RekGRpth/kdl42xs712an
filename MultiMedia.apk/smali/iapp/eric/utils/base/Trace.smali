.class public Liapp/eric/utils/base/Trace;
.super Ljava/lang/Object;
.source "Trace.java"


# static fields
.field private static final DEFAULT_TAG:Ljava/lang/String; = "iAppEricUtils"

.field public static final TRACE_DEBUG:I = 0x1

.field public static final TRACE_FATAL:I = 0x8

.field public static final TRACE_INFO:I = 0x4

.field public static final TRACE_WARNING:I = 0x2

.field private static debug:Z

.field private static fatal:Z

.field private static info:Z

.field private static m_tag:Ljava/lang/String;

.field private static warning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    const-string v0, "iAppEricUtils"

    sput-object v0, Liapp/eric/utils/base/Trace;->m_tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Debug(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v0, v1, v2

    if-eqz v0, :cond_0

    sget-object v1, Liapp/eric/utils/base/Trace;->m_tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static Fatal(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v0, v1, v2

    if-eqz v0, :cond_0

    sget-object v1, Liapp/eric/utils/base/Trace;->m_tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static Info(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v0, v1, v2

    if-eqz v0, :cond_0

    sget-object v1, Liapp/eric/utils/base/Trace;->m_tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static Warning(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v0, v1, v2

    if-eqz v0, :cond_0

    sget-object v1, Liapp/eric/utils/base/Trace;->m_tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static setFilter(I)V
    .locals 2
    .param p0    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sput-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    goto :goto_0

    :pswitch_1
    sput-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    goto :goto_0

    :pswitch_2
    sput-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    goto :goto_0

    :pswitch_3
    sput-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    goto :goto_0

    :pswitch_4
    sput-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    goto :goto_0

    :pswitch_5
    sput-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    goto :goto_0

    :pswitch_6
    sput-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    goto :goto_0

    :pswitch_7
    sput-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    goto :goto_0

    :pswitch_8
    sput-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    goto :goto_0

    :pswitch_9
    sput-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    goto :goto_0

    :pswitch_a
    sput-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->fatal:Z

    goto :goto_0

    :pswitch_b
    sput-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->info:Z

    goto :goto_0

    :pswitch_c
    sput-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->debug:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->warning:Z

    goto :goto_0

    :pswitch_d
    sput-boolean v1, Liapp/eric/utils/base/Trace;->fatal:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->info:Z

    sput-boolean v1, Liapp/eric/utils/base/Trace;->warning:Z

    sput-boolean v0, Liapp/eric/utils/base/Trace;->debug:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_a
        :pswitch_3
        :pswitch_6
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static setTag(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Liapp/eric/utils/base/Trace;->m_tag:Ljava/lang/String;

    return-void
.end method
