.class public final enum Liapp/eric/utils/base/FileOperations$SortordType;
.super Ljava/lang/Enum;
.source "FileOperations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Liapp/eric/utils/base/FileOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SortordType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Liapp/eric/utils/base/FileOperations$SortordType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Liapp/eric/utils/base/FileOperations$SortordType;

.field public static final enum SORT_BY_DIR:Liapp/eric/utils/base/FileOperations$SortordType;

.field public static final enum SORT_BY_MODIFIED_DATE:Liapp/eric/utils/base/FileOperations$SortordType;

.field public static final enum SORT_BY_NAME:Liapp/eric/utils/base/FileOperations$SortordType;

.field public static final enum SORT_BY_SIZE:Liapp/eric/utils/base/FileOperations$SortordType;

.field public static final enum SORT_BY_TYPE:Liapp/eric/utils/base/FileOperations$SortordType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Liapp/eric/utils/base/FileOperations$SortordType;

    const-string v1, "SORT_BY_NAME"

    invoke-direct {v0, v1, v2}, Liapp/eric/utils/base/FileOperations$SortordType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_NAME:Liapp/eric/utils/base/FileOperations$SortordType;

    new-instance v0, Liapp/eric/utils/base/FileOperations$SortordType;

    const-string v1, "SORT_BY_SIZE"

    invoke-direct {v0, v1, v3}, Liapp/eric/utils/base/FileOperations$SortordType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_SIZE:Liapp/eric/utils/base/FileOperations$SortordType;

    new-instance v0, Liapp/eric/utils/base/FileOperations$SortordType;

    const-string v1, "SORT_BY_MODIFIED_DATE"

    invoke-direct {v0, v1, v4}, Liapp/eric/utils/base/FileOperations$SortordType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_MODIFIED_DATE:Liapp/eric/utils/base/FileOperations$SortordType;

    new-instance v0, Liapp/eric/utils/base/FileOperations$SortordType;

    const-string v1, "SORT_BY_DIR"

    invoke-direct {v0, v1, v5}, Liapp/eric/utils/base/FileOperations$SortordType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_DIR:Liapp/eric/utils/base/FileOperations$SortordType;

    new-instance v0, Liapp/eric/utils/base/FileOperations$SortordType;

    const-string v1, "SORT_BY_TYPE"

    invoke-direct {v0, v1, v6}, Liapp/eric/utils/base/FileOperations$SortordType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_TYPE:Liapp/eric/utils/base/FileOperations$SortordType;

    const/4 v0, 0x5

    new-array v0, v0, [Liapp/eric/utils/base/FileOperations$SortordType;

    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_NAME:Liapp/eric/utils/base/FileOperations$SortordType;

    aput-object v1, v0, v2

    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_SIZE:Liapp/eric/utils/base/FileOperations$SortordType;

    aput-object v1, v0, v3

    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_MODIFIED_DATE:Liapp/eric/utils/base/FileOperations$SortordType;

    aput-object v1, v0, v4

    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_DIR:Liapp/eric/utils/base/FileOperations$SortordType;

    aput-object v1, v0, v5

    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_TYPE:Liapp/eric/utils/base/FileOperations$SortordType;

    aput-object v1, v0, v6

    sput-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->ENUM$VALUES:[Liapp/eric/utils/base/FileOperations$SortordType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Liapp/eric/utils/base/FileOperations$SortordType;
    .locals 1

    const-class v0, Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Liapp/eric/utils/base/FileOperations$SortordType;

    return-object v0
.end method

.method public static values()[Liapp/eric/utils/base/FileOperations$SortordType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->ENUM$VALUES:[Liapp/eric/utils/base/FileOperations$SortordType;

    array-length v1, v0

    new-array v2, v1, [Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
