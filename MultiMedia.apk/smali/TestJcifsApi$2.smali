.class LTestJcifsApi$2;
.super Lorg/jmock/Expectations;
.source "TestJcifsApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = LTestJcifsApi;->setUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:LTestJcifsApi;


# direct methods
.method constructor <init>(LTestJcifsApi;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    iput-object p1, p0, LTestJcifsApi$2;->this$0:LTestJcifsApi;

    invoke-direct {p0}, Lorg/jmock/Expectations;-><init>()V

    # getter for: LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;
    invoke-static {p1}, LTestJcifsApi;->access$0(LTestJcifsApi;)Ljcifs/smb/SmbFile;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->oneOf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getType()I

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LTestJcifsApi$2;->returnValue(Ljava/lang/Object;)Lorg/jmock/api/Action;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->will(Lorg/jmock/api/Action;)V

    # getter for: LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;
    invoke-static {p1}, LTestJcifsApi;->access$0(LTestJcifsApi;)Ljcifs/smb/SmbFile;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->oneOf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getType()I

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LTestJcifsApi$2;->returnValue(Ljava/lang/Object;)Lorg/jmock/api/Action;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->will(Lorg/jmock/api/Action;)V

    # getter for: LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;
    invoke-static {p1}, LTestJcifsApi;->access$0(LTestJcifsApi;)Ljcifs/smb/SmbFile;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->oneOf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getType()I

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LTestJcifsApi$2;->returnValue(Ljava/lang/Object;)Lorg/jmock/api/Action;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->will(Lorg/jmock/api/Action;)V

    # getter for: LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;
    invoke-static {p1}, LTestJcifsApi;->access$0(LTestJcifsApi;)Ljcifs/smb/SmbFile;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->oneOf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getType()I

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LTestJcifsApi$2;->returnValue(Ljava/lang/Object;)Lorg/jmock/api/Action;

    move-result-object v0

    invoke-virtual {p0, v0}, LTestJcifsApi$2;->will(Lorg/jmock/api/Action;)V

    return-void
.end method
