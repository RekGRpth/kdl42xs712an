.class public Ljargs/test/gnu/CmdLineParserTestCase;
.super Ljunit/framework/TestCase;
.source "CmdLineParserTestCase.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Ljunit/framework/TestCase;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public testBadFormat()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljargs/gnu/CmdLineParser;

    invoke-direct {v0}, Ljargs/gnu/CmdLineParser;-><init>()V

    const/16 v2, 0x73

    const-string v3, "size"

    invoke-virtual {v0, v2, v3}, Ljargs/gnu/CmdLineParser;->addIntegerOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v1

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "--size=blah"

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;)V

    const-string v2, "Expected IllegalOptionValueException"

    invoke-static {v2}, Ljunit/framework/Assert;->fail(Ljava/lang/String;)V
    :try_end_0
    .catch Ljargs/gnu/CmdLineParser$IllegalOptionValueException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public testDetachedOption()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v5, 0x0

    new-instance v1, Ljargs/gnu/CmdLineParser;

    invoke-direct {v1}, Ljargs/gnu/CmdLineParser;-><init>()V

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$BooleanOption;

    const/16 v2, 0x76

    const-string v3, "verbose"

    invoke-direct {v0, v2, v3}, Ljargs/gnu/CmdLineParser$Option$BooleanOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {v1, v0}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v5, v2}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "-v"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;)V

    const-string v2, "UnknownOptionException expected"

    invoke-static {v2}, Ljunit/framework/Assert;->fail(Ljava/lang/String;)V
    :try_end_0
    .catch Ljargs/gnu/CmdLineParser$UnknownOptionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v1, v0}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v5, v2}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public testLocale()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-wide v0, 0x3fc999999999999aL    # 0.2

    const-wide v4, 0x3e7ad7f29abcaf48L    # 1.0E-7

    new-instance v7, Ljargs/gnu/CmdLineParser;

    invoke-direct {v7}, Ljargs/gnu/CmdLineParser;-><init>()V

    const/16 v2, 0x66

    const-string v3, "fraction"

    invoke-virtual {v7, v2, v3}, Ljargs/gnu/CmdLineParser;->addDoubleOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v6

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "--fraction=0.2"

    aput-object v3, v2, v8

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v2, v3}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v7, v6}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static/range {v0 .. v5}, Ljunit/framework/Assert;->assertEquals(DDD)V

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "--fraction=0,2"

    aput-object v3, v2, v8

    sget-object v3, Ljava/util/Locale;->GERMANY:Ljava/util/Locale;

    invoke-virtual {v7, v2, v3}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v7, v6}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static/range {v0 .. v5}, Ljunit/framework/Assert;->assertEquals(DDD)V

    return-void
.end method

.method public testResetBetweenParse()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v4, 0x0

    new-instance v0, Ljargs/gnu/CmdLineParser;

    invoke-direct {v0}, Ljargs/gnu/CmdLineParser;-><init>()V

    const/16 v2, 0x76

    const-string v3, "verbose"

    invoke-virtual {v0, v2, v3}, Ljargs/gnu/CmdLineParser;->addBooleanOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "-v"

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;)V

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    new-array v2, v4, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public testStandardOptions()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v14, 0x1

    const/4 v13, 0x0

    new-instance v10, Ljargs/gnu/CmdLineParser;

    invoke-direct {v10}, Ljargs/gnu/CmdLineParser;-><init>()V

    const/16 v0, 0x76

    const-string v1, "verbose"

    invoke-virtual {v10, v0, v1}, Ljargs/gnu/CmdLineParser;->addBooleanOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v12

    const/16 v0, 0x73

    const-string v1, "size"

    invoke-virtual {v10, v0, v1}, Ljargs/gnu/CmdLineParser;->addIntegerOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v11

    const/16 v0, 0x6e

    const-string v1, "name"

    invoke-virtual {v10, v0, v1}, Ljargs/gnu/CmdLineParser;->addStringOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v8

    const/16 v0, 0x66

    const-string v1, "fraction"

    invoke-virtual {v10, v0, v1}, Ljargs/gnu/CmdLineParser;->addDoubleOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v6

    const/16 v0, 0x6d

    const-string v1, "missing"

    invoke-virtual {v10, v0, v1}, Ljargs/gnu/CmdLineParser;->addBooleanOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v7

    invoke-virtual {v10, v11}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "-v"

    aput-object v1, v0, v13

    const-string v1, "--size=100"

    aput-object v1, v0, v14

    const/4 v1, 0x2

    const-string v2, "-n"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "foo"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "-f"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "0.1"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "rest"

    aput-object v2, v0, v1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v10, v0, v1}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v10, v7}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v10, v12}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v1, 0x64

    invoke-virtual {v10, v11}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Ljunit/framework/Assert;->assertEquals(II)V

    const-string v0, "foo"

    invoke-virtual {v10, v8}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {v10, v6}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide v4, 0x3e7ad7f29abcaf48L    # 1.0E-7

    invoke-static/range {v0 .. v5}, Ljunit/framework/Assert;->assertEquals(DDD)V

    invoke-virtual {v10}, Ljargs/gnu/CmdLineParser;->getRemainingArgs()[Ljava/lang/String;

    move-result-object v9

    array-length v0, v9

    invoke-static {v14, v0}, Ljunit/framework/Assert;->assertEquals(II)V

    const-string v0, "rest"

    aget-object v1, v9, v13

    invoke-static {v0, v1}, Ljunit/framework/Assert;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
