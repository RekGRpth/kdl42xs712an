.class public Ljargs/examples/gnu/CustomOptionTest;
.super Ljava/lang/Object;
.source "CustomOptionTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljargs/examples/gnu/CustomOptionTest$ShortDateOption;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 9
    .param p0    # [Ljava/lang/String;

    new-instance v5, Ljargs/gnu/CmdLineParser;

    invoke-direct {v5}, Ljargs/gnu/CmdLineParser;-><init>()V

    new-instance v6, Ljargs/examples/gnu/CustomOptionTest$ShortDateOption;

    const/16 v7, 0x64

    const-string v8, "date"

    invoke-direct {v6, v7, v8}, Ljargs/examples/gnu/CustomOptionTest$ShortDateOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {v5, v6}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v0

    :try_start_0
    invoke-virtual {v5, p0}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;)V
    :try_end_0
    .catch Ljargs/gnu/CmdLineParser$OptionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v5, v0}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Date;

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "date: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljargs/gnu/CmdLineParser;->getRemainingArgs()[Ljava/lang/String;

    move-result-object v4

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "remaining args: "

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v3, 0x0

    :goto_1
    array-length v6, v4

    if-lt v3, v6, :cond_0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    return-void

    :catch_0
    move-exception v2

    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Ljargs/examples/gnu/CustomOptionTest;->printUsage()V

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    :cond_0
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v7, v4, v3

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static printUsage()V
    .locals 2

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "usage: prog [{-d,--date} date]"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method
