.class Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;
.super Ljargs/gnu/CmdLineParser;
.source "OptionParserSubclassTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljargs/examples/gnu/OptionParserSubclassTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyOptionsParser"
.end annotation


# static fields
.field public static final FRACTION:Ljargs/gnu/CmdLineParser$Option;

.field public static final NAME:Ljargs/gnu/CmdLineParser$Option;

.field public static final SIZE:Ljargs/gnu/CmdLineParser$Option;

.field public static final VERBOSE:Ljargs/gnu/CmdLineParser$Option;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$BooleanOption;

    const/16 v1, 0x76

    const-string v2, "verbose"

    invoke-direct {v0, v1, v2}, Ljargs/gnu/CmdLineParser$Option$BooleanOption;-><init>(CLjava/lang/String;)V

    sput-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->VERBOSE:Ljargs/gnu/CmdLineParser$Option;

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$IntegerOption;

    const/16 v1, 0x73

    const-string v2, "size"

    invoke-direct {v0, v1, v2}, Ljargs/gnu/CmdLineParser$Option$IntegerOption;-><init>(CLjava/lang/String;)V

    sput-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->SIZE:Ljargs/gnu/CmdLineParser$Option;

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$StringOption;

    const/16 v1, 0x6e

    const-string v2, "name"

    invoke-direct {v0, v1, v2}, Ljargs/gnu/CmdLineParser$Option$StringOption;-><init>(CLjava/lang/String;)V

    sput-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->NAME:Ljargs/gnu/CmdLineParser$Option;

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$DoubleOption;

    const/16 v1, 0x66

    const-string v2, "fraction"

    invoke-direct {v0, v1, v2}, Ljargs/gnu/CmdLineParser$Option$DoubleOption;-><init>(CLjava/lang/String;)V

    sput-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->FRACTION:Ljargs/gnu/CmdLineParser$Option;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljargs/gnu/CmdLineParser;-><init>()V

    sget-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->VERBOSE:Ljargs/gnu/CmdLineParser$Option;

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    sget-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->SIZE:Ljargs/gnu/CmdLineParser$Option;

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    sget-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->NAME:Ljargs/gnu/CmdLineParser$Option;

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    sget-object v0, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->FRACTION:Ljargs/gnu/CmdLineParser$Option;

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    return-void
.end method
