.class public Ljargs/gnu/CmdLineParser$IllegalOptionValueException;
.super Ljargs/gnu/CmdLineParser$OptionException;
.source "CmdLineParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljargs/gnu/CmdLineParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IllegalOptionValueException"
.end annotation


# instance fields
.field private option:Ljargs/gnu/CmdLineParser$Option;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljargs/gnu/CmdLineParser$Option;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljargs/gnu/CmdLineParser$Option;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "illegal value \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\' for option -"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Ljargs/gnu/CmdLineParser$Option;->shortForm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "/--"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Ljargs/gnu/CmdLineParser$Option;->longForm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljargs/gnu/CmdLineParser$OptionException;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;->option:Ljargs/gnu/CmdLineParser$Option;

    iput-object p2, p0, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getOption()Ljargs/gnu/CmdLineParser$Option;
    .locals 1

    iget-object v0, p0, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;->option:Ljargs/gnu/CmdLineParser$Option;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;->value:Ljava/lang/String;

    return-object v0
.end method
