.class public Ljargs/gnu/CmdLineParser;
.super Ljava/lang/Object;
.source "CmdLineParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljargs/gnu/CmdLineParser$Option;,
        Ljargs/gnu/CmdLineParser$IllegalOptionValueException;,
        Ljargs/gnu/CmdLineParser$UnknownOptionException;,
        Ljargs/gnu/CmdLineParser$OptionException;
    }
.end annotation


# instance fields
.field private options:Ljava/util/Hashtable;

.field private remainingArgs:[Ljava/lang/String;

.field private values:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v1, 0xa

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Ljargs/gnu/CmdLineParser;->remainingArgs:[Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Ljargs/gnu/CmdLineParser;->options:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Ljargs/gnu/CmdLineParser;->values:Ljava/util/Hashtable;

    return-void
.end method


# virtual methods
.method public final addBooleanOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;
    .locals 1
    .param p1    # C
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$BooleanOption;

    invoke-direct {v0, p1, p2}, Ljargs/gnu/CmdLineParser$Option$BooleanOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    return-object v0
.end method

.method public final addDoubleOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;
    .locals 1
    .param p1    # C
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$DoubleOption;

    invoke-direct {v0, p1, p2}, Ljargs/gnu/CmdLineParser$Option$DoubleOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    return-object v0
.end method

.method public final addIntegerOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;
    .locals 1
    .param p1    # C
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$IntegerOption;

    invoke-direct {v0, p1, p2}, Ljargs/gnu/CmdLineParser$Option$IntegerOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    return-object v0
.end method

.method public final addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;
    .locals 3
    .param p1    # Ljargs/gnu/CmdLineParser$Option;

    iget-object v0, p0, Ljargs/gnu/CmdLineParser;->options:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljargs/gnu/CmdLineParser$Option;->shortForm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ljargs/gnu/CmdLineParser;->options:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "--"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljargs/gnu/CmdLineParser$Option;->longForm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1
.end method

.method public final addStringOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;
    .locals 1
    .param p1    # C
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljargs/gnu/CmdLineParser$Option$StringOption;

    invoke-direct {v0, p1, p2}, Ljargs/gnu/CmdLineParser$Option$StringOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    return-object v0
.end method

.method public final getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljargs/gnu/CmdLineParser$Option;

    iget-object v0, p0, Ljargs/gnu/CmdLineParser;->values:Ljava/util/Hashtable;

    invoke-virtual {p1}, Ljargs/gnu/CmdLineParser$Option;->longForm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getRemainingArgs()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljargs/gnu/CmdLineParser;->remainingArgs:[Ljava/lang/String;

    return-object v0
.end method

.method public final parse([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljargs/gnu/CmdLineParser$IllegalOptionValueException;,
            Ljargs/gnu/CmdLineParser$UnknownOptionException;
        }
    .end annotation

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;Ljava/util/Locale;)V

    return-void
.end method

.method public final parse([Ljava/lang/String;Ljava/util/Locale;)V
    .locals 11
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljargs/gnu/CmdLineParser$IllegalOptionValueException;,
            Ljargs/gnu/CmdLineParser$UnknownOptionException;
        }
    .end annotation

    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    const/4 v6, 0x0

    new-instance v9, Ljava/util/Hashtable;

    const/16 v10, 0xa

    invoke-direct {v9, v10}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v9, p0, Ljargs/gnu/CmdLineParser;->values:Ljava/util/Hashtable;

    :goto_0
    array-length v9, p1

    if-lt v6, v9, :cond_1

    :cond_0
    :goto_1
    array-length v9, p1

    if-lt v6, v9, :cond_7

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    iput-object v9, p0, Ljargs/gnu/CmdLineParser;->remainingArgs:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v9

    if-nez v9, :cond_8

    return-void

    :cond_1
    aget-object v0, p1, v6

    const-string v9, "-"

    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "--"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    const-string v9, "--"

    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "="

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v9, -0x1

    if-eq v2, v9, :cond_3

    add-int/lit8 v9, v2, 0x1

    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v0, v9, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v9, p0, Ljargs/gnu/CmdLineParser;->options:Ljava/util/Hashtable;

    invoke-virtual {v9, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljargs/gnu/CmdLineParser$Option;

    if-nez v4, :cond_4

    new-instance v9, Ljargs/gnu/CmdLineParser$UnknownOptionException;

    invoke-direct {v9, v0}, Ljargs/gnu/CmdLineParser$UnknownOptionException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_4
    const/4 v7, 0x0

    invoke-virtual {v4}, Ljargs/gnu/CmdLineParser$Option;->wantsValue()Z

    move-result v9

    if-eqz v9, :cond_6

    if-nez v8, :cond_5

    add-int/lit8 v6, v6, 0x1

    const/4 v8, 0x0

    array-length v9, p1

    if-ge v6, v9, :cond_5

    aget-object v8, p1, v6

    :cond_5
    invoke-virtual {v4, v8, p2}, Ljargs/gnu/CmdLineParser$Option;->getValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object v7

    :goto_3
    iget-object v9, p0, Ljargs/gnu/CmdLineParser;->values:Ljava/util/Hashtable;

    invoke-virtual {v4}, Ljargs/gnu/CmdLineParser$Option;->longForm()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_6
    const/4 v9, 0x0

    invoke-virtual {v4, v9, p2}, Ljargs/gnu/CmdLineParser$Option;->getValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object v7

    goto :goto_3

    :cond_7
    aget-object v9, p1, v6

    invoke-virtual {v5, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    :cond_8
    iget-object v10, p0, Ljargs/gnu/CmdLineParser;->remainingArgs:[Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2
.end method
