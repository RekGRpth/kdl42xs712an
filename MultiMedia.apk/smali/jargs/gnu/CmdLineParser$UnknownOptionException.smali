.class public Ljargs/gnu/CmdLineParser$UnknownOptionException;
.super Ljargs/gnu/CmdLineParser$OptionException;
.source "CmdLineParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljargs/gnu/CmdLineParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnknownOptionException"
.end annotation


# instance fields
.field private optionName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "unknown option \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljargs/gnu/CmdLineParser$OptionException;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Ljargs/gnu/CmdLineParser$UnknownOptionException;->optionName:Ljava/lang/String;

    iput-object p1, p0, Ljargs/gnu/CmdLineParser$UnknownOptionException;->optionName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getOptionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljargs/gnu/CmdLineParser$UnknownOptionException;->optionName:Ljava/lang/String;

    return-object v0
.end method
