.class Lantlr/AlternativeBlock;
.super Lantlr/AlternativeElement;
.source "AlternativeBlock.java"


# static fields
.field protected static nblks:I


# instance fields
.field protected ID:I

.field protected alternatives:Lantlr/collections/impl/Vector;

.field protected alti:I

.field protected altj:I

.field protected analysisAlt:I

.field protected doAutoGen:Z

.field protected generateAmbigWarnings:Z

.field greedy:Z

.field greedySet:Z

.field protected hasASynPred:Z

.field protected hasAnAction:Z

.field protected initAction:Ljava/lang/String;

.field protected label:Ljava/lang/String;

.field not:Z

.field protected warnWhenFollowAmbig:Z


# direct methods
.method public constructor <init>(Lantlr/Grammar;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lantlr/AlternativeElement;-><init>(Lantlr/Grammar;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->hasAnAction:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->hasASynPred:Z

    iput v2, p0, Lantlr/AlternativeBlock;->ID:I

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->not:Z

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->greedy:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->greedySet:Z

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->doAutoGen:Z

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->warnWhenFollowAmbig:Z

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->generateAmbigWarnings:Z

    new-instance v0, Lantlr/collections/impl/Vector;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lantlr/collections/impl/Vector;-><init>(I)V

    iput-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->not:Z

    sget v0, Lantlr/AlternativeBlock;->nblks:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lantlr/AlternativeBlock;->nblks:I

    sget v0, Lantlr/AlternativeBlock;->nblks:I

    iput v0, p0, Lantlr/AlternativeBlock;->ID:I

    return-void
.end method

.method public constructor <init>(Lantlr/Grammar;Lantlr/Token;Z)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lantlr/AlternativeElement;-><init>(Lantlr/Grammar;Lantlr/Token;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->hasAnAction:Z

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->hasASynPred:Z

    iput v1, p0, Lantlr/AlternativeBlock;->ID:I

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->not:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->greedy:Z

    iput-boolean v1, p0, Lantlr/AlternativeBlock;->greedySet:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->doAutoGen:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->warnWhenFollowAmbig:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->generateAmbigWarnings:Z

    new-instance v0, Lantlr/collections/impl/Vector;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lantlr/collections/impl/Vector;-><init>(I)V

    iput-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    iput-boolean p3, p0, Lantlr/AlternativeBlock;->not:Z

    sget v0, Lantlr/AlternativeBlock;->nblks:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lantlr/AlternativeBlock;->nblks:I

    sget v0, Lantlr/AlternativeBlock;->nblks:I

    iput v0, p0, Lantlr/AlternativeBlock;->ID:I

    return-void
.end method


# virtual methods
.method public addAlternative(Lantlr/Alternative;)V
    .locals 1

    iget-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, p1}, Lantlr/collections/impl/Vector;->appendElement(Ljava/lang/Object;)V

    return-void
.end method

.method public generate()V
    .locals 1

    iget-object v0, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->generator:Lantlr/CodeGenerator;

    invoke-virtual {v0, p0}, Lantlr/CodeGenerator;->gen(Lantlr/AlternativeBlock;)V

    return-void
.end method

.method public getAlternativeAt(I)Lantlr/Alternative;
    .locals 1

    iget-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, p1}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Alternative;

    return-object v0
.end method

.method public getAlternatives()Lantlr/collections/impl/Vector;
    .locals 1

    iget-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    return-object v0
.end method

.method public getAutoGen()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/AlternativeBlock;->doAutoGen:Z

    return v0
.end method

.method public getInitAction()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/AlternativeBlock;->label:Ljava/lang/String;

    return-object v0
.end method

.method public look(I)Lantlr/Lookahead;
    .locals 1

    iget-object v0, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, p1, p0}, Lantlr/LLkGrammarAnalyzer;->look(ILantlr/AlternativeBlock;)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method

.method public prepareForAnalysis()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, v1}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Alternative;

    iget-object v2, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget v2, v2, Lantlr/Grammar;->maxk:I

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Lantlr/Lookahead;

    iput-object v2, v0, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    const/4 v2, -0x1

    iput v2, v0, Lantlr/Alternative;->lookaheadDepth:I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeTrackingOfRuleRefs(Lantlr/Grammar;)V
    .locals 6

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    invoke-virtual {p0, v3}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v0

    iget-object v1, v0, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    :goto_1
    if-eqz v1, :cond_3

    instance-of v0, v1, Lantlr/RuleRefElement;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lantlr/RuleRefElement;

    iget-object v2, v0, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v2

    check-cast v2, Lantlr/RuleSymbol;

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "rule "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v0, v0, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v4, " referenced in (...)=>, but not defined"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lantlr/Tool;->error(Ljava/lang/String;)V

    :cond_0
    :goto_2
    iget-object v1, v1, Lantlr/AlternativeElement;->next:Lantlr/AlternativeElement;

    goto :goto_1

    :cond_1
    iget-object v2, v2, Lantlr/RuleSymbol;->references:Lantlr/collections/impl/Vector;

    invoke-virtual {v2, v0}, Lantlr/collections/impl/Vector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    instance-of v0, v1, Lantlr/AlternativeBlock;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lantlr/AlternativeBlock;

    invoke-virtual {v0, p1}, Lantlr/AlternativeBlock;->removeTrackingOfRuleRefs(Lantlr/Grammar;)V

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_4
    return-void
.end method

.method public setAlternatives(Lantlr/collections/impl/Vector;)V
    .locals 0

    iput-object p1, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    return-void
.end method

.method public setAutoGen(Z)V
    .locals 0

    iput-boolean p1, p0, Lantlr/AlternativeBlock;->doAutoGen:Z

    return-void
.end method

.method public setInitAction(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/AlternativeBlock;->label:Ljava/lang/String;

    return-void
.end method

.method public setOption(Lantlr/Token;Lantlr/Token;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "warnWhenFollowAmbig"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->warnWhenFollowAmbig:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lantlr/AlternativeBlock;->warnWhenFollowAmbig:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "Value for warnWhenFollowAmbig must be true or false"

    iget-object v2, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "generateAmbigWarnings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->generateAmbigWarnings:Z

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v3, p0, Lantlr/AlternativeBlock;->generateAmbigWarnings:Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "Value for generateAmbigWarnings must be true or false"

    iget-object v2, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "greedy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->greedy:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->greedySet:Z

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v3, p0, Lantlr/AlternativeBlock;->greedy:Z

    iput-boolean v2, p0, Lantlr/AlternativeBlock;->greedySet:Z

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "Value for greedy must be true or false"

    iget-object v2, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Invalid subrule option: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    const-string v0, " ("

    iget-object v1, p0, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/4 v1, 0x0

    move v9, v1

    move-object v1, v0

    move v0, v9

    :goto_0
    iget-object v2, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v2}, Lantlr/collections/impl/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_8

    invoke-virtual {p0, v0}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v3

    iget-object v4, v3, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    iget v5, v3, Lantlr/Alternative;->lookaheadDepth:I

    const/4 v2, -0x1

    if-ne v5, v2, :cond_2

    :goto_1
    iget-object v2, v3, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    iget-object v3, v3, Lantlr/Alternative;->semPred:Ljava/lang/String;

    if-eqz v3, :cond_1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    :goto_2
    if-eqz v2, :cond_6

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v2, Lantlr/AlternativeElement;->next:Lantlr/AlternativeElement;

    move-object v2, v1

    move-object v1, v3

    goto :goto_2

    :cond_2
    const v2, 0x7fffffff

    if-ne v5, v2, :cond_3

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "{?}:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " {"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x1

    move v9, v1

    move-object v1, v2

    move v2, v9

    :goto_3
    if-gt v2, v5, :cond_5

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    aget-object v6, v4, v2

    const-string v7, ","

    iget-object v8, p0, Lantlr/AlternativeBlock;->grammar:Lantlr/Grammar;

    iget-object v8, v8, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v8}, Lantlr/TokenManager;->getVocabulary()Lantlr/collections/impl/Vector;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/collections/impl/Vector;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    if-ge v2, v5, :cond_4

    add-int/lit8 v6, v2, 0x1

    aget-object v6, v4, v6

    if-eqz v6, :cond_4

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v6, ";"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "}:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_6
    iget-object v2, p0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v2}, Lantlr/collections/impl/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " |"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_8
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
