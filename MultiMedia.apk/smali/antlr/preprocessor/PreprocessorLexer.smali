.class public Lantlr/preprocessor/PreprocessorLexer;
.super Lantlr/CharScanner;
.source "PreprocessorLexer.java"

# interfaces
.implements Lantlr/TokenStream;
.implements Lantlr/preprocessor/PreprocessorTokenTypes;


# static fields
.field public static final _tokenSet_0:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_1:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_10:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_2:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_3:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_4:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_5:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_6:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_7:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_8:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_9:Lantlr/collections/impl/BitSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_0()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_1()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_2()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_3()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_4()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_4:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_5()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_6()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_7()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_7:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_8()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_8:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_9()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_9:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/PreprocessorLexer;->mk_tokenSet_10()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_10:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public constructor <init>(Lantlr/InputBuffer;)V
    .locals 1

    new-instance v0, Lantlr/LexerSharedInputState;

    invoke-direct {v0, p1}, Lantlr/LexerSharedInputState;-><init>(Lantlr/InputBuffer;)V

    invoke-direct {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;-><init>(Lantlr/LexerSharedInputState;)V

    return-void
.end method

.method public constructor <init>(Lantlr/LexerSharedInputState;)V
    .locals 4

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Lantlr/CharScanner;-><init>(Lantlr/LexerSharedInputState;)V

    iput-boolean v0, p0, Lantlr/preprocessor/PreprocessorLexer;->caseSensitiveLiterals:Z

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->setCaseSensitive(Z)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "public"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "class"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "throws"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "catch"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "private"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "extends"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "protected"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "returns"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "tokens"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->literals:Ljava/util/Hashtable;

    new-instance v1, Lantlr/ANTLRHashString;

    const-string v2, "exception"

    invoke-direct {v1, v2, p0}, Lantlr/ANTLRHashString;-><init>(Ljava/lang/String;Lantlr/CharScanner;)V

    new-instance v2, Ljava/lang/Integer;

    const/16 v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    new-instance v0, Lantlr/ByteBuffer;

    invoke-direct {v0, p1}, Lantlr/ByteBuffer;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;-><init>(Lantlr/InputBuffer;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1

    new-instance v0, Lantlr/CharBuffer;

    invoke-direct {v0, p1}, Lantlr/CharBuffer;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;-><init>(Lantlr/InputBuffer;)V

    return-void
.end method

.method private static final mk_tokenSet_0()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x800000000000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_1()[J
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x100002600L    # 2.122000597E-314
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_10()[J
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x800100002600L
        0x800000000000000L
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_2()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x20000000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_3()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x800020000000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_4()[J
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x100002600L    # 2.122000597E-314
        0x1000000000000000L
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_5()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x800838400002408L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    const-wide v2, -0x800000000000001L

    aput-wide v2, v1, v0

    const/4 v0, 0x2

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_6()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x8000000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_7()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x400000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    const-wide/32 v2, -0x10000001

    aput-wide v2, v1, v0

    const/4 v0, 0x2

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_8()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x8000000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    const-wide/32 v2, -0x10000001

    aput-wide v2, v1, v0

    const/4 v0, 0x2

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_9()[J
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x800500002600L
        0x800000000000000L
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final mACTION(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0xff

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/4 v2, 0x7

    const/16 v3, 0x7b

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    :goto_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x7d

    if-ne v3, v4, :cond_2

    :cond_0
    const/16 v3, 0x7d

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_2
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_4

    :cond_3
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_4

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_4

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x7b

    if-ne v3, v4, :cond_5

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_5

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_5

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mACTION(Z)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x27

    if-ne v3, v4, :cond_6

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCHAR_LITERAL(Z)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_8

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_7

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_8

    :cond_7
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMENT(Z)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x22

    if-ne v3, v4, :cond_9

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_9

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_9

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mSTRING_LITERAL(Z)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_0

    const v3, 0xffff

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto/16 :goto_0
.end method

.method protected final mALT(Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v5, 0x2

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x1b

    :goto_0
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xff

    if-gt v3, v4, :cond_0

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->mELEMENT(Z)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mARG_ACTION(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v9, 0x0

    const/16 v8, 0xff

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x14

    const/16 v3, 0x5b

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    :goto_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x5d

    if-ne v3, v4, :cond_2

    :cond_0
    const/16 v3, 0x5d

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_2
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x5b

    if-ne v3, v4, :cond_3

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_3

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_3

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->mARG_ACTION(Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_4

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_5

    :cond_4
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_5

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_5

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x27

    if-ne v3, v4, :cond_6

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->mCHAR_LITERAL(Z)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x22

    if-ne v3, v4, :cond_7

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_7

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_7

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->mSTRING_LITERAL(Z)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    const v3, 0xffff

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto/16 :goto_0
.end method

.method public final mASSIGN_RHS(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v9, 0x0

    const/16 v8, 0xff

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0xe

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v3

    const/16 v4, 0x3d

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v3}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :goto_0
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x3b

    if-ne v3, v4, :cond_2

    :cond_0
    const/16 v3, 0x3b

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_2
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x22

    if-ne v3, v4, :cond_3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_3

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->mSTRING_LITERAL(Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x27

    if-ne v3, v4, :cond_4

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->mCHAR_LITERAL(Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_5

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_6

    :cond_5
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_6

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_6

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    const v3, 0xffff

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto/16 :goto_0
.end method

.method public final mBANG(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x13

    const/16 v3, 0x21

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mCHAR_LITERAL(Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v6, 0x27

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x26

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_1

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->mESC(Z)V

    :goto_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_1
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_8:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto :goto_0

    :cond_2
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0
.end method

.method public final mCOMMA(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x18

    const/16 v3, 0x2c

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mCOMMENT(Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v2, 0x2f

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mSL_COMMENT(Z)V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_1
    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    if-ne v1, v2, :cond_2

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    const/16 v2, 0x2a

    if-ne v1, v2, :cond_2

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mML_COMMENT(Z)V

    goto :goto_0

    :cond_2
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0
.end method

.method protected final mCURLY_BLOCK_SCARF(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0xff

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x20

    const/16 v3, 0x7b

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    :goto_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x7d

    if-ne v3, v4, :cond_2

    :cond_0
    const/16 v3, 0x7d

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_2
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_4

    :cond_3
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_4

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_4

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x22

    if-ne v3, v4, :cond_5

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_5

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_5

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mSTRING_LITERAL(Z)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x27

    if-ne v3, v4, :cond_6

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCHAR_LITERAL(Z)V

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_8

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_7

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_8

    :cond_7
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMENT(Z)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_0

    const v3, 0xffff

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto/16 :goto_0
.end method

.method protected final mDIGIT(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x29

    const/16 v3, 0x30

    const/16 v4, 0x39

    invoke-virtual {p0, v3, v4}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method protected final mELEMENT(Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x1c

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(Lantlr/collections/impl/BitSet;)V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_0
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMENT(Z)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mACTION(Z)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mSTRING_LITERAL(Z)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mCHAR_LITERAL(Z)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mSUBRULE_BLOCK(Z)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    goto :goto_0

    :cond_1
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x22 -> :sswitch_2
        0x27 -> :sswitch_3
        0x28 -> :sswitch_4
        0x2f -> :sswitch_0
        0x7b -> :sswitch_1
    .end sparse-switch
.end method

.method protected final mESC(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0xff

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x28

    const/16 v3, 0x5c

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    const/16 v3, 0x6e

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_1
    const/16 v3, 0x72

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_2
    const/16 v3, 0x74

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_3
    const/16 v3, 0x62

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_4
    const/16 v3, 0x66

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_5
    const/16 v3, 0x77

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_6
    const/16 v3, 0x61

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_7
    const/16 v3, 0x22

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_8
    const/16 v3, 0x27

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_9
    const/16 v3, 0x5c

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :sswitch_a
    const/16 v3, 0x30

    const/16 v4, 0x33

    invoke-virtual {p0, v3, v4}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_4

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_4

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_4

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_4

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mDIGIT(Z)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_2

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_2

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_2

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_2

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mDIGIT(Z)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_3

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-le v3, v9, :cond_0

    :cond_3
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_4
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_5

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-le v3, v9, :cond_0

    :cond_5
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_b
    const/16 v3, 0x34

    const/16 v4, 0x37

    invoke-virtual {p0, v3, v4}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_6

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_6

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_6

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_6

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mDIGIT(Z)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_7

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-le v3, v9, :cond_0

    :cond_7
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_c
    const/16 v3, 0x75

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mXDIGIT(Z)V

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mXDIGIT(Z)V

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mXDIGIT(Z)V

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mXDIGIT(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_7
        0x27 -> :sswitch_8
        0x30 -> :sswitch_a
        0x31 -> :sswitch_a
        0x32 -> :sswitch_a
        0x33 -> :sswitch_a
        0x34 -> :sswitch_b
        0x35 -> :sswitch_b
        0x36 -> :sswitch_b
        0x37 -> :sswitch_b
        0x5c -> :sswitch_9
        0x61 -> :sswitch_6
        0x62 -> :sswitch_3
        0x66 -> :sswitch_4
        0x6e -> :sswitch_0
        0x72 -> :sswitch_1
        0x74 -> :sswitch_2
        0x75 -> :sswitch_c
        0x77 -> :sswitch_5
    .end sparse-switch
.end method

.method protected final mID(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0x61

    const/16 v8, 0x5f

    const/16 v7, 0x5a

    const/16 v6, 0x41

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x9

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :pswitch_1
    const/16 v3, 0x7a

    invoke-virtual {p0, v9, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    :goto_0
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    packed-switch v3, :pswitch_data_1

    :pswitch_2
    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v4

    iget-object v5, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v5

    sub-int/2addr v5, v1

    invoke-direct {v3, v4, v1, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v3, v2}, Lantlr/preprocessor/PreprocessorLexer;->testLiteralsTable(Ljava/lang/String;I)I

    move-result v2

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :pswitch_3
    invoke-virtual {p0, v6, v7}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :pswitch_5
    const/16 v3, 0x7a

    invoke-virtual {p0, v9, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, v6, v7}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_0

    :pswitch_8
    const/16 v3, 0x30

    const/16 v4, 0x39

    invoke-virtual {p0, v3, v4}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x30
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_7
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final mID_OR_KEYWORD(Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v10, 0xff

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v2

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mID(Z)V

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    invoke-virtual {v3}, Lantlr/Token;->getType()I

    move-result v1

    sget-object v4, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_9:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v5

    invoke-virtual {v4, v5}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    if-lt v4, v9, :cond_5

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    if-gt v4, v10, :cond_5

    invoke-virtual {v3}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v4

    const-string v5, "header"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v1, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    invoke-virtual {v1, v3}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_9:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    invoke-virtual {v1, v3}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    :cond_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_1
    sget-object v1, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_9:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    invoke-virtual {v1, v3}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    if-lt v1, v9, :cond_2

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    if-le v1, v10, :cond_0

    :cond_2
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mSTRING_LITERAL(Z)V

    :goto_0
    :sswitch_1
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_1

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mACTION(Z)V

    const/4 v1, 0x5

    :cond_3
    :goto_1
    if-eqz p1, :cond_4

    if-nez v0, :cond_4

    const/4 v3, -0x1

    if-eq v1, v3, :cond_4

    invoke-virtual {p0, v1}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v2

    invoke-direct {v1, v3, v2, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_4
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_2
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMENT(Z)V

    goto :goto_0

    :cond_5
    sget-object v4, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_10:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v5

    invoke-virtual {v4, v5}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    if-lt v4, v9, :cond_6

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    if-gt v4, v10, :cond_6

    invoke-virtual {v3}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v4

    const-string v5, "tokens"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    :goto_2
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_2

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCURLY_BLOCK_SCARF(Z)V

    const/16 v1, 0xc

    goto :goto_1

    :sswitch_4
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    goto :goto_2

    :sswitch_5
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMENT(Z)V

    goto :goto_2

    :cond_6
    sget-object v4, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_10:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v5

    invoke-virtual {v4, v5}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    const-string v4, "options"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_3
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_3

    const/16 v1, 0x7b

    invoke-virtual {p0, v1}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    const/16 v1, 0xd

    goto/16 :goto_1

    :sswitch_6
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    goto :goto_3

    :sswitch_7
    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMENT(Z)V

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x22 -> :sswitch_0
        0x2f -> :sswitch_1
        0x7b -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x2f -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x2f -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x2f -> :sswitch_7
    .end sparse-switch
.end method

.method public final mLPAREN(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x1d

    const/16 v3, 0x28

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method protected final mML_COMMENT(Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v8, 0xff

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x25

    const-string v3, "/*"

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2a

    if-ne v3, v4, :cond_2

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x2f

    if-ne v3, v4, :cond_2

    :cond_0
    const-string v3, "*/"

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_2
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_3

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_4

    :cond_3
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_4

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_4

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    const v3, 0xffff

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto :goto_0
.end method

.method protected final mNEWLINE(Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v6, 0xd

    const/16 v5, 0xa

    const/4 v4, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x22

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v6, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v5, :cond_1

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->newline()V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_1
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v6, :cond_2

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->newline()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v5, :cond_3

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->newline()V

    goto :goto_0

    :cond_3
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0
.end method

.method public final mRCURLY(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0xf

    const/16 v3, 0x7d

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mRPAREN(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x1e

    const/16 v3, 0x29

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mRULE_BLOCK(Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v8, 0x7c

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x16

    const/16 v3, 0x3a

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v3}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :cond_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mALT(Z)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_1
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v3}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :goto_0
    :sswitch_1
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v8, :cond_4

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v3}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :cond_2
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mALT(Z)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_3
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_2
    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v3}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto :goto_0

    :cond_4
    const/16 v3, 0x3b

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_5

    if-nez v0, :cond_5

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_5
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x3b -> :sswitch_1
        0x7c -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x3b -> :sswitch_1
        0x7c -> :sswitch_1
    .end sparse-switch
.end method

.method public final mSEMI(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0xb

    const/16 v3, 0x3b

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method protected final mSL_COMMENT(Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v8, 0xff

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x24

    const-string v3, "//"

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_2

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_2
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-lt v3, v7, :cond_0

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-gt v3, v8, :cond_0

    const v3, 0xffff

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto :goto_0
.end method

.method public final mSTRING_LITERAL(Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v6, 0x1

    const/16 v5, 0x22

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x27

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    :goto_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_0

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->mESC(Z)V

    goto :goto_0

    :cond_0
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_7:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->matchNot(C)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    if-eqz p1, :cond_2

    if-nez v0, :cond_2

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_2
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mSUBRULE_BLOCK(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0x2b

    const/16 v8, 0x2a

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/4 v2, 0x6

    const/16 v3, 0x28

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    :cond_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mALT(Z)V

    :goto_0
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_4:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_1
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    :sswitch_1
    const/16 v3, 0x7c

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    :cond_2
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mALT(Z)V

    goto :goto_0

    :cond_3
    sget-object v3, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_4
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_2
    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    :sswitch_3
    const/16 v3, 0x29

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x3d

    if-ne v3, v4, :cond_7

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x3e

    if-ne v3, v4, :cond_7

    const-string v3, "=>"

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(Ljava/lang/String;)V

    :cond_5
    :goto_1
    if-eqz p1, :cond_6

    if-nez v0, :cond_6

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_6
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_7
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v8, :cond_8

    invoke-virtual {p0, v8}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_1

    :cond_8
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v9, :cond_9

    invoke-virtual {p0, v9}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_1

    :cond_9
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x3f

    if-ne v3, v4, :cond_5

    const/16 v3, 0x3f

    invoke-virtual {p0, v3}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x7c -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x29 -> :sswitch_3
    .end sparse-switch
.end method

.method public final mWS(Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v7, 0x20

    const/16 v6, 0x9

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v0}, Lantlr/ANTLRStringBuffer;->length()I

    move v0, v1

    :goto_0
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v7, :cond_0

    invoke-virtual {p0, v7}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    if-ne v3, v6, :cond_1

    invoke-virtual {p0, v6}, Lantlr/preprocessor/PreprocessorLexer;->match(C)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_2

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_3

    :cond_2
    invoke-virtual {p0, v1}, Lantlr/preprocessor/PreprocessorLexer;->mNEWLINE(Z)V

    goto :goto_1

    :cond_3
    if-lt v0, v5, :cond_5

    if-eqz p1, :cond_4

    if-nez v2, :cond_4

    :cond_4
    move-object v0, v2

    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_5
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0
.end method

.method protected final mXDIGIT(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x2a

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v4}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    const/16 v3, 0x30

    const/16 v4, 0x39

    invoke-virtual {p0, v3, v4}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/preprocessor/PreprocessorLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_1
    const/16 v3, 0x61

    const/16 v4, 0x66

    invoke-virtual {p0, v3, v4}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    goto :goto_0

    :sswitch_2
    const/16 v3, 0x41

    const/16 v4, 0x46

    invoke-virtual {p0, v3, v4}, Lantlr/preprocessor/PreprocessorLexer;->matchRange(CC)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
        0x37 -> :sswitch_0
        0x38 -> :sswitch_0
        0x39 -> :sswitch_0
        0x41 -> :sswitch_2
        0x42 -> :sswitch_2
        0x43 -> :sswitch_2
        0x44 -> :sswitch_2
        0x45 -> :sswitch_2
        0x46 -> :sswitch_2
        0x61 -> :sswitch_1
        0x62 -> :sswitch_1
        0x63 -> :sswitch_1
        0x64 -> :sswitch_1
        0x65 -> :sswitch_1
        0x66 -> :sswitch_1
    .end sparse-switch
.end method

.method public nextToken()Lantlr/Token;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v2, 0x28

    :cond_0
    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->resetText()V

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v0

    if-ne v0, v2, :cond_1

    sget-object v0, Lantlr/preprocessor/PreprocessorLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mSUBRULE_BLOCK(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    :goto_0
    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->testLiteralsTable(I)I

    move-result v0

    iget-object v1, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    invoke-virtual {v1, v0}, Lantlr/Token;->setType(I)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    return-object v0

    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mRULE_BLOCK(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mWS(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMENT(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mACTION(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mSTRING_LITERAL(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mCHAR_LITERAL(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mBANG(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_8
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mSEMI(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_9
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mCOMMA(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_a
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mRCURLY(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_b
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mRPAREN(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_c
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mID_OR_KEYWORD(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_d
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mASSIGN_RHS(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_e
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mARG_ACTION(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v0

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->mLPAREN(Z)V

    iget-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v0

    const v1, 0xffff

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->uponEOF()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/PreprocessorLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    iput-object v0, p0, Lantlr/preprocessor/PreprocessorLexer;->_returnToken:Lantlr/Token;
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lantlr/CharStreamException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Lantlr/TokenStreamRecognitionException;

    invoke-direct {v1, v0}, Lantlr/TokenStreamRecognitionException;-><init>(Lantlr/RecognitionException;)V

    throw v1
    :try_end_1
    .catch Lantlr/CharStreamException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    instance-of v1, v0, Lantlr/CharStreamIOException;

    if-eqz v1, :cond_4

    new-instance v1, Lantlr/TokenStreamIOException;

    check-cast v0, Lantlr/CharStreamIOException;

    iget-object v0, v0, Lantlr/CharStreamIOException;->io:Ljava/io/IOException;

    invoke-direct {v1, v0}, Lantlr/TokenStreamIOException;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_3
    :try_start_2
    new-instance v0, Lantlr/NoViableAltForCharException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/PreprocessorLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/preprocessor/PreprocessorLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0
    :try_end_2
    .catch Lantlr/RecognitionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lantlr/CharStreamException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    new-instance v1, Lantlr/TokenStreamException;

    invoke-virtual {v0}, Lantlr/CharStreamException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lantlr/TokenStreamException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_8
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_4
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method
