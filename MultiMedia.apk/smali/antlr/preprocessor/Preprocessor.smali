.class public Lantlr/preprocessor/Preprocessor;
.super Lantlr/LLkParser;
.source "Preprocessor.java"

# interfaces
.implements Lantlr/preprocessor/PreprocessorTokenTypes;


# static fields
.field public static final _tokenNames:[Ljava/lang/String;

.field public static final _tokenSet_0:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_1:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_2:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_3:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_4:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_5:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_6:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_7:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_8:Lantlr/collections/impl/BitSet;


# instance fields
.field private antlrTool:Lantlr/Tool;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x2b

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "<0>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "EOF"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "<2>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "NULL_TREE_LOOKAHEAD"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "\"tokens\""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "HEADER_ACTION"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SUBRULE_BLOCK"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ACTION"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\"class\""

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ID"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\"extends\""

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SEMI"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TOKENS_SPEC"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "OPTIONS_START"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ASSIGN_RHS"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "RCURLY"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "\"protected\""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "\"private\""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "\"public\""

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "BANG"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ARG_ACTION"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "\"returns\""

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "RULE_BLOCK"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "\"throws\""

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "COMMA"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "\"exception\""

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "\"catch\""

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "ALT"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "ELEMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "LPAREN"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "RPAREN"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "ID_OR_KEYWORD"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "CURLY_BLOCK_SCARF"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "WS"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "NEWLINE"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "SL_COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "ML_COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "CHAR_LITERAL"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "STRING_LITERAL"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "ESC"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "DIGIT"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "XDIGIT"

    aput-object v2, v0, v1

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenNames:[Ljava/lang/String;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_0()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_1()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_2()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_3()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_4()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_4:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_5()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_6()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_7()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_7:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/preprocessor/Preprocessor;->mk_tokenSet_8()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_8:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public constructor <init>(Lantlr/ParserSharedInputState;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lantlr/LLkParser;-><init>(Lantlr/ParserSharedInputState;I)V

    sget-object v0, Lantlr/preprocessor/Preprocessor;->_tokenNames:[Ljava/lang/String;

    iput-object v0, p0, Lantlr/preprocessor/Preprocessor;->tokenNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lantlr/TokenBuffer;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lantlr/preprocessor/Preprocessor;-><init>(Lantlr/TokenBuffer;I)V

    return-void
.end method

.method protected constructor <init>(Lantlr/TokenBuffer;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenBuffer;I)V

    sget-object v0, Lantlr/preprocessor/Preprocessor;->_tokenNames:[Ljava/lang/String;

    iput-object v0, p0, Lantlr/preprocessor/Preprocessor;->tokenNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lantlr/TokenStream;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lantlr/preprocessor/Preprocessor;-><init>(Lantlr/TokenStream;I)V

    return-void
.end method

.method protected constructor <init>(Lantlr/TokenStream;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenStream;I)V

    sget-object v0, Lantlr/preprocessor/Preprocessor;->_tokenNames:[Ljava/lang/String;

    iput-object v0, p0, Lantlr/preprocessor/Preprocessor;->tokenNames:[Ljava/lang/String;

    return-void
.end method

.method private static final mk_tokenSet_0()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x2
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_1()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x471382
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_2()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x70200
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_3()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x182
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_4()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x800
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_5()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x70382
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_6()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x402080
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_7()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x2070382
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_8()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x6070382
        0x0
    .end array-data
.end method


# virtual methods
.method public final class_def(Ljava/lang/String;Lantlr/preprocessor/Hierarchy;)Lantlr/preprocessor/Grammar;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v9, 0x1

    new-instance v5, Lantlr/collections/impl/IndexedVector;

    const/16 v0, 0x64

    invoke-direct {v5, v0}, Lantlr/collections/impl/IndexedVector;-><init>(I)V

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    :goto_0
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v1, Lantlr/preprocessor/Preprocessor;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    :cond_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v2, 0x7

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    move-object v4, v0

    :goto_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v6

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v7

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move-object v4, v1

    goto :goto_1

    :sswitch_0
    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->superClass()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    :goto_2
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    invoke-virtual {v6}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lantlr/preprocessor/Hierarchy;->getGrammar(Ljava/lang/String;)Lantlr/preprocessor/Grammar;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Lantlr/SemanticException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "redefinition of grammar "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v6}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {v6}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-direct {v0, v2, p1, v3, v4}, Lantlr/SemanticException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    throw v0
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_0

    :sswitch_1
    move-object v3, v1

    goto :goto_2

    :cond_1
    :try_start_2
    new-instance v0, Lantlr/preprocessor/Grammar;

    invoke-virtual {p2}, Lantlr/preprocessor/Hierarchy;->getTool()Lantlr/Tool;

    move-result-object v8

    invoke-virtual {v6}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v8, v6, v7, v5}, Lantlr/preprocessor/Grammar;-><init>(Lantlr/Tool;Ljava/lang/String;Ljava/lang/String;Lantlr/collections/impl/IndexedVector;)V
    :try_end_2
    .catch Lantlr/RecognitionException; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    iput-object v3, v0, Lantlr/preprocessor/Grammar;->superClass:Ljava/lang/String;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lantlr/preprocessor/Grammar;->setPreambleAction(Ljava/lang/String;)V

    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :pswitch_2
    new-instance v1, Lantlr/NoViableAltException;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v1

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->optionSpec(Lantlr/preprocessor/Grammar;)Lantlr/collections/impl/IndexedVector;

    move-result-object v1

    :pswitch_4
    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Lantlr/preprocessor/Grammar;->setOptions(Lantlr/collections/impl/IndexedVector;)V

    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    packed-switch v1, :pswitch_data_2

    :pswitch_5
    new-instance v1, Lantlr/NoViableAltException;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v1

    :pswitch_6
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/preprocessor/Grammar;->setTokenSection(Ljava/lang/String;)V

    :pswitch_7
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    new-instance v1, Lantlr/NoViableAltException;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v1

    :sswitch_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/preprocessor/Grammar;->setMemberAction(Ljava/lang/String;)V

    :sswitch_3
    const/4 v1, 0x0

    :goto_3
    sget-object v2, Lantlr/preprocessor/Preprocessor;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->rule(Lantlr/preprocessor/Grammar;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    if-ge v1, v9, :cond_0

    new-instance v1, Lantlr/NoViableAltException;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Lantlr/RecognitionException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xb -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x7
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7
        :pswitch_7
        :pswitch_5
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x7 -> :sswitch_2
        0x9 -> :sswitch_3
        0x10 -> :sswitch_3
        0x11 -> :sswitch_3
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public final exceptionGroup()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const-string v0, ""

    move-object v1, v0

    :goto_0
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v0

    const/16 v2, 0x19

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->exceptionSpec()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    :cond_0
    return-object v1
.end method

.method public final exceptionHandler()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    const/16 v1, 0x1a

    :try_start_0
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {p0, v3}, Lantlr/preprocessor/Preprocessor;->match(I)V

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "catch "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v1, Lantlr/preprocessor/Preprocessor;->_tokenSet_8:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    goto :goto_0
.end method

.method public final exceptionSpec()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "exception "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v0, 0x19

    :try_start_0
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    :goto_0
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v1, Lantlr/preprocessor/Preprocessor;->_tokenSet_7:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    :cond_0
    return-object v0

    :sswitch_0
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x14

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    :goto_1
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    const/16 v2, 0x1a

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->exceptionHandler()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_2
    .catch Lantlr/RecognitionException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_1

    :sswitch_1
    move-object v0, v1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
        0x9 -> :sswitch_1
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
        0x12 -> :sswitch_1
        0x14 -> :sswitch_0
        0x19 -> :sswitch_1
        0x1a -> :sswitch_1
    .end sparse-switch
.end method

.method protected getTool()Lantlr/Tool;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Preprocessor;->antlrTool:Lantlr/Tool;

    return-object v0
.end method

.method public final grammarFile(Lantlr/preprocessor/Hierarchy;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v3, 0x5

    :goto_0
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    invoke-virtual {p1, p2}, Lantlr/preprocessor/Hierarchy;->getFile(Ljava/lang/String;)Lantlr/preprocessor/GrammarFile;

    move-result-object v2

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lantlr/preprocessor/GrammarFile;->addHeaderAction(Ljava/lang/String;)V
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->optionSpec(Lantlr/preprocessor/Grammar;)Lantlr/collections/impl/IndexedVector;

    move-result-object v0

    :cond_1
    :goto_2
    :sswitch_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_2

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    :cond_2
    invoke-virtual {p0, p2, p1}, Lantlr/preprocessor/Preprocessor;->class_def(Ljava/lang/String;Lantlr/preprocessor/Hierarchy;)Lantlr/preprocessor/Grammar;

    move-result-object v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {p1, p2}, Lantlr/preprocessor/Hierarchy;->getFile(Ljava/lang/String;)Lantlr/preprocessor/GrammarFile;

    move-result-object v2

    invoke-virtual {v2, v0}, Lantlr/preprocessor/GrammarFile;->setOptions(Lantlr/collections/impl/IndexedVector;)V

    :cond_3
    if-eqz v1, :cond_1

    invoke-virtual {v1, p2}, Lantlr/preprocessor/Grammar;->setFileName(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lantlr/preprocessor/Hierarchy;->addGrammar(Lantlr/preprocessor/Grammar;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
        0xd -> :sswitch_0
    .end sparse-switch
.end method

.method public final optionSpec(Lantlr/preprocessor/Grammar;)Lantlr/collections/impl/IndexedVector;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v6, 0x9

    new-instance v1, Lantlr/collections/impl/IndexedVector;

    invoke-direct {v1}, Lantlr/collections/impl/IndexedVector;-><init>()V

    const/16 v0, 0xd

    :try_start_0
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v0

    if-ne v0, v6, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v3, 0xe

    invoke-virtual {p0, v3}, Lantlr/preprocessor/Preprocessor;->match(I)V

    new-instance v3, Lantlr/preprocessor/Option;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, p1}, Lantlr/preprocessor/Option;-><init>(Ljava/lang/String;Ljava/lang/String;Lantlr/preprocessor/Grammar;)V

    invoke-virtual {v3}, Lantlr/preprocessor/Option;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, Lantlr/collections/impl/IndexedVector;->appendElement(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    const-string v4, "importVocab"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p1, Lantlr/preprocessor/Grammar;->specifiedVocabulary:Z

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lantlr/preprocessor/Grammar;->importVocab:Ljava/lang/String;
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    :goto_1
    return-object v1

    :cond_1
    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v3, "exportVocab"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v0, p1, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public reportError(Lantlr/RecognitionException;)V
    .locals 5

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getTool()Lantlr/Tool;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getTool()Lantlr/Tool;

    move-result-object v0

    invoke-virtual {p1}, Lantlr/RecognitionException;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/RecognitionException;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/RecognitionException;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/RecognitionException;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lantlr/LLkParser;->reportError(Lantlr/RecognitionException;)V

    goto :goto_0
.end method

.method public reportError(Ljava/lang/String;)V
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getTool()Lantlr/Tool;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getTool()Lantlr/Tool;

    move-result-object v0

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2, v2}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lantlr/LLkParser;->reportError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reportWarning(Ljava/lang/String;)V
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getTool()Lantlr/Tool;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getTool()Lantlr/Tool;

    move-result-object v0

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2, v2}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lantlr/LLkParser;->reportWarning(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final rule(Lantlr/preprocessor/Grammar;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v1, ""

    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p0, v4}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v0, Lantlr/preprocessor/Preprocessor;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    const/16 v4, 0x10

    :try_start_1
    invoke-virtual {p0, v4}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const-string v4, "protected"

    move-object v6, v4

    :goto_1
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v7

    const/16 v4, 0x9

    invoke-virtual {p0, v4}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_1
    const/16 v4, 0x11

    invoke-virtual {p0, v4}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const-string v4, "private"

    move-object v6, v4

    goto :goto_1

    :sswitch_2
    const/16 v4, 0x12

    invoke-virtual {p0, v4}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const-string v4, "public"

    move-object v6, v4

    goto :goto_1

    :sswitch_3
    move-object v6, v0

    goto :goto_1

    :sswitch_4
    const/16 v3, 0x13

    invoke-virtual {p0, v3}, Lantlr/preprocessor/Preprocessor;->match(I)V

    move v5, v2

    :goto_2
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_5
    move v5, v3

    goto :goto_2

    :sswitch_6
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v3, 0x14

    invoke-virtual {p0, v3}, Lantlr/preprocessor/Preprocessor;->match(I)V

    move-object v4, v2

    :goto_3
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_7
    move-object v4, v0

    goto :goto_3

    :sswitch_8
    const/16 v2, 0x15

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v3, 0x14

    invoke-virtual {p0, v3}, Lantlr/preprocessor/Preprocessor;->match(I)V

    move-object v3, v2

    :goto_4
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_4

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_9
    move-object v3, v0

    goto :goto_4

    :sswitch_a
    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->throwsSpec()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_5
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_b
    move-object v2, v1

    goto :goto_5

    :sswitch_c
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->optionSpec(Lantlr/preprocessor/Grammar;)Lantlr/collections/impl/IndexedVector;

    move-result-object v1

    :goto_6
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v8

    sparse-switch v8, :sswitch_data_6

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_d
    move-object v1, v0

    goto :goto_6

    :sswitch_e
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v8, 0x7

    invoke-virtual {p0, v8}, Lantlr/preprocessor/Preprocessor;->match(I)V

    :sswitch_f
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v8

    const/16 v9, 0x16

    invoke-virtual {p0, v9}, Lantlr/preprocessor/Preprocessor;->match(I)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->exceptionGroup()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v8}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lantlr/preprocessor/Rule;

    invoke-virtual {v7}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v7, v8, v1, p1}, Lantlr/preprocessor/Rule;-><init>(Ljava/lang/String;Ljava/lang/String;Lantlr/collections/impl/IndexedVector;Lantlr/preprocessor/Grammar;)V

    invoke-virtual {v9, v2}, Lantlr/preprocessor/Rule;->setThrowsSpec(Ljava/lang/String;)V

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lantlr/preprocessor/Rule;->setArgs(Ljava/lang/String;)V

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lantlr/preprocessor/Rule;->setReturnValue(Ljava/lang/String;)V

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lantlr/preprocessor/Rule;->setInitAction(Ljava/lang/String;)V

    :cond_3
    if-eqz v5, :cond_4

    invoke-virtual {v9}, Lantlr/preprocessor/Rule;->setBang()V

    :cond_4
    invoke-virtual {v9, v6}, Lantlr/preprocessor/Rule;->setVisibility(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1, v9}, Lantlr/preprocessor/Grammar;->addRule(Lantlr/preprocessor/Rule;)V
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_3
        0x10 -> :sswitch_0
        0x11 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7 -> :sswitch_5
        0xd -> :sswitch_5
        0x13 -> :sswitch_4
        0x14 -> :sswitch_5
        0x15 -> :sswitch_5
        0x16 -> :sswitch_5
        0x17 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7 -> :sswitch_7
        0xd -> :sswitch_7
        0x14 -> :sswitch_6
        0x15 -> :sswitch_7
        0x16 -> :sswitch_7
        0x17 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x7 -> :sswitch_9
        0xd -> :sswitch_9
        0x15 -> :sswitch_8
        0x16 -> :sswitch_9
        0x17 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x7 -> :sswitch_b
        0xd -> :sswitch_b
        0x16 -> :sswitch_b
        0x17 -> :sswitch_a
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x7 -> :sswitch_d
        0xd -> :sswitch_c
        0x16 -> :sswitch_d
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0x7 -> :sswitch_e
        0x16 -> :sswitch_f
    .end sparse-switch
.end method

.method public setTool(Lantlr/Tool;)V
    .locals 2

    iget-object v0, p0, Lantlr/preprocessor/Preprocessor;->antlrTool:Lantlr/Tool;

    if-nez v0, :cond_0

    iput-object p1, p0, Lantlr/preprocessor/Preprocessor;->antlrTool:Lantlr/Tool;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "antlr.Tool already registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final superClass()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    :try_start_0
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->match(I)V
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v1, Lantlr/preprocessor/Preprocessor;->_tokenSet_4:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    goto :goto_0
.end method

.method public final throwsSpec()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v4, 0x18

    const-string v1, "throws "

    const/16 v0, 0x17

    :try_start_0
    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LA(I)I

    move-result v1

    if-ne v1, v4, :cond_0

    const/16 v1, 0x18

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->match(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->LT(I)Lantlr/Token;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lantlr/preprocessor/Preprocessor;->match(I)V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    :goto_1
    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->reportError(Lantlr/RecognitionException;)V

    invoke-virtual {p0}, Lantlr/preprocessor/Preprocessor;->consume()V

    sget-object v1, Lantlr/preprocessor/Preprocessor;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Preprocessor;->consumeUntil(Lantlr/collections/impl/BitSet;)V

    :cond_0
    return-object v0

    :catch_1
    move-exception v1

    goto :goto_1
.end method
