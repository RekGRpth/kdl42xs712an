.class public interface abstract Lantlr/preprocessor/PreprocessorTokenTypes;
.super Ljava/lang/Object;
.source "PreprocessorTokenTypes.java"


# static fields
.field public static final ACTION:I = 0x7

.field public static final ALT:I = 0x1b

.field public static final ARG_ACTION:I = 0x14

.field public static final ASSIGN_RHS:I = 0xe

.field public static final BANG:I = 0x13

.field public static final CHAR_LITERAL:I = 0x26

.field public static final COMMA:I = 0x18

.field public static final COMMENT:I = 0x23

.field public static final CURLY_BLOCK_SCARF:I = 0x20

.field public static final DIGIT:I = 0x29

.field public static final ELEMENT:I = 0x1c

.field public static final EOF:I = 0x1

.field public static final ESC:I = 0x28

.field public static final HEADER_ACTION:I = 0x5

.field public static final ID:I = 0x9

.field public static final ID_OR_KEYWORD:I = 0x1f

.field public static final LITERAL_catch:I = 0x1a

.field public static final LITERAL_class:I = 0x8

.field public static final LITERAL_exception:I = 0x19

.field public static final LITERAL_extends:I = 0xa

.field public static final LITERAL_private:I = 0x11

.field public static final LITERAL_protected:I = 0x10

.field public static final LITERAL_public:I = 0x12

.field public static final LITERAL_returns:I = 0x15

.field public static final LITERAL_throws:I = 0x17

.field public static final LITERAL_tokens:I = 0x4

.field public static final LPAREN:I = 0x1d

.field public static final ML_COMMENT:I = 0x25

.field public static final NEWLINE:I = 0x22

.field public static final NULL_TREE_LOOKAHEAD:I = 0x3

.field public static final OPTIONS_START:I = 0xd

.field public static final RCURLY:I = 0xf

.field public static final RPAREN:I = 0x1e

.field public static final RULE_BLOCK:I = 0x16

.field public static final SEMI:I = 0xb

.field public static final SL_COMMENT:I = 0x24

.field public static final STRING_LITERAL:I = 0x27

.field public static final SUBRULE_BLOCK:I = 0x6

.field public static final TOKENS_SPEC:I = 0xc

.field public static final WS:I = 0x21

.field public static final XDIGIT:I = 0x2a
