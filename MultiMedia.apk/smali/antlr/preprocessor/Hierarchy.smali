.class public Lantlr/preprocessor/Hierarchy;
.super Ljava/lang/Object;
.source "Hierarchy.java"


# instance fields
.field protected LexerRoot:Lantlr/preprocessor/Grammar;

.field protected ParserRoot:Lantlr/preprocessor/Grammar;

.field protected TreeParserRoot:Lantlr/preprocessor/Grammar;

.field protected antlrTool:Lantlr/Tool;

.field protected files:Ljava/util/Hashtable;

.field protected symbols:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>(Lantlr/Tool;)V
    .locals 5

    const/16 v4, 0xa

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lantlr/preprocessor/Hierarchy;->LexerRoot:Lantlr/preprocessor/Grammar;

    iput-object v2, p0, Lantlr/preprocessor/Hierarchy;->ParserRoot:Lantlr/preprocessor/Grammar;

    iput-object v2, p0, Lantlr/preprocessor/Hierarchy;->TreeParserRoot:Lantlr/preprocessor/Grammar;

    iput-object p1, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    new-instance v0, Lantlr/preprocessor/Grammar;

    const-string v1, "Lexer"

    invoke-direct {v0, p1, v1, v2, v2}, Lantlr/preprocessor/Grammar;-><init>(Lantlr/Tool;Ljava/lang/String;Ljava/lang/String;Lantlr/collections/impl/IndexedVector;)V

    iput-object v0, p0, Lantlr/preprocessor/Hierarchy;->LexerRoot:Lantlr/preprocessor/Grammar;

    new-instance v0, Lantlr/preprocessor/Grammar;

    const-string v1, "Parser"

    invoke-direct {v0, p1, v1, v2, v2}, Lantlr/preprocessor/Grammar;-><init>(Lantlr/Tool;Ljava/lang/String;Ljava/lang/String;Lantlr/collections/impl/IndexedVector;)V

    iput-object v0, p0, Lantlr/preprocessor/Hierarchy;->ParserRoot:Lantlr/preprocessor/Grammar;

    new-instance v0, Lantlr/preprocessor/Grammar;

    const-string v1, "TreeParser"

    invoke-direct {v0, p1, v1, v2, v2}, Lantlr/preprocessor/Grammar;-><init>(Lantlr/Tool;Ljava/lang/String;Ljava/lang/String;Lantlr/collections/impl/IndexedVector;)V

    iput-object v0, p0, Lantlr/preprocessor/Hierarchy;->TreeParserRoot:Lantlr/preprocessor/Grammar;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0, v4}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0, v4}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Lantlr/preprocessor/Hierarchy;->files:Ljava/util/Hashtable;

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->LexerRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v0, v3}, Lantlr/preprocessor/Grammar;->setPredefined(Z)V

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->ParserRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v0, v3}, Lantlr/preprocessor/Grammar;->setPredefined(Z)V

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->TreeParserRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v0, v3}, Lantlr/preprocessor/Grammar;->setPredefined(Z)V

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->LexerRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/preprocessor/Hierarchy;->LexerRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->ParserRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/preprocessor/Hierarchy;->ParserRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->TreeParserRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/preprocessor/Hierarchy;->TreeParserRoot:Lantlr/preprocessor/Grammar;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static optionsToString(Lantlr/collections/impl/IndexedVector;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "options {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lantlr/collections/impl/IndexedVector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Option;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "line.separator"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addGrammar(Lantlr/preprocessor/Grammar;)V
    .locals 2

    invoke-virtual {p1, p0}, Lantlr/preprocessor/Grammar;->setHierarchy(Lantlr/preprocessor/Hierarchy;)V

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lantlr/preprocessor/Grammar;->getFileName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Hierarchy;->getFile(Ljava/lang/String;)Lantlr/preprocessor/GrammarFile;

    move-result-object v0

    invoke-virtual {v0, p1}, Lantlr/preprocessor/GrammarFile;->addGrammar(Lantlr/preprocessor/Grammar;)V

    return-void
.end method

.method public addGrammarFile(Lantlr/preprocessor/GrammarFile;)V
    .locals 2

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->files:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lantlr/preprocessor/GrammarFile;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public expandGrammarsInFile(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1}, Lantlr/preprocessor/Hierarchy;->getFile(Ljava/lang/String;)Lantlr/preprocessor/GrammarFile;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/preprocessor/GrammarFile;->getGrammars()Lantlr/collections/impl/IndexedVector;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/collections/impl/IndexedVector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Grammar;

    invoke-virtual {v0}, Lantlr/preprocessor/Grammar;->expandInPlace()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public findRoot(Lantlr/preprocessor/Grammar;)Lantlr/preprocessor/Grammar;
    .locals 1

    invoke-virtual {p1}, Lantlr/preprocessor/Grammar;->getSuperGrammarName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-virtual {p1}, Lantlr/preprocessor/Grammar;->getSuperGrammar()Lantlr/preprocessor/Grammar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Hierarchy;->findRoot(Lantlr/preprocessor/Grammar;)Lantlr/preprocessor/Grammar;

    move-result-object p1

    goto :goto_0
.end method

.method public getFile(Ljava/lang/String;)Lantlr/preprocessor/GrammarFile;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->files:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/GrammarFile;

    return-object v0
.end method

.method public getGrammar(Ljava/lang/String;)Lantlr/preprocessor/Grammar;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Grammar;

    return-object v0
.end method

.method public getTool()Lantlr/Tool;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    return-object v0
.end method

.method public readGrammarFile(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/FileReader;

    invoke-direct {v1, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v1, Lantlr/preprocessor/GrammarFile;

    iget-object v2, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    invoke-direct {v1, v2, p1}, Lantlr/preprocessor/GrammarFile;-><init>(Lantlr/Tool;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lantlr/preprocessor/Hierarchy;->addGrammarFile(Lantlr/preprocessor/GrammarFile;)V

    new-instance v1, Lantlr/preprocessor/PreprocessorLexer;

    invoke-direct {v1, v0}, Lantlr/preprocessor/PreprocessorLexer;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v1, p1}, Lantlr/preprocessor/PreprocessorLexer;->setFilename(Ljava/lang/String;)V

    new-instance v0, Lantlr/preprocessor/Preprocessor;

    invoke-direct {v0, v1}, Lantlr/preprocessor/Preprocessor;-><init>(Lantlr/TokenStream;)V

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    invoke-virtual {v0, v1}, Lantlr/preprocessor/Preprocessor;->setTool(Lantlr/Tool;)V

    invoke-virtual {v0, p1}, Lantlr/preprocessor/Preprocessor;->setFilename(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0, p0, p1}, Lantlr/preprocessor/Preprocessor;->grammarFile(Lantlr/preprocessor/Hierarchy;Ljava/lang/String;)V
    :try_end_0
    .catch Lantlr/TokenStreamException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lantlr/ANTLRException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Token stream error reading grammar(s):\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lantlr/Tool;->toolError(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "error reading grammar(s):\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lantlr/Tool;->toolError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTool(Lantlr/Tool;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    return-void
.end method

.method public verifyThatHierarchyIsComplete()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v4

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Grammar;

    invoke-virtual {v0}, Lantlr/preprocessor/Grammar;->getSuperGrammarName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lantlr/preprocessor/Grammar;->getSuperGrammar()Lantlr/preprocessor/Grammar;

    move-result-object v5

    if-nez v5, :cond_0

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->antlrTool:Lantlr/Tool;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "grammar "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v0}, Lantlr/preprocessor/Grammar;->getSuperGrammarName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " not defined"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lantlr/Tool;->toolError(Ljava/lang/String;)V

    iget-object v1, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v3

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    :goto_1
    return v3

    :cond_2
    iget-object v0, p0, Lantlr/preprocessor/Hierarchy;->symbols:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Grammar;

    invoke-virtual {v0}, Lantlr/preprocessor/Grammar;->getSuperGrammarName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Hierarchy;->findRoot(Lantlr/preprocessor/Grammar;)Lantlr/preprocessor/Grammar;

    move-result-object v3

    invoke-virtual {v3}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lantlr/preprocessor/Grammar;->setType(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move v3, v2

    goto :goto_1
.end method
