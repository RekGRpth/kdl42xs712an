.class public Lantlr/LLkParser;
.super Lantlr/Parser;
.source "LLkParser.java"


# instance fields
.field k:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Lantlr/Parser;-><init>()V

    iput p1, p0, Lantlr/LLkParser;->k:I

    return-void
.end method

.method public constructor <init>(Lantlr/ParserSharedInputState;I)V
    .locals 0

    invoke-direct {p0, p1}, Lantlr/Parser;-><init>(Lantlr/ParserSharedInputState;)V

    iput p2, p0, Lantlr/LLkParser;->k:I

    return-void
.end method

.method public constructor <init>(Lantlr/TokenBuffer;I)V
    .locals 0

    invoke-direct {p0}, Lantlr/Parser;-><init>()V

    iput p2, p0, Lantlr/LLkParser;->k:I

    invoke-virtual {p0, p1}, Lantlr/LLkParser;->setTokenBuffer(Lantlr/TokenBuffer;)V

    return-void
.end method

.method public constructor <init>(Lantlr/TokenStream;I)V
    .locals 1

    invoke-direct {p0}, Lantlr/Parser;-><init>()V

    iput p2, p0, Lantlr/LLkParser;->k:I

    new-instance v0, Lantlr/TokenBuffer;

    invoke-direct {v0, p1}, Lantlr/TokenBuffer;-><init>(Lantlr/TokenStream;)V

    invoke-virtual {p0, v0}, Lantlr/LLkParser;->setTokenBuffer(Lantlr/TokenBuffer;)V

    return-void
.end method

.method private trace(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, Lantlr/LLkParser;->traceIndent()V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v0, p0, Lantlr/LLkParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-lez v0, :cond_1

    const-string v0, "; [guessing]"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    iget v2, p0, Lantlr/LLkParser;->k:I

    if-gt v0, v2, :cond_3

    if-eq v0, v1, :cond_0

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v0}, Lantlr/LLkParser;->LT(I)Lantlr/Token;

    move-result-object v2

    if-eqz v2, :cond_2

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "LA("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ")=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0, v0}, Lantlr/LLkParser;->LT(I)Lantlr/Token;

    move-result-object v4

    invoke-virtual {v4}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-string v0, "; "

    goto :goto_0

    :cond_2
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "LA("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ")==null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public LA(I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/LLkParser;->inputState:Lantlr/ParserSharedInputState;

    iget-object v0, v0, Lantlr/ParserSharedInputState;->input:Lantlr/TokenBuffer;

    invoke-virtual {v0, p1}, Lantlr/TokenBuffer;->LA(I)I

    move-result v0

    return v0
.end method

.method public LT(I)Lantlr/Token;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/LLkParser;->inputState:Lantlr/ParserSharedInputState;

    iget-object v0, v0, Lantlr/ParserSharedInputState;->input:Lantlr/TokenBuffer;

    invoke-virtual {v0, p1}, Lantlr/TokenBuffer;->LT(I)Lantlr/Token;

    move-result-object v0

    return-object v0
.end method

.method public consume()V
    .locals 1

    iget-object v0, p0, Lantlr/LLkParser;->inputState:Lantlr/ParserSharedInputState;

    iget-object v0, v0, Lantlr/ParserSharedInputState;->input:Lantlr/TokenBuffer;

    invoke-virtual {v0}, Lantlr/TokenBuffer;->consume()V

    return-void
.end method

.method public traceIn(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    iget v0, p0, Lantlr/LLkParser;->traceDepth:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/LLkParser;->traceDepth:I

    const-string v0, "> "

    invoke-direct {p0, v0, p1}, Lantlr/LLkParser;->trace(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public traceOut(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const-string v0, "< "

    invoke-direct {p0, v0, p1}, Lantlr/LLkParser;->trace(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lantlr/LLkParser;->traceDepth:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/LLkParser;->traceDepth:I

    return-void
.end method
