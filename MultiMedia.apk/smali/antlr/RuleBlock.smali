.class public Lantlr/RuleBlock;
.super Lantlr/AlternativeBlock;
.source "RuleBlock.java"


# instance fields
.field protected argAction:Ljava/lang/String;

.field protected cache:[Lantlr/Lookahead;

.field protected defaultErrorHandler:Z

.field protected endNode:Lantlr/RuleEndElement;

.field exceptionSpecs:Ljava/util/Hashtable;

.field protected ignoreRule:Ljava/lang/String;

.field labeledElements:Lantlr/collections/impl/Vector;

.field protected lock:[Z

.field protected returnAction:Ljava/lang/String;

.field protected ruleName:Ljava/lang/String;

.field protected testLiterals:Z

.field protected throwsSpec:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lantlr/Grammar;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lantlr/AlternativeBlock;-><init>(Lantlr/Grammar;)V

    iput-object v1, p0, Lantlr/RuleBlock;->argAction:Ljava/lang/String;

    iput-object v1, p0, Lantlr/RuleBlock;->throwsSpec:Ljava/lang/String;

    iput-object v1, p0, Lantlr/RuleBlock;->returnAction:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/RuleBlock;->testLiterals:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lantlr/RuleBlock;->defaultErrorHandler:Z

    iput-object v1, p0, Lantlr/RuleBlock;->ignoreRule:Ljava/lang/String;

    iput-object p2, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    new-instance v0, Lantlr/collections/impl/Vector;

    invoke-direct {v0}, Lantlr/collections/impl/Vector;-><init>()V

    iput-object v0, p0, Lantlr/RuleBlock;->labeledElements:Lantlr/collections/impl/Vector;

    iget v0, p1, Lantlr/Grammar;->maxk:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Lantlr/Lookahead;

    iput-object v0, p0, Lantlr/RuleBlock;->cache:[Lantlr/Lookahead;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/RuleBlock;->exceptionSpecs:Ljava/util/Hashtable;

    instance-of v0, p1, Lantlr/ParserGrammar;

    invoke-virtual {p0, v0}, Lantlr/RuleBlock;->setAutoGen(Z)V

    return-void
.end method

.method public constructor <init>(Lantlr/Grammar;Ljava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lantlr/RuleBlock;-><init>(Lantlr/Grammar;Ljava/lang/String;)V

    iput p3, p0, Lantlr/RuleBlock;->line:I

    invoke-virtual {p0, p4}, Lantlr/RuleBlock;->setAutoGen(Z)V

    return-void
.end method


# virtual methods
.method public addExceptionSpec(Lantlr/ExceptionSpec;)V
    .locals 3

    iget-object v0, p1, Lantlr/ExceptionSpec;->label:Lantlr/Token;

    invoke-virtual {p0, v0}, Lantlr/RuleBlock;->findExceptionSpec(Lantlr/Token;)Lantlr/ExceptionSpec;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lantlr/ExceptionSpec;->label:Lantlr/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Rule \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' already has an exception handler for label: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p1, Lantlr/ExceptionSpec;->label:Lantlr/Token;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->error(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Rule \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' already has an exception handler"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->error(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lantlr/RuleBlock;->exceptionSpecs:Ljava/util/Hashtable;

    iget-object v0, p1, Lantlr/ExceptionSpec;->label:Lantlr/Token;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lantlr/ExceptionSpec;->label:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public findExceptionSpec(Lantlr/Token;)Lantlr/ExceptionSpec;
    .locals 2

    iget-object v1, p0, Lantlr/RuleBlock;->exceptionSpecs:Ljava/util/Hashtable;

    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/ExceptionSpec;

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public findExceptionSpec(Ljava/lang/String;)Lantlr/ExceptionSpec;
    .locals 1

    iget-object v0, p0, Lantlr/RuleBlock;->exceptionSpecs:Ljava/util/Hashtable;

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/ExceptionSpec;

    return-object v0
.end method

.method public generate()V
    .locals 1

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->generator:Lantlr/CodeGenerator;

    invoke-virtual {v0, p0}, Lantlr/CodeGenerator;->gen(Lantlr/AlternativeBlock;)V

    return-void
.end method

.method public getDefaultErrorHandler()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/RuleBlock;->defaultErrorHandler:Z

    return v0
.end method

.method public getEndElement()Lantlr/RuleEndElement;
    .locals 1

    iget-object v0, p0, Lantlr/RuleBlock;->endNode:Lantlr/RuleEndElement;

    return-object v0
.end method

.method public getIgnoreRule()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/RuleBlock;->ignoreRule:Ljava/lang/String;

    return-object v0
.end method

.method public getRuleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    return-object v0
.end method

.method public getTestLiterals()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/RuleBlock;->testLiterals:Z

    return v0
.end method

.method public isLexerAutoGenRule()Z
    .locals 2

    iget-object v0, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    const-string v1, "nextToken"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public look(I)Lantlr/Lookahead;
    .locals 1

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, p1, p0}, Lantlr/LLkGrammarAnalyzer;->look(ILantlr/RuleBlock;)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method

.method public prepareForAnalysis()V
    .locals 1

    invoke-super {p0}, Lantlr/AlternativeBlock;->prepareForAnalysis()V

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget v0, v0, Lantlr/Grammar;->maxk:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Z

    iput-object v0, p0, Lantlr/RuleBlock;->lock:[Z

    return-void
.end method

.method public setDefaultErrorHandler(Z)V
    .locals 0

    iput-boolean p1, p0, Lantlr/RuleBlock;->defaultErrorHandler:Z

    return-void
.end method

.method public setEndElement(Lantlr/RuleEndElement;)V
    .locals 0

    iput-object p1, p0, Lantlr/RuleBlock;->endNode:Lantlr/RuleEndElement;

    return-void
.end method

.method public setOption(Lantlr/Token;Lantlr/Token;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "defaultErrorHandler"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lantlr/RuleBlock;->defaultErrorHandler:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lantlr/RuleBlock;->defaultErrorHandler:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "Value for defaultErrorHandler must be true or false"

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "testLiterals"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_3

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "testLiterals option only valid for lexer rules"

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v3, p0, Lantlr/RuleBlock;->testLiterals:Z

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v2, p0, Lantlr/RuleBlock;->testLiterals:Z

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "Value for testLiterals must be true or false"

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ignore"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_7

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "ignore option only valid for lexer rules"

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/RuleBlock;->ignoreRule:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "paraphrase"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_9

    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "paraphrase option only valid for lexer rules"

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    iget-object v1, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->getTokenSymbol(Ljava/lang/String;)Lantlr/TokenSymbol;

    move-result-object v0

    if-nez v0, :cond_a

    iget-object v1, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "cannot find token associated with rule "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/TokenSymbol;->setParaphrase(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "generateAmbigWarnings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iput-boolean v3, p0, Lantlr/RuleBlock;->generateAmbigWarnings:Z

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iput-boolean v2, p0, Lantlr/RuleBlock;->generateAmbigWarnings:Z

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "Value for generateAmbigWarnings must be true or false"

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Invalid rule option: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const/4 v0, 0x1

    const-string v1, " FOLLOW={"

    iget-object v2, p0, Lantlr/RuleBlock;->endNode:Lantlr/RuleEndElement;

    iget-object v3, v2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    iget-object v2, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget v4, v2, Lantlr/Grammar;->maxk:I

    move v2, v0

    :goto_0
    if-gt v2, v4, :cond_2

    aget-object v5, v3, v2

    if-nez v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    aget-object v1, v3, v2

    const-string v5, ","

    iget-object v6, p0, Lantlr/RuleBlock;->grammar:Lantlr/Grammar;

    iget-object v6, v6, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v6}, Lantlr/TokenManager;->getVocabulary()Lantlr/collections/impl/Vector;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/collections/impl/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    if-ge v2, v4, :cond_0

    add-int/lit8 v5, v2, 0x1

    aget-object v5, v3, v5

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v5, ";"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_3

    const-string v0, ""

    :goto_2
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-super {p0}, Lantlr/AlternativeBlock;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " ;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method
