.class public Lantlr/MismatchedCharException;
.super Lantlr/RecognitionException;
.source "MismatchedCharException.java"


# static fields
.field public static final CHAR:I = 0x1

.field public static final NOT_CHAR:I = 0x2

.field public static final NOT_RANGE:I = 0x4

.field public static final NOT_SET:I = 0x6

.field public static final RANGE:I = 0x3

.field public static final SET:I = 0x5


# instance fields
.field public expecting:I

.field public foundChar:I

.field public mismatchType:I

.field public scanner:Lantlr/CharScanner;

.field public set:Lantlr/collections/impl/BitSet;

.field public upper:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Mismatched char"

    invoke-direct {p0, v0}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(CCCZLantlr/CharScanner;)V
    .locals 4

    const-string v0, "Mismatched char"

    invoke-virtual {p5}, Lantlr/CharScanner;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5}, Lantlr/CharScanner;->getLine()I

    move-result v2

    invoke-virtual {p5}, Lantlr/CharScanner;->getColumn()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    if-eqz p4, :cond_0

    const/4 v0, 0x4

    :goto_0
    iput v0, p0, Lantlr/MismatchedCharException;->mismatchType:I

    iput p1, p0, Lantlr/MismatchedCharException;->foundChar:I

    iput p2, p0, Lantlr/MismatchedCharException;->expecting:I

    iput p3, p0, Lantlr/MismatchedCharException;->upper:I

    iput-object p5, p0, Lantlr/MismatchedCharException;->scanner:Lantlr/CharScanner;

    return-void

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public constructor <init>(CCZLantlr/CharScanner;)V
    .locals 4

    const-string v0, "Mismatched char"

    invoke-virtual {p4}, Lantlr/CharScanner;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lantlr/CharScanner;->getLine()I

    move-result v2

    invoke-virtual {p4}, Lantlr/CharScanner;->getColumn()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    if-eqz p3, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lantlr/MismatchedCharException;->mismatchType:I

    iput p1, p0, Lantlr/MismatchedCharException;->foundChar:I

    iput p2, p0, Lantlr/MismatchedCharException;->expecting:I

    iput-object p4, p0, Lantlr/MismatchedCharException;->scanner:Lantlr/CharScanner;

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(CLantlr/collections/impl/BitSet;ZLantlr/CharScanner;)V
    .locals 4

    const-string v0, "Mismatched char"

    invoke-virtual {p4}, Lantlr/CharScanner;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lantlr/CharScanner;->getLine()I

    move-result v2

    invoke-virtual {p4}, Lantlr/CharScanner;->getColumn()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    if-eqz p3, :cond_0

    const/4 v0, 0x6

    :goto_0
    iput v0, p0, Lantlr/MismatchedCharException;->mismatchType:I

    iput p1, p0, Lantlr/MismatchedCharException;->foundChar:I

    iput-object p2, p0, Lantlr/MismatchedCharException;->set:Lantlr/collections/impl/BitSet;

    iput-object p4, p0, Lantlr/MismatchedCharException;->scanner:Lantlr/CharScanner;

    return-void

    :cond_0
    const/4 v0, 0x5

    goto :goto_0
.end method

.method private appendCharName(Ljava/lang/StringBuffer;I)V
    .locals 2

    const/16 v1, 0x27

    sparse-switch p2, :sswitch_data_0

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    int-to-char v0, p2

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_0
    return-void

    :sswitch_0
    const-string v0, "\'<EOF>\'"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :sswitch_1
    const-string v0, "\'\\n\'"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :sswitch_2
    const-string v0, "\'\\r\'"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :sswitch_3
    const-string v0, "\'\\t\'"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_3
        0xa -> :sswitch_1
        0xd -> :sswitch_2
        0xffff -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget v0, p0, Lantlr/MismatchedCharException;->mismatchType:I

    packed-switch v0, :pswitch_data_0

    invoke-super {p0}, Lantlr/RecognitionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    const-string v0, "expecting "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->expecting:I

    invoke-direct {p0, v1, v0}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    const-string v0, ", found "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->foundChar:I

    invoke-direct {p0, v1, v0}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    goto :goto_0

    :pswitch_1
    const-string v0, "expecting anything but \'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->expecting:I

    invoke-direct {p0, v1, v0}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    const-string v0, "\'; got it anyway"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_2
    const-string v0, "expecting token "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->mismatchType:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const-string v0, "NOT "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    const-string v0, "in range: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->expecting:I

    invoke-direct {p0, v1, v0}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    const-string v0, ".."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->upper:I

    invoke-direct {p0, v1, v0}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    const-string v0, ", found "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->foundChar:I

    invoke-direct {p0, v1, v0}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "expecting "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v0, p0, Lantlr/MismatchedCharException;->mismatchType:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_1

    const-string v0, "NOT "

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "one of ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lantlr/MismatchedCharException;->set:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->toArray()[I

    move-result-object v2

    const/4 v0, 0x0

    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget v3, v2, v0

    invoke-direct {p0, v1, v3}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    const-string v0, ""

    goto :goto_1

    :cond_2
    const-string v0, "), found "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v0, p0, Lantlr/MismatchedCharException;->foundChar:I

    invoke-direct {p0, v1, v0}, Lantlr/MismatchedCharException;->appendCharName(Ljava/lang/StringBuffer;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
