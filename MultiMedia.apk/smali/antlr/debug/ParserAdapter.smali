.class public Lantlr/debug/ParserAdapter;
.super Ljava/lang/Object;
.source "ParserAdapter.java"

# interfaces
.implements Lantlr/debug/ParserListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doneParsing(Lantlr/debug/TraceEvent;)V
    .locals 0

    return-void
.end method

.method public enterRule(Lantlr/debug/TraceEvent;)V
    .locals 0

    return-void
.end method

.method public exitRule(Lantlr/debug/TraceEvent;)V
    .locals 0

    return-void
.end method

.method public parserConsume(Lantlr/debug/ParserTokenEvent;)V
    .locals 0

    return-void
.end method

.method public parserLA(Lantlr/debug/ParserTokenEvent;)V
    .locals 0

    return-void
.end method

.method public parserMatch(Lantlr/debug/ParserMatchEvent;)V
    .locals 0

    return-void
.end method

.method public parserMatchNot(Lantlr/debug/ParserMatchEvent;)V
    .locals 0

    return-void
.end method

.method public parserMismatch(Lantlr/debug/ParserMatchEvent;)V
    .locals 0

    return-void
.end method

.method public parserMismatchNot(Lantlr/debug/ParserMatchEvent;)V
    .locals 0

    return-void
.end method

.method public refresh()V
    .locals 0

    return-void
.end method

.method public reportError(Lantlr/debug/MessageEvent;)V
    .locals 0

    return-void
.end method

.method public reportWarning(Lantlr/debug/MessageEvent;)V
    .locals 0

    return-void
.end method

.method public semanticPredicateEvaluated(Lantlr/debug/SemanticPredicateEvent;)V
    .locals 0

    return-void
.end method

.method public syntacticPredicateFailed(Lantlr/debug/SyntacticPredicateEvent;)V
    .locals 0

    return-void
.end method

.method public syntacticPredicateStarted(Lantlr/debug/SyntacticPredicateEvent;)V
    .locals 0

    return-void
.end method

.method public syntacticPredicateSucceeded(Lantlr/debug/SyntacticPredicateEvent;)V
    .locals 0

    return-void
.end method
