.class public abstract Lantlr/debug/DebuggingCharScanner;
.super Lantlr/CharScanner;
.source "DebuggingCharScanner.java"

# interfaces
.implements Lantlr/debug/DebuggingParser;


# instance fields
.field private _notDebugMode:Z

.field private parserEventSupport:Lantlr/debug/ParserEventSupport;

.field protected ruleNames:[Ljava/lang/String;

.field protected semPredNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lantlr/InputBuffer;)V
    .locals 1

    invoke-direct {p0, p1}, Lantlr/CharScanner;-><init>(Lantlr/InputBuffer;)V

    new-instance v0, Lantlr/debug/ParserEventSupport;

    invoke-direct {v0, p0}, Lantlr/debug/ParserEventSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/debug/DebuggingCharScanner;->_notDebugMode:Z

    return-void
.end method

.method public constructor <init>(Lantlr/LexerSharedInputState;)V
    .locals 1

    invoke-direct {p0, p1}, Lantlr/CharScanner;-><init>(Lantlr/LexerSharedInputState;)V

    new-instance v0, Lantlr/debug/ParserEventSupport;

    invoke-direct {v0, p0}, Lantlr/debug/ParserEventSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/debug/DebuggingCharScanner;->_notDebugMode:Z

    return-void
.end method


# virtual methods
.method public LA(I)C
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/CharStreamException;
        }
    .end annotation

    invoke-super {p0, p1}, Lantlr/CharScanner;->LA(I)C

    move-result v0

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v1, p1, v0}, Lantlr/debug/ParserEventSupport;->fireLA(II)V

    return v0
.end method

.method public addMessageListener(Lantlr/debug/MessageListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addMessageListener(Lantlr/debug/MessageListener;)V

    return-void
.end method

.method public addNewLineListener(Lantlr/debug/NewLineListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addNewLineListener(Lantlr/debug/NewLineListener;)V

    return-void
.end method

.method public addParserListener(Lantlr/debug/ParserListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addParserListener(Lantlr/debug/ParserListener;)V

    return-void
.end method

.method public addParserMatchListener(Lantlr/debug/ParserMatchListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addParserMatchListener(Lantlr/debug/ParserMatchListener;)V

    return-void
.end method

.method public addParserTokenListener(Lantlr/debug/ParserTokenListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addParserTokenListener(Lantlr/debug/ParserTokenListener;)V

    return-void
.end method

.method public addSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V

    return-void
.end method

.method public addSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V

    return-void
.end method

.method public addTraceListener(Lantlr/debug/TraceListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addTraceListener(Lantlr/debug/TraceListener;)V

    return-void
.end method

.method public consume()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/CharStreamException;
        }
    .end annotation

    const/16 v0, -0x63

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lantlr/debug/DebuggingCharScanner;->LA(I)C
    :try_end_0
    .catch Lantlr/CharStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    invoke-super {p0}, Lantlr/CharScanner;->consume()V

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v1, v0}, Lantlr/debug/ParserEventSupport;->fireConsume(I)V

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected fireEnterRule(II)V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/DebuggingCharScanner;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v1, v1, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, p1, v1, p2}, Lantlr/debug/ParserEventSupport;->fireEnterRule(III)V

    :cond_0
    return-void
.end method

.method protected fireExitRule(II)V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/DebuggingCharScanner;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v1, v1, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, p1, v1, p2}, Lantlr/debug/ParserEventSupport;->fireExitRule(III)V

    :cond_0
    return-void
.end method

.method protected fireSemanticPredicateEvaluated(IIZ)Z
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/DebuggingCharScanner;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v1, v1, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, p1, p2, p3, v1}, Lantlr/debug/ParserEventSupport;->fireSemanticPredicateEvaluated(IIZI)Z

    move-result p3

    :cond_0
    return p3
.end method

.method protected fireSyntacticPredicateFailed()V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/DebuggingCharScanner;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v1, v1, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, v1}, Lantlr/debug/ParserEventSupport;->fireSyntacticPredicateFailed(I)V

    :cond_0
    return-void
.end method

.method protected fireSyntacticPredicateStarted()V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/DebuggingCharScanner;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v1, v1, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, v1}, Lantlr/debug/ParserEventSupport;->fireSyntacticPredicateStarted(I)V

    :cond_0
    return-void
.end method

.method protected fireSyntacticPredicateSucceeded()V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/DebuggingCharScanner;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v1, v1, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, v1}, Lantlr/debug/ParserEventSupport;->fireSyntacticPredicateSucceeded(I)V

    :cond_0
    return-void
.end method

.method public getRuleName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->ruleNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getSemPredName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->semPredNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public declared-synchronized goToSleep()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isDebugMode()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/debug/DebuggingCharScanner;->_notDebugMode:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeToken(I)Lantlr/Token;
    .locals 1

    invoke-super {p0, p1}, Lantlr/CharScanner;->makeToken(I)Lantlr/Token;

    move-result-object v0

    return-object v0
.end method

.method public match(C)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedCharException;,
            Lantlr/CharStreamException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/debug/DebuggingCharScanner;->LA(I)C

    move-result v1

    :try_start_0
    invoke-super {p0, p1}, Lantlr/CharScanner;->match(C)V

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v2, v2, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, p1, v2}, Lantlr/debug/ParserEventSupport;->fireMatch(CI)V
    :try_end_0
    .catch Lantlr/MismatchedCharException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v2, v2, Lantlr/LexerSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v3, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v3, v3, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v2, v1, p1, v3}, Lantlr/debug/ParserEventSupport;->fireMismatch(CCI)V

    :cond_0
    throw v0
.end method

.method public match(Lantlr/collections/impl/BitSet;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedCharException;,
            Lantlr/CharStreamException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v0}, Lantlr/ANTLRStringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/debug/DebuggingCharScanner;->LA(I)C

    move-result v2

    :try_start_0
    invoke-super {p0, p1}, Lantlr/CharScanner;->match(Lantlr/collections/impl/BitSet;)V

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v3, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v3, v3, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, v2, p1, v1, v3}, Lantlr/debug/ParserEventSupport;->fireMatch(ILantlr/collections/impl/BitSet;Ljava/lang/String;I)V
    :try_end_0
    .catch Lantlr/MismatchedCharException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v3, v3, Lantlr/LexerSharedInputState;->guessing:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v4, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v4, v4, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v3, v2, p1, v1, v4}, Lantlr/debug/ParserEventSupport;->fireMismatch(ILantlr/collections/impl/BitSet;Ljava/lang/String;I)V

    :cond_0
    throw v0
.end method

.method public match(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedCharException;,
            Lantlr/CharStreamException;
        }
    .end annotation

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v0, ""

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v0, 0x1

    :goto_0
    if-gt v0, v2, :cond_0

    :try_start_0
    invoke-super {p0, v0}, Lantlr/CharScanner;->LA(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1}, Lantlr/CharScanner;->match(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v2, v2, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, p1, v2}, Lantlr/debug/ParserEventSupport;->fireMatch(Ljava/lang/String;I)V
    :try_end_1
    .catch Lantlr/MismatchedCharException; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_1
    move-exception v0

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v2, v2, Lantlr/LexerSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v3, v3, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v2, v1, p1, v3}, Lantlr/debug/ParserEventSupport;->fireMismatch(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    throw v0
.end method

.method public matchNot(C)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedCharException;,
            Lantlr/CharStreamException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/debug/DebuggingCharScanner;->LA(I)C

    move-result v1

    :try_start_0
    invoke-super {p0, p1}, Lantlr/CharScanner;->matchNot(C)V

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v2, v2, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, v1, p1, v2}, Lantlr/debug/ParserEventSupport;->fireMatchNot(CCI)V
    :try_end_0
    .catch Lantlr/MismatchedCharException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v2, v2, Lantlr/LexerSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v3, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v3, v3, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v2, v1, p1, v3}, Lantlr/debug/ParserEventSupport;->fireMismatchNot(CCI)V

    :cond_0
    throw v0
.end method

.method public matchRange(CC)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedCharException;,
            Lantlr/CharStreamException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/debug/DebuggingCharScanner;->LA(I)C

    move-result v1

    :try_start_0
    invoke-super {p0, p1, p2}, Lantlr/CharScanner;->matchRange(CC)V

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v3, v3, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v0, v1, v2, v3}, Lantlr/debug/ParserEventSupport;->fireMatch(CLjava/lang/String;I)V
    :try_end_0
    .catch Lantlr/MismatchedCharException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v2, v2, Lantlr/LexerSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lantlr/debug/DebuggingCharScanner;->inputState:Lantlr/LexerSharedInputState;

    iget v4, v4, Lantlr/LexerSharedInputState;->guessing:I

    invoke-virtual {v2, v1, v3, v4}, Lantlr/debug/ParserEventSupport;->fireMismatch(CLjava/lang/String;I)V

    :cond_0
    throw v0
.end method

.method public newline()V
    .locals 2

    invoke-super {p0}, Lantlr/CharScanner;->newline()V

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {p0}, Lantlr/debug/DebuggingCharScanner;->getLine()I

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/debug/ParserEventSupport;->fireNewLine(I)V

    return-void
.end method

.method public removeMessageListener(Lantlr/debug/MessageListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeMessageListener(Lantlr/debug/MessageListener;)V

    return-void
.end method

.method public removeNewLineListener(Lantlr/debug/NewLineListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeNewLineListener(Lantlr/debug/NewLineListener;)V

    return-void
.end method

.method public removeParserListener(Lantlr/debug/ParserListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeParserListener(Lantlr/debug/ParserListener;)V

    return-void
.end method

.method public removeParserMatchListener(Lantlr/debug/ParserMatchListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeParserMatchListener(Lantlr/debug/ParserMatchListener;)V

    return-void
.end method

.method public removeParserTokenListener(Lantlr/debug/ParserTokenListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeParserTokenListener(Lantlr/debug/ParserTokenListener;)V

    return-void
.end method

.method public removeSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V

    return-void
.end method

.method public removeSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V

    return-void
.end method

.method public removeTraceListener(Lantlr/debug/TraceListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeTraceListener(Lantlr/debug/TraceListener;)V

    return-void
.end method

.method public reportError(Lantlr/MismatchedCharException;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->fireReportError(Ljava/lang/Exception;)V

    invoke-super {p0, p1}, Lantlr/CharScanner;->reportError(Lantlr/RecognitionException;)V

    return-void
.end method

.method public reportError(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->fireReportError(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lantlr/CharScanner;->reportError(Ljava/lang/String;)V

    return-void
.end method

.method public reportWarning(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/DebuggingCharScanner;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->fireReportWarning(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lantlr/CharScanner;->reportWarning(Ljava/lang/String;)V

    return-void
.end method

.method public setDebugMode(Z)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lantlr/debug/DebuggingCharScanner;->_notDebugMode:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setupDebugging()V
    .locals 0

    return-void
.end method

.method public declared-synchronized wakeUp()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
