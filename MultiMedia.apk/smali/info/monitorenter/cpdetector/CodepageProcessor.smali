.class public Linfo/monitorenter/cpdetector/CodepageProcessor;
.super Linfo/monitorenter/cpdetector/ACmdLineArgsInheritor;
.source "CodepageProcessor.java"


# static fields
.field private static fileseparator:Ljava/lang/String;

.field private static rawtransportBuffer:[B

.field private static transcodeBuffer:[C


# instance fields
.field protected collectionRoot:Ljava/io/File;

.field protected detector:Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

.field private extensionFilter:Ljava/io/FileFilter;

.field private moveUnknown:Z

.field private outputDir:Ljava/io/File;

.field private parseCodepages:[Ljava/nio/charset/Charset;

.field private printCharsets:Z

.field private targetCodepage:Ljava/nio/charset/Charset;

.field private verbose:Z

.field private wait:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x400

    const-string v0, "file.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->fileseparator:Ljava/lang/String;

    new-array v0, v1, [C

    sput-object v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->transcodeBuffer:[C

    new-array v0, v1, [B

    sput-object v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->rawtransportBuffer:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Linfo/monitorenter/cpdetector/ACmdLineArgsInheritor;-><init>()V

    iput-object v2, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    iput-boolean v0, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->moveUnknown:Z

    iput-boolean v0, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->printCharsets:Z

    iput-boolean v0, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->wait:J

    iput-object v2, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    invoke-static {}, Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;->getInstance()Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

    move-result-object v0

    iput-object v0, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->detector:Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

    const-string v0, "documents"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$StringOption;

    const/16 v2, 0x72

    const-string v3, "documents"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$StringOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "extensions"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$StringOption;

    const/16 v2, 0x65

    const-string v3, "extensions"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$StringOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "outputDir"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$StringOption;

    const/16 v2, 0x6f

    const-string v3, "outputDir"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$StringOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "moveUnknown"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$BooleanOption;

    const/16 v2, 0x6d

    const-string v3, "moveUnknown"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$BooleanOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "verbose"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$BooleanOption;

    const/16 v2, 0x76

    const-string v3, "verbose"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$BooleanOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "wait"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$IntegerOption;

    const/16 v2, 0x77

    const-string v3, "wait"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$IntegerOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "transform"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$StringOption;

    const/16 v2, 0x74

    const-string v3, "transform"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$StringOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "detectors"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$StringOption;

    const/16 v2, 0x64

    const-string v3, "detectors"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$StringOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    const-string v0, "charsets"

    new-instance v1, Ljargs/gnu/CmdLineParser$Option$BooleanOption;

    const/16 v2, 0x63

    const-string v3, "charsets"

    invoke-direct {v1, v2, v3}, Ljargs/gnu/CmdLineParser$Option$BooleanOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->addCmdLineOption(Ljava/lang/String;Ljargs/gnu/CmdLineParser$Option;)V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 1
    .param p0    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Linfo/monitorenter/cpdetector/CodepageProcessor;

    invoke-direct {v0}, Linfo/monitorenter/cpdetector/CodepageProcessor;-><init>()V

    invoke-virtual {v0, p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseArgs([Ljava/lang/String;)V

    invoke-virtual {v0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->process()V

    return-void
.end method

.method private final parseCSVList(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, ";,"

    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    goto :goto_0
.end method

.method private printCharsets()V
    .locals 7

    iget-object v4, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    if-eqz v4, :cond_0

    iget-object v4, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    array-length v4, v4

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->loadCodepages()V

    :cond_1
    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    array-length v4, v4

    if-ge v2, v4, :cond_3

    iget-object v4, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    aget-object v1, v4, v2

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->aliases()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "    "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private process(Ljava/io/File;)V
    .locals 17
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-wide v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->wait:J

    invoke-static {v13, v14}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Linfo/monitorenter/util/FileUtil;->cutDirectoryInformation(Ljava/lang/String;)Ljava/util/Map$Entry;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    sget-object v13, Linfo/monitorenter/cpdetector/CodepageProcessor;->fileseparator:Ljava/lang/String;

    invoke-virtual {v7, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v8

    if-le v8, v9, :cond_4

    const-string v7, ""

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_0

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Processing document: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->detector:Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->toURL()Ljava/net/URL;

    move-result-object v14

    invoke-virtual {v13, v14}, Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;->detectCodepage(Ljava/net/URL;)Ljava/nio/charset/Charset;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Linfo/monitorenter/cpdetector/io/UnknownCharset;->getInstance()Ljava/nio/charset/Charset;

    move-result-object v13

    if-ne v2, v13, :cond_6

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_2

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "  Charset not detected."

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->moveUnknown:Z

    if-nez v13, :cond_5

    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_3

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "  Dropping document."

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v14, v9, 0x1

    invoke-virtual {v7, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_5
    invoke-static {}, Linfo/monitorenter/cpdetector/io/UnknownCharset;->getInstance()Ljava/nio/charset/Charset;

    move-result-object v2

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    if-eqz v13, :cond_c

    if-eqz v2, :cond_c

    invoke-static {}, Linfo/monitorenter/cpdetector/io/UnknownCharset;->getInstance()Ljava/nio/charset/Charset;

    move-result-object v13

    if-eq v13, v2, :cond_c

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_9

    new-instance v10, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    invoke-virtual {v14}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    move-result v13

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_7

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "  Created directory : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_7
    new-instance v11, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_8

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "  Moving to \""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\"."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_a

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v13

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v15

    cmp-long v13, v13, v15

    if-nez v13, :cond_a

    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_10

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "  File already exists and has same size. Skipping move."

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object v10, v11

    goto/16 :goto_2

    :cond_9
    new-instance v10, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    invoke-virtual {v14}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_a
    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z

    new-instance v4, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    new-instance v14, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v13, v14, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v4, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v6, Ljava/io/BufferedWriter;

    new-instance v13, Ljava/io/OutputStreamWriter;

    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    invoke-direct {v13, v14, v15}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v6, v13}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    sget-object v13, Linfo/monitorenter/cpdetector/CodepageProcessor;->transcodeBuffer:[C

    array-length v12, v13

    :goto_4
    sget-object v13, Linfo/monitorenter/cpdetector/CodepageProcessor;->transcodeBuffer:[C

    const/4 v14, 0x0

    invoke-virtual {v4, v13, v14, v12}, Ljava/io/Reader;->read([CII)I

    move-result v5

    const/4 v13, -0x1

    if-eq v5, v13, :cond_b

    sget-object v13, Linfo/monitorenter/cpdetector/CodepageProcessor;->transcodeBuffer:[C

    const/4 v14, 0x0

    invoke-virtual {v6, v13, v14, v5}, Ljava/io/Writer;->write([CII)V

    goto :goto_4

    :cond_b
    invoke-virtual {v4}, Ljava/io/Reader;->close()V

    invoke-virtual {v6}, Ljava/io/Writer;->close()V

    move-object v10, v11

    goto/16 :goto_2

    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    if-eqz v13, :cond_d

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Skipping transformation of document "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " because it\'s charset could not be detected."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_11

    new-instance v10, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_5
    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    move-result v13

    if-eqz v13, :cond_e

    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_e

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Created directory : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_e
    new-instance v11, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v13, :cond_f

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "  Moving to \""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\"."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_f
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Linfo/monitorenter/cpdetector/CodepageProcessor;->rawCopy(Ljava/io/File;Ljava/io/File;)V

    :cond_10
    move-object v10, v11

    goto/16 :goto_2

    :cond_11
    new-instance v10, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_0
    move-exception v13

    goto/16 :goto_0
.end method

.method private processRecursive(Ljava/io/File;)V
    .locals 5
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "File argument is null!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exist."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v2, v0

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_3

    aget-object v2, v0, v1

    invoke-direct {p0, v2}, Linfo/monitorenter/cpdetector/CodepageProcessor;->processRecursive(Ljava/io/File;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->extensionFilter:Ljava/io/FileFilter;

    invoke-interface {v2, p1}, Ljava/io/FileFilter;->accept(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, p1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->process(Ljava/io/File;)V

    :cond_3
    return-void
.end method

.method private rawCopy(Ljava/io/File;Ljava/io/File;)V
    .locals 9
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x0

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    :cond_1
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    sget-object v4, Linfo/monitorenter/cpdetector/CodepageProcessor;->rawtransportBuffer:[B

    array-length v3, v4

    :goto_1
    sget-object v4, Linfo/monitorenter/cpdetector/CodepageProcessor;->rawtransportBuffer:[B

    invoke-virtual {v0, v4, v8, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_2

    sget-object v4, Linfo/monitorenter/cpdetector/CodepageProcessor;->rawtransportBuffer:[B

    invoke-virtual {v2, v4, v8, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    goto :goto_0
.end method


# virtual methods
.method protected describe()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Setup:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  Collection-Root        : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  Output-Dir             : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  Move unknown           : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->moveUnknown:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  verbose                : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  wait                   : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->wait:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    if-eqz v1, :cond_0

    const-string v1, "  transform to codepage  : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    const-string v1, "  detection algorithm    : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->detector:Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

    invoke-virtual {v1}, Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method loadCodepages()V
    .locals 10

    invoke-static {}, Ljava/nio/charset/Charset;->availableCharsets()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    iget-boolean v7, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v7, :cond_0

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "Loading system codepages..."

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v2}, Ljava/util/SortedMap;->size()I

    move-result v7

    new-array v7, v7, [Ljava/nio/charset/Charset;

    iput-object v7, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    const/4 v6, 0x0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/nio/charset/Charset;

    iget-boolean v7, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    if-eqz v7, :cond_1

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Charset: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->aliases()Ljava/util/Set;

    move-result-object v1

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "  Aliases: "

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v7, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    aput-object v3, v7, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public parseArgs([Ljava/lang/String;)V
    .locals 23
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-super/range {p0 .. p1}, Linfo/monitorenter/cpdetector/ACmdLineArgsInheritor;->parseArgs([Ljava/lang/String;)V

    const-string v19, "documents"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    const-string v19, "extensions"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    const-string v19, "outputDir"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    const-string v19, "moveUnknown"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    const-string v19, "verbose"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    const-string v19, "wait"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    const-string v19, "transform"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    const-string v19, "detectors"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    const-string v19, "charsets"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->getParsedCmdLineOption(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->printCharsets:Z

    :goto_0
    return-void

    :cond_0
    if-nez v5, :cond_1

    invoke-virtual/range {p0 .. p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->usage()V

    new-instance v19, Ljava/util/MissingResourceException;

    const-string v20, "Parameter for collection root directory is missing."

    const-string v21, "String"

    const-string v22, "-r"

    invoke-direct/range {v19 .. v22}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v19

    :cond_1
    new-instance v19, Ljava/io/File;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    if-nez v15, :cond_2

    invoke-virtual/range {p0 .. p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->usage()V

    new-instance v19, Ljava/util/MissingResourceException;

    const-string v20, "Parameter for output directory is missing."

    const-string v21, "String"

    const-string v22, "-o"

    invoke-direct/range {v19 .. v22}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v19

    :cond_2
    new-instance v19, Ljava/io/File;

    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    if-eqz v10, :cond_7

    new-instance v19, Linfo/monitorenter/cpdetector/io/FileFilterExtensions;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCSVList(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Linfo/monitorenter/cpdetector/io/FileFilterExtensions;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->extensionFilter:Ljava/io/FileFilter;

    :goto_1
    if-eqz v13, :cond_3

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->moveUnknown:Z

    :cond_3
    if-eqz v17, :cond_4

    check-cast v17, Ljava/lang/Boolean;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_4

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    :cond_4
    if-eqz v18, :cond_5

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    move-object/from16 v2, p0

    iput-wide v0, v2, Linfo/monitorenter/cpdetector/CodepageProcessor;->wait:J

    :cond_5
    if-eqz v16, :cond_6

    move-object/from16 v3, v16

    check-cast v3, Ljava/lang/String;

    :try_start_0
    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->targetCodepage:Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    if-eqz v7, :cond_b

    check-cast v7, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCSVList(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v0, v8

    move/from16 v19, v0

    if-nez v19, :cond_9

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v19, "You specified the codepage detector argument \"-d\" but ommited any comma-separated fully qualified class-name."

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v19, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v19

    :cond_7
    new-instance v19, Linfo/monitorenter/cpdetector/CodepageProcessor$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Linfo/monitorenter/cpdetector/CodepageProcessor$1;-><init>(Linfo/monitorenter/cpdetector/CodepageProcessor;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Linfo/monitorenter/cpdetector/CodepageProcessor;->extensionFilter:Ljava/io/FileFilter;

    goto :goto_1

    :catch_0
    move-exception v9

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v19, "Given charset name: \""

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v14, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v19, "\" for option -t is illegal: \n"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v19, "  "

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v19, "\n"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v19, "   Legal values are: \n"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v11, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_8

    const-string v19, "    "

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v0, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->parseCodepages:[Ljava/nio/charset/Charset;

    move-object/from16 v19, v0

    aget-object v19, v19, v11

    invoke-virtual/range {v19 .. v19}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v19, "\n"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_8
    new-instance v19, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v19

    :cond_9
    const/4 v6, 0x0

    const/4 v11, 0x0

    :goto_3
    array-length v0, v8

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_c

    :try_start_1
    invoke-static {}, Linfo/monitorenter/cpdetector/reflect/SingletonLoader;->getInstance()Linfo/monitorenter/cpdetector/reflect/SingletonLoader;

    move-result-object v19

    aget-object v20, v8, v11

    invoke-virtual/range {v19 .. v20}, Linfo/monitorenter/cpdetector/reflect/SingletonLoader;->newInstance(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v19

    check-cast v0, Linfo/monitorenter/cpdetector/io/ICodepageDetector;

    move-object v6, v0

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->detector:Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;->add(Linfo/monitorenter/cpdetector/io/ICodepageDetector;)Z
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_a
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :catch_1
    move-exception v12

    sget-object v19, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Could not instantiate custom ICodepageDetector: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    aget-object v21, v8, v11

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " (argument \"-c\"): "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v12}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->detector:Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

    move-object/from16 v19, v0

    new-instance v20, Linfo/monitorenter/cpdetector/io/ParsingDetector;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->verbose:Z

    move/from16 v21, v0

    invoke-direct/range {v20 .. v21}, Linfo/monitorenter/cpdetector/io/ParsingDetector;-><init>(Z)V

    invoke-virtual/range {v19 .. v20}, Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;->add(Linfo/monitorenter/cpdetector/io/ICodepageDetector;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Linfo/monitorenter/cpdetector/CodepageProcessor;->detector:Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;

    move-object/from16 v19, v0

    invoke-static {}, Linfo/monitorenter/cpdetector/io/JChardetFacade;->getInstance()Linfo/monitorenter/cpdetector/io/JChardetFacade;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Linfo/monitorenter/cpdetector/io/CodepageDetectorProxy;->add(Linfo/monitorenter/cpdetector/io/ICodepageDetector;)Z

    :cond_c
    invoke-virtual/range {p0 .. p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->loadCodepages()V

    goto/16 :goto_0
.end method

.method public final process()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-boolean v0, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->printCharsets:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->printCharsets()V

    :goto_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "No exceptional program flow occured!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->verifyFiles()V

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->describe()V

    iget-object v0, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    invoke-direct {p0, v0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->processRecursive(Ljava/io/File;)V

    goto :goto_0
.end method

.method protected usage()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "usage: java -cp jargs-1.0.jar"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-char v2, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "cpdetector_1.0.9.jar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-char v2, Ljava/io/File;->pathSeparatorChar:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "antlr-2.7.4.jar"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-char v2, Ljava/io/File;->pathSeparatorChar:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "chardet.jar info.monitorenter.cpdetector.CodepageProcessor -r <testdocumentdir> -o <testoutputdir> [options]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "options: \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\n  Optional:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -c              : Only print available charsets on this system.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -e <extensions> : A comma- or semicolon- separated string for document extensions like \"-e txt,dat\" (without dot or space!).\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -m              : Move files with unknown charset to directory \"unknown\".\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -v              : Verbose output.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -w <int>        : Wait <int> seconds before trying next document (good, if you want to work on the very same machine).\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -t <charset>    : Try to transform the document to given charset (codepage) name. \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "                    This is only possible for documents that are detected to have a  \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "                    codepage that is supported by the current java VM. If not possible \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "                    sorting will be done as normal. \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -d              : Semicolon-separated list of fully qualified classnames. \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "                    These classes will be casted to ICodepageDetector instances \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "                    and used in the order specified.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "                    If this argument is ommited, a HTMLCodepageDetector followed by .\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "                    a JChardetFacade is used by default.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  Mandatory (if no -c option given) :\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -r            : Root directory containing the collection (recursive).\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "  -o            : Output directory containing the sorted collection.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    return-void
.end method

.method protected verifyFiles()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    if-nez v1, :cond_2

    const-string v1, "-> Collection root directory is null!\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    :goto_0
    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    if-nez v1, :cond_3

    const-string v1, "-> Output directory is null!\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-lez v1, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "-> Collection root directory:\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->collectionRoot:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\" does not exist!\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    iget-object v1, p0, Linfo/monitorenter/cpdetector/CodepageProcessor;->outputDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "-> Output directory has to be a directory, no File!\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_4
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "All parameters are valid."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method
