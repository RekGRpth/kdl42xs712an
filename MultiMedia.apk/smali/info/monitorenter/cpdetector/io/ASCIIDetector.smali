.class public final Linfo/monitorenter/cpdetector/io/ASCIIDetector;
.super Linfo/monitorenter/cpdetector/io/AbstractCodepageDetector;
.source "ASCIIDetector.java"


# static fields
.field private static instance:Linfo/monitorenter/cpdetector/io/ICodepageDetector;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Linfo/monitorenter/cpdetector/io/AbstractCodepageDetector;-><init>()V

    return-void
.end method

.method public static getInstance()Linfo/monitorenter/cpdetector/io/ICodepageDetector;
    .locals 1

    sget-object v0, Linfo/monitorenter/cpdetector/io/ASCIIDetector;->instance:Linfo/monitorenter/cpdetector/io/ICodepageDetector;

    if-nez v0, :cond_0

    new-instance v0, Linfo/monitorenter/cpdetector/io/ASCIIDetector;

    invoke-direct {v0}, Linfo/monitorenter/cpdetector/io/ASCIIDetector;-><init>()V

    sput-object v0, Linfo/monitorenter/cpdetector/io/ASCIIDetector;->instance:Linfo/monitorenter/cpdetector/io/ICodepageDetector;

    :cond_0
    sget-object v0, Linfo/monitorenter/cpdetector/io/ASCIIDetector;->instance:Linfo/monitorenter/cpdetector/io/ICodepageDetector;

    return-object v0
.end method


# virtual methods
.method public detectCodepage(Ljava/io/InputStream;I)Ljava/nio/charset/Charset;
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Linfo/monitorenter/cpdetector/io/UnknownCharset;->getInstance()Ljava/nio/charset/Charset;

    move-result-object v1

    instance-of v2, p1, Ljava/io/BufferedInputStream;

    if-nez v2, :cond_1

    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v2, 0x1000

    invoke-direct {v0, p1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    :goto_0
    invoke-static {v0}, Linfo/monitorenter/util/FileUtil;->isAllASCII(Ljava/io/InputStream;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "US-ASCII"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    :cond_0
    return-object v1

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method
