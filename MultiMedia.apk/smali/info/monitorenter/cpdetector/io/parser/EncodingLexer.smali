.class public Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;
.super Lantlr/CharScanner;
.source "EncodingLexer.java"

# interfaces
.implements Lantlr/TokenStream;
.implements Linfo/monitorenter/cpdetector/io/parser/EncodingParserTokenTypes;


# static fields
.field public static final _tokenSet_0:Lantlr/collections/impl/BitSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mk_tokenSet_0()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public constructor <init>(Lantlr/InputBuffer;)V
    .locals 1
    .param p1    # Lantlr/InputBuffer;

    new-instance v0, Lantlr/LexerSharedInputState;

    invoke-direct {v0, p1}, Lantlr/LexerSharedInputState;-><init>(Lantlr/InputBuffer;)V

    invoke-direct {p0, v0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;-><init>(Lantlr/LexerSharedInputState;)V

    return-void
.end method

.method public constructor <init>(Lantlr/LexerSharedInputState;)V
    .locals 1
    .param p1    # Lantlr/LexerSharedInputState;

    invoke-direct {p0, p1}, Lantlr/CharScanner;-><init>(Lantlr/LexerSharedInputState;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->caseSensitiveLiterals:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->setCaseSensitive(Z)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->literals:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    new-instance v0, Lantlr/ByteBuffer;

    invoke-direct {v0, p1}, Lantlr/ByteBuffer;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;-><init>(Lantlr/InputBuffer;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lantlr/CharBuffer;

    invoke-direct {v0, p1}, Lantlr/CharBuffer;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;-><init>(Lantlr/InputBuffer;)V

    return-void
.end method

.method private static final mk_tokenSet_0()[J
    .locals 4

    const/16 v1, 0x401

    new-array v0, v1, [J

    const/4 v1, 0x0

    const-wide v2, 0x100002400L    # 2.1220003443E-314

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    const-wide v2, 0x200000000000L

    aput-wide v2, v0, v1

    return-object v0
.end method


# virtual methods
.method protected final mDIGIT(Z)V
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v3, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/16 v2, 0xa

    const/16 v3, 0x30

    const/16 v4, 0x39

    invoke-virtual {p0, v3, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->matchRange(CC)V

    if-eqz p1, :cond_0

    if-nez v1, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0, v2}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v1

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v4

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-direct {v3, v4, v0, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v3}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v1, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method protected final mIDENTIFIER(Z)V
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/4 v2, 0x6

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mLETTER(Z)V

    :goto_0
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    if-eqz p1, :cond_0

    if-nez v1, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0, v2}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v1

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v4

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-direct {v3, v4, v0, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v3}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v1, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void

    :pswitch_1
    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mLETTER(Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mDIGIT(Z)V

    goto :goto_0

    :pswitch_3
    const/16 v3, 0x5f

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    goto :goto_0

    :pswitch_4
    const/16 v3, 0x2e

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    goto :goto_0

    :pswitch_5
    const/16 v3, 0x2d

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected final mLETTER(Z)V
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v3, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/16 v2, 0xb

    const/16 v3, 0x61

    const/16 v4, 0x7a

    invoke-virtual {p0, v3, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->matchRange(CC)V

    if-eqz p1, :cond_0

    if-nez v1, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0, v2}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v1

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v4

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-direct {v3, v4, v0, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v3}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v1, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mMETA_CONTENT_TYPE(Z)V
    .locals 11
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v10, 0x20

    const/16 v9, 0xd

    const/16 v8, 0xa

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/4 v3, 0x4

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x3c

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_0

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_0
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_1
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "meta"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_1

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_2
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_3
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "http-equiv"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_2

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_4
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_5
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x3d

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_3

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_6
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_7
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_4

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_8
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x22

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_5

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_9
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_a
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "content-type"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_6

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_b
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_c
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_7

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_d
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x22

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_8

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_e
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_f
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "content"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_9

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_10
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_11
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x3d

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_a

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_12
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_13
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :pswitch_1
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x22

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_b

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_14
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :goto_0
    :pswitch_2
    :sswitch_15
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_3
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x3b

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_c

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :pswitch_4
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mLETTER(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_d

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_16
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto :goto_0

    :pswitch_5
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mDIGIT(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_e

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_17
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x2f

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_f

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_18
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_0

    :sswitch_19
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_1a
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "charset"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_10

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_1b
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_1c
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x3d

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_0

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_0

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-ne v4, v10, :cond_1

    :cond_0
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :cond_1
    :goto_1
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    packed-switch v4, :pswitch_data_2

    :pswitch_7
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    const/16 v5, 0x22

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x22

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_2

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_2

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-ne v4, v10, :cond_3

    :cond_2
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :cond_3
    if-eqz p1, :cond_4

    if-nez v2, :cond_4

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v2

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v5

    iget-object v6, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v6}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v6

    sub-int/2addr v6, v0

    invoke-direct {v4, v5, v0, v6}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v4}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_4
    iput-object v2, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void

    :pswitch_8
    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mLETTER(Z)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_5

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_5

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-ne v4, v10, :cond_1

    :cond_5
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_1

    :pswitch_9
    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mDIGIT(Z)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_6

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_6

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-ne v4, v10, :cond_1

    :cond_6
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_1

    :pswitch_a
    const/16 v4, 0x2d

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_7

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_7

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-ne v4, v10, :cond_1

    :cond_7
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_1

    :pswitch_b
    const/16 v4, 0x5f

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_8

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_8

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-ne v4, v10, :cond_1

    :cond_8
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x6d -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_2
        0xd -> :sswitch_2
        0x20 -> :sswitch_2
        0x68 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0xa -> :sswitch_4
        0xd -> :sswitch_4
        0x20 -> :sswitch_4
        0x3d -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0xa -> :sswitch_6
        0xd -> :sswitch_6
        0x20 -> :sswitch_6
        0x22 -> :sswitch_7
        0x63 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x22 -> :sswitch_8
        0x63 -> :sswitch_a
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0xa -> :sswitch_9
        0xd -> :sswitch_9
        0x20 -> :sswitch_9
        0x63 -> :sswitch_a
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0xa -> :sswitch_b
        0xd -> :sswitch_b
        0x20 -> :sswitch_b
        0x22 -> :sswitch_c
        0x63 -> :sswitch_c
    .end sparse-switch

    :sswitch_data_7
    .sparse-switch
        0x22 -> :sswitch_d
        0x63 -> :sswitch_f
    .end sparse-switch

    :sswitch_data_8
    .sparse-switch
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x63 -> :sswitch_f
    .end sparse-switch

    :sswitch_data_9
    .sparse-switch
        0xa -> :sswitch_10
        0xd -> :sswitch_10
        0x20 -> :sswitch_10
        0x3d -> :sswitch_11
    .end sparse-switch

    :sswitch_data_a
    .sparse-switch
        0xa -> :sswitch_12
        0xd -> :sswitch_12
        0x20 -> :sswitch_12
        0x22 -> :sswitch_13
        0x2f -> :sswitch_13
        0x30 -> :sswitch_13
        0x31 -> :sswitch_13
        0x32 -> :sswitch_13
        0x33 -> :sswitch_13
        0x34 -> :sswitch_13
        0x35 -> :sswitch_13
        0x36 -> :sswitch_13
        0x37 -> :sswitch_13
        0x38 -> :sswitch_13
        0x39 -> :sswitch_13
        0x3b -> :sswitch_13
        0x61 -> :sswitch_13
        0x62 -> :sswitch_13
        0x63 -> :sswitch_13
        0x64 -> :sswitch_13
        0x65 -> :sswitch_13
        0x66 -> :sswitch_13
        0x67 -> :sswitch_13
        0x68 -> :sswitch_13
        0x69 -> :sswitch_13
        0x6a -> :sswitch_13
        0x6b -> :sswitch_13
        0x6c -> :sswitch_13
        0x6d -> :sswitch_13
        0x6e -> :sswitch_13
        0x6f -> :sswitch_13
        0x70 -> :sswitch_13
        0x71 -> :sswitch_13
        0x72 -> :sswitch_13
        0x73 -> :sswitch_13
        0x74 -> :sswitch_13
        0x75 -> :sswitch_13
        0x76 -> :sswitch_13
        0x77 -> :sswitch_13
        0x78 -> :sswitch_13
        0x79 -> :sswitch_13
        0x7a -> :sswitch_13
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x22
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :sswitch_data_b
    .sparse-switch
        0xa -> :sswitch_14
        0xd -> :sswitch_14
        0x20 -> :sswitch_14
        0x2f -> :sswitch_15
        0x30 -> :sswitch_15
        0x31 -> :sswitch_15
        0x32 -> :sswitch_15
        0x33 -> :sswitch_15
        0x34 -> :sswitch_15
        0x35 -> :sswitch_15
        0x36 -> :sswitch_15
        0x37 -> :sswitch_15
        0x38 -> :sswitch_15
        0x39 -> :sswitch_15
        0x3b -> :sswitch_15
        0x61 -> :sswitch_15
        0x62 -> :sswitch_15
        0x63 -> :sswitch_15
        0x64 -> :sswitch_15
        0x65 -> :sswitch_15
        0x66 -> :sswitch_15
        0x67 -> :sswitch_15
        0x68 -> :sswitch_15
        0x69 -> :sswitch_15
        0x6a -> :sswitch_15
        0x6b -> :sswitch_15
        0x6c -> :sswitch_15
        0x6d -> :sswitch_15
        0x6e -> :sswitch_15
        0x6f -> :sswitch_15
        0x70 -> :sswitch_15
        0x71 -> :sswitch_15
        0x72 -> :sswitch_15
        0x73 -> :sswitch_15
        0x74 -> :sswitch_15
        0x75 -> :sswitch_15
        0x76 -> :sswitch_15
        0x77 -> :sswitch_15
        0x78 -> :sswitch_15
        0x79 -> :sswitch_15
        0x7a -> :sswitch_15
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x2f
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :sswitch_data_c
    .sparse-switch
        0xa -> :sswitch_19
        0xd -> :sswitch_19
        0x20 -> :sswitch_19
        0x63 -> :sswitch_1a
    .end sparse-switch

    :sswitch_data_d
    .sparse-switch
        0xa -> :sswitch_16
        0xd -> :sswitch_16
        0x20 -> :sswitch_16
        0x2f -> :sswitch_15
        0x30 -> :sswitch_15
        0x31 -> :sswitch_15
        0x32 -> :sswitch_15
        0x33 -> :sswitch_15
        0x34 -> :sswitch_15
        0x35 -> :sswitch_15
        0x36 -> :sswitch_15
        0x37 -> :sswitch_15
        0x38 -> :sswitch_15
        0x39 -> :sswitch_15
        0x3b -> :sswitch_15
        0x61 -> :sswitch_15
        0x62 -> :sswitch_15
        0x63 -> :sswitch_15
        0x64 -> :sswitch_15
        0x65 -> :sswitch_15
        0x66 -> :sswitch_15
        0x67 -> :sswitch_15
        0x68 -> :sswitch_15
        0x69 -> :sswitch_15
        0x6a -> :sswitch_15
        0x6b -> :sswitch_15
        0x6c -> :sswitch_15
        0x6d -> :sswitch_15
        0x6e -> :sswitch_15
        0x6f -> :sswitch_15
        0x70 -> :sswitch_15
        0x71 -> :sswitch_15
        0x72 -> :sswitch_15
        0x73 -> :sswitch_15
        0x74 -> :sswitch_15
        0x75 -> :sswitch_15
        0x76 -> :sswitch_15
        0x77 -> :sswitch_15
        0x78 -> :sswitch_15
        0x79 -> :sswitch_15
        0x7a -> :sswitch_15
    .end sparse-switch

    :sswitch_data_e
    .sparse-switch
        0xa -> :sswitch_17
        0xd -> :sswitch_17
        0x20 -> :sswitch_17
        0x2f -> :sswitch_15
        0x30 -> :sswitch_15
        0x31 -> :sswitch_15
        0x32 -> :sswitch_15
        0x33 -> :sswitch_15
        0x34 -> :sswitch_15
        0x35 -> :sswitch_15
        0x36 -> :sswitch_15
        0x37 -> :sswitch_15
        0x38 -> :sswitch_15
        0x39 -> :sswitch_15
        0x3b -> :sswitch_15
        0x61 -> :sswitch_15
        0x62 -> :sswitch_15
        0x63 -> :sswitch_15
        0x64 -> :sswitch_15
        0x65 -> :sswitch_15
        0x66 -> :sswitch_15
        0x67 -> :sswitch_15
        0x68 -> :sswitch_15
        0x69 -> :sswitch_15
        0x6a -> :sswitch_15
        0x6b -> :sswitch_15
        0x6c -> :sswitch_15
        0x6d -> :sswitch_15
        0x6e -> :sswitch_15
        0x6f -> :sswitch_15
        0x70 -> :sswitch_15
        0x71 -> :sswitch_15
        0x72 -> :sswitch_15
        0x73 -> :sswitch_15
        0x74 -> :sswitch_15
        0x75 -> :sswitch_15
        0x76 -> :sswitch_15
        0x77 -> :sswitch_15
        0x78 -> :sswitch_15
        0x79 -> :sswitch_15
        0x7a -> :sswitch_15
    .end sparse-switch

    :sswitch_data_f
    .sparse-switch
        0xa -> :sswitch_18
        0xd -> :sswitch_18
        0x20 -> :sswitch_18
        0x2f -> :sswitch_15
        0x30 -> :sswitch_15
        0x31 -> :sswitch_15
        0x32 -> :sswitch_15
        0x33 -> :sswitch_15
        0x34 -> :sswitch_15
        0x35 -> :sswitch_15
        0x36 -> :sswitch_15
        0x37 -> :sswitch_15
        0x38 -> :sswitch_15
        0x39 -> :sswitch_15
        0x3b -> :sswitch_15
        0x61 -> :sswitch_15
        0x62 -> :sswitch_15
        0x63 -> :sswitch_15
        0x64 -> :sswitch_15
        0x65 -> :sswitch_15
        0x66 -> :sswitch_15
        0x67 -> :sswitch_15
        0x68 -> :sswitch_15
        0x69 -> :sswitch_15
        0x6a -> :sswitch_15
        0x6b -> :sswitch_15
        0x6c -> :sswitch_15
        0x6d -> :sswitch_15
        0x6e -> :sswitch_15
        0x6f -> :sswitch_15
        0x70 -> :sswitch_15
        0x71 -> :sswitch_15
        0x72 -> :sswitch_15
        0x73 -> :sswitch_15
        0x74 -> :sswitch_15
        0x75 -> :sswitch_15
        0x76 -> :sswitch_15
        0x77 -> :sswitch_15
        0x78 -> :sswitch_15
        0x79 -> :sswitch_15
        0x7a -> :sswitch_15
    .end sparse-switch

    :sswitch_data_10
    .sparse-switch
        0xa -> :sswitch_1b
        0xd -> :sswitch_1b
        0x20 -> :sswitch_1b
        0x3d -> :sswitch_1c
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x2d
        :pswitch_a
        :pswitch_7
        :pswitch_7
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_b
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method protected final mNEWLINE(Z)V
    .locals 8
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v5, 0xa

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/16 v2, 0x8

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-instance v3, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v7

    invoke-direct {v3, v4, v5, v6, v7}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v3

    :pswitch_1
    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->newline()V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v1, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0, v2}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v1

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v4

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-direct {v3, v4, v0, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v3}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v1, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void

    :pswitch_2
    const/16 v3, 0xd

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->newline()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final mSPACE(Z)V
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v3, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/16 v2, 0x9

    const/16 v3, 0x20

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v1, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0, v2}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v1

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v4

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-direct {v3, v4, v0, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v3}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v1, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method protected final mSPACING(Z)V
    .locals 8
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/4 v2, 0x7

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v3, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v7

    invoke-direct {v3, v4, v5, v6, v7}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v3

    :sswitch_0
    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mNEWLINE(Z)V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v1, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0, v2}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v1

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v4

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-direct {v3, v4, v0, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v3}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v1, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_1
    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACE(Z)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method

.method public final mXML_ENCODING_DECL(Z)V
    .locals 10
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0xd

    const/16 v8, 0xa

    const/16 v7, 0x22

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v0

    const/4 v3, 0x5

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "<?xml"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_0

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_0
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_1
    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_1

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_2
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "version"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_2

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_3
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_4
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "="

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_3

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_5
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_6
    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_4

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_7
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "\'"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_5

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_8
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_9
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mDIGIT(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_6

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_a
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_b
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x2e

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_7

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_c
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_d
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mDIGIT(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_8

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_e
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_f
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "\'"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_9

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_10
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :goto_0
    :sswitch_11
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "encoding"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_a

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_12
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_b

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_13
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_14
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mDIGIT(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_c

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_15
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_16
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v4, 0x2e

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_d

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_17
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_18
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mDIGIT(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_e

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_19
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_1a
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_f

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_1b
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_0

    :sswitch_1c
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_1d
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "="

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_10

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_1e
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_1f
    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_11

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_20
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "\'"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_12

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_21
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_22
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mIDENTIFIER(Z)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_13

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_23
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_24
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const-string v4, "\'"

    invoke-virtual {p0, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(Ljava/lang/String;)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_0

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_0

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    const/16 v5, 0x20

    if-ne v4, v5, :cond_1

    :cond_0
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    if-nez v2, :cond_2

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    invoke-virtual {p0, v3}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v2

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v5}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v5

    iget-object v6, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v6}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v6

    sub-int/2addr v6, v0

    invoke-direct {v4, v5, v0, v6}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v4}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_2
    iput-object v2, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_25
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_14

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_26
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_27
    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mIDENTIFIER(Z)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    sparse-switch v4, :sswitch_data_15

    new-instance v4, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getLine()I

    move-result v7

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getColumn()I

    move-result v8

    invoke-direct {v4, v5, v6, v7, v8}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v4

    :sswitch_28
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    :sswitch_29
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v7}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->match(C)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v8, :cond_3

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    if-eq v4, v9, :cond_3

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v4

    const/16 v5, 0x20

    if-ne v4, v5, :cond_1

    :cond_3
    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mSPACING(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4, v1}, Lantlr/ANTLRStringBuffer;->setLength(I)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x65 -> :sswitch_1
        0x76 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x65 -> :sswitch_11
        0x76 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0xa -> :sswitch_3
        0xd -> :sswitch_3
        0x20 -> :sswitch_3
        0x3d -> :sswitch_4
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0xa -> :sswitch_5
        0xd -> :sswitch_5
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
        0x27 -> :sswitch_6
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x22 -> :sswitch_12
        0x27 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0xa -> :sswitch_8
        0xd -> :sswitch_8
        0x20 -> :sswitch_8
        0x30 -> :sswitch_9
        0x31 -> :sswitch_9
        0x32 -> :sswitch_9
        0x33 -> :sswitch_9
        0x34 -> :sswitch_9
        0x35 -> :sswitch_9
        0x36 -> :sswitch_9
        0x37 -> :sswitch_9
        0x38 -> :sswitch_9
        0x39 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0xa -> :sswitch_a
        0xd -> :sswitch_a
        0x20 -> :sswitch_a
        0x2e -> :sswitch_b
    .end sparse-switch

    :sswitch_data_7
    .sparse-switch
        0xa -> :sswitch_c
        0xd -> :sswitch_c
        0x20 -> :sswitch_c
        0x30 -> :sswitch_d
        0x31 -> :sswitch_d
        0x32 -> :sswitch_d
        0x33 -> :sswitch_d
        0x34 -> :sswitch_d
        0x35 -> :sswitch_d
        0x36 -> :sswitch_d
        0x37 -> :sswitch_d
        0x38 -> :sswitch_d
        0x39 -> :sswitch_d
    .end sparse-switch

    :sswitch_data_8
    .sparse-switch
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x20 -> :sswitch_e
        0x27 -> :sswitch_f
    .end sparse-switch

    :sswitch_data_9
    .sparse-switch
        0xa -> :sswitch_10
        0xd -> :sswitch_10
        0x20 -> :sswitch_10
        0x65 -> :sswitch_11
    .end sparse-switch

    :sswitch_data_a
    .sparse-switch
        0xa -> :sswitch_1c
        0xd -> :sswitch_1c
        0x20 -> :sswitch_1c
        0x3d -> :sswitch_1d
    .end sparse-switch

    :sswitch_data_b
    .sparse-switch
        0xa -> :sswitch_13
        0xd -> :sswitch_13
        0x20 -> :sswitch_13
        0x30 -> :sswitch_14
        0x31 -> :sswitch_14
        0x32 -> :sswitch_14
        0x33 -> :sswitch_14
        0x34 -> :sswitch_14
        0x35 -> :sswitch_14
        0x36 -> :sswitch_14
        0x37 -> :sswitch_14
        0x38 -> :sswitch_14
        0x39 -> :sswitch_14
    .end sparse-switch

    :sswitch_data_c
    .sparse-switch
        0xa -> :sswitch_15
        0xd -> :sswitch_15
        0x20 -> :sswitch_15
        0x2e -> :sswitch_16
    .end sparse-switch

    :sswitch_data_d
    .sparse-switch
        0xa -> :sswitch_17
        0xd -> :sswitch_17
        0x20 -> :sswitch_17
        0x30 -> :sswitch_18
        0x31 -> :sswitch_18
        0x32 -> :sswitch_18
        0x33 -> :sswitch_18
        0x34 -> :sswitch_18
        0x35 -> :sswitch_18
        0x36 -> :sswitch_18
        0x37 -> :sswitch_18
        0x38 -> :sswitch_18
        0x39 -> :sswitch_18
    .end sparse-switch

    :sswitch_data_e
    .sparse-switch
        0xa -> :sswitch_19
        0xd -> :sswitch_19
        0x20 -> :sswitch_19
        0x22 -> :sswitch_1a
    .end sparse-switch

    :sswitch_data_f
    .sparse-switch
        0xa -> :sswitch_1b
        0xd -> :sswitch_1b
        0x20 -> :sswitch_1b
        0x65 -> :sswitch_11
    .end sparse-switch

    :sswitch_data_10
    .sparse-switch
        0xa -> :sswitch_1e
        0xd -> :sswitch_1e
        0x20 -> :sswitch_1e
        0x22 -> :sswitch_1f
        0x27 -> :sswitch_1f
    .end sparse-switch

    :sswitch_data_11
    .sparse-switch
        0x22 -> :sswitch_25
        0x27 -> :sswitch_20
    .end sparse-switch

    :sswitch_data_12
    .sparse-switch
        0xa -> :sswitch_21
        0xd -> :sswitch_21
        0x20 -> :sswitch_21
        0x61 -> :sswitch_22
        0x62 -> :sswitch_22
        0x63 -> :sswitch_22
        0x64 -> :sswitch_22
        0x65 -> :sswitch_22
        0x66 -> :sswitch_22
        0x67 -> :sswitch_22
        0x68 -> :sswitch_22
        0x69 -> :sswitch_22
        0x6a -> :sswitch_22
        0x6b -> :sswitch_22
        0x6c -> :sswitch_22
        0x6d -> :sswitch_22
        0x6e -> :sswitch_22
        0x6f -> :sswitch_22
        0x70 -> :sswitch_22
        0x71 -> :sswitch_22
        0x72 -> :sswitch_22
        0x73 -> :sswitch_22
        0x74 -> :sswitch_22
        0x75 -> :sswitch_22
        0x76 -> :sswitch_22
        0x77 -> :sswitch_22
        0x78 -> :sswitch_22
        0x79 -> :sswitch_22
        0x7a -> :sswitch_22
    .end sparse-switch

    :sswitch_data_13
    .sparse-switch
        0xa -> :sswitch_23
        0xd -> :sswitch_23
        0x20 -> :sswitch_23
        0x27 -> :sswitch_24
    .end sparse-switch

    :sswitch_data_14
    .sparse-switch
        0xa -> :sswitch_26
        0xd -> :sswitch_26
        0x20 -> :sswitch_26
        0x61 -> :sswitch_27
        0x62 -> :sswitch_27
        0x63 -> :sswitch_27
        0x64 -> :sswitch_27
        0x65 -> :sswitch_27
        0x66 -> :sswitch_27
        0x67 -> :sswitch_27
        0x68 -> :sswitch_27
        0x69 -> :sswitch_27
        0x6a -> :sswitch_27
        0x6b -> :sswitch_27
        0x6c -> :sswitch_27
        0x6d -> :sswitch_27
        0x6e -> :sswitch_27
        0x6f -> :sswitch_27
        0x70 -> :sswitch_27
        0x71 -> :sswitch_27
        0x72 -> :sswitch_27
        0x73 -> :sswitch_27
        0x74 -> :sswitch_27
        0x75 -> :sswitch_27
        0x76 -> :sswitch_27
        0x77 -> :sswitch_27
        0x78 -> :sswitch_27
        0x79 -> :sswitch_27
        0x7a -> :sswitch_27
    .end sparse-switch

    :sswitch_data_15
    .sparse-switch
        0xa -> :sswitch_28
        0xd -> :sswitch_28
        0x20 -> :sswitch_28
        0x22 -> :sswitch_29
    .end sparse-switch
.end method

.method public nextToken()Lantlr/Token;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v7, 0x3c

    const/4 v4, 0x0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->setCommitToPath(Z)V

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->resetText()V

    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    if-ne v5, v7, :cond_1

    sget-object v5, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    const/4 v6, 0x2

    invoke-virtual {p0, v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v6

    invoke-virtual {v5, v6}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mMETA_CONTENT_TYPE(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    :goto_1
    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    if-eqz v5, :cond_0

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    invoke-virtual {v5}, Lantlr/Token;->getType()I

    move-result v1

    invoke-virtual {p0, v1}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->testLiteralsTable(I)I

    move-result v1

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    invoke-virtual {v5, v1}, Lantlr/Token;->setType(I)V

    iget-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    return-object v5

    :cond_1
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    if-ne v5, v7, :cond_2

    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    const/16 v6, 0x3f

    if-ne v5, v6, :cond_2

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->mXML_ENCODING_DECL(Z)V

    iget-object v4, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;

    goto :goto_1

    :cond_2
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->LA(I)C

    move-result v5

    const v6, 0xffff

    if-ne v5, v6, :cond_3

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->uponEOF()V

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->makeToken(I)Lantlr/Token;

    move-result-object v5

    iput-object v5, p0, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->_returnToken:Lantlr/Token;
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lantlr/CharStreamException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v3

    :try_start_1
    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->getCommitToPath()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->consume()V
    :try_end_1
    .catch Lantlr/CharStreamException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    instance-of v5, v2, Lantlr/CharStreamIOException;

    if-eqz v5, :cond_5

    new-instance v5, Lantlr/TokenStreamIOException;

    check-cast v2, Lantlr/CharStreamIOException;

    iget-object v6, v2, Lantlr/CharStreamIOException;->io:Ljava/io/IOException;

    invoke-direct {v5, v6}, Lantlr/TokenStreamIOException;-><init>(Ljava/io/IOException;)V

    throw v5

    :cond_3
    :try_start_2
    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;->consume()V
    :try_end_2
    .catch Lantlr/RecognitionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lantlr/CharStreamException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    :cond_4
    :try_start_3
    new-instance v5, Lantlr/TokenStreamRecognitionException;

    invoke-direct {v5, v3}, Lantlr/TokenStreamRecognitionException;-><init>(Lantlr/RecognitionException;)V

    throw v5
    :try_end_3
    .catch Lantlr/CharStreamException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_5
    new-instance v5, Lantlr/TokenStreamException;

    invoke-virtual {v2}, Lantlr/CharStreamException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lantlr/TokenStreamException;-><init>(Ljava/lang/String;)V

    throw v5
.end method
