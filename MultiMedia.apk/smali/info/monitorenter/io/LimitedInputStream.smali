.class public Linfo/monitorenter/io/LimitedInputStream;
.super Ljava/io/FilterInputStream;
.source "LimitedInputStream.java"


# instance fields
.field protected m_amountOfBytesReadable:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .param p2    # I

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    iput p2, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    return-void
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Ljava/io/FilterInputStream;->available()I

    move-result v0

    iget v1, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    if-ge v1, v0, :cond_0

    iget v0, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    goto :goto_0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    if-nez v1, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    if-ltz v0, :cond_0

    iget v1, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    goto :goto_0
.end method

.method public read([BII)I
    .locals 3
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move v0, p3

    iget v2, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    if-nez v2, :cond_1

    const/4 v1, -0x1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v2, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    if-ge v2, p3, :cond_2

    iget v0, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    :cond_2
    invoke-super {p0, p1, p2, v0}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v1

    if-lez v1, :cond_0

    iget v2, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    sub-int/2addr v2, v1

    iput v2, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    goto :goto_0
.end method

.method public skip(J)J
    .locals 6
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-wide v0, p1

    iget v4, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    if-nez v4, :cond_0

    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2

    :cond_0
    iget v4, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    int-to-long v4, v4

    cmp-long v4, v4, p1

    if-gez v4, :cond_1

    iget v4, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    int-to-long v0, v4

    :cond_1
    invoke-super {p0, p1, p2}, Ljava/io/FilterInputStream;->skip(J)J

    move-result-wide v2

    iget v4, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    int-to-long v4, v4

    sub-long/2addr v4, v2

    long-to-int v4, v4

    iput v4, p0, Linfo/monitorenter/io/LimitedInputStream;->m_amountOfBytesReadable:I

    goto :goto_0
.end method
