.class public Ljcifs/util/Hexdump;
.super Ljava/lang/Object;
.source "Hexdump.java"


# static fields
.field public static final HEX_DIGITS:[C

.field private static final NL:Ljava/lang/String;

.field private static final NL_LENGTH:I

.field private static final SPACE_CHARS:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljcifs/util/Hexdump;->NL:Ljava/lang/String;

    sget-object v0, Ljcifs/util/Hexdump;->NL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Ljcifs/util/Hexdump;->NL_LENGTH:I

    const/16 v0, 0x30

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Ljcifs/util/Hexdump;->SPACE_CHARS:[C

    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Ljcifs/util/Hexdump;->HEX_DIGITS:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
        0x20s
    .end array-data

    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hexdump(Ljava/io/PrintStream;[BII)V
    .locals 12
    .param p0    # Ljava/io/PrintStream;
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    rem-int/lit8 v7, p3, 0x10

    if-nez v7, :cond_3

    div-int/lit8 v6, p3, 0x10

    :goto_1
    sget v9, Ljcifs/util/Hexdump;->NL_LENGTH:I

    add-int/lit8 v9, v9, 0x4a

    mul-int/2addr v9, v6

    new-array v0, v9, [C

    const/16 v9, 0x10

    new-array v3, v9, [C

    const/4 v8, 0x0

    const/4 v1, 0x0

    :cond_1
    const/4 v9, 0x5

    invoke-static {v8, v0, v1, v9}, Ljcifs/util/Hexdump;->toHexChars(I[CII)V

    add-int/lit8 v1, v1, 0x5

    add-int/lit8 v2, v1, 0x1

    const/16 v9, 0x3a

    aput-char v9, v0, v1

    :goto_2
    if-ne v8, p3, :cond_4

    rsub-int/lit8 v5, v7, 0x10

    sget-object v9, Ljcifs/util/Hexdump;->SPACE_CHARS:[C

    const/4 v10, 0x0

    mul-int/lit8 v11, v5, 0x3

    invoke-static {v9, v10, v0, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    mul-int/lit8 v9, v5, 0x3

    add-int v1, v2, v9

    sget-object v9, Ljcifs/util/Hexdump;->SPACE_CHARS:[C

    const/4 v10, 0x0

    invoke-static {v9, v10, v3, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    add-int/lit8 v2, v1, 0x1

    const/16 v9, 0x20

    aput-char v9, v0, v1

    add-int/lit8 v1, v2, 0x1

    const/16 v9, 0x20

    aput-char v9, v0, v2

    add-int/lit8 v2, v1, 0x1

    const/16 v9, 0x7c

    aput-char v9, v0, v1

    const/4 v9, 0x0

    const/16 v10, 0x10

    invoke-static {v3, v9, v0, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v1, v2, 0x10

    add-int/lit8 v2, v1, 0x1

    const/16 v9, 0x7c

    aput-char v9, v0, v1

    sget-object v9, Ljcifs/util/Hexdump;->NL:Ljava/lang/String;

    const/4 v10, 0x0

    sget v11, Ljcifs/util/Hexdump;->NL_LENGTH:I

    invoke-virtual {v9, v10, v11, v0, v2}, Ljava/lang/String;->getChars(II[CI)V

    sget v9, Ljcifs/util/Hexdump;->NL_LENGTH:I

    add-int v1, v2, v9

    if-lt v8, p3, :cond_1

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println([C)V

    goto :goto_0

    :cond_3
    div-int/lit8 v9, p3, 0x10

    add-int/lit8 v6, v9, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v1, v2, 0x1

    const/16 v9, 0x20

    aput-char v9, v0, v2

    add-int v9, p2, v8

    aget-byte v9, p1, v9

    and-int/lit16 v4, v9, 0xff

    const/4 v9, 0x2

    invoke-static {v4, v0, v1, v9}, Ljcifs/util/Hexdump;->toHexChars(I[CII)V

    add-int/lit8 v1, v1, 0x2

    if-ltz v4, :cond_5

    int-to-char v9, v4

    invoke-static {v9}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    rem-int/lit8 v9, v8, 0x10

    const/16 v10, 0x2e

    aput-char v10, v3, v9

    :goto_3
    add-int/lit8 v8, v8, 0x1

    rem-int/lit8 v9, v8, 0x10

    if-eqz v9, :cond_2

    move v2, v1

    goto :goto_2

    :cond_6
    rem-int/lit8 v9, v8, 0x10

    int-to-char v10, v4

    aput-char v10, v3, v9

    goto :goto_3
.end method

.method public static toHexChars(I[CII)V
    .locals 3
    .param p0    # I
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    :goto_0
    if-lez p3, :cond_2

    add-int v1, p2, p3

    add-int/lit8 v0, v1, -0x1

    array-length v1, p1

    if-ge v0, v1, :cond_0

    sget-object v1, Ljcifs/util/Hexdump;->HEX_DIGITS:[C

    and-int/lit8 v2, p0, 0xf

    aget-char v1, v1, v2

    aput-char v1, p1, v0

    :cond_0
    if-eqz p0, :cond_1

    ushr-int/lit8 p0, p0, 0x4

    :cond_1
    add-int/lit8 p3, p3, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static toHexChars(J[CII)V
    .locals 4
    .param p0    # J
    .param p2    # [C
    .param p3    # I
    .param p4    # I

    :goto_0
    if-lez p4, :cond_1

    add-int v0, p3, p4

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Ljcifs/util/Hexdump;->HEX_DIGITS:[C

    const-wide/16 v2, 0xf

    and-long/2addr v2, p0

    long-to-int v2, v2

    aget-char v1, v1, v2

    aput-char v1, p2, v0

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    ushr-long/2addr p0, v0

    :cond_0
    add-int/lit8 p4, p4, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static toHexString(II)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # I

    new-array v0, p1, [C

    const/4 v1, 0x0

    invoke-static {p0, v0, v1, p1}, Ljcifs/util/Hexdump;->toHexChars(I[CII)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public static toHexString(JI)Ljava/lang/String;
    .locals 2
    .param p0    # J
    .param p2    # I

    new-array v0, p2, [C

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljcifs/util/Hexdump;->toHexChars(J[CII)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public static toHexString([BII)Ljava/lang/String;
    .locals 6
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    new-array v0, p2, [C

    rem-int/lit8 v4, p2, 0x2

    if-nez v4, :cond_0

    div-int/lit8 p2, p2, 0x2

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v1, p2, :cond_2

    add-int/lit8 v2, v3, 0x1

    sget-object v4, Ljcifs/util/Hexdump;->HEX_DIGITS:[C

    aget-byte v5, p0, v1

    shr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    array-length v4, v0

    if-ne v2, v4, :cond_1

    :goto_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    return-object v4

    :cond_0
    div-int/lit8 v4, p2, 0x2

    add-int/lit8 p2, v4, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v3, v2, 0x1

    sget-object v4, Ljcifs/util/Hexdump;->HEX_DIGITS:[C

    aget-byte v5, p0, v1

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method
