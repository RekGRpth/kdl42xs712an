.class public abstract Ljcifs/dcerpc/DcerpcHandle;
.super Ljava/lang/Object;
.source "DcerpcHandle.java"

# interfaces
.implements Ljcifs/dcerpc/DcerpcConstants;


# static fields
.field private static call_id:I


# instance fields
.field protected binding:Ljcifs/dcerpc/DcerpcBinding;

.field protected max_recv:I

.field protected max_xmit:I

.field protected securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

.field protected state:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Ljcifs/dcerpc/DcerpcHandle;->call_id:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10b8

    iput v0, p0, Ljcifs/dcerpc/DcerpcHandle;->max_xmit:I

    iget v0, p0, Ljcifs/dcerpc/DcerpcHandle;->max_xmit:I

    iput v0, p0, Ljcifs/dcerpc/DcerpcHandle;->max_recv:I

    const/4 v0, 0x0

    iput v0, p0, Ljcifs/dcerpc/DcerpcHandle;->state:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    return-void
.end method

.method public static getHandle(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/dcerpc/DcerpcHandle;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljcifs/smb/NtlmPasswordAuthentication;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;,
            Ljava/net/MalformedURLException;,
            Ljcifs/dcerpc/DcerpcException;
        }
    .end annotation

    const-string v0, "ncacn_np:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljcifs/dcerpc/DcerpcPipeHandle;

    invoke-direct {v0, p0, p1}, Ljcifs/dcerpc/DcerpcPipeHandle;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    return-object v0

    :cond_0
    new-instance v0, Ljcifs/dcerpc/DcerpcException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DCERPC transport not supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljcifs/dcerpc/DcerpcException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected static parseBinding(Ljava/lang/String;)Ljcifs/dcerpc/DcerpcBinding;
    .locals 13
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/DcerpcException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v7, 0x0

    move v4, v7

    move v8, v7

    :cond_0
    aget-char v2, v0, v7

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    array-length v7, v0

    :cond_1
    :goto_0
    add-int/lit8 v7, v7, 0x1

    array-length v10, v0

    if-lt v7, v10, :cond_0

    if-eqz v1, :cond_2

    iget-object v10, v1, Ljcifs/dcerpc/DcerpcBinding;->endpoint:Ljava/lang/String;

    if-nez v10, :cond_8

    :cond_2
    new-instance v10, Ljcifs/dcerpc/DcerpcException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid binding URL: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljcifs/dcerpc/DcerpcException;-><init>(Ljava/lang/String;)V

    throw v10

    :pswitch_1
    const/16 v10, 0x3a

    if-ne v2, v10, :cond_1

    invoke-virtual {p0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v4, v7, 0x1

    const/4 v8, 0x1

    goto :goto_0

    :pswitch_2
    const/16 v10, 0x5c

    if-ne v2, v10, :cond_3

    add-int/lit8 v4, v7, 0x1

    goto :goto_0

    :cond_3
    const/4 v8, 0x2

    :pswitch_3
    const/16 v10, 0x5b

    if-ne v2, v10, :cond_1

    invoke-virtual {p0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_4

    const-string v6, "127.0.0.1"

    :cond_4
    new-instance v1, Ljcifs/dcerpc/DcerpcBinding;

    invoke-virtual {p0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v5, v10}, Ljcifs/dcerpc/DcerpcBinding;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v7, 0x1

    const/4 v8, 0x5

    goto :goto_0

    :pswitch_4
    const/16 v10, 0x3d

    if-ne v2, v10, :cond_5

    invoke-virtual {p0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v4, v7, 0x1

    goto :goto_0

    :cond_5
    const/16 v10, 0x2c

    if-eq v2, v10, :cond_6

    const/16 v10, 0x5d

    if-ne v2, v10, :cond_1

    :cond_6
    invoke-virtual {p0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    if-nez v3, :cond_7

    const-string v3, "endpoint"

    :cond_7
    invoke-virtual {v1, v3, v9}, Ljcifs/dcerpc/DcerpcBinding;->setOption(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v3, 0x0

    goto :goto_0

    :cond_8
    return-object v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public bind()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/DcerpcException;,
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput v2, p0, Ljcifs/dcerpc/DcerpcHandle;->state:I

    new-instance v0, Ljcifs/dcerpc/DcerpcBind;

    iget-object v2, p0, Ljcifs/dcerpc/DcerpcHandle;->binding:Ljcifs/dcerpc/DcerpcBinding;

    invoke-direct {v0, v2, p0}, Ljcifs/dcerpc/DcerpcBind;-><init>(Ljcifs/dcerpc/DcerpcBinding;Ljcifs/dcerpc/DcerpcHandle;)V

    invoke-virtual {p0, v0}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v1

    const/4 v2, 0x0

    iput v2, p0, Ljcifs/dcerpc/DcerpcHandle;->state:I

    throw v1

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract doReceiveFragment([BZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract doSendFragment([BIIZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getPrincipal()Ljava/security/Principal;
    .locals 1

    instance-of v0, p0, Ljcifs/dcerpc/DcerpcPipeHandle;

    if-eqz v0, :cond_0

    check-cast p0, Ljcifs/dcerpc/DcerpcPipeHandle;

    iget-object v0, p0, Ljcifs/dcerpc/DcerpcPipeHandle;->pipe:Ljcifs/smb/SmbNamedPipe;

    invoke-virtual {v0}, Ljcifs/smb/SmbNamedPipe;->getPrincipal()Ljava/security/Principal;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getServer()Ljava/lang/String;
    .locals 1

    instance-of v0, p0, Ljcifs/dcerpc/DcerpcPipeHandle;

    if-eqz v0, :cond_0

    check-cast p0, Ljcifs/dcerpc/DcerpcPipeHandle;

    iget-object v0, p0, Ljcifs/dcerpc/DcerpcPipeHandle;->pipe:Ljcifs/smb/SmbNamedPipe;

    invoke-virtual {v0}, Ljcifs/smb/SmbNamedPipe;->getServer()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V
    .locals 13
    .param p1    # Ljcifs/dcerpc/DcerpcMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/DcerpcException;,
            Ljava/io/IOException;
        }
    .end annotation

    iget v11, p0, Ljcifs/dcerpc/DcerpcHandle;->state:I

    if-nez v11, :cond_0

    invoke-virtual {p0}, Ljcifs/dcerpc/DcerpcHandle;->bind()V

    :cond_0
    const/4 v4, 0x1

    invoke-static {}, Ljcifs/smb/BufferCache;->getBuffer()[B

    move-result-object v7

    :try_start_0
    new-instance v0, Ljcifs/dcerpc/ndr/NdrBuffer;

    const/4 v11, 0x0

    invoke-direct {v0, v7, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;-><init>([BI)V

    const/4 v11, 0x3

    iput v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    sget v11, Ljcifs/dcerpc/DcerpcHandle;->call_id:I

    add-int/lit8 v12, v11, 0x1

    sput v12, Ljcifs/dcerpc/DcerpcHandle;->call_id:I

    iput v11, p1, Ljcifs/dcerpc/DcerpcMessage;->call_id:I

    invoke-virtual {p1, v0}, Ljcifs/dcerpc/DcerpcMessage;->encode(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    iget-object v11, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    if-eqz v11, :cond_1

    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->setIndex(I)V

    iget-object v11, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    invoke-interface {v11, v0}, Ljcifs/dcerpc/DcerpcSecurityProvider;->wrap(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    :cond_1
    invoke-virtual {v0}, Ljcifs/dcerpc/ndr/NdrBuffer;->getLength()I

    move-result v11

    add-int/lit8 v10, v11, -0x18

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v10, :cond_5

    sub-int v5, v10, v6

    add-int/lit8 v11, v5, 0x18

    iget v12, p0, Ljcifs/dcerpc/DcerpcHandle;->max_xmit:I

    if-le v11, v12, :cond_4

    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    and-int/lit8 v11, v11, -0x3

    iput v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    iget v11, p0, Ljcifs/dcerpc/DcerpcHandle;->max_xmit:I

    add-int/lit8 v5, v11, -0x18

    :goto_1
    add-int/lit8 v11, v5, 0x18

    iput v11, p1, Ljcifs/dcerpc/DcerpcMessage;->length:I

    if-lez v6, :cond_2

    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    and-int/lit8 v11, v11, -0x2

    iput v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    :cond_2
    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    and-int/lit8 v11, v11, 0x3

    const/4 v12, 0x3

    if-eq v11, v12, :cond_3

    iput v6, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->start:I

    invoke-virtual {v0}, Ljcifs/dcerpc/ndr/NdrBuffer;->reset()V

    invoke-virtual {p1, v0}, Ljcifs/dcerpc/DcerpcMessage;->encode_header(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->alloc_hint:I

    invoke-virtual {v0, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    invoke-virtual {p1}, Ljcifs/dcerpc/DcerpcMessage;->getOpnum()I

    move-result v11

    invoke-virtual {v0, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    :cond_3
    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->length:I

    invoke-virtual {p0, v7, v6, v11, v4}, Ljcifs/dcerpc/DcerpcHandle;->doSendFragment([BIIZ)V

    add-int/2addr v6, v5

    goto :goto_0

    :cond_4
    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    or-int/lit8 v11, v11, 0x2

    iput v11, p1, Ljcifs/dcerpc/DcerpcMessage;->flags:I

    const/4 v4, 0x0

    iput v5, p1, Ljcifs/dcerpc/DcerpcMessage;->alloc_hint:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v11

    invoke-static {v7}, Ljcifs/smb/BufferCache;->releaseBuffer([B)V

    throw v11

    :cond_5
    :try_start_1
    invoke-virtual {p0, v7, v4}, Ljcifs/dcerpc/DcerpcHandle;->doReceiveFragment([BZ)V

    invoke-virtual {v0}, Ljcifs/dcerpc/ndr/NdrBuffer;->reset()V

    const/16 v11, 0x8

    invoke-virtual {v0, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->setIndex(I)V

    invoke-virtual {v0}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v11

    invoke-virtual {v0, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->setLength(I)V

    iget-object v11, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    if-eqz v11, :cond_6

    iget-object v11, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    invoke-interface {v11, v0}, Ljcifs/dcerpc/DcerpcSecurityProvider;->unwrap(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    :cond_6
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->setIndex(I)V

    invoke-virtual {p1, v0}, Ljcifs/dcerpc/DcerpcMessage;->decode_header(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    const/16 v6, 0x18

    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->ptype:I

    const/4 v12, 0x2

    if-ne v11, v12, :cond_7

    const/4 v11, 0x2

    invoke-virtual {p1, v11}, Ljcifs/dcerpc/DcerpcMessage;->isFlagSet(I)Z

    move-result v11

    if-nez v11, :cond_7

    iget v6, p1, Ljcifs/dcerpc/DcerpcMessage;->length:I

    :cond_7
    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_2
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, Ljcifs/dcerpc/DcerpcMessage;->isFlagSet(I)Z

    move-result v11

    if-nez v11, :cond_b

    if-nez v3, :cond_8

    iget v11, p0, Ljcifs/dcerpc/DcerpcHandle;->max_recv:I

    new-array v3, v11, [B

    new-instance v2, Ljcifs/dcerpc/ndr/NdrBuffer;

    const/4 v11, 0x0

    invoke-direct {v2, v3, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;-><init>([BI)V

    :cond_8
    invoke-virtual {p0, v3, v4}, Ljcifs/dcerpc/DcerpcHandle;->doReceiveFragment([BZ)V

    invoke-virtual {v2}, Ljcifs/dcerpc/ndr/NdrBuffer;->reset()V

    const/16 v11, 0x8

    invoke-virtual {v2, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->setIndex(I)V

    invoke-virtual {v2}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v11

    invoke-virtual {v2, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->setLength(I)V

    iget-object v11, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    if-eqz v11, :cond_9

    iget-object v11, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    invoke-interface {v11, v2}, Ljcifs/dcerpc/DcerpcSecurityProvider;->unwrap(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    :cond_9
    invoke-virtual {v2}, Ljcifs/dcerpc/ndr/NdrBuffer;->reset()V

    invoke-virtual {p1, v2}, Ljcifs/dcerpc/DcerpcMessage;->decode_header(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    iget v11, p1, Ljcifs/dcerpc/DcerpcMessage;->length:I

    add-int/lit8 v8, v11, -0x18

    add-int v11, v6, v8

    array-length v12, v7

    if-le v11, v12, :cond_a

    add-int v11, v6, v8

    new-array v9, v11, [B

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v7, v11, v9, v12, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v7, v9

    :cond_a
    const/16 v11, 0x18

    invoke-static {v3, v11, v7, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v6, v8

    goto :goto_2

    :cond_b
    new-instance v0, Ljcifs/dcerpc/ndr/NdrBuffer;

    const/4 v11, 0x0

    invoke-direct {v0, v7, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;-><init>([BI)V

    invoke-virtual {p1, v0}, Ljcifs/dcerpc/DcerpcMessage;->decode(Ljcifs/dcerpc/ndr/NdrBuffer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v7}, Ljcifs/smb/BufferCache;->releaseBuffer([B)V

    invoke-virtual {p1}, Ljcifs/dcerpc/DcerpcMessage;->getResult()Ljcifs/dcerpc/DcerpcException;

    move-result-object v1

    if-eqz v1, :cond_c

    throw v1

    :cond_c
    return-void
.end method

.method public setDcerpcSecurityProvider(Ljcifs/dcerpc/DcerpcSecurityProvider;)V
    .locals 0
    .param p1    # Ljcifs/dcerpc/DcerpcSecurityProvider;

    iput-object p1, p0, Ljcifs/dcerpc/DcerpcHandle;->securityProvider:Ljcifs/dcerpc/DcerpcSecurityProvider;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcifs/dcerpc/DcerpcHandle;->binding:Ljcifs/dcerpc/DcerpcBinding;

    invoke-virtual {v0}, Ljcifs/dcerpc/DcerpcBinding;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
