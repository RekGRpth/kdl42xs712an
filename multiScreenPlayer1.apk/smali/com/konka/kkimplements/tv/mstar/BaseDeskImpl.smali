.class public Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.super Ljava/lang/Object;
.source "BaseDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# instance fields
.field private final max_handler:I

.field pHandler:[Landroid/os/Handler;


# direct methods
.method protected constructor <init>()V
    .locals 4

    const/16 v3, 0xa

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->max_handler:I

    new-array v1, v3, [Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->pHandler:[Landroid/os/Handler;

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v3, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->pHandler:[Landroid/os/Handler;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getHandler(I)Landroid/os/Handler;
    .locals 1
    .param p1    # I

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->pHandler:[Landroid/os/Handler;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseHandler(I)V
    .locals 2
    .param p1    # I

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->pHandler:[Landroid/os/Handler;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    :cond_0
    return-void
.end method

.method public setHandler(Landroid/os/Handler;I)Z
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/16 v0, 0xa

    if-ge p2, v0, :cond_1

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->pHandler:[Landroid/os/Handler;

    aget-object v0, v0, p2

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->pHandler:[Landroid/os/Handler;

    aput-object p1, v0, p2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
