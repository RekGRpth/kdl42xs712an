.class public Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "SettingDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/SettingDesk;


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumLanguage:[I

.field private static menuTimeOut:I

.field private static settingMgrImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;


# instance fields
.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumLanguage()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumLanguage:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ABKHAZIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x34

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1f3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x35

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1f2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACOLI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x36

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1f1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x31

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1f0

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ADANGME:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x37

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1ef

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ADYGHE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x38

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1ee

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AFAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x39

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1ed

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AFRIHILI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x3a

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1ec

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AFRIKAANS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x3b

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1eb

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AFRO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x3c

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1ea

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AINU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x3d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e9

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AKAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x3e

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1e8

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AKKADIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x3f

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1e7

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ALBANIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x40

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1e6

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ALEUT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x41

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1e5

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ALGONQUIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x42

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1e4

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ALTAIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x44

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_1e3

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ALTAISOUTHERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x43

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_1e2

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AMHARIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x45

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_1e1

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ANGIKA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa6

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_1e0

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_APACHE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x46

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_1df

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARABIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_1de

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARAGONESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x48

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_1dd

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARAMAIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x47

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_1dc

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARAPAHO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x49

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_1db

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARAUCANIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x4a

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_1da

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARAWAK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x4b

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_1d9

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARMENIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x4c

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_1d8

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AROMANIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x17b

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_1d7

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ARTIFICIAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x4d

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_1d6

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ASSAMESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x4e

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_1d5

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ASTURIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x4f

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_1d4

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ATHAPASCAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x50

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_1d3

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AUSTRALIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x52

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_1d2

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AUSTRONESIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x51

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_1d1

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AVARIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x53

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_1d0

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AVESTAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x54

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_1cf

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AWADHI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x55

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_1ce

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AYMARA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x56

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_1cd

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AZERBAIJANI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x57

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1cc

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_AZTEC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x58

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_1cb

    :goto_29
    :try_start_29
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BALINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x59

    aput v2, v0, v1
    :try_end_29
    .catch Ljava/lang/NoSuchFieldError; {:try_start_29 .. :try_end_29} :catch_1ca

    :goto_2a
    :try_start_2a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BALTIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x5a

    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_2a} :catch_1c9

    :goto_2b
    :try_start_2b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BALUCHI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x5b

    aput v2, v0, v1
    :try_end_2b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2b .. :try_end_2b} :catch_1c8

    :goto_2c
    :try_start_2c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BAMBARA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x5c

    aput v2, v0, v1
    :try_end_2c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2c .. :try_end_2c} :catch_1c7

    :goto_2d
    :try_start_2d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BAMILEKE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x5d

    aput v2, v0, v1
    :try_end_2d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2d .. :try_end_2d} :catch_1c6

    :goto_2e
    :try_start_2e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BANDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x5e

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2e .. :try_end_2e} :catch_1c5

    :goto_2f
    :try_start_2f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BANTU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x5f

    aput v2, v0, v1
    :try_end_2f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2f .. :try_end_2f} :catch_1c4

    :goto_30
    :try_start_30
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BASA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x60

    aput v2, v0, v1
    :try_end_30
    .catch Ljava/lang/NoSuchFieldError; {:try_start_30 .. :try_end_30} :catch_1c3

    :goto_31
    :try_start_31
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BASHKIR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x61

    aput v2, v0, v1
    :try_end_31
    .catch Ljava/lang/NoSuchFieldError; {:try_start_31 .. :try_end_31} :catch_1c2

    :goto_32
    :try_start_32
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BASQUE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x62

    aput v2, v0, v1
    :try_end_32
    .catch Ljava/lang/NoSuchFieldError; {:try_start_32 .. :try_end_32} :catch_1c1

    :goto_33
    :try_start_33
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BEJA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x63

    aput v2, v0, v1
    :try_end_33
    .catch Ljava/lang/NoSuchFieldError; {:try_start_33 .. :try_end_33} :catch_1c0

    :goto_34
    :try_start_34
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BEMBA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x64

    aput v2, v0, v1
    :try_end_34
    .catch Ljava/lang/NoSuchFieldError; {:try_start_34 .. :try_end_34} :catch_1bf

    :goto_35
    :try_start_35
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BENGALI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x65

    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_35} :catch_1be

    :goto_36
    :try_start_36
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BERBER:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x66

    aput v2, v0, v1
    :try_end_36
    .catch Ljava/lang/NoSuchFieldError; {:try_start_36 .. :try_end_36} :catch_1bd

    :goto_37
    :try_start_37
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BHOJPURI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x67

    aput v2, v0, v1
    :try_end_37
    .catch Ljava/lang/NoSuchFieldError; {:try_start_37 .. :try_end_37} :catch_1bc

    :goto_38
    :try_start_38
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BIHARI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x68

    aput v2, v0, v1
    :try_end_38
    .catch Ljava/lang/NoSuchFieldError; {:try_start_38 .. :try_end_38} :catch_1bb

    :goto_39
    :try_start_39
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BIKOL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x69

    aput v2, v0, v1
    :try_end_39
    .catch Ljava/lang/NoSuchFieldError; {:try_start_39 .. :try_end_39} :catch_1ba

    :goto_3a
    :try_start_3a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BINI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x6a

    aput v2, v0, v1
    :try_end_3a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3a .. :try_end_3a} :catch_1b9

    :goto_3b
    :try_start_3b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BISLAMA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x6b

    aput v2, v0, v1
    :try_end_3b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3b .. :try_end_3b} :catch_1b8

    :goto_3c
    :try_start_3c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BLIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x72

    aput v2, v0, v1
    :try_end_3c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3c .. :try_end_3c} :catch_1b7

    :goto_3d
    :try_start_3d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BLISSYMBOLICS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ec

    aput v2, v0, v1
    :try_end_3d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3d .. :try_end_3d} :catch_1b6

    :goto_3e
    :try_start_3e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BOSNIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x6c

    aput v2, v0, v1
    :try_end_3e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3e .. :try_end_3e} :catch_1b5

    :goto_3f
    :try_start_3f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BRAJ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x6d

    aput v2, v0, v1
    :try_end_3f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3f .. :try_end_3f} :catch_1b4

    :goto_40
    :try_start_40
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BRETON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x6e

    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_40} :catch_1b3

    :goto_41
    :try_start_41
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BUGINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x6f

    aput v2, v0, v1
    :try_end_41
    .catch Ljava/lang/NoSuchFieldError; {:try_start_41 .. :try_end_41} :catch_1b2

    :goto_42
    :try_start_42
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BULGARIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_42
    .catch Ljava/lang/NoSuchFieldError; {:try_start_42 .. :try_end_42} :catch_1b1

    :goto_43
    :try_start_43
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BURIAT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x70

    aput v2, v0, v1
    :try_end_43
    .catch Ljava/lang/NoSuchFieldError; {:try_start_43 .. :try_end_43} :catch_1b0

    :goto_44
    :try_start_44
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BURMESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x71

    aput v2, v0, v1
    :try_end_44
    .catch Ljava/lang/NoSuchFieldError; {:try_start_44 .. :try_end_44} :catch_1af

    :goto_45
    :try_start_45
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_BYELORUSSIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x73

    aput v2, v0, v1
    :try_end_45
    .catch Ljava/lang/NoSuchFieldError; {:try_start_45 .. :try_end_45} :catch_1ae

    :goto_46
    :try_start_46
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CADDO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x74

    aput v2, v0, v1
    :try_end_46
    .catch Ljava/lang/NoSuchFieldError; {:try_start_46 .. :try_end_46} :catch_1ad

    :goto_47
    :try_start_47
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CANTONESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x2d

    aput v2, v0, v1
    :try_end_47
    .catch Ljava/lang/NoSuchFieldError; {:try_start_47 .. :try_end_47} :catch_1ac

    :goto_48
    :try_start_48
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CARIB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x75

    aput v2, v0, v1
    :try_end_48
    .catch Ljava/lang/NoSuchFieldError; {:try_start_48 .. :try_end_48} :catch_1ab

    :goto_49
    :try_start_49
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CATALAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x76

    aput v2, v0, v1
    :try_end_49
    .catch Ljava/lang/NoSuchFieldError; {:try_start_49 .. :try_end_49} :catch_1aa

    :goto_4a
    :try_start_4a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CAUCASIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x77

    aput v2, v0, v1
    :try_end_4a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4a .. :try_end_4a} :catch_1a9

    :goto_4b
    :try_start_4b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CEBUANO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x78

    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_4b} :catch_1a8

    :goto_4c
    :try_start_4c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CELTIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x79

    aput v2, v0, v1
    :try_end_4c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4c .. :try_end_4c} :catch_1a7

    :goto_4d
    :try_start_4d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CENTRALAMERICANINDIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x7a

    aput v2, v0, v1
    :try_end_4d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4d .. :try_end_4d} :catch_1a6

    :goto_4e
    :try_start_4e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHAGATAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x7b

    aput v2, v0, v1
    :try_end_4e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4e .. :try_end_4e} :catch_1a5

    :goto_4f
    :try_start_4f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHAMIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4f .. :try_end_4f} :catch_1a4

    :goto_50
    :try_start_50
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHAMORRO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x7c

    aput v2, v0, v1
    :try_end_50
    .catch Ljava/lang/NoSuchFieldError; {:try_start_50 .. :try_end_50} :catch_1a3

    :goto_51
    :try_start_51
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHECHEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x7d

    aput v2, v0, v1
    :try_end_51
    .catch Ljava/lang/NoSuchFieldError; {:try_start_51 .. :try_end_51} :catch_1a2

    :goto_52
    :try_start_52
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHEROKEE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x7e

    aput v2, v0, v1
    :try_end_52
    .catch Ljava/lang/NoSuchFieldError; {:try_start_52 .. :try_end_52} :catch_1a1

    :goto_53
    :try_start_53
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHEYENNE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x7f

    aput v2, v0, v1
    :try_end_53
    .catch Ljava/lang/NoSuchFieldError; {:try_start_53 .. :try_end_53} :catch_1a0

    :goto_54
    :try_start_54
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHIBCHA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x80

    aput v2, v0, v1
    :try_end_54
    .catch Ljava/lang/NoSuchFieldError; {:try_start_54 .. :try_end_54} :catch_19f

    :goto_55
    :try_start_55
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_55
    .catch Ljava/lang/NoSuchFieldError; {:try_start_55 .. :try_end_55} :catch_19e

    :goto_56
    :try_start_56
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINOOKJARGON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x81

    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_56} :catch_19d

    :goto_57
    :try_start_57
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHIPEWYAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x83

    aput v2, v0, v1
    :try_end_57
    .catch Ljava/lang/NoSuchFieldError; {:try_start_57 .. :try_end_57} :catch_19c

    :goto_58
    :try_start_58
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHOCTAW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x82

    aput v2, v0, v1
    :try_end_58
    .catch Ljava/lang/NoSuchFieldError; {:try_start_58 .. :try_end_58} :catch_19b

    :goto_59
    :try_start_59
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHURCHSLAVIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x84

    aput v2, v0, v1
    :try_end_59
    .catch Ljava/lang/NoSuchFieldError; {:try_start_59 .. :try_end_59} :catch_19a

    :goto_5a
    :try_start_5a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHUUKESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_5a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5a .. :try_end_5a} :catch_199

    :goto_5b
    :try_start_5b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHUVASH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x85

    aput v2, v0, v1
    :try_end_5b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5b .. :try_end_5b} :catch_198

    :goto_5c
    :try_start_5c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CIRCASSIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf8

    aput v2, v0, v1
    :try_end_5c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5c .. :try_end_5c} :catch_197

    :goto_5d
    :try_start_5d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_COPTIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x86

    aput v2, v0, v1
    :try_end_5d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5d .. :try_end_5d} :catch_196

    :goto_5e
    :try_start_5e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CORNISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x87

    aput v2, v0, v1
    :try_end_5e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5e .. :try_end_5e} :catch_195

    :goto_5f
    :try_start_5f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CORSICAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x88

    aput v2, v0, v1
    :try_end_5f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5f .. :try_end_5f} :catch_194

    :goto_60
    :try_start_60
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CREE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x89

    aput v2, v0, v1
    :try_end_60
    .catch Ljava/lang/NoSuchFieldError; {:try_start_60 .. :try_end_60} :catch_193

    :goto_61
    :try_start_61
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CREEK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8a

    aput v2, v0, v1
    :try_end_61
    .catch Ljava/lang/NoSuchFieldError; {:try_start_61 .. :try_end_61} :catch_192

    :goto_62
    :try_start_62
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CREOLESANDPIDGINS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8b

    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_62 .. :try_end_62} :catch_191

    :goto_63
    :try_start_63
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CREOLESANDPIDGINSENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8d

    aput v2, v0, v1
    :try_end_63
    .catch Ljava/lang/NoSuchFieldError; {:try_start_63 .. :try_end_63} :catch_190

    :goto_64
    :try_start_64
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CREOLESANDPIDGINSFRENCH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8e

    aput v2, v0, v1
    :try_end_64
    .catch Ljava/lang/NoSuchFieldError; {:try_start_64 .. :try_end_64} :catch_18f

    :goto_65
    :try_start_65
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CREOLESANDPIDGINSPORTUGUESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8f

    aput v2, v0, v1
    :try_end_65
    .catch Ljava/lang/NoSuchFieldError; {:try_start_65 .. :try_end_65} :catch_18e

    :goto_66
    :try_start_66
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CRIMEANTATAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8c

    aput v2, v0, v1
    :try_end_66
    .catch Ljava/lang/NoSuchFieldError; {:try_start_66 .. :try_end_66} :catch_18d

    :goto_67
    :try_start_67
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CROATIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_67
    .catch Ljava/lang/NoSuchFieldError; {:try_start_67 .. :try_end_67} :catch_18c

    :goto_68
    :try_start_68
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CUSHITIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x91

    aput v2, v0, v1
    :try_end_68
    .catch Ljava/lang/NoSuchFieldError; {:try_start_68 .. :try_end_68} :catch_18b

    :goto_69
    :try_start_69
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CZECH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_69
    .catch Ljava/lang/NoSuchFieldError; {:try_start_69 .. :try_end_69} :catch_18a

    :goto_6a
    :try_start_6a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DAKOTA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x92

    aput v2, v0, v1
    :try_end_6a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6a .. :try_end_6a} :catch_189

    :goto_6b
    :try_start_6b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DANISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6b .. :try_end_6b} :catch_188

    :goto_6c
    :try_start_6c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DARGWA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6c .. :try_end_6c} :catch_187

    :goto_6d
    :try_start_6d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DELAWARE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x93

    aput v2, v0, v1
    :try_end_6d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6d .. :try_end_6d} :catch_186

    :goto_6e
    :try_start_6e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DHIVEHI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x96

    aput v2, v0, v1
    :try_end_6e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6e .. :try_end_6e} :catch_185

    :goto_6f
    :try_start_6f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DINKA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x97

    aput v2, v0, v1
    :try_end_6f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6f .. :try_end_6f} :catch_184

    :goto_70
    :try_start_70
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DIVEHI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x98

    aput v2, v0, v1
    :try_end_70
    .catch Ljava/lang/NoSuchFieldError; {:try_start_70 .. :try_end_70} :catch_183

    :goto_71
    :try_start_71
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DOGRI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x99

    aput v2, v0, v1
    :try_end_71
    .catch Ljava/lang/NoSuchFieldError; {:try_start_71 .. :try_end_71} :catch_182

    :goto_72
    :try_start_72
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DOGRIB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x95

    aput v2, v0, v1
    :try_end_72
    .catch Ljava/lang/NoSuchFieldError; {:try_start_72 .. :try_end_72} :catch_181

    :goto_73
    :try_start_73
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DRAVIDIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9a

    aput v2, v0, v1
    :try_end_73
    .catch Ljava/lang/NoSuchFieldError; {:try_start_73 .. :try_end_73} :catch_180

    :goto_74
    :try_start_74
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DUALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9c

    aput v2, v0, v1
    :try_end_74
    .catch Ljava/lang/NoSuchFieldError; {:try_start_74 .. :try_end_74} :catch_17f

    :goto_75
    :try_start_75
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DUTCH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_75
    .catch Ljava/lang/NoSuchFieldError; {:try_start_75 .. :try_end_75} :catch_17e

    :goto_76
    :try_start_76
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DUTCHMIDDLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9d

    aput v2, v0, v1
    :try_end_76
    .catch Ljava/lang/NoSuchFieldError; {:try_start_76 .. :try_end_76} :catch_17d

    :goto_77
    :try_start_77
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DYULA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9e

    aput v2, v0, v1
    :try_end_77
    .catch Ljava/lang/NoSuchFieldError; {:try_start_77 .. :try_end_77} :catch_17c

    :goto_78
    :try_start_78
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_DZONGKHA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9f

    aput v2, v0, v1
    :try_end_78
    .catch Ljava/lang/NoSuchFieldError; {:try_start_78 .. :try_end_78} :catch_17b

    :goto_79
    :try_start_79
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_EFIK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa0

    aput v2, v0, v1
    :try_end_79
    .catch Ljava/lang/NoSuchFieldError; {:try_start_79 .. :try_end_79} :catch_17a

    :goto_7a
    :try_start_7a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_EGYPTIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa1

    aput v2, v0, v1
    :try_end_7a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7a .. :try_end_7a} :catch_179

    :goto_7b
    :try_start_7b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_EKAJUK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa2

    aput v2, v0, v1
    :try_end_7b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7b .. :try_end_7b} :catch_178

    :goto_7c
    :try_start_7c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ELAMITE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa3

    aput v2, v0, v1
    :try_end_7c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7c .. :try_end_7c} :catch_177

    :goto_7d
    :try_start_7d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_7d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7d .. :try_end_7d} :catch_176

    :goto_7e
    :try_start_7e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISHMIDDLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa4

    aput v2, v0, v1
    :try_end_7e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7e .. :try_end_7e} :catch_175

    :goto_7f
    :try_start_7f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISHOLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa5

    aput v2, v0, v1
    :try_end_7f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7f .. :try_end_7f} :catch_174

    :goto_80
    :try_start_80
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ERZYA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x137

    aput v2, v0, v1
    :try_end_80
    .catch Ljava/lang/NoSuchFieldError; {:try_start_80 .. :try_end_80} :catch_173

    :goto_81
    :try_start_81
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ESKIMO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa7

    aput v2, v0, v1
    :try_end_81
    .catch Ljava/lang/NoSuchFieldError; {:try_start_81 .. :try_end_81} :catch_172

    :goto_82
    :try_start_82
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ESPERANTO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa8

    aput v2, v0, v1
    :try_end_82
    .catch Ljava/lang/NoSuchFieldError; {:try_start_82 .. :try_end_82} :catch_171

    :goto_83
    :try_start_83
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ESTONIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_83
    .catch Ljava/lang/NoSuchFieldError; {:try_start_83 .. :try_end_83} :catch_170

    :goto_84
    :try_start_84
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_EWE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa9

    aput v2, v0, v1
    :try_end_84
    .catch Ljava/lang/NoSuchFieldError; {:try_start_84 .. :try_end_84} :catch_16f

    :goto_85
    :try_start_85
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_EWONDO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xaa

    aput v2, v0, v1
    :try_end_85
    .catch Ljava/lang/NoSuchFieldError; {:try_start_85 .. :try_end_85} :catch_16e

    :goto_86
    :try_start_86
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FANG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xab

    aput v2, v0, v1
    :try_end_86
    .catch Ljava/lang/NoSuchFieldError; {:try_start_86 .. :try_end_86} :catch_16d

    :goto_87
    :try_start_87
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FANTI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xac

    aput v2, v0, v1
    :try_end_87
    .catch Ljava/lang/NoSuchFieldError; {:try_start_87 .. :try_end_87} :catch_16c

    :goto_88
    :try_start_88
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FAROESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xad

    aput v2, v0, v1
    :try_end_88
    .catch Ljava/lang/NoSuchFieldError; {:try_start_88 .. :try_end_88} :catch_16b

    :goto_89
    :try_start_89
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FIJIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xae

    aput v2, v0, v1
    :try_end_89
    .catch Ljava/lang/NoSuchFieldError; {:try_start_89 .. :try_end_89} :catch_16a

    :goto_8a
    :try_start_8a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FILIPINO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xaf

    aput v2, v0, v1
    :try_end_8a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8a .. :try_end_8a} :catch_169

    :goto_8b
    :try_start_8b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FINNISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_8b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8b .. :try_end_8b} :catch_168

    :goto_8c
    :try_start_8c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FINNOUGRIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb0

    aput v2, v0, v1
    :try_end_8c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8c .. :try_end_8c} :catch_167

    :goto_8d
    :try_start_8d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb1

    aput v2, v0, v1
    :try_end_8d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8d .. :try_end_8d} :catch_166

    :goto_8e
    :try_start_8e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FRENCH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8e .. :try_end_8e} :catch_165

    :goto_8f
    :try_start_8f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FRENCHMIDDLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb2

    aput v2, v0, v1
    :try_end_8f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8f .. :try_end_8f} :catch_164

    :goto_90
    :try_start_90
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FRENCHOLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb3

    aput v2, v0, v1
    :try_end_90
    .catch Ljava/lang/NoSuchFieldError; {:try_start_90 .. :try_end_90} :catch_163

    :goto_91
    :try_start_91
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FRISIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb6

    aput v2, v0, v1
    :try_end_91
    .catch Ljava/lang/NoSuchFieldError; {:try_start_91 .. :try_end_91} :catch_162

    :goto_92
    :try_start_92
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FRISIANEASTERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb5

    aput v2, v0, v1
    :try_end_92
    .catch Ljava/lang/NoSuchFieldError; {:try_start_92 .. :try_end_92} :catch_161

    :goto_93
    :try_start_93
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FRISIANNORTHERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb4

    aput v2, v0, v1
    :try_end_93
    .catch Ljava/lang/NoSuchFieldError; {:try_start_93 .. :try_end_93} :catch_160

    :goto_94
    :try_start_94
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FRIULIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb8

    aput v2, v0, v1
    :try_end_94
    .catch Ljava/lang/NoSuchFieldError; {:try_start_94 .. :try_end_94} :catch_15f

    :goto_95
    :try_start_95
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_FULAH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb7

    aput v2, v0, v1
    :try_end_95
    .catch Ljava/lang/NoSuchFieldError; {:try_start_95 .. :try_end_95} :catch_15e

    :goto_96
    :try_start_96
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xb9

    aput v2, v0, v1
    :try_end_96
    .catch Ljava/lang/NoSuchFieldError; {:try_start_96 .. :try_end_96} :catch_15d

    :goto_97
    :try_start_97
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GAELIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_97
    .catch Ljava/lang/NoSuchFieldError; {:try_start_97 .. :try_end_97} :catch_15c

    :goto_98
    :try_start_98
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GALLEGAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xba

    aput v2, v0, v1
    :try_end_98
    .catch Ljava/lang/NoSuchFieldError; {:try_start_98 .. :try_end_98} :catch_15b

    :goto_99
    :try_start_99
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GANDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xbb

    aput v2, v0, v1
    :try_end_99
    .catch Ljava/lang/NoSuchFieldError; {:try_start_99 .. :try_end_99} :catch_15a

    :goto_9a
    :try_start_9a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GAYO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xbc

    aput v2, v0, v1
    :try_end_9a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9a .. :try_end_9a} :catch_159

    :goto_9b
    :try_start_9b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GBAYA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_9b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9b .. :try_end_9b} :catch_158

    :goto_9c
    :try_start_9c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GEEZ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xbd

    aput v2, v0, v1
    :try_end_9c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9c .. :try_end_9c} :catch_157

    :goto_9d
    :try_start_9d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GEORGIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xbe

    aput v2, v0, v1
    :try_end_9d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9d .. :try_end_9d} :catch_156

    :goto_9e
    :try_start_9e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GERMAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_9e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9e .. :try_end_9e} :catch_155

    :goto_9f
    :try_start_9f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GERMANIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc2

    aput v2, v0, v1
    :try_end_9f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9f .. :try_end_9f} :catch_154

    :goto_a0
    :try_start_a0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GERMANLOW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14a

    aput v2, v0, v1
    :try_end_a0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a0 .. :try_end_a0} :catch_153

    :goto_a1
    :try_start_a1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GERMANMIDDLEHIGH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xbf

    aput v2, v0, v1
    :try_end_a1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a1 .. :try_end_a1} :catch_152

    :goto_a2
    :try_start_a2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GERMANOLDHIGH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc0

    aput v2, v0, v1
    :try_end_a2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a2 .. :try_end_a2} :catch_151

    :goto_a3
    :try_start_a3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GILBERTESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc3

    aput v2, v0, v1
    :try_end_a3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a3 .. :try_end_a3} :catch_150

    :goto_a4
    :try_start_a4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GONDI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc4

    aput v2, v0, v1
    :try_end_a4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a4 .. :try_end_a4} :catch_14f

    :goto_a5
    :try_start_a5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GORONTALO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc1

    aput v2, v0, v1
    :try_end_a5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a5 .. :try_end_a5} :catch_14e

    :goto_a6
    :try_start_a6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GOTHIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc5

    aput v2, v0, v1
    :try_end_a6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a6 .. :try_end_a6} :catch_14d

    :goto_a7
    :try_start_a7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GREBO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc6

    aput v2, v0, v1
    :try_end_a7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a7 .. :try_end_a7} :catch_14c

    :goto_a8
    :try_start_a8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GREEK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a8 .. :try_end_a8} :catch_14b

    :goto_a9
    :try_start_a9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GREEKANCIENT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc7

    aput v2, v0, v1
    :try_end_a9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a9 .. :try_end_a9} :catch_14a

    :goto_aa
    :try_start_aa
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GREENLANDIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc8

    aput v2, v0, v1
    :try_end_aa
    .catch Ljava/lang/NoSuchFieldError; {:try_start_aa .. :try_end_aa} :catch_149

    :goto_ab
    :try_start_ab
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GUARANI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc9

    aput v2, v0, v1
    :try_end_ab
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ab .. :try_end_ab} :catch_148

    :goto_ac
    :try_start_ac
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GUJARATI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xcb

    aput v2, v0, v1
    :try_end_ac
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ac .. :try_end_ac} :catch_147

    :goto_ad
    :try_start_ad
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_GWICHIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xcc

    aput v2, v0, v1
    :try_end_ad
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ad .. :try_end_ad} :catch_146

    :goto_ae
    :try_start_ae
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HAIDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xcd

    aput v2, v0, v1
    :try_end_ae
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ae .. :try_end_ae} :catch_145

    :goto_af
    :try_start_af
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HAITIANCREOLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xce

    aput v2, v0, v1
    :try_end_af
    .catch Ljava/lang/NoSuchFieldError; {:try_start_af .. :try_end_af} :catch_144

    :goto_b0
    :try_start_b0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HAUSA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xcf

    aput v2, v0, v1
    :try_end_b0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b0 .. :try_end_b0} :catch_143

    :goto_b1
    :try_start_b1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HAWAIIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd0

    aput v2, v0, v1
    :try_end_b1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b1 .. :try_end_b1} :catch_142

    :goto_b2
    :try_start_b2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HEBREW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_b2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b2 .. :try_end_b2} :catch_141

    :goto_b3
    :try_start_b3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HERERO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd1

    aput v2, v0, v1
    :try_end_b3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b3 .. :try_end_b3} :catch_140

    :goto_b4
    :try_start_b4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HILIGAYNON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd2

    aput v2, v0, v1
    :try_end_b4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b4 .. :try_end_b4} :catch_13f

    :goto_b5
    :try_start_b5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HIMACHALI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd3

    aput v2, v0, v1
    :try_end_b5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b5 .. :try_end_b5} :catch_13e

    :goto_b6
    :try_start_b6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HINDI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_b6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b6 .. :try_end_b6} :catch_13d

    :goto_b7
    :try_start_b7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HIRIMOTU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd4

    aput v2, v0, v1
    :try_end_b7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b7 .. :try_end_b7} :catch_13c

    :goto_b8
    :try_start_b8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HITTITE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x2a

    aput v2, v0, v1
    :try_end_b8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b8 .. :try_end_b8} :catch_13b

    :goto_b9
    :try_start_b9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HMONG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x2b

    aput v2, v0, v1
    :try_end_b9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b9 .. :try_end_b9} :catch_13a

    :goto_ba
    :try_start_ba
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HUNGARIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_ba
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ba .. :try_end_ba} :catch_139

    :goto_bb
    :try_start_bb
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_HUPA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd5

    aput v2, v0, v1
    :try_end_bb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_bb .. :try_end_bb} :catch_138

    :goto_bc
    :try_start_bc
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IBAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd6

    aput v2, v0, v1
    :try_end_bc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_bc .. :try_end_bc} :catch_137

    :goto_bd
    :try_start_bd
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ICELANDIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd7

    aput v2, v0, v1
    :try_end_bd
    .catch Ljava/lang/NoSuchFieldError; {:try_start_bd .. :try_end_bd} :catch_136

    :goto_be
    :try_start_be
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IDO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd8

    aput v2, v0, v1
    :try_end_be
    .catch Ljava/lang/NoSuchFieldError; {:try_start_be .. :try_end_be} :catch_135

    :goto_bf
    :try_start_bf
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IGBO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xda

    aput v2, v0, v1
    :try_end_bf
    .catch Ljava/lang/NoSuchFieldError; {:try_start_bf .. :try_end_bf} :catch_134

    :goto_c0
    :try_start_c0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IJO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xdb

    aput v2, v0, v1
    :try_end_c0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c0 .. :try_end_c0} :catch_133

    :goto_c1
    :try_start_c1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ILOKO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xdc

    aput v2, v0, v1
    :try_end_c1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c1 .. :try_end_c1} :catch_132

    :goto_c2
    :try_start_c2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INARISAMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x182

    aput v2, v0, v1
    :try_end_c2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c2 .. :try_end_c2} :catch_131

    :goto_c3
    :try_start_c3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INDIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xdd

    aput v2, v0, v1
    :try_end_c3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c3 .. :try_end_c3} :catch_130

    :goto_c4
    :try_start_c4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INDOEUROPEAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xde

    aput v2, v0, v1
    :try_end_c4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c4 .. :try_end_c4} :catch_12f

    :goto_c5
    :try_start_c5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INDONESIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe0

    aput v2, v0, v1
    :try_end_c5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c5 .. :try_end_c5} :catch_12e

    :goto_c6
    :try_start_c6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INGUSH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xdf

    aput v2, v0, v1
    :try_end_c6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c6 .. :try_end_c6} :catch_12d

    :goto_c7
    :try_start_c7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INTERLINGUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe1

    aput v2, v0, v1
    :try_end_c7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c7 .. :try_end_c7} :catch_12c

    :goto_c8
    :try_start_c8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INTERLINGUE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe2

    aput v2, v0, v1
    :try_end_c8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c8 .. :try_end_c8} :catch_12b

    :goto_c9
    :try_start_c9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INUKTITUT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe3

    aput v2, v0, v1
    :try_end_c9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c9 .. :try_end_c9} :catch_12a

    :goto_ca
    :try_start_ca
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_INUPIAK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe4

    aput v2, v0, v1
    :try_end_ca
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ca .. :try_end_ca} :catch_129

    :goto_cb
    :try_start_cb
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IRANIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe5

    aput v2, v0, v1
    :try_end_cb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_cb .. :try_end_cb} :catch_128

    :goto_cc
    :try_start_cc
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IRISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_cc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_cc .. :try_end_cc} :catch_127

    :goto_cd
    :try_start_cd
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IRISHMIDDLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe7

    aput v2, v0, v1
    :try_end_cd
    .catch Ljava/lang/NoSuchFieldError; {:try_start_cd .. :try_end_cd} :catch_126

    :goto_ce
    :try_start_ce
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IRISHOLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe6

    aput v2, v0, v1
    :try_end_ce
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ce .. :try_end_ce} :catch_125

    :goto_cf
    :try_start_cf
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_IROQUOIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe8

    aput v2, v0, v1
    :try_end_cf
    .catch Ljava/lang/NoSuchFieldError; {:try_start_cf .. :try_end_cf} :catch_124

    :goto_d0
    :try_start_d0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ITALIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_d0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d0 .. :try_end_d0} :catch_123

    :goto_d1
    :try_start_d1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_JAPANESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xe9

    aput v2, v0, v1
    :try_end_d1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d1 .. :try_end_d1} :catch_122

    :goto_d2
    :try_start_d2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_JAVANESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xea

    aput v2, v0, v1
    :try_end_d2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d2 .. :try_end_d2} :catch_121

    :goto_d3
    :try_start_d3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_JUDEOARABIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xec

    aput v2, v0, v1
    :try_end_d3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d3 .. :try_end_d3} :catch_120

    :goto_d4
    :try_start_d4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_JUDEOPERSIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xed

    aput v2, v0, v1
    :try_end_d4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d4 .. :try_end_d4} :catch_11f

    :goto_d5
    :try_start_d5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KABYLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xee

    aput v2, v0, v1
    :try_end_d5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d5 .. :try_end_d5} :catch_11e

    :goto_d6
    :try_start_d6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KACHIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xef

    aput v2, v0, v1
    :try_end_d6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d6 .. :try_end_d6} :catch_11d

    :goto_d7
    :try_start_d7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KALMYK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e3

    aput v2, v0, v1
    :try_end_d7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d7 .. :try_end_d7} :catch_11c

    :goto_d8
    :try_start_d8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KAMBA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf0

    aput v2, v0, v1
    :try_end_d8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d8 .. :try_end_d8} :catch_11b

    :goto_d9
    :try_start_d9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KANNADA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf1

    aput v2, v0, v1
    :try_end_d9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d9 .. :try_end_d9} :catch_11a

    :goto_da
    :try_start_da
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KANURI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf2

    aput v2, v0, v1
    :try_end_da
    .catch Ljava/lang/NoSuchFieldError; {:try_start_da .. :try_end_da} :catch_119

    :goto_db
    :try_start_db
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KARACHAYBALKAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x105

    aput v2, v0, v1
    :try_end_db
    .catch Ljava/lang/NoSuchFieldError; {:try_start_db .. :try_end_db} :catch_118

    :goto_dc
    :try_start_dc
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KARAKALPAK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf3

    aput v2, v0, v1
    :try_end_dc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_dc .. :try_end_dc} :catch_117

    :goto_dd
    :try_start_dd
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KARELIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x106

    aput v2, v0, v1
    :try_end_dd
    .catch Ljava/lang/NoSuchFieldError; {:try_start_dd .. :try_end_dd} :catch_116

    :goto_de
    :try_start_de
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KAREN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf4

    aput v2, v0, v1
    :try_end_de
    .catch Ljava/lang/NoSuchFieldError; {:try_start_de .. :try_end_de} :catch_115

    :goto_df
    :try_start_df
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KASHMIRI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf5

    aput v2, v0, v1
    :try_end_df
    .catch Ljava/lang/NoSuchFieldError; {:try_start_df .. :try_end_df} :catch_114

    :goto_e0
    :try_start_e0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KASHUBIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x90

    aput v2, v0, v1
    :try_end_e0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e0 .. :try_end_e0} :catch_113

    :goto_e1
    :try_start_e1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KAWI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf6

    aput v2, v0, v1
    :try_end_e1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e1 .. :try_end_e1} :catch_112

    :goto_e2
    :try_start_e2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KAZAKH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf7

    aput v2, v0, v1
    :try_end_e2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e2 .. :try_end_e2} :catch_111

    :goto_e3
    :try_start_e3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KHASI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xf9

    aput v2, v0, v1
    :try_end_e3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e3 .. :try_end_e3} :catch_110

    :goto_e4
    :try_start_e4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KHMER:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xfa

    aput v2, v0, v1
    :try_end_e4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e4 .. :try_end_e4} :catch_10f

    :goto_e5
    :try_start_e5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KHOISAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xfb

    aput v2, v0, v1
    :try_end_e5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e5 .. :try_end_e5} :catch_10e

    :goto_e6
    :try_start_e6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KHOTANESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xfc

    aput v2, v0, v1
    :try_end_e6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e6 .. :try_end_e6} :catch_10d

    :goto_e7
    :try_start_e7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KIKUYU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xfd

    aput v2, v0, v1
    :try_end_e7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e7 .. :try_end_e7} :catch_10c

    :goto_e8
    :try_start_e8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KIMBUNDU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x100

    aput v2, v0, v1
    :try_end_e8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e8 .. :try_end_e8} :catch_10b

    :goto_e9
    :try_start_e9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KINYARWANDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xfe

    aput v2, v0, v1
    :try_end_e9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e9 .. :try_end_e9} :catch_10a

    :goto_ea
    :try_start_ea
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KIRGHIZ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xff

    aput v2, v0, v1
    :try_end_ea
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ea .. :try_end_ea} :catch_109

    :goto_eb
    :try_start_eb
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KLINGON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c1

    aput v2, v0, v1
    :try_end_eb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_eb .. :try_end_eb} :catch_108

    :goto_ec
    :try_start_ec
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KOMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x101

    aput v2, v0, v1
    :try_end_ec
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ec .. :try_end_ec} :catch_107

    :goto_ed
    :try_start_ed
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KONGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x102

    aput v2, v0, v1
    :try_end_ed
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ed .. :try_end_ed} :catch_106

    :goto_ee
    :try_start_ee
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KONKANI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x103

    aput v2, v0, v1
    :try_end_ee
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ee .. :try_end_ee} :catch_105

    :goto_ef
    :try_start_ef
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KOREAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_ef
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ef .. :try_end_ef} :catch_104

    :goto_f0
    :try_start_f0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KOSRAEAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_f0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f0 .. :try_end_f0} :catch_103

    :goto_f1
    :try_start_f1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KPELLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x104

    aput v2, v0, v1
    :try_end_f1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f1 .. :try_end_f1} :catch_102

    :goto_f2
    :try_start_f2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KRU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x107

    aput v2, v0, v1
    :try_end_f2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f2 .. :try_end_f2} :catch_101

    :goto_f3
    :try_start_f3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KUANYAMA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x108

    aput v2, v0, v1
    :try_end_f3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f3 .. :try_end_f3} :catch_100

    :goto_f4
    :try_start_f4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KUMYK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x109

    aput v2, v0, v1
    :try_end_f4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f4 .. :try_end_f4} :catch_ff

    :goto_f5
    :try_start_f5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KURDISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10a

    aput v2, v0, v1
    :try_end_f5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f5 .. :try_end_f5} :catch_fe

    :goto_f6
    :try_start_f6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KURUKH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10b

    aput v2, v0, v1
    :try_end_f6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f6 .. :try_end_f6} :catch_fd

    :goto_f7
    :try_start_f7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KUSAIE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10c

    aput v2, v0, v1
    :try_end_f7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f7 .. :try_end_f7} :catch_fc

    :goto_f8
    :try_start_f8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_KUTENAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10d

    aput v2, v0, v1
    :try_end_f8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f8 .. :try_end_f8} :catch_fb

    :goto_f9
    :try_start_f9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LADINO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10e

    aput v2, v0, v1
    :try_end_f9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f9 .. :try_end_f9} :catch_fa

    :goto_fa
    :try_start_fa
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LAHNDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10f

    aput v2, v0, v1
    :try_end_fa
    .catch Ljava/lang/NoSuchFieldError; {:try_start_fa .. :try_end_fa} :catch_f9

    :goto_fb
    :try_start_fb
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LAMBA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x110

    aput v2, v0, v1
    :try_end_fb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_fb .. :try_end_fb} :catch_f8

    :goto_fc
    :try_start_fc
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LANDDAYAK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_fc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_fc .. :try_end_fc} :catch_f7

    :goto_fd
    :try_start_fd
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LANGUE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x111

    aput v2, v0, v1
    :try_end_fd
    .catch Ljava/lang/NoSuchFieldError; {:try_start_fd .. :try_end_fd} :catch_f6

    :goto_fe
    :try_start_fe
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LAO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x112

    aput v2, v0, v1
    :try_end_fe
    .catch Ljava/lang/NoSuchFieldError; {:try_start_fe .. :try_end_fe} :catch_f5

    :goto_ff
    :try_start_ff
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LATIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x113

    aput v2, v0, v1
    :try_end_ff
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ff .. :try_end_ff} :catch_f4

    :goto_100
    :try_start_100
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LATVIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_100
    .catch Ljava/lang/NoSuchFieldError; {:try_start_100 .. :try_end_100} :catch_f3

    :goto_101
    :try_start_101
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LETZEBURGESCH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x114

    aput v2, v0, v1
    :try_end_101
    .catch Ljava/lang/NoSuchFieldError; {:try_start_101 .. :try_end_101} :catch_f2

    :goto_102
    :try_start_102
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LEZGHIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x115

    aput v2, v0, v1
    :try_end_102
    .catch Ljava/lang/NoSuchFieldError; {:try_start_102 .. :try_end_102} :catch_f1

    :goto_103
    :try_start_103
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LIMBURGISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x116

    aput v2, v0, v1
    :try_end_103
    .catch Ljava/lang/NoSuchFieldError; {:try_start_103 .. :try_end_103} :catch_f0

    :goto_104
    :try_start_104
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LINGALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x117

    aput v2, v0, v1
    :try_end_104
    .catch Ljava/lang/NoSuchFieldError; {:try_start_104 .. :try_end_104} :catch_ef

    :goto_105
    :try_start_105
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LITHUANIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x118

    aput v2, v0, v1
    :try_end_105
    .catch Ljava/lang/NoSuchFieldError; {:try_start_105 .. :try_end_105} :catch_ee

    :goto_106
    :try_start_106
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LOJBAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xeb

    aput v2, v0, v1
    :try_end_106
    .catch Ljava/lang/NoSuchFieldError; {:try_start_106 .. :try_end_106} :catch_ed

    :goto_107
    :try_start_107
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LOZI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x119

    aput v2, v0, v1
    :try_end_107
    .catch Ljava/lang/NoSuchFieldError; {:try_start_107 .. :try_end_107} :catch_ec

    :goto_108
    :try_start_108
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LUBAKATANGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11b

    aput v2, v0, v1
    :try_end_108
    .catch Ljava/lang/NoSuchFieldError; {:try_start_108 .. :try_end_108} :catch_eb

    :goto_109
    :try_start_109
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LUBALULUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11a

    aput v2, v0, v1
    :try_end_109
    .catch Ljava/lang/NoSuchFieldError; {:try_start_109 .. :try_end_109} :catch_ea

    :goto_10a
    :try_start_10a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LUISENO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11c

    aput v2, v0, v1
    :try_end_10a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10a .. :try_end_10a} :catch_e9

    :goto_10b
    :try_start_10b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LULESAMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x181

    aput v2, v0, v1
    :try_end_10b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10b .. :try_end_10b} :catch_e8

    :goto_10c
    :try_start_10c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LUNDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11d

    aput v2, v0, v1
    :try_end_10c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10c .. :try_end_10c} :catch_e7

    :goto_10d
    :try_start_10d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LUO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11e

    aput v2, v0, v1
    :try_end_10d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10d .. :try_end_10d} :catch_e6

    :goto_10e
    :try_start_10e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_LUSHAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11f

    aput v2, v0, v1
    :try_end_10e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10e .. :try_end_10e} :catch_e5

    :goto_10f
    :try_start_10f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MACEDONIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x120

    aput v2, v0, v1
    :try_end_10f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10f .. :try_end_10f} :catch_e4

    :goto_110
    :try_start_110
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MADURESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x121

    aput v2, v0, v1
    :try_end_110
    .catch Ljava/lang/NoSuchFieldError; {:try_start_110 .. :try_end_110} :catch_e3

    :goto_111
    :try_start_111
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MAGAHI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x122

    aput v2, v0, v1
    :try_end_111
    .catch Ljava/lang/NoSuchFieldError; {:try_start_111 .. :try_end_111} :catch_e2

    :goto_112
    :try_start_112
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MAITHILI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x123

    aput v2, v0, v1
    :try_end_112
    .catch Ljava/lang/NoSuchFieldError; {:try_start_112 .. :try_end_112} :catch_e1

    :goto_113
    :try_start_113
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MAKASAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x124

    aput v2, v0, v1
    :try_end_113
    .catch Ljava/lang/NoSuchFieldError; {:try_start_113 .. :try_end_113} :catch_e0

    :goto_114
    :try_start_114
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MALAGASY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x125

    aput v2, v0, v1
    :try_end_114
    .catch Ljava/lang/NoSuchFieldError; {:try_start_114 .. :try_end_114} :catch_df

    :goto_115
    :try_start_115
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MALAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x126

    aput v2, v0, v1
    :try_end_115
    .catch Ljava/lang/NoSuchFieldError; {:try_start_115 .. :try_end_115} :catch_de

    :goto_116
    :try_start_116
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MALAYALAM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x129

    aput v2, v0, v1
    :try_end_116
    .catch Ljava/lang/NoSuchFieldError; {:try_start_116 .. :try_end_116} :catch_dd

    :goto_117
    :try_start_117
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MALTESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12a

    aput v2, v0, v1
    :try_end_117
    .catch Ljava/lang/NoSuchFieldError; {:try_start_117 .. :try_end_117} :catch_dc

    :goto_118
    :try_start_118
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MANCHU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12b

    aput v2, v0, v1
    :try_end_118
    .catch Ljava/lang/NoSuchFieldError; {:try_start_118 .. :try_end_118} :catch_db

    :goto_119
    :try_start_119
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MANDAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x128

    aput v2, v0, v1
    :try_end_119
    .catch Ljava/lang/NoSuchFieldError; {:try_start_119 .. :try_end_119} :catch_da

    :goto_11a
    :try_start_11a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MANDARIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x2c

    aput v2, v0, v1
    :try_end_11a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11a .. :try_end_11a} :catch_d9

    :goto_11b
    :try_start_11b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MANDINGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12c

    aput v2, v0, v1
    :try_end_11b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11b .. :try_end_11b} :catch_d8

    :goto_11c
    :try_start_11c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MANIPURI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12d

    aput v2, v0, v1
    :try_end_11c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11c .. :try_end_11c} :catch_d7

    :goto_11d
    :try_start_11d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MANOBO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12e

    aput v2, v0, v1
    :try_end_11d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11d .. :try_end_11d} :catch_d6

    :goto_11e
    :try_start_11e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MANX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12f

    aput v2, v0, v1
    :try_end_11e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11e .. :try_end_11e} :catch_d5

    :goto_11f
    :try_start_11f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MAORI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x2e

    aput v2, v0, v1
    :try_end_11f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11f .. :try_end_11f} :catch_d4

    :goto_120
    :try_start_120
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MARATHI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x130

    aput v2, v0, v1
    :try_end_120
    .catch Ljava/lang/NoSuchFieldError; {:try_start_120 .. :try_end_120} :catch_d3

    :goto_121
    :try_start_121
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MARI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x131

    aput v2, v0, v1
    :try_end_121
    .catch Ljava/lang/NoSuchFieldError; {:try_start_121 .. :try_end_121} :catch_d2

    :goto_122
    :try_start_122
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MARSHALL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x132

    aput v2, v0, v1
    :try_end_122
    .catch Ljava/lang/NoSuchFieldError; {:try_start_122 .. :try_end_122} :catch_d1

    :goto_123
    :try_start_123
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MARWARI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x134

    aput v2, v0, v1
    :try_end_123
    .catch Ljava/lang/NoSuchFieldError; {:try_start_123 .. :try_end_123} :catch_d0

    :goto_124
    :try_start_124
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MASAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x135

    aput v2, v0, v1
    :try_end_124
    .catch Ljava/lang/NoSuchFieldError; {:try_start_124 .. :try_end_124} :catch_cf

    :goto_125
    :try_start_125
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1f4

    aput v2, v0, v1
    :try_end_125
    .catch Ljava/lang/NoSuchFieldError; {:try_start_125 .. :try_end_125} :catch_ce

    :goto_126
    :try_start_126
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MAYAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x136

    aput v2, v0, v1
    :try_end_126
    .catch Ljava/lang/NoSuchFieldError; {:try_start_126 .. :try_end_126} :catch_cd

    :goto_127
    :try_start_127
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MENDE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x138

    aput v2, v0, v1
    :try_end_127
    .catch Ljava/lang/NoSuchFieldError; {:try_start_127 .. :try_end_127} :catch_cc

    :goto_128
    :try_start_128
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MICMAC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x139

    aput v2, v0, v1
    :try_end_128
    .catch Ljava/lang/NoSuchFieldError; {:try_start_128 .. :try_end_128} :catch_cb

    :goto_129
    :try_start_129
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MINANGKABAU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13a

    aput v2, v0, v1
    :try_end_129
    .catch Ljava/lang/NoSuchFieldError; {:try_start_129 .. :try_end_129} :catch_ca

    :goto_12a
    :try_start_12a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MIRANDESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x133

    aput v2, v0, v1
    :try_end_12a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12a .. :try_end_12a} :catch_c9

    :goto_12b
    :try_start_12b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MISCELLANEOUS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13b

    aput v2, v0, v1
    :try_end_12b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12b .. :try_end_12b} :catch_c8

    :goto_12c
    :try_start_12c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MOHAWK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13c

    aput v2, v0, v1
    :try_end_12c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12c .. :try_end_12c} :catch_c7

    :goto_12d
    :try_start_12d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MOKSHA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x127

    aput v2, v0, v1
    :try_end_12d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12d .. :try_end_12d} :catch_c6

    :goto_12e
    :try_start_12e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MOLDAVIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13d

    aput v2, v0, v1
    :try_end_12e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12e .. :try_end_12e} :catch_c5

    :goto_12f
    :try_start_12f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MONGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13f

    aput v2, v0, v1
    :try_end_12f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12f .. :try_end_12f} :catch_c4

    :goto_130
    :try_start_130
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MONGOLIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x140

    aput v2, v0, v1
    :try_end_130
    .catch Ljava/lang/NoSuchFieldError; {:try_start_130 .. :try_end_130} :catch_c3

    :goto_131
    :try_start_131
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MONKMER:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13e

    aput v2, v0, v1
    :try_end_131
    .catch Ljava/lang/NoSuchFieldError; {:try_start_131 .. :try_end_131} :catch_c2

    :goto_132
    :try_start_132
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MOSSI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x141

    aput v2, v0, v1
    :try_end_132
    .catch Ljava/lang/NoSuchFieldError; {:try_start_132 .. :try_end_132} :catch_c1

    :goto_133
    :try_start_133
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MULTIPLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x142

    aput v2, v0, v1
    :try_end_133
    .catch Ljava/lang/NoSuchFieldError; {:try_start_133 .. :try_end_133} :catch_c0

    :goto_134
    :try_start_134
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MUNDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x143

    aput v2, v0, v1
    :try_end_134
    .catch Ljava/lang/NoSuchFieldError; {:try_start_134 .. :try_end_134} :catch_bf

    :goto_135
    :try_start_135
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NAURU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x145

    aput v2, v0, v1
    :try_end_135
    .catch Ljava/lang/NoSuchFieldError; {:try_start_135 .. :try_end_135} :catch_be

    :goto_136
    :try_start_136
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NAVAJO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x146

    aput v2, v0, v1
    :try_end_136
    .catch Ljava/lang/NoSuchFieldError; {:try_start_136 .. :try_end_136} :catch_bd

    :goto_137
    :try_start_137
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NDEBELENORTH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x147

    aput v2, v0, v1
    :try_end_137
    .catch Ljava/lang/NoSuchFieldError; {:try_start_137 .. :try_end_137} :catch_bc

    :goto_138
    :try_start_138
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NDEBELESOUTH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x148

    aput v2, v0, v1
    :try_end_138
    .catch Ljava/lang/NoSuchFieldError; {:try_start_138 .. :try_end_138} :catch_bb

    :goto_139
    :try_start_139
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NDONGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x149

    aput v2, v0, v1
    :try_end_139
    .catch Ljava/lang/NoSuchFieldError; {:try_start_139 .. :try_end_139} :catch_ba

    :goto_13a
    :try_start_13a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NEAPOLITAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x144

    aput v2, v0, v1
    :try_end_13a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13a .. :try_end_13a} :catch_b9

    :goto_13b
    :try_start_13b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NEPALI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14b

    aput v2, v0, v1
    :try_end_13b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13b .. :try_end_13b} :catch_b8

    :goto_13c
    :try_start_13c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NETHERLANDS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_13c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13c .. :try_end_13c} :catch_b7

    :goto_13d
    :try_start_13d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NEWARI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14c

    aput v2, v0, v1
    :try_end_13d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13d .. :try_end_13d} :catch_b6

    :goto_13e
    :try_start_13e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NEWARICLASSICAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x158

    aput v2, v0, v1
    :try_end_13e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13e .. :try_end_13e} :catch_b5

    :goto_13f
    :try_start_13f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NIAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14d

    aput v2, v0, v1
    :try_end_13f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13f .. :try_end_13f} :catch_b4

    :goto_140
    :try_start_140
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NIGERKORDOFANIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14e

    aput v2, v0, v1
    :try_end_140
    .catch Ljava/lang/NoSuchFieldError; {:try_start_140 .. :try_end_140} :catch_b3

    :goto_141
    :try_start_141
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NILOSAHARAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14f

    aput v2, v0, v1
    :try_end_141
    .catch Ljava/lang/NoSuchFieldError; {:try_start_141 .. :try_end_141} :catch_b2

    :goto_142
    :try_start_142
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NIUEAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x150

    aput v2, v0, v1
    :try_end_142
    .catch Ljava/lang/NoSuchFieldError; {:try_start_142 .. :try_end_142} :catch_b1

    :goto_143
    :try_start_143
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NKO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a3

    aput v2, v0, v1
    :try_end_143
    .catch Ljava/lang/NoSuchFieldError; {:try_start_143 .. :try_end_143} :catch_b0

    :goto_144
    :try_start_144
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NOGAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x152

    aput v2, v0, v1
    :try_end_144
    .catch Ljava/lang/NoSuchFieldError; {:try_start_144 .. :try_end_144} :catch_af

    :goto_145
    :try_start_145
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x33

    aput v2, v0, v1
    :try_end_145
    .catch Ljava/lang/NoSuchFieldError; {:try_start_145 .. :try_end_145} :catch_ae

    :goto_146
    :try_start_146
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NORSEOLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x153

    aput v2, v0, v1
    :try_end_146
    .catch Ljava/lang/NoSuchFieldError; {:try_start_146 .. :try_end_146} :catch_ad

    :goto_147
    :try_start_147
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NORTHAMERICANINDIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x154

    aput v2, v0, v1
    :try_end_147
    .catch Ljava/lang/NoSuchFieldError; {:try_start_147 .. :try_end_147} :catch_ac

    :goto_148
    :try_start_148
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NORWEGIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_148
    .catch Ljava/lang/NoSuchFieldError; {:try_start_148 .. :try_end_148} :catch_ab

    :goto_149
    :try_start_149
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NORWEGIANBOKMAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x151

    aput v2, v0, v1
    :try_end_149
    .catch Ljava/lang/NoSuchFieldError; {:try_start_149 .. :try_end_149} :catch_aa

    :goto_14a
    :try_start_14a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NORWEGIANNYNORSK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x155

    aput v2, v0, v1
    :try_end_14a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14a .. :try_end_14a} :catch_a9

    :goto_14b
    :try_start_14b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NUBIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x156

    aput v2, v0, v1
    :try_end_14b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14b .. :try_end_14b} :catch_a8

    :goto_14c
    :try_start_14c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NUOSU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xd9

    aput v2, v0, v1
    :try_end_14c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14c .. :try_end_14c} :catch_a7

    :goto_14d
    :try_start_14d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NYAMWEZI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x157

    aput v2, v0, v1
    :try_end_14d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14d .. :try_end_14d} :catch_a6

    :goto_14e
    :try_start_14e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NYANJA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x159

    aput v2, v0, v1
    :try_end_14e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14e .. :try_end_14e} :catch_a5

    :goto_14f
    :try_start_14f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NYANKOLE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x15a

    aput v2, v0, v1
    :try_end_14f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14f .. :try_end_14f} :catch_a4

    :goto_150
    :try_start_150
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NYORO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x15b

    aput v2, v0, v1
    :try_end_150
    .catch Ljava/lang/NoSuchFieldError; {:try_start_150 .. :try_end_150} :catch_a3

    :goto_151
    :try_start_151
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_NZIMA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x15c

    aput v2, v0, v1
    :try_end_151
    .catch Ljava/lang/NoSuchFieldError; {:try_start_151 .. :try_end_151} :catch_a2

    :goto_152
    :try_start_152
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_OJIBWA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x15d

    aput v2, v0, v1
    :try_end_152
    .catch Ljava/lang/NoSuchFieldError; {:try_start_152 .. :try_end_152} :catch_a1

    :goto_153
    :try_start_153
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ORIYA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x15e

    aput v2, v0, v1
    :try_end_153
    .catch Ljava/lang/NoSuchFieldError; {:try_start_153 .. :try_end_153} :catch_a0

    :goto_154
    :try_start_154
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_OROMO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x15f

    aput v2, v0, v1
    :try_end_154
    .catch Ljava/lang/NoSuchFieldError; {:try_start_154 .. :try_end_154} :catch_9f

    :goto_155
    :try_start_155
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_OSAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x160

    aput v2, v0, v1
    :try_end_155
    .catch Ljava/lang/NoSuchFieldError; {:try_start_155 .. :try_end_155} :catch_9e

    :goto_156
    :try_start_156
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_OSSETIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x161

    aput v2, v0, v1
    :try_end_156
    .catch Ljava/lang/NoSuchFieldError; {:try_start_156 .. :try_end_156} :catch_9d

    :goto_157
    :try_start_157
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_OTOMIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x162

    aput v2, v0, v1
    :try_end_157
    .catch Ljava/lang/NoSuchFieldError; {:try_start_157 .. :try_end_157} :catch_9c

    :goto_158
    :try_start_158
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PAHLAVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x163

    aput v2, v0, v1
    :try_end_158
    .catch Ljava/lang/NoSuchFieldError; {:try_start_158 .. :try_end_158} :catch_9b

    :goto_159
    :try_start_159
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PALAUAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x164

    aput v2, v0, v1
    :try_end_159
    .catch Ljava/lang/NoSuchFieldError; {:try_start_159 .. :try_end_159} :catch_9a

    :goto_15a
    :try_start_15a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PALI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x165

    aput v2, v0, v1
    :try_end_15a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15a .. :try_end_15a} :catch_99

    :goto_15b
    :try_start_15b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PAMPANGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x166

    aput v2, v0, v1
    :try_end_15b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15b .. :try_end_15b} :catch_98

    :goto_15c
    :try_start_15c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PANGASINAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x167

    aput v2, v0, v1
    :try_end_15c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15c .. :try_end_15c} :catch_97

    :goto_15d
    :try_start_15d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PANJABI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x168

    aput v2, v0, v1
    :try_end_15d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15d .. :try_end_15d} :catch_96

    :goto_15e
    :try_start_15e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PAPIAMENTO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x169

    aput v2, v0, v1
    :try_end_15e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15e .. :try_end_15e} :catch_95

    :goto_15f
    :try_start_15f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PAPUANAUSTRALIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x16a

    aput v2, v0, v1
    :try_end_15f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15f .. :try_end_15f} :catch_94

    :goto_160
    :try_start_160
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PERSIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x16b

    aput v2, v0, v1
    :try_end_160
    .catch Ljava/lang/NoSuchFieldError; {:try_start_160 .. :try_end_160} :catch_93

    :goto_161
    :try_start_161
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PERSIANOLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x16c

    aput v2, v0, v1
    :try_end_161
    .catch Ljava/lang/NoSuchFieldError; {:try_start_161 .. :try_end_161} :catch_92

    :goto_162
    :try_start_162
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PHILIPPINE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x16e

    aput v2, v0, v1
    :try_end_162
    .catch Ljava/lang/NoSuchFieldError; {:try_start_162 .. :try_end_162} :catch_91

    :goto_163
    :try_start_163
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PHOENICIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x16d

    aput v2, v0, v1
    :try_end_163
    .catch Ljava/lang/NoSuchFieldError; {:try_start_163 .. :try_end_163} :catch_90

    :goto_164
    :try_start_164
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_POLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_164
    .catch Ljava/lang/NoSuchFieldError; {:try_start_164 .. :try_end_164} :catch_8f

    :goto_165
    :try_start_165
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PONAPE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x16f

    aput v2, v0, v1
    :try_end_165
    .catch Ljava/lang/NoSuchFieldError; {:try_start_165 .. :try_end_165} :catch_8e

    :goto_166
    :try_start_166
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PORTUGUESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_166
    .catch Ljava/lang/NoSuchFieldError; {:try_start_166 .. :try_end_166} :catch_8d

    :goto_167
    :try_start_167
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PRAKRIT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x170

    aput v2, v0, v1
    :try_end_167
    .catch Ljava/lang/NoSuchFieldError; {:try_start_167 .. :try_end_167} :catch_8c

    :goto_168
    :try_start_168
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PROVENCALOLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x171

    aput v2, v0, v1
    :try_end_168
    .catch Ljava/lang/NoSuchFieldError; {:try_start_168 .. :try_end_168} :catch_8b

    :goto_169
    :try_start_169
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_PUSHTO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x172

    aput v2, v0, v1
    :try_end_169
    .catch Ljava/lang/NoSuchFieldError; {:try_start_169 .. :try_end_169} :catch_8a

    :goto_16a
    :try_start_16a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_QAA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x2f

    aput v2, v0, v1
    :try_end_16a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16a .. :try_end_16a} :catch_89

    :goto_16b
    :try_start_16b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_QUECHUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x173

    aput v2, v0, v1
    :try_end_16b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16b .. :try_end_16b} :catch_88

    :goto_16c
    :try_start_16c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_RAJASTHANI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x175

    aput v2, v0, v1
    :try_end_16c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16c .. :try_end_16c} :catch_87

    :goto_16d
    :try_start_16d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_RAPANUI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x176

    aput v2, v0, v1
    :try_end_16d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16d .. :try_end_16d} :catch_86

    :goto_16e
    :try_start_16e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_RAROTONGAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x177

    aput v2, v0, v1
    :try_end_16e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16e .. :try_end_16e} :catch_85

    :goto_16f
    :try_start_16f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_RHAETOROMANCE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x174

    aput v2, v0, v1
    :try_end_16f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16f .. :try_end_16f} :catch_84

    :goto_170
    :try_start_170
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ROMANCE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x178

    aput v2, v0, v1
    :try_end_170
    .catch Ljava/lang/NoSuchFieldError; {:try_start_170 .. :try_end_170} :catch_83

    :goto_171
    :try_start_171
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ROMANIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_171
    .catch Ljava/lang/NoSuchFieldError; {:try_start_171 .. :try_end_171} :catch_82

    :goto_172
    :try_start_172
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ROMANY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x179

    aput v2, v0, v1
    :try_end_172
    .catch Ljava/lang/NoSuchFieldError; {:try_start_172 .. :try_end_172} :catch_81

    :goto_173
    :try_start_173
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_RUNDI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x17a

    aput v2, v0, v1
    :try_end_173
    .catch Ljava/lang/NoSuchFieldError; {:try_start_173 .. :try_end_173} :catch_80

    :goto_174
    :try_start_174
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_RUSSIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_174
    .catch Ljava/lang/NoSuchFieldError; {:try_start_174 .. :try_end_174} :catch_7f

    :goto_175
    :try_start_175
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SALISHAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x17c

    aput v2, v0, v1
    :try_end_175
    .catch Ljava/lang/NoSuchFieldError; {:try_start_175 .. :try_end_175} :catch_7e

    :goto_176
    :try_start_176
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SAMARITANARAMAIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x17d

    aput v2, v0, v1
    :try_end_176
    .catch Ljava/lang/NoSuchFieldError; {:try_start_176 .. :try_end_176} :catch_7d

    :goto_177
    :try_start_177
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SAMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x180

    aput v2, v0, v1
    :try_end_177
    .catch Ljava/lang/NoSuchFieldError; {:try_start_177 .. :try_end_177} :catch_7c

    :goto_178
    :try_start_178
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SAMINORTHERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x17f

    aput v2, v0, v1
    :try_end_178
    .catch Ljava/lang/NoSuchFieldError; {:try_start_178 .. :try_end_178} :catch_7b

    :goto_179
    :try_start_179
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SAMISOUTHERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x17e

    aput v2, v0, v1
    :try_end_179
    .catch Ljava/lang/NoSuchFieldError; {:try_start_179 .. :try_end_179} :catch_7a

    :goto_17a
    :try_start_17a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SAMOAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x183

    aput v2, v0, v1
    :try_end_17a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17a .. :try_end_17a} :catch_79

    :goto_17b
    :try_start_17b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SANDAWE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x185

    aput v2, v0, v1
    :try_end_17b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17b .. :try_end_17b} :catch_78

    :goto_17c
    :try_start_17c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SANGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x186

    aput v2, v0, v1
    :try_end_17c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17c .. :try_end_17c} :catch_77

    :goto_17d
    :try_start_17d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SANSKRIT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x187

    aput v2, v0, v1
    :try_end_17d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17d .. :try_end_17d} :catch_76

    :goto_17e
    :try_start_17e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SANTALI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x18a

    aput v2, v0, v1
    :try_end_17e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17e .. :try_end_17e} :catch_75

    :goto_17f
    :try_start_17f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SARDINIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x188

    aput v2, v0, v1
    :try_end_17f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17f .. :try_end_17f} :catch_74

    :goto_180
    :try_start_180
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SASAK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x189

    aput v2, v0, v1
    :try_end_180
    .catch Ljava/lang/NoSuchFieldError; {:try_start_180 .. :try_end_180} :catch_73

    :goto_181
    :try_start_181
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SCOTS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x18c

    aput v2, v0, v1
    :try_end_181
    .catch Ljava/lang/NoSuchFieldError; {:try_start_181 .. :try_end_181} :catch_72

    :goto_182
    :try_start_182
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SELKUP:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x18d

    aput v2, v0, v1
    :try_end_182
    .catch Ljava/lang/NoSuchFieldError; {:try_start_182 .. :try_end_182} :catch_71

    :goto_183
    :try_start_183
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SEMITIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x18e

    aput v2, v0, v1
    :try_end_183
    .catch Ljava/lang/NoSuchFieldError; {:try_start_183 .. :try_end_183} :catch_70

    :goto_184
    :try_start_184
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SERBIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_184
    .catch Ljava/lang/NoSuchFieldError; {:try_start_184 .. :try_end_184} :catch_6f

    :goto_185
    :try_start_185
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SERBOCROATIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x190

    aput v2, v0, v1
    :try_end_185
    .catch Ljava/lang/NoSuchFieldError; {:try_start_185 .. :try_end_185} :catch_6e

    :goto_186
    :try_start_186
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SERER:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x191

    aput v2, v0, v1
    :try_end_186
    .catch Ljava/lang/NoSuchFieldError; {:try_start_186 .. :try_end_186} :catch_6d

    :goto_187
    :try_start_187
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SHAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x193

    aput v2, v0, v1
    :try_end_187
    .catch Ljava/lang/NoSuchFieldError; {:try_start_187 .. :try_end_187} :catch_6c

    :goto_188
    :try_start_188
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SHONA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x194

    aput v2, v0, v1
    :try_end_188
    .catch Ljava/lang/NoSuchFieldError; {:try_start_188 .. :try_end_188} :catch_6b

    :goto_189
    :try_start_189
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SICILIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x18b

    aput v2, v0, v1
    :try_end_189
    .catch Ljava/lang/NoSuchFieldError; {:try_start_189 .. :try_end_189} :catch_6a

    :goto_18a
    :try_start_18a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SIDAMO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x195

    aput v2, v0, v1
    :try_end_18a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18a .. :try_end_18a} :catch_69

    :goto_18b
    :try_start_18b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SIGN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x192

    aput v2, v0, v1
    :try_end_18b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18b .. :try_end_18b} :catch_68

    :goto_18c
    :try_start_18c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SIKSIKA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x196

    aput v2, v0, v1
    :try_end_18c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18c .. :try_end_18c} :catch_67

    :goto_18d
    :try_start_18d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SINDHI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x197

    aput v2, v0, v1
    :try_end_18d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18d .. :try_end_18d} :catch_66

    :goto_18e
    :try_start_18e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SINGHALESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x199

    aput v2, v0, v1
    :try_end_18e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18e .. :try_end_18e} :catch_65

    :goto_18f
    :try_start_18f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SINOTIBETAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x19a

    aput v2, v0, v1
    :try_end_18f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18f .. :try_end_18f} :catch_64

    :goto_190
    :try_start_190
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SIOUAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x19b

    aput v2, v0, v1
    :try_end_190
    .catch Ljava/lang/NoSuchFieldError; {:try_start_190 .. :try_end_190} :catch_63

    :goto_191
    :try_start_191
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SISWANT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x19d

    aput v2, v0, v1
    :try_end_191
    .catch Ljava/lang/NoSuchFieldError; {:try_start_191 .. :try_end_191} :catch_62

    :goto_192
    :try_start_192
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SKOLTSAMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x184

    aput v2, v0, v1
    :try_end_192
    .catch Ljava/lang/NoSuchFieldError; {:try_start_192 .. :try_end_192} :catch_61

    :goto_193
    :try_start_193
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SLAVEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x94

    aput v2, v0, v1
    :try_end_193
    .catch Ljava/lang/NoSuchFieldError; {:try_start_193 .. :try_end_193} :catch_60

    :goto_194
    :try_start_194
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SLAVIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x19c

    aput v2, v0, v1
    :try_end_194
    .catch Ljava/lang/NoSuchFieldError; {:try_start_194 .. :try_end_194} :catch_5f

    :goto_195
    :try_start_195
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SLOVAK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_195
    .catch Ljava/lang/NoSuchFieldError; {:try_start_195 .. :try_end_195} :catch_5e

    :goto_196
    :try_start_196
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SLOVENIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_196
    .catch Ljava/lang/NoSuchFieldError; {:try_start_196 .. :try_end_196} :catch_5d

    :goto_197
    :try_start_197
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SOGDIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x19e

    aput v2, v0, v1
    :try_end_197
    .catch Ljava/lang/NoSuchFieldError; {:try_start_197 .. :try_end_197} :catch_5c

    :goto_198
    :try_start_198
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SOMALI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x19f

    aput v2, v0, v1
    :try_end_198
    .catch Ljava/lang/NoSuchFieldError; {:try_start_198 .. :try_end_198} :catch_5b

    :goto_199
    :try_start_199
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SONGHAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a0

    aput v2, v0, v1
    :try_end_199
    .catch Ljava/lang/NoSuchFieldError; {:try_start_199 .. :try_end_199} :catch_5a

    :goto_19a
    :try_start_19a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SONINKE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x198

    aput v2, v0, v1
    :try_end_19a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19a .. :try_end_19a} :catch_59

    :goto_19b
    :try_start_19b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SORBIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a1

    aput v2, v0, v1
    :try_end_19b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19b .. :try_end_19b} :catch_58

    :goto_19c
    :try_start_19c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SORBIANLOWER:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x9b

    aput v2, v0, v1
    :try_end_19c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19c .. :try_end_19c} :catch_57

    :goto_19d
    :try_start_19d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SORBIANUPPER:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_19d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19d .. :try_end_19d} :catch_56

    :goto_19e
    :try_start_19e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SOTHONORTHERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a4

    aput v2, v0, v1
    :try_end_19e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19e .. :try_end_19e} :catch_55

    :goto_19f
    :try_start_19f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SOTHOSOUTHERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a5

    aput v2, v0, v1
    :try_end_19f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19f .. :try_end_19f} :catch_54

    :goto_1a0
    :try_start_1a0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SOUTHAMERICANINDIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a6

    aput v2, v0, v1
    :try_end_1a0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a0 .. :try_end_1a0} :catch_53

    :goto_1a1
    :try_start_1a1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SPANISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1a1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a1 .. :try_end_1a1} :catch_52

    :goto_1a2
    :try_start_1a2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SRANANTONGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x18f

    aput v2, v0, v1
    :try_end_1a2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a2 .. :try_end_1a2} :catch_51

    :goto_1a3
    :try_start_1a3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SUDANESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a9

    aput v2, v0, v1
    :try_end_1a3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a3 .. :try_end_1a3} :catch_50

    :goto_1a4
    :try_start_1a4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SUKUMA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a7

    aput v2, v0, v1
    :try_end_1a4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a4 .. :try_end_1a4} :catch_4f

    :goto_1a5
    :try_start_1a5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SUMERIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a8

    aput v2, v0, v1
    :try_end_1a5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a5 .. :try_end_1a5} :catch_4e

    :goto_1a6
    :try_start_1a6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SUSU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1aa

    aput v2, v0, v1
    :try_end_1a6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a6 .. :try_end_1a6} :catch_4d

    :goto_1a7
    :try_start_1a7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SWAHILI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ab

    aput v2, v0, v1
    :try_end_1a7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a7 .. :try_end_1a7} :catch_4c

    :goto_1a8
    :try_start_1a8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SWAZI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ac

    aput v2, v0, v1
    :try_end_1a8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a8 .. :try_end_1a8} :catch_4b

    :goto_1a9
    :try_start_1a9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SWEDISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_1a9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a9 .. :try_end_1a9} :catch_4a

    :goto_1aa
    :try_start_1aa
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SWISSGERMAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0xca

    aput v2, v0, v1
    :try_end_1aa
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1aa .. :try_end_1aa} :catch_49

    :goto_1ab
    :try_start_1ab
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SWIZE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ad

    aput v2, v0, v1
    :try_end_1ab
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ab .. :try_end_1ab} :catch_48

    :goto_1ac
    :try_start_1ac
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SYRIAC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1af

    aput v2, v0, v1
    :try_end_1ac
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ac .. :try_end_1ac} :catch_47

    :goto_1ad
    :try_start_1ad
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_SYRIACCLASSICAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ae

    aput v2, v0, v1
    :try_end_1ad
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ad .. :try_end_1ad} :catch_46

    :goto_1ae
    :try_start_1ae
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TAGALOG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b0

    aput v2, v0, v1
    :try_end_1ae
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ae .. :try_end_1ae} :catch_45

    :goto_1af
    :try_start_1af
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TAHITIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b1

    aput v2, v0, v1
    :try_end_1af
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1af .. :try_end_1af} :catch_44

    :goto_1b0
    :try_start_1b0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b5

    aput v2, v0, v1
    :try_end_1b0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b0 .. :try_end_1b0} :catch_43

    :goto_1b1
    :try_start_1b1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TAJIK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b3

    aput v2, v0, v1
    :try_end_1b1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b1 .. :try_end_1b1} :catch_42

    :goto_1b2
    :try_start_1b2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TAMASHEK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b4

    aput v2, v0, v1
    :try_end_1b2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b2 .. :try_end_1b2} :catch_41

    :goto_1b3
    :try_start_1b3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TAMIL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b6

    aput v2, v0, v1
    :try_end_1b3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b3 .. :try_end_1b3} :catch_40

    :goto_1b4
    :try_start_1b4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TATAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b7

    aput v2, v0, v1
    :try_end_1b4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b4 .. :try_end_1b4} :catch_3f

    :goto_1b5
    :try_start_1b5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TELETEXT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b8

    aput v2, v0, v1
    :try_end_1b5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b5 .. :try_end_1b5} :catch_3e

    :goto_1b6
    :try_start_1b6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TELUGU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b9

    aput v2, v0, v1
    :try_end_1b6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b6 .. :try_end_1b6} :catch_3d

    :goto_1b7
    :try_start_1b7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TERENO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ba

    aput v2, v0, v1
    :try_end_1b7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b7 .. :try_end_1b7} :catch_3c

    :goto_1b8
    :try_start_1b8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TETUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1b2

    aput v2, v0, v1
    :try_end_1b8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b8 .. :try_end_1b8} :catch_3b

    :goto_1b9
    :try_start_1b9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_THAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1bb

    aput v2, v0, v1
    :try_end_1b9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b9 .. :try_end_1b9} :catch_3a

    :goto_1ba
    :try_start_1ba
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TIBETAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1bc

    aput v2, v0, v1
    :try_end_1ba
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ba .. :try_end_1ba} :catch_39

    :goto_1bb
    :try_start_1bb
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TIGRE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1bd

    aput v2, v0, v1
    :try_end_1bb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1bb .. :try_end_1bb} :catch_38

    :goto_1bc
    :try_start_1bc
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TIGRINYA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1be

    aput v2, v0, v1
    :try_end_1bc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1bc .. :try_end_1bc} :catch_37

    :goto_1bd
    :try_start_1bd
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TIMNE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1bf

    aput v2, v0, v1
    :try_end_1bd
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1bd .. :try_end_1bd} :catch_36

    :goto_1be
    :try_start_1be
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TIVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c0

    aput v2, v0, v1
    :try_end_1be
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1be .. :try_end_1be} :catch_35

    :goto_1bf
    :try_start_1bf
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TLINGIT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c3

    aput v2, v0, v1
    :try_end_1bf
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1bf .. :try_end_1bf} :catch_34

    :goto_1c0
    :try_start_1c0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TOKELAU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c2

    aput v2, v0, v1
    :try_end_1c0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c0 .. :try_end_1c0} :catch_33

    :goto_1c1
    :try_start_1c1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TOKPISIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c7

    aput v2, v0, v1
    :try_end_1c1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c1 .. :try_end_1c1} :catch_32

    :goto_1c2
    :try_start_1c2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TONGAISLANDS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c5

    aput v2, v0, v1
    :try_end_1c2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c2 .. :try_end_1c2} :catch_31

    :goto_1c3
    :try_start_1c3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TONGANYASA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c4

    aput v2, v0, v1
    :try_end_1c3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c3 .. :try_end_1c3} :catch_30

    :goto_1c4
    :try_start_1c4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TRUK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c6

    aput v2, v0, v1
    :try_end_1c4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c4 .. :try_end_1c4} :catch_2f

    :goto_1c5
    :try_start_1c5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TSIMSHIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c8

    aput v2, v0, v1
    :try_end_1c5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c5 .. :try_end_1c5} :catch_2e

    :goto_1c6
    :try_start_1c6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TSONGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1c9

    aput v2, v0, v1
    :try_end_1c6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c6 .. :try_end_1c6} :catch_2d

    :goto_1c7
    :try_start_1c7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TSWANA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ca

    aput v2, v0, v1
    :try_end_1c7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c7 .. :try_end_1c7} :catch_2c

    :goto_1c8
    :try_start_1c8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TUMBUKA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1cb

    aput v2, v0, v1
    :try_end_1c8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c8 .. :try_end_1c8} :catch_2b

    :goto_1c9
    :try_start_1c9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TUPIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1cc

    aput v2, v0, v1
    :try_end_1c9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c9 .. :try_end_1c9} :catch_2a

    :goto_1ca
    :try_start_1ca
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TURKISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_1ca
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ca .. :try_end_1ca} :catch_29

    :goto_1cb
    :try_start_1cb
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TURKISHOTTOMAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1cd

    aput v2, v0, v1
    :try_end_1cb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1cb .. :try_end_1cb} :catch_28

    :goto_1cc
    :try_start_1cc
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TURKMEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ce

    aput v2, v0, v1
    :try_end_1cc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1cc .. :try_end_1cc} :catch_27

    :goto_1cd
    :try_start_1cd
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TUVALU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d0

    aput v2, v0, v1
    :try_end_1cd
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1cd .. :try_end_1cd} :catch_26

    :goto_1ce
    :try_start_1ce
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TUVINIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1cf

    aput v2, v0, v1
    :try_end_1ce
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ce .. :try_end_1ce} :catch_25

    :goto_1cf
    :try_start_1cf
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_TWI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d1

    aput v2, v0, v1
    :try_end_1cf
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1cf .. :try_end_1cf} :catch_24

    :goto_1d0
    :try_start_1d0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UDMURT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d2

    aput v2, v0, v1
    :try_end_1d0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d0 .. :try_end_1d0} :catch_23

    :goto_1d1
    :try_start_1d1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UGARITIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d3

    aput v2, v0, v1
    :try_end_1d1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d1 .. :try_end_1d1} :catch_22

    :goto_1d2
    :try_start_1d2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UIGHUR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d4

    aput v2, v0, v1
    :try_end_1d2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d2 .. :try_end_1d2} :catch_21

    :goto_1d3
    :try_start_1d3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UKRAINIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d5

    aput v2, v0, v1
    :try_end_1d3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d3 .. :try_end_1d3} :catch_20

    :goto_1d4
    :try_start_1d4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UMBUNDU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d6

    aput v2, v0, v1
    :try_end_1d4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d4 .. :try_end_1d4} :catch_1f

    :goto_1d5
    :try_start_1d5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UNDETERMINED:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x30

    aput v2, v0, v1
    :try_end_1d5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d5 .. :try_end_1d5} :catch_1e

    :goto_1d6
    :try_start_1d6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UNKNOWN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x32

    aput v2, v0, v1
    :try_end_1d6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d6 .. :try_end_1d6} :catch_1d

    :goto_1d7
    :try_start_1d7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_URDU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d7

    aput v2, v0, v1
    :try_end_1d7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d7 .. :try_end_1d7} :catch_1c

    :goto_1d8
    :try_start_1d8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_UZBEK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d8

    aput v2, v0, v1
    :try_end_1d8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d8 .. :try_end_1d8} :catch_1b

    :goto_1d9
    :try_start_1d9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_VAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1d9

    aput v2, v0, v1
    :try_end_1d9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d9 .. :try_end_1d9} :catch_1a

    :goto_1da
    :try_start_1da
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_VALENCIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1f3

    aput v2, v0, v1
    :try_end_1da
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1da .. :try_end_1da} :catch_19

    :goto_1db
    :try_start_1db
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_VENDA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1da

    aput v2, v0, v1
    :try_end_1db
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1db .. :try_end_1db} :catch_18

    :goto_1dc
    :try_start_1dc
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_VIETNAMESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1db

    aput v2, v0, v1
    :try_end_1dc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1dc .. :try_end_1dc} :catch_17

    :goto_1dd
    :try_start_1dd
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_VOLAPUK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1dc

    aput v2, v0, v1
    :try_end_1dd
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1dd .. :try_end_1dd} :catch_16

    :goto_1de
    :try_start_1de
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_VOTIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1dd

    aput v2, v0, v1
    :try_end_1de
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1de .. :try_end_1de} :catch_15

    :goto_1df
    :try_start_1df
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_WAKASHAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1de

    aput v2, v0, v1
    :try_end_1df
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1df .. :try_end_1df} :catch_14

    :goto_1e0
    :try_start_1e0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_WALAMO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1df

    aput v2, v0, v1
    :try_end_1e0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e0 .. :try_end_1e0} :catch_13

    :goto_1e1
    :try_start_1e1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_WALLOON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1a2

    aput v2, v0, v1
    :try_end_1e1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e1 .. :try_end_1e1} :catch_12

    :goto_1e2
    :try_start_1e2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_WARAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e0

    aput v2, v0, v1
    :try_end_1e2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e2 .. :try_end_1e2} :catch_11

    :goto_1e3
    :try_start_1e3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_WASHO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e1

    aput v2, v0, v1
    :try_end_1e3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e3 .. :try_end_1e3} :catch_10

    :goto_1e4
    :try_start_1e4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_WELSH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_1e4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e4 .. :try_end_1e4} :catch_f

    :goto_1e5
    :try_start_1e5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_WOLOF:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e2

    aput v2, v0, v1
    :try_end_1e5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e5 .. :try_end_1e5} :catch_e

    :goto_1e6
    :try_start_1e6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_XHOSA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e4

    aput v2, v0, v1
    :try_end_1e6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e6 .. :try_end_1e6} :catch_d

    :goto_1e7
    :try_start_1e7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_YAKUT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e5

    aput v2, v0, v1
    :try_end_1e7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e7 .. :try_end_1e7} :catch_c

    :goto_1e8
    :try_start_1e8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_YAO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e6

    aput v2, v0, v1
    :try_end_1e8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e8 .. :try_end_1e8} :catch_b

    :goto_1e9
    :try_start_1e9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_YAP:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e7

    aput v2, v0, v1
    :try_end_1e9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e9 .. :try_end_1e9} :catch_a

    :goto_1ea
    :try_start_1ea
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_YIDDISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e8

    aput v2, v0, v1
    :try_end_1ea
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ea .. :try_end_1ea} :catch_9

    :goto_1eb
    :try_start_1eb
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_YORUBA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1e9

    aput v2, v0, v1
    :try_end_1eb
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1eb .. :try_end_1eb} :catch_8

    :goto_1ec
    :try_start_1ec
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_YUPIK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ea

    aput v2, v0, v1
    :try_end_1ec
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ec .. :try_end_1ec} :catch_7

    :goto_1ed
    :try_start_1ed
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ZANDE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ef

    aput v2, v0, v1
    :try_end_1ed
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ed .. :try_end_1ed} :catch_6

    :goto_1ee
    :try_start_1ee
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ZAPOTEC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1eb

    aput v2, v0, v1
    :try_end_1ee
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ee .. :try_end_1ee} :catch_5

    :goto_1ef
    :try_start_1ef
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ZAZAKI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1f2

    aput v2, v0, v1
    :try_end_1ef
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1ef .. :try_end_1ef} :catch_4

    :goto_1f0
    :try_start_1f0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ZENAGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ed

    aput v2, v0, v1
    :try_end_1f0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f0 .. :try_end_1f0} :catch_3

    :goto_1f1
    :try_start_1f1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ZHUANG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1ee

    aput v2, v0, v1
    :try_end_1f1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f1 .. :try_end_1f1} :catch_2

    :goto_1f2
    :try_start_1f2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ZULU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1f0

    aput v2, v0, v1
    :try_end_1f2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f2 .. :try_end_1f2} :catch_1

    :goto_1f3
    :try_start_1f3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ZUNI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const/16 v2, 0x1f1

    aput v2, v0, v1
    :try_end_1f3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f3 .. :try_end_1f3} :catch_0

    :goto_1f4
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumLanguage:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_1f4

    :catch_1
    move-exception v1

    goto :goto_1f3

    :catch_2
    move-exception v1

    goto :goto_1f2

    :catch_3
    move-exception v1

    goto :goto_1f1

    :catch_4
    move-exception v1

    goto :goto_1f0

    :catch_5
    move-exception v1

    goto :goto_1ef

    :catch_6
    move-exception v1

    goto :goto_1ee

    :catch_7
    move-exception v1

    goto :goto_1ed

    :catch_8
    move-exception v1

    goto :goto_1ec

    :catch_9
    move-exception v1

    goto :goto_1eb

    :catch_a
    move-exception v1

    goto :goto_1ea

    :catch_b
    move-exception v1

    goto/16 :goto_1e9

    :catch_c
    move-exception v1

    goto/16 :goto_1e8

    :catch_d
    move-exception v1

    goto/16 :goto_1e7

    :catch_e
    move-exception v1

    goto/16 :goto_1e6

    :catch_f
    move-exception v1

    goto/16 :goto_1e5

    :catch_10
    move-exception v1

    goto/16 :goto_1e4

    :catch_11
    move-exception v1

    goto/16 :goto_1e3

    :catch_12
    move-exception v1

    goto/16 :goto_1e2

    :catch_13
    move-exception v1

    goto/16 :goto_1e1

    :catch_14
    move-exception v1

    goto/16 :goto_1e0

    :catch_15
    move-exception v1

    goto/16 :goto_1df

    :catch_16
    move-exception v1

    goto/16 :goto_1de

    :catch_17
    move-exception v1

    goto/16 :goto_1dd

    :catch_18
    move-exception v1

    goto/16 :goto_1dc

    :catch_19
    move-exception v1

    goto/16 :goto_1db

    :catch_1a
    move-exception v1

    goto/16 :goto_1da

    :catch_1b
    move-exception v1

    goto/16 :goto_1d9

    :catch_1c
    move-exception v1

    goto/16 :goto_1d8

    :catch_1d
    move-exception v1

    goto/16 :goto_1d7

    :catch_1e
    move-exception v1

    goto/16 :goto_1d6

    :catch_1f
    move-exception v1

    goto/16 :goto_1d5

    :catch_20
    move-exception v1

    goto/16 :goto_1d4

    :catch_21
    move-exception v1

    goto/16 :goto_1d3

    :catch_22
    move-exception v1

    goto/16 :goto_1d2

    :catch_23
    move-exception v1

    goto/16 :goto_1d1

    :catch_24
    move-exception v1

    goto/16 :goto_1d0

    :catch_25
    move-exception v1

    goto/16 :goto_1cf

    :catch_26
    move-exception v1

    goto/16 :goto_1ce

    :catch_27
    move-exception v1

    goto/16 :goto_1cd

    :catch_28
    move-exception v1

    goto/16 :goto_1cc

    :catch_29
    move-exception v1

    goto/16 :goto_1cb

    :catch_2a
    move-exception v1

    goto/16 :goto_1ca

    :catch_2b
    move-exception v1

    goto/16 :goto_1c9

    :catch_2c
    move-exception v1

    goto/16 :goto_1c8

    :catch_2d
    move-exception v1

    goto/16 :goto_1c7

    :catch_2e
    move-exception v1

    goto/16 :goto_1c6

    :catch_2f
    move-exception v1

    goto/16 :goto_1c5

    :catch_30
    move-exception v1

    goto/16 :goto_1c4

    :catch_31
    move-exception v1

    goto/16 :goto_1c3

    :catch_32
    move-exception v1

    goto/16 :goto_1c2

    :catch_33
    move-exception v1

    goto/16 :goto_1c1

    :catch_34
    move-exception v1

    goto/16 :goto_1c0

    :catch_35
    move-exception v1

    goto/16 :goto_1bf

    :catch_36
    move-exception v1

    goto/16 :goto_1be

    :catch_37
    move-exception v1

    goto/16 :goto_1bd

    :catch_38
    move-exception v1

    goto/16 :goto_1bc

    :catch_39
    move-exception v1

    goto/16 :goto_1bb

    :catch_3a
    move-exception v1

    goto/16 :goto_1ba

    :catch_3b
    move-exception v1

    goto/16 :goto_1b9

    :catch_3c
    move-exception v1

    goto/16 :goto_1b8

    :catch_3d
    move-exception v1

    goto/16 :goto_1b7

    :catch_3e
    move-exception v1

    goto/16 :goto_1b6

    :catch_3f
    move-exception v1

    goto/16 :goto_1b5

    :catch_40
    move-exception v1

    goto/16 :goto_1b4

    :catch_41
    move-exception v1

    goto/16 :goto_1b3

    :catch_42
    move-exception v1

    goto/16 :goto_1b2

    :catch_43
    move-exception v1

    goto/16 :goto_1b1

    :catch_44
    move-exception v1

    goto/16 :goto_1b0

    :catch_45
    move-exception v1

    goto/16 :goto_1af

    :catch_46
    move-exception v1

    goto/16 :goto_1ae

    :catch_47
    move-exception v1

    goto/16 :goto_1ad

    :catch_48
    move-exception v1

    goto/16 :goto_1ac

    :catch_49
    move-exception v1

    goto/16 :goto_1ab

    :catch_4a
    move-exception v1

    goto/16 :goto_1aa

    :catch_4b
    move-exception v1

    goto/16 :goto_1a9

    :catch_4c
    move-exception v1

    goto/16 :goto_1a8

    :catch_4d
    move-exception v1

    goto/16 :goto_1a7

    :catch_4e
    move-exception v1

    goto/16 :goto_1a6

    :catch_4f
    move-exception v1

    goto/16 :goto_1a5

    :catch_50
    move-exception v1

    goto/16 :goto_1a4

    :catch_51
    move-exception v1

    goto/16 :goto_1a3

    :catch_52
    move-exception v1

    goto/16 :goto_1a2

    :catch_53
    move-exception v1

    goto/16 :goto_1a1

    :catch_54
    move-exception v1

    goto/16 :goto_1a0

    :catch_55
    move-exception v1

    goto/16 :goto_19f

    :catch_56
    move-exception v1

    goto/16 :goto_19e

    :catch_57
    move-exception v1

    goto/16 :goto_19d

    :catch_58
    move-exception v1

    goto/16 :goto_19c

    :catch_59
    move-exception v1

    goto/16 :goto_19b

    :catch_5a
    move-exception v1

    goto/16 :goto_19a

    :catch_5b
    move-exception v1

    goto/16 :goto_199

    :catch_5c
    move-exception v1

    goto/16 :goto_198

    :catch_5d
    move-exception v1

    goto/16 :goto_197

    :catch_5e
    move-exception v1

    goto/16 :goto_196

    :catch_5f
    move-exception v1

    goto/16 :goto_195

    :catch_60
    move-exception v1

    goto/16 :goto_194

    :catch_61
    move-exception v1

    goto/16 :goto_193

    :catch_62
    move-exception v1

    goto/16 :goto_192

    :catch_63
    move-exception v1

    goto/16 :goto_191

    :catch_64
    move-exception v1

    goto/16 :goto_190

    :catch_65
    move-exception v1

    goto/16 :goto_18f

    :catch_66
    move-exception v1

    goto/16 :goto_18e

    :catch_67
    move-exception v1

    goto/16 :goto_18d

    :catch_68
    move-exception v1

    goto/16 :goto_18c

    :catch_69
    move-exception v1

    goto/16 :goto_18b

    :catch_6a
    move-exception v1

    goto/16 :goto_18a

    :catch_6b
    move-exception v1

    goto/16 :goto_189

    :catch_6c
    move-exception v1

    goto/16 :goto_188

    :catch_6d
    move-exception v1

    goto/16 :goto_187

    :catch_6e
    move-exception v1

    goto/16 :goto_186

    :catch_6f
    move-exception v1

    goto/16 :goto_185

    :catch_70
    move-exception v1

    goto/16 :goto_184

    :catch_71
    move-exception v1

    goto/16 :goto_183

    :catch_72
    move-exception v1

    goto/16 :goto_182

    :catch_73
    move-exception v1

    goto/16 :goto_181

    :catch_74
    move-exception v1

    goto/16 :goto_180

    :catch_75
    move-exception v1

    goto/16 :goto_17f

    :catch_76
    move-exception v1

    goto/16 :goto_17e

    :catch_77
    move-exception v1

    goto/16 :goto_17d

    :catch_78
    move-exception v1

    goto/16 :goto_17c

    :catch_79
    move-exception v1

    goto/16 :goto_17b

    :catch_7a
    move-exception v1

    goto/16 :goto_17a

    :catch_7b
    move-exception v1

    goto/16 :goto_179

    :catch_7c
    move-exception v1

    goto/16 :goto_178

    :catch_7d
    move-exception v1

    goto/16 :goto_177

    :catch_7e
    move-exception v1

    goto/16 :goto_176

    :catch_7f
    move-exception v1

    goto/16 :goto_175

    :catch_80
    move-exception v1

    goto/16 :goto_174

    :catch_81
    move-exception v1

    goto/16 :goto_173

    :catch_82
    move-exception v1

    goto/16 :goto_172

    :catch_83
    move-exception v1

    goto/16 :goto_171

    :catch_84
    move-exception v1

    goto/16 :goto_170

    :catch_85
    move-exception v1

    goto/16 :goto_16f

    :catch_86
    move-exception v1

    goto/16 :goto_16e

    :catch_87
    move-exception v1

    goto/16 :goto_16d

    :catch_88
    move-exception v1

    goto/16 :goto_16c

    :catch_89
    move-exception v1

    goto/16 :goto_16b

    :catch_8a
    move-exception v1

    goto/16 :goto_16a

    :catch_8b
    move-exception v1

    goto/16 :goto_169

    :catch_8c
    move-exception v1

    goto/16 :goto_168

    :catch_8d
    move-exception v1

    goto/16 :goto_167

    :catch_8e
    move-exception v1

    goto/16 :goto_166

    :catch_8f
    move-exception v1

    goto/16 :goto_165

    :catch_90
    move-exception v1

    goto/16 :goto_164

    :catch_91
    move-exception v1

    goto/16 :goto_163

    :catch_92
    move-exception v1

    goto/16 :goto_162

    :catch_93
    move-exception v1

    goto/16 :goto_161

    :catch_94
    move-exception v1

    goto/16 :goto_160

    :catch_95
    move-exception v1

    goto/16 :goto_15f

    :catch_96
    move-exception v1

    goto/16 :goto_15e

    :catch_97
    move-exception v1

    goto/16 :goto_15d

    :catch_98
    move-exception v1

    goto/16 :goto_15c

    :catch_99
    move-exception v1

    goto/16 :goto_15b

    :catch_9a
    move-exception v1

    goto/16 :goto_15a

    :catch_9b
    move-exception v1

    goto/16 :goto_159

    :catch_9c
    move-exception v1

    goto/16 :goto_158

    :catch_9d
    move-exception v1

    goto/16 :goto_157

    :catch_9e
    move-exception v1

    goto/16 :goto_156

    :catch_9f
    move-exception v1

    goto/16 :goto_155

    :catch_a0
    move-exception v1

    goto/16 :goto_154

    :catch_a1
    move-exception v1

    goto/16 :goto_153

    :catch_a2
    move-exception v1

    goto/16 :goto_152

    :catch_a3
    move-exception v1

    goto/16 :goto_151

    :catch_a4
    move-exception v1

    goto/16 :goto_150

    :catch_a5
    move-exception v1

    goto/16 :goto_14f

    :catch_a6
    move-exception v1

    goto/16 :goto_14e

    :catch_a7
    move-exception v1

    goto/16 :goto_14d

    :catch_a8
    move-exception v1

    goto/16 :goto_14c

    :catch_a9
    move-exception v1

    goto/16 :goto_14b

    :catch_aa
    move-exception v1

    goto/16 :goto_14a

    :catch_ab
    move-exception v1

    goto/16 :goto_149

    :catch_ac
    move-exception v1

    goto/16 :goto_148

    :catch_ad
    move-exception v1

    goto/16 :goto_147

    :catch_ae
    move-exception v1

    goto/16 :goto_146

    :catch_af
    move-exception v1

    goto/16 :goto_145

    :catch_b0
    move-exception v1

    goto/16 :goto_144

    :catch_b1
    move-exception v1

    goto/16 :goto_143

    :catch_b2
    move-exception v1

    goto/16 :goto_142

    :catch_b3
    move-exception v1

    goto/16 :goto_141

    :catch_b4
    move-exception v1

    goto/16 :goto_140

    :catch_b5
    move-exception v1

    goto/16 :goto_13f

    :catch_b6
    move-exception v1

    goto/16 :goto_13e

    :catch_b7
    move-exception v1

    goto/16 :goto_13d

    :catch_b8
    move-exception v1

    goto/16 :goto_13c

    :catch_b9
    move-exception v1

    goto/16 :goto_13b

    :catch_ba
    move-exception v1

    goto/16 :goto_13a

    :catch_bb
    move-exception v1

    goto/16 :goto_139

    :catch_bc
    move-exception v1

    goto/16 :goto_138

    :catch_bd
    move-exception v1

    goto/16 :goto_137

    :catch_be
    move-exception v1

    goto/16 :goto_136

    :catch_bf
    move-exception v1

    goto/16 :goto_135

    :catch_c0
    move-exception v1

    goto/16 :goto_134

    :catch_c1
    move-exception v1

    goto/16 :goto_133

    :catch_c2
    move-exception v1

    goto/16 :goto_132

    :catch_c3
    move-exception v1

    goto/16 :goto_131

    :catch_c4
    move-exception v1

    goto/16 :goto_130

    :catch_c5
    move-exception v1

    goto/16 :goto_12f

    :catch_c6
    move-exception v1

    goto/16 :goto_12e

    :catch_c7
    move-exception v1

    goto/16 :goto_12d

    :catch_c8
    move-exception v1

    goto/16 :goto_12c

    :catch_c9
    move-exception v1

    goto/16 :goto_12b

    :catch_ca
    move-exception v1

    goto/16 :goto_12a

    :catch_cb
    move-exception v1

    goto/16 :goto_129

    :catch_cc
    move-exception v1

    goto/16 :goto_128

    :catch_cd
    move-exception v1

    goto/16 :goto_127

    :catch_ce
    move-exception v1

    goto/16 :goto_126

    :catch_cf
    move-exception v1

    goto/16 :goto_125

    :catch_d0
    move-exception v1

    goto/16 :goto_124

    :catch_d1
    move-exception v1

    goto/16 :goto_123

    :catch_d2
    move-exception v1

    goto/16 :goto_122

    :catch_d3
    move-exception v1

    goto/16 :goto_121

    :catch_d4
    move-exception v1

    goto/16 :goto_120

    :catch_d5
    move-exception v1

    goto/16 :goto_11f

    :catch_d6
    move-exception v1

    goto/16 :goto_11e

    :catch_d7
    move-exception v1

    goto/16 :goto_11d

    :catch_d8
    move-exception v1

    goto/16 :goto_11c

    :catch_d9
    move-exception v1

    goto/16 :goto_11b

    :catch_da
    move-exception v1

    goto/16 :goto_11a

    :catch_db
    move-exception v1

    goto/16 :goto_119

    :catch_dc
    move-exception v1

    goto/16 :goto_118

    :catch_dd
    move-exception v1

    goto/16 :goto_117

    :catch_de
    move-exception v1

    goto/16 :goto_116

    :catch_df
    move-exception v1

    goto/16 :goto_115

    :catch_e0
    move-exception v1

    goto/16 :goto_114

    :catch_e1
    move-exception v1

    goto/16 :goto_113

    :catch_e2
    move-exception v1

    goto/16 :goto_112

    :catch_e3
    move-exception v1

    goto/16 :goto_111

    :catch_e4
    move-exception v1

    goto/16 :goto_110

    :catch_e5
    move-exception v1

    goto/16 :goto_10f

    :catch_e6
    move-exception v1

    goto/16 :goto_10e

    :catch_e7
    move-exception v1

    goto/16 :goto_10d

    :catch_e8
    move-exception v1

    goto/16 :goto_10c

    :catch_e9
    move-exception v1

    goto/16 :goto_10b

    :catch_ea
    move-exception v1

    goto/16 :goto_10a

    :catch_eb
    move-exception v1

    goto/16 :goto_109

    :catch_ec
    move-exception v1

    goto/16 :goto_108

    :catch_ed
    move-exception v1

    goto/16 :goto_107

    :catch_ee
    move-exception v1

    goto/16 :goto_106

    :catch_ef
    move-exception v1

    goto/16 :goto_105

    :catch_f0
    move-exception v1

    goto/16 :goto_104

    :catch_f1
    move-exception v1

    goto/16 :goto_103

    :catch_f2
    move-exception v1

    goto/16 :goto_102

    :catch_f3
    move-exception v1

    goto/16 :goto_101

    :catch_f4
    move-exception v1

    goto/16 :goto_100

    :catch_f5
    move-exception v1

    goto/16 :goto_ff

    :catch_f6
    move-exception v1

    goto/16 :goto_fe

    :catch_f7
    move-exception v1

    goto/16 :goto_fd

    :catch_f8
    move-exception v1

    goto/16 :goto_fc

    :catch_f9
    move-exception v1

    goto/16 :goto_fb

    :catch_fa
    move-exception v1

    goto/16 :goto_fa

    :catch_fb
    move-exception v1

    goto/16 :goto_f9

    :catch_fc
    move-exception v1

    goto/16 :goto_f8

    :catch_fd
    move-exception v1

    goto/16 :goto_f7

    :catch_fe
    move-exception v1

    goto/16 :goto_f6

    :catch_ff
    move-exception v1

    goto/16 :goto_f5

    :catch_100
    move-exception v1

    goto/16 :goto_f4

    :catch_101
    move-exception v1

    goto/16 :goto_f3

    :catch_102
    move-exception v1

    goto/16 :goto_f2

    :catch_103
    move-exception v1

    goto/16 :goto_f1

    :catch_104
    move-exception v1

    goto/16 :goto_f0

    :catch_105
    move-exception v1

    goto/16 :goto_ef

    :catch_106
    move-exception v1

    goto/16 :goto_ee

    :catch_107
    move-exception v1

    goto/16 :goto_ed

    :catch_108
    move-exception v1

    goto/16 :goto_ec

    :catch_109
    move-exception v1

    goto/16 :goto_eb

    :catch_10a
    move-exception v1

    goto/16 :goto_ea

    :catch_10b
    move-exception v1

    goto/16 :goto_e9

    :catch_10c
    move-exception v1

    goto/16 :goto_e8

    :catch_10d
    move-exception v1

    goto/16 :goto_e7

    :catch_10e
    move-exception v1

    goto/16 :goto_e6

    :catch_10f
    move-exception v1

    goto/16 :goto_e5

    :catch_110
    move-exception v1

    goto/16 :goto_e4

    :catch_111
    move-exception v1

    goto/16 :goto_e3

    :catch_112
    move-exception v1

    goto/16 :goto_e2

    :catch_113
    move-exception v1

    goto/16 :goto_e1

    :catch_114
    move-exception v1

    goto/16 :goto_e0

    :catch_115
    move-exception v1

    goto/16 :goto_df

    :catch_116
    move-exception v1

    goto/16 :goto_de

    :catch_117
    move-exception v1

    goto/16 :goto_dd

    :catch_118
    move-exception v1

    goto/16 :goto_dc

    :catch_119
    move-exception v1

    goto/16 :goto_db

    :catch_11a
    move-exception v1

    goto/16 :goto_da

    :catch_11b
    move-exception v1

    goto/16 :goto_d9

    :catch_11c
    move-exception v1

    goto/16 :goto_d8

    :catch_11d
    move-exception v1

    goto/16 :goto_d7

    :catch_11e
    move-exception v1

    goto/16 :goto_d6

    :catch_11f
    move-exception v1

    goto/16 :goto_d5

    :catch_120
    move-exception v1

    goto/16 :goto_d4

    :catch_121
    move-exception v1

    goto/16 :goto_d3

    :catch_122
    move-exception v1

    goto/16 :goto_d2

    :catch_123
    move-exception v1

    goto/16 :goto_d1

    :catch_124
    move-exception v1

    goto/16 :goto_d0

    :catch_125
    move-exception v1

    goto/16 :goto_cf

    :catch_126
    move-exception v1

    goto/16 :goto_ce

    :catch_127
    move-exception v1

    goto/16 :goto_cd

    :catch_128
    move-exception v1

    goto/16 :goto_cc

    :catch_129
    move-exception v1

    goto/16 :goto_cb

    :catch_12a
    move-exception v1

    goto/16 :goto_ca

    :catch_12b
    move-exception v1

    goto/16 :goto_c9

    :catch_12c
    move-exception v1

    goto/16 :goto_c8

    :catch_12d
    move-exception v1

    goto/16 :goto_c7

    :catch_12e
    move-exception v1

    goto/16 :goto_c6

    :catch_12f
    move-exception v1

    goto/16 :goto_c5

    :catch_130
    move-exception v1

    goto/16 :goto_c4

    :catch_131
    move-exception v1

    goto/16 :goto_c3

    :catch_132
    move-exception v1

    goto/16 :goto_c2

    :catch_133
    move-exception v1

    goto/16 :goto_c1

    :catch_134
    move-exception v1

    goto/16 :goto_c0

    :catch_135
    move-exception v1

    goto/16 :goto_bf

    :catch_136
    move-exception v1

    goto/16 :goto_be

    :catch_137
    move-exception v1

    goto/16 :goto_bd

    :catch_138
    move-exception v1

    goto/16 :goto_bc

    :catch_139
    move-exception v1

    goto/16 :goto_bb

    :catch_13a
    move-exception v1

    goto/16 :goto_ba

    :catch_13b
    move-exception v1

    goto/16 :goto_b9

    :catch_13c
    move-exception v1

    goto/16 :goto_b8

    :catch_13d
    move-exception v1

    goto/16 :goto_b7

    :catch_13e
    move-exception v1

    goto/16 :goto_b6

    :catch_13f
    move-exception v1

    goto/16 :goto_b5

    :catch_140
    move-exception v1

    goto/16 :goto_b4

    :catch_141
    move-exception v1

    goto/16 :goto_b3

    :catch_142
    move-exception v1

    goto/16 :goto_b2

    :catch_143
    move-exception v1

    goto/16 :goto_b1

    :catch_144
    move-exception v1

    goto/16 :goto_b0

    :catch_145
    move-exception v1

    goto/16 :goto_af

    :catch_146
    move-exception v1

    goto/16 :goto_ae

    :catch_147
    move-exception v1

    goto/16 :goto_ad

    :catch_148
    move-exception v1

    goto/16 :goto_ac

    :catch_149
    move-exception v1

    goto/16 :goto_ab

    :catch_14a
    move-exception v1

    goto/16 :goto_aa

    :catch_14b
    move-exception v1

    goto/16 :goto_a9

    :catch_14c
    move-exception v1

    goto/16 :goto_a8

    :catch_14d
    move-exception v1

    goto/16 :goto_a7

    :catch_14e
    move-exception v1

    goto/16 :goto_a6

    :catch_14f
    move-exception v1

    goto/16 :goto_a5

    :catch_150
    move-exception v1

    goto/16 :goto_a4

    :catch_151
    move-exception v1

    goto/16 :goto_a3

    :catch_152
    move-exception v1

    goto/16 :goto_a2

    :catch_153
    move-exception v1

    goto/16 :goto_a1

    :catch_154
    move-exception v1

    goto/16 :goto_a0

    :catch_155
    move-exception v1

    goto/16 :goto_9f

    :catch_156
    move-exception v1

    goto/16 :goto_9e

    :catch_157
    move-exception v1

    goto/16 :goto_9d

    :catch_158
    move-exception v1

    goto/16 :goto_9c

    :catch_159
    move-exception v1

    goto/16 :goto_9b

    :catch_15a
    move-exception v1

    goto/16 :goto_9a

    :catch_15b
    move-exception v1

    goto/16 :goto_99

    :catch_15c
    move-exception v1

    goto/16 :goto_98

    :catch_15d
    move-exception v1

    goto/16 :goto_97

    :catch_15e
    move-exception v1

    goto/16 :goto_96

    :catch_15f
    move-exception v1

    goto/16 :goto_95

    :catch_160
    move-exception v1

    goto/16 :goto_94

    :catch_161
    move-exception v1

    goto/16 :goto_93

    :catch_162
    move-exception v1

    goto/16 :goto_92

    :catch_163
    move-exception v1

    goto/16 :goto_91

    :catch_164
    move-exception v1

    goto/16 :goto_90

    :catch_165
    move-exception v1

    goto/16 :goto_8f

    :catch_166
    move-exception v1

    goto/16 :goto_8e

    :catch_167
    move-exception v1

    goto/16 :goto_8d

    :catch_168
    move-exception v1

    goto/16 :goto_8c

    :catch_169
    move-exception v1

    goto/16 :goto_8b

    :catch_16a
    move-exception v1

    goto/16 :goto_8a

    :catch_16b
    move-exception v1

    goto/16 :goto_89

    :catch_16c
    move-exception v1

    goto/16 :goto_88

    :catch_16d
    move-exception v1

    goto/16 :goto_87

    :catch_16e
    move-exception v1

    goto/16 :goto_86

    :catch_16f
    move-exception v1

    goto/16 :goto_85

    :catch_170
    move-exception v1

    goto/16 :goto_84

    :catch_171
    move-exception v1

    goto/16 :goto_83

    :catch_172
    move-exception v1

    goto/16 :goto_82

    :catch_173
    move-exception v1

    goto/16 :goto_81

    :catch_174
    move-exception v1

    goto/16 :goto_80

    :catch_175
    move-exception v1

    goto/16 :goto_7f

    :catch_176
    move-exception v1

    goto/16 :goto_7e

    :catch_177
    move-exception v1

    goto/16 :goto_7d

    :catch_178
    move-exception v1

    goto/16 :goto_7c

    :catch_179
    move-exception v1

    goto/16 :goto_7b

    :catch_17a
    move-exception v1

    goto/16 :goto_7a

    :catch_17b
    move-exception v1

    goto/16 :goto_79

    :catch_17c
    move-exception v1

    goto/16 :goto_78

    :catch_17d
    move-exception v1

    goto/16 :goto_77

    :catch_17e
    move-exception v1

    goto/16 :goto_76

    :catch_17f
    move-exception v1

    goto/16 :goto_75

    :catch_180
    move-exception v1

    goto/16 :goto_74

    :catch_181
    move-exception v1

    goto/16 :goto_73

    :catch_182
    move-exception v1

    goto/16 :goto_72

    :catch_183
    move-exception v1

    goto/16 :goto_71

    :catch_184
    move-exception v1

    goto/16 :goto_70

    :catch_185
    move-exception v1

    goto/16 :goto_6f

    :catch_186
    move-exception v1

    goto/16 :goto_6e

    :catch_187
    move-exception v1

    goto/16 :goto_6d

    :catch_188
    move-exception v1

    goto/16 :goto_6c

    :catch_189
    move-exception v1

    goto/16 :goto_6b

    :catch_18a
    move-exception v1

    goto/16 :goto_6a

    :catch_18b
    move-exception v1

    goto/16 :goto_69

    :catch_18c
    move-exception v1

    goto/16 :goto_68

    :catch_18d
    move-exception v1

    goto/16 :goto_67

    :catch_18e
    move-exception v1

    goto/16 :goto_66

    :catch_18f
    move-exception v1

    goto/16 :goto_65

    :catch_190
    move-exception v1

    goto/16 :goto_64

    :catch_191
    move-exception v1

    goto/16 :goto_63

    :catch_192
    move-exception v1

    goto/16 :goto_62

    :catch_193
    move-exception v1

    goto/16 :goto_61

    :catch_194
    move-exception v1

    goto/16 :goto_60

    :catch_195
    move-exception v1

    goto/16 :goto_5f

    :catch_196
    move-exception v1

    goto/16 :goto_5e

    :catch_197
    move-exception v1

    goto/16 :goto_5d

    :catch_198
    move-exception v1

    goto/16 :goto_5c

    :catch_199
    move-exception v1

    goto/16 :goto_5b

    :catch_19a
    move-exception v1

    goto/16 :goto_5a

    :catch_19b
    move-exception v1

    goto/16 :goto_59

    :catch_19c
    move-exception v1

    goto/16 :goto_58

    :catch_19d
    move-exception v1

    goto/16 :goto_57

    :catch_19e
    move-exception v1

    goto/16 :goto_56

    :catch_19f
    move-exception v1

    goto/16 :goto_55

    :catch_1a0
    move-exception v1

    goto/16 :goto_54

    :catch_1a1
    move-exception v1

    goto/16 :goto_53

    :catch_1a2
    move-exception v1

    goto/16 :goto_52

    :catch_1a3
    move-exception v1

    goto/16 :goto_51

    :catch_1a4
    move-exception v1

    goto/16 :goto_50

    :catch_1a5
    move-exception v1

    goto/16 :goto_4f

    :catch_1a6
    move-exception v1

    goto/16 :goto_4e

    :catch_1a7
    move-exception v1

    goto/16 :goto_4d

    :catch_1a8
    move-exception v1

    goto/16 :goto_4c

    :catch_1a9
    move-exception v1

    goto/16 :goto_4b

    :catch_1aa
    move-exception v1

    goto/16 :goto_4a

    :catch_1ab
    move-exception v1

    goto/16 :goto_49

    :catch_1ac
    move-exception v1

    goto/16 :goto_48

    :catch_1ad
    move-exception v1

    goto/16 :goto_47

    :catch_1ae
    move-exception v1

    goto/16 :goto_46

    :catch_1af
    move-exception v1

    goto/16 :goto_45

    :catch_1b0
    move-exception v1

    goto/16 :goto_44

    :catch_1b1
    move-exception v1

    goto/16 :goto_43

    :catch_1b2
    move-exception v1

    goto/16 :goto_42

    :catch_1b3
    move-exception v1

    goto/16 :goto_41

    :catch_1b4
    move-exception v1

    goto/16 :goto_40

    :catch_1b5
    move-exception v1

    goto/16 :goto_3f

    :catch_1b6
    move-exception v1

    goto/16 :goto_3e

    :catch_1b7
    move-exception v1

    goto/16 :goto_3d

    :catch_1b8
    move-exception v1

    goto/16 :goto_3c

    :catch_1b9
    move-exception v1

    goto/16 :goto_3b

    :catch_1ba
    move-exception v1

    goto/16 :goto_3a

    :catch_1bb
    move-exception v1

    goto/16 :goto_39

    :catch_1bc
    move-exception v1

    goto/16 :goto_38

    :catch_1bd
    move-exception v1

    goto/16 :goto_37

    :catch_1be
    move-exception v1

    goto/16 :goto_36

    :catch_1bf
    move-exception v1

    goto/16 :goto_35

    :catch_1c0
    move-exception v1

    goto/16 :goto_34

    :catch_1c1
    move-exception v1

    goto/16 :goto_33

    :catch_1c2
    move-exception v1

    goto/16 :goto_32

    :catch_1c3
    move-exception v1

    goto/16 :goto_31

    :catch_1c4
    move-exception v1

    goto/16 :goto_30

    :catch_1c5
    move-exception v1

    goto/16 :goto_2f

    :catch_1c6
    move-exception v1

    goto/16 :goto_2e

    :catch_1c7
    move-exception v1

    goto/16 :goto_2d

    :catch_1c8
    move-exception v1

    goto/16 :goto_2c

    :catch_1c9
    move-exception v1

    goto/16 :goto_2b

    :catch_1ca
    move-exception v1

    goto/16 :goto_2a

    :catch_1cb
    move-exception v1

    goto/16 :goto_29

    :catch_1cc
    move-exception v1

    goto/16 :goto_28

    :catch_1cd
    move-exception v1

    goto/16 :goto_27

    :catch_1ce
    move-exception v1

    goto/16 :goto_26

    :catch_1cf
    move-exception v1

    goto/16 :goto_25

    :catch_1d0
    move-exception v1

    goto/16 :goto_24

    :catch_1d1
    move-exception v1

    goto/16 :goto_23

    :catch_1d2
    move-exception v1

    goto/16 :goto_22

    :catch_1d3
    move-exception v1

    goto/16 :goto_21

    :catch_1d4
    move-exception v1

    goto/16 :goto_20

    :catch_1d5
    move-exception v1

    goto/16 :goto_1f

    :catch_1d6
    move-exception v1

    goto/16 :goto_1e

    :catch_1d7
    move-exception v1

    goto/16 :goto_1d

    :catch_1d8
    move-exception v1

    goto/16 :goto_1c

    :catch_1d9
    move-exception v1

    goto/16 :goto_1b

    :catch_1da
    move-exception v1

    goto/16 :goto_1a

    :catch_1db
    move-exception v1

    goto/16 :goto_19

    :catch_1dc
    move-exception v1

    goto/16 :goto_18

    :catch_1dd
    move-exception v1

    goto/16 :goto_17

    :catch_1de
    move-exception v1

    goto/16 :goto_16

    :catch_1df
    move-exception v1

    goto/16 :goto_15

    :catch_1e0
    move-exception v1

    goto/16 :goto_14

    :catch_1e1
    move-exception v1

    goto/16 :goto_13

    :catch_1e2
    move-exception v1

    goto/16 :goto_12

    :catch_1e3
    move-exception v1

    goto/16 :goto_11

    :catch_1e4
    move-exception v1

    goto/16 :goto_10

    :catch_1e5
    move-exception v1

    goto/16 :goto_f

    :catch_1e6
    move-exception v1

    goto/16 :goto_e

    :catch_1e7
    move-exception v1

    goto/16 :goto_d

    :catch_1e8
    move-exception v1

    goto/16 :goto_c

    :catch_1e9
    move-exception v1

    goto/16 :goto_b

    :catch_1ea
    move-exception v1

    goto/16 :goto_a

    :catch_1eb
    move-exception v1

    goto/16 :goto_9

    :catch_1ec
    move-exception v1

    goto/16 :goto_8

    :catch_1ed
    move-exception v1

    goto/16 :goto_7

    :catch_1ee
    move-exception v1

    goto/16 :goto_6

    :catch_1ef
    move-exception v1

    goto/16 :goto_5

    :catch_1f0
    move-exception v1

    goto/16 :goto_4

    :catch_1f1
    move-exception v1

    goto/16 :goto_3

    :catch_1f2
    move-exception v1

    goto/16 :goto_2

    :catch_1f3
    move-exception v1

    goto/16 :goto_1
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    const-string v3, "SettingServiceImpl constructor!!"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    iget-short v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    mul-int/lit8 v1, v0, 0x5

    add-int/lit8 v1, v1, 0x5

    sput v1, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    sget v1, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    add-int/lit8 v1, v1, 0xa

    sput v1, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    :cond_0
    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    const v1, 0xffff

    sput v1, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    :cond_1
    return-void
.end method

.method private chmodFile(Ljava/io/File;)V
    .locals 5
    .param p1    # Ljava/io/File;

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chmod 666 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "zyl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "command = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v2, "zyl"

    const-string v3, "chmod fail!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 5
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-direct {p0, v1, p2}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_0
    invoke-direct {p0, p2}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->chmodFile(Ljava/io/File;)V

    return v2

    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    const-string v3, "copyFile(File srcFile, File destFile)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 7
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/File;

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-gez v1, :cond_1

    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/FileDescriptor;->sync()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    const/4 v4, 0x1

    :goto_2
    return v4

    :cond_1
    :try_start_5
    const-string v5, " out.write(buffer, 0, bytesRead);"

    const-string v6, " out.write(buffer, 0, bytesRead);"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/FileDescriptor;->sync()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    :goto_3
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    throw v5
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    :catch_0
    move-exception v2

    const-string v5, "copyToFile(InputStream inputStream, File destFile)"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_1
    move-exception v5

    goto :goto_1

    :catch_2
    move-exception v6

    goto :goto_3
.end method

.method public static getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->settingMgrImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->settingMgrImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->settingMgrImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    return-object v0
.end method


# virtual methods
.method public ExecRestoreToDefault()Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v3, Ljava/io/File;

    const-string v4, "/tvdatabase/DatabaseBackup/"

    const-string v5, "user_setting.db"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    const-string v4, "/tvdatabase/Database/"

    const-string v5, "user_setting.db"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3, v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v1

    const/4 v1, 0x0

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :cond_0
    return v2
.end method

.method public GetBlueScreenFlag()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bBlueScreen:Z

    return v0
.end method

.method public GetCecStatus()Lcom/mstar/android/tvapi/common/vo/CecSetting;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCecManager()Lcom/mstar/android/tvapi/common/CecManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/CecManager;->getCecConfiguration()Lcom/mstar/android/tvapi/common/vo/CecSetting;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public GetChannelSWMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;
    .locals 4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetChannelSWMode Parameter:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    return-object v0
.end method

.method public GetColorRange()S
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    return v0
.end method

.method public GetEnvironmentPowerOnLogoMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;
    .locals 3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->E_POWERON_LOGO_OFF:Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getEnvironmentPowerOnLogoMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public GetEnvironmentPowerOnMusicMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;
    .locals 3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->E_POWERON_MUSIC_OFF:Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getEnvironmentPowerOnMusicMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public GetEnvironmentPowerOnMusicVolume()S
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->getEnvironmentPowerOnMusicVolume()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "GetEnvironmentPowerOnMusicVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Get Music Volume: 0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public GetFilmMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    return-object v0
.end method

.method public GetOffDetMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;
    .locals 4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetOffDetMode Parameter:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eOffDetMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eOffDetMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    return-object v0
.end method

.method public GetOsdDuration()S
    .locals 5

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetOsdDuration Parameter:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v4

    iget-short v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    iget-wide v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    long-to-int v0, v1

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    const/4 v2, 0x5

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    :goto_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    iget-short v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    return v1

    :sswitch_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    const/4 v2, 0x0

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    const/4 v2, 0x1

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    const/4 v2, 0x2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    const/4 v2, 0x3

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    const/4 v2, 0x4

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_1
        0xf -> :sswitch_2
        0x14 -> :sswitch_3
        0x1e -> :sswitch_4
    .end sparse-switch
.end method

.method public GetOsdLanguage()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .locals 4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetOsdLanguage Parameter:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    return-object v0
.end method

.method public GetSystemAutoTimeType()I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8SystemAutoTimeType:I

    return v0
.end method

.method public SetBlueScreenFlag(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-boolean p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bBlueScreen:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public SetCecStatus(Lcom/mstar/android/tvapi/common/vo/CecSetting;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/CecSetting;

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCecManager()Lcom/mstar/android/tvapi/common/CecManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/CecManager;->setCecConfiguration(Lcom/mstar/android/tvapi/common/vo/CecSetting;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public SetChannelSWMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;)Z
    .locals 5
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    const-string v3, "SetOsdLanguage SetChannelSWMode: Nothing to do in mock!!"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    iput-object p1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_BLACKSCREEN:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    if-ne p1, v1, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setChannelChangeFreezeMode(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setChannelChangeFreezeMode(Z)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetColorRanger(S)Z
    .locals 4
    .param p1    # S

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v2

    iput-short p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->autoHDMIColorRange()Z

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->setColorRange(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public SetEnvironmentPowerOnLogoMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironmentPowerOnLogoMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "SetEnvironmentPowerOnLogoMode"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Set Logo Mode:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetEnvironmentPowerOnMusicMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironmentPowerOnMusicMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "SetEnvironmentPowerOnMusicMode"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Set Music Mode:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetEnvironmentPowerOnMusicVolume(S)Z
    .locals 5
    .param p1    # S

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setEnvironmentPowerOnMusicVolume(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "SetEnvironmentPowerOnMusicVolume"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Set Music Volume: 0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetFilmMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;)Z
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v1

    iput-object p1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->values()[Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    move-result-object v2

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;->ordinal()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setFilm(Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetOffDetMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;)Z
    .locals 3
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "SetOsdLanguage SetOffDetMode: Nothing to do in mock!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-object p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eOffDetMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public SetOsdDuration(S)Z
    .locals 5
    .param p1    # S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "SetOsdDuration Parameter: Nothing to do in mock!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-short p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    mul-int/lit8 v1, p1, 0x5

    add-int/lit8 v1, v1, 0x5

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    iput-wide v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    mul-int/lit8 v0, p1, 0x5

    add-int/lit8 v0, v0, 0x5

    sput v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-wide v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    const-wide/16 v3, 0x1388

    add-long/2addr v1, v3

    iput-wide v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    sget v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    add-int/lit8 v0, v0, 0x5

    sput v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    :cond_0
    const/4 v0, 0x5

    if-lt p1, v0, :cond_1

    const v0, 0xffff

    sput v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    const-wide/32 v1, 0xffff

    iput-wide v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    :cond_1
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SetOsdDuration : TimeOut = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v3

    iget-wide v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public SetOsdLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v2

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSubtitleSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    move-result-object v2

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSubtitleSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    move-result-object v2

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumLanguage()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSubtitleSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSubtitleSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v2, 0x1

    return v2

    :sswitch_0
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    goto :goto_0

    :sswitch_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1b -> :sswitch_0
        0x35 -> :sswitch_1
    .end sparse-switch
.end method

.method public SetSystemAutoTimeType(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8SystemAutoTimeType:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public getBurnInMode()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getFactoryExt()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->bBurnIn:Z

    return v0
.end method

.method public getColorWheelMode()Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    return-object v0
.end method

.method public getOsdTimeoutSecond()I
    .locals 4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-wide v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    sput v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    sget v0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->menuTimeOut:I

    return v0
.end method

.method public getScreenSaveModeStatus()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->screenSaveMode:Z

    return v0
.end method

.method public getSmartEnergySaving()Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    return-object v0
.end method

.method public getStandbyNoOperation()I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoOperation:I

    return v0
.end method

.method public getStandbyNoSignal()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoSignal:Z

    return v0
.end method

.method public setColorWheelMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;)Z
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v3

    iput-object p1, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_COLOR_ROLL_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    if-ne p1, v3, :cond_1

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_ROLL_Index_Main;->PQ_GRule_COLOR_ROLL_Off_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_ROLL_Index_Main;

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->ordinal()I

    move-result v4

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_ROLL_Index_Main;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/PictureManager;->setStatusByCustomerPqRule(II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v3, 0x1

    return v3

    :cond_1
    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_ROLL_Index_Main;->PQ_GRule_COLOR_ROLL_On_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_ROLL_Index_Main;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setScreenSaveModeStatus(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-boolean p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->screenSaveMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setSmartEnergySaving(Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;)Z
    .locals 2
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-object p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setStandbyNoOperation(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoOperation:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setStandbyNoSignal(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iput-boolean p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoSignal:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method
