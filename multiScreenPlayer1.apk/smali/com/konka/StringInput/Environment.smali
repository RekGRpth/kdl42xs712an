.class public Lcom/konka/StringInput/Environment;
.super Ljava/lang/Object;
.source "Environment.java"


# static fields
.field private static final CANDIDATES_AREA_HEIGHT_RATIO_LANDSCAPE:F = 0.125f

.field private static final CANDIDATES_AREA_HEIGHT_RATIO_PORTRAIT:F = 0.084f

.field private static final FUNCTION_BALLOON_TEXT_SIZE_RATIO:F = 0.085f

.field private static final FUNCTION_KEY_TEXT_SIZE_RATIO:F = 0.055f

.field private static final KEY_BALLOON_HEIGHT_PLUS_RATIO:F = 0.07f

.field private static final KEY_BALLOON_WIDTH_PLUS_RATIO:F = 0.08f

.field private static final KEY_HEIGHT_RATIO_LANDSCAPE:F = 0.147f

.field private static final KEY_HEIGHT_RATIO_PORTRAIT:F = 0.105f

.field private static final NORMAL_BALLOON_TEXT_SIZE_RATIO:F = 0.14f

.field private static final NORMAL_KEY_TEXT_SIZE_RATIO:F = 0.075f

.field private static mInstance:Lcom/konka/StringInput/Environment;


# instance fields
.field private mCandidatesAreaHeight:I

.field private mConfig:Landroid/content/res/Configuration;

.field private mDebug:Z

.field private mFunctionBalloonTextSize:I

.field private mFunctionKeyTextSize:I

.field private mKeyBalloonHeightPlus:I

.field private mKeyBalloonWidthPlus:I

.field private mKeyHeight:I

.field private mNormalBalloonTextSize:I

.field private mNormalKeyTextSize:I

.field private mScreenHeight:I

.field private mScreenWidth:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/StringInput/Environment;->mDebug:Z

    return-void
.end method

.method public static getInstance()Lcom/konka/StringInput/Environment;
    .locals 1

    sget-object v0, Lcom/konka/StringInput/Environment;->mInstance:Lcom/konka/StringInput/Environment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/StringInput/Environment;

    invoke-direct {v0}, Lcom/konka/StringInput/Environment;-><init>()V

    sput-object v0, Lcom/konka/StringInput/Environment;->mInstance:Lcom/konka/StringInput/Environment;

    :cond_0
    sget-object v0, Lcom/konka/StringInput/Environment;->mInstance:Lcom/konka/StringInput/Environment;

    return-object v0
.end method


# virtual methods
.method public getBalloonTextSize(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/konka/StringInput/Environment;->mFunctionBalloonTextSize:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/konka/StringInput/Environment;->mNormalBalloonTextSize:I

    goto :goto_0
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
    .locals 1

    iget-object v0, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    return-object v0
.end method

.method public getHeightForCandidates()I
    .locals 1

    iget v0, p0, Lcom/konka/StringInput/Environment;->mCandidatesAreaHeight:I

    return v0
.end method

.method public getKeyBalloonHeightPlus()I
    .locals 1

    iget v0, p0, Lcom/konka/StringInput/Environment;->mKeyBalloonHeightPlus:I

    return v0
.end method

.method public getKeyBalloonWidthPlus()I
    .locals 1

    iget v0, p0, Lcom/konka/StringInput/Environment;->mKeyBalloonWidthPlus:I

    return v0
.end method

.method public getKeyHeight()I
    .locals 1

    iget v0, p0, Lcom/konka/StringInput/Environment;->mKeyHeight:I

    return v0
.end method

.method public getKeyTextSize(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/konka/StringInput/Environment;->mFunctionKeyTextSize:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/konka/StringInput/Environment;->mNormalKeyTextSize:I

    goto :goto_0
.end method

.method public getKeyXMarginFactor()F
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getKeyYMarginFactor()F
    .locals 2

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    const v0, 0x3f333333    # 0.7f

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getScreenHeight()I
    .locals 1

    iget v0, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    return v0
.end method

.method public getScreenWidth()I
    .locals 1

    iget v0, p0, Lcom/konka/StringInput/Environment;->mScreenWidth:I

    return v0
.end method

.method public getSkbHeight()I
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/konka/StringInput/Environment;->mKeyHeight:I

    mul-int/lit8 v0, v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/konka/StringInput/Environment;->mKeyHeight:I

    mul-int/lit8 v0, v0, 0x4

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHardKeyboard()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->keyboard:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    iget v1, v1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method public needDebug()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/StringInput/Environment;->mDebug:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/res/Configuration;
    .param p2    # Landroid/content/Context;

    iget-object v3, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iget v4, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v3, v4, :cond_0

    const-string v3, "window"

    invoke-virtual {p2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mScreenWidth:I

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    iget v3, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    iget v4, p0, Lcom/konka/StringInput/Environment;->mScreenWidth:I

    if-le v3, v4, :cond_1

    iget v3, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    int-to-float v3, v3

    const v4, 0x3dd70a3d    # 0.105f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mKeyHeight:I

    iget v3, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    int-to-float v3, v3

    const v4, 0x3dac0831    # 0.084f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mCandidatesAreaHeight:I

    iget v1, p0, Lcom/konka/StringInput/Environment;->mScreenWidth:I

    :goto_0
    int-to-float v3, v1

    const v4, 0x3d99999a    # 0.075f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mNormalKeyTextSize:I

    int-to-float v3, v1

    const v4, 0x3d6147ae    # 0.055f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mFunctionKeyTextSize:I

    int-to-float v3, v1

    const v4, 0x3e0f5c29    # 0.14f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mNormalBalloonTextSize:I

    int-to-float v3, v1

    const v4, 0x3dae147b    # 0.085f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mFunctionBalloonTextSize:I

    int-to-float v3, v1

    const v4, 0x3da3d70a    # 0.08f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mKeyBalloonWidthPlus:I

    int-to-float v3, v1

    const v4, 0x3d8f5c29    # 0.07f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mKeyBalloonHeightPlus:I

    :cond_0
    iget-object v3, p0, Lcom/konka/StringInput/Environment;->mConfig:Landroid/content/res/Configuration;

    invoke-virtual {v3, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    return-void

    :cond_1
    iget v3, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    int-to-float v3, v3

    const v4, 0x3e16872b    # 0.147f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mKeyHeight:I

    iget v3, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    int-to-float v3, v3

    const/high16 v4, 0x3e000000    # 0.125f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/konka/StringInput/Environment;->mCandidatesAreaHeight:I

    iget v1, p0, Lcom/konka/StringInput/Environment;->mScreenHeight:I

    goto :goto_0
.end method
