.class Lcom/konka/StringInput/IMEService$StringReceiver;
.super Landroid/content/BroadcastReceiver;
.source "IMEService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/StringInput/IMEService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StringReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/StringInput/IMEService;


# direct methods
.method private constructor <init>(Lcom/konka/StringInput/IMEService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/StringInput/IMEService;Lcom/konka/StringInput/IMEService$StringReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/StringInput/IMEService$StringReceiver;-><init>(Lcom/konka/StringInput/IMEService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "result"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "stop"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v4, "com.konka.multiScreen.InputMethod"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "result ="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    iget-object v4, v4, Lcom/konka/StringInput/IMEService;->curInputMethodId:Ljava/lang/String;

    const-string v5, "com.konka.mediaSharePlayer/com.konka.StringInput.IMEService"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    invoke-virtual {v4}, Lcom/konka/StringInput/IMEService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "default_input_method"

    const-string v6, "com.iflytek.inputmethod.TV/.FlyIME"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :goto_0
    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090008    # com.konka.mediaSharePlayer.R.string.ime_close

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    invoke-virtual {v4}, Lcom/konka/StringInput/IMEService;->stopSelf()V

    :cond_0
    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    invoke-virtual {v4}, Lcom/konka/StringInput/IMEService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    invoke-virtual {v4}, Lcom/konka/StringInput/IMEService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "default_input_method"

    iget-object v6, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    iget-object v6, v6, Lcom/konka/StringInput/IMEService;->curInputMethodId:Ljava/lang/String;

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    # getter for: Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;
    invoke-static {v4}, Lcom/konka/StringInput/IMEService;->access$0(Lcom/konka/StringInput/IMEService;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_4

    iget-object v4, p0, Lcom/konka/StringInput/IMEService$StringReceiver;->this$0:Lcom/konka/StringInput/IMEService;

    # invokes: Lcom/konka/StringInput/IMEService;->commitTyped(Landroid/view/inputmethod/InputConnection;)V
    invoke-static {v4, v1}, Lcom/konka/StringInput/IMEService;->access$1(Lcom/konka/StringInput/IMEService;Landroid/view/inputmethod/InputConnection;)V

    :cond_4
    invoke-interface {v1, v3, v7}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_1
.end method
