.class public Lcom/konka/musicPlayer/MusicInfo;
.super Ljava/lang/Object;
.source "MusicInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x2196242faf37f41dL


# instance fields
.field private artist:Ljava/lang/String;

.field private data:Ljava/lang/String;

.field private desplayName:Ljava/lang/String;

.field private duration:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private size:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/konka/musicPlayer/MusicInfo;->artist:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/musicPlayer/MusicInfo;->title:Ljava/lang/String;

    iput-object p4, p0, Lcom/konka/musicPlayer/MusicInfo;->data:Ljava/lang/String;

    iput-object p5, p0, Lcom/konka/musicPlayer/MusicInfo;->desplayName:Ljava/lang/String;

    iput-object p6, p0, Lcom/konka/musicPlayer/MusicInfo;->duration:Ljava/lang/String;

    iput-object p7, p0, Lcom/konka/musicPlayer/MusicInfo;->size:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lcom/konka/musicPlayer/MusicInfo;

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicInfo;->artist:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicInfo;->title:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicInfo;->data:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicInfo;->desplayName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getDesplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicInfo;->duration:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getDuration()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicInfo;->size:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getSize()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicInfo;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicInfo;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getDesplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicInfo;->desplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicInfo;->duration:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicInfo;->size:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->artist:Ljava/lang/String;

    return-void
.end method

.method public setData(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->data:Ljava/lang/String;

    return-void
.end method

.method public setDesplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->desplayName:Ljava/lang/String;

    return-void
.end method

.method public setDuration(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->duration:Ljava/lang/String;

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->id:Ljava/lang/String;

    return-void
.end method

.method public setSize(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->size:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicInfo;->title:Ljava/lang/String;

    return-void
.end method
