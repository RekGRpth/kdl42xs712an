.class Lcom/konka/musicPlayer/musicPlayerActivity$3;
.super Ljava/lang/Object;
.source "musicPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const v2, 0x7f0900a5    # com.konka.mediaSharePlayer.R.string.forbidoption

    const v5, 0x7f02001e    # com.konka.mediaSharePlayer.R.drawable.com_play_sel

    const v4, 0x7f020015    # com.konka.mediaSharePlayer.R.drawable.com_pause_sel

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$9(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v6

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return v7

    :sswitch_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_STOP:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&ma"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->stopMusicService()V
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$10(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput v7, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->finish()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$11(Lcom/konka/musicPlayer/musicPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$12(Lcom/konka/musicPlayer/musicPlayerActivity;)Z

    move-result v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnFocus(Z)V
    invoke-static {v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$13(Lcom/konka/musicPlayer/musicPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$9(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$14(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v0

    if-ne v0, v7, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f020016    # com.konka.mediaSharePlayer.R.drawable.com_pause_uns

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f020021    # com.konka.mediaSharePlayer.R.drawable.com_play_uns

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v7, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$14(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v0

    if-ne v0, v7, :cond_2

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v7, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$9(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->progressSpeed:I
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$15(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v0

    sub-int/2addr v6, v0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playPos(I)V
    invoke-static {v0, v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$16(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$9(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->progressSpeed:I
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$15(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v0

    add-int/2addr v6, v0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playPos(I)V
    invoke-static {v0, v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$16(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$14(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v0

    if-ne v0, v7, :cond_5

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v3, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$3(Lcom/konka/musicPlayer/musicPlayerActivity;ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v7, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$8(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v7, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$3(Lcom/konka/musicPlayer/musicPlayerActivity;ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$3;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$8(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
        0x42 -> :sswitch_5
        0x6f -> :sswitch_0
        0x8a -> :sswitch_5
    .end sparse-switch
.end method
