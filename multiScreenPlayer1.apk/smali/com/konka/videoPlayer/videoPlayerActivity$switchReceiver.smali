.class Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "videoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "switchReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v1, "mediaSwitch"

    const/4 v2, 0x1

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaSwitch:I

    const-string v0, "media stop"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-mediaSwitch-------------video--mediaSwitch: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v2, v2, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaSwitch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaSwitch:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SWITCH_PLAYER:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&mv"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerStop()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$9(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    :cond_0
    return-void
.end method
