.class Lcom/konka/videoPlayer/videoPlayerActivity$7;
.super Ljava/lang/Object;
.source "videoPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;->initVideoPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    return-object v0
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 9
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const v8, 0x7f090080    # com.konka.mediaSharePlayer.R.string.videoformaterror

    const/4 v7, 0x1

    const/4 v5, -0x2

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mTimesError:I
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$45(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$46(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput-boolean v6, v2, Lcom/konka/videoPlayer/videoPlayerActivity;->isSend:Z

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput v6, v2, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->cancelProgressDlg()V

    const-string v2, "weikan"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "times of error!:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mTimesError:I
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$45(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mTimesError:I
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$45(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0900a7    # com.konka.mediaSharePlayer.R.string.file_downLoad_error

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->showProgressDialog(II)V
    invoke-static {v2, v8, v8}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$47(Lcom/konka/videoPlayer/videoPlayerActivity;II)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$36(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x4

    const-wide/16 v4, 0x1770

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    const-string v2, "VideoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onError ID="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p3, :sswitch_data_0

    :sswitch_0
    return v6

    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$48(Lcom/konka/videoPlayer/videoPlayerActivity;Landroid/app/AlertDialog;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900a9    # com.konka.mediaSharePlayer.R.string.tips_title

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900a8    # com.konka.mediaSharePlayer.R.string.video_file_error

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09003a    # com.konka.mediaSharePlayer.R.string.MM_IS_QUIT_APP

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;

    invoke-direct {v4, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity$7;)V

    invoke-virtual {v2, v5, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x138b -> :sswitch_0
        -0x138a -> :sswitch_0
        -0x1389 -> :sswitch_0
        -0x7d1 -> :sswitch_0
        -0x3f2 -> :sswitch_0
        -0x3ef -> :sswitch_0
        -0x3ec -> :sswitch_0
        -0x3e9 -> :sswitch_0
    .end sparse-switch
.end method
