.class Lcom/konka/videoPlayer/videoPlayerActivity$8;
.super Ljava/lang/Object;
.source "videoPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;->initVideoPlayer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/videoPlayer/videoPlayerActivity$8;)Lcom/konka/videoPlayer/videoPlayerActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    return-object v0
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->cancelProgressDlg()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    const/16 v1, 0xff

    invoke-static {v3, v3, v1, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setBackgroundColor(I)V

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mVideoView.getDuration():"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-static {v0, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$46(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/videoPlayer/videoPlayerActivity$8$1;

    invoke-direct {v2, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$8$1;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity$8;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/videoPlayer/videoPlayerActivity$8$2;

    invoke-direct {v2, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$8$2;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity$8;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->hideThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$8;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->hideController()V

    return-void
.end method
