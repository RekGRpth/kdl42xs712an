.class Lcom/konka/videoPlayer/VideoPlayerPlayController$1;
.super Ljava/lang/Object;
.source "VideoPlayerPlayController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/videoPlayer/VideoPlayerPlayController;->findBtnView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$1;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$1;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$4(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "duration: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$1;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    iput p2, v0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->current:I

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$1;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$0(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method
