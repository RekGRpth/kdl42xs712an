.class Lcom/konka/videoPlayer/VideoView$2;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/VideoView;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/VideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, v2}, Lcom/konka/videoPlayer/VideoView;->access$5(Lcom/konka/videoPlayer/VideoView;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$6(Lcom/konka/videoPlayer/VideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$6(Lcom/konka/videoPlayer/VideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v1, v1, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/MediaController;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/videoPlayer/VideoView;->access$0(Lcom/konka/videoPlayer/VideoView;I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/videoPlayer/VideoView;->access$1(Lcom/konka/videoPlayer/VideoView;I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$3(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$4(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoView;->access$3(Lcom/konka/videoPlayer/VideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I
    invoke-static {v2}, Lcom/konka/videoPlayer/VideoView;->access$4(Lcom/konka/videoPlayer/VideoView;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSurfaceWidth:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$8(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoView;->access$3(Lcom/konka/videoPlayer/VideoView;)I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSurfaceHeight:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$9(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoView;->access$4(Lcom/konka/videoPlayer/VideoView;)I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$10(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoView;->access$10(Lcom/konka/videoPlayer/VideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, v3}, Lcom/konka/videoPlayer/VideoView;->access$11(Lcom/konka/videoPlayer/VideoView;I)V

    :cond_2
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$12(Lcom/konka/videoPlayer/VideoView;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, v3}, Lcom/konka/videoPlayer/VideoView;->access$13(Lcom/konka/videoPlayer/VideoView;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$10(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    if-lez v0, :cond_3

    :cond_5
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/MediaController;->show(I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$10(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoView;->access$10(Lcom/konka/videoPlayer/VideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, v3}, Lcom/konka/videoPlayer/VideoView;->access$11(Lcom/konka/videoPlayer/VideoView;I)V

    :cond_7
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$12(Lcom/konka/videoPlayer/VideoView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$2;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, v3}, Lcom/konka/videoPlayer/VideoView;->access$13(Lcom/konka/videoPlayer/VideoView;Z)V

    goto :goto_0
.end method
