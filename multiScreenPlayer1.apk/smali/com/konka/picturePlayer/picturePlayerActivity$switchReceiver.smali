.class Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "picturePlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "switchReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method private constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v2, "mediaSwitch"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaSwitch:I

    const-string v1, "media stop"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "-mediaSwitch------------picture---mediaSwitch: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaSwitch:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v1, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaSwitch:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-static {v1, v4, v4, v4}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    :cond_0
    return-void
.end method
