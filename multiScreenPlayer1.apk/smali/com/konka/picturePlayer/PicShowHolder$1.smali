.class Lcom/konka/picturePlayer/PicShowHolder$1;
.super Ljava/lang/Object;
.source "PicShowHolder.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/PicShowHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/PicShowHolder;


# direct methods
.method constructor <init>(Lcom/konka/picturePlayer/PicShowHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, v1, Lcom/konka/picturePlayer/PicShowHolder;->text_leferotion:Landroid/widget/TextView;

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/picturePlayer/PicShowHolder;->access$0(Lcom/konka/picturePlayer/PicShowHolder;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, v1, Lcom/konka/picturePlayer/PicShowHolder;->text_rightrotion:Landroid/widget/TextView;

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/picturePlayer/PicShowHolder;->access$0(Lcom/konka/picturePlayer/PicShowHolder;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, v1, Lcom/konka/picturePlayer/PicShowHolder;->text_descale:Landroid/widget/TextView;

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/picturePlayer/PicShowHolder;->access$0(Lcom/konka/picturePlayer/PicShowHolder;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder$1;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v1, v1, Lcom/konka/picturePlayer/PicShowHolder;->text_enlarge:Landroid/widget/TextView;

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/picturePlayer/PicShowHolder;->access$0(Lcom/konka/picturePlayer/PicShowHolder;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b0014
        :pswitch_1    # com.konka.mediaSharePlayer.R.id.buttomitem6
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.textview_item6
        :pswitch_2    # com.konka.mediaSharePlayer.R.id.buttomitem7
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.textview_item7
        :pswitch_3    # com.konka.mediaSharePlayer.R.id.buttomitem8
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.textview_item8
        :pswitch_4    # com.konka.mediaSharePlayer.R.id.buttomitem9
    .end packed-switch
.end method
