.class Lcom/konka/picturePlayer/picturePlayerActivity$5;
.super Ljava/lang/Object;
.source "picturePlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;->creatImageFile()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$picturePlayer$picturePlayerActivity$DOWNLOAD_STATE:[I


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$picturePlayer$picturePlayerActivity$DOWNLOAD_STATE()[I
    .locals 3

    sget-object v0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->$SWITCH_TABLE$com$konka$picturePlayer$picturePlayerActivity$DOWNLOAD_STATE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->values()[Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_continue:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    invoke-virtual {v1}, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_file_err:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    invoke-virtual {v1}, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_file_lage:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    invoke-virtual {v1}, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_finish:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    invoke-virtual {v1}, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->$SWITCH_TABLE$com$konka$picturePlayer$picturePlayerActivity$DOWNLOAD_STATE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v1, 0x14

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->down(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-static {}, Lcom/konka/picturePlayer/picturePlayerActivity$5;->$SWITCH_TABLE$com$konka$picturePlayer$picturePlayerActivity$DOWNLOAD_STATE()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    invoke-virtual {v2}, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath1:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->down(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->downloadhandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$11(Lcom/konka/picturePlayer/picturePlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->downloadhandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$11(Lcom/konka/picturePlayer/picturePlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$5;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->downloadhandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$11(Lcom/konka/picturePlayer/picturePlayerActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
