.class public Landroid/net/samba/SmbShareFolder;
.super Landroid/net/samba/SambaFile;
.source "SmbShareFolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/samba/SmbShareFolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private device:Landroid/net/samba/SmbDevice;

.field private mFolderName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/net/samba/SmbShareFolder$1;

    invoke-direct {v0}, Landroid/net/samba/SmbShareFolder$1;-><init>()V

    sput-object v0, Landroid/net/samba/SmbShareFolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/samba/SmbDevice;)V
    .locals 1
    .param p1    # Landroid/net/samba/SmbDevice;

    invoke-direct {p0}, Landroid/net/samba/SambaFile;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    iput-object p1, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    return-void
.end method

.method public constructor <init>(Landroid/net/samba/SmbDevice;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/net/samba/SmbDevice;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Landroid/net/samba/SambaFile;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    iput-object p1, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    iput-object p2, p0, Landroid/net/samba/SmbShareFolder;->mFolderName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Landroid/net/samba/SambaFile;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/samba/SmbShareFolder;->mFolderName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public canRead()Z
    .locals 1

    invoke-super {p0}, Landroid/net/samba/SambaFile;->canRead()Z

    move-result v0

    return v0
.end method

.method public canWrite()Z
    .locals 1

    invoke-super {p0}, Landroid/net/samba/SambaFile;->canWrite()Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAuth()Landroid/net/samba/SmbAuthentication;
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    invoke-virtual {v0}, Landroid/net/samba/SmbDevice;->getAuth()Landroid/net/samba/SmbAuthentication;

    move-result-object v0

    return-object v0
.end method

.method public getSmbDevice()Landroid/net/samba/SmbDevice;
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    return-object v0
.end method

.method public isMounted()Z
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    invoke-virtual {v0}, Landroid/net/samba/SmbDevice;->isMounted()Z

    move-result v0

    return v0
.end method

.method public localPath()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Landroid/net/samba/SmbShareFolder;->getSmbDevice()Landroid/net/samba/SmbDevice;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/samba/SmbDevice;->localPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/net/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public mount(Landroid/net/samba/SmbAuthentication;I)V
    .locals 1
    .param p1    # Landroid/net/samba/SmbAuthentication;
    .param p2    # I

    iget-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    invoke-virtual {v0, p1, p2}, Landroid/net/samba/SmbDevice;->mount(Landroid/net/samba/SmbAuthentication;I)I

    return-void
.end method

.method public remotePath()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/net/samba/SmbShareFolder;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unmount()V
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SmbShareFolder;->device:Landroid/net/samba/SmbDevice;

    invoke-virtual {v0}, Landroid/net/samba/SmbDevice;->unmount()I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Landroid/net/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
