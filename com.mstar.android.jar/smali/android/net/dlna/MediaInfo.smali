.class public Landroid/net/dlna/MediaInfo;
.super Ljava/lang/Object;
.source "MediaInfo.java"


# instance fields
.field current_uri:Ljava/lang/String;

.field current_uri_metadata:Ljava/lang/String;

.field media_duration:Ljava/lang/String;

.field next_uri:Ljava/lang/String;

.field next_uri_metadata:Ljava/lang/String;

.field nr_tracks:I

.field play_medium:Ljava/lang/String;

.field record_medium:Ljava/lang/String;

.field write_status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/net/dlna/MediaInfo;->nr_tracks:I

    iput-object p2, p0, Landroid/net/dlna/MediaInfo;->media_duration:Ljava/lang/String;

    iput-object p3, p0, Landroid/net/dlna/MediaInfo;->current_uri:Ljava/lang/String;

    iput-object p4, p0, Landroid/net/dlna/MediaInfo;->current_uri_metadata:Ljava/lang/String;

    iput-object p5, p0, Landroid/net/dlna/MediaInfo;->next_uri:Ljava/lang/String;

    iput-object p6, p0, Landroid/net/dlna/MediaInfo;->next_uri_metadata:Ljava/lang/String;

    iput-object p7, p0, Landroid/net/dlna/MediaInfo;->play_medium:Ljava/lang/String;

    iput-object p8, p0, Landroid/net/dlna/MediaInfo;->record_medium:Ljava/lang/String;

    iput-object p9, p0, Landroid/net/dlna/MediaInfo;->write_status:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCurrentURI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->current_uri:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentURIMetadata()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->current_uri_metadata:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaDuration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->media_duration:Ljava/lang/String;

    return-object v0
.end method

.method public getNRTracks()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/MediaInfo;->nr_tracks:I

    return v0
.end method

.method public getNextURI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->next_uri:Ljava/lang/String;

    return-object v0
.end method

.method public getNextURIMetadata()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->next_uri_metadata:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayMedium()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->play_medium:Ljava/lang/String;

    return-object v0
.end method

.method public getRecordMedium()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->record_medium:Ljava/lang/String;

    return-object v0
.end method

.method public getWritestatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaInfo;->write_status:Ljava/lang/String;

    return-object v0
.end method

.method public setCurrentURI(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->current_uri:Ljava/lang/String;

    return-void
.end method

.method public setCurrentURIMetadata(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->current_uri_metadata:Ljava/lang/String;

    return-void
.end method

.method public setMediaDuration(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->media_duration:Ljava/lang/String;

    return-void
.end method

.method public setNRTracks(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/MediaInfo;->nr_tracks:I

    return-void
.end method

.method public setNextURI(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->next_uri:Ljava/lang/String;

    return-void
.end method

.method public setNextURIMetadata(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->next_uri_metadata:Ljava/lang/String;

    return-void
.end method

.method public setPlayMedium(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->play_medium:Ljava/lang/String;

    return-void
.end method

.method public setRecordMedium(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->record_medium:Ljava/lang/String;

    return-void
.end method

.method public setWritestatus(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaInfo;->write_status:Ljava/lang/String;

    return-void
.end method
