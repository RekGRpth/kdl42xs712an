.class Landroid/net/dlna/MediaRendererDeviceImpl;
.super Ljava/lang/Object;
.source "DLNAImpl.java"

# interfaces
.implements Landroid/net/dlna/MediaRendererDevice;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synchronized native declared-synchronized JNI_DMR_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetDeviceCapabilities(ILandroid/net/dlna/DeviceCapabilities;)V
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetListener(Landroid/net/dlna/MediaRendererDeviceListener;)V
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetMediaInfo(ILandroid/net/dlna/MediaInfo;)V
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetPositionInfo(ILandroid/net/dlna/PositionInfo;)V
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetProtocolInfo(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetTransportInfo(ILandroid/net/dlna/TransportInfo;)V
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetTransportSettings(ILandroid/net/dlna/TransportSettings;)V
.end method

.method private synchronized native declared-synchronized JNI_DMR_SetVolume(ILandroid/net/dlna/VolumeInfo;)V
.end method


# virtual methods
.method public GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public SetDeviceCapabilities(ILandroid/net/dlna/DeviceCapabilities;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/DeviceCapabilities;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetDeviceCapabilities(ILandroid/net/dlna/DeviceCapabilities;)V

    return-void
.end method

.method public SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V
    .locals 0
    .param p1    # Landroid/net/dlna/DeviceInfo;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V

    return-void
.end method

.method public SetListener(Landroid/net/dlna/MediaRendererDeviceListener;)V
    .locals 0
    .param p1    # Landroid/net/dlna/MediaRendererDeviceListener;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetListener(Landroid/net/dlna/MediaRendererDeviceListener;)V

    return-void
.end method

.method public SetMediaInfo(ILandroid/net/dlna/MediaInfo;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/MediaInfo;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetMediaInfo(ILandroid/net/dlna/MediaInfo;)V

    return-void
.end method

.method public SetPositionInfo(ILandroid/net/dlna/PositionInfo;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/PositionInfo;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetPositionInfo(ILandroid/net/dlna/PositionInfo;)V

    return-void
.end method

.method public SetProtocolInfo(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetProtocolInfo(Ljava/util/ArrayList;)V

    return-void
.end method

.method public SetTransportInfo(ILandroid/net/dlna/TransportInfo;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/TransportInfo;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetTransportInfo(ILandroid/net/dlna/TransportInfo;)V

    return-void
.end method

.method public SetTransportSettings(ILandroid/net/dlna/TransportSettings;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/TransportSettings;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetTransportSettings(ILandroid/net/dlna/TransportSettings;)V

    return-void
.end method

.method public SetVolume(ILandroid/net/dlna/VolumeInfo;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/VolumeInfo;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererDeviceImpl;->JNI_DMR_SetVolume(ILandroid/net/dlna/VolumeInfo;)V

    return-void
.end method
