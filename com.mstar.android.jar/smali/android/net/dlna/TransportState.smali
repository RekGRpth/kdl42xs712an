.class public final enum Landroid/net/dlna/TransportState;
.super Ljava/lang/Enum;
.source "TransportState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/dlna/TransportState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/dlna/TransportState;

.field public static final enum NO_MEDIA_PRESENT:Landroid/net/dlna/TransportState;

.field public static final enum PAUSED_PLAYBACK:Landroid/net/dlna/TransportState;

.field public static final enum PLAYING:Landroid/net/dlna/TransportState;

.field public static final enum STOPPED:Landroid/net/dlna/TransportState;

.field public static final enum TRANSITIONING:Landroid/net/dlna/TransportState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/net/dlna/TransportState;

    const-string v1, "NO_MEDIA_PRESENT"

    invoke-direct {v0, v1, v2}, Landroid/net/dlna/TransportState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/TransportState;->NO_MEDIA_PRESENT:Landroid/net/dlna/TransportState;

    new-instance v0, Landroid/net/dlna/TransportState;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v3}, Landroid/net/dlna/TransportState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/TransportState;->STOPPED:Landroid/net/dlna/TransportState;

    new-instance v0, Landroid/net/dlna/TransportState;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v4}, Landroid/net/dlna/TransportState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/TransportState;->PLAYING:Landroid/net/dlna/TransportState;

    new-instance v0, Landroid/net/dlna/TransportState;

    const-string v1, "TRANSITIONING"

    invoke-direct {v0, v1, v5}, Landroid/net/dlna/TransportState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/TransportState;->TRANSITIONING:Landroid/net/dlna/TransportState;

    new-instance v0, Landroid/net/dlna/TransportState;

    const-string v1, "PAUSED_PLAYBACK"

    invoke-direct {v0, v1, v6}, Landroid/net/dlna/TransportState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/TransportState;->PAUSED_PLAYBACK:Landroid/net/dlna/TransportState;

    const/4 v0, 0x5

    new-array v0, v0, [Landroid/net/dlna/TransportState;

    sget-object v1, Landroid/net/dlna/TransportState;->NO_MEDIA_PRESENT:Landroid/net/dlna/TransportState;

    aput-object v1, v0, v2

    sget-object v1, Landroid/net/dlna/TransportState;->STOPPED:Landroid/net/dlna/TransportState;

    aput-object v1, v0, v3

    sget-object v1, Landroid/net/dlna/TransportState;->PLAYING:Landroid/net/dlna/TransportState;

    aput-object v1, v0, v4

    sget-object v1, Landroid/net/dlna/TransportState;->TRANSITIONING:Landroid/net/dlna/TransportState;

    aput-object v1, v0, v5

    sget-object v1, Landroid/net/dlna/TransportState;->PAUSED_PLAYBACK:Landroid/net/dlna/TransportState;

    aput-object v1, v0, v6

    sput-object v0, Landroid/net/dlna/TransportState;->$VALUES:[Landroid/net/dlna/TransportState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/dlna/TransportState;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Landroid/net/dlna/TransportState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/net/dlna/TransportState;

    return-object v0
.end method

.method public static values()[Landroid/net/dlna/TransportState;
    .locals 1

    sget-object v0, Landroid/net/dlna/TransportState;->$VALUES:[Landroid/net/dlna/TransportState;

    invoke-virtual {v0}, [Landroid/net/dlna/TransportState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/dlna/TransportState;

    return-object v0
.end method
