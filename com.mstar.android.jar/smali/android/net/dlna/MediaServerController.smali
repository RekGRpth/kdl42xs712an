.class public interface abstract Landroid/net/dlna/MediaServerController;
.super Ljava/lang/Object;
.source "MediaServerController.java"


# virtual methods
.method public abstract Authenticate(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;,
            Landroid/net/dlna/AuthenticateFailedException;
        }
    .end annotation
.end method

.method public abstract Browse(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;,
            Landroid/net/dlna/MissingAuthenticationException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract Browse_Ex(Ljava/lang/String;Landroid/net/dlna/BrowseFlag;Ljava/lang/String;IILjava/lang/String;)Landroid/net/dlna/BrowseResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;,
            Landroid/net/dlna/MissingAuthenticationException;
        }
    .end annotation
.end method

.method public abstract CreateObject(Ljava/lang/String;Ljava/lang/String;)Landroid/net/dlna/ShareItem;
.end method

.method public abstract GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetProtocolInfo()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract TransferFile(Ljava/lang/String;Ljava/lang/String;)I
.end method
