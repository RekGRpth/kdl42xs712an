.class public Landroid/net/dlna/ShareItem;
.super Landroid/net/dlna/ShareObject;
.source "ShareItem.java"


# instance fields
.field private share_resource:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/net/dlna/ShareObject;-><init>()V

    return-void
.end method


# virtual methods
.method public getShareResource()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareResource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/net/dlna/ShareItem;->share_resource:Ljava/util/ArrayList;

    return-object v0
.end method
