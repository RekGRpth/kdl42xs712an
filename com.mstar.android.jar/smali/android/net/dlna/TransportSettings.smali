.class public Landroid/net/dlna/TransportSettings;
.super Ljava/lang/Object;
.source "TransportSettings.java"


# instance fields
.field private play_mode:Landroid/net/dlna/PlayMode;

.field private rec_quality_mode:Landroid/net/dlna/RecordQualityMode;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/net/dlna/PlayMode;Landroid/net/dlna/RecordQualityMode;)V
    .locals 0
    .param p1    # Landroid/net/dlna/PlayMode;
    .param p2    # Landroid/net/dlna/RecordQualityMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/net/dlna/TransportSettings;->play_mode:Landroid/net/dlna/PlayMode;

    iput-object p2, p0, Landroid/net/dlna/TransportSettings;->rec_quality_mode:Landroid/net/dlna/RecordQualityMode;

    return-void
.end method


# virtual methods
.method public getPlayMode()Landroid/net/dlna/PlayMode;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/TransportSettings;->play_mode:Landroid/net/dlna/PlayMode;

    return-object v0
.end method

.method public getRecordQualityMode()Landroid/net/dlna/RecordQualityMode;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/TransportSettings;->rec_quality_mode:Landroid/net/dlna/RecordQualityMode;

    return-object v0
.end method

.method public setPlayMode(Landroid/net/dlna/PlayMode;)V
    .locals 0
    .param p1    # Landroid/net/dlna/PlayMode;

    iput-object p1, p0, Landroid/net/dlna/TransportSettings;->play_mode:Landroid/net/dlna/PlayMode;

    return-void
.end method

.method public setRecordQualityMode(Landroid/net/dlna/RecordQualityMode;)V
    .locals 0
    .param p1    # Landroid/net/dlna/RecordQualityMode;

    iput-object p1, p0, Landroid/net/dlna/TransportSettings;->rec_quality_mode:Landroid/net/dlna/RecordQualityMode;

    return-void
.end method
