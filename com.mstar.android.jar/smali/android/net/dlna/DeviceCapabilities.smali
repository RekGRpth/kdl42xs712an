.class public Landroid/net/dlna/DeviceCapabilities;
.super Ljava/lang/Object;
.source "DeviceCapabilities.java"


# instance fields
.field private play_media:Ljava/lang/String;

.field private rec_media:Ljava/lang/String;

.field private rec_quality_modes:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/net/dlna/DeviceCapabilities;->play_media:Ljava/lang/String;

    iput-object p2, p0, Landroid/net/dlna/DeviceCapabilities;->rec_media:Ljava/lang/String;

    iput-object p3, p0, Landroid/net/dlna/DeviceCapabilities;->rec_quality_modes:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPlayMedia()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/DeviceCapabilities;->play_media:Ljava/lang/String;

    return-object v0
.end method

.method public getRecMedia()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/DeviceCapabilities;->rec_media:Ljava/lang/String;

    return-object v0
.end method

.method public getRecQualityModes()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/DeviceCapabilities;->rec_quality_modes:Ljava/lang/String;

    return-object v0
.end method

.method public setPlayMedia(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/DeviceCapabilities;->play_media:Ljava/lang/String;

    return-void
.end method

.method public setRecMedia(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/DeviceCapabilities;->rec_media:Ljava/lang/String;

    return-void
.end method

.method public setRecQualityModes(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/DeviceCapabilities;->rec_quality_modes:Ljava/lang/String;

    return-void
.end method
