.class Landroid/net/dlna/MediaServerDeviceImpl;
.super Ljava/lang/Object;
.source "DLNAImpl.java"

# interfaces
.implements Landroid/net/dlna/MediaServerDevice;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synchronized native declared-synchronized DMSDisableUpload()I
.end method

.method private synchronized native declared-synchronized DMSEnableUpload()I
.end method

.method private synchronized native declared-synchronized DMSSetUploadItemSaveDir(Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized JNI_DMS_AddMediaFile(Ljava/lang/String;Landroid/net/dlna/MediaMetaData;)V
.end method

.method private synchronized native declared-synchronized JNI_DMS_Browse(Ljava/lang/String;)Landroid/net/dlna/ShareItem;
.end method

.method private synchronized native declared-synchronized JNI_DMS_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
.end method

.method private synchronized native declared-synchronized JNI_DMS_RemoveMediaFile(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized JNI_DMS_SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V
.end method

.method private synchronized native declared-synchronized JNI_DMS_SetListener(Landroid/net/dlna/MediaServerDeviceListener;)V
.end method

.method private synchronized native declared-synchronized JNI_DMS_SetPassword(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized JNI_DMS_SetProtocolInfo(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;)V"
        }
    .end annotation
.end method


# virtual methods
.method public AddMediaFile(Ljava/lang/String;Landroid/net/dlna/MediaMetaData;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/dlna/MediaMetaData;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_AddMediaFile(Ljava/lang/String;Landroid/net/dlna/MediaMetaData;)V

    return-void
.end method

.method public Browse(Ljava/lang/String;)Landroid/net/dlna/ShareItem;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_Browse(Ljava/lang/String;)Landroid/net/dlna/ShareItem;

    move-result-object v0

    return-object v0
.end method

.method public DisableUpload()I
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/MediaServerDeviceImpl;->DMSDisableUpload()I

    move-result v0

    return v0
.end method

.method public EnableUpload()I
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/MediaServerDeviceImpl;->DMSEnableUpload()I

    move-result v0

    return v0
.end method

.method public GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public RemoveMediaFile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_RemoveMediaFile(Ljava/lang/String;)V

    return-void
.end method

.method public SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V
    .locals 0
    .param p1    # Landroid/net/dlna/DeviceInfo;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V

    return-void
.end method

.method public SetListener(Landroid/net/dlna/MediaServerDeviceListener;)V
    .locals 0
    .param p1    # Landroid/net/dlna/MediaServerDeviceListener;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_SetListener(Landroid/net/dlna/MediaServerDeviceListener;)V

    return-void
.end method

.method public SetPassword(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_SetPassword(Ljava/lang/String;)V

    return-void
.end method

.method public SetProtocolInfo(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerDeviceImpl;->JNI_DMS_SetProtocolInfo(Ljava/util/ArrayList;)V

    return-void
.end method

.method public SetUploadItemSaveDir(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerDeviceImpl;->DMSSetUploadItemSaveDir(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
