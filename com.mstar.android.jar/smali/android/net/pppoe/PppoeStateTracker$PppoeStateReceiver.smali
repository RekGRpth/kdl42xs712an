.class Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PppoeStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/pppoe/PppoeStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PppoeStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/pppoe/PppoeStateTracker;


# direct methods
.method private constructor <init>(Landroid/net/pppoe/PppoeStateTracker;)V
    .locals 0

    iput-object p1, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/net/pppoe/PppoeStateTracker;Landroid/net/pppoe/PppoeStateTracker$1;)V
    .locals 0
    .param p1    # Landroid/net/pppoe/PppoeStateTracker;
    .param p2    # Landroid/net/pppoe/PppoeStateTracker$1;

    invoke-direct {p0, p1}, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;-><init>(Landroid/net/pppoe/PppoeStateTracker;)V

    return-void
.end method

.method private isCableAvailable(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/sys/class/net/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/carrier"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->isCableStatusAvailable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isCableStatusAvailable(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->readCableStatus(Ljava/lang/String;)C

    move-result v0

    const/16 v1, 0x31

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readCableStatus(Ljava/lang/String;)C
    .locals 6
    .param p1    # Ljava/lang/String;

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/Reader;->read()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    :goto_0
    :try_start_2
    invoke-virtual {v3}, Ljava/io/Reader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_0
    :goto_1
    int-to-char v5, v4

    return v5

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v2, v3

    :goto_2
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catchall_0
    move-exception v5

    :goto_3
    :try_start_6
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :goto_4
    throw v5

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :catchall_1
    move-exception v5

    move-object v2, v3

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # invokes: Landroid/net/pppoe/PppoeStateTracker;->checkPppoeManager()V
    invoke-static {v3}, Landroid/net/pppoe/PppoeStateTracker;->access$100(Landroid/net/pppoe/PppoeStateTracker;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.net.pppoe.PPPOE_STATE_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "PppoeStatus"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # invokes: Landroid/net/pppoe/PppoeStateTracker;->notifyStateChange(Ljava/lang/String;)V
    invoke-static {v3, v2}, Landroid/net/pppoe/PppoeStateTracker;->access$200(Landroid/net/pppoe/PppoeStateTracker;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "ETHERNET_state"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    const-string v3, "PppoeStateTracker"

    const-string v4, "receive EthernetStateTracker.EVENT_HW_DISCONNECTED"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # getter for: Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;
    invoke-static {v3}, Landroid/net/pppoe/PppoeStateTracker;->access$300(Landroid/net/pppoe/PppoeStateTracker;)Landroid/net/pppoe/PppoeManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/pppoe/PppoeManager;->getPppoeStatus()Ljava/lang/String;

    move-result-object v3

    const-string v4, "connect"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # getter for: Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;
    invoke-static {v3}, Landroid/net/pppoe/PppoeStateTracker;->access$300(Landroid/net/pppoe/PppoeStateTracker;)Landroid/net/pppoe/PppoeManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/pppoe/PppoeManager;->PppoeGetInterface()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v3, "eth"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # getter for: Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;
    invoke-static {v3}, Landroid/net/pppoe/PppoeStateTracker;->access$300(Landroid/net/pppoe/PppoeStateTracker;)Landroid/net/pppoe/PppoeManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/pppoe/PppoeManager;->disconnectPppoe()V

    goto :goto_0

    :cond_2
    const-string v3, "com.mstar.android.wifi.device.removed"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PppoeStateTracker"

    const-string v4, "receive WifiManager.WIFI_DEVICE_REMOVED_ACTION"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # getter for: Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;
    invoke-static {v3}, Landroid/net/pppoe/PppoeStateTracker;->access$300(Landroid/net/pppoe/PppoeStateTracker;)Landroid/net/pppoe/PppoeManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/pppoe/PppoeManager;->getPppoeStatus()Ljava/lang/String;

    move-result-object v3

    const-string v4, "connect"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # getter for: Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;
    invoke-static {v3}, Landroid/net/pppoe/PppoeStateTracker;->access$300(Landroid/net/pppoe/PppoeStateTracker;)Landroid/net/pppoe/PppoeManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/pppoe/PppoeManager;->PppoeGetInterface()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v3, "wlan"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;->this$0:Landroid/net/pppoe/PppoeStateTracker;

    # getter for: Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;
    invoke-static {v3}, Landroid/net/pppoe/PppoeStateTracker;->access$300(Landroid/net/pppoe/PppoeStateTracker;)Landroid/net/pppoe/PppoeManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/pppoe/PppoeManager;->disconnectPppoe()V

    goto/16 :goto_0
.end method
