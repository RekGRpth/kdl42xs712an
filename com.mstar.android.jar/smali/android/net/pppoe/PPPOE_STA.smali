.class public final enum Landroid/net/pppoe/PPPOE_STA;
.super Ljava/lang/Enum;
.source "PPPOE_STA.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/pppoe/PPPOE_STA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/pppoe/PPPOE_STA;

.field public static final enum CONNECTED:Landroid/net/pppoe/PPPOE_STA;

.field public static final enum CONNECTING:Landroid/net/pppoe/PPPOE_STA;

.field public static final enum DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/net/pppoe/PPPOE_STA;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2}, Landroid/net/pppoe/PPPOE_STA;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/pppoe/PPPOE_STA;->CONNECTED:Landroid/net/pppoe/PPPOE_STA;

    new-instance v0, Landroid/net/pppoe/PPPOE_STA;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3}, Landroid/net/pppoe/PPPOE_STA;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    new-instance v0, Landroid/net/pppoe/PPPOE_STA;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4}, Landroid/net/pppoe/PPPOE_STA;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/pppoe/PPPOE_STA;->CONNECTING:Landroid/net/pppoe/PPPOE_STA;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/pppoe/PPPOE_STA;

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTED:Landroid/net/pppoe/PPPOE_STA;

    aput-object v1, v0, v2

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    aput-object v1, v0, v3

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTING:Landroid/net/pppoe/PPPOE_STA;

    aput-object v1, v0, v4

    sput-object v0, Landroid/net/pppoe/PPPOE_STA;->$VALUES:[Landroid/net/pppoe/PPPOE_STA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/pppoe/PPPOE_STA;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Landroid/net/pppoe/PPPOE_STA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/net/pppoe/PPPOE_STA;

    return-object v0
.end method

.method public static values()[Landroid/net/pppoe/PPPOE_STA;
    .locals 1

    sget-object v0, Landroid/net/pppoe/PPPOE_STA;->$VALUES:[Landroid/net/pppoe/PPPOE_STA;

    invoke-virtual {v0}, [Landroid/net/pppoe/PPPOE_STA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/pppoe/PPPOE_STA;

    return-object v0
.end method
