.class public interface abstract Landroid/net/pppoe/IPppoeManager;
.super Ljava/lang/Object;
.source "IPppoeManager.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/pppoe/IPppoeManager$Stub;
    }
.end annotation


# virtual methods
.method public abstract getPppoeStatus()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setPppoeStatus(Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
