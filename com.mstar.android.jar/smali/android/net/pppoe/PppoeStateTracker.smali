.class public Landroid/net/pppoe/PppoeStateTracker;
.super Landroid/os/Handler;
.source "PppoeStateTracker.java"

# interfaces
.implements Landroid/net/NetworkStateTracker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/pppoe/PppoeStateTracker$1;,
        Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PppoeStateTracker"

.field public static sInstance:Landroid/net/pppoe/PppoeStateTracker;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCsHandler:Landroid/os/Handler;

.field private mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mLinkCapabilities:Landroid/net/LinkCapabilities;

.field private mLinkProperties:Landroid/net/LinkProperties;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mPppoeManager:Landroid/net/pppoe/PppoeManager;

.field private mPppoeStateReceiver:Landroid/content/BroadcastReceiver;

.field private mPrefixLength:I

.field private mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Landroid/net/NetworkInfo;

    const/16 v1, 0xe

    const-string v2, "PPPOE"

    const-string v3, ""

    invoke-direct {v0, v1, v4, v2, v3}, Landroid/net/NetworkInfo;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    new-instance v0, Landroid/net/LinkProperties;

    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    iput-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v0, v4}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    invoke-virtual {p0, v4}, Landroid/net/pppoe/PppoeStateTracker;->setTeardownRequested(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    return-void
.end method

.method static synthetic access$100(Landroid/net/pppoe/PppoeStateTracker;)V
    .locals 0
    .param p0    # Landroid/net/pppoe/PppoeStateTracker;

    invoke-direct {p0}, Landroid/net/pppoe/PppoeStateTracker;->checkPppoeManager()V

    return-void
.end method

.method static synthetic access$200(Landroid/net/pppoe/PppoeStateTracker;Ljava/lang/String;)V
    .locals 0
    .param p0    # Landroid/net/pppoe/PppoeStateTracker;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/pppoe/PppoeStateTracker;->notifyStateChange(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Landroid/net/pppoe/PppoeStateTracker;)Landroid/net/pppoe/PppoeManager;
    .locals 1
    .param p0    # Landroid/net/pppoe/PppoeStateTracker;

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    return-object v0
.end method

.method private checkPppoeManager()V
    .locals 1

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/net/pppoe/PppoeManager;->getInstance()Landroid/net/pppoe/PppoeManager;

    move-result-object v0

    iput-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    :cond_0
    return-void
.end method

.method public static declared-synchronized getInstance()Landroid/net/pppoe/PppoeStateTracker;
    .locals 2

    const-class v1, Landroid/net/pppoe/PppoeStateTracker;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid/net/pppoe/PppoeStateTracker;->sInstance:Landroid/net/pppoe/PppoeStateTracker;

    if-nez v0, :cond_0

    new-instance v0, Landroid/net/pppoe/PppoeStateTracker;

    invoke-direct {v0}, Landroid/net/pppoe/PppoeStateTracker;-><init>()V

    sput-object v0, Landroid/net/pppoe/PppoeStateTracker;->sInstance:Landroid/net/pppoe/PppoeStateTracker;

    :cond_0
    sget-object v0, Landroid/net/pppoe/PppoeStateTracker;->sInstance:Landroid/net/pppoe/PppoeStateTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private makeLinkAddress()Landroid/net/LinkAddress;
    .locals 4

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager;->getIpaddr()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PppoeStateTracker"

    const-string v2, "pppoe ip is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/net/LinkAddress;

    invoke-static {v0}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    iget v3, p0, Landroid/net/pppoe/PppoeStateTracker;->mPrefixLength:I

    invoke-direct {v1, v2, v3}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    goto :goto_0
.end method

.method private notifyStateChange(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "connect"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "disconnect"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const-string v1, "connect"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1, v2, v3, v3}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v1, v4}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    invoke-direct {p0}, Landroid/net/pppoe/PppoeStateTracker;->updateLinkProperties()V

    :cond_1
    :goto_0
    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mCsHandler:Landroid/os/Handler;

    new-instance v2, Landroid/net/NetworkInfo;

    iget-object v3, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-direct {v2, v3}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    invoke-virtual {v1, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    return-void

    :cond_3
    const-string v1, "disconnect"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1, v2, v3, v3}, Landroid/net/NetworkInfo;->setDetailedState(Landroid/net/NetworkInfo$DetailedState;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/NetworkInfo;->setIsAvailable(Z)V

    goto :goto_0
.end method

.method private updateLinkProperties()V
    .locals 8

    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-virtual {v6}, Landroid/net/LinkProperties;->clear()V

    invoke-direct {p0}, Landroid/net/pppoe/PppoeStateTracker;->makeLinkAddress()Landroid/net/LinkAddress;

    move-result-object v3

    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-virtual {v6, v3}, Landroid/net/LinkProperties;->addLinkAddress(Landroid/net/LinkAddress;)V

    :try_start_0
    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v6}, Landroid/net/pppoe/PppoeManager;->getIpaddr()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    new-instance v5, Landroid/net/RouteInfo;

    invoke-direct {v5, v3, v4}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-virtual {v6, v5}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v6}, Landroid/net/pppoe/PppoeManager;->getDns1()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-static {v0}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    :goto_1
    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v6}, Landroid/net/pppoe/PppoeManager;->getDns2()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-static {v1}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    :goto_2
    iget-object v6, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    iget-object v7, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeManager:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v7}, Landroid/net/pppoe/PppoeManager;->getInterfaceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/LinkProperties;->setInterfaceName(Ljava/lang/String;)V

    const-string v6, "PppoeStateTracker"

    const-string v7, "print linkproperties of pppoe:"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "PppoeStateTracker"

    iget-object v7, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-virtual {v7}, Landroid/net/LinkProperties;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v2

    const-string v6, "PppoeStateTracker"

    const-string v7, "failed to add route"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-string v6, "PppoeStateTracker"

    const-string v7, "dns1 is empty"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    const-string v6, "PppoeStateTracker"

    const-string v7, "dns2 is empty"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public captivePortalCheckComplete()V
    .locals 0

    return-void
.end method

.method public defaultRouteSet(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public getLinkCapabilities()Landroid/net/LinkCapabilities;
    .locals 1

    new-instance v0, Landroid/net/LinkCapabilities;

    invoke-direct {v0}, Landroid/net/LinkCapabilities;-><init>()V

    return-object v0
.end method

.method public getLinkProperties()Landroid/net/LinkProperties;
    .locals 2

    new-instance v0, Landroid/net/LinkProperties;

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-direct {v0, v1}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    return-object v0
.end method

.method public getNetworkInfo()Landroid/net/NetworkInfo;
    .locals 2

    new-instance v0, Landroid/net/NetworkInfo;

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-direct {v0, v1}, Landroid/net/NetworkInfo;-><init>(Landroid/net/NetworkInfo;)V

    return-object v0
.end method

.method public getTcpBufferSizesPropName()Ljava/lang/String;
    .locals 1

    const-string v0, "net.tcp.buffersize.default"

    return-object v0
.end method

.method public isAvailable()Z
    .locals 1

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    return v0
.end method

.method public isDefaultRouteSet()Z
    .locals 1

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mDefaultRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public isPrivateDnsRouteSet()Z
    .locals 1

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public isTeardownRequested()Z
    .locals 1

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public privateDnsRouteSet(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mPrivateDnsRouteSet:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public reconnect()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setDependencyMet(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setPolicyDataEnable(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setRadio(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    return v0
.end method

.method public setTeardownRequested(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Landroid/net/pppoe/PppoeStateTracker;->mTeardownRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public setUserDataEnable(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public startMonitoring(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    iput-object p1, p0, Landroid/net/pppoe/PppoeStateTracker;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/net/pppoe/PppoeStateTracker;->mCsHandler:Landroid/os/Handler;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.pppoe.PPPOE_STATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mstar.android.wifi.device.removed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/net/pppoe/PppoeStateTracker$PppoeStateReceiver;-><init>(Landroid/net/pppoe/PppoeStateTracker;Landroid/net/pppoe/PppoeStateTracker$1;)V

    iput-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeStateReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Landroid/net/pppoe/PppoeStateTracker;->mContext:Landroid/content/Context;

    iget-object v2, p0, Landroid/net/pppoe/PppoeStateTracker;->mPppoeStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public teardown()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
