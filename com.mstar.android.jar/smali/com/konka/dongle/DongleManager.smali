.class public Lcom/konka/dongle/DongleManager;
.super Ljava/lang/Object;
.source "DongleManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/dongle/DongleManager$REMOTE_CONTROL_WORK_MODE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DongleManager"

.field private static dongleService:Lcom/mstar/android/tv/IDongleService;

.field static mInstance:Lcom/konka/dongle/DongleManager;

.field static final mInstanceSync:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    sput-object v0, Lcom/konka/dongle/DongleManager;->mInstance:Lcom/konka/dongle/DongleManager;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/konka/dongle/DongleManager;->mInstanceSync:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcom/mstar/android/tv/IDongleService;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tv/IDongleService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sput-object p1, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    return-void
.end method

.method public static getInstance()Lcom/konka/dongle/DongleManager;
    .locals 4

    sget-object v1, Lcom/konka/dongle/DongleManager;->mInstance:Lcom/konka/dongle/DongleManager;

    if-nez v1, :cond_1

    sget-object v2, Lcom/konka/dongle/DongleManager;->mInstanceSync:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/konka/dongle/DongleManager;->mInstance:Lcom/konka/dongle/DongleManager;

    if-nez v1, :cond_0

    const-string v1, "dongle"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Lcom/konka/dongle/DongleManager;

    invoke-static {v0}, Lcom/mstar/android/tv/IDongleService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/IDongleService;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/konka/dongle/DongleManager;-><init>(Lcom/mstar/android/tv/IDongleService;)V

    sput-object v1, Lcom/konka/dongle/DongleManager;->mInstance:Lcom/konka/dongle/DongleManager;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v1, Lcom/konka/dongle/DongleManager;->mInstance:Lcom/konka/dongle/DongleManager;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public bAgreeCnDevice(ZIII)Z
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/mstar/android/tv/IDongleService;->bAgreeCnDevice(ZIII)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public getCnDeviceInfo(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1}, Lcom/mstar/android/tv/IDongleService;->getCnDeviceInfo(I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public getDongleSwVersion(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public resetAllSystem()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public searchCnDevice(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1, p2}, Lcom/mstar/android/tv/IDongleService;->searchCnDevice(II)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public sendUpgradePackage(Lcom/mstar/android/tv/PackageParcel;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tv/PackageParcel;

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_1

    if-nez p1, :cond_0

    :try_start_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "pkg ========= null"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "dongleService.sendUpgradePackage!!!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1}, Lcom/mstar/android/tv/IDongleService;->sendUpgradePackage(Lcom/mstar/android/tv/PackageParcel;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_1
    :goto_1
    move v2, v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public setAudioTrack(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1, p2}, Lcom/mstar/android/tv/IDongleService;->setAudioTrack(II)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public setAudioVolume(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1, p2}, Lcom/mstar/android/tv/IDongleService;->setAudioVolume(II)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public setPowerOffMode(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1}, Lcom/mstar/android/tv/IDongleService;->setPowerOffMode(I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public setPowerOnTime(J)Z
    .locals 3
    .param p1    # J

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2, p1, p2}, Lcom/mstar/android/tv/IDongleService;->setPowerOnTime(J)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public startUpgradeDongle()Z
    .locals 4

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "dongleService.startUpgradeDongle!!!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    invoke-interface {v2}, Lcom/mstar/android/tv/IDongleService;->startUpgradeDongle()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public switchRemoteCtrMode(Lcom/konka/dongle/DongleManager$REMOTE_CONTROL_WORK_MODE;)Z
    .locals 4
    .param p1    # Lcom/konka/dongle/DongleManager$REMOTE_CONTROL_WORK_MODE;

    const/4 v1, 0x0

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    if-eqz v2, :cond_0

    :try_start_0
    sget-object v2, Lcom/konka/dongle/DongleManager$REMOTE_CONTROL_WORK_MODE;->INFRARED:Lcom/konka/dongle/DongleManager$REMOTE_CONTROL_WORK_MODE;

    if-ne p1, v2, :cond_1

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/mstar/android/tv/IDongleService;->switchRemoteCtrMode(I)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v2, Lcom/konka/dongle/DongleManager$REMOTE_CONTROL_WORK_MODE;->_24G:Lcom/konka/dongle/DongleManager$REMOTE_CONTROL_WORK_MODE;

    if-ne p1, v2, :cond_2

    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/mstar/android/tv/IDongleService;->switchRemoteCtrMode(I)Z

    move-result v1

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/konka/dongle/DongleManager;->dongleService:Lcom/mstar/android/tv/IDongleService;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/mstar/android/tv/IDongleService;->switchRemoteCtrMode(I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public upgradeDongleSw(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
