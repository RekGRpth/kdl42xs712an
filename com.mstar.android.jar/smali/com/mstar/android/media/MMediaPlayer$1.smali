.class synthetic Lcom/mstar/android/media/MMediaPlayer$1;
.super Ljava/lang/Object;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE:[I

.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE:[I

.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EnumPlayerSeamlessMode:[I

.field static final synthetic $SwitchMap$com$mstar$android$media$MMediaPlayer$EnumVideoAspectRatio:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->values()[Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumVideoAspectRatio:[I

    :try_start_0
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumVideoAspectRatio:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_AUTO:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3e

    :goto_0
    :try_start_1
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumVideoAspectRatio:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3d

    :goto_1
    :try_start_2
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumVideoAspectRatio:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3c

    :goto_2
    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->values()[Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumPlayerSeamlessMode:[I

    :try_start_3
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumPlayerSeamlessMode:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_NONE:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3b

    :goto_3
    :try_start_4
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumPlayerSeamlessMode:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_FREEZ:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3a

    :goto_4
    :try_start_5
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumPlayerSeamlessMode:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_SMOTH:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_39

    :goto_5
    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->values()[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE:[I

    :try_start_6
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->E_DATASOURCE_CONTENT_TYPE_NETWORK_STREAM_WITH_SEEK:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_38

    :goto_6
    :try_start_7
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->E_DATASOURCE_CONTENT_TYPE_NETWORK_STREAM_WITHOUT_SEEK:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_37

    :goto_7
    :try_start_8
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->E_DATASOURCE_CONTENT_TYPE_ES:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_36

    :goto_8
    :try_start_9
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->E_DATASOURCE_CONTENT_TYPE_MASS_STORAGE:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_35

    :goto_9
    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->values()[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE:[I

    :try_start_a
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->E_DATASOURCE_PLAYER_MOVIE:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_34

    :goto_a
    :try_start_b
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->E_DATASOURCE_PLAYER_MUSIC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_33

    :goto_b
    :try_start_c
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->E_DATASOURCE_PLAYER_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_32

    :goto_c
    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->values()[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    :try_start_d
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_WMA:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_31

    :goto_d
    :try_start_e
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_DTS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_30

    :goto_e
    :try_start_f
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_MP3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_2f

    :goto_f
    :try_start_10
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_MPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_2e

    :goto_10
    :try_start_11
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AC3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_2d

    :goto_11
    :try_start_12
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AC3_PLUS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_2c

    :goto_12
    :try_start_13
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_2b

    :goto_13
    :try_start_14
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_PCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_2a

    :goto_14
    :try_start_15
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_ADPCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_29

    :goto_15
    :try_start_16
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_RAAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_28

    :goto_16
    :try_start_17
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_COOK:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_27

    :goto_17
    :try_start_18
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_FLAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_26

    :goto_18
    :try_start_19
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_VORBIS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_25

    :goto_19
    :try_start_1a
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AMR_NB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_24

    :goto_1a
    :try_start_1b
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AMR_WB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_23

    :goto_1b
    :try_start_1c
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_22

    :goto_1c
    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->values()[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    :try_start_1d
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG1VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_21

    :goto_1d
    :try_start_1e
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG2VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_20

    :goto_1e
    :try_start_1f
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_1f

    :goto_1f
    :try_start_20
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_H263:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_1e

    :goto_20
    :try_start_21
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_1d

    :goto_21
    :try_start_22
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_1c

    :goto_22
    :try_start_23
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_1b

    :goto_23
    :try_start_24
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_H264:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_1a

    :goto_24
    :try_start_25
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_AVS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_19

    :goto_25
    :try_start_26
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_RV30:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_18

    :goto_26
    :try_start_27
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_RV40:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_17

    :goto_27
    :try_start_28
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MJPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_16

    :goto_28
    :try_start_29
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_VC1:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_29
    .catch Ljava/lang/NoSuchFieldError; {:try_start_29 .. :try_end_29} :catch_15

    :goto_29
    :try_start_2a
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_WMV3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_2a} :catch_14

    :goto_2a
    :try_start_2b
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_2b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2b .. :try_end_2b} :catch_13

    :goto_2b
    :try_start_2c
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_FOURCCEX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_2c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2c .. :try_end_2c} :catch_12

    :goto_2c
    :try_start_2d
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_TS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_2d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2d .. :try_end_2d} :catch_11

    :goto_2d
    :try_start_2e
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2e .. :try_end_2e} :catch_10

    :goto_2e
    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->values()[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    :try_start_2f
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_AVI:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2f .. :try_end_2f} :catch_f

    :goto_2f
    :try_start_30
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MP4:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_30
    .catch Ljava/lang/NoSuchFieldError; {:try_start_30 .. :try_end_30} :catch_e

    :goto_30
    :try_start_31
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MKV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_31
    .catch Ljava/lang/NoSuchFieldError; {:try_start_31 .. :try_end_31} :catch_d

    :goto_31
    :try_start_32
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_ASF:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_32
    .catch Ljava/lang/NoSuchFieldError; {:try_start_32 .. :try_end_32} :catch_c

    :goto_32
    :try_start_33
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_RM:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_33
    .catch Ljava/lang/NoSuchFieldError; {:try_start_33 .. :try_end_33} :catch_b

    :goto_33
    :try_start_34
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_TS:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_34
    .catch Ljava/lang/NoSuchFieldError; {:try_start_34 .. :try_end_34} :catch_a

    :goto_34
    :try_start_35
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MPG:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_35} :catch_9

    :goto_35
    :try_start_36
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_36
    .catch Ljava/lang/NoSuchFieldError; {:try_start_36 .. :try_end_36} :catch_8

    :goto_36
    :try_start_37
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_ESDATA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_37
    .catch Ljava/lang/NoSuchFieldError; {:try_start_37 .. :try_end_37} :catch_7

    :goto_37
    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->values()[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    :try_start_38
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_NETFLIX:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_38
    .catch Ljava/lang/NoSuchFieldError; {:try_start_38 .. :try_end_38} :catch_6

    :goto_38
    :try_start_39
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_DLNA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_39
    .catch Ljava/lang/NoSuchFieldError; {:try_start_39 .. :try_end_39} :catch_5

    :goto_39
    :try_start_3a
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_HBBTV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3a .. :try_end_3a} :catch_4

    :goto_3a
    :try_start_3b
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_WEBBROWSER:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3b .. :try_end_3b} :catch_3

    :goto_3b
    :try_start_3c
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_WMDRM10:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3c .. :try_end_3c} :catch_2

    :goto_3c
    :try_start_3d
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_ANDROID_USB:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3d .. :try_end_3d} :catch_1

    :goto_3d
    :try_start_3e
    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_ANDROID_STREAMING:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_3e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3e .. :try_end_3e} :catch_0

    :goto_3e
    return-void

    :catch_0
    move-exception v0

    goto :goto_3e

    :catch_1
    move-exception v0

    goto :goto_3d

    :catch_2
    move-exception v0

    goto :goto_3c

    :catch_3
    move-exception v0

    goto :goto_3b

    :catch_4
    move-exception v0

    goto :goto_3a

    :catch_5
    move-exception v0

    goto :goto_39

    :catch_6
    move-exception v0

    goto :goto_38

    :catch_7
    move-exception v0

    goto :goto_37

    :catch_8
    move-exception v0

    goto :goto_36

    :catch_9
    move-exception v0

    goto/16 :goto_35

    :catch_a
    move-exception v0

    goto/16 :goto_34

    :catch_b
    move-exception v0

    goto/16 :goto_33

    :catch_c
    move-exception v0

    goto/16 :goto_32

    :catch_d
    move-exception v0

    goto/16 :goto_31

    :catch_e
    move-exception v0

    goto/16 :goto_30

    :catch_f
    move-exception v0

    goto/16 :goto_2f

    :catch_10
    move-exception v0

    goto/16 :goto_2e

    :catch_11
    move-exception v0

    goto/16 :goto_2d

    :catch_12
    move-exception v0

    goto/16 :goto_2c

    :catch_13
    move-exception v0

    goto/16 :goto_2b

    :catch_14
    move-exception v0

    goto/16 :goto_2a

    :catch_15
    move-exception v0

    goto/16 :goto_29

    :catch_16
    move-exception v0

    goto/16 :goto_28

    :catch_17
    move-exception v0

    goto/16 :goto_27

    :catch_18
    move-exception v0

    goto/16 :goto_26

    :catch_19
    move-exception v0

    goto/16 :goto_25

    :catch_1a
    move-exception v0

    goto/16 :goto_24

    :catch_1b
    move-exception v0

    goto/16 :goto_23

    :catch_1c
    move-exception v0

    goto/16 :goto_22

    :catch_1d
    move-exception v0

    goto/16 :goto_21

    :catch_1e
    move-exception v0

    goto/16 :goto_20

    :catch_1f
    move-exception v0

    goto/16 :goto_1f

    :catch_20
    move-exception v0

    goto/16 :goto_1e

    :catch_21
    move-exception v0

    goto/16 :goto_1d

    :catch_22
    move-exception v0

    goto/16 :goto_1c

    :catch_23
    move-exception v0

    goto/16 :goto_1b

    :catch_24
    move-exception v0

    goto/16 :goto_1a

    :catch_25
    move-exception v0

    goto/16 :goto_19

    :catch_26
    move-exception v0

    goto/16 :goto_18

    :catch_27
    move-exception v0

    goto/16 :goto_17

    :catch_28
    move-exception v0

    goto/16 :goto_16

    :catch_29
    move-exception v0

    goto/16 :goto_15

    :catch_2a
    move-exception v0

    goto/16 :goto_14

    :catch_2b
    move-exception v0

    goto/16 :goto_13

    :catch_2c
    move-exception v0

    goto/16 :goto_12

    :catch_2d
    move-exception v0

    goto/16 :goto_11

    :catch_2e
    move-exception v0

    goto/16 :goto_10

    :catch_2f
    move-exception v0

    goto/16 :goto_f

    :catch_30
    move-exception v0

    goto/16 :goto_e

    :catch_31
    move-exception v0

    goto/16 :goto_d

    :catch_32
    move-exception v0

    goto/16 :goto_c

    :catch_33
    move-exception v0

    goto/16 :goto_b

    :catch_34
    move-exception v0

    goto/16 :goto_a

    :catch_35
    move-exception v0

    goto/16 :goto_9

    :catch_36
    move-exception v0

    goto/16 :goto_8

    :catch_37
    move-exception v0

    goto/16 :goto_7

    :catch_38
    move-exception v0

    goto/16 :goto_6

    :catch_39
    move-exception v0

    goto/16 :goto_5

    :catch_3a
    move-exception v0

    goto/16 :goto_4

    :catch_3b
    move-exception v0

    goto/16 :goto_3

    :catch_3c
    move-exception v0

    goto/16 :goto_2

    :catch_3d
    move-exception v0

    goto/16 :goto_1

    :catch_3e
    move-exception v0

    goto/16 :goto_0
.end method
