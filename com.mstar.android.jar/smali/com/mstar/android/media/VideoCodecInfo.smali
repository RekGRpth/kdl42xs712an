.class public Lcom/mstar/android/media/VideoCodecInfo;
.super Ljava/lang/Object;
.source "VideoCodecInfo.java"


# static fields
.field private static final LOGD:Z = true

.field private static final TAG:Ljava/lang/String; = "VideoCodecInfo"


# instance fields
.field private mDuration:I

.field private mNumTracks:I

.field private mVideoCodec:Ljava/lang/String;

.field private mVideoHeight:I

.field private mVideoWidth:I

.field private mVideoframeRate:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoCodec:Ljava/lang/String;

    iput v1, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoframeRate:I

    iput v1, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoWidth:I

    iput v1, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoHeight:I

    iput v1, p0, Lcom/mstar/android/media/VideoCodecInfo;->mNumTracks:I

    iput v1, p0, Lcom/mstar/android/media/VideoCodecInfo;->mDuration:I

    return-void
.end method

.method public constructor <init>(Landroid/media/Metadata;)V
    .locals 7
    .param p1    # Landroid/media/Metadata;

    const/16 v6, 0x1c

    const/16 v2, 0x1b

    const/16 v5, 0x18

    const/16 v4, 0xe

    const/4 v3, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "VideoCodecInfo"

    const-string v1, "VideoCodecInfo constructure"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v2}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoCodec:Ljava/lang/String;

    :goto_0
    const-string v0, "VideoCodecInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoCodec : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoCodec:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v5}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v5}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoframeRate:I

    :goto_1
    const-string v0, "VideoCodecInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoframeRate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoframeRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x1d

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x1d

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoWidth:I

    :goto_2
    const-string v0, "VideoCodecInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v6}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoHeight:I

    :goto_3
    const-string v0, "VideoCodecInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x1e

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x1e

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mNumTracks:I

    :goto_4
    const-string v0, "VideoCodecInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mNumTracks : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/VideoCodecInfo;->mNumTracks:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v4}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1, v4}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mDuration:I

    :goto_5
    const-string v0, "VideoCodecInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDuration : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/VideoCodecInfo;->mDuration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoCodec:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    iput v3, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoframeRate:I

    goto/16 :goto_1

    :cond_2
    iput v3, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoWidth:I

    goto/16 :goto_2

    :cond_3
    iput v3, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoHeight:I

    goto :goto_3

    :cond_4
    iput v3, p0, Lcom/mstar/android/media/VideoCodecInfo;->mNumTracks:I

    goto :goto_4

    :cond_5
    iput v3, p0, Lcom/mstar/android/media/VideoCodecInfo;->mDuration:I

    goto :goto_5
.end method


# virtual methods
.method public getCodecType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoCodec:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "VideoCodecInfo"

    const-string v1, "error in getCodecType!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoCodec:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 2

    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mDuration:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "VideoCodecInfo"

    const-string v1, "error in getDuration!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mDuration:I

    return v0
.end method

.method public getNumTracks()I
    .locals 2

    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mNumTracks:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "VideoCodecInfo"

    const-string v1, "error in getNumTracks!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mNumTracks:I

    return v0
.end method

.method public getVideoHeight()I
    .locals 2

    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoHeight:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "VideoCodecInfo"

    const-string v1, "error in getVideoHeight!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoHeight:I

    return v0
.end method

.method public getVideoWidth()I
    .locals 2

    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoWidth:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "VideoCodecInfo"

    const-string v1, "error in getVideoWidth!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoWidth:I

    return v0
.end method

.method public getVideoframeRate()I
    .locals 2

    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoframeRate:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "VideoCodecInfo"

    const-string v1, "error in getVideoframeRate!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoframeRate:I

    return v0
.end method

.method public setCodecType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/media/VideoCodecInfo;->mVideoCodec:Ljava/lang/String;

    return-void
.end method
