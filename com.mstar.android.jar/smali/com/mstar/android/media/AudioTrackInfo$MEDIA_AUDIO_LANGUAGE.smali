.class final enum Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;
.super Ljava/lang/Enum;
.source "AudioTrackInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/AudioTrackInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MEDIA_AUDIO_LANGUAGE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_ARABIC:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_CHINESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_CROATIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_DUTCH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_ENGLISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_FRENCH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_GERMAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_GREEK:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_ITALIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_JAPANESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_KOREAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_POLISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_PORTUGUESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_ROMANIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_RUSSIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_SPANISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_SWEDISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field public static final enum E_MEDIA_AUDIO_LANGUAGE_UNDEFINED:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_GERMAN"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_GERMAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_ENGLISH"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ENGLISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_SPANISH"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_SPANISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_GREEK"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_GREEK:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_FRENCH"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_FRENCH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_CROATIAN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_CROATIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_ITALIAN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ITALIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_DUTCH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_DUTCH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_POLISH"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_POLISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_PORTUGUESE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_PORTUGUESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_RUSSIAN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_RUSSIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_ROMANIAN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ROMANIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_SWEDISH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_SWEDISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_ARABIC"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ARABIC:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_CHINESE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_CHINESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_JAPANESE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_JAPANESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_KOREAN"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_KOREAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    new-instance v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const-string v1, "E_MEDIA_AUDIO_LANGUAGE_UNDEFINED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_UNDEFINED:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    sget-object v1, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_GERMAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ENGLISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_SPANISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_GREEK:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_FRENCH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_CROATIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ITALIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_DUTCH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_POLISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_PORTUGUESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_RUSSIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ROMANIAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_SWEDISH:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_ARABIC:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_CHINESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_JAPANESE:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_KOREAN:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->E_MEDIA_AUDIO_LANGUAGE_UNDEFINED:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->$VALUES:[Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->$VALUES:[Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    invoke-virtual {v0}, [Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    return-object v0
.end method
