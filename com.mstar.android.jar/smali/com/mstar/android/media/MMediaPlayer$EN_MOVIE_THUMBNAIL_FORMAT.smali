.class public final enum Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;
.super Ljava/lang/Enum;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MOVIE_THUMBNAIL_FORMAT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

.field public static final enum E_MOVIE_THUMBNAIL_ARGB1555:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

.field public static final enum E_MOVIE_THUMBNAIL_ARGB4444:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

.field public static final enum E_MOVIE_THUMBNAIL_ARGB8888:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

.field public static final enum E_MOVIE_THUMBNAIL_NOT_SUPPORT:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

.field public static final enum E_MOVIE_THUMBNAIL_YUV422:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    const-string v1, "E_MOVIE_THUMBNAIL_ARGB8888"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_ARGB8888:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    const-string v1, "E_MOVIE_THUMBNAIL_ARGB1555"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_ARGB1555:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    const-string v1, "E_MOVIE_THUMBNAIL_ARGB4444"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_ARGB4444:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    const-string v1, "E_MOVIE_THUMBNAIL_YUV422"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_YUV422:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    const-string v1, "E_MOVIE_THUMBNAIL_NOT_SUPPORT"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_NOT_SUPPORT:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_ARGB8888:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_ARGB1555:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_ARGB4444:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_YUV422:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->E_MOVIE_THUMBNAIL_NOT_SUPPORT:Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    invoke-virtual {v0}, [Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;

    return-object v0
.end method
