.class public Lcom/mstar/android/tv/widget/TvView;
.super Landroid/view/SurfaceView;
.source "TvView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private TAG:Ljava/lang/String;

.field private availableSrcList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isPowerOn:Z

.field private mContext:Landroid/content/Context;

.field private mCreateFromLayout:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mVideoHeight:I

.field private mVideoLeft:I

.field private mVideoTop:I

.field private mVideoWidth:I

.field private surfaceParams:Landroid/view/WindowManager$LayoutParams;

.field private totalSrcList:[Ljava/lang/String;

.field private wm:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const-string v0, "TvView"

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VGA"

    aput-object v1, v0, v3

    const-string v1, "ATV"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "CVBS"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CVBS2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CVBS3"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CVBS4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CVBS5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CVBS6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CVBS7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CVBS8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CVBS_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SVIDEO"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SVIDEO2"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SVIDEO3"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SVIDEO4"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SVIDEO_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "YPBPR1"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "YPBPR2"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "YPBPR3"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "YPBPR_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SCART"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SCART2"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SCART_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "HDMI1"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "HDMI2"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "HDMI3"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "HDMI4"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "HDMI_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "DTV"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->totalSrcList:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->isPowerOn:Z

    iput-boolean v4, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const-string v0, "TvView"

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VGA"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "ATV"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CVBS"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CVBS2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CVBS3"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CVBS4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CVBS5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CVBS6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CVBS7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CVBS8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CVBS_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SVIDEO"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SVIDEO2"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SVIDEO3"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SVIDEO4"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SVIDEO_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "YPBPR1"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "YPBPR2"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "YPBPR3"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "YPBPR_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SCART"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SCART2"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SCART_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "HDMI1"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "HDMI2"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "HDMI3"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "HDMI4"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "HDMI_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "DTV"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->totalSrcList:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->isPowerOn:Z

    iput p3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iput p2, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    iput p4, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    iput p5, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    iput-object p1, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "TvView"

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VGA"

    aput-object v1, v0, v3

    const-string v1, "ATV"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "CVBS"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CVBS2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CVBS3"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CVBS4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CVBS5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CVBS6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CVBS7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CVBS8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CVBS_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SVIDEO"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SVIDEO2"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SVIDEO3"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SVIDEO4"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SVIDEO_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "YPBPR1"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "YPBPR2"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "YPBPR3"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "YPBPR_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SCART"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SCART2"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SCART_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "HDMI1"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "HDMI2"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "HDMI3"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "HDMI4"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "HDMI_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "DTV"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->totalSrcList:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->isPowerOn:Z

    iput-object p1, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    iput-boolean v4, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "TvView"

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    iput v3, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    iput-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VGA"

    aput-object v1, v0, v3

    const-string v1, "ATV"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "CVBS"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CVBS2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CVBS3"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CVBS4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CVBS5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CVBS6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CVBS7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CVBS8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CVBS_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SVIDEO"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SVIDEO2"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SVIDEO3"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SVIDEO4"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SVIDEO_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "YPBPR1"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "YPBPR2"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "YPBPR3"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "YPBPR_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SCART"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SCART2"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SCART_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "HDMI1"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "HDMI2"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "HDMI3"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "HDMI4"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "HDMI_MAX"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "DTV"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->totalSrcList:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/mstar/android/tv/widget/TvView;->isPowerOn:Z

    iput-object p1, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    iput-boolean v4, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    return-void
.end method

.method private doSourceSwitch(I)V
    .locals 3
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v1, v2, p1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    if-ne p1, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private initSrcList()V
    .locals 5

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getSourceList()[I

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mstar/android/tv/widget/TvView;->totalSrcList:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    aget v3, v2, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mstar/android/tv/widget/TvView;->totalSrcList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_1
    return-void
.end method

.method private initVideoView()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/mstar/android/tv/widget/TvView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-virtual {p0, v2}, Lcom/mstar/android/tv/widget/TvView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/mstar/android/tv/widget/TvView;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private initWMVideoView()V
    .locals 3

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x18

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "openSurfaceView==="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->wm:Landroid/view/WindowManager;

    invoke-direct {p0}, Lcom/mstar/android/tv/widget/TvView;->initVideoView()V

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->wm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/mstar/android/tv/widget/TvView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private setVideoFullRectangle()V
    .locals 4

    :try_start_0
    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    const v2, 0xffff

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    const v2, 0xffff

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    const v2, 0xffff

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    const v2, 0xffff

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setVideoRectangle()V
    .locals 4

    :try_start_0
    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iget v2, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget v2, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iget v2, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iget v2, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tv/widget/TvView;->wm:Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public openView(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mstar/android/tv/widget/TvView;->isPowerOn:Z

    iget-boolean v0, p0, Lcom/mstar/android/tv/widget/TvView;->mCreateFromLayout:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mstar/android/tv/widget/TvView;->initVideoView()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mstar/android/tv/widget/TvView;->initWMVideoView()V

    goto :goto_0
.end method

.method public selectDTVProgram(I)Z
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    int-to-short v3, v1

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTvURI(Landroid/net/Uri;)V
    .locals 8
    .param p1    # Landroid/net/Uri;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "command"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v5

    const-string v6, "mstar.tv"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v5, "setinputsrc"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    const/4 v0, -0x1

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    if-nez v5, :cond_3

    const/4 v0, -0x1

    :goto_1
    const-string v5, "ATV"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    if-gez v0, :cond_4

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getInstance()Lcom/mstar/android/tv/TvChannelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tv/TvChannelManager;->getCurrentChannelNumber()I

    move-result v2

    const/16 v5, 0xff

    if-le v2, v5, :cond_2

    const/4 v2, 0x0

    :cond_2
    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getInstance()Lcom/mstar/android/tv/TvChannelManager;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/mstar/android/tv/TvChannelManager;->setAtvChannel(I)I

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getInstance()Lcom/mstar/android/tv/TvChannelManager;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/mstar/android/tv/TvChannelManager;->setAtvChannel(I)I

    goto :goto_0

    :cond_5
    const-string v5, "DTV"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    if-gez v0, :cond_6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getInstance()Lcom/mstar/android/tv/TvChannelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tv/TvChannelManager;->playDtvCurrentProgram()Z

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0, v0}, Lcom/mstar/android/tv/widget/TvView;->selectDTVProgram(I)Z

    goto/16 :goto_0

    :cond_7
    const-string v5, "VGA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    goto/16 :goto_0

    :cond_8
    const-string v5, "CVBS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    add-int/lit8 v5, v0, -0x1

    if-gtz v5, :cond_9

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    goto/16 :goto_0

    :cond_9
    iget-object v5, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CVBS"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(I)V

    goto/16 :goto_0

    :cond_a
    const-string v5, "SVIDEO"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    add-int/lit8 v5, v0, -0x1

    if-gtz v5, :cond_b

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    goto/16 :goto_0

    :cond_b
    iget-object v5, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SVIDEO"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(I)V

    goto/16 :goto_0

    :cond_c
    const-string v5, "YPBPR"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    add-int/lit8 v5, v0, -0x1

    if-gtz v5, :cond_d

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    goto/16 :goto_0

    :cond_d
    iget-object v5, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "YPBPR"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(I)V

    goto/16 :goto_0

    :cond_e
    const-string v5, "SCART"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_10

    add-int/lit8 v5, v0, -0x1

    if-gtz v5, :cond_f

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    goto/16 :goto_0

    :cond_f
    iget-object v5, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SCART"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(I)V

    goto/16 :goto_0

    :cond_10
    const-string v5, "HDMI"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v5, v0, -0x1

    if-gtz v5, :cond_11

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    goto/16 :goto_0

    :cond_11
    iget-object v5, p0, Lcom/mstar/android/tv/widget/TvView;->availableSrcList:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HDMI"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-direct {p0, v5}, Lcom/mstar/android/tv/widget/TvView;->doSourceSwitch(I)V

    goto/16 :goto_0

    :cond_12
    const-string v5, "setfullscale"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/mstar/android/tv/widget/TvView;->setVideoFullRectangle()V

    goto/16 :goto_0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tv/widget/TvView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/mstar/android/tv/widget/TvView;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoWidth:I

    invoke-virtual {p0}, Lcom/mstar/android/tv/widget/TvView;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoHeight:I

    invoke-virtual {p0}, Lcom/mstar/android/tv/widget/TvView;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoLeft:I

    invoke-virtual {p0}, Lcom/mstar/android/tv/widget/TvView;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/mstar/android/tv/widget/TvView;->mVideoTop:I

    iget-boolean v1, p0, Lcom/mstar/android/tv/widget/TvView;->isPowerOn:Z

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mstar/android/tv/widget/TvView;->isPowerOn:Z

    invoke-direct {p0}, Lcom/mstar/android/tv/widget/TvView;->setVideoRectangle()V

    :cond_2
    invoke-direct {p0}, Lcom/mstar/android/tv/widget/TvView;->initSrcList()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method
