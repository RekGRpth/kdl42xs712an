.class public Lcom/mstar/android/tv/TvCcManager;
.super Ljava/lang/Object;
.source "TvCcManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvCcManager"

.field static mInstance:Lcom/mstar/android/tv/TvCcManager;

.field private static mService:Lcom/mstar/android/tv/ITvCc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvCcManager;->mInstance:Lcom/mstar/android/tv/TvCcManager;

    sput-object v0, Lcom/mstar/android/tv/TvCcManager;->mService:Lcom/mstar/android/tv/ITvCc;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvCcManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tv/TvCcManager;->mInstance:Lcom/mstar/android/tv/TvCcManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tv/TvCcManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tv/TvCcManager;->mInstance:Lcom/mstar/android/tv/TvCcManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tv/TvCcManager;

    invoke-direct {v0}, Lcom/mstar/android/tv/TvCcManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/TvCcManager;->mInstance:Lcom/mstar/android/tv/TvCcManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tv/TvCcManager;->mInstance:Lcom/mstar/android/tv/TvCcManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getService()Lcom/mstar/android/tv/ITvCc;
    .locals 1

    sget-object v0, Lcom/mstar/android/tv/TvCcManager;->mService:Lcom/mstar/android/tv/ITvCc;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tv/TvCcManager;->mService:Lcom/mstar/android/tv/ITvCc;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvManager;->getTvCc()Lcom/mstar/android/tv/ITvCc;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tv/TvCcManager;->mService:Lcom/mstar/android/tv/ITvCc;

    sget-object v0, Lcom/mstar/android/tv/TvCcManager;->mService:Lcom/mstar/android/tv/ITvCc;

    goto :goto_0
.end method


# virtual methods
.method public creatPreviewCcWindow()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvCcManager;->getService()Lcom/mstar/android/tv/ITvCc;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCc;->creatPreviewCcWindow()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public drawPreviewCc(Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;

    const-string v2, "TvCcManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drawPreviewCc paras CaptionOptionSetting setting.currProgInfoBGColor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoBGColor:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", setting.currProgInfoBGOpacity = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoBGOpacity:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", setting.currProgInfoEdgeColor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoEdgeColor:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", setting.currProgInfoEdgeStyle = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoEdgeStyle:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", setting.currProgInfoFGColor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoFGColor:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", setting.currProgInfoFGOpacity = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoFGOpacity:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", setting.currProgInfoFontSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoFontSize:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", setting.currProgInfoFontStyle = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;->currProgInfoFontStyle:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvCcManager;->getService()Lcom/mstar/android/tv/ITvCc;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvCc;->drawPreviewCc(Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public exitPreviewCc()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvCcManager;->getService()Lcom/mstar/android/tv/ITvCc;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCc;->exitPreviewCc()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public startCc()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvCcManager;->getService()Lcom/mstar/android/tv/ITvCc;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCc;->startCc()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public stopCc()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvCcManager;->getService()Lcom/mstar/android/tv/ITvCc;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCc;->stopCc()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method
