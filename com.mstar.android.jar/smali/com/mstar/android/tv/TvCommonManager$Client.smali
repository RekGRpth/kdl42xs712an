.class Lcom/mstar/android/tv/TvCommonManager$Client;
.super Lcom/mstar/android/tv/ITvEventClient$Stub;
.source "TvCommonManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tv/TvCommonManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Client"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/tv/TvCommonManager;


# direct methods
.method private constructor <init>(Lcom/mstar/android/tv/TvCommonManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvEventClient$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mstar/android/tv/TvCommonManager;Lcom/mstar/android/tv/TvCommonManager$1;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tv/TvCommonManager;
    .param p2    # Lcom/mstar/android/tv/TvCommonManager$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tv/TvCommonManager$Client;-><init>(Lcom/mstar/android/tv/TvCommonManager;)V

    return-void
.end method


# virtual methods
.method public onAtscPopupDialog(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    # getter for: Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCommonManager;->access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;->onAtscPopupDialog(III)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onDtvReadyPopupDialog(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    # getter for: Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCommonManager;->access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;->onDtvReadyPopupDialog(III)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onScartMuteOsdMode(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    # getter for: Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCommonManager;->access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;->onScartMuteOsdMode(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onScreenSaverMode(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    # getter for: Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCommonManager;->access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;->onScreenSaverMode(III)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onSignalLock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    # getter for: Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCommonManager;->access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;->onSignalLock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onSignalUnlock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    # getter for: Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCommonManager;->access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;->onSignalUnlock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onUnityEvent(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager$Client;->this$0:Lcom/mstar/android/tv/TvCommonManager;

    # getter for: Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCommonManager;->access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;->onUnityEvent(III)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method
