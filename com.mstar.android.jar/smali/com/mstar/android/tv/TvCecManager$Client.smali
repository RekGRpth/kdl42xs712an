.class Lcom/mstar/android/tv/TvCecManager$Client;
.super Lcom/mstar/android/tv/ICecEventClient$Stub;
.source "TvCecManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tv/TvCecManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Client"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/tv/TvCecManager;


# direct methods
.method private constructor <init>(Lcom/mstar/android/tv/TvCecManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/tv/TvCecManager$Client;->this$0:Lcom/mstar/android/tv/TvCecManager;

    invoke-direct {p0}, Lcom/mstar/android/tv/ICecEventClient$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mstar/android/tv/TvCecManager;Lcom/mstar/android/tv/TvCecManager$1;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tv/TvCecManager;
    .param p2    # Lcom/mstar/android/tv/TvCecManager$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tv/TvCecManager$Client;-><init>(Lcom/mstar/android/tv/TvCecManager;)V

    return-void
.end method


# virtual methods
.method public onImageViewOn(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCecManager$Client;->this$0:Lcom/mstar/android/tv/TvCecManager;

    # getter for: Lcom/mstar/android/tv/TvCecManager;->listeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCecManager;->access$000(Lcom/mstar/android/tv/TvCecManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;->onImageViewOn(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onTextViewOn(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvCecManager$Client;->this$0:Lcom/mstar/android/tv/TvCecManager;

    # getter for: Lcom/mstar/android/tv/TvCecManager;->listeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvCecManager;->access$000(Lcom/mstar/android/tv/TvCecManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;->onTextViewOn(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method
