.class public interface abstract Lcom/mstar/android/tv/ICecEventClient;
.super Ljava/lang/Object;
.source "ICecEventClient.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tv/ICecEventClient$Stub;
    }
.end annotation


# virtual methods
.method public abstract onImageViewOn(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onTextViewOn(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
