.class public interface abstract Lcom/mstar/android/tv/IAtvPlayerEventClient;
.super Ljava/lang/Object;
.source "IAtvPlayerEventClient.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tv/IAtvPlayerEventClient$Stub;
    }
.end annotation


# virtual methods
.method public abstract onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onAtvManualTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onAtvProgramInfoReady(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onSignalLock(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onSignalUnLock(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
