.class Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;
.super Ljava/lang/Object;
.source "MStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/storage/MStorageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ISOListenerDelegate"
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mISOEvnetListener:Lcom/mstar/android/storage/OnISOEvnetListener;

.field private final nonce:I

.field final synthetic this$0:Lcom/mstar/android/storage/MStorageManager;


# direct methods
.method public constructor <init>(Lcom/mstar/android/storage/MStorageManager;Lcom/mstar/android/storage/OnISOEvnetListener;)V
    .locals 2
    .param p2    # Lcom/mstar/android/storage/OnISOEvnetListener;

    iput-object p1, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->this$0:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # invokes: Lcom/mstar/android/storage/MStorageManager;->getNextNonce()I
    invoke-static {p1}, Lcom/mstar/android/storage/MStorageManager;->access$100(Lcom/mstar/android/storage/MStorageManager;)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->nonce:I

    iput-object p2, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->mISOEvnetListener:Lcom/mstar/android/storage/OnISOEvnetListener;

    new-instance v0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate$1;

    iget-object v1, p1, Lcom/mstar/android/storage/MStorageManager;->mTgtLooper:Landroid/os/Looper;

    invoke-direct {v0, p0, v1, p1}, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate$1;-><init>(Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;Landroid/os/Looper;Lcom/mstar/android/storage/MStorageManager;)V

    iput-object v0, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$200(Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;)I
    .locals 1
    .param p0    # Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;

    iget v0, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->nonce:I

    return v0
.end method


# virtual methods
.method public getListener()Lcom/mstar/android/storage/OnISOEvnetListener;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->mISOEvnetListener:Lcom/mstar/android/storage/OnISOEvnetListener;

    return-object v0
.end method

.method public sendISOStateChanged(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Lcom/mstar/android/storage/MStorageManager$ISOStateChangedStorageEvent;

    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->this$0:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {v0, v1, p1, p2}, Lcom/mstar/android/storage/MStorageManager$ISOStateChangedStorageEvent;-><init>(Lcom/mstar/android/storage/MStorageManager;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Lcom/mstar/android/storage/MStorageManager$ISOStateChangedStorageEvent;->getMessage()Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
