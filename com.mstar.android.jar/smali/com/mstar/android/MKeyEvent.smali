.class public Lcom/mstar/android/MKeyEvent;
.super Ljava/lang/Object;
.source "MKeyEvent.java"


# static fields
.field public static final KEYCODE_ASPECT_RATIO:I = 0xfd

.field public static final KEYCODE_CC:I = 0x107

.field public static final KEYCODE_CHANGHONGIR_APP:I = 0x1006

.field public static final KEYCODE_CHANGHONGIR_BARCODE:I = 0x100c

.field public static final KEYCODE_CHANGHONGIR_FAC_3D:I = 0x100f

.field public static final KEYCODE_CHANGHONGIR_FAC_AUTOSCAN:I = 0x101c

.field public static final KEYCODE_CHANGHONGIR_FAC_AV1:I = 0x1011

.field public static final KEYCODE_CHANGHONGIR_FAC_AV2:I = 0x1012

.field public static final KEYCODE_CHANGHONGIR_FAC_CSYS:I = 0x1019

.field public static final KEYCODE_CHANGHONGIR_FAC_FACOUT:I = 0x1018

.field public static final KEYCODE_CHANGHONGIR_FAC_HDMI1:I = 0x1014

.field public static final KEYCODE_CHANGHONGIR_FAC_M:I = 0x1017

.field public static final KEYCODE_CHANGHONGIR_FAC_MAC:I = 0x1010

.field public static final KEYCODE_CHANGHONGIR_FAC_PCAUTO:I = 0x100d

.field public static final KEYCODE_CHANGHONGIR_FAC_PCPHASE:I = 0x101d

.field public static final KEYCODE_CHANGHONGIR_FAC_SBAL:I = 0x101b

.field public static final KEYCODE_CHANGHONGIR_FAC_SSYS:I = 0x101a

.field public static final KEYCODE_CHANGHONGIR_FAC_USB1:I = 0x1015

.field public static final KEYCODE_CHANGHONGIR_FAC_VGA:I = 0x1016

.field public static final KEYCODE_CHANGHONGIR_FAC_YPBPR:I = 0x1013

.field public static final KEYCODE_CHANGHONGIR_FAC_YUVAUTO:I = 0x100e

.field public static final KEYCODE_CHANGHONGIR_FLCK_SL:I = 0x1009

.field public static final KEYCODE_CHANGHONGIR_FLCK_SR:I = 0x100a

.field public static final KEYCODE_CHANGHONGIR_HELP:I = 0x1005

.field public static final KEYCODE_CHANGHONGIR_INPUT:I = 0x100b

.field public static final KEYCODE_CHANGHONGIR_PINCH:I = 0x1008

.field public static final KEYCODE_CHANGHONGIR_SPREAD:I = 0x1007

.field public static final KEYCODE_CHANNEL_RETURN:I = 0xfe

.field public static final KEYCODE_CLOUD:I = 0x10a

.field public static final KEYCODE_DISPLAY_MODE:I = 0x10e

.field public static final KEYCODE_EPG:I = 0x100

.field public static final KEYCODE_FAVORITE:I = 0x103

.field public static final KEYCODE_FREEZE:I = 0x105

.field public static final KEYCODE_HAIER_CLEANSEARCH:I = 0x196

.field public static final KEYCODE_HAIER_POWERSLEEP:I = 0x193

.field public static final KEYCODE_HAIER_TASK:I = 0x191

.field public static final KEYCODE_HAIER_TOOLS:I = 0x192

.field public static final KEYCODE_HAIER_UNMUTE:I = 0x195

.field public static final KEYCODE_HAIER_WAKEUP:I = 0x194

.field public static final KEYCODE_HDMI:I = 0x10d

.field public static final KEYCODE_HISENSE_FAC_NEC_3D:I = 0x1076

.field public static final KEYCODE_HISENSE_FAC_NEC_ADC:I = 0x1088

.field public static final KEYCODE_HISENSE_FAC_NEC_AGING:I = 0x1086

.field public static final KEYCODE_HISENSE_FAC_NEC_AV:I = 0x1084

.field public static final KEYCODE_HISENSE_FAC_NEC_BALANCE:I = 0x1087

.field public static final KEYCODE_HISENSE_FAC_NEC_BCUT_DECREASE:I = 0x1094

.field public static final KEYCODE_HISENSE_FAC_NEC_BCUT_INCREASE:I = 0x1093

.field public static final KEYCODE_HISENSE_FAC_NEC_BDRV_DECREASE:I = 0x108e

.field public static final KEYCODE_HISENSE_FAC_NEC_BDRV_INCREASE:I = 0x108d

.field public static final KEYCODE_HISENSE_FAC_NEC_F1:I = 0x107b

.field public static final KEYCODE_HISENSE_FAC_NEC_F2:I = 0x107c

.field public static final KEYCODE_HISENSE_FAC_NEC_F3:I = 0x107d

.field public static final KEYCODE_HISENSE_FAC_NEC_F4:I = 0x107e

.field public static final KEYCODE_HISENSE_FAC_NEC_F5:I = 0x107f

.field public static final KEYCODE_HISENSE_FAC_NEC_F6:I = 0x1080

.field public static final KEYCODE_HISENSE_FAC_NEC_F7:I = 0x1081

.field public static final KEYCODE_HISENSE_FAC_NEC_GCUT_DECREASE:I = 0x1092

.field public static final KEYCODE_HISENSE_FAC_NEC_GCUT_INCREASE:I = 0x1091

.field public static final KEYCODE_HISENSE_FAC_NEC_GDRV_DECREASE:I = 0x108c

.field public static final KEYCODE_HISENSE_FAC_NEC_GDRV_INCREASE:I = 0x108b

.field public static final KEYCODE_HISENSE_FAC_NEC_HDMI:I = 0x107a

.field public static final KEYCODE_HISENSE_FAC_NEC_IP:I = 0x1074

.field public static final KEYCODE_HISENSE_FAC_NEC_LOGO:I = 0x1078

.field public static final KEYCODE_HISENSE_FAC_NEC_M:I = 0x1073

.field public static final KEYCODE_HISENSE_FAC_NEC_MAC:I = 0x1083

.field public static final KEYCODE_HISENSE_FAC_NEC_OK:I = 0x1082

.field public static final KEYCODE_HISENSE_FAC_NEC_PATTERN:I = 0x1085

.field public static final KEYCODE_HISENSE_FAC_NEC_PC:I = 0x1077

.field public static final KEYCODE_HISENSE_FAC_NEC_RCUT_DECREASE:I = 0x1090

.field public static final KEYCODE_HISENSE_FAC_NEC_RCUT_INCREASE:I = 0x108f

.field public static final KEYCODE_HISENSE_FAC_NEC_RDRV_DECREASE:I = 0x108a

.field public static final KEYCODE_HISENSE_FAC_NEC_RDRV_INCREASE:I = 0x1089

.field public static final KEYCODE_HISENSE_FAC_NEC_SAVE:I = 0x1075

.field public static final KEYCODE_HISENSE_FAC_NEC_YPBPR:I = 0x1079

.field public static final KEYCODE_HISENSE_G_SENSOR:I = 0x1069

.field public static final KEYCODE_HISENSE_LOW_BATTERY:I = 0x106a

.field public static final KEYCODE_HISENSE_PRODUCT_SCAN_OVER:I = 0x1096

.field public static final KEYCODE_HISENSE_PRODUCT_SCAN_START:I = 0x1095

.field public static final KEYCODE_HISENSE_RAPID_SLIDEDOWN:I = 0x1070

.field public static final KEYCODE_HISENSE_RAPID_SLIDELEFT:I = 0x1071

.field public static final KEYCODE_HISENSE_RAPID_SLIDERIGHT:I = 0x1072

.field public static final KEYCODE_HISENSE_RAPID_SLIDEUP:I = 0x106f

.field public static final KEYCODE_HISENSE_SLIDEDOWN:I = 0x106c

.field public static final KEYCODE_HISENSE_SLIDELEFT:I = 0x106d

.field public static final KEYCODE_HISENSE_SLIDERIGHT:I = 0x106e

.field public static final KEYCODE_HISENSE_SLIDEUP:I = 0x106b

.field public static final KEYCODE_HISENSE_TEST_BOARD_AUTOCOLOR:I = 0x10ad

.field public static final KEYCODE_HISENSE_TEST_BOARD_BTSC:I = 0x10b3

.field public static final KEYCODE_HISENSE_TEST_BOARD_CCD:I = 0x10b2

.field public static final KEYCODE_HISENSE_TEST_BOARD_DMP:I = 0x10ab

.field public static final KEYCODE_HISENSE_TEST_BOARD_EMP:I = 0x10ac

.field public static final KEYCODE_HISENSE_TEST_BOARD_FAC_OK:I = 0x10b4

.field public static final KEYCODE_HISENSE_TEST_BOARD_HDMI1:I = 0x10a6

.field public static final KEYCODE_HISENSE_TEST_BOARD_HDMI2:I = 0x10a7

.field public static final KEYCODE_HISENSE_TEST_BOARD_HDMI3:I = 0x10a8

.field public static final KEYCODE_HISENSE_TEST_BOARD_HDMI4:I = 0x10a9

.field public static final KEYCODE_HISENSE_TEST_BOARD_HDMI5:I = 0x10aa

.field public static final KEYCODE_HISENSE_TEST_BOARD_SAPL:I = 0x10b0

.field public static final KEYCODE_HISENSE_TEST_BOARD_SAVE:I = 0x10ae

.field public static final KEYCODE_HISENSE_TEST_BOARD_TELITEXT:I = 0x10af

.field public static final KEYCODE_HISENSE_TEST_BOARD_VCHIP:I = 0x10b1

.field public static final KEYCODE_HISENSE_TEST_BOARD_VGA:I = 0x10a5

.field public static final KEYCODE_HISENSE_TEST_BOARD_YPBPR1:I = 0x10a2

.field public static final KEYCODE_HISENSE_TEST_BOARD_YPBPR2:I = 0x10a3

.field public static final KEYCODE_HISENSE_TEST_BOARD_YPBPR3:I = 0x10a4

.field public static final KEYCODE_HISENSE_TEST_BROAD_AV1:I = 0x1099

.field public static final KEYCODE_HISENSE_TEST_BROAD_AV2:I = 0x109a

.field public static final KEYCODE_HISENSE_TEST_BROAD_AV3:I = 0x109b

.field public static final KEYCODE_HISENSE_TEST_BROAD_DTV:I = 0x1098

.field public static final KEYCODE_HISENSE_TEST_BROAD_SCART1:I = 0x109f

.field public static final KEYCODE_HISENSE_TEST_BROAD_SCART2:I = 0x10a0

.field public static final KEYCODE_HISENSE_TEST_BROAD_SCART3:I = 0x10a1

.field public static final KEYCODE_HISENSE_TEST_BROAD_SVIDEO1:I = 0x109c

.field public static final KEYCODE_HISENSE_TEST_BROAD_SVIDEO2:I = 0x109d

.field public static final KEYCODE_HISENSE_TEST_BROAD_SVIDEO3:I = 0x109e

.field public static final KEYCODE_HISENSE_TEST_BROAD_TV:I = 0x1097

.field public static final KEYCODE_KONKA_3D_SETTING:I = 0x205

.field public static final KEYCODE_KONKA_AUTO_SEARCH:I = 0x21b

.field public static final KEYCODE_KONKA_BACKLIGHT:I = 0x211

.field public static final KEYCODE_KONKA_BESTV_BACKWARD:I = 0x200

.field public static final KEYCODE_KONKA_BESTV_EXIT:I = 0x1fe

.field public static final KEYCODE_KONKA_BESTV_FORWARD:I = 0x1ff

.field public static final KEYCODE_KONKA_BURN_HDCPKEY:I = 0x219

.field public static final KEYCODE_KONKA_BURN_MAC:I = 0x218

.field public static final KEYCODE_KONKA_BURN_MODE:I = 0x20d

.field public static final KEYCODE_KONKA_BURN_SERIAL_NUMBER:I = 0x21a

.field public static final KEYCODE_KONKA_ENTER_ATV:I = 0x206

.field public static final KEYCODE_KONKA_ENTER_AV:I = 0x20c

.field public static final KEYCODE_KONKA_ENTER_DTV:I = 0x207

.field public static final KEYCODE_KONKA_ENTER_FACTORY:I = 0x201

.field public static final KEYCODE_KONKA_ENTER_FACTORY_MENU:I = 0x21c

.field public static final KEYCODE_KONKA_ENTER_HDMI:I = 0x20b

.field public static final KEYCODE_KONKA_ENTER_USB:I = 0x209

.field public static final KEYCODE_KONKA_ENTER_VGA:I = 0x208

.field public static final KEYCODE_KONKA_ENTER_YPBPR:I = 0x20a

.field public static final KEYCODE_KONKA_FACTORY_BAKE_TV:I = 0x202

.field public static final KEYCODE_KONKA_FACTORY_DEFAULT:I = 0x20e

.field public static final KEYCODE_KONKA_FLYIMEFINGER_CANCEL:I = 0x1fb

.field public static final KEYCODE_KONKA_FLYIMEFINGER_SELECT:I = 0x1fa

.field public static final KEYCODE_KONKA_HOME:I = 0x217

.field public static final KEYCODE_KONKA_LANGUAGE:I = 0x213

.field public static final KEYCODE_KONKA_MICMUTE:I = 0x212

.field public static final KEYCODE_KONKA_MIX:I = 0x203

.field public static final KEYCODE_KONKA_NET:I = 0x216

.field public static final KEYCODE_KONKA_NICAM:I = 0x214

.field public static final KEYCODE_KONKA_PICTURE_MODE:I = 0x21e

.field public static final KEYCODE_KONKA_QUICKSEARCH:I = 0x204

.field public static final KEYCODE_KONKA_SOUNDOUTPUT_DISABLE:I = 0x1fd

.field public static final KEYCODE_KONKA_SOUNDOUTPUT_ENABLE:I = 0x1fc

.field public static final KEYCODE_KONKA_SOUND_BALANCE:I = 0x20f

.field public static final KEYCODE_KONKA_SOUND_MODE:I = 0x21d

.field public static final KEYCODE_KONKA_TEXT:I = 0x215

.field public static final KEYCODE_KONKA_THREEPOINT_COLLECT:I = 0x1f7

.field public static final KEYCODE_KONKA_THREEPOINT_DISPERSE:I = 0x1f8

.field public static final KEYCODE_KONKA_THREEPOINT_LOONPRESS:I = 0x1f6

.field public static final KEYCODE_KONKA_VOICESWITCH:I = 0x1f9

.field public static final KEYCODE_KONKA_VOLUME_TEST:I = 0x210

.field public static final KEYCODE_KONKA_YPBPR:I = 0x1f5

.field public static final KEYCODE_LIST:I = 0x101

.field public static final KEYCODE_MSTAR_AUDIO_TRACK:I = 0x13a

.field public static final KEYCODE_MSTAR_BALANCE:I = 0x12d

.field public static final KEYCODE_MSTAR_CLOCK:I = 0x134

.field public static final KEYCODE_MSTAR_FILE:I = 0x138

.field public static final KEYCODE_MSTAR_HOLD:I = 0x12f

.field public static final KEYCODE_MSTAR_INBOX:I = 0x13d

.field public static final KEYCODE_MSTAR_INDEX:I = 0x12e

.field public static final KEYCODE_MSTAR_LOOP:I = 0x13c

.field public static final KEYCODE_MSTAR_MOVIE:I = 0x137

.field public static final KEYCODE_MSTAR_OPTIONAL_TIME:I = 0x13b

.field public static final KEYCODE_MSTAR_OSC:I = 0x21f

.field public static final KEYCODE_MSTAR_REVEAL:I = 0x131

.field public static final KEYCODE_MSTAR_SIZE:I = 0x133

.field public static final KEYCODE_MSTAR_STAR_PLUS:I = 0x139

.field public static final KEYCODE_MSTAR_STORE_UP:I = 0x135

.field public static final KEYCODE_MSTAR_SUBCODE:I = 0x132

.field public static final KEYCODE_MSTAR_TRIANGLE_UP:I = 0x136

.field public static final KEYCODE_MSTAR_UPDATE:I = 0x130

.field public static final KEYCODE_MSTAR_VVOIP:I = 0x13e

.field public static final KEYCODE_MTS:I = 0x104

.field public static final KEYCODE_PICTURE_MODE:I = 0xfc

.field public static final KEYCODE_SCREENSHOT:I = 0x109

.field public static final KEYCODE_SLEEP:I = 0xff

.field public static final KEYCODE_SONG_SYSTEM:I = 0x10f

.field public static final KEYCODE_SOUND_MODE:I = 0xfb

.field public static final KEYCODE_SUBTITLE:I = 0x102

.field public static final KEYCODE_TCL_BODY_SENSOR:I = 0xfb4

.field public static final KEYCODE_TCL_CIRCLE_CLOCKWISE:I = 0xfb5

.field public static final KEYCODE_TCL_CIRCLE_CTR_CLOCKWISE:I = 0xfb6

.field public static final KEYCODE_TCL_DOWN:I = 0xfbb

.field public static final KEYCODE_TCL_DOWN_LEFT:I = 0xfc0

.field public static final KEYCODE_TCL_DOWN_RIGHT:I = 0xfc1

.field public static final KEYCODE_TCL_GESTURE_ALPHA:I = 0xfb8

.field public static final KEYCODE_TCL_GESTURE_MUTE:I = 0xfb9

.field public static final KEYCODE_TCL_GESTURE_X:I = 0xfb7

.field public static final KEYCODE_TCL_LEFT:I = 0xfbc

.field public static final KEYCODE_TCL_MITV:I = 0xfa1

.field public static final KEYCODE_TCL_RIGHT:I = 0xfbd

.field public static final KEYCODE_TCL_SWING_L1:I = 0xfa7

.field public static final KEYCODE_TCL_SWING_L2:I = 0xfa8

.field public static final KEYCODE_TCL_SWING_L3:I = 0xfa9

.field public static final KEYCODE_TCL_SWING_L4:I = 0xfaa

.field public static final KEYCODE_TCL_SWING_R1:I = 0xfa3

.field public static final KEYCODE_TCL_SWING_R2:I = 0xfa4

.field public static final KEYCODE_TCL_SWING_R3:I = 0xfa5

.field public static final KEYCODE_TCL_SWING_R4:I = 0xfa6

.field public static final KEYCODE_TCL_UP:I = 0xfba

.field public static final KEYCODE_TCL_UP_LEFT:I = 0xfbe

.field public static final KEYCODE_TCL_UP_RIGHT:I = 0xfbf

.field public static final KEYCODE_TCL_USB_MENU:I = 0xfa2

.field public static final KEYCODE_TCL_VGR_ACTIVE:I = 0xfb2

.field public static final KEYCODE_TCL_VGR_DEACTIVE:I = 0xfb3

.field public static final KEYCODE_TCL_VGR_LEFT:I = 0xfac

.field public static final KEYCODE_TCL_VGR_RIGHT:I = 0xfad

.field public static final KEYCODE_TCL_VGR_TAP:I = 0xfae

.field public static final KEYCODE_TCL_VGR_WAVE:I = 0xfaf

.field public static final KEYCODE_TCL_VGR_WAVE_LEFT:I = 0xfb0

.field public static final KEYCODE_TCL_VGR_WAVE_RIGHT:I = 0xfb1

.field public static final KEYCODE_TCL_WIDGET:I = 0xfab

.field public static final KEYCODE_TTX:I = 0x106

.field public static final KEYCODE_TV_SETTING:I = 0x108

.field public static final KEYCODE_USB:I = 0x10c

.field public static final KEYCODE_VOICE:I = 0x10b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
