.class public Lcom/mstar/android/sip/SipSession$Listener;
.super Ljava/lang/Object;
.source "SipSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/sip/SipSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Listener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallBusy(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method

.method public onCallChangeFailed(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onCallEnded(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method

.method public onCallEstablished(Lcom/mstar/android/sip/SipSession;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onCallTransferring(Lcom/mstar/android/sip/SipSession;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onCalling(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method

.method public onError(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRegistering(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method

.method public onRegistrationDone(Lcom/mstar/android/sip/SipSession;I)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I

    return-void
.end method

.method public onRegistrationFailed(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRegistrationTimeout(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method

.method public onRinging(Lcom/mstar/android/sip/SipSession;Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # Lcom/mstar/android/sip/SipProfile;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRingingBack(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method
