.class Lcom/mstar/android/sip/SipAudioCall$1;
.super Lcom/mstar/android/sip/SipSession$Listener;
.source "SipAudioCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mstar/android/sip/SipAudioCall;->createListener()Lcom/mstar/android/sip/SipSession$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/sip/SipAudioCall;


# direct methods
.method constructor <init>(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-direct {p0}, Lcom/mstar/android/sip/SipSession$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallBusy(Lcom/mstar/android/sip/SipSession;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sip call busy: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mListener:Lcom/mstar/android/sip/SipAudioCall$Listener;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$100(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipAudioCall$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {v0, v2}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onCallBusy(Lcom/mstar/android/sip/SipAudioCall;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    const/4 v3, 0x0

    # invokes: Lcom/mstar/android/sip/SipAudioCall;->close(Z)V
    invoke-static {v2, v3}, Lcom/mstar/android/sip/SipAudioCall;->access$900(Lcom/mstar/android/sip/SipAudioCall;Z)V

    return-void

    :catch_0
    move-exception v1

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCallBusy(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCallChangeFailed(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sip call change failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # setter for: Lcom/mstar/android/sip/SipAudioCall;->mErrorCode:I
    invoke-static {v2, p2}, Lcom/mstar/android/sip/SipAudioCall;->access$1002(Lcom/mstar/android/sip/SipAudioCall;I)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # setter for: Lcom/mstar/android/sip/SipAudioCall;->mErrorMessage:Ljava/lang/String;
    invoke-static {v2, p3}, Lcom/mstar/android/sip/SipAudioCall;->access$1102(Lcom/mstar/android/sip/SipAudioCall;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mListener:Lcom/mstar/android/sip/SipAudioCall$Listener;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$100(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipAudioCall$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    iget-object v3, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mErrorCode:I
    invoke-static {v3}, Lcom/mstar/android/sip/SipAudioCall;->access$1000(Lcom/mstar/android/sip/SipAudioCall;)I

    move-result v3

    invoke-virtual {v0, v2, v3, p3}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onError(Lcom/mstar/android/sip/SipAudioCall;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCallBusy(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCallEnded(Lcom/mstar/android/sip/SipSession;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sip call ended: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mSipSession:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mSipSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v4}, Lcom/mstar/android/sip/SipAudioCall;->access$200(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mTransferringSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$600(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v2

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    const/4 v3, 0x0

    # setter for: Lcom/mstar/android/sip/SipAudioCall;->mTransferringSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2, v3}, Lcom/mstar/android/sip/SipAudioCall;->access$602(Lcom/mstar/android/sip/SipAudioCall;Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mTransferringSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$600(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mSipSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$200(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v2

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mListener:Lcom/mstar/android/sip/SipAudioCall$Listener;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$100(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipAudioCall$Listener;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {v0, v2}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onCallEnded(Lcom/mstar/android/sip/SipAudioCall;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {v2}, Lcom/mstar/android/sip/SipAudioCall;->close()V

    goto :goto_0

    :catch_0
    move-exception v1

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCallEnded(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCallEstablished(Lcom/mstar/android/sip/SipSession;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # setter for: Lcom/mstar/android/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;
    invoke-static {v2, p2}, Lcom/mstar/android/sip/SipAudioCall;->access$502(Lcom/mstar/android/sip/SipAudioCall;Ljava/lang/String;)Ljava/lang/String;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCallEstablished()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mPeerSd:Ljava/lang/String;
    invoke-static {v4}, Lcom/mstar/android/sip/SipAudioCall;->access$500(Lcom/mstar/android/sip/SipAudioCall;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mTransferringSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$600(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mTransferringSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$600(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v2

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # invokes: Lcom/mstar/android/sip/SipAudioCall;->transferToNewSession()V
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$700(Lcom/mstar/android/sip/SipAudioCall;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mListener:Lcom/mstar/android/sip/SipAudioCall$Listener;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$100(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipAudioCall$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mHold:Z
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$800(Lcom/mstar/android/sip/SipAudioCall;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {v0, v2}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onCallHeld(Lcom/mstar/android/sip/SipAudioCall;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCallEstablished(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {v0, v2}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onCallEstablished(Lcom/mstar/android/sip/SipAudioCall;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onCallTransferring(Lcom/mstar/android/sip/SipSession;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # Ljava/lang/String;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCallTransferring mSipSession:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mSipSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v4}, Lcom/mstar/android/sip/SipAudioCall;->access$200(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newSession:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # setter for: Lcom/mstar/android/sip/SipAudioCall;->mTransferringSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2, p1}, Lcom/mstar/android/sip/SipAudioCall;->access$602(Lcom/mstar/android/sip/SipAudioCall;Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession;

    if-nez p2, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/sip/SipSession;->getPeerProfile()Lcom/mstar/android/sip/SipProfile;

    move-result-object v2

    iget-object v3, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # invokes: Lcom/mstar/android/sip/SipAudioCall;->createOffer()Lcom/mstar/android/sip/SimpleSessionDescription;
    invoke-static {v3}, Lcom/mstar/android/sip/SipAudioCall;->access$1300(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SimpleSessionDescription;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/sip/SimpleSessionDescription;->encode()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xf

    invoke-virtual {p1, v2, v3, v4}, Lcom/mstar/android/sip/SipSession;->makeCall(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # invokes: Lcom/mstar/android/sip/SipAudioCall;->createAnswer(Ljava/lang/String;)Lcom/mstar/android/sip/SimpleSessionDescription;
    invoke-static {v2, p2}, Lcom/mstar/android/sip/SipAudioCall;->access$400(Lcom/mstar/android/sip/SipAudioCall;Ljava/lang/String;)Lcom/mstar/android/sip/SimpleSessionDescription;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/sip/SimpleSessionDescription;->encode()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {p1, v0, v2}, Lcom/mstar/android/sip/SipSession;->answerCall(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "onCallTransferring()"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipSession;->endCall()V

    goto :goto_0
.end method

.method public onCalling(Lcom/mstar/android/sip/SipSession;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calling... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mListener:Lcom/mstar/android/sip/SipAudioCall$Listener;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$100(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipAudioCall$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {v0, v2}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onCalling(Lcom/mstar/android/sip/SipAudioCall;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCalling(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onError(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V
    .locals 1
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # invokes: Lcom/mstar/android/sip/SipAudioCall;->onError(ILjava/lang/String;)V
    invoke-static {v0, p2, p3}, Lcom/mstar/android/sip/SipAudioCall;->access$1200(Lcom/mstar/android/sip/SipAudioCall;ILjava/lang/String;)V

    return-void
.end method

.method public onRegistering(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method

.method public onRegistrationDone(Lcom/mstar/android/sip/SipSession;I)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I

    return-void
.end method

.method public onRegistrationFailed(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRegistrationTimeout(Lcom/mstar/android/sip/SipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipSession;

    return-void
.end method

.method public onRinging(Lcom/mstar/android/sip/SipSession;Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;
    .param p2    # Lcom/mstar/android/sip/SipProfile;
    .param p3    # Ljava/lang/String;

    iget-object v3, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mSipSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$200(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mInCall:Z
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$300(Lcom/mstar/android/sip/SipAudioCall;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipSession;->getCallId()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mSipSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v4}, Lcom/mstar/android/sip/SipAudioCall;->access$200(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/sip/SipSession;->getCallId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/android/sip/SipSession;->endCall()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # invokes: Lcom/mstar/android/sip/SipAudioCall;->createAnswer(Ljava/lang/String;)Lcom/mstar/android/sip/SimpleSessionDescription;
    invoke-static {v2, p3}, Lcom/mstar/android/sip/SipAudioCall;->access$400(Lcom/mstar/android/sip/SipAudioCall;Ljava/lang/String;)Lcom/mstar/android/sip/SimpleSessionDescription;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/sip/SimpleSessionDescription;->encode()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mSipSession:Lcom/mstar/android/sip/SipSession;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$200(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipSession;

    move-result-object v2

    const/4 v4, 0x5

    invoke-virtual {v2, v0, v4}, Lcom/mstar/android/sip/SipSession;->answerCall(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :catch_0
    move-exception v1

    :try_start_3
    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v4, "onRinging()"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipSession;->endCall()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public onRingingBack(Lcom/mstar/android/sip/SipSession;)V
    .locals 5
    .param p1    # Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sip call ringing back: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->mListener:Lcom/mstar/android/sip/SipAudioCall$Listener;
    invoke-static {v2}, Lcom/mstar/android/sip/SipAudioCall;->access$100(Lcom/mstar/android/sip/SipAudioCall;)Lcom/mstar/android/sip/SipAudioCall$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipAudioCall$1;->this$0:Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {v0, v2}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onRingingBack(Lcom/mstar/android/sip/SipAudioCall;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    # getter for: Lcom/mstar/android/sip/SipAudioCall;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/mstar/android/sip/SipAudioCall;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onRingingBack(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
