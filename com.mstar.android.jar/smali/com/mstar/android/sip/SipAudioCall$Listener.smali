.class public Lcom/mstar/android/sip/SipAudioCall$Listener;
.super Ljava/lang/Object;
.source "SipAudioCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/sip/SipAudioCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Listener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallBusy(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method

.method public onCallEnded(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method

.method public onCallEstablished(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method

.method public onCallHeld(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method

.method public onCalling(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method

.method public onChanged(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    return-void
.end method

.method public onError(Lcom/mstar/android/sip/SipAudioCall;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onReadyToCall(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method

.method public onRinging(Lcom/mstar/android/sip/SipAudioCall;Lcom/mstar/android/sip/SipProfile;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;
    .param p2    # Lcom/mstar/android/sip/SipProfile;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method

.method public onRingingBack(Lcom/mstar/android/sip/SipAudioCall;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/SipAudioCall;

    invoke-virtual {p0, p1}, Lcom/mstar/android/sip/SipAudioCall$Listener;->onChanged(Lcom/mstar/android/sip/SipAudioCall;)V

    return-void
.end method
