.class public final enum Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;
.super Ljava/lang/Enum;
.source "EnumSetProgramCtrl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_COPY_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_DEC_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_DELETE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_INC_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_INIT_ALL_CHANNEL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_MOVE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_RESET_CHANNEL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_SET_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

.field public static final enum E_SWAP_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_RESET_CHANNEL_DATA"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_RESET_CHANNEL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_INIT_ALL_CHANNEL_DATA"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_INIT_ALL_CHANNEL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_SET_CURRENT_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_SET_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_INC_CURRENT_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_INC_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_DEC_CURRENT_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_DEC_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_DELETE_PROGRAM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_DELETE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_MOVE_PROGRAM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_MOVE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_SWAP_PROGRAM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_SWAP_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const-string v1, "E_COPY_PROGRAM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_COPY_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_RESET_CHANNEL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_INIT_ALL_CHANNEL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_SET_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_INC_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_DEC_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_DELETE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_MOVE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_SWAP_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->E_COPY_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    return-object v0
.end method
