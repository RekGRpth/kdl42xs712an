.class public final enum Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;
.super Ljava/lang/Enum;
.source "EnumSetProgramInfo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_ENABLE_REALTIME_AUDIO_DETECTION:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_HIDE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_LOCK_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_NEED_AFT:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_AFT_OFFSET:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_AUDIO_STANDARD:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_CHANNEL_INDEX:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_DIRECT_TUNED:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_MISC:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_PROGRAM_PLL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_SORTING_PRIORITY:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_STATION_NAME:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

.field public static final enum E_SKIP_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_PROGRAM_PLL_DATA"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_PROGRAM_PLL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_AUDIO_STANDARD"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_AUDIO_STANDARD:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_VIDEO_STANDARD_OF_PROGRAM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SKIP_PROGRAM"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SKIP_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_HIDE_PROGRAM"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_HIDE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_LOCK_PROGRAM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_LOCK_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_NEED_AFT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_NEED_AFT:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_DIRECT_TUNED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_DIRECT_TUNED:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_AFT_OFFSET"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_AFT_OFFSET:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_ENABLE_REALTIME_AUDIO_DETECTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_ENABLE_REALTIME_AUDIO_DETECTION:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_STATION_NAME"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_STATION_NAME:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_SORTING_PRIORITY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_SORTING_PRIORITY:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_CHANNEL_INDEX"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_CHANNEL_INDEX:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const-string v1, "E_SET_MISC"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_MISC:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_PROGRAM_PLL_DATA:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_AUDIO_STANDARD:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SKIP_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_HIDE_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_LOCK_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_NEED_AFT:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_DIRECT_TUNED:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_AFT_OFFSET:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_ENABLE_REALTIME_AUDIO_DETECTION:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_STATION_NAME:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_SORTING_PRIORITY:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_CHANNEL_INDEX:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_MISC:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    return-object v0
.end method
