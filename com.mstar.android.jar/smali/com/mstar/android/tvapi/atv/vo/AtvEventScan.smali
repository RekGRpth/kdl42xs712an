.class public Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;
.super Ljava/lang/Object;
.source "AtvEventScan.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bIsScaningEnable:Z

.field public curScannedChannel:S

.field public frequencyKHz:I

.field public percent:S

.field public scannedChannelNum:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->percent:S

    iput v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->frequencyKHz:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->scannedChannelNum:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->curScannedChannel:S

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->bIsScaningEnable:Z

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->percent:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->frequencyKHz:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->scannedChannelNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->curScannedChannel:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->bIsScaningEnable:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/atv/vo/AtvEventScan$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->percent:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->frequencyKHz:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->scannedChannelNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->curScannedChannel:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->bIsScaningEnable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
