.class public interface abstract Lcom/mstar/android/tvapi/atv/AtvPlayer;
.super Ljava/lang/Object;
.source "AtvPlayer.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/TvPlayer;


# virtual methods
.method public abstract disableAft()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract enableAft()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract initAtvVif()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract isAftEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract saveAtvProgram(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setChannelChangeFreezeMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setOnAtvPlayerEventListener(Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;)V
.end method
