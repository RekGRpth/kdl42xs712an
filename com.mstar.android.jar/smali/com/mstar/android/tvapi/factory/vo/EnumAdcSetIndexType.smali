.class public final enum Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;
.super Ljava/lang/Enum;
.source "EnumAdcSetIndexType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_NUMS:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_SCART_RGB:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_VGA:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_YPBPR2_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_YPBPR2_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_YPBPR3_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_YPBPR3_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_YPBPR_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

.field public static final enum E_ADC_SET_YPBPR_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_VGA"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_VGA:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_YPBPR_SD"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_YPBPR_HD"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_SCART_RGB"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_SCART_RGB:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_YPBPR2_SD"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR2_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_YPBPR2_HD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR2_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_YPBPR3_SD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR3_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_YPBPR3_HD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR3_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v1, "E_ADC_SET_NUMS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_NUMS:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_VGA:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_SCART_RGB:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR2_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR2_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR3_SD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_YPBPR3_HD:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->E_ADC_SET_NUMS:Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    return-object v0
.end method
