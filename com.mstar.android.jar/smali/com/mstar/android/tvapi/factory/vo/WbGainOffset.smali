.class public Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;
.super Ljava/lang/Object;
.source "WbGainOffset.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public blueGain:S

.field public blueOffset:S

.field public greenGain:S

.field public greenOffset:S

.field public redGain:S

.field public redOffset:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->redGain:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->greenGain:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->blueGain:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->redOffset:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->greenOffset:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->blueOffset:S

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->redGain:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->greenGain:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->blueGain:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->redOffset:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->greenOffset:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->blueOffset:S

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/factory/vo/WbGainOffset$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/factory/vo/WbGainOffset$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->redGain:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->greenGain:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->blueGain:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->redOffset:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->greenOffset:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;->blueOffset:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
