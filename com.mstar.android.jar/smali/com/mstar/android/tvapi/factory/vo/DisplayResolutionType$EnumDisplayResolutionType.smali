.class public final enum Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;
.super Ljava/lang/Enum;
.source "DisplayResolutionType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumDisplayResolutionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_CMO_CMO260J2_WUXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_1080I_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_1080I_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_1080P_24:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_1080P_25:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_1080P_30:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_1080P_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_1080P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_480I:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_480P:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_576I:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_576P:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_720P_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_720P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_AUTO:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_NTSC_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_NTSC_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_PAL_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_DACOUT_PAL_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_RES_FULL_HD:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_RES_MAX_NUM:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_RES_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_RES_SXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_RES_WSXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_RES_WXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_RES_WXGA_PLUS:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_SEC32_LE32A_FULLHD:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_TTLOUT_480X272_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_TTLOUT_60_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_TTLOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_VGAOUT_60_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_VGAOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field public static final enum E_DISPLAY_VGAOUT_640x480P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_RES_MIN"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_SEC32_LE32A_FULLHD"

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v2

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_SEC32_LE32A_FULLHD:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_RES_SXGA"

    invoke-direct {v0, v1, v6, v5}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_SXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_RES_WXGA"

    invoke-direct {v0, v1, v7, v6}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_WXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_RES_WXGA_PLUS"

    invoke-direct {v0, v1, v8, v7}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_WXGA_PLUS:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_RES_WSXGA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_WSXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_RES_FULL_HD"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_FULL_HD:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_PAL_MIN"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_PAL_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_576I"

    const/16 v2, 0x8

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_PAL_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_576I:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_576P"

    const/16 v2, 0x9

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_576P:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_720P_50"

    const/16 v2, 0xa

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_720P_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_1080P_24"

    const/16 v2, 0xb

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_24:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_1080P_25"

    const/16 v2, 0xc

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_25:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_1080I_50"

    const/16 v2, 0xd

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080I_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_1080P_50"

    const/16 v2, 0xe

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_PAL_MAX"

    const/16 v2, 0xf

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_PAL_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_NTSC_MIN"

    const/16 v2, 0x10

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_NTSC_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_480I"

    const/16 v2, 0x11

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_NTSC_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_480I:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_480P"

    const/16 v2, 0x12

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_480P:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_720P_60"

    const/16 v2, 0x13

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_720P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_1080P_30"

    const/16 v2, 0x14

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_30:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_1080I_60"

    const/16 v2, 0x15

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080I_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_1080P_60"

    const/16 v2, 0x16

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_NTSC_MAX"

    const/16 v2, 0x17

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_NTSC_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_DACOUT_AUTO"

    const/16 v2, 0x18

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_AUTO:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_CMO_CMO260J2_WUXGA"

    const/16 v2, 0x19

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_CMO_CMO260J2_WUXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_VGAOUT_60_MIN"

    const/16 v2, 0x1a

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_VGAOUT_640x480P_60"

    const/16 v2, 0x1b

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_640x480P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_VGAOUT_60_MAX"

    const/16 v2, 0x1c

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_640x480P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_60_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_TTLOUT_60_MIN"

    const/16 v2, 0x1d

    const/16 v3, 0xc0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_TTLOUT_480X272_60"

    const/16 v2, 0x1e

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_480X272_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_TTLOUT_60_MAX"

    const/16 v2, 0x1f

    sget-object v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_480X272_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_60_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const-string v1, "E_DISPLAY_RES_MAX_NUM"

    const/16 v2, 0x20

    const/16 v3, 0xc1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_MAX_NUM:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    const/16 v0, 0x21

    new-array v0, v0, [Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_SEC32_LE32A_FULLHD:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_SXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_WXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_WXGA_PLUS:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_WSXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_FULL_HD:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_PAL_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_576I:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_576P:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_720P_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_24:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_25:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080I_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_50:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_PAL_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_NTSC_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_480I:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_480P:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_720P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_30:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080I_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_1080P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_NTSC_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_DACOUT_AUTO:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_CMO_CMO260J2_WUXGA:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_640x480P_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_VGAOUT_60_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_60_MIN:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_480X272_60:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_TTLOUT_60_MAX:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->E_DISPLAY_RES_MAX_NUM:Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    sput v4, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->value:I

    return v0
.end method
