.class public final Lcom/mstar/android/tvapi/dtv/common/DtvManager;
.super Lcom/mstar/android/tvapi/common/TvManager;
.source "DtvManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;-><init>()V

    return-void
.end method

.method public static getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;
    .locals 3

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "-----AtscPlayer------"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/DtvPlayerImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvPlayerImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvPlayerImplProxy;->getPlayerImplInstance()Lcom/mstar/android/tvapi/impl/PlayerImpl;

    move-result-object v1

    return-object v1
.end method

.method public static getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    return-object v0
.end method

.method public static getDtvScanManager()Lcom/mstar/android/tvapi/dtv/common/DtvScanManager;
    .locals 2

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;->getScanImplInstance()Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    move-result-object v1

    return-object v1
.end method

.method public static getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;
    .locals 3

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "-----DvbPlayer------"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/DtvPlayerImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvPlayerImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvPlayerImplProxy;->getPlayerImplInstance()Lcom/mstar/android/tvapi/impl/PlayerImpl;

    move-result-object v1

    return-object v1
.end method

.method public static getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;
    .locals 2

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;->getScanImplInstance()Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    move-result-object v1

    return-object v1
.end method

.method public static getDvbtScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DvbtScanManager;
    .locals 2

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/DtvScanImplProxy;->getScanImplInstance()Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    move-result-object v1

    return-object v1
.end method

.method public static getEpgManager()Lcom/mstar/android/tvapi/dtv/common/EpgManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    move-result-object v0

    return-object v0
.end method

.method public static getOadManager()Lcom/mstar/android/tvapi/dtv/common/OadManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/OadManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/OadManager;

    move-result-object v0

    return-object v0
.end method

.method public static getSubtitleManager()Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    move-result-object v0

    return-object v0
.end method
