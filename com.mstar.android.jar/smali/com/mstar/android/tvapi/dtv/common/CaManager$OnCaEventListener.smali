.class public interface abstract Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
.super Ljava/lang/Object;
.source "CaManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/common/CaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnCaEventListener"
.end annotation


# virtual methods
.method public abstract onActionRequest(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onDetitleReceived(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onEmailNotifyIcon(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onEntitleChanged(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onHideIPPVDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onHideOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaLockService;)Z
.end method

.method public abstract onOtaState(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onRequestFeeding(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onShowBuyMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onShowFingerMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onShowOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILjava/lang/String;)Z
.end method

.method public abstract onShowProgressStrip(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method

.method public abstract onStartIppvBuyDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;)Z
.end method

.method public abstract onUNLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
.end method
