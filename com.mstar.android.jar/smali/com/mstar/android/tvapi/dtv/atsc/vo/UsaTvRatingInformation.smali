.class public Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;
.super Ljava/lang/Object;
.source "UsaTvRatingInformation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bTV_14_ALL_Lock:Z

.field public bTV_14_D_Lock:Z

.field public bTV_14_L_Lock:Z

.field public bTV_14_S_Lock:Z

.field public bTV_14_V_Lock:Z

.field public bTV_G_ALL_Lock:Z

.field public bTV_G_D_Lock:Z

.field public bTV_G_L_Lock:Z

.field public bTV_G_S_Lock:Z

.field public bTV_G_V_Lock:Z

.field public bTV_MA_ALL_Lock:Z

.field public bTV_MA_D_Lock:Z

.field public bTV_MA_L_Lock:Z

.field public bTV_MA_S_Lock:Z

.field public bTV_MA_V_Lock:Z

.field public bTV_PG_ALL_Lock:Z

.field public bTV_PG_D_Lock:Z

.field public bTV_PG_L_Lock:Z

.field public bTV_PG_S_Lock:Z

.field public bTV_PG_V_Lock:Z

.field public bTV_Y7_ALL_Lock:Z

.field public bTV_Y7_FV_Lock:Z

.field public bTV_Y_ALL_Lock:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_Y_ALL_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_Y7_ALL_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_Y7_FV_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_ALL_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_V_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_S_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_L_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_D_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_ALL_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_V_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_S_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_L_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_D_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_ALL_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_V_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_f

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_S_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_10

    move v0, v1

    :goto_10
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_L_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_11

    move v0, v1

    :goto_11
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_D_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_12

    move v0, v1

    :goto_12
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_ALL_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_13

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_V_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_14

    move v0, v1

    :goto_14
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_S_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_15

    move v0, v1

    :goto_15
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_L_Lock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_16

    :goto_16
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_D_Lock:Z

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto/16 :goto_4

    :cond_5
    move v0, v2

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto/16 :goto_6

    :cond_7
    move v0, v2

    goto/16 :goto_7

    :cond_8
    move v0, v2

    goto/16 :goto_8

    :cond_9
    move v0, v2

    goto/16 :goto_9

    :cond_a
    move v0, v2

    goto/16 :goto_a

    :cond_b
    move v0, v2

    goto/16 :goto_b

    :cond_c
    move v0, v2

    goto/16 :goto_c

    :cond_d
    move v0, v2

    goto :goto_d

    :cond_e
    move v0, v2

    goto :goto_e

    :cond_f
    move v0, v2

    goto :goto_f

    :cond_10
    move v0, v2

    goto :goto_10

    :cond_11
    move v0, v2

    goto :goto_11

    :cond_12
    move v0, v2

    goto :goto_12

    :cond_13
    move v0, v2

    goto :goto_13

    :cond_14
    move v0, v2

    goto :goto_14

    :cond_15
    move v0, v2

    goto :goto_15

    :cond_16
    move v1, v2

    goto :goto_16
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_Y_ALL_Lock:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_Y7_ALL_Lock:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_Y7_FV_Lock:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_ALL_Lock:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_V_Lock:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_S_Lock:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_L_Lock:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_G_D_Lock:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_ALL_Lock:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_V_Lock:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_S_Lock:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_L_Lock:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_PG_D_Lock:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_ALL_Lock:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_V_Lock:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_S_Lock:Z

    if-eqz v0, :cond_f

    move v0, v1

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_L_Lock:Z

    if-eqz v0, :cond_10

    move v0, v1

    :goto_10
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_14_D_Lock:Z

    if-eqz v0, :cond_11

    move v0, v1

    :goto_11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_ALL_Lock:Z

    if-eqz v0, :cond_12

    move v0, v1

    :goto_12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_V_Lock:Z

    if-eqz v0, :cond_13

    move v0, v1

    :goto_13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_S_Lock:Z

    if-eqz v0, :cond_14

    move v0, v1

    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_L_Lock:Z

    if-eqz v0, :cond_15

    move v0, v1

    :goto_15
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;->bTV_MA_D_Lock:Z

    if-eqz v0, :cond_16

    :goto_16
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto/16 :goto_4

    :cond_5
    move v0, v2

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto/16 :goto_6

    :cond_7
    move v0, v2

    goto/16 :goto_7

    :cond_8
    move v0, v2

    goto/16 :goto_8

    :cond_9
    move v0, v2

    goto/16 :goto_9

    :cond_a
    move v0, v2

    goto/16 :goto_a

    :cond_b
    move v0, v2

    goto :goto_b

    :cond_c
    move v0, v2

    goto :goto_c

    :cond_d
    move v0, v2

    goto :goto_d

    :cond_e
    move v0, v2

    goto :goto_e

    :cond_f
    move v0, v2

    goto :goto_f

    :cond_10
    move v0, v2

    goto :goto_10

    :cond_11
    move v0, v2

    goto :goto_11

    :cond_12
    move v0, v2

    goto :goto_12

    :cond_13
    move v0, v2

    goto :goto_13

    :cond_14
    move v0, v2

    goto :goto_14

    :cond_15
    move v0, v2

    goto :goto_15

    :cond_16
    move v1, v2

    goto :goto_16
.end method
