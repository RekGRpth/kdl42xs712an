.class public Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;
.super Ljava/lang/Object;
.source "AtscProgramInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;,
        Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;,
        Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_AUD_LANG_NUM:I = 0x10

.field public static final MAX_SERVICE_NAME:I = 0x8


# instance fields
.field audInfo:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

.field public audLangNum:S

.field chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

.field public id:I

.field public muxTableId:I

.field public pcrPid:I

.field public pmtPID:I

.field public programNumber:I

.field psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

.field public serviceName:Ljava/lang/String;

.field public sourceId:I

.field public videoPID:I

.field virtualChNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/16 v4, 0x10

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->id:I

    iput v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->muxTableId:I

    new-instance v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    invoke-direct {v1, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    new-instance v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-direct {v1, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    const-string v1, ""

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->serviceName:Ljava/lang/String;

    iput v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->pcrPid:I

    iput v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->videoPID:I

    iput v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->pmtPID:I

    new-instance v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    invoke-direct {v1, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->virtualChNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    iput-short v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->audLangNum:S

    new-array v1, v4, [Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->audInfo:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->audInfo:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->programNumber:I

    iput v3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->sourceId:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->id:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->muxTableId:I

    new-instance v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    invoke-direct {v1, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->mgtVer:S

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->patVer:S

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->pmtVer:S

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->rrtVer:S

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->vctVer:S

    new-instance v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-direct {v1, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->favorite:B

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isHide:B

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isLock:B

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isRenamed:B

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isScramble:B

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isSkipped:B

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->scrambleChStatus:B

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->serviceType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->pcrPid:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->videoPID:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->pmtPID:I

    new-instance v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    invoke-direct {v1, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->virtualChNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->virtualChNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;->majorNumber:I

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->virtualChNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;->minorNumber:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->audLangNum:S

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->audInfo:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->programNumber:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->sourceId:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->id:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->muxTableId:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    iget-short v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->mgtVer:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    iget-short v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->patVer:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    iget-short v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->pmtVer:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    iget-short v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->rrtVer:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->psipVersionNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;

    iget-short v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscTableVersion;->vctVer:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-byte v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->favorite:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-byte v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isHide:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-byte v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isLock:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-byte v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isRenamed:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-byte v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isScramble:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-byte v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isSkipped:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-byte v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->scrambleChStatus:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->chAttribute:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;

    iget-short v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->serviceType:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->pcrPid:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->videoPID:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->pmtPID:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->virtualChNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    iget v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;->majorNumber:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->virtualChNum:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;

    iget v1, v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscVirtualChannelNumber;->minorNumber:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->audLangNum:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->audInfo:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->programNumber:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->sourceId:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
