.class public final enum Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;
.super Ljava/lang/Enum;
.source "AtscEpgEventInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumAtscEpgFunctionStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_DB_NO_CHANNEL_DB:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_DB_NO_CONNECT:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_DB_NO_LOCK:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_INVALID:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_NO_CHANNEL:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_NO_EVENT:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_NO_FUNCTION:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_NO_STRING:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_SUCCESS:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public static final enum EN_ATSC_EPG_FUNC_STATUS_UNDEFINED:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_SUCCESS:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_INVALID"

    invoke-direct {v0, v1, v5, v5}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_INVALID:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_NO_EVENT"

    invoke-direct {v0, v1, v6, v6}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_EVENT:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_NO_STRING"

    invoke-direct {v0, v1, v7, v7}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_STRING:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_NO_CHANNEL"

    invoke-direct {v0, v1, v8, v8}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_CHANNEL:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_DB_NO_CONNECT"

    const/4 v2, 0x5

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_DB_NO_CONNECT:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_DB_NO_LOCK"

    const/4 v2, 0x6

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_DB_NO_LOCK:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_DB_NO_CHANNEL_DB"

    const/4 v2, 0x7

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_DB_NO_CHANNEL_DB:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_NO_FUNCTION"

    const/16 v2, 0x8

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_FUNCTION:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v1, "EN_ATSC_EPG_FUNC_STATUS_UNDEFINED"

    const/16 v2, 0x9

    const/16 v3, 0x100

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_UNDEFINED:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_SUCCESS:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_INVALID:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_EVENT:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_STRING:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_CHANNEL:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_DB_NO_CONNECT:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_DB_NO_LOCK:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_DB_NO_CHANNEL_DB:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_NO_FUNCTION:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_UNDEFINED:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->$VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    sput v4, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    # getter for: Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    const/16 v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->$VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->value:I

    return v0
.end method
