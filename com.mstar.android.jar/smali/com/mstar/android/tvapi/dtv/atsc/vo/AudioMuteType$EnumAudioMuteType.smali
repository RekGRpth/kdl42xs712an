.class public final enum Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;
.super Ljava/lang/Enum;
.source "AudioMuteType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumAudioMuteType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_ALL_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_ALL_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYBLOCK_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYBLOCK_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYSYNC_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYSYNC_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYUSER_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYUSER_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYVCHIP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_BYVCHIP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_CI_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_CI_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_DATA_IN_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_DATA_IN_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_DURING_LIMITED_TIME_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_DURING_LIMITED_TIME_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_INTERNAL_1_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_INTERNAL_1_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_INTERNAL_3_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_INTERNAL_3_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_INTERNAL_4_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_INTERNAL_4_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_MHEGAP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_MHEGAP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_MOMENT_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_MOMENT_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_PERMANENT_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_PERMANENT_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_POWERON_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_POWERON_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_SCAN_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_SCAN_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_SIGNAL_UNSTABLE_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_SIGNAL_UNSTABLE_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_SOURCESWITCH_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_SOURCESWITCH_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_HP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_HP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SCART1_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SCART1_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SCART2_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SCART2_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SPDIF_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SPDIF_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SPEAKER_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field public static final enum E_AUDIO_USER_SPEAKER_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/16 v8, 0x20

    const/16 v7, 0x11

    const/16 v6, 0x10

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_PERMANENT_MUTEOFF_"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_PERMANENT_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_PERMANENT_MUTEON_"

    invoke-direct {v0, v1, v5, v5}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_PERMANENT_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_MOMENT_MUTEOFF_"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MOMENT_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_MOMENT_MUTEON_"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MOMENT_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYUSER_MUTEOFF_"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v8}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYUSER_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYUSER_MUTEON_"

    const/4 v2, 0x5

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYUSER_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYSYNC_MUTEOFF_"

    const/4 v2, 0x6

    const/16 v3, 0x30

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYSYNC_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYSYNC_MUTEON_"

    const/4 v2, 0x7

    const/16 v3, 0x31

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYSYNC_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYVCHIP_MUTEOFF_"

    const/16 v2, 0x8

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYVCHIP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYVCHIP_MUTEON_"

    const/16 v2, 0x9

    const/16 v3, 0x41

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYVCHIP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYBLOCK_MUTEOFF_"

    const/16 v2, 0xa

    const/16 v3, 0x50

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYBLOCK_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_BYBLOCK_MUTEON_"

    const/16 v2, 0xb

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYBLOCK_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_INTERNAL_1_MUTEOFF_"

    const/16 v2, 0xc

    const/16 v3, 0x60

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_1_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_INTERNAL_1_MUTEON_"

    const/16 v2, 0xd

    const/16 v3, 0x61

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_1_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_SIGNAL_UNSTABLE_MUTEOFF_"

    const/16 v2, 0xe

    const/16 v3, 0x70

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SIGNAL_UNSTABLE_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_SIGNAL_UNSTABLE_MUTEON_"

    const/16 v2, 0xf

    const/16 v3, 0x71

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SIGNAL_UNSTABLE_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_INTERNAL_3_MUTEOFF_"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v6, v2}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_3_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_INTERNAL_3_MUTEON_"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v7, v2}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_3_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_INTERNAL_4_MUTEOFF_"

    const/16 v2, 0x12

    const/16 v3, 0x82

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_4_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_INTERNAL_4_MUTEON_"

    const/16 v2, 0x13

    const/16 v3, 0x83

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_4_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_DURING_LIMITED_TIME_MUTEOFF_"

    const/16 v2, 0x14

    const/16 v3, 0x90

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DURING_LIMITED_TIME_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_DURING_LIMITED_TIME_MUTEON_"

    const/16 v2, 0x15

    const/16 v3, 0x91

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DURING_LIMITED_TIME_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_MHEGAP_MUTEOFF_"

    const/16 v2, 0x16

    const/16 v3, 0xa0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MHEGAP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_MHEGAP_MUTEON_"

    const/16 v2, 0x17

    const/16 v3, 0xa1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MHEGAP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_CI_MUTEOFF_"

    const/16 v2, 0x18

    const/16 v3, 0xb0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_CI_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_CI_MUTEON_"

    const/16 v2, 0x19

    const/16 v3, 0xb1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_CI_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_SCAN_MUTEOFF_"

    const/16 v2, 0x1a

    const/16 v3, 0xc0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SCAN_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_SCAN_MUTEON_"

    const/16 v2, 0x1b

    const/16 v3, 0xc1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SCAN_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_SOURCESWITCH_MUTEOFF_"

    const/16 v2, 0x1c

    const/16 v3, 0xd0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SOURCESWITCH_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_SOURCESWITCH_MUTEON_"

    const/16 v2, 0x1d

    const/16 v3, 0xd1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SOURCESWITCH_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SPEAKER_MUTEOFF_"

    const/16 v2, 0x1e

    const/16 v3, 0xe0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPEAKER_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SPEAKER_MUTEON_"

    const/16 v2, 0x1f

    const/16 v3, 0xe1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPEAKER_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_HP_MUTEOFF_"

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v8, v2}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_HP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_HP_MUTEON_"

    const/16 v2, 0x21

    const/16 v3, 0xf1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_HP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SPDIF_MUTEOFF_"

    const/16 v2, 0x22

    const/16 v3, 0x100

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPDIF_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SPDIF_MUTEON_"

    const/16 v2, 0x23

    const/16 v3, 0x101

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPDIF_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SCART1_MUTEOFF_"

    const/16 v2, 0x24

    const/16 v3, 0x110

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART1_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SCART1_MUTEON_"

    const/16 v2, 0x25

    const/16 v3, 0x111

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART1_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SCART2_MUTEOFF_"

    const/16 v2, 0x26

    const/16 v3, 0x120

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART2_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_USER_SCART2_MUTEON_"

    const/16 v2, 0x27

    const/16 v3, 0x121

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART2_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_ALL_MUTEOFF_"

    const/16 v2, 0x28

    const/16 v3, 0x130

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_ALL_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_ALL_MUTEON_"

    const/16 v2, 0x29

    const/16 v3, 0x131

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_ALL_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_DATA_IN_MUTEOFF_"

    const/16 v2, 0x2a

    const/16 v3, 0x140

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DATA_IN_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_DATA_IN_MUTEON_"

    const/16 v2, 0x2b

    const/16 v3, 0x141

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DATA_IN_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_POWERON_MUTEOFF_"

    const/16 v2, 0x2c

    const/16 v3, 0x150

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_POWERON_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const-string v1, "E_AUDIO_POWERON_MUTEON_"

    const/16 v2, 0x2d

    const/16 v3, 0x151

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_POWERON_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    const/16 v0, 0x2e

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_PERMANENT_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_PERMANENT_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v1, v0, v5

    const/4 v1, 0x2

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MOMENT_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MOMENT_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYUSER_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYUSER_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYSYNC_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYSYNC_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYVCHIP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYVCHIP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYBLOCK_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_BYBLOCK_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_1_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_1_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SIGNAL_UNSTABLE_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SIGNAL_UNSTABLE_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_3_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_3_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v1, v0, v7

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_4_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_INTERNAL_4_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DURING_LIMITED_TIME_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DURING_LIMITED_TIME_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MHEGAP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_MHEGAP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_CI_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_CI_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SCAN_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SCAN_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SOURCESWITCH_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_SOURCESWITCH_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPEAKER_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPEAKER_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_HP_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v1, v0, v8

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_HP_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPDIF_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SPDIF_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART1_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART1_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART2_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_USER_SCART2_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_ALL_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_ALL_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DATA_IN_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_DATA_IN_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_POWERON_MUTEOFF_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->E_AUDIO_POWERON_MUTEON_:Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    sput v4, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType;->htAudioMuteType:Ljava/util/Hashtable;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType;->htAudioMuteType:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->value:I

    return v0
.end method
