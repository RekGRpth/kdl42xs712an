.class public final enum Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;
.super Ljava/lang/Enum;
.source "DtvType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/vo/DtvType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumEpgMainGenreType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum ARTS:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum CHILDREN:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum EDUCATION:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum INVALID:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum LEISURE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum MAX:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum MIN:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum MOVIE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum MUSIC:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum NEWS:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum RESERVED1:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum RESERVED2:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum RESERVED3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum SHOW:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum SIZE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum SOCIAL:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum SPECIAL:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum SPORT:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum UNCLASSIFIED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field public static final enum USER_DEFINED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "UNCLASSIFIED"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->UNCLASSIFIED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "MOVIE"

    invoke-direct {v0, v1, v5, v5}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MOVIE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "NEWS"

    invoke-direct {v0, v1, v6, v6}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->NEWS:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v7, v7}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SHOW:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "SPORT"

    invoke-direct {v0, v1, v8, v8}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SPORT:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "CHILDREN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->CHILDREN:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "MUSIC"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MUSIC:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "ARTS"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->ARTS:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "SOCIAL"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SOCIAL:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "EDUCATION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->EDUCATION:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "LEISURE"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->LEISURE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "SPECIAL"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SPECIAL:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "RESERVED1"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->RESERVED1:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "RESERVED2"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->RESERVED2:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "RESERVED3"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->RESERVED3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "USER_DEFINED"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->USER_DEFINED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "MIN"

    const/16 v2, 0x10

    sget-object v3, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->UNCLASSIFIED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MIN:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "MAX"

    const/16 v2, 0x11

    sget-object v3, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->USER_DEFINED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MAX:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "SIZE"

    const/16 v2, 0x12

    sget-object v3, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MAX:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SIZE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const-string v1, "INVALID"

    const/16 v2, 0x13

    sget-object v3, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MAX:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->INVALID:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->UNCLASSIFIED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MOVIE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->NEWS:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SHOW:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SPORT:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->CHILDREN:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MUSIC:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->ARTS:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SOCIAL:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->EDUCATION:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->LEISURE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SPECIAL:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->RESERVED1:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->RESERVED2:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->RESERVED3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->USER_DEFINED:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MIN:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->MAX:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->SIZE:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->INVALID:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    sput v4, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/dtv/vo/DtvType;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/DtvType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/dtv/vo/DtvType;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/DtvType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->value:I

    return v0
.end method
