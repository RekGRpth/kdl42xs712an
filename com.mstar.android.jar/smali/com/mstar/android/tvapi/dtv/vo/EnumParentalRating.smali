.class public final enum Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;
.super Ljava/lang/Enum;
.source "EnumParentalRating.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_0:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_1:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_10:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_11:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_12:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_13:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_14:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_15:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_16:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_17:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_18:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_2:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_3:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_4:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_5:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_6:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_7:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_8:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

.field public static final enum E_9:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_0"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_0:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_1"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_1:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_2"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_2:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_3"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_3:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_4"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_4:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_5"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_5:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_6"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_6:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_7"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_7:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_8"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_8:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_9"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_9:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_10"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_10:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_11"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_11:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_12"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_12:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_13"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_13:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_14"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_14:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_15"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_15:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_16"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_16:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_17"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_17:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const-string v1, "E_18"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_18:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_0:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_1:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_2:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_3:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_4:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_5:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_6:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_7:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_8:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_9:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_10:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_11:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_12:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_13:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_14:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_15:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_16:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_17:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->E_18:Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;

    return-object v0
.end method
