.class public Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;
.super Ljava/lang/Object;
.source "CaEmailHeadsInfo.java"


# instance fields
.field public EmailHeads:[Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

.field public sCount:S

.field public sEmailHeadsState:S

.field public sFromIndex:S


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sEmailHeadsState:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sCount:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sFromIndex:S

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->EmailHeads:[Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->EmailHeads:[Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getEmailHeads()[Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->EmailHeads:[Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

    return-object v0
.end method

.method public getsCount()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sCount:S

    return v0
.end method

.method public getsEmailHeadsState()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sEmailHeadsState:S

    return v0
.end method

.method public getsFromIndex()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sFromIndex:S

    return v0
.end method

.method public setEmailHeads([Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;)V
    .locals 0
    .param p1    # [Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->EmailHeads:[Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

    return-void
.end method

.method public setsCount(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sCount:S

    return-void
.end method

.method public setsEmailHeadsState(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sEmailHeadsState:S

    return-void
.end method

.method public setsFromIndex(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;->sFromIndex:S

    return-void
.end method
