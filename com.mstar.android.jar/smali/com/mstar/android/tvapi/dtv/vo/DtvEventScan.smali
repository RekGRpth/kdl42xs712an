.class public Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;
.super Ljava/lang/Object;
.source "DtvEventScan.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public currFrequency:I

.field public currRFCh:S

.field public dataSrvCount:S

.field public dtvSrvCount:S

.field public radioSrvCount:S

.field public scanPercentageNum:S

.field public scanStatus:I

.field public signalQuality:S

.field public signalStrength:S

.field public userData:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanStatus:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->currRFCh:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanPercentageNum:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dtvSrvCount:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->radioSrvCount:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dataSrvCount:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalQuality:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalStrength:S

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->currFrequency:I

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->userData:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanStatus:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->currRFCh:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanPercentageNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dtvSrvCount:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->radioSrvCount:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dataSrvCount:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalQuality:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalStrength:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->currFrequency:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->userData:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->currRFCh:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanPercentageNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dtvSrvCount:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->radioSrvCount:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dataSrvCount:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalQuality:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalStrength:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->currFrequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->userData:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
