.class public Lcom/mstar/android/tvapi/dtv/vo/CaLockService;
.super Ljava/lang/Object;
.source "CaLockService.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/CaLockService;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public m_CompArr:[Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

.field public m_ComponentNum:S

.field public m_Modulation:S

.field public m_dwFrequency:I

.field public m_fec_inner:S

.field public m_fec_outer:S

.field public m_symbol_rate:I

.field public m_wPcrPid:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/CaLockService$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x5

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_CompArr:[Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    iput v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_dwFrequency:I

    iput v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_symbol_rate:I

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_wPcrPid:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_Modulation:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_ComponentNum:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_CompArr:[Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/CaComponent;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    const/4 v3, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_CompArr:[Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_dwFrequency:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_symbol_rate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_wPcrPid:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_Modulation:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_ComponentNum:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_CompArr:[Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/CaComponent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/vo/CaLockService$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/CaLockService$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_dwFrequency:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_symbol_rate:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_wPcrPid:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_Modulation:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_ComponentNum:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;->m_CompArr:[Lcom/mstar/android/tvapi/dtv/vo/CaComponent;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/CaComponent;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
