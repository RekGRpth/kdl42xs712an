.class public Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;
.super Ljava/lang/Object;
.source "MenuSubtitleService.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_STRINGCODE_COUNT:I = 0x4


# instance fields
.field public eLanguage:I

.field public enSubtitleType:I

.field public refCount:S

.field public stringCodes:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->stringCodes:[C

    iput v2, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->eLanguage:I

    iput v2, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->enSubtitleType:I

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->refCount:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->stringCodes:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(IIS[C)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # S
    .param p4    # [C

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->stringCodes:[C

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->eLanguage:I

    iput p2, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->enSubtitleType:I

    const/4 v0, 0x0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->refCount:S

    iput-object p4, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->stringCodes:[C

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->stringCodes:[C

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->eLanguage:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->enSubtitleType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->refCount:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->stringCodes:[C

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getLanguage()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->eLanguage:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->eLanguage:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->eLanguage:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->enSubtitleType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->refCount:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->stringCodes:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    return-void
.end method
