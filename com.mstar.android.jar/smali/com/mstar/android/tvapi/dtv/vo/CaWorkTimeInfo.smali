.class public Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;
.super Ljava/lang/Object;
.source "CaWorkTimeInfo.java"


# instance fields
.field public sStartHour:S

.field public sWorkTimeState:S

.field public syEndHour:S

.field public syEndMin:S

.field public syEndSec:S

.field public syStartMin:S

.field public syStartSec:S


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;->sWorkTimeState:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;->sStartHour:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;->syStartMin:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;->syStartSec:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;->syEndHour:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;->syEndMin:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;->syEndSec:S

    return-void
.end method
