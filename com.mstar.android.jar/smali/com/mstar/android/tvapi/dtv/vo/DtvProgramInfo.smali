.class public Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;
.super Ljava/lang/Object;
.source "DtvProgramInfo.java"


# instance fields
.field private antennaType:S

.field private audioPid:I

.field private favorite:S

.field private frequency:I

.field private isDelete:Z

.field private isHide:Z

.field private isLock:Z

.field private isOpService:Z

.field private isRename:Z

.field private isScramble:Z

.field private isSelectable:Z

.field private isSelectableOp:Z

.field private isSkip:Z

.field private isSpecialSrv:Z

.field private isVisible:Z

.field private isVisibleOp:Z

.field private majorNumber:I

.field private minorNumber:I

.field private number:I

.field private nvodRealSrvNumber:S

.field private nvodRealSrvs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/DtvTripleId;",
            ">;"
        }
    .end annotation
.end field

.field private originalNetworkId:I

.field private pcrPid:I

.field private pmtPid:I

.field private providerNameOp:Ljava/lang/String;

.field private screenMuteStatus:I

.field private serviceId:I

.field private serviceName:Ljava/lang/String;

.field private serviceNameOp:Ljava/lang/String;

.field private serviceType:S

.field private transportStreamId:I

.field private videoPid:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->number:I

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->favorite:S

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisible:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isLock:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSkip:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectable:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isScramble:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isDelete:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isHide:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isRename:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSpecialSrv:Z

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->frequency:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->transportStreamId:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceId:I

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceType:S

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceName:Ljava/lang/String;

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->screenMuteStatus:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pcrPid:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->videoPid:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->audioPid:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pmtPid:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->majorNumber:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->minorNumber:I

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->antennaType:S

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->originalNetworkId:I

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvNumber:S

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvs:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisibleOp:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectableOp:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isOpService:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceNameOp:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->providerNameOp:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ISZZZZZZZZZIIISLjava/lang/String;IIIIIIISISLjava/util/ArrayList;ZZZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # S
    .param p3    # Z
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z
    .param p8    # Z
    .param p9    # Z
    .param p10    # Z
    .param p11    # Z
    .param p12    # I
    .param p13    # I
    .param p14    # I
    .param p15    # S
    .param p16    # Ljava/lang/String;
    .param p17    # I
    .param p18    # I
    .param p19    # I
    .param p20    # I
    .param p21    # I
    .param p22    # I
    .param p23    # I
    .param p24    # S
    .param p25    # I
    .param p26    # S
    .param p28    # Z
    .param p29    # Z
    .param p30    # Z
    .param p31    # Ljava/lang/String;
    .param p32    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ISZZZZZZZZZIIIS",
            "Ljava/lang/String;",
            "IIIIIIISIS",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/DtvTripleId;",
            ">;ZZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->number:I

    iput-short p2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->favorite:S

    iput-boolean p3, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisible:Z

    iput-boolean p4, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isLock:Z

    iput-boolean p5, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSkip:Z

    iput-boolean p6, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectable:Z

    iput-boolean p7, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isScramble:Z

    iput-boolean p8, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isDelete:Z

    iput-boolean p9, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isHide:Z

    iput-boolean p10, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isRename:Z

    iput-boolean p11, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSpecialSrv:Z

    iput p12, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->frequency:I

    iput p13, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->transportStreamId:I

    iput p14, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceId:I

    move/from16 v0, p15

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceType:S

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceName:Ljava/lang/String;

    move/from16 v0, p17

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->screenMuteStatus:I

    move/from16 v0, p18

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pcrPid:I

    move/from16 v0, p19

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->videoPid:I

    move/from16 v0, p20

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->audioPid:I

    move/from16 v0, p21

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pmtPid:I

    move/from16 v0, p22

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->majorNumber:I

    move/from16 v0, p23

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->minorNumber:I

    move/from16 v0, p24

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->antennaType:S

    move/from16 v0, p25

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->originalNetworkId:I

    move/from16 v0, p26

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvNumber:S

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvs:Ljava/util/ArrayList;

    move/from16 v0, p28

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisibleOp:Z

    move/from16 v0, p29

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectableOp:Z

    move/from16 v0, p30

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isOpService:Z

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceNameOp:Ljava/lang/String;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->providerNameOp:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAntennaType()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->antennaType:S

    return v0
.end method

.method public getAudioPid()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->audioPid:I

    return v0
.end method

.method public getFavorite()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->favorite:S

    return v0
.end method

.method public getFrequency()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->frequency:I

    return v0
.end method

.method public getMajorNumber()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->majorNumber:I

    return v0
.end method

.method public getMinorNumber()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->minorNumber:I

    return v0
.end method

.method public getNumber()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->number:I

    return v0
.end method

.method public getNvodRealSrvNumber()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvNumber:S

    return v0
.end method

.method public getNvodRealSrvs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/DtvTripleId;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOriginalNetworkId()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->originalNetworkId:I

    return v0
.end method

.method public getPcrPid()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pcrPid:I

    return v0
.end method

.method public getPmtPid()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pmtPid:I

    return v0
.end method

.method public getProviderNameOp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->providerNameOp:Ljava/lang/String;

    return-object v0
.end method

.method public getScreenMuteStatus()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->screenMuteStatus:I

    return v0
.end method

.method public getServiceId()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceId:I

    return v0
.end method

.method public getServiceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceNameOp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceNameOp:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceType()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceType:S

    return v0
.end method

.method public getTransportStreamId()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->transportStreamId:I

    return v0
.end method

.method public getVideoPid()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->videoPid:I

    return v0
.end method

.method public isDelete()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isDelete:Z

    return v0
.end method

.method public isHide()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isHide:Z

    return v0
.end method

.method public isLock()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isLock:Z

    return v0
.end method

.method public isOpService()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isOpService:Z

    return v0
.end method

.method public isRename()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isRename:Z

    return v0
.end method

.method public isScramble()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isScramble:Z

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectable:Z

    return v0
.end method

.method public isSelectableOp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectableOp:Z

    return v0
.end method

.method public isSkip()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSkip:Z

    return v0
.end method

.method public isSpecialSrv()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSpecialSrv:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisible:Z

    return v0
.end method

.method public isVisibleOp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisibleOp:Z

    return v0
.end method

.method public setAntennaType(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->antennaType:S

    return-void
.end method

.method public setAudioPid(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->audioPid:I

    return-void
.end method

.method public setDelete(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isDelete:Z

    return-void
.end method

.method public setFavorite(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->favorite:S

    return-void
.end method

.method public setFrequency(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->frequency:I

    return-void
.end method

.method public setHide(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isHide:Z

    return-void
.end method

.method public setLock(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isLock:Z

    return-void
.end method

.method public setMajorNumber(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->majorNumber:I

    return-void
.end method

.method public setMinorNumber(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->minorNumber:I

    return-void
.end method

.method public setNumber(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->number:I

    return-void
.end method

.method public setNvodRealSrvNumber(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvNumber:S

    return-void
.end method

.method public setNvodRealSrvs(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/DtvTripleId;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->nvodRealSrvs:Ljava/util/ArrayList;

    return-void
.end method

.method public setOpService(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isOpService:Z

    return-void
.end method

.method public setOriginalNetworkId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->originalNetworkId:I

    return-void
.end method

.method public setPcrPid(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pcrPid:I

    return-void
.end method

.method public setPmtPid(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->pmtPid:I

    return-void
.end method

.method public setProviderNameOp(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->providerNameOp:Ljava/lang/String;

    return-void
.end method

.method public setRename(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isRename:Z

    return-void
.end method

.method public setScramble(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isScramble:Z

    return-void
.end method

.method public setScreenMuteStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->screenMuteStatus:I

    return-void
.end method

.method public setSelectable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectable:Z

    return-void
.end method

.method public setSelectableOp(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSelectableOp:Z

    return-void
.end method

.method public setServiceId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceId:I

    return-void
.end method

.method public setServiceName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceName:Ljava/lang/String;

    return-void
.end method

.method public setServiceNameOp(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceNameOp:Ljava/lang/String;

    return-void
.end method

.method public setServiceType(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->serviceType:S

    return-void
.end method

.method public setSkip(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSkip:Z

    return-void
.end method

.method public setSpecialSrv(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isSpecialSrv:Z

    return-void
.end method

.method public setTransportStreamId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->transportStreamId:I

    return-void
.end method

.method public setVideoPid(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->videoPid:I

    return-void
.end method

.method public setVisible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisible:Z

    return-void
.end method

.method public setVisibleOp(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvProgramInfo;->isVisibleOp:Z

    return-void
.end method
