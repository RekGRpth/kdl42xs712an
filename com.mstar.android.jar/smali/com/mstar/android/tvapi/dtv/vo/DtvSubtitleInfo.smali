.class public Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;
.super Ljava/lang/Object;
.source "DtvSubtitleInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_MENUSUBTITLESERVICE_COUNT:I = 0x18


# instance fields
.field public currentSubtitleIndex:S

.field public subtitleOn:Z

.field public subtitleServiceNumber:S

.field public subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x18

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleOn:Z

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1    # Landroid/os/Parcel;

    const/16 v4, 0x18

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v4, [Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    aput-object v1, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleOn:Z

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>([Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;SSZ)V
    .locals 1
    .param p1    # [Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;
    .param p2    # S
    .param p3    # S
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x18

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    iput-short p2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    iput-short p3, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    iput-boolean p4, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleOn:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleOn:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
