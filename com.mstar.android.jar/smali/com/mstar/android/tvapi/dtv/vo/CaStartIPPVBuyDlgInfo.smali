.class public Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;
.super Ljava/lang/Object;
.source "CaStartIPPVBuyDlgInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = -0x85f9969cb1398L


# instance fields
.field public dwProductID:I

.field public m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

.field public wEcmPid:S

.field public wExpiredDate:S

.field public wTvsID:S

.field public wyMessageType:S

.field public wyPriceNum:S

.field public wySlotID:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    iput v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->dwProductID:I

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wySlotID:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wyPriceNum:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wExpiredDate:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    const/4 v3, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->dwProductID:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wySlotID:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wyPriceNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wExpiredDate:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDwProductID()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->dwProductID:I

    return v0
.end method

.method public getM_Price()[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    return-object v0
.end method

.method public getWyMessageType()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wyMessageType:S

    return v0
.end method

.method public getWyPriceNum()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wyPriceNum:S

    return v0
.end method

.method public getWySlotID()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wySlotID:S

    return v0
.end method

.method public getwEcmPid()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wEcmPid:S

    return v0
.end method

.method public getwExpiredDate()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wExpiredDate:S

    return v0
.end method

.method public getwTvsID()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wTvsID:S

    return v0
.end method

.method public setDwProductID(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->dwProductID:I

    return-void
.end method

.method public setM_Price([Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;)V
    .locals 0
    .param p1    # [Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    return-void
.end method

.method public setWyMessageType(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wyMessageType:S

    return-void
.end method

.method public setWyPriceNum(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wyPriceNum:S

    return-void
.end method

.method public setWySlotID(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wySlotID:S

    return-void
.end method

.method public setwEcmPid(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wEcmPid:S

    return-void
.end method

.method public setwExpiredDate(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wExpiredDate:S

    return-void
.end method

.method public setwTvsID(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wTvsID:S

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->dwProductID:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wySlotID:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wyPriceNum:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->wExpiredDate:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;->m_Price:[Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/CaIPPVPrice;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
