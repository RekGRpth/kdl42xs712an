.class public Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;
.super Ljava/lang/Object;
.source "CaOperatorChildStatus.java"


# instance fields
.field public bIsCanFeed:Z

.field public pParentCardSN:Ljava/lang/String;

.field public sDelayTime:S

.field public sIsChild:S

.field public sOperatorChildState:S

.field public wLastFeedTime:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;->sOperatorChildState:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;->sIsChild:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;->sDelayTime:S

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;->wLastFeedTime:I

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;->pParentCardSN:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;->bIsCanFeed:Z

    return-void
.end method
