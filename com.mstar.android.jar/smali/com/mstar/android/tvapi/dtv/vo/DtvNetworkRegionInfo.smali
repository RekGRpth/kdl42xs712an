.class public Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;
.super Ljava/lang/Object;
.source "DtvNetworkRegionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public dtvNetworkRegionVOs:[Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

.field public networkRegionsNumber:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->networkRegionsNumber:S

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->dtvNetworkRegionVOs:[Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->dtvNetworkRegionVOs:[Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->networkRegionsNumber:S

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->dtvNetworkRegionVOs:[Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->dtvNetworkRegionVOs:[Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->networkRegionsNumber:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->dtvNetworkRegionVOs:[Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;->dtvNetworkRegionVOs:[Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
