.class public Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;
.super Ljava/lang/Object;
.source "CaEmailContentInfo.java"


# instance fields
.field public pcEmailContent:Ljava/lang/String;

.field public sEmailContentState:S


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;->sEmailContentState:S

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;->pcEmailContent:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPcEmailContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;->pcEmailContent:Ljava/lang/String;

    return-object v0
.end method

.method public getsEmailContentState()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;->sEmailContentState:S

    return v0
.end method

.method public setPcEmailContent(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;->pcEmailContent:Ljava/lang/String;

    return-void
.end method

.method public setsEmailContentState(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;->sEmailContentState:S

    return-void
.end method
