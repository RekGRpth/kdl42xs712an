.class public Lcom/mstar/android/tvapi/dtv/vo/LangIso639;
.super Ljava/lang/Object;
.source "LangIso639.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/LangIso639;",
            ">;"
        }
    .end annotation
.end field

.field private static final E_ISO_LANG_MAX_LENGTH:I = 0x3


# instance fields
.field public audioMode:S

.field public audioType:S

.field public isValid:Z

.field public isoLangInfo:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/LangIso639$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isoLangInfo:[C

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->audioMode:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->audioType:S

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isValid:Z

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isoLangInfo:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isoLangInfo:[C

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isoLangInfo:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readCharArray([C)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->audioMode:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->audioType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isValid:Z

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/vo/LangIso639$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/LangIso639$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isoLangInfo:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->audioMode:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->audioType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isValid:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
