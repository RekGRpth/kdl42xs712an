.class public Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;
.super Ljava/lang/Object;
.source "DtvAudioInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

.field public audioLangNum:S

.field public currentAudioIndex:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0x10

    new-array v1, v1, [Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->currentAudioIndex:S

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0x10

    new-array v1, v1, [Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->currentAudioIndex:S

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->currentAudioIndex:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
