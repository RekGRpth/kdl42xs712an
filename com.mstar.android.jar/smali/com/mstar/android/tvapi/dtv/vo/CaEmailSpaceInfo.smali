.class public Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;
.super Ljava/lang/Object;
.source "CaEmailSpaceInfo.java"


# instance fields
.field public sEmailNum:S

.field public sEmptyNum:S

.field public saEmailSpaceState:S


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->saEmailSpaceState:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->sEmailNum:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->sEmptyNum:S

    return-void
.end method


# virtual methods
.method public getSaEmailSpaceState()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->saEmailSpaceState:S

    return v0
.end method

.method public getsEmailNum()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->sEmailNum:S

    return v0
.end method

.method public getsEmptyNum()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->sEmptyNum:S

    return v0
.end method

.method public setSaEmailSpaceState(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->saEmailSpaceState:S

    return-void
.end method

.method public setsEmailNum(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->sEmailNum:S

    return-void
.end method

.method public setsEmptyNum(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;->sEmptyNum:S

    return-void
.end method
