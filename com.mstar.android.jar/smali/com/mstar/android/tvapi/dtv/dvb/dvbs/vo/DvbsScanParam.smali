.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;
.super Ljava/lang/Object;
.source "DvbsScanParam.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private networkSearch:I

.field private polarization:Z

.field private scanMode:S

.field private serviceType:S

.field private symbolRate:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->symbolRate:I

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->networkSearch:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->scanMode:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->serviceType:S

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->polarization:Z

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->symbolRate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->networkSearch:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->scanMode:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->serviceType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->polarization:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getNetworkSearch()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->networkSearch:I

    return v0
.end method

.method public getScanMode()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->scanMode:S

    return v0
.end method

.method public getServiceType()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->serviceType:S

    return v0
.end method

.method public getSymbolRate()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->symbolRate:I

    return v0
.end method

.method public isPolarization()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->polarization:Z

    return v0
.end method

.method public setNetworkSearch(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->networkSearch:I

    return-void
.end method

.method public setPolarization(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->polarization:Z

    return-void
.end method

.method public setScanMode(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->scanMode:S

    return-void
.end method

.method public setServiceType(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->serviceType:S

    return-void
.end method

.method public setSymbolRate(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->symbolRate:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->symbolRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->networkSearch:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->scanMode:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->serviceType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsScanParam;->polarization:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
