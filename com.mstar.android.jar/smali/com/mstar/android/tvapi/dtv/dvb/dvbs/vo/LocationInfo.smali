.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;
.super Ljava/lang/Object;
.source "LocationInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private latitude:S

.field private locationName:Ljava/lang/String;

.field private longitude:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->locationName:Ljava/lang/String;

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->longitude:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->latitude:S

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->locationName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->longitude:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->latitude:S

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getLatitude()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->latitude:S

    return v0
.end method

.method public getLocationName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->locationName:Ljava/lang/String;

    return-object v0
.end method

.method public getLongitude()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->longitude:S

    return v0
.end method

.method public setLatitude(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->latitude:S

    return-void
.end method

.method public setLocationName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->locationName:Ljava/lang/String;

    return-void
.end method

.method public setLongitude(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->longitude:S

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->locationName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->longitude:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/LocationInfo;->latitude:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
