.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;
.super Ljava/lang/Object;
.source "DtvDemodDvbcInfo.java"


# instance fields
.field public bSerialTS:Z

.field public bSpecInv:Z

.field public eLockStatus:B

.field public eQamMode:B

.field public u16Quality:S

.field public u16Strength:S

.field public u16SymbolRate:S

.field public u16SymbolRateHal:S

.field public u16Version:S

.field public u32ChkScanTimeStart:I

.field public u32FcFs:I

.field public u32IFFreq:I

.field public u32Intp:I

.field public u8Qam:B

.field public u8SarValue:B


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u16Version:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u16SymbolRate:S

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->eQamMode:B

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u32IFFreq:I

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->bSpecInv:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->bSerialTS:Z

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u8SarValue:B

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u32ChkScanTimeStart:I

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->eLockStatus:B

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u16Strength:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u16Quality:S

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u32Intp:I

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u32FcFs:I

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u8Qam:B

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;->u16SymbolRateHal:S

    return-void
.end method
