.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;
.super Ljava/lang/Object;
.source "SatelliteInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;,
        Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public angle:I

.field public channelId:S

.field public diseqcLevel:S

.field public e22KOnOff:S

.field eLNBType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

.field eLNBTypeReal:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

.field public frequency:I

.field public hiLOF:I

.field public lnbPwrOnOff:S

.field public lowLOF:I

.field public numberOfTp:I

.field public ov12VOnOff:S

.field public position:S

.field public satName:Ljava/lang/String;

.field public satelliteId:S

.field public swt10Port:S

.field public swt11Port:S

.field public toneburstType:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->satName:Ljava/lang/String;

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->lowLOF:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->hiLOF:I

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->E_LNB_5150:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->eLNBTypeReal:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->E_C_BAND:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->eLNBType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->diseqcLevel:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->toneburstType:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->swt10Port:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->swt11Port:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->e22KOnOff:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->lnbPwrOnOff:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->ov12VOnOff:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->position:S

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->angle:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->numberOfTp:I

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->satelliteId:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->channelId:S

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->frequency:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->satName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->lowLOF:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->hiLOF:I

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->eLNBTypeReal:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->eLNBType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->diseqcLevel:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->toneburstType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->swt10Port:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->swt11Port:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->e22KOnOff:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->lnbPwrOnOff:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->ov12VOnOff:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->position:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->angle:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->numberOfTp:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->satelliteId:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->channelId:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->frequency:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->satName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->lowLOF:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->hiLOF:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->eLNBTypeReal:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->eLNBType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->diseqcLevel:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->toneburstType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->swt10Port:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->swt11Port:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->e22KOnOff:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->lnbPwrOnOff:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->ov12VOnOff:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->position:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->angle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->numberOfTp:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->satelliteId:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->channelId:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;->frequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
