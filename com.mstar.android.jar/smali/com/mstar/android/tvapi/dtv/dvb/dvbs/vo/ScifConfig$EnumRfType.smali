.class public final enum Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;
.super Ljava/lang/Enum;
.source "ScifConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumRfType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

.field public static final enum E_NONE_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

.field public static final enum E_STANDARD_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

.field public static final enum E_WIDE_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    const-string v1, "E_NONE_RF"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->E_NONE_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    const-string v1, "E_STANDARD_RF"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->E_STANDARD_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    const-string v1, "E_WIDE_RF"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->E_WIDE_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->E_NONE_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->E_STANDARD_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->E_WIDE_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    return-object v0
.end method
