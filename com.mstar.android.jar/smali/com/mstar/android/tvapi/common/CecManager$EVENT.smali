.class public final enum Lcom/mstar/android/tvapi/common/CecManager$EVENT;
.super Ljava/lang/Enum;
.source "CecManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/CecManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/CecManager$EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/CecManager$EVENT;

.field public static final enum EV_CEC_IMAGE_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

.field public static final enum EV_CEC_TEXT_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

.field public static final enum EV_UNDEFINED:Lcom/mstar/android/tvapi/common/CecManager$EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    const-string v1, "EV_CEC_IMAGE_VIEW_ON"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/CecManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_CEC_IMAGE_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    const-string v1, "EV_CEC_TEXT_VIEW_ON"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/CecManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_CEC_TEXT_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    const-string v1, "EV_UNDEFINED"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/CecManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_UNDEFINED:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    sget-object v1, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_CEC_IMAGE_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_CEC_TEXT_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_UNDEFINED:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/CecManager$EVENT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/CecManager$EVENT;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/CecManager$EVENT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    return-object v0
.end method
