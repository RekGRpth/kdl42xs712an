.class public interface abstract Lcom/mstar/android/tvapi/common/TvPlayer;
.super Ljava/lang/Object;
.source "TvPlayer.java"


# virtual methods
.method public abstract closeTeletext()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract detectInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract forceVideoStandard(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getAspectRatio()Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getChinaDvbcRegion()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getMheg5PfgContent()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getNitFrequencyByDtvRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getPhaseRange()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getVideoStandardDetectionState()Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract hasTeletextClockSignal()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract hasTeletextSignal()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract initOfflineDetection()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract isHdmiMode()Z
.end method

.method public abstract isSignalStable()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract isTeletextDisplayed()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract isTeletextSubtitleChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract openPAT(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract release()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation
.end method

.method public abstract sendMheg5Command(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract sendMheg5IcsCommand(IS)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setChinaDvbcRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setDisplay(Landroid/view/SurfaceHolder;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setHPosition(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setHdmiGpio([I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setMirror(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setOnTvPlayerEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;)V
.end method

.method public abstract setPhase(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setSize(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setVPosition(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract startAutoStandardDetection()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract startPcModeAtuoTune()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
