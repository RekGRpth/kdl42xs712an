.class public final enum Lcom/mstar/android/tvapi/common/PictureManager$EVENT;
.super Ljava/lang/Enum;
.source "PictureManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/PictureManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/PictureManager$EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

.field public static final enum EV_4K2K_PHOTO_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

.field public static final enum EV_4K2K_PHOTO_DISABLE_PIP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

.field public static final enum EV_4K2K_PHOTO_DISABLE_POP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

.field public static final enum EV_4K2K_PHOTO_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

.field public static final enum EV_MAX:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

.field public static final enum EV_SET_ASPECTRATIO:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    const-string v1, "EV_SET_ASPECTRATIO"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_SET_ASPECTRATIO:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    const-string v1, "EV_4K2K_PHOTO_DISABLE_PIP"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_PIP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    const-string v1, "EV_4K2K_PHOTO_DISABLE_POP"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_POP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    const-string v1, "EV_4K2K_PHOTO_DISABLE_DUALVIEW"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    const-string v1, "EV_4K2K_PHOTO_DISABLE_TRAVELINGMODE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    const-string v1, "EV_MAX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    sget-object v1, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_SET_ASPECTRATIO:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_PIP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_POP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/PictureManager$EVENT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/PictureManager$EVENT;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    return-object v0
.end method
