.class public final enum Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;
.super Ljava/lang/Enum;
.source "TvTypeInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumStbSystemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

.field public static final enum E_STB_SYSTEM_TYPE_STB_DISABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

.field public static final enum E_STB_SYSTEM_TYPE_STB_ENABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    const-string v1, "E_STB_SYSTEM_TYPE_STB_DISABLE"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->E_STB_SYSTEM_TYPE_STB_DISABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    const-string v1, "E_STB_SYSTEM_TYPE_STB_ENABLE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->E_STB_SYSTEM_TYPE_STB_ENABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->E_STB_SYSTEM_TYPE_STB_DISABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->E_STB_SYSTEM_TYPE_STB_ENABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    return-object v0
.end method
