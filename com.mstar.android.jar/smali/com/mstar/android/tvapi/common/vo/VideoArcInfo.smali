.class public Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;
.super Ljava/lang/Object;
.source "VideoArcInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public adjArcDown:S

.field public adjArcLeft:S

.field public adjArcRight:S

.field public adjArcUp:S

.field private arcType:I

.field public bSetCusWin:Z

.field private threeDimensionArcType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->arcType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->threeDimensionArcType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcLeft:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcRight:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcUp:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcDown:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->bSetCusWin:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public get3dArcType()Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->threeDimensionArcType:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getArcType()Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->arcType:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public set3dArcType(Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->threeDimensionArcType:I

    return-void
.end method

.method public setArcType(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->arcType:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->arcType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->threeDimensionArcType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcLeft:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcRight:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcUp:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->adjArcDown:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;->bSetCusWin:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
