.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;
.super Ljava/lang/Enum;
.source "EnumAudioInputSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_ATV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_AV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_CVBS:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_DTV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_GAME1:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_GAME2:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_LMIC:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_MIC:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_MP3:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_PCM:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

.field public static final enum E_SCART:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_PCM"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_PCM:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_MIC"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_MIC:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_MP3"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_MP3:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_GAME1"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_GAME1:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_GAME2"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_GAME2:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_DTV"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_DTV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_ATV"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_ATV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_HDMI"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_SCART"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_SCART:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_AV"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_AV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_CVBS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_CVBS:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const-string v1, "E_LMIC"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_LMIC:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_PCM:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_MIC:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_MP3:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_GAME1:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_GAME2:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_DTV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_ATV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_SCART:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_AV:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_CVBS:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->E_LMIC:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumAudioInputSource;

    return-object v0
.end method
