.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;
.super Ljava/lang/Enum;
.source "EnumVideoItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MS_VIDEOITEM_BRIGHTNESS:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

.field public static final enum MS_VIDEOITEM_CONTRAST:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

.field public static final enum MS_VIDEOITEM_HUE:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

.field public static final enum MS_VIDEOITEM_NUM:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

.field public static final enum MS_VIDEOITEM_SATURATION:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

.field public static final enum MS_VIDEOITEM_SHARPNESS:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    const-string v1, "MS_VIDEOITEM_BRIGHTNESS"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_BRIGHTNESS:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    const-string v1, "MS_VIDEOITEM_CONTRAST"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_CONTRAST:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    const-string v1, "MS_VIDEOITEM_SATURATION"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_SATURATION:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    const-string v1, "MS_VIDEOITEM_SHARPNESS"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_SHARPNESS:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    const-string v1, "MS_VIDEOITEM_HUE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_HUE:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    const-string v1, "MS_VIDEOITEM_NUM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_NUM:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_BRIGHTNESS:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_CONTRAST:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_SATURATION:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_SHARPNESS:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_HUE:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->MS_VIDEOITEM_NUM:Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
