.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;
.super Ljava/lang/Enum;
.source "EnumSurroundMode.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum E_SURROUND_MODE_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

.field public static final enum E_SURROUND_MODE_ON:Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    const-string v1, "E_SURROUND_MODE_OFF"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->E_SURROUND_MODE_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    const-string v1, "E_SURROUND_MODE_ON"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->E_SURROUND_MODE_ON:Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->E_SURROUND_MODE_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->E_SURROUND_MODE_ON:Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
