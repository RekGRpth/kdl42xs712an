.class public Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;
.super Ljava/lang/Object;
.source "OverScanSettingUser.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public overScanHRatio:S

.field public overScanHposition:S

.field public overScanVRatio:S

.field public overScanVposition:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanHposition:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanVposition:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanHRatio:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanVRatio:S

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanHposition:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanVposition:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanHRatio:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanVRatio:S

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanHposition:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanVposition:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanHRatio:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OverScanSettingUser;->overScanVRatio:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
