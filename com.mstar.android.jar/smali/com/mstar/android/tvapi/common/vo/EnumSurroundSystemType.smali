.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;
.super Ljava/lang/Enum;
.source "EnumSurroundSystemType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public static final enum E_BBE:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public static final enum E_NUMS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public static final enum E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public static final enum E_SRS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public static final enum E_SURROUNDMAX:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public static final enum E_VDS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public static final enum E_VSPK:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const-string v1, "E_OFF"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const-string v1, "E_BBE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_BBE:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const-string v1, "E_SRS"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_SRS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const-string v1, "E_VDS"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_VDS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const-string v1, "E_VSPK"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_VSPK:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const-string v1, "E_SURROUNDMAX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_SURROUNDMAX:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const-string v1, "E_NUMS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_NUMS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_BBE:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_SRS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_VDS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_VSPK:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_SURROUNDMAX:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_NUMS:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    return-object v0
.end method
