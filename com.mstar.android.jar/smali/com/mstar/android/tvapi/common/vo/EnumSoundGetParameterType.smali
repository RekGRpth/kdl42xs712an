.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;
.super Ljava/lang/Enum;
.source "EnumSoundGetParameterType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_AVC_AT:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_AVC_ONOFF:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_AVC_RT:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_AVC_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_BASS:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_DRC_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_ECHO_TIME:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND0_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND1_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND2_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND3_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND4_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND5_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND6_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_EQ_BAND7_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_MSURR_XA:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_MSURR_XB:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_MSURR_XK:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_NR_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND0_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND0_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND0_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND1_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND1_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND1_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND2_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND2_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND2_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND3_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND3_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND3_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND4_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND4_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND4_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND5_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND5_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND5_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND6_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND6_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND6_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND7_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND7_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PEQ_BAND7_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

.field public static final enum E_TREBLE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PRESCALE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_TREBLE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_TREBLE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_BASS"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_BASS:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_BALANCE"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND0_LEVEL"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND0_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND1_LEVEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND1_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND2_LEVEL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND2_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND3_LEVEL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND3_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND4_LEVEL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND4_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND5_LEVEL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND5_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND6_LEVEL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND6_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_EQ_BAND7_LEVEL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND7_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND0_GAIN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND0_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND1_GAIN"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND1_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND2_GAIN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND2_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND3_GAIN"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND3_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND4_GAIN"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND4_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND5_GAIN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND5_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND6_GAIN"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND6_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND7_GAIN"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND7_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND0_FC"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND0_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND1_FC"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND1_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND2_FC"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND2_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND3_FC"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND3_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND4_FC"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND4_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND5_FC"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND5_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND6_FC"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND6_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND7_FC"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND7_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND0_QVALUE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND0_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND1_QVALUE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND1_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND2_QVALUE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND2_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND3_QVALUE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND3_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND4_QVALUE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND4_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND5_QVALUE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND5_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND6_QVALUE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND6_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_PEQ_BAND7_QVALUE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND7_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_AVC_ONOFF"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_ONOFF:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_AVC_THRESHOLD"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_AVC_AT"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_AT:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_AVC_RT"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_RT:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_MSURR_XA"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_MSURR_XA:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_MSURR_XB"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_MSURR_XB:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_MSURR_XK"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_MSURR_XK:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_DRC_THRESHOLD"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_DRC_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_NR_THRESHOLD"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_NR_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const-string v1, "E_ECHO_TIME"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_ECHO_TIME:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    const/16 v0, 0x2e

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_TREBLE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_BASS:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND0_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND1_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND2_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND3_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND4_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND5_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND6_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_EQ_BAND7_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND0_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND1_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND2_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND3_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND4_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND5_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND6_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND7_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND0_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND1_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND2_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND3_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND4_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND5_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND6_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND7_FC:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND0_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND1_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND2_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND3_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND4_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND5_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND6_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_PEQ_BAND7_QVALUE:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_ONOFF:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_AT:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_AVC_RT:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_MSURR_XA:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_MSURR_XB:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_MSURR_XK:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_DRC_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_NR_THRESHOLD:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->E_ECHO_TIME:Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;

    return-object v0
.end method
