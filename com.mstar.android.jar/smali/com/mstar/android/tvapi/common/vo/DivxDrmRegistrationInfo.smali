.class public Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;
.super Ljava/lang/Object;
.source "DivxDrmRegistrationInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public clearLastMemory:S

.field public deActivationCode:[C

.field public drmData:[S

.field public isActivated:S

.field public isDeactivated:S

.field public isKeyGenerated:S

.field public registrationCode:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x30

    const/16 v4, 0xb

    const/16 v3, 0x9

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v4, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->registrationCode:[C

    new-array v1, v3, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->deActivationCode:[C

    new-array v1, v5, [S

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->drmData:[S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isKeyGenerated:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isActivated:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isDeactivated:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->clearLastMemory:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->registrationCode:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->deActivationCode:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v5, :cond_2

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->drmData:[S

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    const/16 v3, 0x30

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0xb

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->registrationCode:[C

    const/16 v1, 0x9

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->deActivationCode:[C

    new-array v1, v3, [S

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->drmData:[S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isKeyGenerated:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isActivated:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isDeactivated:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->clearLastMemory:S

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->registrationCode:[C

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readCharArray([C)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->deActivationCode:[C

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readCharArray([C)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->drmData:[S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->registrationCode:[C

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeCharArray([C)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->deActivationCode:[C

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeCharArray([C)V

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x30

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->drmData:[S

    aget-short v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isKeyGenerated:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isActivated:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->isDeactivated:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;->clearLastMemory:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
