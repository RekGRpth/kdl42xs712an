.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;
.super Ljava/lang/Enum;
.source "EnumDisplayMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

.field public static final enum E_MODE_2D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

.field public static final enum E_MODE_2DTO3D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

.field public static final enum E_MODE_3D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

.field public static final enum E_MODE_3DTO2D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

.field public static final enum E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    const-string v1, "E_MODE_2D"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_2D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    const-string v1, "E_MODE_3D"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_3D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    const-string v1, "E_MODE_2DTO3D"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_2DTO3D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    const-string v1, "E_MODE_3DTO2D"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_3DTO2D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    const-string v1, "E_NUM"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_2D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_3D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_2DTO3D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_3DTO2D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    return-object v0
.end method
