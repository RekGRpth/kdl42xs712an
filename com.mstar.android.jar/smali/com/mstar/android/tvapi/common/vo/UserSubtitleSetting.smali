.class public Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;
.super Ljava/lang/Object;
.source "UserSubtitleSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public enableSubTitle:I

.field public hardOfHearing:I

.field public reserved:I

.field public subtitleDefaultLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public subtitleDefaultLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->subtitleDefaultLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->subtitleDefaultLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->hardOfHearing:I

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->enableSubTitle:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->reserved:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->subtitleDefaultLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->subtitleDefaultLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->hardOfHearing:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->enableSubTitle:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->reserved:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->subtitleDefaultLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->subtitleDefaultLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->hardOfHearing:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->enableSubTitle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSubtitleSetting;->reserved:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
