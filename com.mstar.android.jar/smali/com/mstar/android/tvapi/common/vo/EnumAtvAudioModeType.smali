.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
.super Ljava/lang/Enum;
.source "EnumAtvAudioModeType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_FORCED_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_G_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_HIDEV_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_K_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_LEFT_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_LEFT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_MONO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_NICAM_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_NICAM_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_NICAM_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_NICAM_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_NICAM_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_RIGHT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

.field public static final enum E_ATV_AUDIOMODE_STEREO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_INVALID"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_MONO"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_FORCED_MONO"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_FORCED_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_G_STEREO"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_G_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_K_STEREO"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_K_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_MONO_SAP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_STEREO_SAP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_STEREO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_DUAL_A"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_DUAL_B"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_DUAL_AB"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_NICAM_MONO"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_NICAM_STEREO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_NICAM_DUAL_A"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_NICAM_DUAL_B"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_NICAM_DUAL_AB"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_HIDEV_MONO"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_HIDEV_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_LEFT_LEFT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_RIGHT_RIGHT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_RIGHT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_LEFT_RIGHT"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const-string v1, "E_ATV_AUDIOMODE_NUM"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_FORCED_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_G_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_K_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_STEREO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_HIDEV_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_RIGHT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    return-object v0
.end method
