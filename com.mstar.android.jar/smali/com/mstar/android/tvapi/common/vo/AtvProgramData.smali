.class public Lcom/mstar/android/tvapi/common/vo/AtvProgramData;
.super Ljava/lang/Object;
.source "AtvProgramData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/AtvProgramData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public fineTune:B

.field public listPage:[S

.field public misc:Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

.field public name:Ljava/lang/String;

.field public pll:I

.field public sort:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/AtvProgramData$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->pll:I

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;-><init>()V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->misc:Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->sort:S

    iput-byte v2, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->fineTune:B

    const-string v1, ""

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->name:Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x5

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->pll:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->misc:Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->sort:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->fineTune:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->name:Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->pll:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->misc:Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->writeToParcel(Landroid/os/Parcel;I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->sort:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-byte v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->fineTune:B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->listPage:[S

    aget-short v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
