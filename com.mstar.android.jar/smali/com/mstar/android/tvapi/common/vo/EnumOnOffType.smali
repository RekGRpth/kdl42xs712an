.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;
.super Ljava/lang/Enum;
.source "EnumOnOffType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

.field public static final enum E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

.field public static final enum E_ON:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    const-string v1, "E_OFF"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    const-string v1, "E_ON"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->E_ON:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    const-string v1, "E_NUM"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->E_ON:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/android/tvapi/common/vo/EnumOnOffType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
