.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;
.super Ljava/lang/Enum;
.source "EnumAdvancedSoundSubProcessType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_ABX_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_ABX_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_DYNAMIC_EQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_DYNAMIC_EQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_DYNAMIC_VOL_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_DYNAMIC_VOL_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_PEQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_AUDYSSEY_PEQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_DTS_ULTRATV_ENVELO_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_DTS_ULTRATV_ENVELO_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_DTS_ULTRATV_SYM_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_DTS_ULTRATV_SYM_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_NO_SUBPROC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_DEFINITION_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_DEFINITION_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_HARDLIMITER_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_HARDLIMITER_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_HPF_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_HPF_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUEQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUEQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUEVOLUME_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUEVOLUME_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TSHD_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TSHD_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TSHD_SURR_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_THEATERSOUND_TSHD_SURR_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_DEFINITION_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_DEFINITION_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_SRS3D_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_SRS3D_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSHD_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSXT_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSXT_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSXT_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

.field public static final enum E_SRS_TSXT_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSXT_TRUEBASS_ON"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSXT_TRUEBASS_OFF"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSXT_DYNAMIC_CLARITY_ON"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSXT_DYNAMIC_CLARITY_OFF"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_TRUEBASS_ON"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_TRUEBASS_OFF"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_DYNAMIC_CLARITY_ON"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_DYNAMIC_CLARITY_OFF"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_DEFINITION_ON"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DEFINITION_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_DEFINITION_OFF"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DEFINITION_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_SRS3D_ON"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_SRS3D_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_TSHD_SRS3D_OFF"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_SRS3D_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TSHD_ON"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TSHD_OFF"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUEBASS_ON"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUEBASS_OFF"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_DYNAMIC_CLARITY_ON"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_DYNAMIC_CLARITY_OFF"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_DEFINITION_ON"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DEFINITION_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_DEFINITION_OFF"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DEFINITION_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUEVOLUME_ON"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEVOLUME_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUEVOLUME_OFF"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEVOLUME_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_HARDLIMITER_ON"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HARDLIMITER_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_HARDLIMITER_OFF"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HARDLIMITER_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_HPF_ON"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HPF_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_HPF_OFF"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HPF_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUEQ_ON"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUEQ_OFF"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_ON"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_OFF"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_DTS_ULTRATV_ENVELO_ON"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_ENVELO_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_DTS_ULTRATV_ENVELO_OFF"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_ENVELO_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_DTS_ULTRATV_SYM_ON"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_SYM_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_DTS_ULTRATV_SYM_OFF"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_SYM_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_DYNAMIC_VOL_ON"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_VOL_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_DYNAMIC_VOL_OFF"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_VOL_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_DYNAMIC_EQ_ON"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_EQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_DYNAMIC_EQ_OFF"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_EQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_PEQ_ON"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_PEQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_PEQ_OFF"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_PEQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_ABX_ON"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_ABX_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_AUDYSSEY_ABX_OFF"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_ABX_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TSHD_SURR_ON"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_SURR_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TSHD_SURR_OFF"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_SURR_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_ON"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_OFF"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const-string v1, "E_NO_SUBPROC"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_NO_SUBPROC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    const/16 v0, 0x2f

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSXT_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DEFINITION_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_DEFINITION_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_SRS3D_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_TSHD_SRS3D_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DEFINITION_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DEFINITION_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEVOLUME_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEVOLUME_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HARDLIMITER_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HARDLIMITER_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HPF_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_HPF_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUVOLUME_NOISE_MANAGER_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_ENVELO_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_ENVELO_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_SYM_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_DTS_ULTRATV_SYM_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_VOL_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_VOL_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_EQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_DYNAMIC_EQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_PEQ_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_PEQ_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_ABX_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_AUDYSSEY_ABX_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_SURR_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_SURR_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUBASS_LEVEL_INDP_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_NO_SUBPROC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    return-object v0
.end method
