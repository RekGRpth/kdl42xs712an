.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;
.super Ljava/lang/Enum;
.source "EnumAdvancedSoundType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_AUDYSSEY:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_BBE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_DOLBY_PL2VDPK:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_DOLBY_PL2VDS:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_DTS_ULTRATV:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_NONE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_RESERVE1:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_RESERVE2:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_RESERVE3:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_RESERVE4:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_RESERVE5:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_RESERVE6:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_SRS_THEATERSOUND:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_SRS_TSXT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

.field public static final enum E_SUPER_VOICE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_DOLBY_PL2VDS"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_DOLBY_PL2VDS:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_DOLBY_PL2VDPK"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_DOLBY_PL2VDPK:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_BBE"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_BBE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_SRS_TSXT"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSXT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_SRS_TSHD"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_SRS_THEATERSOUND"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_THEATERSOUND:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_DTS_ULTRATV"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_DTS_ULTRATV:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_AUDYSSEY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_AUDYSSEY:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_SUPER_VOICE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SUPER_VOICE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_RESERVE1"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE1:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_RESERVE2"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE2:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_RESERVE3"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE3:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_RESERVE4"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE4:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_RESERVE5"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE5:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_RESERVE6"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE6:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const-string v1, "E_NONE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_NONE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_DOLBY_PL2VDS:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_DOLBY_PL2VDPK:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_BBE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSXT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_THEATERSOUND:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_DTS_ULTRATV:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_AUDYSSEY:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SUPER_VOICE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE1:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE2:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE3:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE4:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE5:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_RESERVE6:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_NONE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    return-object v0
.end method
