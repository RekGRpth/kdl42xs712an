.class public final enum Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;
.super Ljava/lang/Enum;
.source "PvrPlaybackSpeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumPvrPlaybackSpeed"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_0X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_16XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_16XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_1XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_2XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_2XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_32XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_32XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_4XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_4XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_8XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_8XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_FF_1_16X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_FF_1_2X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_FF_1_32X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_FF_1_4X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_FF_1_8X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_INVALID:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field public static final enum E_PVR_PLAYBACK_SPEED_STEP_IN:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_32XFB"

    const/16 v2, -0x7d00

    invoke-direct {v0, v1, v4, v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_16XFB"

    const/16 v2, -0x3e80

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_8XFB"

    const/16 v2, -0x1f40

    invoke-direct {v0, v1, v6, v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_4XFB"

    const/16 v2, -0xfa0

    invoke-direct {v0, v1, v7, v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_2XFB"

    const/16 v2, -0x7d0

    invoke-direct {v0, v1, v8, v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_1XFB"

    const/4 v2, 0x5

    const/16 v3, -0x3e8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_0X"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_0X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_STEP_IN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v5}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_STEP_IN:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_FF_1_32X"

    const/16 v2, 0x8

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_32X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_FF_1_16X"

    const/16 v2, 0x9

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_16X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_FF_1_8X"

    const/16 v2, 0xa

    const/16 v3, 0x7d

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_8X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_FF_1_4X"

    const/16 v2, 0xb

    const/16 v3, 0xfa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_4X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_FF_1_2X"

    const/16 v2, 0xc

    const/16 v3, 0x1f4

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_2X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_1X"

    const/16 v2, 0xd

    const/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_2XFF"

    const/16 v2, 0xe

    const/16 v3, 0x7d0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_4XFF"

    const/16 v2, 0xf

    const/16 v3, 0xfa0

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_8XFF"

    const/16 v2, 0x10

    const/16 v3, 0x1f40

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_16XFF"

    const/16 v2, 0x11

    const/16 v3, 0x3e80

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_32XFF"

    const/16 v2, 0x12

    const/16 v3, 0x7d00

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const-string v1, "E_PVR_PLAYBACK_SPEED_INVALID"

    const/16 v2, 0x13

    const v3, 0xffff

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_INVALID:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_0X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_STEP_IN:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_32X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_16X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_8X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_4X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_2X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_INVALID:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    sput v4, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed;->mHtEnumPvrPlaybackSpeed:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed;->mHtEnumPvrPlaybackSpeed:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->value:I

    return v0
.end method
