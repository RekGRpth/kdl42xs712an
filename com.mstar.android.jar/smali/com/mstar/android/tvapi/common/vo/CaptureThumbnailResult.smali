.class public Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;
.super Ljava/lang/Object;
.source "CaptureThumbnailResult.java"


# instance fields
.field public thumbnailIndex:I

.field private thumbnailStatus:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPvrThumbnailStatus()Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;->thumbnailStatus:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setPvrThumbnailStatus(Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;->thumbnailStatus:I

    return-void
.end method
