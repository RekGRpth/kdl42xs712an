.class public Lcom/mstar/android/tvapi/common/vo/NetworkSetting;
.super Ljava/lang/Object;
.source "NetworkSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/NetworkSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bnetSelected:Z

.field public dns0:S

.field public dns1:S

.field public dns2:S

.field public dns3:S

.field public gateway0:S

.field public gateway1:S

.field public gateway2:S

.field public gateway3:S

.field public ip:[C

.field public ipAddr0:S

.field public ipAddr1:S

.field public ipAddr2:S

.field public ipAddr3:S

.field public ipName:[C

.field public netMask0:S

.field public netMask1:S

.field public netMask2:S

.field public netMask3:S

.field public netPassword:[C

.field public netUserName:[C

.field public netconfig:Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/NetworkSetting$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x80

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ip:[C

    new-array v1, v3, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipName:[C

    new-array v1, v3, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netUserName:[C

    new-array v1, v3, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netPassword:[C

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->bnetSelected:Z

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;->E_DHCP:Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netconfig:Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr0:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr1:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr2:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr3:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask0:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask1:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask2:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask3:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway0:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway1:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway2:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway3:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns0:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns1:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns2:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns3:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ip:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipName:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_2

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netUserName:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v3, :cond_3

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netPassword:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    const/16 v2, 0x80

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v2, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ip:[C

    new-array v1, v2, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipName:[C

    new-array v1, v2, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netUserName:[C

    new-array v1, v2, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netPassword:[C

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->bnetSelected:Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netconfig:Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr0:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr1:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr2:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr3:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask0:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask1:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask2:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask3:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway0:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway1:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway2:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway3:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns0:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns1:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns2:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns3:S

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ip:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readCharArray([C)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipName:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readCharArray([C)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netUserName:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readCharArray([C)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netPassword:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readCharArray([C)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->bnetSelected:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netconfig:Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumNetConfigurationType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr0:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr1:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr2:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipAddr3:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask0:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask1:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask2:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netMask3:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway0:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway1:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway2:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->gateway3:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns0:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns1:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns2:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->dns3:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ip:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->ipName:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netUserName:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/NetworkSetting;->netPassword:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
