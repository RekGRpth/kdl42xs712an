.class public final enum Lcom/mstar/android/tvapi/common/vo/Enum3dType;
.super Ljava/lang/Enum;
.source "Enum3dType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/Enum3dType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field public static final enum EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_NONE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_SIDE_BY_SIDE_HALF"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_TOP_BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_FRAME_PACKING_1080P"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_FRAME_PACKING_720P"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_LINE_ALTERNATIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_FRAME_ALTERNATIVE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_FIELD_ALTERNATIVE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_CHECK_BORAD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_2DTO3D"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_DUALVIEW"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_PIXEL_ALTERNATIVE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "EN_3D_TYPE_NUM"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/Enum3dType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    return-object v0
.end method
