.class public Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;
.super Ljava/lang/Object;
.source "PvrFileInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public filename:Ljava/lang/String;

.field public isRecording:Z

.field private key:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->isRecording:Z

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->key:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->isRecording:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->key:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPvrFileInfoSortKey()Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->values()[Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->key:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getPvrFileInfoSortKey(Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->key:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->isRecording:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->key:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
