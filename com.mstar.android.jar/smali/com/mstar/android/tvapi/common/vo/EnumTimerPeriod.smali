.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;
.super Ljava/lang/Enum;
.source "EnumTimerPeriod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Everyday:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Mon2Fri:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Mon2Sat:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Num:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Sat2Sun:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

.field public static final enum EN_Timer_Sun:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Off"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Once"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Everyday"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Everyday:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Mon2Fri"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Mon2Fri:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Mon2Sat"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Mon2Sat:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Sat2Sun"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Sat2Sun:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Sun"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Sun:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const-string v1, "EN_Timer_Num"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Num:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Everyday:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Mon2Fri:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Mon2Sat:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Sat2Sun:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Sun:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Num:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    return-object v0
.end method
