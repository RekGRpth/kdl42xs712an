.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;
.super Ljava/lang/Enum;
.source "EnumThreeDVideo3DTo2D.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum E_ThreeD_Video_3DTO2D_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field public static final enum E_ThreeD_Video_3DTO2D_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field public static final enum E_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field public static final enum E_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field public static final enum E_ThreeD_Video_3DTO2D_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field public static final enum E_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field public static final enum E_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v1, "E_ThreeD_Video_3DTO2D_NONE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v1, "E_ThreeD_Video_3DTO2D_SIDE_BY_SIDE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v1, "E_ThreeD_Video_3DTO2D_TOP_BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v1, "E_ThreeD_Video_3DTO2D_FRAME_PACKING"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v1, "E_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v1, "E_ThreeD_Video_3DTO2D_AUTO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v1, "E_ThreeD_Video_3DTO2D_COUNT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
