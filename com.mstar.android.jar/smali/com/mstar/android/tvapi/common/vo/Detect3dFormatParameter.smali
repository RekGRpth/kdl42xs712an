.class public Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;
.super Ljava/lang/Object;
.source "Detect3dFormatParameter.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bCbPixelThreshold:I

.field public detect3DFormatPara_Version:I

.field public gYPixelThreshold:I

.field public hitPixelPercentage:I

.field public horSampleCount:I

.field public horSearchRange:I

.field public maxCheckingFrameCount:I

.field public rCrPixelThreshold:I

.field public verSampleCount:I

.field public verSearchRange:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->detect3DFormatPara_Version:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSearchRange:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSearchRange:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->maxCheckingFrameCount:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->detect3DFormatPara_Version:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSearchRange:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSearchRange:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->maxCheckingFrameCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->detect3DFormatPara_Version:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSearchRange:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSearchRange:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->maxCheckingFrameCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
