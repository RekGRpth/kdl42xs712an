.class public Lcom/mstar/android/tvapi/common/vo/TvOsType;
.super Ljava/lang/Object;
.source "TvOsType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCaption;,
        Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTravelingEngineType;,
        Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;,
        Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;,
        Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;,
        Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    }
.end annotation


# static fields
.field public static final BIT0:I = 0x1

.field public static final BIT1:I = 0x2

.field public static final BIT10:I = 0x400

.field public static final BIT11:I = 0x800

.field public static final BIT12:I = 0x1000

.field public static final BIT13:I = 0x2000

.field public static final BIT14:I = 0x4000

.field public static final BIT15:I = 0x8000

.field public static final BIT16:I = 0x10000

.field public static final BIT17:I = 0x20000

.field public static final BIT18:I = 0x40000

.field public static final BIT19:I = 0x80000

.field public static final BIT2:I = 0x4

.field public static final BIT20:I = 0x100000

.field public static final BIT3:I = 0x8

.field public static final BIT4:I = 0x10

.field public static final BIT5:I = 0x20

.field public static final BIT6:I = 0x40

.field public static final BIT7:I = 0x80

.field public static final BIT8:I = 0x100

.field public static final BIT9:I = 0x200

.field private static htTimeZone:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType;->htTimeZone:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/Hashtable;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType;->htTimeZone:Ljava/util/Hashtable;

    return-object v0
.end method
