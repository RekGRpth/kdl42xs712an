.class public final enum Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
.super Ljava/lang/Enum;
.source "TvOsType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/TvOsType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumTimeZone"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ACRE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ADLAIDE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ALMAATA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_AMSTERDAM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_AM_WEST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ASUNCION:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ATHENS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_AZORES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BANGKOK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BEIJING:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BELMOPAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BEOGRAD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BERLIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BRASILIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BRUSSELS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BUCURESTI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BUDAPEST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_BUENOS_AIRES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_CALCUTTA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_CANARY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_CARACAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_COPENHAGEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_DUBAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_DUBLIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_F_NORONHA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GENEVA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_0_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_0_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_10POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_10POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_12_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_12_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_13_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_13_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_3POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_3POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_4POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_4POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_5POINT45_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_5POINT45_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_5POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_5POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_6POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_6POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_9POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_9POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS3_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS3_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GMT_MINUS9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_GUATEMALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_HELSINKI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ISTANBUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_KABUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_KATHMANDU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_LA_PAZ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_LIMA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_LISBON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_LIUBLJANA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_LONDON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_LUXEMBOURG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_MADRID:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_MANAGUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_MONTEVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_MOSCOW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_M_GROSSO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTHEAST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_ALASKAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_ATLANTIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_CENTRAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_EASTERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_HAWAIIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_MIDWAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_MOUNTAIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_NEWFOUNDLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NORTH_AMERICA_PACIFIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NSW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_NZST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_OSLO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_PARIS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_PRAGUE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_QLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_QUITO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ROME:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_SA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_SANTIAGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_SAN_JOSE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_SEOUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_SOFIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_STOCKHOLM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_SYNDEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_TALLINN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_TAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_TEHERAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_TOKYO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_TONGATAPU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_URAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_VIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_VIENNA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_VILNIUS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_WA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_WARSAW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_YANGON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field public static final enum E_TIMEZONE_ZAGREB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_0_START"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_CANARY"

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v2

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_CANARY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_MONTEVIDEO"

    invoke-direct {v0, v1, v6, v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MONTEVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_LIMA"

    invoke-direct {v0, v1, v7, v6}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LIMA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_SANTIAGO"

    invoke-direct {v0, v1, v8, v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SANTIAGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_CARACAS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_CARACAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_QUITO"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_QUITO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_SAN_JOSE"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SAN_JOSE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ASUNCION"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ASUNCION:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_LA_PAZ"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LA_PAZ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BELMOPAN"

    const/16 v2, 0xa

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BELMOPAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_MANAGUA"

    const/16 v2, 0xb

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MANAGUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GUATEMALA"

    const/16 v2, 0xc

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GUATEMALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_LISBON"

    const/16 v2, 0xd

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LISBON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_DUBLIN"

    const/16 v2, 0xe

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_DUBLIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_LONDON"

    const/16 v2, 0xf

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LONDON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_0_END"

    const/16 v2, 0x10

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LONDON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_1_START"

    const/16 v2, 0x11

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_AMSTERDAM"

    const/16 v2, 0x12

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_AMSTERDAM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BEOGRAD"

    const/16 v2, 0x13

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BEOGRAD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BERLIN"

    const/16 v2, 0x14

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BERLIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BRUSSELS"

    const/16 v2, 0x15

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BRUSSELS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BUDAPEST"

    const/16 v2, 0x16

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BUDAPEST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_COPENHAGEN"

    const/16 v2, 0x17

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_COPENHAGEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GENEVA"

    const/16 v2, 0x18

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GENEVA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_LIUBLJANA"

    const/16 v2, 0x19

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LIUBLJANA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_LUXEMBOURG"

    const/16 v2, 0x1a

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LUXEMBOURG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_MADRID"

    const/16 v2, 0x1b

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MADRID:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_OSLO"

    const/16 v2, 0x1c

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_OSLO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_PARIS"

    const/16 v2, 0x1d

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_PARIS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_PRAGUE"

    const/16 v2, 0x1e

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_PRAGUE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ROME"

    const/16 v2, 0x1f

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ROME:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_STOCKHOLM"

    const/16 v2, 0x20

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_STOCKHOLM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_WARSAW"

    const/16 v2, 0x21

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_WARSAW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_VIENNA"

    const/16 v2, 0x22

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_VIENNA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ZAGREB"

    const/16 v2, 0x23

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ZAGREB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_1_END"

    const/16 v2, 0x24

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ZAGREB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_2_START"

    const/16 v2, 0x25

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ATHENS"

    const/16 v2, 0x26

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ATHENS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BUCURESTI"

    const/16 v2, 0x27

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BUCURESTI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_HELSINKI"

    const/16 v2, 0x28

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_HELSINKI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ISTANBUL"

    const/16 v2, 0x29

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ISTANBUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_SOFIA"

    const/16 v2, 0x2a

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SOFIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_TALLINN"

    const/16 v2, 0x2b

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TALLINN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_VILNIUS"

    const/16 v2, 0x2c

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_VILNIUS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_2_END"

    const/16 v2, 0x2d

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_VILNIUS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_3_START"

    const/16 v2, 0x2e

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_MOSCOW"

    const/16 v2, 0x2f

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MOSCOW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_3_END"

    const/16 v2, 0x30

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MOSCOW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_3POINT5_START"

    const/16 v2, 0x31

    const/16 v3, 0x29

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_TEHERAN"

    const/16 v2, 0x32

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TEHERAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_3POINT5_END"

    const/16 v2, 0x33

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TEHERAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_4_START"

    const/16 v2, 0x34

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_DUBAI"

    const/16 v2, 0x35

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_DUBAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_4_END"

    const/16 v2, 0x36

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_DUBAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_4POINT5_START"

    const/16 v2, 0x37

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_KABUL"

    const/16 v2, 0x38

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_KABUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_4POINT5_END"

    const/16 v2, 0x39

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_KABUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_5_START"

    const/16 v2, 0x3a

    const/16 v3, 0x2c

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_URAL"

    const/16 v2, 0x3b

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_URAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_5_END"

    const/16 v2, 0x3c

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_URAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_5POINT5_START"

    const/16 v2, 0x3d

    const/16 v3, 0x2d

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_CALCUTTA"

    const/16 v2, 0x3e

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_CALCUTTA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_5POINT5_END"

    const/16 v2, 0x3f

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_CALCUTTA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_5POINT45_START"

    const/16 v2, 0x40

    const/16 v3, 0x2e

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_KATHMANDU"

    const/16 v2, 0x41

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_KATHMANDU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_5POINT45_END"

    const/16 v2, 0x42

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_KATHMANDU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_6_START"

    const/16 v2, 0x43

    const/16 v3, 0x2f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ALMAATA"

    const/16 v2, 0x44

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ALMAATA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_6_END"

    const/16 v2, 0x45

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ALMAATA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_6POINT5_START"

    const/16 v2, 0x46

    const/16 v3, 0x30

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_YANGON"

    const/16 v2, 0x47

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_YANGON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_6POINT5_END"

    const/16 v2, 0x48

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_YANGON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_7_START"

    const/16 v2, 0x49

    const/16 v3, 0x31

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BANGKOK"

    const/16 v2, 0x4a

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BANGKOK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_7_END"

    const/16 v2, 0x4b

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BANGKOK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_8_START"

    const/16 v2, 0x4c

    const/16 v3, 0x32

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_WA"

    const/16 v2, 0x4d

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_WA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BEIJING"

    const/16 v2, 0x4e

    const/16 v3, 0x33

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BEIJING:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_8_END"

    const/16 v2, 0x4f

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BEIJING:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_9_START"

    const/16 v2, 0x50

    const/16 v3, 0x34

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_TOKYO"

    const/16 v2, 0x51

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TOKYO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_SEOUL"

    const/16 v2, 0x52

    const/16 v3, 0x35

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SEOUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_9_END"

    const/16 v2, 0x53

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SEOUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_9POINT5_START"

    const/16 v2, 0x54

    const/16 v3, 0x36

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_SA"

    const/16 v2, 0x55

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NT"

    const/16 v2, 0x56

    const/16 v3, 0x37

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_9POINT5_END"

    const/16 v2, 0x57

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_10_START"

    const/16 v2, 0x58

    const/16 v3, 0x38

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NSW"

    const/16 v2, 0x59

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NSW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_VIC"

    const/16 v2, 0x5a

    const/16 v3, 0x39

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_VIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_QLD"

    const/16 v2, 0x5b

    const/16 v3, 0x3a

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_QLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_TAS"

    const/16 v2, 0x5c

    const/16 v3, 0x3b

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_10_END"

    const/16 v2, 0x5d

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_10POINT5_START"

    const/16 v2, 0x5e

    const/16 v3, 0x3c

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ADLAIDE"

    const/16 v2, 0x5f

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ADLAIDE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_10POINT5_END"

    const/16 v2, 0x60

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ADLAIDE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_11_START"

    const/16 v2, 0x61

    const/16 v3, 0x3d

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_SYNDEY"

    const/16 v2, 0x62

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SYNDEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_11_END"

    const/16 v2, 0x63

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SYNDEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_12_START"

    const/16 v2, 0x64

    const/16 v3, 0x3e

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NZST"

    const/16 v2, 0x65

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NZST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_12_END"

    const/16 v2, 0x66

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NZST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_13_START"

    const/16 v2, 0x67

    const/16 v3, 0x3f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_TONGATAPU"

    const/16 v2, 0x68

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TONGATAPU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_13_END"

    const/16 v2, 0x69

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TONGATAPU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS11_START"

    const/16 v2, 0x6a

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_MIDWAY"

    const/16 v2, 0x6b

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_MIDWAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS11_END"

    const/16 v2, 0x6c

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_MIDWAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS10_START"

    const/16 v2, 0x6d

    const/16 v3, 0x41

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_HAWAIIAN"

    const/16 v2, 0x6e

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_HAWAIIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS10_END"

    const/16 v2, 0x6f

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_HAWAIIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS9_START"

    const/16 v2, 0x70

    const/16 v3, 0x42

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_ALASKAN"

    const/16 v2, 0x71

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_ALASKAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS9_END"

    const/16 v2, 0x72

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_ALASKAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS8_START"

    const/16 v2, 0x73

    const/16 v3, 0x43

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_PACIFIC"

    const/16 v2, 0x74

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_PACIFIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS8_END"

    const/16 v2, 0x75

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_PACIFIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS7_START"

    const/16 v2, 0x76

    const/16 v3, 0x44

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_MOUNTAIN"

    const/16 v2, 0x77

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_MOUNTAIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS7_END"

    const/16 v2, 0x78

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_MOUNTAIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS6_START"

    const/16 v2, 0x79

    const/16 v3, 0x45

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_CENTRAL"

    const/16 v2, 0x7a

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_CENTRAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS6_END"

    const/16 v2, 0x7b

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_CENTRAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS5_START"

    const/16 v2, 0x7c

    const/16 v3, 0x46

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_EASTERN"

    const/16 v2, 0x7d

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_EASTERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_AM_WEST"

    const/16 v2, 0x7e

    const/16 v3, 0x47

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_AM_WEST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_ACRE"

    const/16 v2, 0x7f

    const/16 v3, 0x48

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ACRE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS5_END"

    const/16 v2, 0x80

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ACRE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS4_START"

    const/16 v2, 0x81

    const/16 v3, 0x49

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_ATLANTIC"

    const/16 v2, 0x82

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_ATLANTIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_M_GROSSO"

    const/16 v2, 0x83

    const/16 v3, 0x4a

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_M_GROSSO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH"

    const/16 v2, 0x84

    const/16 v3, 0x4b

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS4_END"

    const/16 v2, 0x85

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS3_5_START"

    const/16 v2, 0x86

    const/16 v3, 0x4c

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTH_AMERICA_NEWFOUNDLAND"

    const/16 v2, 0x87

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_NEWFOUNDLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS3_5_END"

    const/16 v2, 0x88

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_NEWFOUNDLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS3_START"

    const/16 v2, 0x89

    const/16 v3, 0x4d

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BUENOS_AIRES"

    const/16 v2, 0x8a

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BUENOS_AIRES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_BRASILIA"

    const/16 v2, 0x8b

    const/16 v3, 0x4e

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BRASILIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NORTHEAST"

    const/16 v2, 0x8c

    const/16 v3, 0x4f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTHEAST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS3_END"

    const/16 v2, 0x8d

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTHEAST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS2_START"

    const/16 v2, 0x8e

    const/16 v3, 0x50

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_F_NORONHA"

    const/16 v2, 0x8f

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_F_NORONHA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS2_END"

    const/16 v2, 0x90

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_F_NORONHA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS1_START"

    const/16 v2, 0x91

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_AZORES"

    const/16 v2, 0x92

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_AZORES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_GMT_MINUS1_END"

    const/16 v2, 0x93

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_AZORES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const-string v1, "E_TIMEZONE_NUM"

    const/16 v2, 0x94

    const/16 v3, 0x52

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    const/16 v0, 0x95

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_CANARY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MONTEVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LIMA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SANTIAGO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_CARACAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_QUITO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SAN_JOSE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ASUNCION:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LA_PAZ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BELMOPAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MANAGUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GUATEMALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LISBON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_DUBLIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LONDON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_AMSTERDAM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BEOGRAD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BERLIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BRUSSELS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BUDAPEST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_COPENHAGEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GENEVA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LIUBLJANA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_LUXEMBOURG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MADRID:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_OSLO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_PARIS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_PRAGUE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ROME:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_STOCKHOLM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_WARSAW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_VIENNA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ZAGREB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ATHENS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BUCURESTI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_HELSINKI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ISTANBUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SOFIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TALLINN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_VILNIUS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_MOSCOW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TEHERAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_DUBAI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_KABUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_URAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_CALCUTTA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_KATHMANDU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ALMAATA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_YANGON:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BANGKOK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_WA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BEIJING:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TOKYO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SEOUL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NSW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_VIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_QLD:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TAS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ADLAIDE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_SYNDEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NZST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_TONGATAPU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_MIDWAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_HAWAIIAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_ALASKAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_PACIFIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_MOUNTAIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_CENTRAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_EASTERN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_AM_WEST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_ACRE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_ATLANTIC:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_M_GROSSO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTH_AMERICA_NEWFOUNDLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BUENOS_AIRES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BRASILIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NORTHEAST:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_F_NORONHA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_AZORES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    sput v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/TvOsType;->htTimeZone:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/TvOsType;->htTimeZone:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->value:I

    return v0
.end method
