.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;
.super Ljava/lang/Enum;
.source "EnumAdvancedSoundParameterType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE1:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE10:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE11:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE12:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE13:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE14:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE15:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE16:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE17:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE18:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE19:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE2:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE20:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE21:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE22:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE23:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE24:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE25:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE26:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE27:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE28:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE29:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE3:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE30:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE4:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE5:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE6:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE7:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE8:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_RESERVE9:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_SPEAKER_ANALYSIS:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_SURR_LEVEL_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_TRUBASS_COMPRESSOR_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_TRUBASS_PROCESS_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_TRUBASS_SPEAKER_AUDIO:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_TSHD_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum ADVSND_SRS_THEATERSOUND_TSHD_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_AUDYSSEY_ABX_FILSET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_AUDYSSEY_ABX_GDRY:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_AUDYSSEY_ABX_GWET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_AUDYSSEY_DYNAMICEQ_EQOFFSET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_AUDYSSEY_DYNAMICVOL_COMPRESS_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_AUDYSSEY_DYNAMICVOL_GC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_AUDYSSEY_DYNAMICVOL_VOLSETTING:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DOLBY_PL2VDPK_SMOD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DOLBY_PL2VDPK_WMOD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_EVO_ADD3DBON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_EVO_MONOINPUT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_EVO_PCELEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_EVO_VLFELEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_EVO_WIDENINGON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_SYM_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_SYM_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_SYM_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_DTS_ULTRATV_SYM_RESET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_BOOST_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_HEADROOM_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_HPF_FC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_CALIBRATE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MAX_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_NOISE_MNGR_THLD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_REF_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_INPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_LIMITERCONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_OUTPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_SURROUND_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_WOWCENTER_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_WOWHDSRS3DMODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSHD_SET_WOWSPACE_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSXT_SET_DC_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSXT_SET_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSXT_SET_INPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSXT_SET_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSXT_SET_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

.field public static final enum E_ADVSND_SRS_TSXT_SET_TRUBASS_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DOLBY_PL2VDPK_SMOD"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DOLBY_PL2VDPK_SMOD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DOLBY_PL2VDPK_WMOD"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DOLBY_PL2VDPK_WMOD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSXT_SET_INPUT_GAIN"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSXT_SET_DC_GAIN"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_DC_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSXT_SET_TRUBASS_GAIN"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_TRUBASS_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSXT_SET_SPEAKERSIZE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSXT_SET_INPUT_MODE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_INPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSXT_SET_OUTPUT_GAIN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_INPUT_MODE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_INPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_OUTPUT_MODE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_OUTPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_SPEAKERSIZE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_TRUBASS_CONTROL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_DEFINITION_CONTROL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_DC_CONTROL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_SURROUND_LEVEL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_SURROUND_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_INPUT_GAIN"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_WOWSPACE_CONTROL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_WOWSPACE_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_WOWCENTER_CONTROL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_WOWCENTER_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_WOWHDSRS3DMODE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_WOWHDSRS3DMODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_LIMITERCONTROL"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_LIMITERCONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_TSHD_SET_OUTPUT_GAIN"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_INPUT_GAIN"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_DEFINITION_CONTROL"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_DC_CONTROL"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUBASS_CONTROL"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_SPEAKERSIZE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_LEVEL"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_BOOST_GAIN"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_BOOST_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_HEADROOM_GAIN"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HEADROOM_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MODE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_REF_LEVEL"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_REF_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MAX_GAIN"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MAX_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_NOISE_MNGR_THLD"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_NOISE_MNGR_THLD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_CALIBRATE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_CALIBRATE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_INPUT_GAIN"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_OUTPUT_GAIN"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_SRS_THEATERSOUND_HPF_FC"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HPF_FC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_EVO_MONOINPUT"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_MONOINPUT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_EVO_WIDENINGON"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_WIDENINGON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_EVO_ADD3DBON"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_ADD3DBON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_EVO_PCELEVEL"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_PCELEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_EVO_VLFELEVEL"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_VLFELEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_SYM_DEFAULT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_SYM_MODE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_SYM_LEVEL"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_DTS_ULTRATV_SYM_RESET"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_RESET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_AUDYSSEY_DYNAMICVOL_COMPRESS_MODE"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICVOL_COMPRESS_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_AUDYSSEY_DYNAMICVOL_GC"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICVOL_GC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_AUDYSSEY_DYNAMICVOL_VOLSETTING"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICVOL_VOLSETTING:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_AUDYSSEY_DYNAMICEQ_EQOFFSET"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICEQ_EQOFFSET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_AUDYSSEY_ABX_GWET"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_ABX_GWET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_AUDYSSEY_ABX_GDRY"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_ABX_GDRY:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "E_ADVSND_AUDYSSEY_ABX_FILSET"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_ABX_FILSET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_TSHD_INPUT_GAIN"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TSHD_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_TSHD_OUTPUT_GAIN"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TSHD_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_SURR_LEVEL_CONTROL"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_SURR_LEVEL_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_TRUBASS_COMPRESSOR_CONTROL"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TRUBASS_COMPRESSOR_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_TRUBASS_PROCESS_MODE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TRUBASS_PROCESS_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_TRUBASS_SPEAKER_AUDIO"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TRUBASS_SPEAKER_AUDIO:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_SPEAKER_ANALYSIS"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_SPEAKER_ANALYSIS:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE1"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE1:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE2"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE2:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE3"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE3:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE4"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE4:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE5"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE5:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE6"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE6:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE7"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE7:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE8"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE8:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE9"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE9:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE10"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE10:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE11"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE11:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE12"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE12:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE13"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE13:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE14"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE14:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE15"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE15:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE16"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE16:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE17"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE17:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE18"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE18:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE19"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE19:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE20"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE20:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE21"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE21:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE22"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE22:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE23"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE23:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE24"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE24:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE25"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE25:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE26"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE26:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE27"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE27:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE28"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE28:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE29"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE29:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const-string v1, "ADVSND_SRS_THEATERSOUND_RESERVE30"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE30:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    const/16 v0, 0x5a

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DOLBY_PL2VDPK_SMOD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DOLBY_PL2VDPK_WMOD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_DC_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_TRUBASS_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_INPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSXT_SET_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_INPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_OUTPUT_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_SURROUND_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_WOWSPACE_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_WOWCENTER_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_WOWHDSRS3DMODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_LIMITERCONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_TSHD_SET_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_SPEAKERSIZE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HARDLIMITER_BOOST_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HEADROOM_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_REF_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_MAX_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_NOISE_MNGR_THLD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_CALIBRATE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUVOLUME_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_HPF_FC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_MONOINPUT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_WIDENINGON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_ADD3DBON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_PCELEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_EVO_VLFELEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_LEVEL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_DTS_ULTRATV_SYM_RESET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICVOL_COMPRESS_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICVOL_GC:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICVOL_VOLSETTING:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_DYNAMICEQ_EQOFFSET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_ABX_GWET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_ABX_GDRY:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_AUDYSSEY_ABX_FILSET:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TSHD_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TSHD_OUTPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_SURR_LEVEL_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TRUBASS_COMPRESSOR_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TRUBASS_PROCESS_MODE:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TRUBASS_SPEAKER_AUDIO:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_SPEAKER_ANALYSIS:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE1:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE2:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE3:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE4:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE5:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE6:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE7:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE8:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE9:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE10:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE11:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE12:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE13:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE14:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE15:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE16:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE17:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE18:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE19:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE20:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE21:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE22:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE23:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE24:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE25:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE26:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE27:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE28:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE29:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_RESERVE30:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    return-object v0
.end method
