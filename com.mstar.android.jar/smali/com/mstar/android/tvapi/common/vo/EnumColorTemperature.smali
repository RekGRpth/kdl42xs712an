.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;
.super Ljava/lang/Enum;
.source "EnumColorTemperature.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

.field public static final enum E_COLOR_TEMP_COOL:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

.field public static final enum E_COLOR_TEMP_MAX:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

.field public static final enum E_COLOR_TEMP_MIN:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

.field public static final enum E_COLOR_TEMP_NATURE:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

.field public static final enum E_COLOR_TEMP_NUM:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

.field public static final enum E_COLOR_TEMP_USER:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

.field public static final enum E_COLOR_TEMP_WARM:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const-string v1, "E_COLOR_TEMP_MIN"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_MIN:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const-string v1, "E_COLOR_TEMP_COOL"

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_MIN:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->getValue()I

    move-result v2

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_COOL:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const-string v1, "E_COLOR_TEMP_NATURE"

    invoke-direct {v0, v1, v6, v5}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_NATURE:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const-string v1, "E_COLOR_TEMP_WARM"

    invoke-direct {v0, v1, v7, v6}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_WARM:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const-string v1, "E_COLOR_TEMP_USER"

    invoke-direct {v0, v1, v8, v7}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_USER:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const-string v1, "E_COLOR_TEMP_MAX"

    const/4 v2, 0x5

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_USER:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_MAX:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const-string v1, "E_COLOR_TEMP_NUM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v8}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_NUM:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_MIN:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_COOL:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_NATURE:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_WARM:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_USER:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_MAX:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->E_COLOR_TEMP_NUM:Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->value:I

    return v0
.end method
