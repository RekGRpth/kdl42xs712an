.class public Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;
.super Ljava/lang/Object;
.source "AdvancedSoundParameter.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public paramAudysseyAbxFilset:I

.field public paramAudysseyAbxGdry:I

.field public paramAudysseyAbxGwet:I

.field public paramAudysseyDynamicEqEqOffset:I

.field public paramAudysseyDynamicVolCompressMode:I

.field public paramAudysseyDynamicVolGc:I

.field public paramAudysseyDynamicVolVolSetting:I

.field public paramDolbyPl2vdpkSmod:I

.field public paramDolbyPl2vdpkWmod:I

.field public paramDtsUltraTvEvoAdd3dBon:I

.field public paramDtsUltraTvEvoMonoInput:I

.field public paramDtsUltraTvEvoPceLevel:I

.field public paramDtsUltraTvEvoVlfeLevel:I

.field public paramDtsUltraTvEvoWideningon:I

.field public paramDtsUltraTvSymDefault:I

.field public paramDtsUltraTvSymLevel:I

.field public paramDtsUltraTvSymMode:I

.field public paramDtsUltraTvSymReset:I

.field public paramSrsTheaterSoundDcControl:I

.field public paramSrsTheaterSoundDefinitionControl:I

.field public paramSrsTheaterSoundHardLimiterBoostGain:I

.field public paramSrsTheaterSoundHardLimiterLevel:I

.field public paramSrsTheaterSoundHeadRoomGain:I

.field public paramSrsTheaterSoundHpfFc:I

.field public paramSrsTheaterSoundInputGain:I

.field public paramSrsTheaterSoundSpeakerSize:I

.field public paramSrsTheaterSoundTruVolumeCalibrate:I

.field public paramSrsTheaterSoundTruVolumeInputGain:I

.field public paramSrsTheaterSoundTruVolumeMaxGain:I

.field public paramSrsTheaterSoundTruVolumeMode:I

.field public paramSrsTheaterSoundTruVolumeNoiseMngrThld:I

.field public paramSrsTheaterSoundTruVolumeOutputGain:I

.field public paramSrsTheaterSoundTruVolumeRefLevel:I

.field public paramSrsTheaterSoundTrubassControl:I

.field public paramSrsTheaterasoundSurrLevelControl:I

.field public paramSrsTheaterasoundTrubassCompressorControl:I

.field public paramSrsTheaterasoundTrubassProcessMode:I

.field public paramSrsTheaterasoundTrubassSpeakerAnalysis:I

.field public paramSrsTheaterasoundTrubassSpeakerAudio:I

.field public paramSrsTheaterasoundTshdInputGain:I

.field public paramSrsTheaterasoundTshdOnputGain:I

.field public paramSrsTshdSetDcControl:I

.field public paramSrsTshdSetDefinitionControl:I

.field public paramSrsTshdSetInputGain:I

.field public paramSrsTshdSetInputMode:I

.field public paramSrsTshdSetLimiterControl:I

.field public paramSrsTshdSetOutputGain:I

.field public paramSrsTshdSetOutputMode:I

.field public paramSrsTshdSetSpeakerSize:I

.field public paramSrsTshdSetSurroundLevel:I

.field public paramSrsTshdSetTrubassControl:I

.field public paramSrsTshdSetWowCenterControl:I

.field public paramSrsTshdSetWowHdSrs3dMode:I

.field public paramSrsTshdSetWowSpaceControl:I

.field public paramSrsTsxtSetDcGain:I

.field public paramSrsTsxtSetInputGain:I

.field public paramSrsTsxtSetInputMode:I

.field public paramSrsTsxtSetOutputGain:I

.field public paramSrsTsxtSetSpeakerSize:I

.field public paramSrsTsxtSetTrubassGain:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDolbyPl2vdpkSmod:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDolbyPl2vdpkWmod:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetInputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetDcGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetTrubassGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetSpeakerSize:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetInputMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetOutputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetInputMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetOutputMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetSpeakerSize:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetTrubassControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetDefinitionControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetDcControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetSurroundLevel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetInputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowSpaceControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowCenterControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowHdSrs3dMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetLimiterControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetOutputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundInputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDefinitionControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDcControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTrubassControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundSpeakerSize:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHardLimiterLevel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHardLimiterBoostGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHeadRoomGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeRefLevel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeMaxGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeNoiseMngrThld:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeCalibrate:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeInputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeOutputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHpfFc:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoMonoInput:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoWideningon:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoAdd3dBon:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoPceLevel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoVlfeLevel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymDefault:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymLevel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymReset:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolCompressMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolGc:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolVolSetting:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicEqEqOffset:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxGwet:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxGdry:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxFilset:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTshdInputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTshdOnputGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundSurrLevelControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassCompressorControl:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassProcessMode:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAudio:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAnalysis:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDolbyPl2vdpkSmod:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDolbyPl2vdpkWmod:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetInputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetDcGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetTrubassGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetSpeakerSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetInputMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetOutputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetInputMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetOutputMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetSpeakerSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetTrubassControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetDefinitionControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetDcControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetSurroundLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetInputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowSpaceControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowCenterControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowHdSrs3dMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetLimiterControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetOutputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundInputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDefinitionControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDcControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTrubassControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundSpeakerSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHardLimiterLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHardLimiterBoostGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHeadRoomGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeRefLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeMaxGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeNoiseMngrThld:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeCalibrate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeInputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeOutputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHpfFc:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoMonoInput:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoWideningon:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoAdd3dBon:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoPceLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoVlfeLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymDefault:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymLevel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymReset:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolCompressMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolGc:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolVolSetting:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicEqEqOffset:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxGwet:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxGdry:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxFilset:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTshdInputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTshdOnputGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundSurrLevelControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassCompressorControl:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassProcessMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAudio:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAnalysis:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDolbyPl2vdpkSmod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDolbyPl2vdpkWmod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetInputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetDcGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetTrubassGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetSpeakerSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetInputMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTsxtSetOutputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetInputMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetOutputMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetSpeakerSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetTrubassControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetDefinitionControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetDcControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetSurroundLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetInputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowSpaceControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowCenterControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetWowHdSrs3dMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetLimiterControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTshdSetOutputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundInputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDefinitionControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDcControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTrubassControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundSpeakerSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHardLimiterLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHardLimiterBoostGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHeadRoomGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeRefLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeMaxGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeNoiseMngrThld:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeCalibrate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeInputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTruVolumeOutputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundHpfFc:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoMonoInput:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoWideningon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoAdd3dBon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoPceLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvEvoVlfeLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymDefault:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramDtsUltraTvSymReset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolCompressMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolGc:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicVolVolSetting:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyDynamicEqEqOffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxGwet:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxGdry:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramAudysseyAbxFilset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTshdInputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTshdOnputGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundSurrLevelControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassCompressorControl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassProcessMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAudio:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAnalysis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
