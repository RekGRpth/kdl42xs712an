.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;
.super Ljava/lang/Enum;
.source "EnumProgramLoopType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_ALL:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_ALL_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_ATV_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_DTV_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

.field public static final enum E_PROG_LOOP_TYPE_MAX:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_ALL"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_ATV"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_DTV"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_DTV_TV"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_DTV_RADIO"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_DTV_DATA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_ALL_NO_CYCLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_ATV_NO_CYCLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ATV_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_DTV_NO_CYCLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const-string v1, "E_PROG_LOOP_TYPE_MAX"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_TYPE_MAX:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ATV_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_DTV_NO_CYCLE:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_TYPE_MAX:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    return-object v0
.end method
