.class public Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
.super Ljava/lang/Object;
.source "EpgEventTimerInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public checkSum:I

.field public durationTime:I

.field public enRepeatMode:S

.field public enTimerType:S

.field public eventID:I

.field public isEndTimeBeforeStart:Z

.field public majorNumber:I

.field public minorNumber:I

.field public serviceNumber:I

.field public serviceType:I

.field public startTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->checkSum:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->majorNumber:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->minorNumber:I

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->isEndTimeBeforeStart:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->checkSum:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->majorNumber:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->minorNumber:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->isEndTimeBeforeStart:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->checkSum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->majorNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->minorNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->isEndTimeBeforeStart:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
