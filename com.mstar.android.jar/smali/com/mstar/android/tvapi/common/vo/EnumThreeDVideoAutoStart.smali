.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;
.super Ljava/lang/Enum;
.source "EnumThreeDVideoAutoStart.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum E_ThreeD_Video_AUTOSTART_2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

.field public static final enum E_ThreeD_Video_AUTOSTART_3D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

.field public static final enum E_ThreeD_Video_AUTOSTART_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

.field public static final enum E_ThreeD_Video_AUTOSTART_OFF:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    const-string v1, "E_ThreeD_Video_AUTOSTART_OFF"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_OFF:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    const-string v1, "E_ThreeD_Video_AUTOSTART_2D"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    const-string v1, "E_ThreeD_Video_AUTOSTART_3D"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_3D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    const-string v1, "E_ThreeD_Video_AUTOSTART_COUNT"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_OFF:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_3D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->E_ThreeD_Video_AUTOSTART_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
