.class public Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;
.super Ljava/lang/Object;
.source "AudioOutParameter.java"


# instance fields
.field public spdifDelayTime:I

.field private spdifOutModeInUi:I

.field public speakerDelayTime:I

.field private spidfOutModActive:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getspdifOutModeInUi()Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spdifOutModeInUi:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->E_PCM:Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spdifOutModeInUi:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->E_NONPCM:Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spdifOutModeInUi:I

    aget-object v0, v0, v1

    return-object v0

    :cond_1
    new-instance v0, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v1, "getEnumSpdifType  failed"

    invoke-direct {v0, v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getspidfOutModActive()Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spidfOutModActive:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->E_PCM:Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spidfOutModActive:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->E_NONPCM:Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spidfOutModActive:I

    aget-object v0, v0, v1

    return-object v0

    :cond_1
    new-instance v0, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v1, "spidfOutModActive  failed"

    invoke-direct {v0, v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setspdifOutModeInUi(Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spdifOutModeInUi:I

    return-void
.end method

.method public setspidfOutModActive(Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->spidfOutModActive:I

    return-void
.end method
