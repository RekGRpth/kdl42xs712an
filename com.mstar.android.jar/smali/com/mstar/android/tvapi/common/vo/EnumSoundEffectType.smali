.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;
.super Ljava/lang/Enum;
.source "EnumSoundEffectType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_AVC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_BASS:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_DRC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_ECHO:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_NR:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_Surround:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

.field public static final enum E_TREBLE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_PRESCALE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_TREBLE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_TREBLE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_BASS"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_BASS:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_BALANCE"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_EQ"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_PEQ"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_AVC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_AVC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_Surround"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_Surround:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_DRC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_DRC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_NR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_NR:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const-string v1, "E_ECHO"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_ECHO:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_TREBLE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_BASS:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_AVC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_Surround:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_DRC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_NR:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_ECHO:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    return-object v0
.end method
