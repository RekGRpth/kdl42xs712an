.class public Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;
.super Ljava/lang/Object;
.source "DtvProgramSignalInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static enumhash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public amMode:S

.field private modulationMode:I

.field public networkName:Ljava/lang/String;

.field public quality:I

.field public rfNumber:S

.field public strength:I

.field public symbolRate:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->enumhash:Ljava/util/Hashtable;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->rfNumber:S

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->modulationMode:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->amMode:S

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->quality:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->strength:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->symbolRate:I

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->networkName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->rfNumber:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->modulationMode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->amMode:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->quality:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->strength:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->symbolRate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->networkName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000()Ljava/util/Hashtable;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->enumhash:Ljava/util/Hashtable;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getModulationMode()Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;
        }
    .end annotation

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->modulationMode:I

    invoke-static {v1}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->getOrdinalThroughValue(I)I

    move-result v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->values()[Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public setModulationMode(Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->modulationMode:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->rfNumber:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->modulationMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->amMode:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->quality:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->strength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->symbolRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->networkName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
