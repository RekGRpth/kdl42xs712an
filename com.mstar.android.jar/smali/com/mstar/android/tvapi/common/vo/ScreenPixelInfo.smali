.class public Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo;
.super Ljava/lang/Object;
.source "ScreenPixelInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;
    }
.end annotation


# instance fields
.field public bShowRepWin:Z

.field public enStage:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

.field public tmpStage:I

.field public u16BCbMax:S

.field public u16BCbMin:S

.field public u16GYMax:S

.field public u16GYMin:S

.field public u16RCrMax:S

.field public u16RCrMin:S

.field public u16RepWinColor:S

.field public u16ReportPixelInfo_Length:S

.field public u16XEnd:S

.field public u16XStart:S

.field public u16YEnd:S

.field public u16YStart:S

.field public u32BCbSum:J

.field public u32GYSum:J

.field public u32RCrSum:J

.field public u32ReportPixelInfo_Version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
