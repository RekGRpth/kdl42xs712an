.class public final enum Lcom/mstar/android/tvapi/common/AudioManager$EVENT;
.super Ljava/lang/Enum;
.source "AudioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/AudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/AudioManager$EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

.field public static final enum EV_AP_SETVOLUME_EVENT:Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

.field public static final enum EV_UNDEFINED:Lcom/mstar/android/tvapi/common/AudioManager$EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    const-string v1, "EV_AP_SETVOLUME_EVENT"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->EV_AP_SETVOLUME_EVENT:Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    const-string v1, "EV_UNDEFINED"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->EV_UNDEFINED:Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    sget-object v1, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->EV_AP_SETVOLUME_EVENT:Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->EV_UNDEFINED:Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/AudioManager$EVENT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/AudioManager$EVENT;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    return-object v0
.end method
