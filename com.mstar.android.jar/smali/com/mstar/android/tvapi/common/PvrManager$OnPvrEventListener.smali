.class public interface abstract Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
.super Ljava/lang/Object;
.source "PvrManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/PvrManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnPvrEventListener"
.end annotation


# virtual methods
.method public abstract onPvrNotifyFormatFinished(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
.end method

.method public abstract onPvrNotifyPlaybackBegin(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
.end method

.method public abstract onPvrNotifyPlaybackStop(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
.end method

.method public abstract onPvrNotifyUsbInserted(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
.end method

.method public abstract onPvrNotifyUsbRemoved(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
.end method
