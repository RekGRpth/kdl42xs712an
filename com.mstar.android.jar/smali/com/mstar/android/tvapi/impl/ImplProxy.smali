.class public Lcom/mstar/android/tvapi/impl/ImplProxy;
.super Ljava/lang/Object;
.source "ImplProxy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static getPlayerImplInstance(Ljava/lang/Object;)Lcom/mstar/android/tvapi/impl/PlayerImpl;
    .locals 1
    .param p0    # Ljava/lang/Object;

    invoke-static {p0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->getInstance(Ljava/lang/Object;)Lcom/mstar/android/tvapi/impl/PlayerImpl;

    move-result-object v0

    return-object v0
.end method

.method protected static getScanManagerImplInstance(Ljava/lang/Object;)Lcom/mstar/android/tvapi/impl/ScanManagerImpl;
    .locals 1
    .param p0    # Ljava/lang/Object;

    invoke-static {p0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->getInstance(Ljava/lang/Object;)Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    move-result-object v0

    return-object v0
.end method
