.class public Lcom/mstar/android/tvapi/impl/ScanManagerImpl;
.super Ljava/lang/Object;
.source "ScanManagerImpl.java"

# interfaces
.implements Lcom/mstar/android/tvapi/atv/AtvScanManager;
.implements Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;
.implements Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DvbtScanManager;


# static fields
.field private static final IEPG_MANAGER:Ljava/lang/String; = "mstar.IScanManagerImpl"

.field private static _scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;


# instance fields
.field private mNativeContext:I

.field private mScanManagerImplContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    :try_start_0
    const-string v1, "scanmanagerimpl_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load scanmanagerimpl_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setup(Ljava/lang/Object;)V

    return-void
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private final native _getRegionInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected static getInstance(Ljava/lang/Object;)Lcom/mstar/android/tvapi/impl/ScanManagerImpl;
    .locals 3
    .param p0    # Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mstar.android.tvapi.atv.AtvScanImplProxy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.mstar.android.tvapi.dtv.common.DtvScanImplProxy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.mstar.android.tvapi.common.TvScanImplProxy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    sget-object v1, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    if-nez v1, :cond_2

    const-class v2, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    if-nez v1, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;-><init>()V

    sput-object v1, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    sget-object v1, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private native native_finalize()V
.end method

.method private final native native_getAtvProgramInfo(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getNtscAntenna()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getProgramControl(III)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static native native_init()V
.end method

.method private final native native_setAtvProgramInfo(III)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setAutoTuningStart(IIII)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setBandwidth(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setCableOperator(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setManualTuningStart(III)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setNtscAntenna(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setProgramControl(III)Z
.end method

.method private final native native_setScanParam(SIIISZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 2
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "\n NativeScanManagerImpl callback"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    return-void
.end method

.method public getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_getAtvProgramInfo(II)I

    move-result v0

    return v0
.end method

.method public final native getAtvProgramMiscInfo(I)Lcom/mstar/android/tvapi/common/vo/AtvProgramData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getAtvStationName(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCurrentFrequency()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getDefaultHomingChannelFrequency()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getDefaultNetworkId()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getNtscAntenna()Lcom/mstar/android/tvapi/common/vo/EnumMedium;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_getNtscAntenna()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMedium;->MEDIUM_CABLE:Lcom/mstar/android/tvapi/common/vo/EnumMedium;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumMedium;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMedium;->MEDIUM_NUM:Lcom/mstar/android/tvapi/common/vo/EnumMedium;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumMedium;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getNtscAntenna failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumMedium;->values()[Lcom/mstar/android/tvapi/common/vo/EnumMedium;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getProgramControl(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;II)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_getProgramControl(III)I

    move-result v0

    return v0
.end method

.method public final getRegionInfo()Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    const-string v7, "mstar.IScanManagerImpl"

    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-direct {p0, v5, v4}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_getRegionInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I

    new-instance v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;

    invoke-direct {v6}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;-><init>()V

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryCount:S

    iget-short v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryCount:S

    new-array v7, v7, [Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    iput-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    const/4 v0, 0x0

    :goto_0
    iget-short v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryCount:S

    if-ge v0, v7, :cond_4

    const/4 v1, 0x0

    :goto_1
    const/4 v7, 0x3

    if-ge v1, v7, :cond_0

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->countryCode:[C

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    int-to-char v8, v8

    aput-char v8, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionCount:I

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v8, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v8, v8, v0

    iget v8, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionCount:I

    new-array v8, v8, [Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    iput-object v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    const/4 v1, 0x0

    :goto_2
    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionCount:I

    if-ge v1, v7, :cond_3

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    int-to-short v8, v8

    iput-short v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->code:S

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    invoke-virtual {v4}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->name:Ljava/lang/String;

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionNum:I

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v8, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v8, v8, v0

    iget-object v8, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v8, v8, v1

    iget v8, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionNum:I

    new-array v8, v8, [Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    iput-object v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    const/4 v2, 0x0

    :goto_3
    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionNum:I

    if-ge v2, v7, :cond_2

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v7, v7, v2

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    int-to-short v8, v8

    iput-short v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->code:S

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v7, v7, v2

    invoke-virtual {v4}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->regionName:Ljava/lang/String;

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v7, v7, v2

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionNum:I

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v7, v7, v2

    iget-object v8, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v8, v8, v0

    iget-object v8, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v8, v8, v1

    iget-object v8, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v8, v8, v2

    iget v8, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionNum:I

    new-array v8, v8, [Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    iput-object v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    const/4 v3, 0x0

    :goto_4
    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v7, v7, v2

    iget v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionNum:I

    if-ge v3, v7, :cond_1

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v7, v7, v2

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    aget-object v7, v7, v3

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    int-to-short v8, v8

    iput v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;->code:I

    iget-object v7, v6, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTargetRegionInfo;->countryInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;

    aget-object v7, v7, v0

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v7, v7, v1

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->secondaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;

    aget-object v7, v7, v2

    iget-object v7, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    aget-object v7, v7, v3

    invoke-virtual {v4}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;->regionName:Ljava/lang/String;

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    return-object v6
.end method

.method public final native getRegionNetworks()Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getSmartScanMode()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isScanning()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native pauseScan()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->_scanmanagerimpl:Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    return-void
.end method

.method public final native resolveConflictLcn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native resumeScan()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;II)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setAtvProgramInfo(III)Z

    move-result v0

    return v0
.end method

.method public final native setAtvProgramMiscInfo(ILcom/mstar/android/tvapi/common/vo/AtvProgramData;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setAtvStationName(ILjava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setAutoTuningEnd()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setAutoTuningPause()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setAutoTuningResume()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setAutoTuningStart(IIILcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p4}, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->ordinal()I

    move-result v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setAutoTuningStart(IIII)Z

    move-result v0

    return v0
.end method

.method public setBandwidth(Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setBandwidth(I)Z

    move-result v0

    return v0
.end method

.method public setCableOperator(Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setCableOperator(I)V

    return-void
.end method

.method public final native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setManualTuningEnd()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p3}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->ordinal()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setManualTuningStart(III)Z

    move-result v0

    return v0
.end method

.method public setNtscAntenna(Lcom/mstar/android/tvapi/common/vo/EnumMedium;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumMedium;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumMedium;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setNtscAntenna(I)Z

    move-result v0

    return v0
.end method

.method public setProgramControl(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;II)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setProgramControl(III)Z

    move-result v0

    return v0
.end method

.method public final native setRegion(Ljava/lang/String;SSI)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IISZ)Z
    .locals 7
    .param p1    # S
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;
    .param p3    # I
    .param p4    # I
    .param p5    # S
    .param p6    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->ordinal()I

    move-result v2

    move-object v0, p0

    move v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/mstar/android/tvapi/impl/ScanManagerImpl;->native_setScanParam(SIIISZ)Z

    move-result v0

    return v0
.end method

.method public final native setSmartScanMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startAutoScan()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startAutoUpdateScan()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startFullScan()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startManualScan()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startNtscDirectTune(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startQuickScan()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startStandbyScan()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopScan()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
