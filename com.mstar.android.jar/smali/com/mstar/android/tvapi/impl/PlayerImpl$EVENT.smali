.class public final enum Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;
.super Ljava/lang/Enum;
.source "PlayerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/impl/PlayerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_ATV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_ATV_MANUAL_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_AUDIO_MODE_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_CHANGE_TTX_STATUS:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_CI_LOAD_CREDENTIAL_FAIL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_DTV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_DTV_AUTO_UPDATE_SCAN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_DTV_CHANNELNAME_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_DTV_PRI_COMPONENT_MISSING:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_DTV_PROGRAM_INFO_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_EPGTIMER_SIMULCAST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_EPG_UPDATE_LIST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_GINGA_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_HBBTV_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_HBBTV_UI_EVENT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_MAX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_MHEG5_EVENT_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_MHEG5_RETURN_KEY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_MHEG5_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_OAD_DOWNLOAD:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_OAD_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_OAD_TIMEOUT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_POPUP_DIALOG:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_CI_PLUS_PROTECTION:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_OVER_RUN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_PARENTAL_CONTROL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_RECORD_SIZE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_RECORD_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_RECORD_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_PVR_NOTIFY_USB_REMOVED:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_RCT_PRESENCE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

.field public static final enum EV_TS_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_DTV_CHANNELNAME_READY"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_CHANNELNAME_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_ATV_AUTO_TUNING_SCAN_INFO"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_ATV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_ATV_MANUAL_TUNING_SCAN_INFO"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_ATV_MANUAL_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_DTV_AUTO_TUNING_SCAN_INFO"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_DTV_PROGRAM_INFO_READY"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_PROGRAM_INFO_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_SIGNAL_LOCK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_SIGNAL_UNLOCK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_POPUP_DIALOG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_DIALOG:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_SCREEN_SAVER_MODE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_CI_LOAD_CREDENTIAL_FAIL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_CI_LOAD_CREDENTIAL_FAIL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_EPGTIMER_SIMULCAST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_EPGTIMER_SIMULCAST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_HBBTV_STATUS_MODE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_HBBTV_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_MHEG5_STATUS_MODE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_MHEG5_RETURN_KEY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_RETURN_KEY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_OAD_HANDLER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_OAD_DOWNLOAD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_DOWNLOAD:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_TIME"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_RECORD_TIME"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_RECORD_SIZE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_SIZE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_RECORD_STOP"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_STOP"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_BEGIN"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_OVER_RUN"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_OVER_RUN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_USB_REMOVED"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_USB_REMOVED:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_CI_PLUS_PROTECTION"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_CI_PLUS_PROTECTION:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_PARENTAL_CONTROL"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PARENTAL_CONTROL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_DTV_AUTO_UPDATE_SCAN"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_AUTO_UPDATE_SCAN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_TS_CHANGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_TS_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_RCT_PRESENCE"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_RCT_PRESENCE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_CHANGE_TTX_STATUS"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_CHANGE_TTX_STATUS:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_DTV_PRI_COMPONENT_MISSING"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_PRI_COMPONENT_MISSING:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_AUDIO_MODE_CHANGE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_AUDIO_MODE_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_MHEG5_EVENT_HANDLER"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_EVENT_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_OAD_TIMEOUT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_TIMEOUT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_GINGA_STATUS_MODE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_GINGA_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_HBBTV_UI_EVENT"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_HBBTV_UI_EVENT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_EPG_UPDATE_LIST"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_EPG_UPDATE_LIST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_PIP"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_POP"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_DUALVIEW"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_TRAVELINGMODE"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const-string v1, "EV_MAX"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    const/16 v0, 0x33

    new-array v0, v0, [Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_CHANNELNAME_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_ATV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_ATV_MANUAL_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_PROGRAM_INFO_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_DIALOG:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_CI_LOAD_CREDENTIAL_FAIL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_EPGTIMER_SIMULCAST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_HBBTV_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_RETURN_KEY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_DOWNLOAD:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_SIZE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_OVER_RUN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_USB_REMOVED:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_CI_PLUS_PROTECTION:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PARENTAL_CONTROL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_AUTO_UPDATE_SCAN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_TS_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_RCT_PRESENCE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_CHANGE_TTX_STATUS:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_PRI_COMPONENT_MISSING:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_AUDIO_MODE_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_EVENT_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_TIMEOUT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_GINGA_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_HBBTV_UI_EVENT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_EPG_UPDATE_LIST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    return-object v0
.end method
