.class public Lcom/mstar/android/MIntent;
.super Landroid/content/Intent;
.source "MIntent.java"


# static fields
.field public static final ACTION_3D_MODE_BUTTON:Ljava/lang/String; = "com.mstar.tv.service.3D"

.field public static final ACTION_ASPECT_RATIO_BUTTON:Ljava/lang/String; = "com.mstar.android.intent.action.ASPECT_RATIO_BUTTON"

.field public static final ACTION_CAMERA_PLUG_IN:Ljava/lang/String; = "com.mstar.android.intent.action.CAMERA_PLUG_IN"

.field public static final ACTION_CAMERA_PLUG_OUT:Ljava/lang/String; = "com.mstar.android.intent.action.CAMERA_PLUG_OUT"

.field public static final ACTION_MICROPHONE_PLUG_IN:Ljava/lang/String; = "com.mstar.android.intent.action.MICROPHONE_PLUG_IN"

.field public static final ACTION_MICROPHONE_PLUG_OUT:Ljava/lang/String; = "com.mstar.android.intent.action.MICROPHONE_PLUG_OUT"

.field public static final ACTION_NOTIFICATION_BUTTON:Ljava/lang/String; = "com.mstar.android.intent.action.NOTIFICATION_BUTTON"

.field public static final ACTION_PICTURE_MODE_BUTTON:Ljava/lang/String; = "com.mstar.android.intent.action.PICTURE_MODE_BUTTON"

.field public static final ACTION_SLEEP_BUTTON:Ljava/lang/String; = "com.mstar.android.intent.action.SLEEP_BUTTON"

.field public static final ACTION_SOUND_MODE_BUTTON:Ljava/lang/String; = "com.mstar.android.intent.action.SOUND_MODE_BUTTON"

.field public static final ACTION_TV_INPUT_BUTTON:Ljava/lang/String; = "com.mstar.android.intent.action.TV_INPUT_BUTTON"

.field public static final ACTION_TV_SETTING_BUTTON:Ljava/lang/String; = "com.mstar.android.intent.action.TV_SETTING_BUTTON"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    return-void
.end method
