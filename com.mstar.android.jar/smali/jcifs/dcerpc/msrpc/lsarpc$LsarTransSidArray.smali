.class public Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;
.super Ljcifs/dcerpc/ndr/NdrObject;
.source "lsarpc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljcifs/dcerpc/msrpc/lsarpc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LsarTransSidArray"
.end annotation


# instance fields
.field public count:I

.field public sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljcifs/dcerpc/ndr/NdrObject;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljcifs/dcerpc/ndr/NdrBuffer;)V
    .locals 6
    .param p1    # Ljcifs/dcerpc/ndr/NdrBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/ndr/NdrException;
        }
    .end annotation

    const/4 v4, 0x4

    invoke-virtual {p1, v4}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    invoke-virtual {p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v4

    iput v4, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->count:I

    invoke-virtual {p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v2

    if-eqz v2, :cond_4

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    invoke-virtual {p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v3

    iget v1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v4, v3, 0xc

    invoke-virtual {p1, v4}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    iget-object v4, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    if-nez v4, :cond_2

    if-ltz v3, :cond_0

    const v4, 0xffff

    if-le v3, v4, :cond_1

    :cond_0
    new-instance v4, Ljcifs/dcerpc/ndr/NdrException;

    const-string v5, "invalid array conformance"

    invoke-direct {v4, v5}, Ljcifs/dcerpc/ndr/NdrException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    new-array v4, v3, [Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    iput-object v4, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    :cond_2
    invoke-virtual {p1, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_4

    iget-object v4, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    aget-object v4, v4, v0

    if-nez v4, :cond_3

    iget-object v4, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    new-instance v5, Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    invoke-direct {v5}, Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;-><init>()V

    aput-object v5, v4, v0

    :cond_3
    iget-object v4, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    aget-object v4, v4, v0

    invoke-virtual {v4, p1}, Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;->decode(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public encode(Ljcifs/dcerpc/ndr/NdrBuffer;)V
    .locals 5
    .param p1    # Ljcifs/dcerpc/ndr/NdrBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/ndr/NdrException;
        }
    .end annotation

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    iget v3, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->count:I

    invoke-virtual {p1, v3}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget-object v3, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_referent(Ljava/lang/Object;I)V

    iget-object v3, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    if-eqz v3, :cond_0

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    iget v2, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->count:I

    invoke-virtual {p1, v2}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget v1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v3, v2, 0xc

    invoke-virtual {p1, v3}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    invoke-virtual {p1, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedSid;->encode(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
