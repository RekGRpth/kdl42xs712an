.class abstract Ljcifs/smb/SmbComNtTransactionResponse;
.super Ljcifs/smb/SmbComTransactionResponse;
.source "SmbComNtTransactionResponse.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljcifs/smb/SmbComTransactionResponse;-><init>()V

    return-void
.end method


# virtual methods
.method readParameterWordsWireFormat([BI)I
    .locals 5
    .param p1    # [B
    .param p2    # I

    const/4 v2, 0x0

    move v1, p2

    add-int/lit8 v0, p2, 0x1

    aput-byte v2, p1, p2

    add-int/lit8 p2, v0, 0x1

    aput-byte v2, p1, v0

    add-int/lit8 v0, p2, 0x1

    aput-byte v2, p1, p2

    invoke-static {p1, v0}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->totalParameterCount:I

    iget v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->bufDataStart:I

    if-nez v2, :cond_0

    iget v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->totalParameterCount:I

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->bufDataStart:I

    :cond_0
    add-int/lit8 p2, v0, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->totalDataCount:I

    add-int/lit8 p2, p2, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->parameterCount:I

    add-int/lit8 p2, p2, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->parameterOffset:I

    add-int/lit8 p2, p2, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->parameterDisplacement:I

    add-int/lit8 p2, p2, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->dataCount:I

    add-int/lit8 p2, p2, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->dataOffset:I

    add-int/lit8 p2, p2, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/SmbComNtTransactionResponse;->readInt4([BI)I

    move-result v2

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->dataDisplacement:I

    add-int/lit8 p2, p2, 0x4

    aget-byte v2, p1, p2

    and-int/lit16 v2, v2, 0xff

    iput v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->setupCount:I

    add-int/lit8 p2, p2, 0x2

    iget v2, p0, Ljcifs/smb/SmbComNtTransactionResponse;->setupCount:I

    if-eqz v2, :cond_1

    sget-object v2, Ljcifs/smb/SmbComNtTransactionResponse;->log:Ljcifs/util/LogStream;

    sget v2, Ljcifs/util/LogStream;->level:I

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    sget-object v2, Ljcifs/smb/SmbComNtTransactionResponse;->log:Ljcifs/util/LogStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setupCount is not zero: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Ljcifs/smb/SmbComNtTransactionResponse;->setupCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    :cond_1
    sub-int v2, p2, v1

    return v2
.end method
