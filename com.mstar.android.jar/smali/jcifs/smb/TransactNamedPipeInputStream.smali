.class Ljcifs/smb/TransactNamedPipeInputStream;
.super Ljcifs/smb/SmbFileInputStream;
.source "TransactNamedPipeInputStream.java"


# static fields
.field private static final INIT_PIPE_SIZE:I = 0x1000


# instance fields
.field private beg_idx:I

.field private dcePipe:Z

.field lock:Ljava/lang/Object;

.field private nxt_idx:I

.field private pipe_buf:[B

.field private used:I


# direct methods
.method constructor <init>(Ljcifs/smb/SmbNamedPipe;)V
    .locals 2
    .param p1    # Ljcifs/smb/SmbNamedPipe;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;,
            Ljava/net/MalformedURLException;,
            Ljava/net/UnknownHostException;
        }
    .end annotation

    iget v0, p1, Ljcifs/smb/SmbNamedPipe;->pipeType:I

    const v1, -0xff01

    and-int/2addr v0, v1

    or-int/lit8 v0, v0, 0x20

    invoke-direct {p0, p1, v0}, Ljcifs/smb/SmbFileInputStream;-><init>(Ljcifs/smb/SmbFile;I)V

    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v0, p1, Ljcifs/smb/SmbNamedPipe;->pipeType:I

    and-int/lit16 v0, v0, 0x600

    const/16 v1, 0x600

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ljcifs/smb/TransactNamedPipeInputStream;->dcePipe:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljcifs/smb/TransactNamedPipeInputStream;->lock:Ljava/lang/Object;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Ljcifs/smb/TransactNamedPipeInputStream;->file:Ljcifs/smb/SmbFile;

    sget-object v0, Ljcifs/smb/SmbFile;->log:Ljcifs/util/LogStream;

    sget v0, Ljcifs/util/LogStream;->level:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Ljcifs/smb/TransactNamedPipeInputStream;->file:Ljcifs/smb/SmbFile;

    sget-object v0, Ljcifs/smb/SmbFile;->log:Ljcifs/util/LogStream;

    const-string v1, "Named Pipe available() does not apply to TRANSACT Named Pipes"

    invoke-virtual {v0, v1}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public dce_read([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1, p2, p3}, Ljcifs/smb/SmbFileInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, -0x1

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->lock:Ljava/lang/Object;

    monitor-enter v3

    :goto_0
    :try_start_0
    iget v2, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    if-nez v2, :cond_0

    iget-object v2, p0, Ljcifs/smb/TransactNamedPipeInputStream;->lock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_0
    :try_start_2
    iget-object v2, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    aget-byte v2, v2, v4

    and-int/lit16 v1, v2, 0xff

    iget v2, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    add-int/lit8 v2, v2, 0x1

    iget-object v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v4, v4

    rem-int/2addr v2, v4

    iput v2, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return v1
.end method

.method public read([B)I
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Ljcifs/smb/TransactNamedPipeInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 7
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, -0x1

    if-gtz p3, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->lock:Ljava/lang/Object;

    monitor-enter v4

    :goto_1
    :try_start_0
    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    if-nez v3, :cond_1

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->lock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    new-instance v3, Ljava/io/IOException;

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_1
    :try_start_2
    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v3, v3

    iget v5, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    sub-int v0, v3, v5

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    if-le p3, v3, :cond_2

    iget v2, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    :goto_2
    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    if-le v3, v0, :cond_3

    if-le v2, v0, :cond_3

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v5, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    invoke-static {v3, v5, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    const/4 v5, 0x0

    sub-int v6, v2, v0

    invoke-static {v3, v5, p1, p2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_3
    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    sub-int/2addr v3, v2

    iput v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    add-int/2addr v3, v2

    iget-object v5, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v5, v5

    rem-int/2addr v3, v5

    iput v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    monitor-exit v4

    move v3, v2

    goto :goto_0

    :cond_2
    move v2, p3

    goto :goto_2

    :cond_3
    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v5, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    invoke-static {v3, v5, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method receive([BII)I
    .locals 7
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/4 v6, 0x0

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v3, v3

    iget v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    sub-int/2addr v3, v4

    if-le p3, v3, :cond_1

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v3, v3

    mul-int/lit8 v1, v3, 0x2

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    sub-int v3, v1, v3

    if-le p3, v3, :cond_0

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    add-int v1, p3, v3

    :cond_0
    iget-object v2, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    new-array v3, v1, [B

    iput-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v3, v2

    iget v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    sub-int v0, v3, v4

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    if-le v3, v0, :cond_2

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    iget-object v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    invoke-static {v2, v3, v4, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    sub-int/2addr v4, v0

    invoke-static {v2, v6, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    iput v6, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    iput v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->nxt_idx:I

    :cond_1
    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v3, v3

    iget v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->nxt_idx:I

    sub-int v0, v3, v4

    if-le p3, v0, :cond_3

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->nxt_idx:I

    invoke-static {p1, p2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    sub-int v4, p3, v0

    invoke-static {p1, p2, v3, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_1
    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->nxt_idx:I

    add-int/2addr v3, p3

    iget-object v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    array-length v4, v4

    rem-int/2addr v3, v4

    iput v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->nxt_idx:I

    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    add-int/2addr v3, p3

    iput v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    return p3

    :cond_2
    iget v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->beg_idx:I

    iget-object v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v5, p0, Ljcifs/smb/TransactNamedPipeInputStream;->used:I

    invoke-static {v2, v3, v4, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Ljcifs/smb/TransactNamedPipeInputStream;->pipe_buf:[B

    iget v4, p0, Ljcifs/smb/TransactNamedPipeInputStream;->nxt_idx:I

    invoke-static {p1, p2, v3, v4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method
