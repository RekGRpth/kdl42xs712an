.class Lcom/konka/appassistant/AppAssistantActivity$7;
.super Landroid/os/Handler;
.source "AppAssistantActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/AppAssistantActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1    # Landroid/os/Message;

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-static {v2, v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$21(Lcom/konka/appassistant/AppAssistantActivity;Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    :cond_1
    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->refreshView()V
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$7(Lcom/konka/appassistant/AppAssistantActivity;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->showViewList()V
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$22(Lcom/konka/appassistant/AppAssistantActivity;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    goto :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->requestFocus()Z

    :goto_1
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    goto :goto_1

    :sswitch_6
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const v3, 0x7f060011    # com.konka.appassistant.R.string.app_assistant_zipinstall_zip_status

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v4

    iget v4, v4, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurrentIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$24(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$25(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ScrollView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$26(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallStatus:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$28(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$26(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$29(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$30(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const v3, 0x7f060013    # com.konka.appassistant.R.string.app_assistant_zipinstall_zip_failed

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$9(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$26(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$29(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$30(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const v3, 0x7f060012    # com.konka.appassistant.R.string.app_assistant_zipinstall_zip_succeed

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$9(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$9(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$5(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const/4 v4, 0x5

    invoke-direct {v2, v3, v0, v4}, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;-><init>(Lcom/konka/appassistant/AppAssistantActivity;Landroid/net/Uri;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipAppInfo:Ljava/util/List;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$6(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    sget-object v4, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    const/4 v5, -0x1

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/konka/appassistant/ApplicationInfoView;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/konka/appassistant/ApplicationInfoView$ViewType;I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const v3, 0x7f060014    # com.konka.appassistant.R.string.app_assistant_zipinstall_install_status

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v4

    iget v4, v4, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurrentIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v4

    iget v4, v4, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mTotalApkNum:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$24(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$25(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$29(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$26(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$25(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ScrollView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallStatus:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$28(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$31(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationList;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v9}, Lcom/konka/appassistant/ApplicationList;->AddView(Lcom/konka/appassistant/ApplicationInfoView$ViewType;Landroid/net/Uri;Lcom/konka/appassistant/AppAssistantActivity$AppInfo;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$17(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_a
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$31(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationList;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v5, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v6}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v6

    iget v6, v6, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mErrorCode:I

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->getExplanationFromErrorCode(I)I
    invoke-static {v5, v6}, Lcom/konka/appassistant/AppAssistantActivity;->access$32(Lcom/konka/appassistant/AppAssistantActivity;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/konka/appassistant/ApplicationList;->ShowZipApkInstallResult(Ljava/lang/String;ILjava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    const/4 v3, 0x6

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v5, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v6}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v6

    iget v6, v6, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mErrorCode:I

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->getExplanationFromErrorCode(I)I
    invoke-static {v5, v6}, Lcom/konka/appassistant/AppAssistantActivity;->access$32(Lcom/konka/appassistant/AppAssistantActivity;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->ChangeZipInstallStatus(Ljava/lang/String;ILjava/lang/String;)V
    invoke-static {v1, v2, v3, v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$33(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$31(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationList;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    const/4 v3, 0x7

    invoke-virtual {v1, v2, v3, v9}, Lcom/konka/appassistant/ApplicationList;->ShowZipApkInstallResult(Ljava/lang/String;ILjava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    const/4 v3, 0x7

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->ChangeZipInstallStatus(Ljava/lang/String;ILjava/lang/String;)V
    invoke-static {v1, v2, v3, v9}, Lcom/konka/appassistant/AppAssistantActivity;->access$33(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;ILjava/lang/String;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallNum:I
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$34(Lcom/konka/appassistant/AppAssistantActivity;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$4(Lcom/konka/appassistant/AppAssistantActivity;I)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const v3, 0x7f060008    # com.konka.appassistant.R.string.app_assistant_second_menu_app_num_zipinstall

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$5(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallNum:I
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$34(Lcom/konka/appassistant/AppAssistantActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$24(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallStatus:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$28(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$9(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$7;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-virtual {v1}, Lcom/konka/appassistant/AppAssistantActivity;->ScrollToEnd()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_6
        0x3 -> :sswitch_7
        0x4 -> :sswitch_8
        0x5 -> :sswitch_9
        0x6 -> :sswitch_a
        0x7 -> :sswitch_b
        0x8 -> :sswitch_0
        0xa -> :sswitch_c
        0x64 -> :sswitch_4
        0x65 -> :sswitch_5
        0x66 -> :sswitch_3
        0x67 -> :sswitch_d
        0x68 -> :sswitch_0
    .end sparse-switch
.end method
