.class Lcom/konka/appassistant/ApplicationInfoView$4;
.super Ljava/lang/Object;
.source "ApplicationInfoView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/ApplicationInfoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/ApplicationInfoView;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/ApplicationInfoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v4, -0x356382

    const v3, -0xcdcdce

    const v2, -0x87e2

    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$8(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$9(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$10(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    instance-of v0, p1, Landroid/widget/Button;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/widget/Button;

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$11(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$11(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$12(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x7f020000    # com.konka.appassistant.R.drawable.app_chosen

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$12(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020007    # com.konka.appassistant.R.drawable.null_button_bg

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    instance-of v0, p1, Landroid/widget/Button;

    if-eqz v0, :cond_4

    check-cast p1, Landroid/widget/Button;

    const v0, -0x20242e

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    :cond_4
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$0(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$13(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$8(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$9(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$10(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$11(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$11(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$8(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$9(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$10(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    const v1, -0x717172

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$11(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$4;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$11(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0
.end method
