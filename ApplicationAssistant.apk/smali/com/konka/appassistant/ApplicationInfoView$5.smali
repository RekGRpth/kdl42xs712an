.class Lcom/konka/appassistant/ApplicationInfoView$5;
.super Ljava/lang/Object;
.source "ApplicationInfoView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/appassistant/ApplicationInfoView;->OpenDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/ApplicationInfoView;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/ApplicationInfoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView$5;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView$5;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/konka/appassistant/ApplicationInfoView;->access$5(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView$5;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/konka/appassistant/ApplicationInfoView;->access$5(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView$5;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/konka/appassistant/ApplicationInfoView;->access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " delete fail!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView$5;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/konka/appassistant/ApplicationInfoView;->access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Lcom/konka/appassistant/AppAssistantActivity;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView$5;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/konka/appassistant/ApplicationInfoView;->access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView$5;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I
    invoke-static {v4}, Lcom/konka/appassistant/ApplicationInfoView;->access$4(Lcom/konka/appassistant/ApplicationInfoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/konka/appassistant/AppAssistantActivity;->removeInstallList(I)V

    :cond_1
    return-void
.end method
