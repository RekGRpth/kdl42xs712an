.class public Lcom/konka/appassistant/KKApkUnzip;
.super Ljava/lang/Object;
.source "KKApkUnzip.java"


# static fields
.field public static final ERROR_CODE_KKAPKFILE_NOT_FOUND:I = 0x1

.field public static final ERROR_CODE_KKAPKFILE_UNZIP_FAILED:I = 0x2

.field public static final ERROR_CODE_KKAPKFILE_UNZIP_HAVE_NO_APK:I = 0x3

.field public static final ERROR_CODE_SUCCESS:I = 0x0

.field private static final KKAPK_DIR_NAME:Ljava/lang/String; = "kkapk"

.field private static final KKAPK_SUFFIX:Ljava/lang/String; = ".kkapk"

.field private static final KKAPK_UNZIP_FOLDER:Ljava/lang/String; = "apks_tmp"

.field private static final TAG:Ljava/lang/String; = "KKApkUnzip"


# instance fields
.field mContext:Landroid/content/Context;

.field private mErrorCode:I

.field private mUnzipApksTmpFolder:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/appassistant/KKApkUnzip;->mContext:Landroid/content/Context;

    return-void
.end method

.method private searchKkapkFile()Ljava/lang/String;
    .locals 7

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/konka/appassistant/KKApkUnzip;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/konka/appassistant/ApkScan;->getAllExternalStoragePath(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    :cond_0
    move-object v3, v4

    :goto_1
    return-object v3

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "kkapk"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, ".kkapk"

    const/4 v6, 0x1

    invoke-static {v3, v5, v6, v4}, Lcom/konka/appassistant/ApkScan;->scanDir(Ljava/lang/String;Ljava/lang/String;ZLjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private unZipKkapkBySysCmd(Ljava/lang/String;)Z
    .locals 12
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x0

    if-nez p1, :cond_0

    const-string v9, "KKApkUnzip"

    const-string v10, "filePath is null"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v8

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "KKApkUnzip"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "filePath="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is not exist!!"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "busybox unzip"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    :try_start_0
    invoke-virtual {v6, v2}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    :goto_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    if-nez v4, :cond_2

    :goto_2
    const-string v8, "KKApkUnzip"

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "/n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public clearUnzipApks()V
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/KKApkUnzip;->mUnzipApksTmpFolder:Ljava/lang/String;

    invoke-static {v0}, Lcom/konka/appassistant/FileOperation;->deleteDirectory(Ljava/lang/String;)Z

    return-void
.end method

.method public getApkListFromKkapk()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/konka/appassistant/KKApkUnzip;->searchKkapkFile()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    iput v7, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    move-object v1, v6

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    :try_start_0
    invoke-static {v5, v7, v8}, Lcom/konka/appassistant/ZipApi;->apacheGetZipFileList(Ljava/lang/String;ZZ)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_3

    const/4 v4, 0x0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v6, ".apk"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v7, 0x2

    iput v7, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    move-object v1, v6

    goto :goto_0

    :cond_3
    const/4 v7, 0x3

    iput v7, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    move-object v1, v6

    goto :goto_0
.end method

.method public getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    return v0
.end method

.method public getUnzipApksTmpFolder()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/KKApkUnzip;->mUnzipApksTmpFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getkkapkFile()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/konka/appassistant/KKApkUnzip;->searchKkapkFile()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public kkapkFileExists()Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/appassistant/KKApkUnzip;->searchKkapkFile()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public unzipKkapkFiles()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v7, 0x0

    iput v12, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    invoke-direct {p0}, Lcom/konka/appassistant/KKApkUnzip;->searchKkapkFile()Ljava/lang/String;

    move-result-object v4

    const-string v8, "KKApkUnzip"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "kkapkFile = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v4, :cond_0

    iput v13, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_0
    const/16 v8, 0x2f

    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {v4, v12, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "apks_tmp"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/appassistant/KKApkUnzip;->mUnzipApksTmpFolder:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-string v8, "KKApkUnzip"

    const-string v9, "unzip begin ......."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {v4, v1}, Lcom/konka/appassistant/ZipApi;->apacheUnzipFolder(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v8, "KKApkUnzip"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Time spends on unzip == "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v5

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, ".apk"

    invoke-static {v1, v8, v13, v7}, Lcom/konka/appassistant/ApkScan;->scanDir(Ljava/lang/String;Ljava/lang/String;ZLjava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_2

    :cond_1
    const/4 v8, 0x3

    iput v8, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    move-object v0, v7

    goto :goto_0

    :catch_0
    move-exception v2

    const/4 v8, 0x2

    iput v8, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    move-object v0, v7

    goto :goto_0

    :cond_2
    iput v12, p0, Lcom/konka/appassistant/KKApkUnzip;->mErrorCode:I

    goto :goto_0
.end method
