.class Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;
.super Landroid/content/BroadcastReceiver;
.source "OneKeyInstallProcess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/OneKeyInstallProcess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SystemPackageAddedMsgReveicer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/OneKeyInstallProcess;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/OneKeyInstallProcess;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    # getter for: Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z
    invoke-static {}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$0()Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SystemPackageAddedMsgReveicer.onReceive:android.intent.action.PACKAGE_ADDED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PackageName  = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurPackageInfo:Landroid/content/pm/PackageParser$Package;

    iget-object v1, v1, Landroid/content/pm/PackageParser$Package;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget v4, v4, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->onInstallOneApkFinished(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
