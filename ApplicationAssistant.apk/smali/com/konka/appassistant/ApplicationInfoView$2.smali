.class Lcom/konka/appassistant/ApplicationInfoView$2;
.super Ljava/lang/Object;
.source "ApplicationInfoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/ApplicationInfoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/ApplicationInfoView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I
    .locals 3

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$2;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->values()[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/appassistant/ApplicationInfoView$2;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/konka/appassistant/ApplicationInfoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView$2;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v2

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v3}, Lcom/konka/appassistant/ApplicationInfoView;->access$3(Lcom/konka/appassistant/ApplicationInfoView;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/konka/appassistant/ApplicationInfoView;->access$5(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/konka/appassistant/ApplicationInfoView;->access$5(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "application/vnd.android.package-archive"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/konka/appassistant/ApplicationInfoView;->access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;
    invoke-static {v2}, Lcom/konka/appassistant/ApplicationInfoView;->access$6(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "package:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;
    invoke-static {v3}, Lcom/konka/appassistant/ApplicationInfoView;->access$6(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.DELETE"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView$2;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/konka/appassistant/ApplicationInfoView;->access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
