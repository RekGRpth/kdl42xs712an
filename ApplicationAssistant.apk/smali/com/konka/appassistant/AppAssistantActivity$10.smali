.class Lcom/konka/appassistant/AppAssistantActivity$10;
.super Ljava/lang/Object;
.source "AppAssistantActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/appassistant/AppAssistantActivity;->refreshView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x1

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->IsThreadRun:Ljava/lang/Boolean;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$1(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/konka/appassistant/AppAssistantActivity;->access$45(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-static {v4}, Lcom/konka/appassistant/ApkScan;->getAllExterbalApk(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/konka/appassistant/AppAssistantActivity;->access$18(Lcom/konka/appassistant/AppAssistantActivity;Ljava/util/List;)V

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v4

    sget-object v5, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v4, v4, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/konka/appassistant/AppAssistantActivity;->access$45(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V

    :cond_2
    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lt v0, v4, :cond_4

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$17(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstallComparator:Ljava/util/Comparator;
    invoke-static {v5}, Lcom/konka/appassistant/AppAssistantActivity;->access$46(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/Comparator;

    move-result-object v5

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_4
    new-instance v5, Ljava/io/File;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/appassistant/PackageUtil;->getApplicationInfo(Landroid/net/Uri;)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v4, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$10;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;
    invoke-static {v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$17(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
