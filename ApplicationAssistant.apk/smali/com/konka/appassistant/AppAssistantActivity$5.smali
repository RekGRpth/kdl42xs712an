.class Lcom/konka/appassistant/AppAssistantActivity$5;
.super Ljava/lang/Object;
.source "AppAssistantActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/AppAssistantActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$5;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/net/Uri;Landroid/net/Uri;)I
    .locals 12
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/net/Uri;

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v8, -0x1

    invoke-static {p1}, Lcom/konka/appassistant/PackageUtil;->getPackageInfo(Landroid/net/Uri;)Landroid/content/pm/PackageParser$Package;

    move-result-object v4

    invoke-static {p2}, Lcom/konka/appassistant/PackageUtil;->getPackageInfo(Landroid/net/Uri;)Landroid/content/pm/PackageParser$Package;

    move-result-object v5

    if-nez v4, :cond_1

    if-nez v5, :cond_1

    move v6, v7

    :cond_0
    :goto_0
    return v6

    :cond_1
    if-nez v4, :cond_2

    move v6, v8

    goto :goto_0

    :cond_2
    if-nez v5, :cond_3

    move v6, v9

    goto :goto_0

    :cond_3
    iget-object v10, p0, Lcom/konka/appassistant/AppAssistantActivity$5;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v11, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {v10, v11, p1}, Lcom/konka/appassistant/PackageUtil;->getAppSnippet(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Landroid/net/Uri;)Lcom/konka/appassistant/PackageUtil$AppSnippet;

    move-result-object v2

    iget-object v10, p0, Lcom/konka/appassistant/AppAssistantActivity$5;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v11, v5, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {v10, v11, p2}, Lcom/konka/appassistant/PackageUtil;->getAppSnippet(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Landroid/net/Uri;)Lcom/konka/appassistant/PackageUtil$AppSnippet;

    move-result-object v3

    if-nez v2, :cond_4

    if-nez v3, :cond_4

    move v6, v7

    goto :goto_0

    :cond_4
    if-nez v2, :cond_5

    move v6, v8

    goto :goto_0

    :cond_5
    if-nez v3, :cond_6

    move v6, v9

    goto :goto_0

    :cond_6
    iget-object v10, v2, Lcom/konka/appassistant/PackageUtil$AppSnippet;->label:Ljava/lang/CharSequence;

    if-nez v10, :cond_7

    iget-object v10, v4, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v11, p0, Lcom/konka/appassistant/AppAssistantActivity$5;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;
    invoke-static {v11}, Lcom/konka/appassistant/AppAssistantActivity;->access$19(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/content/pm/PackageManager;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    check-cast v0, Ljava/lang/String;

    iget-object v10, v3, Lcom/konka/appassistant/PackageUtil$AppSnippet;->label:Ljava/lang/CharSequence;

    if-nez v10, :cond_8

    iget-object v10, v5, Landroid/content/pm/PackageParser$Package;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v11, p0, Lcom/konka/appassistant/AppAssistantActivity$5;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;
    invoke-static {v11}, Lcom/konka/appassistant/AppAssistantActivity;->access$19(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/content/pm/PackageManager;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    :goto_2
    check-cast v1, Ljava/lang/String;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->sCollator:Ljava/text/Collator;
    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->access$20()Ljava/text/Collator;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_0

    iget-object v10, v4, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    if-nez v10, :cond_9

    iget-object v10, v5, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    if-nez v10, :cond_9

    move v6, v7

    goto :goto_0

    :cond_7
    iget-object v0, v2, Lcom/konka/appassistant/PackageUtil$AppSnippet;->label:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_8
    iget-object v1, v3, Lcom/konka/appassistant/PackageUtil$AppSnippet;->label:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_9
    iget-object v7, v4, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    if-nez v7, :cond_a

    move v6, v8

    goto :goto_0

    :cond_a
    iget-object v7, v5, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    if-nez v7, :cond_b

    move v6, v9

    goto :goto_0

    :cond_b
    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->sCollator:Ljava/text/Collator;
    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->access$20()Ljava/text/Collator;

    move-result-object v7

    iget-object v8, v4, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    iget-object v9, v5, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Lcom/konka/appassistant/AppAssistantActivity$5;->compare(Landroid/net/Uri;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method
