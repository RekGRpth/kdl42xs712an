.class public Lorg/apache/tools/tar/TarUtils;
.super Ljava/lang/Object;
.source "TarUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computeCheckSum([B)J
    .locals 5
    .param p0    # [B

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_0

    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    int-to-long v3, v3

    add-long/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method public static getCheckSumOctalBytes(J[BII)I
    .locals 2
    .param p0    # J
    .param p2    # [B
    .param p3    # I
    .param p4    # I

    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/tools/tar/TarUtils;->getOctalBytes(J[BII)I

    add-int v0, p3, p4

    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0x20

    aput-byte v1, p2, v0

    add-int v0, p3, p4

    add-int/lit8 v0, v0, -0x2

    const/4 v1, 0x0

    aput-byte v1, p2, v0

    add-int v0, p3, p4

    return v0
.end method

.method public static getLongOctalBytes(J[BII)I
    .locals 3
    .param p0    # J
    .param p2    # [B
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x0

    add-int/lit8 v1, p4, 0x1

    new-array v0, v1, [B

    add-int/lit8 v1, p4, 0x1

    invoke-static {p0, p1, v0, v2, v1}, Lorg/apache/tools/tar/TarUtils;->getOctalBytes(J[BII)I

    invoke-static {v0, v2, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int v1, p3, p4

    return v1
.end method

.method public static getNameBytes(Ljava/lang/StringBuffer;[BII)I
    .locals 3
    .param p0    # Ljava/lang/StringBuffer;
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    add-int v1, p2, v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    if-ge v0, p3, :cond_1

    add-int v1, p2, v0

    const/4 v2, 0x0

    aput-byte v2, p1, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int v1, p2, p3

    return v1
.end method

.method public static getOctalBytes(J[BII)I
    .locals 9
    .param p0    # J
    .param p2    # [B
    .param p3    # I
    .param p4    # I

    const-wide/16 v7, 0x0

    const/16 v6, 0x20

    add-int/lit8 v0, p4, -0x1

    add-int v3, p3, v0

    const/4 v4, 0x0

    aput-byte v4, p2, v3

    add-int/lit8 v0, v0, -0x1

    add-int v3, p3, v0

    aput-byte v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    cmp-long v3, p0, v7

    if-nez v3, :cond_1

    add-int v3, p3, v0

    const/16 v4, 0x30

    aput-byte v4, p2, v3

    add-int/lit8 v0, v0, -0x1

    :cond_0
    :goto_0
    if-ltz v0, :cond_2

    add-int v3, p3, v0

    aput-byte v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    move-wide v1, p0

    :goto_1
    if-ltz v0, :cond_0

    cmp-long v3, v1, v7

    if-lez v3, :cond_0

    add-int v3, p3, v0

    const-wide/16 v4, 0x7

    and-long/2addr v4, v1

    long-to-int v4, v4

    int-to-byte v4, v4

    add-int/lit8 v4, v4, 0x30

    int-to-byte v4, v4

    aput-byte v4, p2, v3

    const/4 v3, 0x3

    shr-long/2addr v1, v3

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    add-int v3, p3, p4

    return v3
.end method

.method public static parseName([BII)Ljava/lang/StringBuffer;
    .locals 4
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, p2}, Ljava/lang/StringBuffer;-><init>(I)V

    add-int v0, p1, p2

    move v1, p1

    :goto_0
    if-ge v1, v0, :cond_0

    aget-byte v3, p0, v1

    if-nez v3, :cond_1

    :cond_0
    return-object v2

    :cond_1
    aget-byte v3, p0, v1

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static parseOctal([BII)J
    .locals 10
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    const/16 v9, 0x20

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    add-int v0, p1, p2

    move v1, p1

    :goto_0
    if-ge v1, v0, :cond_0

    aget-byte v5, p0, v1

    if-nez v5, :cond_1

    :cond_0
    return-wide v2

    :cond_1
    aget-byte v5, p0, v1

    if-eq v5, v9, :cond_2

    aget-byte v5, p0, v1

    const/16 v6, 0x30

    if-ne v5, v6, :cond_4

    :cond_2
    if-eqz v4, :cond_3

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    aget-byte v5, p0, v1

    if-eq v5, v9, :cond_0

    :cond_4
    const/4 v4, 0x0

    const/4 v5, 0x3

    shl-long v5, v2, v5

    aget-byte v7, p0, v1

    add-int/lit8 v7, v7, -0x30

    int-to-long v7, v7

    add-long v2, v5, v7

    goto :goto_1
.end method
