.class public Lorg/apache/tools/ant/util/Base64Converter;
.super Ljava/lang/Object;
.source "Base64Converter.java"


# static fields
.field private static final ALPHABET:[C

.field public static final alphabet:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x40

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    sget-object v0, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    sput-object v0, Lorg/apache/tools/ant/util/Base64Converter;->alphabet:[C

    return-void

    :array_0
    .array-data 2
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
        0x47s
        0x48s
        0x49s
        0x4as
        0x4bs
        0x4cs
        0x4ds
        0x4es
        0x4fs
        0x50s
        0x51s
        0x52s
        0x53s
        0x54s
        0x55s
        0x56s
        0x57s
        0x58s
        0x59s
        0x5as
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
        0x67s
        0x68s
        0x69s
        0x6as
        0x6bs
        0x6cs
        0x6ds
        0x6es
        0x6fs
        0x70s
        0x71s
        0x72s
        0x73s
        0x74s
        0x75s
        0x76s
        0x77s
        0x78s
        0x79s
        0x7as
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x2bs
        0x2fs
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/Base64Converter;->encode([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public encode([B)Ljava/lang/String;
    .locals 12
    .param p1    # [B

    const/high16 v11, 0xfc0000

    const v10, 0x3f000

    const/16 v9, 0x3d

    array-length v7, p1

    add-int/lit8 v7, v7, -0x1

    div-int/lit8 v7, v7, 0x3

    add-int/lit8 v7, v7, 0x1

    mul-int/lit8 v7, v7, 0x4

    new-array v4, v7, [C

    const/4 v5, 0x0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v7, v2, 0x3

    array-length v8, p1

    if-gt v7, v8, :cond_0

    add-int/lit8 v3, v2, 0x1

    aget-byte v7, p1, v2

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v0, v7, 0x10

    add-int/lit8 v2, v3, 0x1

    aget-byte v7, p1, v3

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    or-int/2addr v0, v7

    add-int/lit8 v3, v2, 0x1

    aget-byte v7, p1, v2

    or-int/2addr v0, v7

    and-int v7, v0, v11

    shr-int/lit8 v1, v7, 0x12

    add-int/lit8 v6, v5, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v5

    and-int v7, v0, v10

    shr-int/lit8 v1, v7, 0xc

    add-int/lit8 v5, v6, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v6

    and-int/lit16 v7, v0, 0xfc0

    shr-int/lit8 v1, v7, 0x6

    add-int/lit8 v6, v5, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v5

    and-int/lit8 v1, v0, 0x3f

    add-int/lit8 v5, v6, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v6

    move v2, v3

    goto :goto_0

    :cond_0
    array-length v7, p1

    sub-int/2addr v7, v2

    const/4 v8, 0x2

    if-ne v7, v8, :cond_2

    aget-byte v7, p1, v2

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v0, v7, 0x10

    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p1, v7

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    or-int/2addr v0, v7

    and-int v7, v0, v11

    shr-int/lit8 v1, v7, 0x12

    add-int/lit8 v6, v5, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v5

    and-int v7, v0, v10

    shr-int/lit8 v1, v7, 0xc

    add-int/lit8 v5, v6, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v6

    and-int/lit16 v7, v0, 0xfc0

    shr-int/lit8 v1, v7, 0x6

    add-int/lit8 v6, v5, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v5

    add-int/lit8 v5, v6, 0x1

    aput-char v9, v4, v6

    :cond_1
    :goto_1
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    return-object v7

    :cond_2
    array-length v7, p1

    sub-int/2addr v7, v2

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    aget-byte v7, p1, v2

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v0, v7, 0x10

    and-int v7, v0, v11

    shr-int/lit8 v1, v7, 0x12

    add-int/lit8 v6, v5, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v5

    and-int v7, v0, v10

    shr-int/lit8 v1, v7, 0xc

    add-int/lit8 v5, v6, 0x1

    sget-object v7, Lorg/apache/tools/ant/util/Base64Converter;->ALPHABET:[C

    aget-char v7, v7, v1

    aput-char v7, v4, v6

    add-int/lit8 v6, v5, 0x1

    aput-char v9, v4, v5

    add-int/lit8 v5, v6, 0x1

    aput-char v9, v4, v6

    goto :goto_1
.end method
