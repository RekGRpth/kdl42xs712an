.class public abstract Lorg/apache/tools/ant/util/ContainerMapper;
.super Ljava/lang/Object;
.source "ContainerMapper.java"

# interfaces
.implements Lorg/apache/tools/ant/util/FileNameMapper;


# instance fields
.field private mappers:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/ContainerMapper;->mappers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public declared-synchronized add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    monitor-enter p0

    if-eq p0, p1, :cond_0

    :try_start_0
    instance-of v1, p1, Lorg/apache/tools/ant/util/ContainerMapper;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/util/ContainerMapper;

    move-object v1, v0

    invoke-virtual {v1, p0}, Lorg/apache/tools/ant/util/ContainerMapper;->contains(Lorg/apache/tools/ant/util/FileNameMapper;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Circular mapper containment condition detected"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_1
    :try_start_1
    iget-object v1, p0, Lorg/apache/tools/ant/util/ContainerMapper;->mappers:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public addConfigured(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/ContainerMapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public addConfiguredMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/ContainerMapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method protected declared-synchronized contains(Lorg/apache/tools/ant/util/FileNameMapper;)Z
    .locals 5
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/util/ContainerMapper;->mappers:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/util/FileNameMapper;

    move-object v0, v4

    check-cast v0, Lorg/apache/tools/ant/util/FileNameMapper;

    move-object v3, v0

    if-eq v3, p1, :cond_0

    instance-of v4, v3, Lorg/apache/tools/ant/util/ContainerMapper;

    if-eqz v4, :cond_1

    check-cast v3, Lorg/apache/tools/ant/util/ContainerMapper;

    invoke-virtual {v3, p1}, Lorg/apache/tools/ant/util/ContainerMapper;->contains(Lorg/apache/tools/ant/util/FileNameMapper;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_1
    or-int/2addr v1, v4

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized getMappers()Ljava/util/List;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/ContainerMapper;->mappers:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setTo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method
