.class public Lorg/apache/tools/ant/util/CollectionUtils;
.super Ljava/lang/Object;
.source "CollectionUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/util/CollectionUtils$CompoundEnumeration;,
        Lorg/apache/tools/ant/util/CollectionUtils$EmptyEnumeration;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static append(Ljava/util/Enumeration;Ljava/util/Enumeration;)Ljava/util/Enumeration;
    .locals 1
    .param p0    # Ljava/util/Enumeration;
    .param p1    # Ljava/util/Enumeration;

    new-instance v0, Lorg/apache/tools/ant/util/CollectionUtils$CompoundEnumeration;

    invoke-direct {v0, p0, p1}, Lorg/apache/tools/ant/util/CollectionUtils$CompoundEnumeration;-><init>(Ljava/util/Enumeration;Ljava/util/Enumeration;)V

    return-object v0
.end method

.method public static asEnumeration(Ljava/util/Iterator;)Ljava/util/Enumeration;
    .locals 1
    .param p0    # Ljava/util/Iterator;

    new-instance v0, Lorg/apache/tools/ant/util/CollectionUtils$1;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/util/CollectionUtils$1;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method public static asIterator(Ljava/util/Enumeration;)Ljava/util/Iterator;
    .locals 1
    .param p0    # Ljava/util/Enumeration;

    new-instance v0, Lorg/apache/tools/ant/util/CollectionUtils$2;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/util/CollectionUtils$2;-><init>(Ljava/util/Enumeration;)V

    return-object v0
.end method

.method public static equals(Ljava/util/Dictionary;Ljava/util/Dictionary;)Z
    .locals 8
    .param p0    # Ljava/util/Dictionary;
    .param p1    # Ljava/util/Dictionary;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Ljava/util/Dictionary;->size()I

    move-result v6

    invoke-virtual {p1}, Ljava/util/Dictionary;->size()I

    move-result v7

    if-eq v6, v7, :cond_4

    move v4, v5

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Ljava/util/Dictionary;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/Dictionary;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1}, Ljava/util/Dictionary;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    :cond_6
    move v4, v5

    goto :goto_0
.end method

.method public static equals(Ljava/util/Vector;Ljava/util/Vector;)Z
    .locals 1
    .param p0    # Ljava/util/Vector;
    .param p1    # Ljava/util/Vector;

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Ljava/util/Vector;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static putAll(Ljava/util/Dictionary;Ljava/util/Dictionary;)V
    .locals 3
    .param p0    # Ljava/util/Dictionary;
    .param p1    # Ljava/util/Dictionary;

    invoke-virtual {p1}, Ljava/util/Dictionary;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/Dictionary;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/util/Dictionary;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method
