.class public Lorg/apache/tools/ant/util/RegexpPatternMapper;
.super Ljava/lang/Object;
.source "RegexpPatternMapper.java"

# interfaces
.implements Lorg/apache/tools/ant/util/FileNameMapper;


# instance fields
.field private handleDirSep:Z

.field protected reg:Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

.field private regexpOptions:I

.field protected result:Ljava/lang/StringBuffer;

.field protected to:[C


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->reg:Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    iput-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->result:Ljava/lang/StringBuffer;

    iput-boolean v1, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->handleDirSep:Z

    iput v1, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->regexpOptions:I

    new-instance v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;-><init>()V

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->newRegexpMatcher()Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->reg:Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    return-void
.end method


# virtual methods
.method public mapFileName(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->handleDirSep:Z

    if-eqz v0, :cond_0

    const-string v0, "\\"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/16 v0, 0x5c

    const/16 v1, 0x2f

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->reg:Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->reg:Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    iget v1, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->regexpOptions:I

    invoke-interface {v0, p1, v1}, Lorg/apache/tools/ant/util/regexp/RegexpMatcher;->matches(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/RegexpPatternMapper;->replaceReferences(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method protected replaceReferences(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/String;

    const/16 v6, 0x5c

    const/4 v5, 0x0

    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->reg:Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    iget v4, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->regexpOptions:I

    invoke-interface {v3, p1, v4}, Lorg/apache/tools/ant/util/regexp/RegexpMatcher;->getGroups(Ljava/lang/String;I)Ljava/util/Vector;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    aget-char v3, v3, v0

    if-ne v3, v6, :cond_2

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    aget-char v3, v3, v0

    const/16 v4, 0xa

    invoke-static {v3, v4}, Ljava/lang/Character;->digit(CI)I

    move-result v2

    const/4 v3, -0x1

    if-le v2, v3, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->result:Ljava/lang/StringBuffer;

    iget-object v4, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    aget-char v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->result:Ljava/lang/StringBuffer;

    iget-object v4, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    aget-char v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public setCaseSensitive(Z)V
    .locals 1
    .param p1    # Z

    if-nez p1, :cond_0

    const/16 v0, 0x100

    iput v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->regexpOptions:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->regexpOptions:I

    goto :goto_0
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->reg:Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    invoke-interface {v1, p1}, Lorg/apache/tools/ant/util/regexp/RegexpMatcher;->setPattern(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "Cannot load regular expression matcher"

    invoke-direct {v1, v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setHandleDirSep(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->handleDirSep:Z

    return-void
.end method

.method public setTo(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/RegexpPatternMapper;->to:[C

    return-void
.end method
