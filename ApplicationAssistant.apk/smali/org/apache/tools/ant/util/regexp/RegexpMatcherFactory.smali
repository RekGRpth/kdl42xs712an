.class public Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;
.super Ljava/lang/Object;
.source "RegexpMatcherFactory.java"


# static fields
.field static class$org$apache$tools$ant$util$regexp$RegexpMatcher:Ljava/lang/Class;

.field static class$org$apache$tools$ant$util$regexp$RegexpMatcherFactory:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static orCause(Ljava/lang/Throwable;Lorg/apache/tools/ant/BuildException;Z)Ljava/lang/Throwable;
    .locals 2
    .param p0    # Ljava/lang/Throwable;
    .param p1    # Lorg/apache/tools/ant/BuildException;
    .param p2    # Z

    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildException;->getException()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz p2, :cond_1

    instance-of v1, v0, Ljava/lang/ClassNotFoundException;

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method protected createInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    sget-object v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$org$apache$tools$ant$util$regexp$RegexpMatcherFactory:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.util.regexp.RegexpMatcherFactory"

    invoke-static {v0}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$org$apache$tools$ant$util$regexp$RegexpMatcherFactory:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    sget-object v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$org$apache$tools$ant$util$regexp$RegexpMatcher:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "org.apache.tools.ant.util.regexp.RegexpMatcher"

    invoke-static {v0}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$org$apache$tools$ant$util$regexp$RegexpMatcher:Ljava/lang/Class;

    :goto_1
    invoke-static {p1, v1, v0}, Lorg/apache/tools/ant/util/ClasspathUtils;->newInstance(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$org$apache$tools$ant$util$regexp$RegexpMatcherFactory:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v0, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->class$org$apache$tools$ant$util$regexp$RegexpMatcher:Ljava/lang/Class;

    goto :goto_1
.end method

.method public newRegexpMatcher()Lorg/apache/tools/ant/util/regexp/RegexpMatcher;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->newRegexpMatcher(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    move-result-object v0

    return-object v0
.end method

.method public newRegexpMatcher(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;
    .locals 7
    .param p1    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    const-string v3, "ant.regexp.regexpimpl"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->createInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    const-string v3, "ant.regexp.regexpimpl"

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    const-string v3, "java.util.regex.Matcher"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->testAvailability(Ljava/lang/String;)V

    const-string v3, "org.apache.tools.ant.util.regexp.Jdk14RegexpMatcher"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->createInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJavaVersionNumber()I

    move-result v3

    const/16 v5, 0xe

    if-ge v3, v5, :cond_2

    move v3, v4

    :goto_2
    invoke-static {v1, v0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->orCause(Ljava/lang/Throwable;Lorg/apache/tools/ant/BuildException;Z)Ljava/lang/Throwable;

    move-result-object v1

    :try_start_1
    const-string v3, "org.apache.oro.text.regex.Pattern"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->testAvailability(Ljava/lang/String;)V

    const-string v3, "org.apache.tools.ant.util.regexp.JakartaOroMatcher"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->createInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;
    :try_end_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-static {v1, v0, v4}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->orCause(Ljava/lang/Throwable;Lorg/apache/tools/ant/BuildException;Z)Ljava/lang/Throwable;

    move-result-object v1

    :try_start_2
    const-string v3, "org.apache.regexp.RE"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->testAvailability(Ljava/lang/String;)V

    const-string v3, "org.apache.tools.ant.util.regexp.JakartaRegexpMatcher"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->createInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;
    :try_end_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {v1, v0, v4}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->orCause(Ljava/lang/Throwable;Lorg/apache/tools/ant/BuildException;Z)Ljava/lang/Throwable;

    move-result-object v1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "No supported regular expression matcher found"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    if-eqz v1, :cond_3

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, ": "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    :cond_3
    const-string v3, ""

    goto :goto_3
.end method

.method protected testAvailability(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
