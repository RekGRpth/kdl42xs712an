.class public Lorg/apache/tools/ant/util/SourceFileScanner;
.super Ljava/lang/Object;
.source "SourceFileScanner.java"

# interfaces
.implements Lorg/apache/tools/ant/types/ResourceFactory;


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private destDir:Ljava/io/File;

.field protected task:Lorg/apache/tools/ant/Task;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/util/SourceFileScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/Task;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/util/SourceFileScanner;->task:Lorg/apache/tools/ant/Task;

    return-void
.end method


# virtual methods
.method public getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    iget-object v1, p0, Lorg/apache/tools/ant/util/SourceFileScanner;->destDir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)[Ljava/lang/String;
    .locals 7
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .param p4    # Lorg/apache/tools/ant/util/FileNameMapper;

    sget-object v0, Lorg/apache/tools/ant/util/SourceFileScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/FileUtils;->getFileTimestampGranularity()J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;J)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;J)[Ljava/lang/String;
    .locals 12
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .param p4    # Lorg/apache/tools/ant/util/FileNameMapper;
    .param p5    # J

    iput-object p3, p0, Lorg/apache/tools/ant/util/SourceFileScanner;->destDir:Ljava/io/File;

    new-instance v11, Ljava/util/Vector;

    invoke-direct {v11}, Ljava/util/Vector;-><init>()V

    const/4 v7, 0x0

    :goto_0
    array-length v0, p1

    if-ge v7, v0, :cond_0

    sget-object v0, Lorg/apache/tools/ant/util/SourceFileScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    aget-object v2, p1, v7

    invoke-virtual {v0, p2, v2}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    new-instance v0, Lorg/apache/tools/ant/types/Resource;

    aget-object v1, p1, v7

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-virtual {v10}, Ljava/io/File;->isDirectory()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/types/Resource;-><init>(Ljava/lang/String;ZJZ)V

    invoke-virtual {v11, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v1, v0, [Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v11, v1}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/apache/tools/ant/util/SourceFileScanner;->task:Lorg/apache/tools/ant/Task;

    move-object/from16 v2, p4

    move-object v3, p0

    move-wide/from16 v4, p5

    invoke-static/range {v0 .. v5}, Lorg/apache/tools/ant/util/ResourceUtils;->selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;J)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v8

    array-length v0, v8

    new-array v9, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    :goto_1
    array-length v0, v8

    if-ge v6, v0, :cond_1

    aget-object v0, v8, v6

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    return-object v9
.end method

.method public restrictAsFiles([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)[Ljava/io/File;
    .locals 7
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .param p4    # Lorg/apache/tools/ant/util/FileNameMapper;

    sget-object v0, Lorg/apache/tools/ant/util/SourceFileScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/FileUtils;->getFileTimestampGranularity()J

    move-result-wide v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrictAsFiles([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;J)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public restrictAsFiles([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;J)[Ljava/io/File;
    .locals 5
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .param p4    # Lorg/apache/tools/ant/util/FileNameMapper;
    .param p5    # J

    invoke-virtual/range {p0 .. p6}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;J)[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    new-array v2, v3, [Ljava/io/File;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    new-instance v3, Ljava/io/File;

    aget-object v4, v1, v0

    invoke-direct {v3, p2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method
