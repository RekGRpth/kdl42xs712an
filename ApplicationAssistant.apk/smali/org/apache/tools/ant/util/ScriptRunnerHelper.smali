.class public Lorg/apache/tools/ant/util/ScriptRunnerHelper;
.super Ljava/lang/Object;
.source "ScriptRunnerHelper.java"


# instance fields
.field private cpDelegate:Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

.field private language:Ljava/lang/String;

.field private manager:Ljava/lang/String;

.field private projectComponent:Lorg/apache/tools/ant/ProjectComponent;

.field private scriptLoader:Ljava/lang/ClassLoader;

.field private setBeans:Z

.field private srcFile:Ljava/io/File;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->cpDelegate:Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    const-string v0, "auto"

    iput-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->manager:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->setBeans:Z

    iput-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    return-void
.end method

.method private generateClassLoader()Ljava/lang/ClassLoader;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->cpDelegate:Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->cpDelegate:Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    goto :goto_0
.end method

.method private getClassPathDelegate()Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->cpDelegate:Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->projectComponent:Lorg/apache/tools/ant/ProjectComponent;

    invoke-static {v0}, Lorg/apache/tools/ant/util/ClasspathUtils;->getDelegate(Lorg/apache/tools/ant/ProjectComponent;)Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->cpDelegate:Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->cpDelegate:Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    return-object v0
.end method

.method private getRunner()Lorg/apache/tools/ant/util/ScriptRunnerBase;
    .locals 4

    new-instance v0, Lorg/apache/tools/ant/util/ScriptRunnerCreator;

    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->projectComponent:Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {v1}, Lorg/apache/tools/ant/ProjectComponent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/util/ScriptRunnerCreator;-><init>(Lorg/apache/tools/ant/Project;)V

    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->manager:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->language:Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->generateClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/tools/ant/util/ScriptRunnerCreator;->createRunner(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lorg/apache/tools/ant/util/ScriptRunnerBase;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->text:Ljava/lang/String;

    return-void
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->getClassPathDelegate()Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getScriptRunner()Lorg/apache/tools/ant/util/ScriptRunnerBase;
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->getRunner()Lorg/apache/tools/ant/util/ScriptRunnerBase;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->srcFile:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->srcFile:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/ScriptRunnerBase;->setSrc(Ljava/io/File;)V

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->text:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/ScriptRunnerBase;->addText(Ljava/lang/String;)V

    :cond_1
    iget-boolean v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->setBeans:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->projectComponent:Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/ScriptRunnerBase;->bindToComponent(Lorg/apache/tools/ant/ProjectComponent;)V

    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->projectComponent:Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/ScriptRunnerBase;->bindToComponentMinimum(Lorg/apache/tools/ant/ProjectComponent;)V

    goto :goto_0
.end method

.method public setClassLoader(Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1    # Ljava/lang/ClassLoader;

    iput-object p1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->scriptLoader:Ljava/lang/ClassLoader;

    return-void
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-direct {p0}, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->getClassPathDelegate()Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;->setClasspath(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-direct {p0}, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->getClassPathDelegate()Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/util/ClasspathUtils$Delegate;->setClasspathref(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->language:Ljava/lang/String;

    return-void
.end method

.method public setManager(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->manager:Ljava/lang/String;

    return-void
.end method

.method public setProjectComponent(Lorg/apache/tools/ant/ProjectComponent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/ProjectComponent;

    iput-object p1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->projectComponent:Lorg/apache/tools/ant/ProjectComponent;

    return-void
.end method

.method public setSetBeans(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->setBeans:Z

    return-void
.end method

.method public setSrc(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/util/ScriptRunnerHelper;->srcFile:Ljava/io/File;

    return-void
.end method
