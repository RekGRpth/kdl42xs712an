.class public Lorg/apache/tools/ant/util/ProxySetup;
.super Ljava/lang/Object;
.source "ProxySetup.java"


# static fields
.field public static final FTP_NON_PROXY_HOSTS:Ljava/lang/String; = "ftp.nonProxyHosts"

.field public static final FTP_PROXY_HOST:Ljava/lang/String; = "ftp.proxyHost"

.field public static final FTP_PROXY_PORT:Ljava/lang/String; = "ftp.proxyPort"

.field public static final HTTPS_NON_PROXY_HOSTS:Ljava/lang/String; = "https.nonProxyHosts"

.field public static final HTTPS_PROXY_HOST:Ljava/lang/String; = "https.proxyHost"

.field public static final HTTPS_PROXY_PORT:Ljava/lang/String; = "https.proxyPort"

.field public static final HTTP_NON_PROXY_HOSTS:Ljava/lang/String; = "http.nonProxyHosts"

.field public static final HTTP_PROXY_HOST:Ljava/lang/String; = "http.proxyHost"

.field public static final HTTP_PROXY_PASSWORD:Ljava/lang/String; = "http.proxyPassword"

.field public static final HTTP_PROXY_PORT:Ljava/lang/String; = "http.proxyPort"

.field public static final HTTP_PROXY_USERNAME:Ljava/lang/String; = "http.proxyUser"

.field public static final SOCKS_PROXY_HOST:Ljava/lang/String; = "socksProxyHost"

.field public static final SOCKS_PROXY_PASSWORD:Ljava/lang/String; = "java.net.socks.password"

.field public static final SOCKS_PROXY_PORT:Ljava/lang/String; = "socksProxyPort"

.field public static final SOCKS_PROXY_USERNAME:Ljava/lang/String; = "java.net.socks.username"

.field public static final USE_SYSTEM_PROXIES:Ljava/lang/String; = "java.net.useSystemProxies"


# instance fields
.field private owner:Lorg/apache/tools/ant/Project;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/Project;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/util/ProxySetup;->owner:Lorg/apache/tools/ant/Project;

    return-void
.end method

.method public static getSystemProxySetting()Ljava/lang/String;
    .locals 2

    :try_start_0
    const-string v1, "java.net.useSystemProxies"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public enableProxies()V
    .locals 6

    invoke-static {}, Lorg/apache/tools/ant/util/ProxySetup;->getSystemProxySetting()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/util/ProxySetup;->owner:Lorg/apache/tools/ant/Project;

    const-string v4, "java.net.useSystemProxies"

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const-string v2, "true"

    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "setting java.net.useSystemProxies to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/util/ProxySetup;->owner:Lorg/apache/tools/ant/Project;

    const/4 v4, 0x4

    invoke-virtual {v3, v1, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    const-string v3, "java.net.useSystemProxies"

    invoke-static {v3, v2}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lorg/apache/tools/ant/util/ProxySetup;->owner:Lorg/apache/tools/ant/Project;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Security Exception when "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;)V

    goto :goto_0
.end method
