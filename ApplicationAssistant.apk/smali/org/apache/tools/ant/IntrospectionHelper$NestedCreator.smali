.class abstract Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;
.super Ljava/lang/Object;
.source "IntrospectionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/IntrospectionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "NestedCreator"
.end annotation


# instance fields
.field private method:Ljava/lang/reflect/Method;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;)V
    .locals 0
    .param p1    # Ljava/lang/reflect/Method;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->method:Ljava/lang/reflect/Method;

    return-void
.end method

.method static access$100(Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;)Ljava/lang/reflect/Method;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->method:Ljava/lang/reflect/Method;

    return-object v0
.end method


# virtual methods
.method abstract create(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/reflect/InvocationTargetException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation
.end method

.method getMethod()Ljava/lang/reflect/Method;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->method:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method getRealObject()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method isPolyMorphic()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method store(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/reflect/InvocationTargetException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;
        }
    .end annotation

    return-void
.end method
