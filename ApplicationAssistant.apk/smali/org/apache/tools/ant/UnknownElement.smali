.class public Lorg/apache/tools/ant/UnknownElement;
.super Lorg/apache/tools/ant/Task;
.source "UnknownElement.java"


# instance fields
.field private children:Ljava/util/List;

.field private elementName:Ljava/lang/String;

.field private namespace:Ljava/lang/String;

.field private presetDefed:Z

.field private qname:Ljava/lang/String;

.field private realThing:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->namespace:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/UnknownElement;->presetDefed:Z

    iput-object p1, p0, Lorg/apache/tools/ant/UnknownElement;->elementName:Ljava/lang/String;

    return-void
.end method

.method private static equalsString(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private handleChild(Ljava/lang/String;Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/Object;Lorg/apache/tools/ant/UnknownElement;Lorg/apache/tools/ant/RuntimeConfigurable;)Z
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/tools/ant/IntrospectionHelper;
    .param p3    # Ljava/lang/Object;
    .param p4    # Lorg/apache/tools/ant/UnknownElement;
    .param p5    # Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {p4}, Lorg/apache/tools/ant/UnknownElement;->getNamespace()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/tools/ant/ProjectHelper;->genComponentName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, p1, v4}, Lorg/apache/tools/ant/IntrospectionHelper;->supportsNestedElement(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    move-object v0, p2

    move-object v2, p1

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/IntrospectionHelper;->getElementCreator(Lorg/apache/tools/ant/Project;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/IntrospectionHelper$Creator;

    move-result-object v7

    invoke-virtual {p5}, Lorg/apache/tools/ant/RuntimeConfigurable;->getPolyType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->setPolyType(Ljava/lang/String;)V

    invoke-virtual {v7}, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->create()Ljava/lang/Object;

    move-result-object v9

    instance-of v0, v9, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;

    if-eqz v0, :cond_0

    move-object v8, v9

    check-cast v8, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;

    invoke-virtual {v7}, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->getRealObject()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8}, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;->getPreSets()Lorg/apache/tools/ant/UnknownElement;

    move-result-object v0

    invoke-virtual {p4, v0}, Lorg/apache/tools/ant/UnknownElement;->applyPreSet(Lorg/apache/tools/ant/UnknownElement;)V

    :cond_0
    invoke-virtual {p5, v7}, Lorg/apache/tools/ant/RuntimeConfigurable;->setCreator(Lorg/apache/tools/ant/IntrospectionHelper$Creator;)V

    invoke-virtual {p5, v9}, Lorg/apache/tools/ant/RuntimeConfigurable;->setProxy(Ljava/lang/Object;)V

    instance-of v0, v9, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_1

    move-object v6, v9

    check-cast v6, Lorg/apache/tools/ant/Task;

    invoke-virtual {v6, p5}, Lorg/apache/tools/ant/Task;->setRuntimeConfigurableWrapper(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {v6, v4}, Lorg/apache/tools/ant/Task;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Lorg/apache/tools/ant/Task;->setTaskType(Ljava/lang/String;)V

    :cond_1
    instance-of v0, v9, Lorg/apache/tools/ant/ProjectComponent;

    if-eqz v0, :cond_2

    move-object v0, v9

    check-cast v0, Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {p4}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/ProjectComponent;->setLocation(Lorg/apache/tools/ant/Location;)V

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p5, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->maybeConfigure(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p4, v9, p5}, Lorg/apache/tools/ant/UnknownElement;->handleChildren(Ljava/lang/Object;Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {v7}, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->store()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addChild(Lorg/apache/tools/ant/UnknownElement;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/UnknownElement;

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public applyPreSet(Lorg/apache/tools/ant/UnknownElement;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/UnknownElement;

    iget-boolean v1, p0, Lorg/apache/tools/ant/UnknownElement;->presetDefed:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/RuntimeConfigurable;->applyPreSet(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    iget-object v1, p1, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    if-eqz v1, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p1, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iput-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/tools/ant/UnknownElement;->presetDefed:Z

    goto :goto_0
.end method

.method public configure(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/RuntimeConfigurable;->setProxy(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v1, v1, Lorg/apache/tools/ant/Task;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Task;->setRuntimeConfigurableWrapper(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/RuntimeConfigurable;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v2

    iget-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v1, Lorg/apache/tools/ant/Task;

    invoke-virtual {v2, p0, v1}, Lorg/apache/tools/ant/Target;->replaceChild(Lorg/apache/tools/ant/Task;Lorg/apache/tools/ant/Task;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/tools/ant/Task;->maybeConfigure()V

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/UnknownElement;->handleChildren(Ljava/lang/Object;Lorg/apache/tools/ant/RuntimeConfigurable;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/RuntimeConfigurable;->maybeConfigure(Lorg/apache/tools/ant/Project;)V

    goto :goto_0
.end method

.method public copy(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/UnknownElement;
    .locals 12
    .param p1    # Lorg/apache/tools/ant/Project;

    new-instance v7, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Lorg/apache/tools/ant/UnknownElement;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getNamespace()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lorg/apache/tools/ant/UnknownElement;->setNamespace(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Lorg/apache/tools/ant/UnknownElement;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getQName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lorg/apache/tools/ant/UnknownElement;->setQName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getTaskType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lorg/apache/tools/ant/UnknownElement;->setTaskType(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getTaskName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lorg/apache/tools/ant/UnknownElement;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-virtual {v7, v10}, Lorg/apache/tools/ant/UnknownElement;->setLocation(Lorg/apache/tools/ant/Location;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v10

    if-nez v10, :cond_0

    new-instance v8, Lorg/apache/tools/ant/Target;

    invoke-direct {v8}, Lorg/apache/tools/ant/Target;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {v8, v10}, Lorg/apache/tools/ant/Target;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/UnknownElement;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    :goto_0
    new-instance v1, Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getTaskName()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v7, v10}, Lorg/apache/tools/ant/RuntimeConfigurable;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/RuntimeConfigurable;->getPolyType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lorg/apache/tools/ant/RuntimeConfigurable;->setPolyType(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/RuntimeConfigurable;->getAttributeMap()Ljava/util/Hashtable;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v1, v10, v11}, Lorg/apache/tools/ant/RuntimeConfigurable;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v10

    invoke-virtual {v7, v10}, Lorg/apache/tools/ant/UnknownElement;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/RuntimeConfigurable;->getText()Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lorg/apache/tools/ant/RuntimeConfigurable;->addText(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/RuntimeConfigurable;->getChildren()Ljava/util/Enumeration;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {v6}, Lorg/apache/tools/ant/RuntimeConfigurable;->getProxy()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v9, p1}, Lorg/apache/tools/ant/UnknownElement;->copy(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/UnknownElement;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v10

    invoke-virtual {v1, v10}, Lorg/apache/tools/ant/RuntimeConfigurable;->addChild(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {v7, v0}, Lorg/apache/tools/ant/UnknownElement;->addChild(Lorg/apache/tools/ant/UnknownElement;)V

    goto :goto_2

    :cond_2
    return-object v7
.end method

.method public execute()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Could not create task of type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/UnknownElement;->elementName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {v0}, Lorg/apache/tools/ant/Task;->execute()V

    :cond_1
    iput-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v0

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/RuntimeConfigurable;->setProxy(Ljava/lang/Object;)V

    return-void
.end method

.method public getChildren()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    return-object v0
.end method

.method protected getComponentName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getNamespace()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/tools/ant/ProjectHelper;->genComponentName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNamespace()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->namespace:Ljava/lang/String;

    return-object v0
.end method

.method protected getNotFoundException(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/tools/ant/BuildException;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Lorg/apache/tools/ant/ComponentHelper;->diagnoseCreationFailure(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    return-object v2
.end method

.method public getQName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->qname:Ljava/lang/String;

    return-object v0
.end method

.method public getRealThing()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->elementName:Ljava/lang/String;

    return-object v0
.end method

.method public getTask()Lorg/apache/tools/ant/Task;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/Task;->getTaskName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {v0}, Lorg/apache/tools/ant/Task;->getTaskName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;
    .locals 1

    invoke-super {p0}, Lorg/apache/tools/ant/Task;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v0

    return-object v0
.end method

.method protected handleChildren(Ljava/lang/Object;Lorg/apache/tools/ant/RuntimeConfigurable;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
    .param p2    # Lorg/apache/tools/ant/RuntimeConfigurable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    instance-of v1, p1, Lorg/apache/tools/ant/TypeAdapter;

    if-eqz v1, :cond_0

    check-cast p1, Lorg/apache/tools/ant/TypeAdapter;

    invoke-interface {p1}, Lorg/apache/tools/ant/TypeAdapter;->getProxy()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getNamespace()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-static {v1, v11}, Lorg/apache/tools/ant/IntrospectionHelper;->getHelper(Lorg/apache/tools/ant/Project;Ljava/lang/Class;)Lorg/apache/tools/ant/IntrospectionHelper;

    move-result-object v3

    iget-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    const/4 v9, 0x0

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2, v9}, Lorg/apache/tools/ant/RuntimeConfigurable;->getChild(I)Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/UnknownElement;

    move-object v1, p0

    move-object v4, p1

    :try_start_0
    invoke-direct/range {v1 .. v6}, Lorg/apache/tools/ant/UnknownElement;->handleChild(Ljava/lang/String;Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/Object;Lorg/apache/tools/ant/UnknownElement;Lorg/apache/tools/ant/RuntimeConfigurable;)Z

    move-result v1

    if-nez v1, :cond_1

    instance-of v1, p1, Lorg/apache/tools/ant/TaskContainer;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v5}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, p1, v4}, Lorg/apache/tools/ant/IntrospectionHelper;->throwNotSupported(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/TaskContainer;

    move-object v7, v0

    invoke-interface {v7, v5}, Lorg/apache/tools/ant/TaskContainer;->addTask(Lorg/apache/tools/ant/Task;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/UnsupportedElementException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v8

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p2}, Lorg/apache/tools/ant/RuntimeConfigurable;->getElementTag()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v12, " doesn\'t support the nested \""

    invoke-virtual {v4, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v8}, Lorg/apache/tools/ant/UnsupportedElementException;->getElement()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v12, "\" element."

    invoke-virtual {v4, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    return-void
.end method

.method protected handleErrorFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected handleErrorOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected handleFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/Task;->handleFlush(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected handleInput([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/Task;->handleInput([BII)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tools/ant/Task;->handleInput([BII)I

    move-result v0

    goto :goto_0
.end method

.method protected handleOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/Task;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    check-cast v0, Lorg/apache/tools/ant/Task;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/Task;->handleOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected makeObject(Lorg/apache/tools/ant/UnknownElement;Lorg/apache/tools/ant/RuntimeConfigurable;)Ljava/lang/Object;
    .locals 7
    .param p1    # Lorg/apache/tools/ant/UnknownElement;
    .param p2    # Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getComponentName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getNamespace()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, p1, v5, v2}, Lorg/apache/tools/ant/ComponentHelper;->createComponent(Lorg/apache/tools/ant/UnknownElement;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v5, "task or type"

    invoke-virtual {p0, v5, v2}, Lorg/apache/tools/ant/UnknownElement;->getNotFoundException(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/tools/ant/BuildException;

    move-result-object v5

    throw v5

    :cond_0
    instance-of v5, v3, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;

    if-eqz v5, :cond_2

    move-object v0, v3

    check-cast v0, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;->createObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "preset "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;->getPreSets()Lorg/apache/tools/ant/UnknownElement;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/UnknownElement;->getComponentName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/UnknownElement;->getNotFoundException(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/tools/ant/BuildException;

    move-result-object v5

    throw v5

    :cond_1
    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;->getPreSets()Lorg/apache/tools/ant/UnknownElement;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/apache/tools/ant/UnknownElement;->applyPreSet(Lorg/apache/tools/ant/UnknownElement;)V

    instance-of v5, v3, Lorg/apache/tools/ant/Task;

    if-eqz v5, :cond_2

    move-object v4, v3

    check-cast v4, Lorg/apache/tools/ant/Task;

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getTaskType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/Task;->setTaskType(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getTaskName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/Task;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/Task;->init()V

    :cond_2
    instance-of v5, v3, Lorg/apache/tools/ant/UnknownElement;

    if-eqz v5, :cond_3

    move-object v5, v3

    check-cast v5, Lorg/apache/tools/ant/UnknownElement;

    check-cast v3, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v5, v3, p2}, Lorg/apache/tools/ant/UnknownElement;->makeObject(Lorg/apache/tools/ant/UnknownElement;Lorg/apache/tools/ant/RuntimeConfigurable;)Ljava/lang/Object;

    move-result-object v3

    :cond_3
    instance-of v5, v3, Lorg/apache/tools/ant/Task;

    if-eqz v5, :cond_4

    move-object v5, v3

    check-cast v5, Lorg/apache/tools/ant/Task;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/Task;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    :cond_4
    instance-of v5, v3, Lorg/apache/tools/ant/ProjectComponent;

    if-eqz v5, :cond_5

    move-object v5, v3

    check-cast v5, Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/ProjectComponent;->setLocation(Lorg/apache/tools/ant/Location;)V

    :cond_5
    return-object v3
.end method

.method protected makeTask(Lorg/apache/tools/ant/UnknownElement;Lorg/apache/tools/ant/RuntimeConfigurable;)Lorg/apache/tools/ant/Task;
    .locals 3
    .param p1    # Lorg/apache/tools/ant/UnknownElement;
    .param p2    # Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->createTask(Ljava/lang/String;)Lorg/apache/tools/ant/Task;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Task;->setLocation(Lorg/apache/tools/ant/Location;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Task;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/Task;->init()V

    :cond_0
    return-object v0
.end method

.method public maybeConfigure()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v0

    invoke-virtual {p0, p0, v0}, Lorg/apache/tools/ant/UnknownElement;->makeObject(Lorg/apache/tools/ant/UnknownElement;Lorg/apache/tools/ant/RuntimeConfigurable;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/UnknownElement;->configure(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setNamespace(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v1, "ant:current"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/ComponentHelper;->getCurrentAntlibUri()Ljava/lang/String;

    move-result-object p1

    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    :cond_1
    iput-object p1, p0, Lorg/apache/tools/ant/UnknownElement;->namespace:Ljava/lang/String;

    return-void
.end method

.method public setQName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/UnknownElement;->qname:Ljava/lang/String;

    return-void
.end method

.method public setRealThing(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lorg/apache/tools/ant/UnknownElement;->realThing:Ljava/lang/Object;

    return-void
.end method

.method public similar(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v2, p1

    check-cast v2, Lorg/apache/tools/ant/UnknownElement;

    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->elementName:Ljava/lang/String;

    iget-object v6, v2, Lorg/apache/tools/ant/UnknownElement;->elementName:Ljava/lang/String;

    invoke-static {v5, v6}, Lorg/apache/tools/ant/UnknownElement;->equalsString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->namespace:Ljava/lang/String;

    iget-object v6, v2, Lorg/apache/tools/ant/UnknownElement;->namespace:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->qname:Ljava/lang/String;

    iget-object v6, v2, Lorg/apache/tools/ant/UnknownElement;->qname:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/RuntimeConfigurable;->getAttributeMap()Ljava/util/Hashtable;

    move-result-object v5

    invoke-virtual {v2}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/RuntimeConfigurable;->getAttributeMap()Ljava/util/Hashtable;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Hashtable;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/RuntimeConfigurable;->getText()Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/RuntimeConfigurable;->getText()Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_4

    :cond_2
    iget-object v5, v2, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    if-eqz v5, :cond_3

    iget-object v5, v2, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_0

    :cond_3
    move v3, v4

    goto/16 :goto_0

    :cond_4
    iget-object v5, v2, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iget-object v6, v2, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ne v5, v6, :cond_0

    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_5

    iget-object v5, p0, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/UnknownElement;

    iget-object v5, v2, Lorg/apache/tools/ant/UnknownElement;->children:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/UnknownElement;->similar(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    move v3, v4

    goto/16 :goto_0
.end method
