.class public Lorg/apache/tools/ant/DirectoryScanner;
.super Ljava/lang/Object;
.source "DirectoryScanner.java"

# interfaces
.implements Lorg/apache/tools/ant/FileScanner;
.implements Lorg/apache/tools/ant/types/ResourceFactory;
.implements Lorg/apache/tools/ant/types/selectors/SelectorScanner;


# static fields
.field private static final CS_SCAN_ONLY:[Z

.field private static final CS_THEN_NON_CS:[Z

.field protected static final DEFAULTEXCLUDES:[Ljava/lang/String;

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field private static final ON_VMS:Z

.field private static defaultExcludes:Ljava/util/Vector;


# instance fields
.field private areNonPatternSetsReady:Z

.field protected basedir:Ljava/io/File;

.field protected dirsDeselected:Ljava/util/Vector;

.field protected dirsExcluded:Ljava/util/Vector;

.field protected dirsIncluded:Ljava/util/Vector;

.field protected dirsNotIncluded:Ljava/util/Vector;

.field protected everythingIncluded:Z

.field private excludeNonPatterns:Ljava/util/Set;

.field private excludePatterns:[Ljava/lang/String;

.field protected excludes:[Ljava/lang/String;

.field private fileListMap:Ljava/util/Map;

.field protected filesDeselected:Ljava/util/Vector;

.field protected filesExcluded:Ljava/util/Vector;

.field protected filesIncluded:Ljava/util/Vector;

.field protected filesNotIncluded:Ljava/util/Vector;

.field private followSymlinks:Z

.field protected haveSlowResults:Z

.field private illegal:Ljava/lang/IllegalStateException;

.field private includeNonPatterns:Ljava/util/Set;

.field private includePatterns:[Ljava/lang/String;

.field protected includes:[Ljava/lang/String;

.field protected isCaseSensitive:Z

.field private scanLock:Ljava/lang/Object;

.field private scannedDirs:Ljava/util/Set;

.field private scanning:Z

.field protected selectors:[Lorg/apache/tools/ant/types/selectors/FileSelector;

.field private slowScanLock:Ljava/lang/Object;

.field private slowScanning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "openvms"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/tools/ant/DirectoryScanner;->ON_VMS:Z

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "**/*~"

    aput-object v1, v0, v4

    const-string v1, "**/#*#"

    aput-object v1, v0, v3

    const-string v1, "**/.#*"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "**/%*%"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "**/._*"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "**/CVS"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "**/CVS/**"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "**/.cvsignore"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "**/SCCS"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "**/SCCS/**"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "**/vssver.scc"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "**/.svn"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "**/.svn/**"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "**/.DS_Store"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/tools/ant/DirectoryScanner;->DEFAULTEXCLUDES:[Ljava/lang/String;

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    new-array v0, v3, [Z

    aput-boolean v3, v0, v4

    sput-object v0, Lorg/apache/tools/ant/DirectoryScanner;->CS_SCAN_ONLY:[Z

    new-array v0, v5, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/tools/ant/DirectoryScanner;->CS_THEN_NON_CS:[Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    invoke-static {}, Lorg/apache/tools/ant/DirectoryScanner;->resetDefaultExcludes()V

    return-void

    :array_0
    .array-data 1
        0x1t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->selectors:[Lorg/apache/tools/ant/types/selectors/FileSelector;

    iput-boolean v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->haveSlowResults:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->followSymlinks:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->everythingIncluded:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->fileListMap:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->scannedDirs:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->includeNonPatterns:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludeNonPatterns:Ljava/util/Set;

    iput-boolean v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->areNonPatternSetsReady:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanning:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    iput-boolean v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanning:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanLock:Ljava/lang/Object;

    iput-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    return-void
.end method

.method private accountForIncludedDir(Ljava/lang/String;Ljava/io/File;Z)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;
    .param p3    # Z

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsDeselected:Ljava/util/Vector;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/DirectoryScanner;->processIncluded(Ljava/lang/String;Ljava/io/File;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;)V

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/DirectoryScanner;->couldHoldIncluded(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/DirectoryScanner;->contentsExcluded(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0, p3}, Lorg/apache/tools/ant/DirectoryScanner;->scandir(Ljava/io/File;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method private accountForIncludedFile(Ljava/lang/String;Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesIncluded:Ljava/util/Vector;

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesExcluded:Ljava/util/Vector;

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesDeselected:Ljava/util/Vector;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/DirectoryScanner;->processIncluded(Ljava/lang/String;Ljava/io/File;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;)V

    return-void
.end method

.method public static addDefaultExclude(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v0, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkIncludePatterns()V
    .locals 15

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    const/4 v5, 0x0

    :goto_0
    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    array-length v12, v12

    if-ge v5, v12, :cond_3

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v12, v12, v5

    invoke-static {v12}, Lorg/apache/tools/ant/util/FileUtils;->isAbsolutePath(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-eqz v12, :cond_2

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v12, v12, v5

    iget-object v13, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v14

    invoke-static {v12, v13, v14}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPatternStart(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v12

    if-nez v12, :cond_2

    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-eqz v12, :cond_0

    :cond_2
    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v12, v12, v5

    invoke-static {v12}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->rtrimWildcardTokens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v13, v13, v5

    invoke-interface {v9, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    const-string v12, ""

    invoke-interface {v9, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-eqz v12, :cond_5

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    const-string v13, ""

    const/4 v14, 0x1

    invoke-virtual {p0, v12, v13, v14}, Lorg/apache/tools/ant/DirectoryScanner;->scandir(Ljava/io/File;Ljava/lang/String;Z)V

    :cond_4
    return-void

    :cond_5
    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    const/4 v0, 0x0

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-eqz v12, :cond_6

    :try_start_0
    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_6
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-nez v12, :cond_7

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->isAbsolutePath(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    :cond_7
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    new-instance v8, Ljava/io/File;

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-direct {v8, v12, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_9

    :try_start_1
    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-nez v12, :cond_d

    invoke-virtual {v8}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v11

    :goto_3
    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    sget-boolean v12, Lorg/apache/tools/ant/DirectoryScanner;->ON_VMS:Z

    if-eqz v12, :cond_9

    :cond_8
    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    const/4 v13, 0x1

    invoke-direct {p0, v12, v1, v13}, Lorg/apache/tools/ant/DirectoryScanner;->findFile(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v8

    if-eqz v8, :cond_9

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-eqz v12, :cond_9

    sget-object v12, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v13, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v12, v13, v8}, Lorg/apache/tools/ant/util/FileUtils;->removeLeadingPath(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :cond_9
    if-eqz v8, :cond_a

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_b

    :cond_a
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v12

    if-nez v12, :cond_b

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    const/4 v13, 0x0

    invoke-direct {p0, v12, v1, v13}, Lorg/apache/tools/ant/DirectoryScanner;->findFile(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v4

    if-eqz v4, :cond_b

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_b

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-nez v12, :cond_e

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    :goto_4
    move-object v8, v4

    :cond_b
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_6

    iget-boolean v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->followSymlinks:Z

    if-nez v12, :cond_c

    iget-object v12, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-direct {p0, v12, v1}, Lorg/apache/tools/ant/DirectoryScanner;->isSymlink(Ljava/io/File;Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_6

    :cond_c
    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v12

    if-eqz v12, :cond_11

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/DirectoryScanner;->isIncluded(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_f

    const/4 v12, 0x1

    invoke-direct {p0, v1, v8, v12}, Lorg/apache/tools/ant/DirectoryScanner;->accountForIncludedDir(Ljava/lang/String;Ljava/io/File;Z)V

    goto/16 :goto_2

    :catch_0
    move-exception v3

    new-instance v12, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v12, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v12

    :cond_d
    :try_start_2
    sget-object v12, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v8}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v0, v13}, Lorg/apache/tools/ant/util/FileUtils;->removeLeadingPath(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v11

    goto/16 :goto_3

    :catch_1
    move-exception v3

    new-instance v12, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v12, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v12

    :cond_e
    sget-object v12, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v13, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v12, v13, v4}, Lorg/apache/tools/ant/util/FileUtils;->removeLeadingPath(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_f
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v1, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    sget-char v13, Ljava/io/File;->separatorChar:C

    if-eq v12, v13, :cond_10

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    sget-char v13, Ljava/io/File;->separatorChar:C

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_10
    const/4 v12, 0x1

    invoke-virtual {p0, v8, v1, v12}, Lorg/apache/tools/ant/DirectoryScanner;->scandir(Ljava/io/File;Ljava/lang/String;Z)V

    goto/16 :goto_2

    :cond_11
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v12

    if-eqz v12, :cond_12

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    :goto_5
    if-eqz v6, :cond_6

    invoke-direct {p0, v1, v8}, Lorg/apache/tools/ant/DirectoryScanner;->accountForIncludedFile(Ljava/lang/String;Ljava/io/File;)V

    goto/16 :goto_2

    :cond_12
    invoke-virtual {v10, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    goto :goto_5
.end method

.method private declared-synchronized clearCaches()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->fileListMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->includeNonPatterns:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludeNonPatterns:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->includePatterns:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludePatterns:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->areNonPatternSetsReady:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private contentsExcluded(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    aget-object v0, v3, v1

    const-string v3, "**"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v4

    invoke-static {v3, p1, v4}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPath(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private declared-synchronized ensureNonPatternSetsReady()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->areNonPatternSetsReady:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->includeNonPatterns:Ljava/util/Set;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/DirectoryScanner;->fillNonPatternSet(Ljava/util/Set;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->includePatterns:[Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludeNonPatterns:Ljava/util/Set;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/DirectoryScanner;->fillNonPatternSet(Ljava/util/Set;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludePatterns:[Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->areNonPatternSetsReady:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private fillNonPatternSet(Ljava/util/Set;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/util/Set;
    .param p2    # [Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    array-length v2, p2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_2

    aget-object v2, p2, v1

    invoke-static {v2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->hasWildcards(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, p2, v1

    :goto_1
    invoke-interface {p1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    aget-object v2, p2, v1

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    aget-object v2, p2, v1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_3

    :goto_3
    return-object p2

    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    move-object p2, v2

    goto :goto_3
.end method

.method private findFile(Ljava/io/File;Ljava/lang/String;Z)Ljava/io/File;
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-static {p2}, Lorg/apache/tools/ant/util/FileUtils;->isAbsolutePath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    sget-object v2, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2, p2}, Lorg/apache/tools/ant/util/FileUtils;->dissect(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance p1, Ljava/io/File;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-direct {p1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    aget-object p2, v1, v2

    :cond_0
    :goto_0
    invoke-static {p2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePath(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v2

    invoke-direct {p0, p1, v2, p3}, Lorg/apache/tools/ant/DirectoryScanner;->findFile(Ljava/io/File;Ljava/util/Vector;Z)Ljava/io/File;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_1
    sget-object v2, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2, p2}, Lorg/apache/tools/ant/util/FileUtils;->normalize(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    sget-object v2, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2, p1, v0}, Lorg/apache/tools/ant/util/FileUtils;->removeLeadingPath(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    move-object p2, v1

    goto :goto_0
.end method

.method private findFile(Ljava/io/File;Ljava/util/Vector;Z)Ljava/io/File;
    .locals 8
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/util/Vector;
    .param p3    # Z

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v6

    if-nez v6, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const/4 v6, 0x0

    invoke-virtual {p2, v6}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez p1, :cond_1

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v5, p2, p3}, Lorg/apache/tools/ant/DirectoryScanner;->findFile(Ljava/io/File;Ljava/util/Vector;Z)Ljava/io/File;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    move-object p1, v5

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/DirectoryScanner;->list(Ljava/io/File;)[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "IO error scanning directory "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3
    if-eqz p3, :cond_5

    sget-object v4, Lorg/apache/tools/ant/DirectoryScanner;->CS_SCAN_ONLY:[Z

    :goto_1
    const/4 v2, 0x0

    :goto_2
    array-length v6, v4

    if-ge v2, v6, :cond_9

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-ge v3, v6, :cond_8

    aget-boolean v6, v4, v2

    if-eqz v6, :cond_6

    aget-object v6, v1, v3

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    :cond_4
    new-instance v5, Ljava/io/File;

    aget-object v6, v1, v3

    invoke-direct {v5, p1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v5, p2, p3}, Lorg/apache/tools/ant/DirectoryScanner;->findFile(Ljava/io/File;Ljava/util/Vector;Z)Ljava/io/File;

    move-result-object p1

    goto :goto_0

    :cond_5
    sget-object v4, Lorg/apache/tools/ant/DirectoryScanner;->CS_THEN_NON_CS:[Z

    goto :goto_1

    :cond_6
    aget-object v6, v1, v3

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    move-object p1, v5

    goto :goto_0
.end method

.method public static getDefaultExcludes()[Ljava/lang/String;
    .locals 2

    sget-object v0, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    sget-object v1, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private hasBeenScanned(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->scannedDirs:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDeeper(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePath(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v1

    invoke-static {p2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePath(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    const-string v2, "**"

    invoke-virtual {v1, v2}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-le v2, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isMorePowerfulThanExcludes(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private isSymlink(Ljava/io/File;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;

    invoke-static {p2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePath(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/DirectoryScanner;->isSymlink(Ljava/io/File;Ljava/util/Vector;)Z

    move-result v0

    return v0
.end method

.method private isSymlink(Ljava/io/File;Ljava/util/Vector;)Z
    .locals 5
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/util/Vector;

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {p2, v3}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :try_start_0
    sget-object v4, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v4, p1, v0}, Lorg/apache/tools/ant/util/FileUtils;->isSymbolicLink(Ljava/io/File;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v4, p2}, Lorg/apache/tools/ant/DirectoryScanner;->isSymlink(Ljava/io/File;Ljava/util/Vector;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v3, 0x1

    :cond_1
    :goto_0
    return v3

    :catch_0
    move-exception v1

    const-string v2, "IOException caught while checking for links, couldn\'t get canonical path!"

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v4, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private list(Ljava/io/File;)[Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->fileListMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v0, v1

    check-cast v0, [Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->fileListMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public static match(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected static match(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {p0, p1, p2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected static matchPath(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPath(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected static matchPath(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {p0, p1, p2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPath(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected static matchPatternStart(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPatternStart(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected static matchPatternStart(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {p0, p1, p2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPatternStart(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static normalizePattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;

    const/16 v1, 0x2f

    sget-char v2, Ljava/io/File;->separatorChar:C

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5c

    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "**"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private processIncluded(Ljava/lang/String;Ljava/io/File;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/util/Vector;
    .param p4    # Ljava/util/Vector;
    .param p5    # Ljava/util/Vector;

    invoke-virtual {p3, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p4, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p5, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/DirectoryScanner;->isExcluded(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p4, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-boolean v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->everythingIncluded:Z

    and-int/2addr v1, v0

    iput-boolean v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->everythingIncluded:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1, p2}, Lorg/apache/tools/ant/DirectoryScanner;->isSelected(Ljava/lang/String;Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p3, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p5, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private processSlowScan([Ljava/lang/String;)V
    .locals 4
    .param p1    # [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/DirectoryScanner;->couldHoldIncluded(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    aget-object v3, p1, v0

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lorg/apache/tools/ant/DirectoryScanner;->scandir(Ljava/io/File;Ljava/lang/String;Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static removeDefaultExclude(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static resetDefaultExcludes()V
    .locals 3

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    sput-object v1, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lorg/apache/tools/ant/DirectoryScanner;->DEFAULTEXCLUDES:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    sget-object v2, Lorg/apache/tools/ant/DirectoryScanner;->DEFAULTEXCLUDES:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized addDefaultExcludes()V
    .locals 8

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    if-nez v4, :cond_1

    :goto_0
    sget-object v4, Lorg/apache/tools/ant/DirectoryScanner;->defaultExcludes:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/2addr v4, v1

    new-array v3, v4, [Ljava/lang/String;

    if-lez v1, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v5, v3, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    invoke-static {}, Lorg/apache/tools/ant/DirectoryScanner;->getDefaultExcludes()[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-ge v2, v4, :cond_2

    add-int v4, v2, v1

    aget-object v5, v0, v2

    const/16 v6, 0x2f

    sget-char v7, Ljava/io/File;->separatorChar:C

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x5c

    sget-char v7, Ljava/io/File;->separatorChar:C

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    array-length v1, v4

    goto :goto_0

    :cond_2
    iput-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized addExcludes([Ljava/lang/String;)V
    .locals 6
    .param p1    # [Ljava/lang/String;

    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    array-length v2, p1

    if-lez v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    array-length v2, p1

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    array-length v3, v3

    add-int/2addr v2, v3

    new-array v1, v2, [Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    array-length v5, v5

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    array-length v2, v2

    add-int/2addr v2, v0

    aget-object v3, p1, v0

    invoke-static {v3}, Lorg/apache/tools/ant/DirectoryScanner;->normalizePattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/DirectoryScanner;->setExcludes([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method protected declared-synchronized clearResults()V
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesIncluded:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesNotIncluded:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesExcluded:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesDeselected:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsNotIncluded:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsDeselected:Ljava/util/Vector;

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->everythingIncluded:Z

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->scannedDirs:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected couldHoldIncluded(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v2

    invoke-static {v1, p1, v2}, Lorg/apache/tools/ant/DirectoryScanner;->matchPatternStart(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-direct {p0, p1, v1}, Lorg/apache/tools/ant/DirectoryScanner;->isMorePowerfulThanExcludes(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, p1}, Lorg/apache/tools/ant/DirectoryScanner;->isDeeper(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public declared-synchronized getBasedir()Ljava/io/File;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDeselectedDirectories()[Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->slowScan()V

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsDeselected:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsDeselected:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getDeselectedFiles()[Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->slowScan()V

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesDeselected:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesDeselected:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getExcludedDirectories()[Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->slowScan()V

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getExcludedFiles()[Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->slowScan()V

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesExcluded:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesExcluded:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getIncludedDirectories()[Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Must call scan() first"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getIncludedDirsCount()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call scan() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getIncludedFiles()[Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesIncluded:Ljava/util/Vector;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Must call scan() first"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesIncluded:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesIncluded:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getIncludedFilesCount()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesIncluded:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call scan() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesIncluded:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getNotIncludedDirectories()[Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->slowScan()V

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsNotIncluded:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsNotIncluded:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getNotIncludedFiles()[Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->slowScan()V

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesNotIncluded:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesNotIncluded:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getScannedDirs()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->scannedDirs:Ljava/util/Set;

    return-object v0
.end method

.method public declared-synchronized isCaseSensitive()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isEverythingIncluded()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->everythingIncluded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected isExcluded(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    invoke-direct {p0}, Lorg/apache/tools/ant/DirectoryScanner;->ensureNonPatternSetsReady()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludeNonPatterns:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludeNonPatterns:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludePatterns:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludePatterns:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v3

    invoke-static {v2, p1, v3}, Lorg/apache/tools/ant/DirectoryScanner;->matchPath(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized isFollowSymlinks()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/DirectoryScanner;->followSymlinks:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected isIncluded(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    invoke-direct {p0}, Lorg/apache/tools/ant/DirectoryScanner;->ensureNonPatternSetsReady()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->includeNonPatterns:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->includeNonPatterns:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->includePatterns:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->includePatterns:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive()Z

    move-result v3

    invoke-static {v2, p1, v3}, Lorg/apache/tools/ant/DirectoryScanner;->matchPath(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected isSelected(Ljava/lang/String;Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->selectors:[Lorg/apache/tools/ant/types/selectors/FileSelector;

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->selectors:[Lorg/apache/tools/ant/types/selectors/FileSelector;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->selectors:[Lorg/apache/tools/ant/types/selectors/FileSelector;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-interface {v1, v2, p1, p2}, Lorg/apache/tools/ant/types/selectors/FileSelector;->isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public scan()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-boolean v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanning:Z

    if-eqz v6, :cond_2

    :goto_0
    iget-boolean v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    :try_start_1
    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    throw v3

    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    :cond_1
    :try_start_3
    monitor-exit v5

    :goto_1
    return-void

    :cond_2
    const/4 v6, 0x1

    iput-boolean v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanning:Z

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    const/4 v5, 0x0

    :try_start_5
    iput-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/DirectoryScanner;->clearResults()V

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    if-nez v5, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "**"

    aput-object v7, v5, v6

    :goto_3
    iput-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    if-nez v5, :cond_5

    :goto_4
    if-eqz v1, :cond_6

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    :goto_5
    iput-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    if-nez v4, :cond_7

    if-eqz v2, :cond_a

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    monitor-enter v4

    const/4 v3, 0x0

    :try_start_6
    iput-boolean v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanning:Z

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v4

    goto :goto_1

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v3

    :cond_3
    move v2, v4

    goto :goto_2

    :cond_4
    :try_start_7
    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    goto :goto_3

    :cond_5
    move v1, v4

    goto :goto_4

    :cond_6
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    goto :goto_5

    :cond_7
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_8

    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "basedir "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " does not exist"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    :cond_8
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "basedir "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " is not a directory"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    :cond_9
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    if-eqz v4, :cond_a

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->illegal:Ljava/lang/IllegalStateException;

    throw v3

    :catchall_2
    move-exception v3

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catchall_3
    move-exception v3

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    monitor-enter v4

    const/4 v5, 0x0

    :try_start_9
    iput-boolean v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanning:Z

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    throw v3

    :cond_a
    :try_start_a
    const-string v4, ""

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/DirectoryScanner;->isIncluded(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, ""

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/DirectoryScanner;->isExcluded(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, ""

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/DirectoryScanner;->isSelected(Ljava/lang/String;Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsIncluded:Ljava/util/Vector;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :goto_6
    invoke-direct {p0}, Lorg/apache/tools/ant/DirectoryScanner;->checkIncludePatterns()V

    invoke-direct {p0}, Lorg/apache/tools/ant/DirectoryScanner;->clearCaches()V

    if-eqz v2, :cond_e

    move-object v4, v3

    :goto_7
    iput-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    if-eqz v1, :cond_f

    :goto_8
    iput-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    monitor-enter v4

    const/4 v3, 0x0

    :try_start_b
    iput-boolean v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanning:Z

    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->scanLock:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v4

    goto/16 :goto_1

    :catchall_4
    move-exception v3

    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v3

    :cond_b
    :try_start_c
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsDeselected:Ljava/util/Vector;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_6

    :cond_c
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_6

    :cond_d
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsNotIncluded:Ljava/util/Vector;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_6

    :cond_e
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    goto :goto_7

    :cond_f
    iget-object v3, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_8

    :catchall_5
    move-exception v3

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    throw v3
.end method

.method protected scandir(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 10
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v9, 0x0

    if-nez p1, :cond_0

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "dir must not be null."

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " doesn\'t exist."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_2

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " is not a directory."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    if-eqz p3, :cond_4

    invoke-direct {p0, p2}, Lorg/apache/tools/ant/DirectoryScanner;->hasBeenScanned(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_5

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "IO error scanning directory \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_5
    iget-boolean v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->followSymlinks:Z

    if-nez v7, :cond_9

    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v7, v5

    if-ge v1, v7, :cond_8

    :try_start_0
    sget-object v7, Lorg/apache/tools/ant/DirectoryScanner;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    aget-object v8, v5, v1

    invoke-virtual {v7, p1, v8}, Lorg/apache/tools/ant/util/FileUtils;->isSymbolicLink(Ljava/io/File;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    aget-object v8, v5, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/io/File;

    aget-object v7, v5, v1

    invoke-direct {v0, p1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    :goto_1
    invoke-virtual {v7, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    iget-object v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesExcluded:Ljava/util/Vector;

    goto :goto_1

    :cond_7
    aget-object v7, v5, v1

    invoke-virtual {v6, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    const-string v3, "IOException caught while checking for links, couldn\'t get canonical path!"

    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v7, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    aget-object v7, v5, v1

    invoke-virtual {v6, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_2

    :cond_8
    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    move-object v5, v7

    check-cast v5, [Ljava/lang/String;

    :cond_9
    const/4 v1, 0x0

    :goto_3
    array-length v7, v5

    if-ge v1, v7, :cond_3

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    aget-object v8, v5, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/io/File;

    aget-object v7, v5, v1

    invoke-direct {v0, p1, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/DirectoryScanner;->isIncluded(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-direct {p0, v4, v0, p3}, Lorg/apache/tools/ant/DirectoryScanner;->accountForIncludedDir(Ljava/lang/String;Ljava/io/File;Z)V

    :cond_a
    :goto_4
    if-nez p3, :cond_b

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v0, v7, p3}, Lorg/apache/tools/ant/DirectoryScanner;->scandir(Ljava/io/File;Ljava/lang/String;Z)V

    :cond_b
    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_c
    iput-boolean v9, p0, Lorg/apache/tools/ant/DirectoryScanner;->everythingIncluded:Z

    iget-object v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsNotIncluded:Ljava/util/Vector;

    invoke-virtual {v7, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    if-eqz p3, :cond_a

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/DirectoryScanner;->couldHoldIncluded(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v0, v7, p3}, Lorg/apache/tools/ant/DirectoryScanner;->scandir(Ljava/io/File;Ljava/lang/String;Z)V

    goto :goto_4

    :cond_d
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/DirectoryScanner;->isIncluded(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e

    invoke-direct {p0, v4, v0}, Lorg/apache/tools/ant/DirectoryScanner;->accountForIncludedFile(Ljava/lang/String;Ljava/io/File;)V

    goto :goto_5

    :cond_e
    iput-boolean v9, p0, Lorg/apache/tools/ant/DirectoryScanner;->everythingIncluded:Z

    iget-object v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->filesNotIncluded:Ljava/util/Vector;

    invoke-virtual {v7, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_5
.end method

.method public declared-synchronized setBasedir(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/DirectoryScanner;->basedir:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBasedir(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    check-cast v0, Ljava/io/File;

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/DirectoryScanner;->setBasedir(Ljava/io/File;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/File;

    const/16 v1, 0x2f

    sget-char v2, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5c

    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized setCaseSensitive(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/tools/ant/DirectoryScanner;->isCaseSensitive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setExcludes([Ljava/lang/String;)V
    .locals 3
    .param p1    # [Ljava/lang/String;

    monitor-enter p0

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    aget-object v2, p1, v0

    invoke-static {v2}, Lorg/apache/tools/ant/DirectoryScanner;->normalizePattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setFollowSymlinks(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/tools/ant/DirectoryScanner;->followSymlinks:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setIncludes([Ljava/lang/String;)V
    .locals 3
    .param p1    # [Ljava/lang/String;

    monitor-enter p0

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    aget-object v2, p1, v0

    invoke-static {v2}, Lorg/apache/tools/ant/DirectoryScanner;->normalizePattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized setSelectors([Lorg/apache/tools/ant/types/selectors/FileSelector;)V
    .locals 1
    .param p1    # [Lorg/apache/tools/ant/types/selectors/FileSelector;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/DirectoryScanner;->selectors:[Lorg/apache/tools/ant/types/selectors/FileSelector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected slowScan()V
    .locals 9

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanLock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-boolean v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->haveSlowResults:Z

    if-eqz v7, :cond_0

    monitor-exit v6

    :goto_0
    return-void

    :cond_0
    iget-boolean v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanning:Z

    if-eqz v7, :cond_2

    :goto_1
    iget-boolean v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    :try_start_1
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanLock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v4

    goto :goto_1

    :cond_1
    :try_start_2
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    :cond_2
    const/4 v7, 0x1

    :try_start_3
    iput-boolean v7, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanning:Z

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-enter p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    iget-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    if-nez v6, :cond_3

    move v3, v2

    :goto_2
    if-eqz v3, :cond_4

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "**"

    aput-object v8, v6, v7

    :goto_3
    iput-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    if-nez v6, :cond_5

    :goto_4
    if-eqz v2, :cond_6

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    :goto_5
    iput-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v0, v5, [Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsExcluded:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsNotIncluded:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v1, v5, [Ljava/lang/String;

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->dirsNotIncluded:Ljava/util/Vector;

    invoke-virtual {v5, v1}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/DirectoryScanner;->processSlowScan([Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/DirectoryScanner;->processSlowScan([Ljava/lang/String;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/DirectoryScanner;->clearCaches()V

    if-eqz v3, :cond_7

    move-object v5, v4

    :goto_6
    iput-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    if-eqz v2, :cond_8

    :goto_7
    iput-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanLock:Ljava/lang/Object;

    monitor-enter v5

    const/4 v4, 0x1

    :try_start_6
    iput-boolean v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->haveSlowResults:Z

    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanning:Z

    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanLock:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v5

    goto :goto_0

    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v4

    :cond_3
    move v3, v5

    goto :goto_2

    :cond_4
    :try_start_7
    iget-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    goto :goto_3

    :cond_5
    move v2, v5

    goto :goto_4

    :cond_6
    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    goto :goto_5

    :cond_7
    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->includes:[Ljava/lang/String;

    goto :goto_6

    :cond_8
    iget-object v4, p0, Lorg/apache/tools/ant/DirectoryScanner;->excludes:[Ljava/lang/String;

    goto :goto_7

    :catchall_2
    move-exception v4

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catchall_3
    move-exception v4

    iget-object v5, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanLock:Ljava/lang/Object;

    monitor-enter v5

    const/4 v6, 0x1

    :try_start_9
    iput-boolean v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->haveSlowResults:Z

    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanning:Z

    iget-object v6, p0, Lorg/apache/tools/ant/DirectoryScanner;->slowScanLock:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v4

    :catchall_4
    move-exception v4

    :try_start_a
    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v4
.end method
