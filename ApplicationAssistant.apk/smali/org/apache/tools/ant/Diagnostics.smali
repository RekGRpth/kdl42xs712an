.class public final Lorg/apache/tools/ant/Diagnostics;
.super Ljava/lang/Object;
.source "Diagnostics.java"


# static fields
.field private static final BIG_DRIFT_LIMIT:I = 0x2710

.field protected static final ERROR_PROPERTY_ACCESS_BLOCKED:Ljava/lang/String; = "Access to this property blocked by a security manager"

.field private static final KILOBYTE:I = 0x400

.field private static final MINUTES_PER_HOUR:I = 0x3c

.field private static final SECONDS_PER_MILLISECOND:I = 0x3e8

.field private static final SECONDS_PER_MINUTE:I = 0x3c

.field private static final TEST_CLASS:Ljava/lang/String; = "org.apache.tools.ant.taskdefs.optional.Test"

.field private static final TEST_FILE_SIZE:I = 0x20

.field static array$Ljava$lang$String:Ljava/lang/Class;

.field static class$org$apache$tools$ant$Main:Ljava/lang/Class;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static doReport(Ljava/io/PrintStream;)V
    .locals 4
    .param p0    # Ljava/io/PrintStream;

    const-string v2, "------- Ant diagnostics report -------"

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lorg/apache/tools/ant/Main;->getAntVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v2, "Implementation Version"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "core tasks     : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    sget-object v2, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    if-nez v2, :cond_0

    const-string v2, "org.apache.tools.ant.Main"

    invoke-static {v2}, Lorg/apache/tools/ant/Diagnostics;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    :goto_0
    invoke-static {v2}, Lorg/apache/tools/ant/Diagnostics;->getImplementationVersion(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "org.apache.tools.ant.taskdefs.optional.Test"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "optional tasks : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {v1}, Lorg/apache/tools/ant/Diagnostics;->getImplementationVersion(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v2, "ANT PROPERTIES"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportAntProperties(Ljava/io/PrintStream;)V

    const-string v2, "ANT_HOME/lib jar listing"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportAntHomeLibraries(Ljava/io/PrintStream;)V

    const-string v2, "USER_HOME/.ant/lib jar listing"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportUserHomeLibraries(Ljava/io/PrintStream;)V

    const-string v2, "Tasks availability"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportTasksAvailability(Ljava/io/PrintStream;)V

    const-string v2, "org.apache.env.Which diagnostics"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportWhich(Ljava/io/PrintStream;)V

    const-string v2, "XML Parser information"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportParserInfo(Ljava/io/PrintStream;)V

    const-string v2, "System properties"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportSystemProperties(Ljava/io/PrintStream;)V

    const-string v2, "Temp dir"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportTempDir(Ljava/io/PrintStream;)V

    const-string v2, "Locale information"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportLocale(Ljava/io/PrintStream;)V

    const-string v2, "Proxy information"

    invoke-static {p0, v2}, Lorg/apache/tools/ant/Diagnostics;->header(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {p0}, Lorg/apache/tools/ant/Diagnostics;->doReportProxy(Ljava/io/PrintStream;)V

    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    return-void

    :cond_0
    sget-object v2, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lorg/apache/tools/ant/Diagnostics;->ignoreThrowable(Ljava/lang/Throwable;)V

    const-string v2, "optional tasks : not available"

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static doReportAntHomeLibraries(Ljava/io/PrintStream;)V
    .locals 3
    .param p0    # Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "ant.home: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "ant.home"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->listLibraries()[Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p0}, Lorg/apache/tools/ant/Diagnostics;->printLibraries([Ljava/io/File;Ljava/io/PrintStream;)V

    return-void
.end method

.method private static doReportAntProperties(Ljava/io/PrintStream;)V
    .locals 3
    .param p0    # Ljava/io/PrintStream;

    new-instance v0, Lorg/apache/tools/ant/Project;

    invoke-direct {v0}, Lorg/apache/tools/ant/Project;-><init>()V

    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->initProperties()V

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "ant.version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "ant.version"

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "ant.java.version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "ant.java.version"

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "ant.core.lib: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "ant.core.lib"

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "ant.home: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "ant.home"

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private static doReportLocale(Ljava/io/PrintStream;)V
    .locals 10
    .param p0    # Ljava/io/PrintStream;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Timezone "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " offset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v7, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v7, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x7

    invoke-virtual {v7, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0xb

    invoke-virtual {v7, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    mul-int/lit8 v6, v6, 0x3c

    const/16 v9, 0xc

    invoke-virtual {v7, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/2addr v6, v9

    mul-int/lit8 v6, v6, 0x3c

    const/16 v9, 0xd

    invoke-virtual {v7, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/2addr v6, v9

    mul-int/lit16 v6, v6, 0x3e8

    const/16 v9, 0xe

    invoke-virtual {v7, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private static doReportParserInfo(Ljava/io/PrintStream;)V
    .locals 5
    .param p0    # Ljava/io/PrintStream;

    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->getXmlParserName()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->getXMLParserLocation()Ljava/lang/String;

    move-result-object v0

    const-string v2, "XML Parser"

    invoke-static {p0, v2, v1, v0}, Lorg/apache/tools/ant/Diagnostics;->printParserInfo(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Namespace-aware parser"

    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->getNamespaceParserName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->getNamespaceParserLocation()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v2, v3, v4}, Lorg/apache/tools/ant/Diagnostics;->printParserInfo(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static doReportProxy(Ljava/io/PrintStream;)V
    .locals 5
    .param p0    # Ljava/io/PrintStream;

    const-string v3, "http.proxyHost"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "http.proxyPort"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "http.proxyUser"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "http.proxyPassword"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "http.nonProxyHosts"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "https.proxyHost"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "https.proxyPort"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "https.nonProxyHosts"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "ftp.proxyHost"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "ftp.proxyPort"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "ftp.nonProxyHosts"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "socksProxyHost"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "socksProxyPort"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "java.net.socks.username"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v3, "java.net.socks.password"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    invoke-static {}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJavaVersionNumber()I

    move-result v3

    const/16 v4, 0xf

    if-ge v3, v4, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v3, "java.net.useSystemProxies"

    invoke-static {p0, v3}, Lorg/apache/tools/ant/Diagnostics;->printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V

    const-string v2, "org.apache.tools.ant.util.java15.ProxyDiagnostics"

    :try_start_0
    const-string v3, "org.apache.tools.ant.util.java15.ProxyDiagnostics"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    const-string v3, "Java1.5+ proxy settings:"

    invoke-virtual {p0, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0

    :catch_2
    move-exception v3

    goto :goto_0

    :catch_3
    move-exception v3

    goto :goto_0
.end method

.method private static doReportSystemProperties(Ljava/io/PrintStream;)V
    .locals 7
    .param p0    # Ljava/io/PrintStream;

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    invoke-virtual {v3}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/tools/ant/Diagnostics;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lorg/apache/tools/ant/Diagnostics;->ignoreThrowable(Ljava/lang/Throwable;)V

    const-string v5, "Access to System.getProperties() blocked by a security manager"

    invoke-virtual {p0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static doReportTasksAvailability(Ljava/io/PrintStream;)V
    .locals 10
    .param p0    # Ljava/io/PrintStream;

    sget-object v7, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    if-nez v7, :cond_0

    const-string v7, "org.apache.tools.ant.Main"

    invoke-static {v7}, Lorg/apache/tools/ant/Diagnostics;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    sput-object v7, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    :goto_0
    const-string v8, "/org/apache/tools/ant/taskdefs/defaults.properties"

    invoke-virtual {v7, v8}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v7, "None available"

    invoke-virtual {p0, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    sget-object v7, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    new-instance v6, Ljava/util/Properties;

    invoke-direct {v6}, Ljava/util/Properties;-><init>()V

    :try_start_0
    invoke-virtual {v6, v2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v6}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :try_start_1
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    invoke-virtual {v6, v3}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/LinkageError; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_0
    move-exception v1

    :try_start_2
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " : Not Available "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "(the implementation class is not present)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :catch_2
    move-exception v1

    :try_start_3
    invoke-virtual {v1}, Ljava/lang/NoClassDefFoundError;->getMessage()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x2f

    const/16 v9, 0x2e

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " : Missing dependency "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    :catch_3
    move-exception v1

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " : Initialization error"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    invoke-virtual {v6}, Ljava/util/Properties;->size()I

    move-result v7

    if-nez v7, :cond_3

    const-string v7, "All defined tasks are available"

    invoke-virtual {p0, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    const-string v7, "A task being missing/unavailable should only matter if you are trying to use it"

    invoke-virtual {p0, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1
.end method

.method private static doReportTempDir(Ljava/io/PrintStream;)V
    .locals 19
    .param p0    # Ljava/io/PrintStream;

    const-string v15, "java.io.tmpdir"

    invoke-static {v15}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_1

    const-string v15, "Warning: java.io.tmpdir is undefined"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Temp dir is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_2

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Warning, java.io.tmpdir directory does not exist: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const/4 v13, 0x0

    const/4 v5, 0x0

    :try_start_0
    const-string v15, "diag"

    const-string v16, "txt"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v12}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v13

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v15, 0x400

    :try_start_1
    new-array v1, v15, [B

    const/4 v9, 0x0

    :goto_1
    const/16 v15, 0x20

    if-ge v9, v15, :cond_3

    invoke-virtual {v6, v1}, Ljava/io/FileOutputStream;->write([B)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v5, 0x0

    :try_start_2
    invoke-virtual {v13}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    const-string v15, "Temp dir is writeable"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sub-long v2, v7, v10

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Temp dir alignment with system clock is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string v16, " ms"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v15

    const-wide/16 v17, 0x2710

    cmp-long v15, v15, v17

    if-lez v15, :cond_4

    const-string v15, "Warning: big clock drift -maybe a network filesystem"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    :catch_0
    move-exception v4

    :goto_2
    :try_start_3
    invoke-static {v4}, Lorg/apache/tools/ant/Diagnostics;->ignoreThrowable(Ljava/lang/Throwable;)V

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Failed to create a temporary file in the temp dir "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "File  "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string v16, " could not be created/written to"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    :catchall_0
    move-exception v15

    :goto_3
    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    if-eqz v13, :cond_5

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_5

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    :cond_5
    throw v15

    :catchall_1
    move-exception v15

    move-object v5, v6

    goto :goto_3

    :catch_1
    move-exception v4

    move-object v5, v6

    goto :goto_2
.end method

.method private static doReportUserHomeLibraries(Ljava/io/PrintStream;)V
    .locals 5
    .param p0    # Ljava/io/PrintStream;

    const-string v3, "user.home"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "user.home: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    sget-object v3, Lorg/apache/tools/ant/launch/Launcher;->USER_LIBDIR:Ljava/lang/String;

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lorg/apache/tools/ant/Diagnostics;->listJarFiles(Ljava/io/File;)[Ljava/io/File;

    move-result-object v2

    invoke-static {v2, p0}, Lorg/apache/tools/ant/Diagnostics;->printLibraries([Ljava/io/File;Ljava/io/PrintStream;)V

    return-void
.end method

.method private static doReportWhich(Ljava/io/PrintStream;)V
    .locals 8
    .param p0    # Ljava/io/PrintStream;

    const/4 v1, 0x0

    :try_start_0
    const-string v4, "org.apache.env.Which"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v5, "main"

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v4, Lorg/apache/tools/ant/Diagnostics;->array$Ljava$lang$String:Ljava/lang/Class;

    if-nez v4, :cond_1

    const-string v4, "[Ljava.lang.String;"

    invoke-static {v4}, Lorg/apache/tools/ant/Diagnostics;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    sput-object v4, Lorg/apache/tools/ant/Diagnostics;->array$Ljava$lang$String:Ljava/lang/Class;

    :goto_0
    aput-object v4, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    if-eqz v1, :cond_0

    const-string v4, "Error while running org.apache.env.Which"

    invoke-virtual {p0, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    sget-object v4, Lorg/apache/tools/ant/Diagnostics;->array$Ljava$lang$String:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Not available."

    invoke-virtual {p0, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "Download it at http://xml.apache.org/commons/"

    invoke-virtual {p0, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v4

    if-nez v4, :cond_2

    move-object v1, v0

    :goto_2
    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v1

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v0

    goto :goto_1
.end method

.method private static getClassLocation(Ljava/lang/Class;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/Class;

    invoke-static {p0}, Lorg/apache/tools/ant/util/LoaderUtils;->getClassSource(Ljava/lang/Class;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getImplementationVersion(Ljava/lang/Class;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Package;->getImplementationVersion()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getNamespaceParserLocation()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getNamespaceXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/Diagnostics;->getClassLocation(Ljava/lang/Class;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lorg/apache/tools/ant/Diagnostics;->ignoreThrowable(Ljava/lang/Throwable;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getNamespaceParserName()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getNamespaceXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lorg/apache/tools/ant/Diagnostics;->ignoreThrowable(Ljava/lang/Throwable;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "Access to this property blocked by a security manager"

    goto :goto_0
.end method

.method private static getSAXParser()Ljavax/xml/parsers/SAXParser;
    .locals 3

    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lorg/apache/tools/ant/Diagnostics;->ignoreThrowable(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getXMLParserLocation()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->getSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/Diagnostics;->getClassLocation(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getXmlParserName()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->getSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "Could not create an XML Parser"

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static header(Ljava/io/PrintStream;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/io/PrintStream;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    const-string v0, "-------------------------------------------"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, "-------------------------------------------"

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private static ignoreThrowable(Ljava/lang/Throwable;)V
    .locals 0
    .param p0    # Ljava/lang/Throwable;

    return-void
.end method

.method public static isOptionalAvailable()Z
    .locals 2

    :try_start_0
    const-string v1, "org.apache.tools.ant.taskdefs.optional.Test"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static listJarFiles(Ljava/io/File;)[Ljava/io/File;
    .locals 2
    .param p0    # Ljava/io/File;

    new-instance v1, Lorg/apache/tools/ant/Diagnostics$1;

    invoke-direct {v1}, Lorg/apache/tools/ant/Diagnostics$1;-><init>()V

    invoke-virtual {p0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static listLibraries()[Ljava/io/File;
    .locals 3

    const-string v2, "ant.home"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "lib"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lorg/apache/tools/ant/Diagnostics;->listJarFiles(Ljava/io/File;)[Ljava/io/File;

    move-result-object v2

    goto :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 1
    .param p0    # [Ljava/lang/String;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lorg/apache/tools/ant/Diagnostics;->doReport(Ljava/io/PrintStream;)V

    return-void
.end method

.method private static printLibraries([Ljava/io/File;Ljava/io/PrintStream;)V
    .locals 4
    .param p0    # [Ljava/io/File;
    .param p1    # Ljava/io/PrintStream;

    if-nez p0, :cond_1

    const-string v1, "No such directory."

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " bytes)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static printParserInfo(Ljava/io/PrintStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/io/PrintStream;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    if-nez p2, :cond_0

    const-string p2, "unknown"

    :cond_0
    if-nez p3, :cond_1

    const-string p3, "unknown"

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " Location: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private static printProperty(Ljava/io/PrintStream;Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/io/PrintStream;
    .param p1    # Ljava/lang/String;

    const/16 v2, 0x22

    invoke-static {p1}, Lorg/apache/tools/ant/Diagnostics;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    const-string v1, " = "

    invoke-virtual {p0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->print(C)V

    invoke-virtual {p0, v0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(C)V

    :cond_0
    return-void
.end method

.method public static validateVersion()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    :try_start_0
    const-string v4, "org.apache.tools.ant.taskdefs.optional.Test"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sget-object v4, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    if-nez v4, :cond_1

    const-string v4, "org.apache.tools.ant.Main"

    invoke-static {v4}, Lorg/apache/tools/ant/Diagnostics;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    sput-object v4, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    :goto_0
    invoke-static {v4}, Lorg/apache/tools/ant/Diagnostics;->getImplementationVersion(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Lorg/apache/tools/ant/Diagnostics;->getImplementationVersion(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Invalid implementation version between Ant core and Ant optional tasks.\n core    : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " optional: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lorg/apache/tools/ant/Diagnostics;->ignoreThrowable(Ljava/lang/Throwable;)V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    sget-object v4, Lorg/apache/tools/ant/Diagnostics;->class$org$apache$tools$ant$Main:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
