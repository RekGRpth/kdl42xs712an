.class public Lorg/apache/tools/ant/Main;
.super Ljava/lang/Object;
.source "Main.java"

# interfaces
.implements Lorg/apache/tools/ant/launch/AntMain;


# static fields
.field public static final DEFAULT_BUILD_FILENAME:Ljava/lang/String; = "build.xml"

.field private static antVersion:Ljava/lang/String;

.field static class$org$apache$tools$ant$BuildListener:Ljava/lang/Class;

.field static class$org$apache$tools$ant$BuildLogger:Ljava/lang/Class;

.field static class$org$apache$tools$ant$Main:Ljava/lang/Class;

.field static class$org$apache$tools$ant$input$InputHandler:Ljava/lang/Class;

.field private static err:Ljava/io/PrintStream;

.field private static isLogFileUsed:Z

.field private static out:Ljava/io/PrintStream;


# instance fields
.field private allowInput:Z

.field private buildFile:Ljava/io/File;

.field private definedProps:Ljava/util/Properties;

.field private emacsMode:Z

.field private inputHandlerClassname:Ljava/lang/String;

.field private keepGoingMode:Z

.field private listeners:Ljava/util/Vector;

.field private loggerClassname:Ljava/lang/String;

.field private msgOutputLevel:I

.field private projectHelp:Z

.field private propertyFiles:Ljava/util/Vector;

.field private proxy:Z

.field private readyToRun:Z

.field private targets:Ljava/util/Vector;

.field private threadPriority:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sput-object v0, Lorg/apache/tools/ant/Main;->out:Ljava/io/PrintStream;

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    sput-object v0, Lorg/apache/tools/ant/Main;->err:Ljava/io/PrintStream;

    const/4 v0, 0x0

    sput-boolean v0, Lorg/apache/tools/ant/Main;->isLogFileUsed:Z

    const/4 v0, 0x0

    sput-object v0, Lorg/apache/tools/ant/Main;->antVersion:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->targets:Ljava/util/Vector;

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, v2}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->listeners:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, v2}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->propertyFiles:Ljava/util/Vector;

    iput-boolean v2, p0, Lorg/apache/tools/ant/Main;->allowInput:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->keepGoingMode:Z

    iput-object v3, p0, Lorg/apache/tools/ant/Main;->loggerClassname:Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/tools/ant/Main;->inputHandlerClassname:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->emacsMode:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->readyToRun:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->projectHelp:Z

    iput-object v3, p0, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->proxy:Z

    return-void
.end method

.method protected constructor <init>([Ljava/lang/String;)V
    .locals 4
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->targets:Ljava/util/Vector;

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, v2}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->listeners:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, v2}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/Main;->propertyFiles:Ljava/util/Vector;

    iput-boolean v2, p0, Lorg/apache/tools/ant/Main;->allowInput:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->keepGoingMode:Z

    iput-object v3, p0, Lorg/apache/tools/ant/Main;->loggerClassname:Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/tools/ant/Main;->inputHandlerClassname:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->emacsMode:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->readyToRun:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->projectHelp:Z

    iput-object v3, p0, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;

    iput-boolean v1, p0, Lorg/apache/tools/ant/Main;->proxy:Z

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/Main;->processArgs([Ljava/lang/String;)V

    return-void
.end method

.method private addInputHandler(Lorg/apache/tools/ant/Project;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/Main;->inputHandlerClassname:Ljava/lang/String;

    if-nez v1, :cond_1

    new-instance v0, Lorg/apache/tools/ant/input/DefaultInputHandler;

    invoke-direct {v0}, Lorg/apache/tools/ant/input/DefaultInputHandler;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/Project;->setInputHandler(Lorg/apache/tools/ant/input/InputHandler;)V

    return-void

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/Main;->inputHandlerClassname:Ljava/lang/String;

    sget-object v1, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    if-nez v1, :cond_2

    const-string v1, "org.apache.tools.ant.Main"

    invoke-static {v1}, Lorg/apache/tools/ant/Main;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    sget-object v1, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$input$InputHandler:Ljava/lang/Class;

    if-nez v1, :cond_3

    const-string v1, "org.apache.tools.ant.input.InputHandler"

    invoke-static {v1}, Lorg/apache/tools/ant/Main;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$input$InputHandler:Ljava/lang/Class;

    :goto_2
    invoke-static {v2, v3, v1}, Lorg/apache/tools/ant/util/ClasspathUtils;->newInstance(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/input/InputHandler;

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/Project;->setProjectReference(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    sget-object v1, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    goto :goto_1

    :cond_3
    sget-object v1, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$input$InputHandler:Ljava/lang/Class;

    goto :goto_2
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private createLogger()Lorg/apache/tools/ant/BuildLogger;
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/Main;->loggerClassname:Ljava/lang/String;

    if-eqz v2, :cond_2

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/Main;->loggerClassname:Ljava/lang/String;

    sget-object v2, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    if-nez v2, :cond_0

    const-string v2, "org.apache.tools.ant.Main"

    invoke-static {v2}, Lorg/apache/tools/ant/Main;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    sget-object v2, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$BuildLogger:Ljava/lang/Class;

    if-nez v2, :cond_1

    const-string v2, "org.apache.tools.ant.BuildLogger"

    invoke-static {v2}, Lorg/apache/tools/ant/Main;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$BuildLogger:Ljava/lang/Class;

    :goto_1
    invoke-static {v3, v4, v2}, Lorg/apache/tools/ant/util/ClasspathUtils;->newInstance(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/BuildLogger;
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget v2, p0, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    invoke-interface {v1, v2}, Lorg/apache/tools/ant/BuildLogger;->setMessageOutputLevel(I)V

    sget-object v2, Lorg/apache/tools/ant/Main;->out:Ljava/io/PrintStream;

    invoke-interface {v1, v2}, Lorg/apache/tools/ant/BuildLogger;->setOutputPrintStream(Ljava/io/PrintStream;)V

    sget-object v2, Lorg/apache/tools/ant/Main;->err:Ljava/io/PrintStream;

    invoke-interface {v1, v2}, Lorg/apache/tools/ant/BuildLogger;->setErrorPrintStream(Ljava/io/PrintStream;)V

    iget-boolean v2, p0, Lorg/apache/tools/ant/Main;->emacsMode:Z

    invoke-interface {v1, v2}, Lorg/apache/tools/ant/BuildLogger;->setEmacsMode(Z)V

    return-object v1

    :cond_0
    :try_start_1
    sget-object v2, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v2, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$BuildLogger:Ljava/lang/Class;
    :try_end_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "The specified logger class "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/Main;->loggerClassname:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " could not be used because "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/tools/ant/BuildException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2}, Ljava/lang/RuntimeException;-><init>()V

    throw v2

    :cond_2
    new-instance v1, Lorg/apache/tools/ant/DefaultLogger;

    invoke-direct {v1}, Lorg/apache/tools/ant/DefaultLogger;-><init>()V

    goto :goto_2
.end method

.method private findBuildFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget v2, p0, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Searching for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " ..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/Main;->getParentFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Could not locate a build file!"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private static findTargetPosition(Ljava/util/Vector;Ljava/lang/String;)I
    .locals 3
    .param p0    # Ljava/util/Vector;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    move v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public static declared-synchronized getAntVersion()Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const-class v6, Lorg/apache/tools/ant/Main;

    monitor-enter v6

    :try_start_0
    sget-object v5, Lorg/apache/tools/ant/Main;->antVersion:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_0

    :try_start_1
    new-instance v4, Ljava/util/Properties;

    invoke-direct {v4}, Ljava/util/Properties;-><init>()V

    sget-object v5, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    if-nez v5, :cond_1

    const-string v5, "org.apache.tools.ant.Main"

    invoke-static {v5}, Lorg/apache/tools/ant/Main;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    sput-object v5, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    :goto_0
    const-string v7, "/org/apache/tools/ant/version.txt"

    invoke-virtual {v5, v7}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Apache Ant version "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "VERSION"

    invoke-virtual {v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, " compiled on "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, "DATE"

    invoke-virtual {v4, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lorg/apache/tools/ant/Main;->antVersion:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    sget-object v5, Lorg/apache/tools/ant/Main;->antVersion:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v6

    return-object v5

    :cond_1
    :try_start_3
    sget-object v5, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    new-instance v5, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Could not load the version information:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    :catch_1
    move-exception v3

    :try_start_5
    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Could not load the version information."

    invoke-direct {v5, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private getParentFile(Ljava/io/File;)Ljava/io/File;
    .locals 4
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Searching in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method private static handleLogfile()V
    .locals 1

    sget-boolean v0, Lorg/apache/tools/ant/Main;->isLogFileUsed:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/tools/ant/Main;->out:Ljava/io/PrintStream;

    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    sget-object v0, Lorg/apache/tools/ant/Main;->err:Ljava/io/PrintStream;

    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    :cond_0
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 1
    .param p0    # [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0, v0, v0}, Lorg/apache/tools/ant/Main;->start([Ljava/lang/String;Ljava/util/Properties;Ljava/lang/ClassLoader;)V

    return-void
.end method

.method private static printDescription(Lorg/apache/tools/ant/Project;)V
    .locals 1
    .param p0    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/Project;->getDescription()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Project;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static printMessage(Ljava/lang/Throwable;)V
    .locals 2
    .param p0    # Ljava/lang/Throwable;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static printTargets(Lorg/apache/tools/ant/Project;Ljava/util/Vector;Ljava/util/Vector;Ljava/lang/String;I)V
    .locals 6
    .param p0    # Lorg/apache/tools/ant/Project;
    .param p1    # Ljava/util/Vector;
    .param p2    # Ljava/util/Vector;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "    "

    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v4, p4, :cond_0

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    if-eqz p2, :cond_1

    const/4 v5, 0x0

    invoke-virtual {p1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int v4, p4, v4

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    return-void
.end method

.method private static printTargets(Lorg/apache/tools/ant/Project;Z)V
    .locals 13
    .param p0    # Lorg/apache/tools/ant/Project;
    .param p1    # Z

    const/4 v2, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Project;->getTargets()Ljava/util/Hashtable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v4

    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/Target;

    invoke-virtual {v0}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v0}, Lorg/apache/tools/ant/Target;->getDescription()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    invoke-static {v5, v7}, Lorg/apache/tools/ant/Main;->findTargetPosition(Ljava/util/Vector;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v5, v7, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_1
    invoke-static {v9, v7}, Lorg/apache/tools/ant/Main;->findTargetPosition(Ljava/util/Vector;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v9, v7, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    invoke-virtual {v8, v6, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-le v10, v2, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_0

    :cond_2
    const-string v10, "Main targets:"

    invoke-static {p0, v9, v8, v10, v2}, Lorg/apache/tools/ant/Main;->printTargets(Lorg/apache/tools/ant/Project;Ljava/util/Vector;Ljava/util/Vector;Ljava/lang/String;I)V

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v10

    if-nez v10, :cond_3

    const/4 p1, 0x1

    :cond_3
    if-eqz p1, :cond_4

    const/4 v10, 0x0

    const-string v11, "Other targets:"

    const/4 v12, 0x0

    invoke-static {p0, v5, v10, v11, v12}, Lorg/apache/tools/ant/Main;->printTargets(Lorg/apache/tools/ant/Project;Ljava/util/Vector;Ljava/util/Vector;Ljava/lang/String;I)V

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/Project;->getDefaultTarget()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v10, ""

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Default target: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method private static printUsage()V
    .locals 4

    const-string v2, "line.separator"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "ant [options] [target [target2 [target3] ...]]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Options: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -help, -h              print this message"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -projecthelp, -p       print project help information"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -version               print the version information and exit"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -diagnostics           print information that might be helpful to"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "                         diagnose or report problems."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -quiet, -q             be extra quiet"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -verbose, -v           be extra verbose"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -debug, -d             print debugging information"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -emacs, -e             produce logging information without adornments"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -lib <path>            specifies a path to search for jars and classes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -logfile <file>        use given file for log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "    -l     <file>                \'\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -logger <classname>    the class which is to perform logging"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -listener <classname>  add an instance of class as a project listener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -noinput               do not allow interactive input"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -buildfile <file>      use given buildfile"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "    -file    <file>              \'\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "    -f       <file>              \'\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -D<property>=<value>   use value for given property"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -keep-going, -k        execute all targets that do not depend"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "                         on failed target(s)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -propertyfile <name>   load all properties from file with -D"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "                         properties taking precedence"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -inputhandler <class>  the class which will handle input requests"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -find <file>           (s)earch for buildfile towards the root of"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "    -s  <file>           the filesystem and use it"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -nice  number          A niceness value for the main thread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "                         1 (lowest) to 10 (highest); 5 is the default"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -nouserlib             Run ant without using the jar files from"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "                         ${user.home}/.ant/lib"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -noclasspath           Run ant without using CLASSPATH"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "  -autoproxy             Java1.5+: use the OS proxy settings"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "  -main <class>          override Ant\'s normal entry point"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private static printVersion()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {}, Lorg/apache/tools/ant/Main;->getAntVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private processArgs([Ljava/lang/String;)V
    .locals 27
    .param p1    # [Ljava/lang/String;

    const/16 v21, 0x0

    const/4 v13, 0x0

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    const-string v23, "-lib"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v23, "-cp"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v23, "-noclasspath"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v23, "--noclasspath"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v23, "-nouserlib"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v23, "--nouserlib"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v23, "-main"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v9, 0x0

    :goto_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v9, v0, :cond_27

    aget-object v4, p1, v9

    const-string v23, "-help"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_0

    const-string v23, "-h"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    :cond_0
    invoke-static {}, Lorg/apache/tools/ant/Main;->printUsage()V

    :goto_1
    return-void

    :cond_1
    const-string v23, "-version"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    invoke-static {}, Lorg/apache/tools/ant/Main;->printVersion()V

    goto :goto_1

    :cond_2
    const-string v23, "-diagnostics"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static/range {v23 .. v23}, Lorg/apache/tools/ant/Diagnostics;->doReport(Ljava/io/PrintStream;)V

    goto :goto_1

    :cond_3
    const-string v23, "-quiet"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_4

    const-string v23, "-q"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    :cond_4
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    :cond_5
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_6
    const-string v23, "-verbose"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_7

    const-string v23, "-v"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    :cond_7
    invoke-static {}, Lorg/apache/tools/ant/Main;->printVersion()V

    const/16 v23, 0x3

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    goto :goto_2

    :cond_8
    const-string v23, "-debug"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_9

    const-string v23, "-d"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    :cond_9
    invoke-static {}, Lorg/apache/tools/ant/Main;->printVersion()V

    const/16 v23, 0x4

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    goto :goto_2

    :cond_a
    const-string v23, "-noinput"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/Main;->allowInput:Z

    goto :goto_2

    :cond_b
    const-string v23, "-logfile"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_c

    const-string v23, "-l"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    :cond_c
    :try_start_0
    new-instance v12, Ljava/io/File;

    add-int/lit8 v23, v9, 0x1

    aget-object v23, p1, v23

    move-object/from16 v0, v23

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    add-int/lit8 v9, v9, 0x1

    new-instance v14, Ljava/io/PrintStream;

    new-instance v23, Ljava/io/FileOutputStream;

    move-object/from16 v0, v23

    invoke-direct {v0, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v23

    invoke-direct {v14, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v23, 0x1

    :try_start_1
    sput-boolean v23, Lorg/apache/tools/ant/Main;->isLogFileUsed:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_b

    move-object v13, v14

    goto/16 :goto_2

    :catch_0
    move-exception v10

    :goto_3
    const-string v15, "Cannot write on the specified log file. Make sure the path exists and you have write permissions."

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v23

    invoke-direct {v0, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :catch_1
    move-exception v3

    :goto_4
    const-string v15, "You must specify a log file when using the -log argument"

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v23

    invoke-direct {v0, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_d
    const-string v23, "-buildfile"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_e

    const-string v23, "-file"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_e

    const-string v23, "-f"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    :cond_e
    :try_start_2
    new-instance v23, Ljava/io/File;

    add-int/lit8 v24, v9, 0x1

    aget-object v24, p1, v24

    const/16 v25, 0x2f

    sget-char v26, Ljava/io/File;->separatorChar:C

    invoke-virtual/range {v24 .. v26}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    :catch_2
    move-exception v3

    const-string v15, "You must specify a buildfile when using the -buildfile argument"

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v23

    invoke-direct {v0, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_f
    const-string v23, "-listener"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_10

    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->listeners:Ljava/util/Vector;

    move-object/from16 v23, v0

    add-int/lit8 v24, v9, 0x1

    aget-object v24, p1, v24

    invoke-virtual/range {v23 .. v24}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_3

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    :catch_3
    move-exception v3

    const-string v15, "You must specify a classname when using the -listener argument"

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v23

    invoke-direct {v0, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_10
    const-string v23, "-D"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_13

    const/16 v23, 0x2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    const/16 v22, 0x0

    const-string v23, "="

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    if-lez v17, :cond_11

    add-int/lit8 v23, v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v23

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_11
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    if-ge v9, v0, :cond_12

    add-int/lit8 v9, v9, 0x1

    aget-object v22, p1, v9

    goto :goto_5

    :cond_12
    new-instance v23, Lorg/apache/tools/ant/BuildException;

    new-instance v24, Ljava/lang/StringBuffer;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuffer;-><init>()V

    const-string v25, "Missing value for property "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_13
    const-string v23, "-logger"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->loggerClassname:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_14

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "Only one logger class may  be specified."

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_14
    add-int/lit8 v9, v9, 0x1

    :try_start_4
    aget-object v23, p1, v9

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/Main;->loggerClassname:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_2

    :catch_4
    move-exception v3

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "You must specify a classname when using the -logger argument"

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_15
    const-string v23, "-inputhandler"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->inputHandlerClassname:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_16

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "Only one input handler class may be specified."

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_16
    add-int/lit8 v9, v9, 0x1

    :try_start_5
    aget-object v23, p1, v9

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/Main;->inputHandlerClassname:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_2

    :catch_5
    move-exception v3

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "You must specify a classname when using the -inputhandler argument"

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_17
    const-string v23, "-emacs"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_18

    const-string v23, "-e"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_19

    :cond_18
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/Main;->emacsMode:Z

    goto/16 :goto_2

    :cond_19
    const-string v23, "-projecthelp"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_1a

    const-string v23, "-p"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1b

    :cond_1a
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/Main;->projectHelp:Z

    goto/16 :goto_2

    :cond_1b
    const-string v23, "-find"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_1c

    const-string v23, "-s"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1e

    :cond_1c
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    if-ge v9, v0, :cond_1d

    add-int/lit8 v9, v9, 0x1

    aget-object v21, p1, v9

    goto/16 :goto_2

    :cond_1d
    const-string v21, "build.xml"

    goto/16 :goto_2

    :cond_1e
    const-string v23, "-propertyfile"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1f

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->propertyFiles:Ljava/util/Vector;

    move-object/from16 v23, v0

    add-int/lit8 v24, v9, 0x1

    aget-object v24, p1, v24

    invoke-virtual/range {v23 .. v24}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_6

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    :catch_6
    move-exception v3

    const-string v15, "You must specify a property filename when using the -propertyfile argument"

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v23

    invoke-direct {v0, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_1f
    const-string v23, "-k"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_20

    const-string v23, "-keep-going"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_21

    :cond_20
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/Main;->keepGoingMode:Z

    goto/16 :goto_2

    :cond_21
    const-string v23, "-nice"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_23

    add-int/lit8 v23, v9, 0x1

    :try_start_7
    aget-object v23, p1, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;
    :try_end_7
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_8

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    const/16 v24, 0xa

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_5

    :cond_22
    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "Niceness value is out of the range 1-10"

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :catch_7
    move-exception v3

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "You must supply a niceness value (1-10) after the -nice option"

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :catch_8
    move-exception v5

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    new-instance v24, Ljava/lang/StringBuffer;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuffer;-><init>()V

    const-string v25, "Unrecognized niceness value: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    add-int/lit8 v25, v9, 0x1

    aget-object v25, p1, v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_23
    invoke-virtual {v11, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    if-eqz v23, :cond_24

    new-instance v23, Ljava/lang/StringBuffer;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuffer;-><init>()V

    const-string v24, "Ant\'s Main method is being handed an option "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    const-string v24, " that is only for the launcher class."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    const-string v24, "\nThis can be caused by a version mismatch between "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    const-string v24, "the ant script/.bat file and Ant itself."

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v23

    invoke-direct {v0, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_24
    const-string v23, "-autoproxy"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_25

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/Main;->proxy:Z

    goto/16 :goto_2

    :cond_25
    const-string v23, "-"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_26

    new-instance v23, Ljava/lang/StringBuffer;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuffer;-><init>()V

    const-string v24, "Unknown argument: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    sget-object v23, Ljava/lang/System;->err:Ljava/io/PrintStream;

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lorg/apache/tools/ant/Main;->printUsage()V

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, ""

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->targets:Ljava/util/Vector;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    move-object/from16 v23, v0

    if-nez v23, :cond_28

    if-eqz v21, :cond_29

    const-string v23, "user.dir"

    invoke-static/range {v23 .. v23}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/Main;->findBuildFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    :cond_28
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    move-result v23

    if-nez v23, :cond_2a

    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v24, Ljava/lang/StringBuffer;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuffer;-><init>()V

    const-string v25, "Buildfile: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v24

    const-string v25, " does not exist!"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "Build failed"

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_29
    new-instance v23, Ljava/io/File;

    const-string v24, "build.xml"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    goto :goto_6

    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->isDirectory()Z

    move-result v23

    if-eqz v23, :cond_2b

    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v24, Ljava/lang/StringBuffer;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuffer;-><init>()V

    const-string v25, "What? Buildfile: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v24

    const-string v25, " is a dir!"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v23, Lorg/apache/tools/ant/BuildException;

    const-string v24, "Build failed"

    invoke-direct/range {v23 .. v24}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v23

    :cond_2b
    const/16 v18, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->propertyFiles:Ljava/util/Vector;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/Vector;->size()I

    move-result v23

    move/from16 v0, v18

    move/from16 v1, v23

    if-ge v0, v1, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->propertyFiles:Ljava/util/Vector;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    new-instance v20, Ljava/util/Properties;

    invoke-direct/range {v20 .. v20}, Ljava/util/Properties;-><init>()V

    const/4 v7, 0x0

    :try_start_8
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_9
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    move-object v7, v8

    :goto_8
    invoke-virtual/range {v20 .. v20}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v19

    :cond_2c
    :goto_9
    invoke-interface/range {v19 .. v19}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v23

    if-eqz v23, :cond_2d

    invoke-interface/range {v19 .. v19}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    if-nez v23, :cond_2c

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    move-object/from16 v23, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    :catch_9
    move-exception v5

    :goto_a
    :try_start_a
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v24, Ljava/lang/StringBuffer;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuffer;-><init>()V

    const-string v25, "Could not load property file "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    const-string v25, ": "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    invoke-static {v7}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto :goto_8

    :catchall_0
    move-exception v23

    :goto_b
    invoke-static {v7}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v23

    :cond_2d
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_7

    :cond_2e
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    move/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_2f

    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v24, Ljava/lang/StringBuffer;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuffer;-><init>()V

    const-string v25, "Buildfile: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2f
    if-eqz v13, :cond_30

    sput-object v13, Lorg/apache/tools/ant/Main;->out:Ljava/io/PrintStream;

    sput-object v13, Lorg/apache/tools/ant/Main;->err:Ljava/io/PrintStream;

    sget-object v23, Lorg/apache/tools/ant/Main;->out:Ljava/io/PrintStream;

    invoke-static/range {v23 .. v23}, Ljava/lang/System;->setOut(Ljava/io/PrintStream;)V

    sget-object v23, Lorg/apache/tools/ant/Main;->err:Ljava/io/PrintStream;

    invoke-static/range {v23 .. v23}, Ljava/lang/System;->setErr(Ljava/io/PrintStream;)V

    :cond_30
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/Main;->readyToRun:Z

    goto/16 :goto_1

    :catchall_1
    move-exception v23

    move-object v7, v8

    goto :goto_b

    :catch_a
    move-exception v5

    move-object v7, v8

    goto :goto_a

    :catch_b
    move-exception v3

    move-object v13, v14

    goto/16 :goto_4

    :catch_c
    move-exception v10

    move-object v13, v14

    goto/16 :goto_3
.end method

.method private runBuild(Ljava/lang/ClassLoader;)V
    .locals 16
    .param p1    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->readyToRun:Z

    if-nez v13, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v6, Lorg/apache/tools/ant/Project;

    invoke-direct {v6}, Lorg/apache/tools/ant/Project;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lorg/apache/tools/ant/Project;->setCoreLoader(Ljava/lang/ClassLoader;)V

    const/4 v3, 0x0

    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lorg/apache/tools/ant/Main;->addBuildListeners(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lorg/apache/tools/ant/Main;->addInputHandler(Lorg/apache/tools/ant/Project;)V

    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v9, Ljava/lang/System;->in:Ljava/io/InputStream;

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->allowInput:Z

    if-eqz v13, :cond_2

    sget-object v13, Ljava/lang/System;->in:Ljava/io/InputStream;

    invoke-virtual {v6, v13}, Lorg/apache/tools/ant/Project;->setDefaultInputStream(Ljava/io/InputStream;)V

    :cond_2
    new-instance v13, Lorg/apache/tools/ant/DemuxInputStream;

    invoke-direct {v13, v6}, Lorg/apache/tools/ant/DemuxInputStream;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-static {v13}, Ljava/lang/System;->setIn(Ljava/io/InputStream;)V

    new-instance v13, Ljava/io/PrintStream;

    new-instance v14, Lorg/apache/tools/ant/DemuxOutputStream;

    const/4 v15, 0x0

    invoke-direct {v14, v6, v15}, Lorg/apache/tools/ant/DemuxOutputStream;-><init>(Lorg/apache/tools/ant/Project;Z)V

    invoke-direct {v13, v14}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v13}, Ljava/lang/System;->setOut(Ljava/io/PrintStream;)V

    new-instance v13, Ljava/io/PrintStream;

    new-instance v14, Lorg/apache/tools/ant/DemuxOutputStream;

    const/4 v15, 0x1

    invoke-direct {v14, v6, v15}, Lorg/apache/tools/ant/DemuxOutputStream;-><init>(Lorg/apache/tools/ant/Project;Z)V

    invoke-direct {v13, v14}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v13}, Ljava/lang/System;->setErr(Ljava/io/PrintStream;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->projectHelp:Z

    if-nez v13, :cond_3

    invoke-virtual {v6}, Lorg/apache/tools/ant/Project;->fireBuildStarted()V

    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v13, :cond_4

    :try_start_2
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "Setting Ant\'s thread priority to "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x3

    invoke-virtual {v6, v13, v14}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/Main;->threadPriority:Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/Thread;->setPriority(I)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    :goto_1
    :try_start_3
    invoke-virtual {v6}, Lorg/apache/tools/ant/Project;->init()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    invoke-virtual {v13}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    invoke-virtual {v13, v1}, Ljava/util/Properties;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v6, v1, v12}, Lorg/apache/tools/ant/Project;->setUserProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v13

    if-eqz v5, :cond_5

    :try_start_4
    invoke-static {v5}, Ljava/lang/System;->setSecurityManager(Ljava/lang/SecurityManager;)V

    :cond_5
    invoke-static {v10}, Ljava/lang/System;->setOut(Ljava/io/PrintStream;)V

    invoke-static {v8}, Ljava/lang/System;->setErr(Ljava/io/PrintStream;)V

    invoke-static {v9}, Ljava/lang/System;->setIn(Ljava/io/InputStream;)V

    throw v13
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v4

    move-object v3, v4

    :try_start_5
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v13

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lorg/apache/tools/ant/Main;->projectHelp:Z

    if-nez v14, :cond_10

    invoke-virtual {v6, v3}, Lorg/apache/tools/ant/Project;->fireBuildFinished(Ljava/lang/Throwable;)V

    :cond_6
    :goto_3
    throw v13

    :catch_1
    move-exception v11

    :try_start_6
    const-string v13, "A security manager refused to set the -nice value"

    invoke-virtual {v6, v13}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const-string v13, "ant.file"

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v13, v14}, Lorg/apache/tools/ant/Project;->setUserProperty(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->keepGoingMode:Z

    invoke-virtual {v6, v13}, Lorg/apache/tools/ant/Project;->setKeepGoingMode(Z)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->proxy:Z

    if-eqz v13, :cond_8

    new-instance v7, Lorg/apache/tools/ant/util/ProxySetup;

    invoke-direct {v7, v6}, Lorg/apache/tools/ant/util/ProxySetup;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v7}, Lorg/apache/tools/ant/util/ProxySetup;->enableProxies()V

    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/Main;->buildFile:Ljava/io/File;

    invoke-static {v6, v13}, Lorg/apache/tools/ant/ProjectHelper;->configureProject(Lorg/apache/tools/ant/Project;Ljava/io/File;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->projectHelp:Z

    if-eqz v13, :cond_c

    invoke-static {v6}, Lorg/apache/tools/ant/Main;->printDescription(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/tools/ant/Main;->msgOutputLevel:I

    const/4 v14, 0x2

    if-le v13, v14, :cond_a

    const/4 v13, 0x1

    :goto_4
    invoke-static {v6, v13}, Lorg/apache/tools/ant/Main;->printTargets(Lorg/apache/tools/ant/Project;Z)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v5, :cond_9

    :try_start_7
    invoke-static {v5}, Ljava/lang/System;->setSecurityManager(Ljava/lang/SecurityManager;)V

    :cond_9
    invoke-static {v10}, Ljava/lang/System;->setOut(Ljava/io/PrintStream;)V

    invoke-static {v8}, Ljava/lang/System;->setErr(Ljava/io/PrintStream;)V

    invoke-static {v9}, Ljava/lang/System;->setIn(Ljava/io/InputStream;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Error; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->projectHelp:Z

    if-nez v13, :cond_b

    invoke-virtual {v6, v3}, Lorg/apache/tools/ant/Project;->fireBuildFinished(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_a
    const/4 v13, 0x0

    goto :goto_4

    :cond_b
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v6, v13, v14}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_c
    :try_start_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/Main;->targets:Ljava/util/Vector;

    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    move-result v13

    if-nez v13, :cond_d

    invoke-virtual {v6}, Lorg/apache/tools/ant/Project;->getDefaultTarget()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_d

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/Main;->targets:Ljava/util/Vector;

    invoke-virtual {v6}, Lorg/apache/tools/ant/Project;->getDefaultTarget()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/tools/ant/Main;->targets:Ljava/util/Vector;

    invoke-virtual {v6, v13}, Lorg/apache/tools/ant/Project;->executeTargets(Ljava/util/Vector;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v5, :cond_e

    :try_start_9
    invoke-static {v5}, Ljava/lang/System;->setSecurityManager(Ljava/lang/SecurityManager;)V

    :cond_e
    invoke-static {v10}, Ljava/lang/System;->setOut(Ljava/io/PrintStream;)V

    invoke-static {v8}, Ljava/lang/System;->setErr(Ljava/io/PrintStream;)V

    invoke-static {v9}, Ljava/lang/System;->setIn(Ljava/io/InputStream;)V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Error; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lorg/apache/tools/ant/Main;->projectHelp:Z

    if-nez v13, :cond_f

    invoke-virtual {v6, v3}, Lorg/apache/tools/ant/Project;->fireBuildFinished(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_f
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v6, v13, v14}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :catch_2
    move-exception v2

    move-object v3, v2

    :try_start_a
    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_10
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v6, v14, v15}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    goto/16 :goto_3
.end method

.method public static start([Ljava/lang/String;Ljava/util/Properties;Ljava/lang/ClassLoader;)V
    .locals 1
    .param p0    # [Ljava/lang/String;
    .param p1    # Ljava/util/Properties;
    .param p2    # Ljava/lang/ClassLoader;

    new-instance v0, Lorg/apache/tools/ant/Main;

    invoke-direct {v0}, Lorg/apache/tools/ant/Main;-><init>()V

    invoke-virtual {v0, p0, p1, p2}, Lorg/apache/tools/ant/Main;->startAnt([Ljava/lang/String;Ljava/util/Properties;Ljava/lang/ClassLoader;)V

    return-void
.end method


# virtual methods
.method protected addBuildListeners(Lorg/apache/tools/ant/Project;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-direct {p0}, Lorg/apache/tools/ant/Main;->createLogger()Lorg/apache/tools/ant/BuildLogger;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/Project;->addBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/Main;->listeners:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/Main;->listeners:Ljava/util/Vector;

    invoke-virtual {v3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v3, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    if-nez v3, :cond_1

    const-string v3, "org.apache.tools.ant.Main"

    invoke-static {v3}, Lorg/apache/tools/ant/Main;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    sget-object v3, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$BuildListener:Ljava/lang/Class;

    if-nez v3, :cond_2

    const-string v3, "org.apache.tools.ant.BuildListener"

    invoke-static {v3}, Lorg/apache/tools/ant/Main;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$BuildListener:Ljava/lang/Class;

    :goto_2
    invoke-static {v0, v4, v3}, Lorg/apache/tools/ant/util/ClasspathUtils;->newInstance(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/BuildListener;

    if-eqz p1, :cond_0

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/Project;->setProjectReference(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/Project;->addBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v3, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$Main:Ljava/lang/Class;

    goto :goto_1

    :cond_2
    sget-object v3, Lorg/apache/tools/ant/Main;->class$org$apache$tools$ant$BuildListener:Ljava/lang/Class;

    goto :goto_2

    :cond_3
    return-void
.end method

.method protected exit(I)V
    .locals 0
    .param p1    # I

    invoke-static {p1}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public startAnt([Ljava/lang/String;Ljava/util/Properties;Ljava/lang/ClassLoader;)V
    .locals 9
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/util/Properties;
    .param p3    # Ljava/lang/ClassLoader;

    :try_start_0
    invoke-static {}, Lorg/apache/tools/ant/Diagnostics;->validateVersion()V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/Main;->processArgs([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p2, v5}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/Main;->definedProps:Ljava/util/Properties;

    invoke-virtual {v7, v5, v6}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-static {}, Lorg/apache/tools/ant/Main;->handleLogfile()V

    invoke-static {v3}, Lorg/apache/tools/ant/Main;->printMessage(Ljava/lang/Throwable;)V

    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lorg/apache/tools/ant/Main;->exit(I)V

    :goto_1
    return-void

    :cond_0
    const/4 v4, 0x1

    :try_start_1
    invoke-direct {p0, p3}, Lorg/apache/tools/ant/Main;->runBuild(Ljava/lang/ClassLoader;)V
    :try_end_1
    .catch Lorg/apache/tools/ant/ExitStatusException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x0

    :cond_1
    invoke-static {}, Lorg/apache/tools/ant/Main;->handleLogfile()V

    :goto_2
    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/Main;->exit(I)V

    goto :goto_1

    :catch_1
    move-exception v2

    :try_start_2
    invoke-virtual {v2}, Lorg/apache/tools/ant/ExitStatusException;->getStatus()I

    move-result v4

    if-eqz v4, :cond_1

    throw v2
    :try_end_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_2
    move-exception v0

    :try_start_3
    sget-object v7, Lorg/apache/tools/ant/Main;->err:Ljava/io/PrintStream;

    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    if-eq v7, v8, :cond_2

    invoke-static {v0}, Lorg/apache/tools/ant/Main;->printMessage(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    invoke-static {}, Lorg/apache/tools/ant/Main;->handleLogfile()V

    goto :goto_2

    :catch_3
    move-exception v3

    :try_start_4
    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-static {v3}, Lorg/apache/tools/ant/Main;->printMessage(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {}, Lorg/apache/tools/ant/Main;->handleLogfile()V

    goto :goto_2

    :catchall_0
    move-exception v7

    invoke-static {}, Lorg/apache/tools/ant/Main;->handleLogfile()V

    throw v7
.end method
