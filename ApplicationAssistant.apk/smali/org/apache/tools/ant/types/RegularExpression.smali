.class public Lorg/apache/tools/ant/types/RegularExpression;
.super Lorg/apache/tools/ant/types/DataType;
.source "RegularExpression.java"


# static fields
.field public static final DATA_TYPE_NAME:Ljava/lang/String; = "regexp"

.field private static final FACTORY:Lorg/apache/tools/ant/util/regexp/RegexpFactory;


# instance fields
.field private alreadyInit:Z

.field private myPattern:Ljava/lang/String;

.field private regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

.field private setPatternPending:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/util/regexp/RegexpFactory;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/types/RegularExpression;->FACTORY:Lorg/apache/tools/ant/util/regexp/RegexpFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/RegularExpression;->alreadyInit:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/RegularExpression;->setPatternPending:Z

    return-void
.end method

.method private init(Lorg/apache/tools/ant/Project;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->alreadyInit:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/tools/ant/types/RegularExpression;->FACTORY:Lorg/apache/tools/ant/util/regexp/RegexpFactory;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->newRegexp(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/Regexp;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->alreadyInit:Z

    :cond_0
    return-void
.end method

.method private setPattern()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->setPatternPending:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

    iget-object v1, p0, Lorg/apache/tools/ant/types/RegularExpression;->myPattern:Ljava/lang/String;

    invoke-interface {v0, v1}, Lorg/apache/tools/ant/util/regexp/Regexp;->setPattern(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->setPatternPending:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public getPattern(Lorg/apache/tools/ant/Project;)Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->init(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RegularExpression;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/RegularExpression;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->getPattern(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/RegularExpression;->setPattern()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

    invoke-interface {v0}, Lorg/apache/tools/ant/util/regexp/Regexp;->getPattern()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/RegularExpression;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->getCheckedRef(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/RegularExpression;

    return-object v0
.end method

.method public getRegexp(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/Regexp;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->init(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RegularExpression;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/RegularExpression;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/RegularExpression;->getRegexp(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/Regexp;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/RegularExpression;->setPattern()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

    goto :goto_0
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/types/RegularExpression;->myPattern:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->setPatternPending:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/RegularExpression;->regexp:Lorg/apache/tools/ant/util/regexp/Regexp;

    invoke-interface {v0, p1}, Lorg/apache/tools/ant/util/regexp/Regexp;->setPattern(Ljava/lang/String;)V

    goto :goto_0
.end method
