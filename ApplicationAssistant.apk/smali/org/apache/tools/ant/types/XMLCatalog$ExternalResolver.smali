.class Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;
.super Ljava/lang/Object;
.source "XMLCatalog.java"

# interfaces
.implements Lorg/apache/tools/ant/types/XMLCatalog$CatalogResolver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/XMLCatalog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExternalResolver"
.end annotation


# instance fields
.field private externalCatalogsProcessed:Z

.field private parseCatalog:Ljava/lang/reflect/Method;

.field private resolve:Ljava/lang/reflect/Method;

.field private resolveEntity:Ljava/lang/reflect/Method;

.field private resolverImpl:Ljava/lang/Object;

.field private setXMLCatalog:Ljava/lang/reflect/Method;

.field private final this$0:Lorg/apache/tools/ant/types/XMLCatalog;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/types/XMLCatalog;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 5
    .param p2    # Ljava/lang/Class;
    .param p3    # Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    iput-object v2, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->setXMLCatalog:Ljava/lang/reflect/Method;

    iput-object v2, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->parseCatalog:Ljava/lang/reflect/Method;

    iput-object v2, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolveEntity:Ljava/lang/reflect/Method;

    iput-object v2, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolve:Ljava/lang/reflect/Method;

    iput-object v2, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->externalCatalogsProcessed:Z

    iput-object p3, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    :try_start_0
    const-string v2, "setXMLCatalog"

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$org$apache$tools$ant$types$XMLCatalog:Ljava/lang/Class;

    if-nez v1, :cond_0

    const-string v1, "org.apache.tools.ant.types.XMLCatalog"

    invoke-static {v1}, Lorg/apache/tools/ant/types/XMLCatalog;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$org$apache$tools$ant$types$XMLCatalog:Ljava/lang/Class;

    :goto_0
    aput-object v1, v3, v4

    invoke-virtual {p2, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->setXMLCatalog:Ljava/lang/reflect/Method;

    const-string v2, "parseCatalog"

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    if-nez v1, :cond_1

    const-string v1, "java.lang.String"

    invoke-static {v1}, Lorg/apache/tools/ant/types/XMLCatalog;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    :goto_1
    aput-object v1, v3, v4

    invoke-virtual {p2, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->parseCatalog:Ljava/lang/reflect/Method;

    const-string v2, "resolveEntity"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    if-nez v1, :cond_2

    const-string v1, "java.lang.String"

    invoke-static {v1}, Lorg/apache/tools/ant/types/XMLCatalog;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    :goto_2
    aput-object v1, v3, v4

    const/4 v4, 0x1

    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    if-nez v1, :cond_3

    const-string v1, "java.lang.String"

    invoke-static {v1}, Lorg/apache/tools/ant/types/XMLCatalog;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    :goto_3
    aput-object v1, v3, v4

    invoke-virtual {p2, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolveEntity:Ljava/lang/reflect/Method;

    const-string v2, "resolve"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    if-nez v1, :cond_4

    const-string v1, "java.lang.String"

    invoke-static {v1}, Lorg/apache/tools/ant/types/XMLCatalog;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    :goto_4
    aput-object v1, v3, v4

    const/4 v4, 0x1

    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    if-nez v1, :cond_5

    const-string v1, "java.lang.String"

    invoke-static {v1}, Lorg/apache/tools/ant/types/XMLCatalog;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    :goto_5
    aput-object v1, v3, v4

    invoke-virtual {p2, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolve:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "Apache resolver library found, xml-commons resolver will be used"

    const/4 v2, 0x3

    invoke-virtual {p1, v1, v2}, Lorg/apache/tools/ant/types/XMLCatalog;->log(Ljava/lang/String;I)V

    return-void

    :cond_0
    :try_start_1
    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$org$apache$tools$ant$types$XMLCatalog:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_1

    :cond_2
    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_2

    :cond_3
    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_3

    :cond_4
    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_4

    :cond_5
    sget-object v1, Lorg/apache/tools/ant/types/XMLCatalog;->class$java$lang$String:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private processExternalCatalogs()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x1

    iget-boolean v5, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->externalCatalogsProcessed:Z

    if-nez v5, :cond_0

    :try_start_0
    iget-object v5, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->setXMLCatalog:Ljava/lang/reflect/Method;

    iget-object v6, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v5, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/XMLCatalog;->getCatalogPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Using catalogpath \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/XMLCatalog;->getCatalogPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v11}, Lorg/apache/tools/ant/types/XMLCatalog;->log(Ljava/lang/String;I)V

    iget-object v5, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/XMLCatalog;->getCatalogPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Path;->list()[Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    :goto_0
    array-length v5, v2

    if-ge v4, v5, :cond_0

    new-instance v0, Ljava/io/File;

    aget-object v5, v2, v4

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Parsing "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v11}, Lorg/apache/tools/ant/types/XMLCatalog;->log(Ljava/lang/String;I)V

    :try_start_1
    iget-object v5, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->parseCatalog:Ljava/lang/reflect/Method;

    iget-object v6, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v3

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :cond_0
    iput-boolean v10, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->externalCatalogsProcessed:Z

    return-void
.end method


# virtual methods
.method public resolve(Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/transform/Source;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/transform/TransformerException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->processExternalCatalogs()V

    iget-object v7, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-static {v7, p1}, Lorg/apache/tools/ant/types/XMLCatalog;->access$000(Lorg/apache/tools/ant/types/XMLCatalog;Ljava/lang/String;)Lorg/apache/tools/ant/types/ResourceLocation;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v7, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Matching catalog entry found for uri: \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/ResourceLocation;->getPublicId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\' location: \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/ResourceLocation;->getLocation()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v7, v8, v9}, Lorg/apache/tools/ant/types/XMLCatalog;->log(Ljava/lang/String;I)V

    move-object v1, v4

    if-eqz p2, :cond_0

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    new-instance v2, Lorg/apache/tools/ant/types/ResourceLocation;

    invoke-direct {v2}, Lorg/apache/tools/ant/types/ResourceLocation;-><init>()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/types/ResourceLocation;->setBase(Ljava/net/URL;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v1, v2

    :cond_0
    :goto_0
    invoke-virtual {v4}, Lorg/apache/tools/ant/types/ResourceLocation;->getPublicId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lorg/apache/tools/ant/types/ResourceLocation;->setPublicId(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/ResourceLocation;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lorg/apache/tools/ant/types/ResourceLocation;->setLocation(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-static {v7, v1}, Lorg/apache/tools/ant/types/XMLCatalog;->access$100(Lorg/apache/tools/ant/types/XMLCatalog;Lorg/apache/tools/ant/types/ResourceLocation;)Lorg/xml/sax/InputSource;

    move-result-object v6

    if-nez v6, :cond_1

    iget-object v7, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-static {v7, v1}, Lorg/apache/tools/ant/types/XMLCatalog;->access$200(Lorg/apache/tools/ant/types/XMLCatalog;Lorg/apache/tools/ant/types/ResourceLocation;)Lorg/xml/sax/InputSource;

    move-result-object v6

    :cond_1
    if-eqz v6, :cond_2

    new-instance v5, Ljavax/xml/transform/sax/SAXSource;

    invoke-direct {v5, v6}, Ljavax/xml/transform/sax/SAXSource;-><init>(Lorg/xml/sax/InputSource;)V

    :goto_1
    return-object v5

    :cond_2
    :try_start_2
    iget-object v7, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolve:Ljava/lang/reflect/Method;

    iget-object v8, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    const/4 v10, 0x1

    aput-object p2, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljavax/xml/transform/sax/SAXSource;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v7, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :cond_3
    :try_start_3
    iget-object v7, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolve:Ljava/lang/reflect/Method;

    iget-object v8, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    const/4 v10, 0x1

    aput-object p2, v9, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljavax/xml/transform/sax/SAXSource;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v3

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v7, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    :catch_2
    move-exception v7

    goto :goto_0

    :catch_3
    move-exception v7

    move-object v1, v2

    goto :goto_0
.end method

.method public resolveEntity(Ljava/lang/String;Ljava/lang/String;)Lorg/xml/sax/InputSource;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->processExternalCatalogs()V

    iget-object v3, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-static {v3, p1}, Lorg/apache/tools/ant/types/XMLCatalog;->access$000(Lorg/apache/tools/ant/types/XMLCatalog;Ljava/lang/String;)Lorg/apache/tools/ant/types/ResourceLocation;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Matching catalog entry found for publicId: \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/ResourceLocation;->getPublicId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\' location: \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/ResourceLocation;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Lorg/apache/tools/ant/types/XMLCatalog;->log(Ljava/lang/String;I)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-static {v3, v1}, Lorg/apache/tools/ant/types/XMLCatalog;->access$100(Lorg/apache/tools/ant/types/XMLCatalog;Lorg/apache/tools/ant/types/ResourceLocation;)Lorg/xml/sax/InputSource;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->this$0:Lorg/apache/tools/ant/types/XMLCatalog;

    invoke-static {v3, v1}, Lorg/apache/tools/ant/types/XMLCatalog;->access$200(Lorg/apache/tools/ant/types/XMLCatalog;Lorg/apache/tools/ant/types/ResourceLocation;)Lorg/xml/sax/InputSource;

    move-result-object v2

    :cond_0
    if-nez v2, :cond_1

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolveEntity:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/xml/sax/InputSource;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :cond_2
    :try_start_1
    iget-object v3, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolveEntity:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lorg/apache/tools/ant/types/XMLCatalog$ExternalResolver;->resolverImpl:Ljava/lang/Object;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/xml/sax/InputSource;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method
