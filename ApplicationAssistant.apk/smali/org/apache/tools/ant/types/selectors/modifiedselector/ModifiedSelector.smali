.class public Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;
.super Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;
.source "ModifiedSelector.java"

# interfaces
.implements Lorg/apache/tools/ant/BuildListener;
.implements Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;,
        Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;,
        Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;
    }
.end annotation


# static fields
.field static class$java$util$Comparator:Ljava/lang/Class;

.field static class$org$apache$tools$ant$types$selectors$modifiedselector$Algorithm:Ljava/lang/Class;

.field static class$org$apache$tools$ant$types$selectors$modifiedselector$Cache:Ljava/lang/Class;


# instance fields
.field private algoName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

.field private algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

.field private algorithmClass:Ljava/lang/String;

.field private cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

.field private cacheClass:Ljava/lang/String;

.field private cacheName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;

.field private classpath:Lorg/apache/tools/ant/types/Path;

.field private compName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

.field private comparator:Ljava/util/Comparator;

.field private comparatorClass:Ljava/lang/String;

.field private configParameter:Ljava/util/Vector;

.field private delayUpdate:Z

.field private isConfigured:Z

.field private modified:I

.field private myClassLoader:Ljava/lang/ClassLoader;

.field private selectDirectories:Z

.field private selectResourcesWithoutInputStream:Z

.field private specialParameter:Ljava/util/Vector;

.field private update:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cacheName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algoName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->compName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->update:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectDirectories:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectResourcesWithoutInputStream:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->delayUpdate:Z

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    iput v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->modified:I

    iput-boolean v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->isConfigured:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->configParameter:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->specialParameter:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->myClassLoader:Ljava/lang/ClassLoader;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->classpath:Lorg/apache/tools/ant/types/Path;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private isSelected(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->validate()V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-boolean v3, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectDirectories:Z

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    invoke-interface {v4, v1}, Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;->getValue(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    invoke-interface {v4, v0, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    :goto_1
    iget-boolean v4, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->update:Z

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getModified()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setModified(I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getDelayUpdate()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->saveCache()V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public addClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->classpath:Lorg/apache/tools/ant/types/Path;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "<classpath> can be set only once."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->classpath:Lorg/apache/tools/ant/types/Path;

    return-void
.end method

.method public addParam(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    new-instance v0, Lorg/apache/tools/ant/types/Parameter;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Parameter;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Parameter;->setName(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Parameter;->setValue(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->configParameter:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addParam(Lorg/apache/tools/ant/types/Parameter;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Parameter;

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->configParameter:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public buildFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getDelayUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->saveCache()V

    :cond_0
    return-void
.end method

.method public buildStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public configure()V
    .locals 12

    const/4 v10, 0x1

    iget-boolean v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->isConfigured:Z

    if-eqz v9, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->isConfigured:Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    const-string v4, "cache.properties"

    const/4 v0, 0x0

    if-eqz v7, :cond_1

    new-instance v0, Ljava/io/File;

    invoke-virtual {v7}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v9

    invoke-direct {v0, v9, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-virtual {v9, p0}, Lorg/apache/tools/ant/Project;->addBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    :goto_1
    new-instance v2, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;-><init>(Ljava/io/File;)V

    new-instance v1, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;-><init>()V

    new-instance v3, Lorg/apache/tools/ant/types/selectors/modifiedselector/EqualComparator;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/selectors/modifiedselector/EqualComparator;-><init>()V

    iput-boolean v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->update:Z

    iput-boolean v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectDirectories:Z

    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->configParameter:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/types/Parameter;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-lez v9, :cond_2

    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->specialParameter:Ljava/util/Vector;

    invoke-virtual {v9, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setDelayUpdate(Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->useParameter(Lorg/apache/tools/ant/types/Parameter;)V

    goto :goto_2

    :cond_3
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->configParameter:Ljava/util/Vector;

    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algoName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    if-eqz v9, :cond_9

    const-string v9, "hashvalue"

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algoName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    new-instance v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/HashvalueAlgorithm;

    invoke-direct {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/HashvalueAlgorithm;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    :cond_4
    :goto_3
    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cacheName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;

    if-eqz v9, :cond_c

    const-string v9, "propertyfile"

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cacheName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    new-instance v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;

    invoke-direct {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    :cond_5
    :goto_4
    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->compName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

    if-eqz v9, :cond_10

    const-string v9, "equal"

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->compName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    new-instance v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/EqualComparator;

    invoke-direct {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/EqualComparator;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    :cond_6
    :goto_5
    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->specialParameter:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_13

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/types/Parameter;

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->useParameter(Lorg/apache/tools/ant/types/Parameter;)V

    goto :goto_6

    :cond_7
    const-string v9, "digest"

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algoName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    new-instance v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;

    invoke-direct {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    goto :goto_3

    :cond_8
    const-string v9, "checksum"

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algoName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    new-instance v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;

    invoke-direct {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    goto :goto_3

    :cond_9
    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithmClass:Ljava/lang/String;

    if-eqz v9, :cond_b

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithmClass:Ljava/lang/String;

    const-string v11, "is not an Algorithm."

    sget-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$org$apache$tools$ant$types$selectors$modifiedselector$Algorithm:Ljava/lang/Class;

    if-nez v9, :cond_a

    const-string v9, "org.apache.tools.ant.types.selectors.modifiedselector.Algorithm"

    invoke-static {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    sput-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$org$apache$tools$ant$types$selectors$modifiedselector$Algorithm:Ljava/lang/Class;

    :goto_7
    invoke-virtual {p0, v10, v11, v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->loadClass(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    goto/16 :goto_3

    :cond_a
    sget-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$org$apache$tools$ant$types$selectors$modifiedselector$Algorithm:Ljava/lang/Class;

    goto :goto_7

    :cond_b
    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    goto/16 :goto_3

    :cond_c
    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cacheClass:Ljava/lang/String;

    if-eqz v9, :cond_e

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cacheClass:Ljava/lang/String;

    const-string v11, "is not a Cache."

    sget-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$org$apache$tools$ant$types$selectors$modifiedselector$Cache:Ljava/lang/Class;

    if-nez v9, :cond_d

    const-string v9, "org.apache.tools.ant.types.selectors.modifiedselector.Cache"

    invoke-static {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    sput-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$org$apache$tools$ant$types$selectors$modifiedselector$Cache:Ljava/lang/Class;

    :goto_8
    invoke-virtual {p0, v10, v11, v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->loadClass(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    goto/16 :goto_4

    :cond_d
    sget-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$org$apache$tools$ant$types$selectors$modifiedselector$Cache:Ljava/lang/Class;

    goto :goto_8

    :cond_e
    iput-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    goto/16 :goto_4

    :cond_f
    const-string v9, "rule"

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->compName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    const-string v10, "RuleBasedCollator not yet supported."

    invoke-direct {v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_10
    iget-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparatorClass:Ljava/lang/String;

    if-eqz v9, :cond_12

    iget-object v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparatorClass:Ljava/lang/String;

    const-string v11, "is not a Comparator."

    sget-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$java$util$Comparator:Ljava/lang/Class;

    if-nez v9, :cond_11

    const-string v9, "java.util.Comparator"

    invoke-static {v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    sput-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$java$util$Comparator:Ljava/lang/Class;

    :goto_9
    invoke-virtual {p0, v10, v11, v9}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->loadClass(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Comparator;

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    goto/16 :goto_5

    :cond_11
    sget-object v9, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->class$java$util$Comparator:Ljava/lang/Class;

    goto :goto_9

    :cond_12
    iput-object v3, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    goto/16 :goto_5

    :cond_13
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->specialParameter:Ljava/util/Vector;

    goto/16 :goto_0
.end method

.method public getAlgorithm()Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    return-object v0
.end method

.method public getCache()Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    return-object v0
.end method

.method public getClassLoader()Ljava/lang/ClassLoader;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->myClassLoader:Ljava/lang/ClassLoader;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->myClassLoader:Ljava/lang/ClassLoader;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->myClassLoader:Ljava/lang/ClassLoader;

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Project;->createClassLoader(Lorg/apache/tools/ant/types/Path;)Lorg/apache/tools/ant/AntClassLoader;

    move-result-object v0

    goto :goto_0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    return-object v0
.end method

.method public getDelayUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->delayUpdate:Z

    return v0
.end method

.method public getModified()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->modified:I

    return v0
.end method

.method public isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->isSelected(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSelected(Lorg/apache/tools/ant/types/Resource;)Z
    .locals 13
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isFilesystemOnly()Z

    move-result v10

    if-eqz v10, :cond_0

    move-object v3, p1

    check-cast v3, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/FileResource;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, v0, v4, v2}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z

    move-result v6

    :goto_0
    return v6

    :cond_0
    :try_start_0
    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v5

    const-string v10, "modified-"

    const-string v11, ".tmp"

    const/4 v12, 0x0

    invoke-virtual {v5, v10, v11, v12}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    new-instance v8, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v8, v7}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    invoke-static {p1, v8}, Lorg/apache/tools/ant/util/ResourceUtils;->copyResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;)V

    invoke-virtual {v7}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->toLongString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v10, v11, v12}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->isSelected(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v9

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "The resource \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\' does not provide an InputStream, so it is not checked. "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "Akkording to \'selres\' attribute value it is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-boolean v10, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectResourcesWithoutInputStream:Z

    if-eqz v10, :cond_1

    const-string v10, ""

    :goto_1
    invoke-virtual {v11, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "selected."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->log(Ljava/lang/String;I)V

    iget-boolean v6, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectResourcesWithoutInputStream:Z

    goto :goto_0

    :cond_1
    const-string v10, " not"

    goto :goto_1

    :catch_1
    move-exception v1

    new-instance v10, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v10, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v10
.end method

.method protected loadClass(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Class;

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Specified class ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v2

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Specified class ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") not found."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    :try_start_1
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    goto :goto_0

    :catch_1
    move-exception v2

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v4, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :cond_1
    return-object v3
.end method

.method public messageLogged(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method protected saveCache()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getModified()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    invoke-interface {v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;->save()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setModified(I)V

    :cond_0
    return-void
.end method

.method public setAlgorithm(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algoName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    return-void
.end method

.method public setAlgorithmClass(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithmClass:Ljava/lang/String;

    return-void
.end method

.method public setCache(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cacheName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;

    return-void
.end method

.method public setCacheClass(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cacheClass:Ljava/lang/String;

    return-void
.end method

.method public setClassLoader(Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1    # Ljava/lang/ClassLoader;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->myClassLoader:Ljava/lang/ClassLoader;

    return-void
.end method

.method public setComparator(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->compName:Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

    return-void
.end method

.method public setComparatorClass(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparatorClass:Ljava/lang/String;

    return-void
.end method

.method public setDelayUpdate(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->delayUpdate:Z

    return-void
.end method

.method public setModified(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->modified:I

    return-void
.end method

.method public setParameters([Lorg/apache/tools/ant/types/Parameter;)V
    .locals 3
    .param p1    # [Lorg/apache/tools/ant/types/Parameter;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->configParameter:Ljava/util/Vector;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setSeldirs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectDirectories:Z

    return-void
.end method

.method public setSelres(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectResourcesWithoutInputStream:Z

    return-void
.end method

.method public setUpdate(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->update:Z

    return-void
.end method

.method public targetFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getDelayUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->saveCache()V

    :cond_0
    return-void
.end method

.method public targetStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public taskFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getDelayUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->saveCache()V

    :cond_0
    return-void
.end method

.method public taskStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "{modifiedselector"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, " update="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->update:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v1, " seldirs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->selectDirectories:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    const-string v1, " cache="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, " algorithm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, " comparator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected tryToSetAParameter(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/tools/ant/IntrospectionHelper;->getHelper(Lorg/apache/tools/ant/Project;Ljava/lang/Class;)Lorg/apache/tools/ant/IntrospectionHelper;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0, v1, p1, p2, p3}, Lorg/apache/tools/ant/IntrospectionHelper;->setAttribute(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    new-instance v1, Lorg/apache/tools/ant/Project;

    invoke-direct {v1}, Lorg/apache/tools/ant/Project;-><init>()V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public useParameter(Lorg/apache/tools/ant/types/Parameter;)V
    .locals 9
    .param p1    # Lorg/apache/tools/ant/types/Parameter;

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string v8, "cache"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    new-instance v1, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;-><init>()V

    invoke-virtual {v1, v6}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setCache(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$CacheName;)V

    :goto_0
    return-void

    :cond_0
    const-string v8, "algorithm"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;-><init>()V

    invoke-virtual {v0, v6}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setAlgorithm(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$AlgorithmName;)V

    goto :goto_0

    :cond_1
    const-string v8, "comparator"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    new-instance v1, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;-><init>()V

    invoke-virtual {v1, v6}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setComparator(Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector$ComparatorName;)V

    goto :goto_0

    :cond_2
    const-string v8, "update"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    :goto_1
    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setUpdate(Z)V

    goto :goto_0

    :cond_3
    move v5, v7

    goto :goto_1

    :cond_4
    const-string v8, "delayupdate"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    :goto_2
    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setDelayUpdate(Z)V

    goto :goto_0

    :cond_5
    move v5, v7

    goto :goto_2

    :cond_6
    const-string v8, "seldirs"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    const-string v8, "true"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    move v4, v5

    :goto_3
    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setSeldirs(Z)V

    goto :goto_0

    :cond_7
    move v4, v7

    goto :goto_3

    :cond_8
    const-string v7, "cache."

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    const/4 v7, 0x6

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    invoke-virtual {p0, v7, v3, v6}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->tryToSetAParameter(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_9
    const-string v7, "algorithm."

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    const/16 v7, 0xa

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    invoke-virtual {p0, v7, v3, v6}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->tryToSetAParameter(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v7, "comparator."

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    const/16 v7, 0xb

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->comparator:Ljava/util/Comparator;

    invoke-virtual {p0, v7, v3, v6}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->tryToSetAParameter(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Invalid parameter "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setError(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public verifySettings()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->configure()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    if-nez v0, :cond_1

    const-string v0, "Cache must be set."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setError(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    if-nez v0, :cond_2

    const-string v0, "Algorithm must be set."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setError(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->cache:Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;

    invoke-interface {v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;->isValid()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "Cache must be proper configured."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setError(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->algorithm:Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;

    invoke-interface {v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Algorithm must be proper configured."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ModifiedSelector;->setError(Ljava/lang/String;)V

    goto :goto_0
.end method
