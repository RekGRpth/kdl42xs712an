.class public Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;
.super Ljava/lang/Object;
.source "DigestAlgorithm.java"

# interfaces
.implements Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;


# instance fields
.field private algorithm:Ljava/lang/String;

.field private messageDigest:Ljava/security/MessageDigest;

.field private provider:Ljava/lang/String;

.field private readBufferSize:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MD5"

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->algorithm:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->provider:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->messageDigest:Ljava/security/MessageDigest;

    const/16 v0, 0x2000

    iput v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->readBufferSize:I

    return-void
.end method


# virtual methods
.method public getValue(Ljava/io/File;)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/io/File;

    const/4 v10, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->initMessageDigest()V

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v11

    if-nez v11, :cond_0

    :goto_0
    return-object v10

    :cond_0
    const/4 v6, 0x0

    iget v11, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->readBufferSize:I

    new-array v0, v11, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v11, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->messageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v11}, Ljava/security/MessageDigest;->reset()V

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    new-instance v3, Ljava/security/DigestInputStream;

    iget-object v11, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->messageDigest:Ljava/security/MessageDigest;

    invoke-direct {v3, v7, v11}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V

    :cond_1
    const/4 v11, 0x0

    iget v12, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->readBufferSize:I

    invoke-virtual {v3, v0, v11, v12}, Ljava/security/DigestInputStream;->read([BII)I

    move-result v11

    const/4 v12, -0x1

    if-ne v11, v12, :cond_1

    invoke-virtual {v3}, Ljava/security/DigestInputStream;->close()V

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    const/4 v6, 0x0

    :try_start_3
    iget-object v11, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->messageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v11}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v9, 0x0

    :goto_1
    array-length v11, v5

    if-ge v9, v11, :cond_3

    aget-byte v11, v5, v9

    and-int/lit16 v11, v11, 0xff

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x2

    if-ge v11, v12, :cond_2

    const-string v11, "0"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v1

    move-object v10, v1

    goto :goto_0

    :catch_0
    move-exception v4

    :goto_2
    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_0

    :catch_2
    move-exception v4

    move-object v6, v7

    goto :goto_2
.end method

.method public initMessageDigest()V
    .locals 4

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->messageDigest:Ljava/security/MessageDigest;

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->provider:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, ""

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->provider:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "null"

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->provider:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->algorithm:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->provider:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->messageDigest:Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_1
    :try_start_1
    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->algorithm:Ljava/lang/String;

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->messageDigest:Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public isValid()Z
    .locals 2

    const-string v0, "SHA"

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MD5"

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->algorithm:Ljava/lang/String;

    return-void
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->provider:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "<DigestAlgorithm:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "algorithm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ";provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/DigestAlgorithm;->provider:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
