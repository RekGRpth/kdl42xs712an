.class public Lorg/apache/tools/ant/types/selectors/TypeSelector;
.super Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;
.source "TypeSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;
    }
.end annotation


# static fields
.field public static final TYPE_KEY:Ljava/lang/String; = "type"


# instance fields
.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/TypeSelector;->type:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z
    .locals 2
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/TypeSelector;->validate()V

    invoke-virtual {p3}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/TypeSelector;->type:Ljava/lang/String;

    const-string v1, "dir"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/TypeSelector;->type:Ljava/lang/String;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public setParameters([Lorg/apache/tools/ant/types/Parameter;)V
    .locals 5
    .param p1    # [Lorg/apache/tools/ant/types/Parameter;

    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;->setParameters([Lorg/apache/tools/ant/types/Parameter;)V

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "type"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v2, Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;

    invoke-direct {v2}, Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;-><init>()V

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/selectors/TypeSelector;->setType(Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Invalid parameter "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/TypeSelector;->setError(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public setType(Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/selectors/TypeSelector$FileType;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/TypeSelector;->type:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "{typeselector type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/TypeSelector;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public verifySettings()V
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/TypeSelector;->type:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "The type attribute is required"

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/TypeSelector;->setError(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
