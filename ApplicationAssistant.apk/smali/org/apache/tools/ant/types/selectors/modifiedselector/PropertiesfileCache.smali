.class public Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;
.super Ljava/lang/Object;
.source "PropertiesfileCache.java"

# interfaces
.implements Lorg/apache/tools/ant/types/selectors/modifiedselector/Cache;


# instance fields
.field private cache:Ljava/util/Properties;

.field private cacheDirty:Z

.field private cacheLoaded:Z

.field private cachefile:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheLoaded:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheDirty:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheLoaded:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheDirty:Z

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public delete()V
    .locals 1

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheLoaded:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheDirty:Z

    return-void
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    iget-boolean v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheLoaded:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->load()V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCachefile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    invoke-virtual {v2}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    return-object v2
.end method

.method public load()V
    .locals 4

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    invoke-virtual {v2, v0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheLoaded:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheDirty:Z

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheDirty:Z

    return-void
.end method

.method public save()V
    .locals 4

    iget-boolean v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheDirty:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    invoke-virtual {v2}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Ljava/util/Properties;->store(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cacheDirty:Z

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public setCachefile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "<PropertiesfileCache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "cachefile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cachefile:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const-string v1, ";noOfEntries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/PropertiesfileCache;->cache:Ljava/util/Properties;

    invoke-virtual {v2}, Ljava/util/Properties;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
