.class Lorg/apache/tools/ant/types/resources/Sort$SortedBag;
.super Ljava/util/AbstractCollection;
.source "Sort.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/resources/Sort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SortedBag"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;,
        Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;
    }
.end annotation


# instance fields
.field private size:I

.field private t:Ljava/util/TreeMap;


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .param p1    # Ljava/util/Comparator;

    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->t:Ljava/util/TreeMap;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;)Ljava/util/TreeMap;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/resources/Sort$SortedBag;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->t:Ljava/util/TreeMap;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized add(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    iget v2, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->size:I

    const v3, 0x7fffffff

    if-ge v2, v3, :cond_0

    iget v2, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->size:I

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->t:Ljava/util/TreeMap;

    invoke-virtual {v2, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;

    move-object v0, v2

    check-cast v0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;

    move-object v1, v0

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;-><init>(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;Lorg/apache/tools/ant/types/resources/Sort$1;)V

    iget-object v2, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->t:Ljava/util/TreeMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {v1}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;->access$108(Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x1

    monitor-exit p0

    return v2

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized iterator()Ljava/util/Iterator;
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;-><init>(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;Lorg/apache/tools/ant/types/resources/Sort$1;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized size()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->size:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
