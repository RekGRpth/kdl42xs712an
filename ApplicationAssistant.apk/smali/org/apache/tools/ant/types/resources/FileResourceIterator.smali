.class public Lorg/apache/tools/ant/types/resources/FileResourceIterator;
.super Ljava/lang/Object;
.source "FileResourceIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private basedir:Ljava/io/File;

.field private files:[Ljava/lang/String;

.field private pos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->pos:I

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->pos:I

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->basedir:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/io/File;
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, p2}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->addFiles([Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addFiles([Ljava/lang/String;)V
    .locals 5
    .param p1    # [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->files:[Ljava/lang/String;

    if-nez v3, :cond_1

    move v1, v2

    :goto_0
    array-length v3, p1

    add-int/2addr v3, v1

    new-array v0, v3, [Ljava/lang/String;

    if-lez v1, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->files:[Ljava/lang/String;

    invoke-static {v3, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->files:[Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->files:[Ljava/lang/String;

    array-length v4, p1

    invoke-static {p1, v2, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->files:[Ljava/lang/String;

    array-length v1, v3

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->pos:I

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->files:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->nextResource()Lorg/apache/tools/ant/types/resources/FileResource;

    move-result-object v0

    return-object v0
.end method

.method public nextResource()Lorg/apache/tools/ant/types/resources/FileResource;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->basedir:Ljava/io/File;

    iget-object v2, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->files:[Ljava/lang/String;

    iget v3, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->pos:I

    aget-object v2, v2, v3

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
