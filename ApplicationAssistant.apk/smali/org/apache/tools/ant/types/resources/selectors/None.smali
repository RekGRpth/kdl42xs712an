.class public Lorg/apache/tools/ant/types/resources/selectors/None;
.super Lorg/apache/tools/ant/types/resources/selectors/ResourceSelectorContainer;
.source "None.java"

# interfaces
.implements Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/selectors/ResourceSelectorContainer;-><init>()V

    return-void
.end method

.method public constructor <init>([Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V
    .locals 0
    .param p1    # [Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/resources/selectors/ResourceSelectorContainer;-><init>([Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    return-void
.end method


# virtual methods
.method public isSelected(Lorg/apache/tools/ant/types/Resource;)Z
    .locals 3
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/selectors/None;->getSelectors()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    invoke-interface {v2, p1}, Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;->isSelected(Lorg/apache/tools/ant/types/Resource;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    return v1
.end method
