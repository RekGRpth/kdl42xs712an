.class Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;
.super Ljava/lang/Object;
.source "Sort.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/resources/Sort$SortedBag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyIterator"
.end annotation


# instance fields
.field private current:Ljava/lang/Object;

.field private keyIter:Ljava/util/Iterator;

.field private occurrence:I

.field private final this$0:Lorg/apache/tools/ant/types/resources/Sort$SortedBag;


# direct methods
.method private constructor <init>(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->this$0:Lorg/apache/tools/ant/types/resources/Sort$SortedBag;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->this$0:Lorg/apache/tools/ant/types/resources/Sort$SortedBag;

    invoke-static {v0}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->access$000(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;)Ljava/util/TreeMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->keyIter:Ljava/util/Iterator;

    return-void
.end method

.method constructor <init>(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;Lorg/apache/tools/ant/types/resources/Sort$1;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/resources/Sort$SortedBag;
    .param p2    # Lorg/apache/tools/ant/types/resources/Sort$1;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;-><init>(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized hasNext()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->occurrence:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->keyIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized next()Ljava/lang/Object;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->occurrence:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->keyIter:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->current:Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->this$0:Lorg/apache/tools/ant/types/resources/Sort$SortedBag;

    invoke-static {v0}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag;->access$000(Lorg/apache/tools/ant/types/resources/Sort$SortedBag;)Ljava/util/TreeMap;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->current:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;

    invoke-static {v0}, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;->access$100(Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MutableInt;)I

    move-result v0

    iput v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->occurrence:I

    :cond_1
    iget v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->occurrence:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->occurrence:I

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Sort$SortedBag$MyIterator;->current:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
