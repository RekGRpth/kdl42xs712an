.class public Lorg/apache/tools/ant/types/resources/comparators/Date;
.super Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;
.source "Date.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/comparators/ResourceComparator;-><init>()V

    return-void
.end method


# virtual methods
.method protected resourceCompare(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;)I
    .locals 4
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v0

    invoke-virtual {p2}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
