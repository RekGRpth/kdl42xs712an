.class public Lorg/apache/tools/ant/types/PatternSet$NameEntry;
.super Ljava/lang/Object;
.source "PatternSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/PatternSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NameEntry"
.end annotation


# instance fields
.field private ifCond:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private final this$0:Lorg/apache/tools/ant/types/PatternSet;

.field private unlessCond:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/types/PatternSet;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->this$0:Lorg/apache/tools/ant/types/PatternSet;

    return-void
.end method

.method private valid(Lorg/apache/tools/ant/Project;)Z
    .locals 2
    .param p1    # Lorg/apache/tools/ant/Project;

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->ifCond:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->ifCond:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->unlessCond:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->unlessCond:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public evalName(Lorg/apache/tools/ant/Project;)Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->valid(Lorg/apache/tools/ant/Project;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setIf(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->ifCond:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->name:Ljava/lang/String;

    return-void
.end method

.method public setUnless(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->unlessCond:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->name:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, "noname"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->ifCond:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->unlessCond:Ljava/lang/String;

    if-eqz v2, :cond_2

    :cond_0
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ""

    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->ifCond:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "if->"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->ifCond:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ";"

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->unlessCond:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "unless->"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->unlessCond:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
