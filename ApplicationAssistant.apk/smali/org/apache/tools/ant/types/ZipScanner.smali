.class public Lorg/apache/tools/ant/types/ZipScanner;
.super Lorg/apache/tools/ant/types/ArchiveScanner;
.source "ZipScanner.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;-><init>()V

    return-void
.end method


# virtual methods
.method protected fillMapsFromArchive(Lorg/apache/tools/ant/types/Resource;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 12
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/util/Map;
    .param p4    # Ljava/util/Map;
    .param p5    # Ljava/util/Map;
    .param p6    # Ljava/util/Map;

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    instance-of v9, p1, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v9, :cond_2

    check-cast p1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v6

    :try_start_0
    new-instance v8, Lorg/apache/tools/zip/ZipFile;

    invoke-direct {v8, v6, p2}, Lorg/apache/tools/zip/ZipFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/zip/ZipException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v8}, Lorg/apache/tools/zip/ZipFile;->getEntries()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lorg/apache/tools/zip/ZipEntry;

    move-object v2, v0

    new-instance v5, Lorg/apache/tools/ant/types/resources/ZipResource;

    invoke-direct {v5, v6, p2, v2}, Lorg/apache/tools/ant/types/resources/ZipResource;-><init>(Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/zip/ZipEntry;)V

    invoke-virtual {v2}, Lorg/apache/tools/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lorg/apache/tools/zip/ZipEntry;->isDirectory()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-static {v4}, Lorg/apache/tools/ant/types/ZipScanner;->trimSeparator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/types/ZipScanner;->match(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p6

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v9

    move-object v7, v8

    :goto_1
    if-eqz v7, :cond_1

    :try_start_2
    invoke-virtual {v7}, Lorg/apache/tools/zip/ZipFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_1
    :goto_2
    throw v9

    :cond_2
    new-instance v9, Lorg/apache/tools/ant/BuildException;

    const-string v10, "only file resources are supported"

    invoke-direct {v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v9

    :catch_0
    move-exception v3

    :try_start_3
    new-instance v9, Lorg/apache/tools/ant/BuildException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "problem reading "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9

    :catchall_1
    move-exception v9

    goto :goto_1

    :catch_1
    move-exception v3

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "problem opening "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_3
    :try_start_4
    invoke-interface {p3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/types/ZipScanner;->match(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p4

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_4
    if-eqz v8, :cond_5

    :try_start_5
    invoke-virtual {v8}, Lorg/apache/tools/zip/ZipFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_5
    :goto_3
    return-void

    :catch_2
    move-exception v9

    goto :goto_3

    :catch_3
    move-exception v10

    goto :goto_2
.end method
