.class public abstract Lorg/apache/tools/ant/types/ArchiveFileSet;
.super Lorg/apache/tools/ant/types/FileSet;
.source "ArchiveFileSet.java"


# static fields
.field private static final BASE_OCTAL:I = 0x8

.field public static final DEFAULT_DIR_MODE:I = 0x41ed

.field public static final DEFAULT_FILE_MODE:I = 0x81a4


# instance fields
.field private dirMode:I

.field private dirModeHasBeenSet:Z

.field private fileMode:I

.field private fileModeHasBeenSet:Z

.field private fullpath:Ljava/lang/String;

.field private hasDir:Z

.field private prefix:Ljava/lang/String;

.field private src:Lorg/apache/tools/ant/types/Resource;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/FileSet;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    const v0, 0x81a4

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    const/16 v0, 0x41ed

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/ArchiveFileSet;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/ArchiveFileSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/FileSet;-><init>(Lorg/apache/tools/ant/types/FileSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    const v0, 0x81a4

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    const/16 v0, 0x41ed

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    iget-object v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    iget-object v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    iget-object v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    iget-boolean v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    iget v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    iget v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    iget-boolean v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    iget-boolean v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/FileSet;-><init>(Lorg/apache/tools/ant/types/FileSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    const v0, 0x81a4

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    const/16 v0, 0x41ed

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    return-void
.end method

.method private checkArchiveAttributesAllowed()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkAttributesAllowed()V

    :cond_1
    return-void
.end method


# virtual methods
.method public addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkChildrenAllowed()V

    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "only single argument resource collections are supported as archives"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->setSrcResource(Lorg/apache/tools/ant/types/Resource;)V

    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->clone()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/types/FileSet;->clone()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected configureFileSet(Lorg/apache/tools/ant/types/ArchiveFileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ArchiveFileSet;

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->setPrefix(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->setFullpath(Ljava/lang/String;)V

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    iput-boolean v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    iget v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    iput v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    iput-boolean v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    iget v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    iput v0, p1, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    return-void
.end method

.method public getDirMode()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    return v0
.end method

.method public getDirMode(Lorg/apache/tools/ant/Project;)I
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getDirMode(Lorg/apache/tools/ant/Project;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    goto :goto_0
.end method

.method public getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;
    .locals 3
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/types/AbstractFileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v1, :cond_1

    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "the archive doesn\'t exist"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "the archive can\'t be a directory"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->newArchiveScanner()Lorg/apache/tools/ant/types/ArchiveScanner;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ArchiveScanner;->setSrc(Lorg/apache/tools/ant/types/Resource;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v1

    invoke-super {p0, v1}, Lorg/apache/tools/ant/types/FileSet;->setDir(Ljava/io/File;)V

    invoke-virtual {p0, v0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->setupDirectoryScanner(Lorg/apache/tools/ant/FileScanner;Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ArchiveScanner;->init()V

    goto :goto_0
.end method

.method public getFileMode()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    return v0
.end method

.method public getFileMode(Lorg/apache/tools/ant/Project;)I
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getFileMode(Lorg/apache/tools/ant/Project;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    goto :goto_0
.end method

.method public getFullpath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    return-object v0
.end method

.method public getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getFullpath(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    return-object v0
.end method

.method public getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getPrefix(Lorg/apache/tools/ant/Project;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSrc()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    instance-of v0, v0, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    check-cast v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSrc(Lorg/apache/tools/ant/Project;)Ljava/io/File;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getSrc(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getSrc()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public hasDirModeBeenSet()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDirModeBeenSet()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    goto :goto_0
.end method

.method public hasFileModeBeenSet()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasFileModeBeenSet()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    goto :goto_0
.end method

.method public integerSetDirMode(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirModeHasBeenSet:Z

    or-int/lit16 v0, p1, 0x4000

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->dirMode:I

    return-void
.end method

.method public integerSetFileMode(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileModeHasBeenSet:Z

    const v0, 0x8000

    or-int/2addr v0, p1

    iput v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fileMode:I

    return-void
.end method

.method public isFilesystemOnly()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/ResourceCollection;

    check-cast v1, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v1, :cond_1

    invoke-super {p0}, Lorg/apache/tools/ant/types/FileSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveScanner;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ArchiveScanner;->getResourceFiles()Ljava/util/Iterator;

    move-result-object v1

    goto :goto_0
.end method

.method protected abstract newArchiveScanner()Lorg/apache/tools/ant/types/ArchiveScanner;
.end method

.method public setDir(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkAttributesAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot set both dir and src attributes"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/FileSet;->setDir(Ljava/io/File;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    return-void
.end method

.method public setDirMode(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkArchiveAttributesAllowed()V

    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->integerSetDirMode(I)V

    return-void
.end method

.method public setFileMode(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkArchiveAttributesAllowed()V

    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->integerSetFileMode(I)V

    return-void
.end method

.method public setFullpath(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkArchiveAttributesAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot set both fullpath and prefix attributes"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkArchiveAttributesAllowed()V

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->fullpath:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot set both fullpath and prefix attributes"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->prefix:Ljava/lang/String;

    return-void
.end method

.method public setSrc(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->setSrcResource(Lorg/apache/tools/ant/types/Resource;)V

    return-void
.end method

.method public setSrcResource(Lorg/apache/tools/ant/types/Resource;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->checkArchiveAttributesAllowed()V

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot set both dir and src attributes"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    return-void
.end method

.method public size()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/ResourceCollection;

    check-cast v1, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v1, :cond_1

    invoke-super {p0}, Lorg/apache/tools/ant/types/FileSet;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ArchiveScanner;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ArchiveScanner;->getIncludedFilesCount()I

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->hasDir:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lorg/apache/tools/ant/types/FileSet;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveFileSet;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
