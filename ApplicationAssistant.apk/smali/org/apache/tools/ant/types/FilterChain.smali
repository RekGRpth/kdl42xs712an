.class public Lorg/apache/tools/ant/types/FilterChain;
.super Lorg/apache/tools/ant/types/DataType;
.source "FilterChain.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private filterReaders:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/filters/ChainableReader;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/ChainableReader;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addClassConstants(Lorg/apache/tools/ant/filters/ClassConstants;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/ClassConstants;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addContainsRegex(Lorg/apache/tools/ant/filters/TokenFilter$ContainsRegex;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TokenFilter$ContainsRegex;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addDeleteCharacters(Lorg/apache/tools/ant/filters/TokenFilter$DeleteCharacters;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TokenFilter$DeleteCharacters;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addEscapeUnicode(Lorg/apache/tools/ant/filters/EscapeUnicode;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/EscapeUnicode;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addExpandProperties(Lorg/apache/tools/ant/filters/ExpandProperties;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/ExpandProperties;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addFilterReader(Lorg/apache/tools/ant/types/AntFilterReader;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/AntFilterReader;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addHeadFilter(Lorg/apache/tools/ant/filters/HeadFilter;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/HeadFilter;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addIgnoreBlank(Lorg/apache/tools/ant/filters/TokenFilter$IgnoreBlank;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TokenFilter$IgnoreBlank;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addLineContains(Lorg/apache/tools/ant/filters/LineContains;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/LineContains;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addLineContainsRegExp(Lorg/apache/tools/ant/filters/LineContainsRegExp;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/LineContainsRegExp;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addPrefixLines(Lorg/apache/tools/ant/filters/PrefixLines;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/PrefixLines;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addReplaceRegex(Lorg/apache/tools/ant/filters/TokenFilter$ReplaceRegex;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TokenFilter$ReplaceRegex;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addReplaceString(Lorg/apache/tools/ant/filters/TokenFilter$ReplaceString;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TokenFilter$ReplaceString;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addReplaceTokens(Lorg/apache/tools/ant/filters/ReplaceTokens;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/ReplaceTokens;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addStripJavaComments(Lorg/apache/tools/ant/filters/StripJavaComments;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/StripJavaComments;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addStripLineBreaks(Lorg/apache/tools/ant/filters/StripLineBreaks;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/StripLineBreaks;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addStripLineComments(Lorg/apache/tools/ant/filters/StripLineComments;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/StripLineComments;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addTabsToSpaces(Lorg/apache/tools/ant/filters/TabsToSpaces;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TabsToSpaces;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addTailFilter(Lorg/apache/tools/ant/filters/TailFilter;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TailFilter;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addTokenFilter(Lorg/apache/tools/ant/filters/TokenFilter;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TokenFilter;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addTrim(Lorg/apache/tools/ant/filters/TokenFilter$Trim;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/filters/TokenFilter$Trim;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public getFilterReaders()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    return-object v0
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v3, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterChain;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v3

    throw v3

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterChain;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/tools/ant/types/FilterChain;

    if-eqz v3, :cond_1

    move-object v0, v2

    check-cast v0, Lorg/apache/tools/ant/types/FilterChain;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FilterChain;->getFilterReaders()Ljava/util/Vector;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/types/FilterChain;->filterReaders:Ljava/util/Vector;

    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/DataType;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " doesn\'t refer to a FilterChain"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
