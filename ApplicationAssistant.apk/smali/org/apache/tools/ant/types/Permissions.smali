.class public Lorg/apache/tools/ant/types/Permissions;
.super Ljava/lang/Object;
.source "Permissions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/Permissions$1;,
        Lorg/apache/tools/ant/types/Permissions$Permission;,
        Lorg/apache/tools/ant/types/Permissions$MySM;
    }
.end annotation


# instance fields
.field private active:Z

.field private delegateToOldSM:Z

.field private granted:Ljava/security/Permissions;

.field private grantedPermissions:Ljava/util/List;

.field private origSm:Ljava/lang/SecurityManager;

.field private revokedPermissions:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/types/Permissions;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->grantedPermissions:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->revokedPermissions:Ljava/util/List;

    iput-object v1, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    iput-object v1, p0, Lorg/apache/tools/ant/types/Permissions;->origSm:Ljava/lang/SecurityManager;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/Permissions;->active:Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/Permissions;->delegateToOldSM:Z

    return-void
.end method

.method static access$100(Lorg/apache/tools/ant/types/Permissions;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/Permissions;

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/Permissions;->active:Z

    return v0
.end method

.method static access$200(Lorg/apache/tools/ant/types/Permissions;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/Permissions;

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/Permissions;->delegateToOldSM:Z

    return v0
.end method

.method static access$300(Lorg/apache/tools/ant/types/Permissions;)Ljava/security/Permissions;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/Permissions;

    iget-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    return-object v0
.end method

.method static access$400(Lorg/apache/tools/ant/types/Permissions;)Ljava/lang/SecurityManager;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/Permissions;

    iget-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->origSm:Ljava/lang/SecurityManager;

    return-object v0
.end method

.method static access$500(Lorg/apache/tools/ant/types/Permissions;)Ljava/util/List;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/Permissions;

    iget-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->revokedPermissions:Ljava/util/List;

    return-object v0
.end method

.method private init()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v3, Ljava/security/Permissions;

    invoke-direct {v3}, Ljava/security/Permissions;-><init>()V

    iput-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->revokedPermissions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/Permissions$Permission;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Permissions$Permission;->getClassName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Revoked permission "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " does not contain a class."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->grantedPermissions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/Permissions$Permission;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Permissions$Permission;->getClassName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Granted permission "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " does not contain a class."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    new-instance v2, Ljava/security/UnresolvedPermission;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Permissions$Permission;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Permissions$Permission;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Permissions$Permission;->getActions()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Ljava/security/UnresolvedPermission;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/security/cert/Certificate;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    invoke-virtual {v3, v2}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/net/SocketPermission;

    const-string v5, "localhost:1024-"

    const-string v6, "listen"

    invoke-direct {v4, v5, v6}, Ljava/net/SocketPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.version"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vendor"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vendor.url"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.class.version"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "os.name"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "os.version"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "os.arch"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "file.encoding"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "file.separator"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "path.separator"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "line.separator"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.specification.version"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.specification.vendor"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.specification.name"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vm.specification.version"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vm.specification.vendor"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vm.specification.name"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vm.version"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vm.vendor"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    iget-object v3, p0, Lorg/apache/tools/ant/types/Permissions;->granted:Ljava/security/Permissions;

    new-instance v4, Ljava/util/PropertyPermission;

    const-string v5, "java.vm.name"

    const-string v6, "read"

    invoke-direct {v4, v5, v6}, Ljava/util/PropertyPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/security/Permissions;->add(Ljava/security/Permission;)V

    return-void
.end method


# virtual methods
.method public addConfiguredGrant(Lorg/apache/tools/ant/types/Permissions$Permission;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Permissions$Permission;

    iget-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->grantedPermissions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addConfiguredRevoke(Lorg/apache/tools/ant/types/Permissions$Permission;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Permissions$Permission;

    iget-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->revokedPermissions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public declared-synchronized restoreSecurityManager()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lorg/apache/tools/ant/types/Permissions;->active:Z

    iget-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->origSm:Ljava/lang/SecurityManager;

    invoke-static {v0}, Ljava/lang/System;->setSecurityManager(Ljava/lang/SecurityManager;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSecurityManager()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/Permissions;->origSm:Ljava/lang/SecurityManager;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Permissions;->init()V

    new-instance v0, Lorg/apache/tools/ant/types/Permissions$MySM;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/tools/ant/types/Permissions$MySM;-><init>(Lorg/apache/tools/ant/types/Permissions;Lorg/apache/tools/ant/types/Permissions$1;)V

    invoke-static {v0}, Ljava/lang/System;->setSecurityManager(Ljava/lang/SecurityManager;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/Permissions;->active:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
