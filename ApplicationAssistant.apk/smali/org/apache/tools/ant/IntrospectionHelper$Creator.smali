.class public final Lorg/apache/tools/ant/IntrospectionHelper$Creator;
.super Ljava/lang/Object;
.source "IntrospectionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/IntrospectionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Creator"
.end annotation


# instance fields
.field private nestedCreator:Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

.field private nestedObject:Ljava/lang/Object;

.field private parent:Ljava/lang/Object;

.field private polyType:Ljava/lang/String;

.field private project:Lorg/apache/tools/ant/Project;


# direct methods
.method private constructor <init>(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->project:Lorg/apache/tools/ant/Project;

    iput-object p2, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->parent:Ljava/lang/Object;

    iput-object p3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedCreator:Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    return-void
.end method

.method constructor <init>(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;Lorg/apache/tools/ant/IntrospectionHelper$1;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;
    .param p4    # Lorg/apache/tools/ant/IntrospectionHelper$1;

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/tools/ant/IntrospectionHelper$Creator;-><init>(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;)V

    return-void
.end method


# virtual methods
.method public create()Ljava/lang/Object;
    .locals 7

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedCreator:Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    invoke-virtual {v3}, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->isPolyMorphic()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    const-string v4, "Not allowed to use the polymorphic form for this element"

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->project:Lorg/apache/tools/ant/Project;

    invoke-static {v3}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/ComponentHelper;->createComponent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedObject:Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedObject:Ljava/lang/Object;

    if-nez v3, :cond_1

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unable to create object of type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedCreator:Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    iget-object v4, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->project:Lorg/apache/tools/ant/Project;

    iget-object v5, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->parent:Ljava/lang/Object;

    iget-object v6, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedObject:Ljava/lang/Object;

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->create(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedObject:Ljava/lang/Object;

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->project:Lorg/apache/tools/ant/Project;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->project:Lorg/apache/tools/ant/Project;

    iget-object v4, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedObject:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/Project;->setProjectReference(Ljava/lang/Object;)V

    :cond_2
    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedObject:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    return-object v3

    :catch_0
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :catch_1
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :catch_2
    move-exception v0

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    if-eqz v3, :cond_3

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Invalid type used "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    throw v0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/tools/ant/BuildException;

    if-eqz v3, :cond_4

    check-cast v2, Lorg/apache/tools/ant/BuildException;

    throw v2

    :cond_4
    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public getRealObject()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedCreator:Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    invoke-virtual {v0}, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setPolyType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    return-void
.end method

.method public store()V
    .locals 5

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedCreator:Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->parent:Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->nestedObject:Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->store(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception v0

    iget-object v2, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Invalid type used "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/IntrospectionHelper$Creator;->polyType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    throw v0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/tools/ant/BuildException;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/tools/ant/BuildException;

    throw v1

    :cond_1
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
