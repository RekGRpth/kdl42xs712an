.class public Lorg/apache/tools/ant/helper/ProjectHelper2;
.super Lorg/apache/tools/ant/ProjectHelper;
.source "ProjectHelper2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/helper/ProjectHelper2$ElementHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelper2$TargetHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelper2$ProjectHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelper2$MainHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field private static elementHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

.field private static mainHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

.field private static projectHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

.field private static targetHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelper2$ElementHandler;

    invoke-direct {v0}, Lorg/apache/tools/ant/helper/ProjectHelper2$ElementHandler;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->elementHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelper2$TargetHandler;

    invoke-direct {v0}, Lorg/apache/tools/ant/helper/ProjectHelper2$TargetHandler;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->targetHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelper2$MainHandler;

    invoke-direct {v0}, Lorg/apache/tools/ant/helper/ProjectHelper2$MainHandler;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->mainHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelper2$ProjectHandler;

    invoke-direct {v0}, Lorg/apache/tools/ant/helper/ProjectHelper2$ProjectHandler;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->projectHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectHelper;-><init>()V

    return-void
.end method

.method static access$100()Lorg/apache/tools/ant/util/FileUtils;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-object v0
.end method

.method static access$200()Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->projectHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-object v0
.end method

.method static access$300()Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->targetHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-object v0
.end method

.method static access$400()Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->elementHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-object v0
.end method

.method protected static getElementHandler()Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->elementHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-object v0
.end method

.method protected static getMainHandler()Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->mainHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-object v0
.end method

.method protected static getProjectHandler()Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->projectHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-object v0
.end method

.method protected static getTargetHandler()Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelper2;->targetHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-object v0
.end method

.method protected static setElementHandler(Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    sput-object p0, Lorg/apache/tools/ant/helper/ProjectHelper2;->elementHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-void
.end method

.method protected static setMainHandler(Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    sput-object p0, Lorg/apache/tools/ant/helper/ProjectHelper2;->mainHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-void
.end method

.method protected static setProjectHandler(Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    sput-object p0, Lorg/apache/tools/ant/helper/ProjectHelper2;->projectHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-void
.end method

.method protected static setTargetHandler(Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    sput-object p0, Lorg/apache/tools/ant/helper/ProjectHelper2;->targetHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    return-void
.end method


# virtual methods
.method public parse(Lorg/apache/tools/ant/Project;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v7, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/helper/ProjectHelper2;->getImportStack()Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const/4 v0, 0x0

    const-string v5, "ant.parsing.context"

    invoke-virtual {p1, v5}, Lorg/apache/tools/ant/Project;->getReference(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/helper/AntXMLContext;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/helper/AntXMLContext;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/helper/AntXMLContext;-><init>(Lorg/apache/tools/ant/Project;)V

    const-string v5, "ant.parsing.context"

    invoke-virtual {p1, v5, v0}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v5, "ant.targets"

    invoke-virtual {v0}, Lorg/apache/tools/ant/helper/AntXMLContext;->getTargets()Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/helper/ProjectHelper2;->getImportStack()Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-le v5, v7, :cond_1

    invoke-virtual {v0, v7}, Lorg/apache/tools/ant/helper/AntXMLContext;->setIgnoreProjectTag(Z)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/helper/AntXMLContext;->getCurrentTarget()Lorg/apache/tools/ant/Target;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/tools/ant/helper/AntXMLContext;->getImplicitTarget()Lorg/apache/tools/ant/Target;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/tools/ant/helper/AntXMLContext;->getCurrentTargets()Ljava/util/Map;

    move-result-object v3

    :try_start_0
    new-instance v4, Lorg/apache/tools/ant/Target;

    invoke-direct {v4}, Lorg/apache/tools/ant/Target;-><init>()V

    invoke-virtual {v4, p1}, Lorg/apache/tools/ant/Target;->setProject(Lorg/apache/tools/ant/Project;)V

    const-string v5, ""

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/Target;->setName(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/helper/AntXMLContext;->setCurrentTarget(Lorg/apache/tools/ant/Target;)V

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/helper/AntXMLContext;->setCurrentTargets(Ljava/util/Map;)V

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/helper/AntXMLContext;->setImplicitTarget(Lorg/apache/tools/ant/Target;)V

    new-instance v5, Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;

    sget-object v6, Lorg/apache/tools/ant/helper/ProjectHelper2;->mainHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    invoke-direct {v5, v0, v6}, Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;-><init>(Lorg/apache/tools/ant/helper/AntXMLContext;Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;)V

    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/tools/ant/helper/ProjectHelper2;->parse(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/Target;->execute()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/helper/AntXMLContext;->setCurrentTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/helper/AntXMLContext;->setImplicitTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/helper/AntXMLContext;->setCurrentTargets(Ljava/util/Map;)V

    :goto_0
    return-void

    :catchall_0
    move-exception v5

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/helper/AntXMLContext;->setCurrentTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/helper/AntXMLContext;->setImplicitTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/helper/AntXMLContext;->setCurrentTargets(Ljava/util/Map;)V

    throw v5

    :cond_1
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/helper/AntXMLContext;->setCurrentTargets(Ljava/util/Map;)V

    new-instance v5, Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;

    sget-object v6, Lorg/apache/tools/ant/helper/ProjectHelper2;->mainHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    invoke-direct {v5, v0, v6}, Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;-><init>(Lorg/apache/tools/ant/helper/AntXMLContext;Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;)V

    invoke-virtual {p0, p1, p2, v5}, Lorg/apache/tools/ant/helper/ProjectHelper2;->parse(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/helper/AntXMLContext;->getImplicitTarget()Lorg/apache/tools/ant/Target;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/Target;->execute()V

    goto :goto_0
.end method

.method public parse(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;)V
    .locals 21
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-static/range {p3 .. p3}, Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;->access$000(Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;)Lorg/apache/tools/ant/helper/AntXMLContext;

    move-result-object v6

    const/4 v4, 0x0

    const/16 v17, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p2

    instance-of v0, v0, Ljava/io/File;

    move/from16 v18, v0

    if-eqz v18, :cond_1

    move-object/from16 v4, p2

    check-cast v4, Ljava/io/File;

    sget-object v18, Lorg/apache/tools/ant/helper/ProjectHelper2;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lorg/apache/tools/ant/util/FileUtils;->normalize(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v6, v4}, Lorg/apache/tools/ant/helper/AntXMLContext;->setBuildFile(Ljava/io/File;)V

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    const/4 v11, 0x0

    const/4 v9, 0x0

    :try_start_0
    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getNamespaceXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v14

    const/16 v16, 0x0

    if-eqz v4, :cond_3

    sget-object v18, Lorg/apache/tools/ant/helper/ProjectHelper2;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lorg/apache/tools/ant/util/FileUtils;->toURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v11, v12

    :goto_1
    new-instance v10, Lorg/xml/sax/InputSource;

    invoke-direct {v10, v11}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v16, :cond_0

    :try_start_1
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lorg/xml/sax/InputSource;->setSystemId(Ljava/lang/String;)V

    :cond_0
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    const-string v19, "parsing buildfile "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " with URI = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x3

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    move-object/from16 v8, p3

    invoke-interface {v14, v8}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    invoke-interface {v14, v8}, Lorg/xml/sax/XMLReader;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    invoke-interface {v14, v8}, Lorg/xml/sax/XMLReader;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V

    invoke-interface {v14, v8}, Lorg/xml/sax/XMLReader;->setDTDHandler(Lorg/xml/sax/DTDHandler;)V

    invoke-interface {v14, v10}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_1
    .catch Lorg/xml/sax/SAXParseException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v11}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    return-void

    :cond_1
    move-object/from16 v0, p2

    instance-of v0, v0, Ljava/net/URL;

    move/from16 v18, v0

    if-eqz v18, :cond_2

    move-object/from16 v17, p2

    check-cast v17, Ljava/net/URL;

    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_2
    new-instance v18, Lorg/apache/tools/ant/BuildException;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v20, "Source "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, " not supported by this plugin"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_3
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->toString()Ljava/lang/String;
    :try_end_2
    .catch Lorg/xml/sax/SAXParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v16

    goto/16 :goto_1

    :catch_0
    move-exception v7

    :goto_2
    :try_start_3
    new-instance v13, Lorg/apache/tools/ant/Location;

    invoke-virtual {v7}, Lorg/xml/sax/SAXParseException;->getSystemId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual {v7}, Lorg/xml/sax/SAXParseException;->getLineNumber()I

    move-result v19

    invoke-virtual {v7}, Lorg/xml/sax/SAXParseException;->getColumnNumber()I

    move-result v20

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v13, v0, v1, v2}, Lorg/apache/tools/ant/Location;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v7}, Lorg/xml/sax/SAXParseException;->getException()Ljava/lang/Exception;

    move-result-object v15

    instance-of v0, v15, Lorg/apache/tools/ant/BuildException;

    move/from16 v18, v0

    if-eqz v18, :cond_5

    move-object v0, v15

    check-cast v0, Lorg/apache/tools/ant/BuildException;

    move-object v3, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v18

    sget-object v19, Lorg/apache/tools/ant/Location;->UNKNOWN_LOCATION:Lorg/apache/tools/ant/Location;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_4

    invoke-virtual {v3, v13}, Lorg/apache/tools/ant/BuildException;->setLocation(Lorg/apache/tools/ant/Location;)V

    :cond_4
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v18

    :goto_3
    invoke-static {v11}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v18

    :cond_5
    if-nez v15, :cond_6

    move-object v15, v7

    :cond_6
    :try_start_4
    new-instance v18, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v7}, Lorg/xml/sax/SAXParseException;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v15, v13}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v18

    :catch_1
    move-exception v7

    :goto_4
    invoke-virtual {v7}, Lorg/xml/sax/SAXException;->getException()Ljava/lang/Exception;

    move-result-object v15

    instance-of v0, v15, Lorg/apache/tools/ant/BuildException;

    move/from16 v18, v0

    if-eqz v18, :cond_7

    check-cast v15, Lorg/apache/tools/ant/BuildException;

    throw v15

    :cond_7
    if-nez v15, :cond_8

    move-object v15, v7

    :cond_8
    new-instance v18, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v7}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v18

    :catch_2
    move-exception v7

    :goto_5
    new-instance v18, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v18

    invoke-direct {v0, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v18

    :catch_3
    move-exception v7

    :goto_6
    new-instance v18, Lorg/apache/tools/ant/BuildException;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v20, "Encoding of project file "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, " is invalid."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v18

    :catch_4
    move-exception v7

    :goto_7
    new-instance v18, Lorg/apache/tools/ant/BuildException;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v20, "Error reading project file "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, ": "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_1
    move-exception v18

    move-object v9, v10

    goto/16 :goto_3

    :catch_5
    move-exception v7

    move-object v9, v10

    goto :goto_7

    :catch_6
    move-exception v7

    move-object v9, v10

    goto :goto_6

    :catch_7
    move-exception v7

    move-object v9, v10

    goto :goto_5

    :catch_8
    move-exception v7

    move-object v9, v10

    goto/16 :goto_4

    :catch_9
    move-exception v7

    move-object v9, v10

    goto/16 :goto_2
.end method

.method public parseUnknownElement(Lorg/apache/tools/ant/Project;Ljava/net/URL;)Lorg/apache/tools/ant/UnknownElement;
    .locals 6
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v1, Lorg/apache/tools/ant/Target;

    invoke-direct {v1}, Lorg/apache/tools/ant/Target;-><init>()V

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/Target;->setProject(Lorg/apache/tools/ant/Project;)V

    new-instance v0, Lorg/apache/tools/ant/helper/AntXMLContext;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/helper/AntXMLContext;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/helper/AntXMLContext;->addTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/helper/AntXMLContext;->setImplicitTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/helper/AntXMLContext;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    new-instance v4, Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;

    sget-object v5, Lorg/apache/tools/ant/helper/ProjectHelper2;->elementHandler:Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;

    invoke-direct {v4, v0, v5}, Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;-><init>(Lorg/apache/tools/ant/helper/AntXMLContext;Lorg/apache/tools/ant/helper/ProjectHelper2$AntHandler;)V

    invoke-virtual {p0, v3, p2, v4}, Lorg/apache/tools/ant/helper/ProjectHelper2;->parse(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/helper/ProjectHelper2$RootHandler;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/Target;->getTasks()[Lorg/apache/tools/ant/Task;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    const-string v4, "No tasks defined"

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    const/4 v3, 0x0

    aget-object v3, v2, v3

    check-cast v3, Lorg/apache/tools/ant/UnknownElement;

    return-object v3
.end method
