.class public Lorg/apache/tools/ant/helper/ProjectHelperImpl;
.super Lorg/apache/tools/ant/ProjectHelper;
.source "ProjectHelperImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$DataTypeHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$TaskHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$DescriptionHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$ProjectHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$RootHandler;,
        Lorg/apache/tools/ant/helper/ProjectHelperImpl$AbstractHandler;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private buildFile:Ljava/io/File;

.field private buildFileParent:Ljava/io/File;

.field private implicitTarget:Lorg/apache/tools/ant/Target;

.field private locator:Lorg/xml/sax/Locator;

.field private parser:Lorg/xml/sax/Parser;

.field private project:Lorg/apache/tools/ant/Project;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectHelper;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/Target;

    invoke-direct {v0}, Lorg/apache/tools/ant/Target;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->implicitTarget:Lorg/apache/tools/ant/Target;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->implicitTarget:Lorg/apache/tools/ant/Target;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Target;->setName(Ljava/lang/String;)V

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/xml/sax/Parser;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;

    return-object v0
.end method

.method static access$100(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/xml/sax/Locator;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->locator:Lorg/xml/sax/Locator;

    return-object v0
.end method

.method static access$102(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/Locator;)Lorg/xml/sax/Locator;
    .locals 0
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;
    .param p1    # Lorg/xml/sax/Locator;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->locator:Lorg/xml/sax/Locator;

    return-object p1
.end method

.method static access$200(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/apache/tools/ant/Project;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->project:Lorg/apache/tools/ant/Project;

    return-object v0
.end method

.method static access$300()Lorg/apache/tools/ant/util/FileUtils;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-object v0
.end method

.method static access$400(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Ljava/io/File;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->buildFileParent:Ljava/io/File;

    return-object v0
.end method

.method static access$500(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Ljava/io/File;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->buildFile:Ljava/io/File;

    return-object v0
.end method

.method static access$600(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/apache/tools/ant/Target;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->implicitTarget:Lorg/apache/tools/ant/Target;

    return-object v0
.end method

.method static access$700(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Lorg/apache/tools/ant/Target;Ljava/lang/String;Lorg/xml/sax/AttributeList;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;
    .param p1    # Lorg/xml/sax/DocumentHandler;
    .param p2    # Lorg/apache/tools/ant/Target;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/AttributeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXParseException;
        }
    .end annotation

    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->handleElement(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Lorg/apache/tools/ant/Target;Ljava/lang/String;Lorg/xml/sax/AttributeList;)V

    return-void
.end method

.method static access$800(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Ljava/lang/Object;Lorg/xml/sax/AttributeList;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;
    .param p1    # Ljava/lang/Object;
    .param p2    # Lorg/xml/sax/AttributeList;

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->configureId(Ljava/lang/Object;Lorg/xml/sax/AttributeList;)V

    return-void
.end method

.method private configureId(Ljava/lang/Object;Lorg/xml/sax/AttributeList;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Lorg/xml/sax/AttributeList;

    const-string v1, "id"

    invoke-interface {p2, v1}, Lorg/xml/sax/AttributeList;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v1, v0, p1}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private static handleElement(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Lorg/apache/tools/ant/Target;Ljava/lang/String;Lorg/xml/sax/AttributeList;)V
    .locals 6
    .param p0    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;
    .param p1    # Lorg/xml/sax/DocumentHandler;
    .param p2    # Lorg/apache/tools/ant/Target;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/AttributeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXParseException;
        }
    .end annotation

    const-string v0, "description"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$DescriptionHandler;

    invoke-direct {v0, p0, p1}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$DescriptionHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->getDataTypeDefinitions()Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$DataTypeHandler;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$DataTypeHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, p3, p4}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$DataTypeHandler;->init(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TaskHandler;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TaskHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Lorg/apache/tools/ant/TaskContainer;Lorg/apache/tools/ant/RuntimeConfigurable;Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, p3, p4}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TaskHandler;->init(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V

    goto :goto_0
.end method


# virtual methods
.method public parse(Lorg/apache/tools/ant/Project;Ljava/lang/Object;)V
    .locals 17
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p2

    instance-of v14, v0, Ljava/io/File;

    if-nez v14, :cond_0

    new-instance v14, Lorg/apache/tools/ant/BuildException;

    const-string v15, "Only File source supported by default plugin"

    invoke-direct {v14, v15}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v14

    :cond_0
    move-object/from16 v2, p2

    check-cast v2, Ljava/io/File;

    const/4 v9, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->project:Lorg/apache/tools/ant/Project;

    new-instance v14, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->buildFile:Ljava/io/File;

    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->buildFile:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->buildFileParent:Ljava/io/File;

    :try_start_0
    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getParser()Lorg/xml/sax/Parser;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    sget-object v14, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lorg/apache/tools/ant/util/FileUtils;->toURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Lorg/xml/sax/SAXParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v8, Lorg/xml/sax/InputSource;

    invoke-direct {v8, v10}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Lorg/xml/sax/SAXParseException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v8, v13}, Lorg/xml/sax/InputSource;->setSystemId(Ljava/lang/String;)V

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "parsing buildfile "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " with URI = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    new-instance v6, Lorg/apache/tools/ant/helper/ProjectHelperImpl$RootHandler;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$RootHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;

    invoke-interface {v14, v6}, Lorg/xml/sax/Parser;->setDocumentHandler(Lorg/xml/sax/DocumentHandler;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;

    invoke-interface {v14, v6}, Lorg/xml/sax/Parser;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;

    invoke-interface {v14, v6}, Lorg/xml/sax/Parser;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;

    invoke-interface {v14, v6}, Lorg/xml/sax/Parser;->setDTDHandler(Lorg/xml/sax/DTDHandler;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;

    invoke-interface {v14, v8}, Lorg/xml/sax/Parser;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_3
    .catch Lorg/xml/sax/SAXParseException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Lorg/xml/sax/SAXException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    return-void

    :catch_0
    move-exception v4

    :try_start_4
    new-instance v14, Lorg/xml/sax/helpers/XMLReaderAdapter;

    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v15

    invoke-direct {v14, v15}, Lorg/xml/sax/helpers/XMLReaderAdapter;-><init>(Lorg/xml/sax/XMLReader;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->parser:Lorg/xml/sax/Parser;
    :try_end_4
    .catch Lorg/xml/sax/SAXParseException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v5

    :goto_1
    :try_start_5
    new-instance v11, Lorg/apache/tools/ant/Location;

    invoke-virtual {v5}, Lorg/xml/sax/SAXParseException;->getSystemId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5}, Lorg/xml/sax/SAXParseException;->getLineNumber()I

    move-result v15

    invoke-virtual {v5}, Lorg/xml/sax/SAXParseException;->getColumnNumber()I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v11, v14, v15, v0}, Lorg/apache/tools/ant/Location;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v5}, Lorg/xml/sax/SAXParseException;->getException()Ljava/lang/Exception;

    move-result-object v12

    instance-of v14, v12, Lorg/apache/tools/ant/BuildException;

    if-eqz v14, :cond_2

    move-object v0, v12

    check-cast v0, Lorg/apache/tools/ant/BuildException;

    move-object v3, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/BuildException;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v14

    sget-object v15, Lorg/apache/tools/ant/Location;->UNKNOWN_LOCATION:Lorg/apache/tools/ant/Location;

    if-ne v14, v15, :cond_1

    invoke-virtual {v3, v11}, Lorg/apache/tools/ant/BuildException;->setLocation(Lorg/apache/tools/ant/Location;)V

    :cond_1
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v14

    :goto_2
    invoke-static {v9}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v14

    :cond_2
    :try_start_6
    new-instance v14, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v5}, Lorg/xml/sax/SAXParseException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15, v12, v11}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v14

    :catch_2
    move-exception v5

    :goto_3
    invoke-virtual {v5}, Lorg/xml/sax/SAXException;->getException()Ljava/lang/Exception;

    move-result-object v12

    instance-of v14, v12, Lorg/apache/tools/ant/BuildException;

    if-eqz v14, :cond_3

    check-cast v12, Lorg/apache/tools/ant/BuildException;

    throw v12

    :cond_3
    new-instance v14, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v5}, Lorg/xml/sax/SAXException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v14

    :catch_3
    move-exception v5

    :goto_4
    new-instance v14, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v14, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v14

    :catch_4
    move-exception v5

    :goto_5
    new-instance v14, Lorg/apache/tools/ant/BuildException;

    const-string v15, "Encoding of project file is invalid."

    invoke-direct {v14, v15, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v14

    :catch_5
    move-exception v5

    :goto_6
    new-instance v14, Lorg/apache/tools/ant/BuildException;

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string v16, "Error reading project file: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v14
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_1
    move-exception v14

    move-object v9, v10

    goto :goto_2

    :catchall_2
    move-exception v14

    move-object v7, v8

    move-object v9, v10

    goto :goto_2

    :catch_6
    move-exception v5

    move-object v9, v10

    goto :goto_6

    :catch_7
    move-exception v5

    move-object v7, v8

    move-object v9, v10

    goto :goto_6

    :catch_8
    move-exception v5

    move-object v9, v10

    goto :goto_5

    :catch_9
    move-exception v5

    move-object v7, v8

    move-object v9, v10

    goto :goto_5

    :catch_a
    move-exception v5

    move-object v9, v10

    goto :goto_4

    :catch_b
    move-exception v5

    move-object v7, v8

    move-object v9, v10

    goto :goto_4

    :catch_c
    move-exception v5

    move-object v9, v10

    goto :goto_3

    :catch_d
    move-exception v5

    move-object v7, v8

    move-object v9, v10

    goto :goto_3

    :catch_e
    move-exception v5

    move-object v9, v10

    goto/16 :goto_1

    :catch_f
    move-exception v5

    move-object v7, v8

    move-object v9, v10

    goto/16 :goto_1
.end method
