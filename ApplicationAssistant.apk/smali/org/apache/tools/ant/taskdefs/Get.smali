.class public Lorg/apache/tools/ant/taskdefs/Get;
.super Lorg/apache/tools/ant/Task;
.source "Get.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Get$VerboseProgress;,
        Lorg/apache/tools/ant/taskdefs/Get$NullProgress;,
        Lorg/apache/tools/ant/taskdefs/Get$DownloadProgress;,
        Lorg/apache/tools/ant/taskdefs/Get$Base64Converter;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private dest:Ljava/io/File;

.field private ignoreErrors:Z

.field private pword:Ljava/lang/String;

.field private source:Ljava/net/URL;

.field private uname:Ljava/lang/String;

.field private useTimestamp:Z

.field private verbose:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Get;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Get;->verbose:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Get;->useTimestamp:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Get;->ignoreErrors:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Get;->uname:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Get;->pword:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public doGet(ILorg/apache/tools/ant/taskdefs/Get$DownloadProgress;)Z
    .locals 29
    .param p1    # I
    .param p2    # Lorg/apache/tools/ant/taskdefs/Get$DownloadProgress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->source:Ljava/net/URL;

    move-object/from16 v25, v0

    if-nez v25, :cond_0

    new-instance v25, Lorg/apache/tools/ant/BuildException;

    const-string v26, "src attribute is required"

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Get;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v27

    invoke-direct/range {v25 .. v27}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v25

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    if-nez v25, :cond_1

    new-instance v25, Lorg/apache/tools/ant/BuildException;

    const-string v26, "dest attribute is required"

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Get;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v27

    invoke-direct/range {v25 .. v27}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v25

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v25

    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->isDirectory()Z

    move-result v25

    if-eqz v25, :cond_2

    new-instance v25, Lorg/apache/tools/ant/BuildException;

    const-string v26, "The specified destination is a directory"

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Get;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v27

    invoke-direct/range {v25 .. v27}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v25

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v25

    if-eqz v25, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->canWrite()Z

    move-result v25

    if-nez v25, :cond_3

    new-instance v25, Lorg/apache/tools/ant/BuildException;

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Can\'t write to "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Get;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v27

    invoke-direct/range {v25 .. v27}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v25

    :cond_3
    if-nez p2, :cond_4

    new-instance p2, Lorg/apache/tools/ant/taskdefs/Get$NullProgress;

    invoke-direct/range {p2 .. p2}, Lorg/apache/tools/ant/taskdefs/Get$NullProgress;-><init>()V

    :cond_4
    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const-string v26, "Getting: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->source:Ljava/net/URL;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const-string v26, "To: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    const-wide/16 v22, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->useTimestamp:Z

    move/from16 v25, v0

    if-eqz v25, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v25

    if-eqz v25, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->lastModified()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->verbose:Z

    move/from16 v25, v0

    if-eqz v25, :cond_5

    new-instance v21, Ljava/util/Date;

    invoke-direct/range {v21 .. v23}, Ljava/util/Date;-><init>(J)V

    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const-string v26, "local file date : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    :cond_5
    const/4 v11, 0x1

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->source:Ljava/net/URL;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    if-eqz v11, :cond_7

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/net/URLConnection;->setIfModifiedSince(J)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->uname:Ljava/lang/String;

    move-object/from16 v25, v0

    if-nez v25, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->pword:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    :cond_8
    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->uname:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    const-string v26, ":"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->pword:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    new-instance v6, Lorg/apache/tools/ant/taskdefs/Get$Base64Converter;

    invoke-direct {v6}, Lorg/apache/tools/ant/taskdefs/Get$Base64Converter;-><init>()V

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->getBytes()[B

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Lorg/apache/tools/ant/taskdefs/Get$Base64Converter;->encode([B)Ljava/lang/String;

    move-result-object v7

    const-string v25, "Authorization"

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Basic "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Ljava/net/URLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v5}, Ljava/net/URLConnection;->connect()V

    instance-of v0, v5, Ljava/net/HttpURLConnection;

    move/from16 v25, v0

    if-eqz v25, :cond_d

    move-object v12, v5

    check-cast v12, Ljava/net/HttpURLConnection;

    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getLastModified()J

    move-result-wide v15

    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v25

    const/16 v26, 0x130

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_a

    const-wide/16 v25, 0x0

    cmp-long v25, v15, v25

    if-eqz v25, :cond_b

    if-eqz v11, :cond_b

    cmp-long v25, v22, v15

    if-ltz v25, :cond_b

    :cond_a
    const-string v25, "Not modified - so not downloaded"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    const/16 v25, 0x0

    :goto_0
    return v25

    :cond_b
    invoke-virtual {v12}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v25

    const/16 v26, 0x191

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_d

    const-string v18, "HTTP Authorization failure"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->ignoreErrors:Z

    move/from16 v25, v0

    if-eqz v25, :cond_c

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    const/16 v25, 0x0

    goto :goto_0

    :cond_c
    new-instance v25, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v25

    :cond_d
    const/4 v14, 0x0

    const/4 v13, 0x0

    :goto_1
    const/16 v25, 0x3

    move/from16 v0, v25

    if-ge v13, v0, :cond_e

    :try_start_0
    invoke-virtual {v5}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    :cond_e
    if-nez v14, :cond_10

    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const-string v26, "Can\'t get "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->source:Ljava/net/URL;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v25

    const-string v26, " to "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->ignoreErrors:Z

    move/from16 v25, v0

    if-eqz v25, :cond_f

    const/16 v25, 0x0

    goto :goto_0

    :catch_0
    move-exception v8

    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const-string v26, "Error opening connection "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_f
    new-instance v25, Lorg/apache/tools/ant/BuildException;

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Can\'t get "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->source:Ljava/net/URL;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " to "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Get;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v27

    invoke-direct/range {v25 .. v27}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v25

    :cond_10
    new-instance v10, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-interface/range {p2 .. p2}, Lorg/apache/tools/ant/taskdefs/Get$DownloadProgress;->beginDownload()V

    const/4 v9, 0x0

    const v25, 0x19000

    :try_start_1
    move/from16 v0, v25

    new-array v4, v0, [B

    :goto_2
    invoke-virtual {v14, v4}, Ljava/io/InputStream;->read([B)I

    move-result v17

    if-ltz v17, :cond_12

    const/16 v25, 0x0

    move/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v10, v4, v0, v1}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-interface/range {p2 .. p2}, Lorg/apache/tools/ant/taskdefs/Get$DownloadProgress;->onTick()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v25

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    invoke-static {v14}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    if-nez v9, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->delete()Z

    :cond_11
    throw v25

    :cond_12
    const/4 v9, 0x1

    invoke-static {v10}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    invoke-static {v14}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    if-nez v9, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->delete()Z

    :cond_13
    invoke-interface/range {p2 .. p2}, Lorg/apache/tools/ant/taskdefs/Get$DownloadProgress;->endDownload()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->useTimestamp:Z

    move/from16 v25, v0

    if-eqz v25, :cond_15

    invoke-virtual {v5}, Ljava/net/URLConnection;->getLastModified()J

    move-result-wide v19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->verbose:Z

    move/from16 v25, v0

    if-eqz v25, :cond_14

    new-instance v21, Ljava/util/Date;

    move-object/from16 v0, v21

    move-wide/from16 v1, v19

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const-string v26, "last modified = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-wide/16 v27, 0x0

    cmp-long v25, v19, v27

    if-nez v25, :cond_16

    const-string v25, " - using current time instead"

    :goto_3
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;I)V

    :cond_14
    const-wide/16 v25, 0x0

    cmp-long v25, v19, v25

    if-eqz v25, :cond_15

    sget-object v25, Lorg/apache/tools/ant/taskdefs/Get;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-wide/from16 v2, v19

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/tools/ant/util/FileUtils;->setFileLastModified(Ljava/io/File;J)V

    :cond_15
    const/16 v25, 0x1

    goto/16 :goto_0

    :cond_16
    const-string v25, ""

    goto :goto_3
.end method

.method public execute()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Get;->verbose:Z

    if-eqz v3, :cond_0

    new-instance v2, Lorg/apache/tools/ant/taskdefs/Get$VerboseProgress;

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/taskdefs/Get$VerboseProgress;-><init>(Ljava/io/PrintStream;)V

    :cond_0
    :try_start_0
    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Get;->doGet(ILorg/apache/tools/ant/taskdefs/Get$DownloadProgress;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Error getting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Get;->source:Ljava/net/URL;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/Get;->log(Ljava/lang/String;)V

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Get;->ignoreErrors:Z

    if-nez v3, :cond_1

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Get;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v3
.end method

.method public setDest(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Get;->dest:Ljava/io/File;

    return-void
.end method

.method public setIgnoreErrors(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Get;->ignoreErrors:Z

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Get;->pword:Ljava/lang/String;

    return-void
.end method

.method public setSrc(Ljava/net/URL;)V
    .locals 0
    .param p1    # Ljava/net/URL;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Get;->source:Ljava/net/URL;

    return-void
.end method

.method public setUseTimestamp(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Get;->useTimestamp:Z

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Get;->uname:Ljava/lang/String;

    return-void
.end method

.method public setVerbose(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Get;->verbose:Z

    return-void
.end method
