.class public Lorg/apache/tools/ant/taskdefs/VerifyJar;
.super Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;
.source "VerifyJar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/VerifyJar$1;,
        Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilterReader;,
        Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;
    }
.end annotation


# static fields
.field public static final ERROR_NO_FILE:Ljava/lang/String; = "Not found :"

.field public static final ERROR_NO_VERIFY:Ljava/lang/String; = "Failed to verify "

.field private static final VERIFIED_TEXT:Ljava/lang/String; = "jar verified."


# instance fields
.field private certificates:Z

.field private outputCache:Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->certificates:Z

    new-instance v0, Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;-><init>(Lorg/apache/tools/ant/taskdefs/VerifyJar$1;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->outputCache:Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;

    return-void
.end method

.method private verifyOneJar(Ljava/io/File;)V
    .locals 7
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Not found :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->createJarSigner()Lorg/apache/tools/ant/taskdefs/ExecTask;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->setCommonOptions(Lorg/apache/tools/ant/taskdefs/ExecTask;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->bindToKeystore(Lorg/apache/tools/ant/taskdefs/ExecTask;)V

    const-string v4, "-verify"

    invoke-virtual {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->certificates:Z

    if-eqz v4, :cond_1

    const-string v4, "-certs"

    invoke-virtual {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Verifying JAR: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->log(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->outputCache:Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;->clear()V

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->execute()V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->outputCache:Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_2

    const-string v4, "zip file closed"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_3

    const-string v4, "You are running jarsigner against a JVM with a known bug that manifests as an IllegalStateException."

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->log(Ljava/lang/String;I)V

    :cond_2
    const-string v4, "jar verified."

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_4

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Failed to verify "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v1

    move-object v2, v1

    goto :goto_0

    :cond_3
    throw v2

    :cond_4
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v6, 0x1

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->jar:Ljava/io/File;

    if-eqz v7, :cond_0

    move v1, v6

    :goto_0
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->hasResources()Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "jar must be set through jar attribute or nested filesets"

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->beginExecution()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->getRedirector()Lorg/apache/tools/ant/types/RedirectorElement;

    move-result-object v4

    invoke-virtual {v4, v6}, Lorg/apache/tools/ant/types/RedirectorElement;->setAlwaysLog(Z)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/RedirectorElement;->createOutputFilterChain()Lorg/apache/tools/ant/types/FilterChain;

    move-result-object v3

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->outputCache:Lorg/apache/tools/ant/taskdefs/VerifyJar$BufferingOutputFilter;

    invoke-virtual {v3, v6}, Lorg/apache/tools/ant/types/FilterChain;->add(Lorg/apache/tools/ant/filters/ChainableReader;)V

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->createUnifiedSourcePath()Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Path;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->verifyOneJar(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v6

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->endExecution()V

    throw v6

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/VerifyJar;->endExecution()V

    return-void
.end method

.method public setCertificates(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/VerifyJar;->certificates:Z

    return-void
.end method
