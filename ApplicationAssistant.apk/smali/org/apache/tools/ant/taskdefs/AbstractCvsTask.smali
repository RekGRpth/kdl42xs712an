.class public abstract Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;
.super Lorg/apache/tools/ant/Task;
.source "AbstractCvsTask.java"


# static fields
.field private static final DEFAULT_COMMAND:Ljava/lang/String; = "checkout"

.field public static final DEFAULT_COMPRESSION_LEVEL:I = 0x3

.field private static final MAXIMUM_COMRESSION_LEVEL:I = 0x9


# instance fields
.field private append:Z

.field private cmd:Lorg/apache/tools/ant/types/Commandline;

.field private command:Ljava/lang/String;

.field private compression:I

.field private cvsPackage:Ljava/lang/String;

.field private cvsRoot:Ljava/lang/String;

.field private cvsRsh:Ljava/lang/String;

.field private dest:Ljava/io/File;

.field private error:Ljava/io/File;

.field private errorStream:Ljava/io/OutputStream;

.field private executeStreamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

.field private failOnError:Z

.field private noexec:Z

.field private output:Ljava/io/File;

.field private outputStream:Ljava/io/OutputStream;

.field private passFile:Ljava/io/File;

.field private port:I

.field private quiet:Z

.field private reallyquiet:Z

.field private tag:Ljava/lang/String;

.field private vecCommandlines:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cmd:Lorg/apache/tools/ant/types/Commandline;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->vecCommandlines:Ljava/util/Vector;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->command:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->quiet:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->reallyquiet:Z

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->compression:I

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->noexec:Z

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->port:I

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->append:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->failOnError:Z

    return-void
.end method

.method private executeToString(Lorg/apache/tools/ant/taskdefs/Execute;)Ljava/lang/String;
    .locals 5
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Execute;->getCommandline()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/tools/ant/types/Commandline;->describeCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    sget-object v0, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Execute;->getEnvironment()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "environment:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v3, 0x0

    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "\t"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public addCommandArgument(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {p0, v0, p1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->addCommandArgument(Lorg/apache/tools/ant/types/Commandline;Ljava/lang/String;)V

    return-void
.end method

.method public addCommandArgument(Lorg/apache/tools/ant/types/Commandline;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Commandline;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public addConfiguredCommandline(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->addConfiguredCommandline(Lorg/apache/tools/ant/types/Commandline;Z)V

    return-void
.end method

.method public addConfiguredCommandline(Lorg/apache/tools/ant/types/Commandline;Z)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Commandline;
    .param p2    # Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->configureCommandline(Lorg/apache/tools/ant/types/Commandline;)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->vecCommandlines:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->vecCommandlines:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected configureCommandline(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const/4 v3, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "cvs"

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsPackage:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setLine(Ljava/lang/String;)V

    :cond_2
    iget v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->compression:I

    if-lez v0, :cond_3

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->compression:I

    const/16 v1, 0x9

    if-gt v0, v1, :cond_3

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "-z"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->compression:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->quiet:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->reallyquiet:Z

    if-nez v0, :cond_4

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-q"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->reallyquiet:Z

    if-eqz v0, :cond_5

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-Q"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_5
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->noexec:Z

    if-eqz v0, :cond_6

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    const-string v1, "-n"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRoot:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "-d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRoot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setLine(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public execute()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v5, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getCommand()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getCommand()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->vecCommandlines:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "checkout"

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setCommand(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getCommand()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cmd:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Commandline;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v1, v5}, Lorg/apache/tools/ant/types/Commandline;->createArgument(Z)Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v4

    invoke-virtual {v4, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setLine(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v5}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->addConfiguredCommandline(Lorg/apache/tools/ant/types/Commandline;Z)V

    :cond_1
    const/4 v2, 0x0

    :goto_0
    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->vecCommandlines:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->vecCommandlines:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->runCommand(Lorg/apache/tools/ant/types/Commandline;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->removeCommandline(Lorg/apache/tools/ant/types/Commandline;)V

    :cond_3
    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setCommand(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->outputStream:Ljava/io/OutputStream;

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->errorStream:Ljava/io/OutputStream;

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    return-void

    :catchall_0
    move-exception v4

    if-eqz v1, :cond_4

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->removeCommandline(Lorg/apache/tools/ant/types/Commandline;)V

    :cond_4
    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setCommand(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->outputStream:Ljava/io/OutputStream;

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->errorStream:Ljava/io/OutputStream;

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    throw v4
.end method

.method public getCommand()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->command:Ljava/lang/String;

    return-object v0
.end method

.method public getCvsRoot()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRoot:Ljava/lang/String;

    return-object v0
.end method

.method public getCvsRsh()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRsh:Ljava/lang/String;

    return-object v0
.end method

.method public getDest()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->dest:Ljava/io/File;

    return-object v0
.end method

.method protected getErrorStream()Ljava/io/OutputStream;
    .locals 6

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->errorStream:Ljava/io/OutputStream;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->error:Ljava/io/File;

    if-eqz v1, :cond_1

    :try_start_0
    new-instance v1, Ljava/io/PrintStream;

    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->error:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->append:Z

    invoke-direct {v3, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setErrorStream(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->errorStream:Ljava/io/OutputStream;

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_1
    new-instance v1, Lorg/apache/tools/ant/taskdefs/LogOutputStream;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;-><init>(Lorg/apache/tools/ant/Task;I)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setErrorStream(Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method protected getExecuteStreamHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->executeStreamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getErrorStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setExecuteStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->executeStreamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    return-object v0
.end method

.method protected getOutputStream()Ljava/io/OutputStream;
    .locals 6

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->outputStream:Ljava/io/OutputStream;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->output:Ljava/io/File;

    if-eqz v1, :cond_1

    :try_start_0
    new-instance v1, Ljava/io/PrintStream;

    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->output:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->append:Z

    invoke-direct {v3, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setOutputStream(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->outputStream:Ljava/io/OutputStream;

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_1
    new-instance v1, Lorg/apache/tools/ant/taskdefs/LogOutputStream;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;-><init>(Lorg/apache/tools/ant/Task;I)V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setOutputStream(Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method public getPackage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getPassFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->port:I

    return v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->tag:Ljava/lang/String;

    return-object v0
.end method

.method protected removeCommandline(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->vecCommandlines:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    return-void
.end method

.method protected runCommand(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 13
    .param p1    # Lorg/apache/tools/ant/types/Commandline;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v12, 0x3

    const/4 v11, 0x1

    new-instance v3, Lorg/apache/tools/ant/types/Environment;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/Environment;-><init>()V

    iget v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->port:I

    if-lez v8, :cond_0

    new-instance v7, Lorg/apache/tools/ant/types/Environment$Variable;

    invoke-direct {v7}, Lorg/apache/tools/ant/types/Environment$Variable;-><init>()V

    const-string v8, "CVS_CLIENT_PORT"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Environment$Variable;->setKey(Ljava/lang/String;)V

    iget v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->port:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Environment$Variable;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Lorg/apache/tools/ant/types/Environment;->addVariable(Lorg/apache/tools/ant/types/Environment$Variable;)V

    :cond_0
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    if-nez v8, :cond_1

    new-instance v1, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "cygwin.user.home"

    const-string v10, "user.home"

    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    sget-char v9, Ljava/io/File;->separatorChar:C

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, ".cvspass"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setPassfile(Ljava/io/File;)V

    :cond_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->canRead()Z

    move-result v8

    if-eqz v8, :cond_6

    new-instance v7, Lorg/apache/tools/ant/types/Environment$Variable;

    invoke-direct {v7}, Lorg/apache/tools/ant/types/Environment$Variable;-><init>()V

    const-string v8, "CVS_PASSFILE"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Environment$Variable;->setKey(Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Environment$Variable;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Lorg/apache/tools/ant/types/Environment;->addVariable(Lorg/apache/tools/ant/types/Environment$Variable;)V

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Using cvs passfile: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v12}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    :cond_2
    :goto_0
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRsh:Ljava/lang/String;

    if-eqz v8, :cond_3

    new-instance v7, Lorg/apache/tools/ant/types/Environment$Variable;

    invoke-direct {v7}, Lorg/apache/tools/ant/types/Environment$Variable;-><init>()V

    const-string v8, "CVS_RSH"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Environment$Variable;->setKey(Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRsh:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Environment$Variable;->setValue(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Lorg/apache/tools/ant/types/Environment;->addVariable(Lorg/apache/tools/ant/types/Environment$Variable;)V

    :cond_3
    new-instance v4, Lorg/apache/tools/ant/taskdefs/Execute;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getExecuteStreamHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {v4, v8, v9}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v4, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->dest:Ljava/io/File;

    if-nez v8, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->dest:Ljava/io/File;

    :cond_4
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->dest:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_5

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->dest:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    :cond_5
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->dest:Ljava/io/File;

    invoke-virtual {v4, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->setWorkingDirectory(Ljava/io/File;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Environment;->getVariables()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->setEnvironment([Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0, v4}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->executeToString(Lorg/apache/tools/ant/taskdefs/Execute;)Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x3

    invoke-virtual {p0, v0, v8}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    move-result v5

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "retCode="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->failOnError:Z

    if-eqz v8, :cond_9

    invoke-static {v5}, Lorg/apache/tools/ant/taskdefs/Execute;->isFailure(I)Z

    move-result v8

    if-eqz v8, :cond_9

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "cvs exited with error code "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    sget-object v10, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "Command line was ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :catch_0
    move-exception v2

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->failOnError:Z

    if-eqz v8, :cond_8

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v9

    invoke-direct {v8, v2, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_6
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->canRead()Z

    move-result v8

    if-nez v8, :cond_7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "cvs passfile: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " ignored as it is not readable"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v11}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "cvs passfile: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " ignored as it is not a file"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v11}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_8
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Caught exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v11}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    :cond_9
    :goto_1
    return-void

    :catch_1
    move-exception v2

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->failOnError:Z

    if-eqz v8, :cond_a

    throw v2

    :cond_a
    invoke-virtual {v2}, Lorg/apache/tools/ant/BuildException;->getException()Ljava/lang/Throwable;

    move-result-object v6

    if-nez v6, :cond_b

    move-object v6, v2

    :cond_b
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Caught exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v11}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    goto :goto_1

    :catch_2
    move-exception v2

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->failOnError:Z

    if-eqz v8, :cond_c

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v9

    invoke-direct {v8, v2, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_c
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Caught exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v11}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->log(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public setAppend(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->append:Z

    return-void
.end method

.method public setCommand(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->command:Ljava/lang/String;

    return-void
.end method

.method public setCompression(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->setCompressionLevel(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCompressionLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->compression:I

    return-void
.end method

.method public setCvsRoot(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRoot:Ljava/lang/String;

    return-void
.end method

.method public setCvsRsh(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsRsh:Ljava/lang/String;

    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "-D"

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->addCommandArgument(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->addCommandArgument(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setDest(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->dest:Ljava/io/File;

    return-void
.end method

.method public setError(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->error:Ljava/io/File;

    return-void
.end method

.method protected setErrorStream(Ljava/io/OutputStream;)V
    .locals 0
    .param p1    # Ljava/io/OutputStream;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->errorStream:Ljava/io/OutputStream;

    return-void
.end method

.method public setExecuteStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->executeStreamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    return-void
.end method

.method public setFailOnError(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->failOnError:Z

    return-void
.end method

.method public setNoexec(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->noexec:Z

    return-void
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->output:Ljava/io/File;

    return-void
.end method

.method protected setOutputStream(Ljava/io/OutputStream;)V
    .locals 0
    .param p1    # Ljava/io/OutputStream;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->outputStream:Ljava/io/OutputStream;

    return-void
.end method

.method public setPackage(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->cvsPackage:Ljava/lang/String;

    return-void
.end method

.method public setPassfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->passFile:Ljava/io/File;

    return-void
.end method

.method public setPort(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->port:I

    return-void
.end method

.method public setQuiet(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->quiet:Z

    return-void
.end method

.method public setReallyquiet(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->reallyquiet:Z

    return-void
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->tag:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "-r"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->addCommandArgument(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
