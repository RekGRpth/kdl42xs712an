.class public Lorg/apache/tools/ant/taskdefs/ExecTask;
.super Lorg/apache/tools/ant/Task;
.source "ExecTask.java"


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field protected cmdl:Lorg/apache/tools/ant/types/Commandline;

.field private dir:Ljava/io/File;

.field private env:Lorg/apache/tools/ant/types/Environment;

.field private error:Ljava/io/File;

.field private executable:Ljava/lang/String;

.field private failIfExecFails:Z

.field protected failOnError:Z

.field private incompatibleWithSpawn:Z

.field private input:Ljava/io/File;

.field private inputString:Ljava/lang/String;

.field protected newEnvironment:Z

.field private os:Ljava/lang/String;

.field private osFamily:Ljava/lang/String;

.field private output:Ljava/io/File;

.field protected redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

.field protected redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

.field private resolveExecutable:Z

.field private resultProperty:Ljava/lang/String;

.field private searchPath:Z

.field private spawn:Z

.field private timeout:Ljava/lang/Long;

.field private vmLauncher:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/ExecTask;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failOnError:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->newEnvironment:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->timeout:Ljava/lang/Long;

    new-instance v0, Lorg/apache/tools/ant/types/Environment;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Environment;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->env:Lorg/apache/tools/ant/types/Environment;

    new-instance v0, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failIfExecFails:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resolveExecutable:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->searchPath:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->spawn:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Redirector;-><init>(Lorg/apache/tools/ant/Task;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->vmLauncher:Z

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/Task;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/Task;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failOnError:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->newEnvironment:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->timeout:Ljava/lang/Long;

    new-instance v0, Lorg/apache/tools/ant/types/Environment;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Environment;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->env:Lorg/apache/tools/ant/types/Environment;

    new-instance v0, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failIfExecFails:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resolveExecutable:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->searchPath:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->spawn:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Redirector;-><init>(Lorg/apache/tools/ant/Task;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->vmLauncher:Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->bindToOwner(Lorg/apache/tools/ant/Task;)V

    return-void
.end method

.method private isPath(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "PATH="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Path="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addConfiguredRedirector(Lorg/apache/tools/ant/types/RedirectorElement;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/RedirectorElement;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "cannot have > 1 nested <redirector>s"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public addEnv(Lorg/apache/tools/ant/types/Environment$Variable;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Environment$Variable;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->env:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Environment;->addVariable(Lorg/apache/tools/ant/types/Environment$Variable;)V

    return-void
.end method

.method protected checkConfiguration()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->getExecutable()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "no executable specified"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "The directory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " is not a directory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->spawn:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    const-string v1, "spawn does not allow attributes related to input, output, error, result"

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    const-string v1, "spawn also does not allow timeout"

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    const-string v1, "finally, spawn is not compatible with a nested I/O <redirector>"

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "You have used an attribute or nested element which is not compatible with spawn"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->setupRedirector()V

    return-void
.end method

.method public createArg()Lorg/apache/tools/ant/types/Commandline$Argument;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    return-object v0
.end method

.method protected createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v0

    return-object v0
.end method

.method protected createWatchdog()Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->timeout:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->timeout:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;-><init>(J)V

    goto :goto_0
.end method

.method public execute()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->isValidOs()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->executable:Ljava/lang/String;

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->searchPath:Z

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/ExecTask;->resolveExecutable(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->checkConfiguration()V

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->prepareExec()Lorg/apache/tools/ant/taskdefs/Execute;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->runExec(Lorg/apache/tools/ant/taskdefs/Execute;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    goto :goto_0

    :catchall_0
    move-exception v1

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    throw v1
.end method

.method public getResolveExecutable()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resolveExecutable:Z

    return v0
.end method

.method protected isValidOs()Z
    .locals 5

    const/4 v4, 0x3

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->osFamily:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->osFamily:Ljava/lang/String;

    invoke-static {v2, v3, v3, v3}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isOs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const-string v2, "os.name"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Current OS is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v4}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->os:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->os:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "This OS, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " was not found in the specified list of valid OSes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->os:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v4}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected logFlush()V
    .locals 0

    return-void
.end method

.method protected maybeSetResultPropertyValue(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resultProperty:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resultProperty:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected prepareExec()Lorg/apache/tools/ant/taskdefs/Execute;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/types/RedirectorElement;->configure(Lorg/apache/tools/ant/taskdefs/Redirector;)V

    :cond_1
    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->createWatchdog()Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/taskdefs/Execute;->setWorkingDirectory(Ljava/io/File;)V

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->vmLauncher:Z

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/taskdefs/Execute;->setVMLauncher(Z)V

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->spawn:Z

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/taskdefs/Execute;->setSpawn(Z)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->env:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Environment;->getVariables()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Setting environment variable: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    aget-object v4, v0, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->newEnvironment:Z

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/taskdefs/Execute;->setNewenvironment(Z)V

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setEnvironment([Ljava/lang/String;)V

    return-object v1
.end method

.method protected resolveExecutable(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v10, 0x5

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resolveExecutable:Z

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v8, p1}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    if-eqz v8, :cond_3

    sget-object v8, Lorg/apache/tools/ant/taskdefs/ExecTask;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    invoke-virtual {v8, v9, p1}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_0

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->env:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Environment;->getVariables()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    const/4 v5, 0x0

    :goto_1
    array-length v8, v3

    if-ge v5, v8, :cond_4

    aget-object v8, v3, v5

    invoke-direct {p0, v8}, Lorg/apache/tools/ant/taskdefs/ExecTask;->isPath(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    new-instance v7, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    aget-object v9, v3, v5

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;Ljava/lang/String;)V

    :cond_4
    if-nez v7, :cond_6

    invoke-static {}, Lorg/apache/tools/ant/taskdefs/Execute;->getProcEnvironment()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/ExecTask;->isPath(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    new-instance v7, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    invoke-virtual {v6, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;Ljava/lang/String;)V

    :cond_6
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/Path;->list()[Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    :goto_2
    array-length v8, v0

    if-ge v5, v8, :cond_0

    sget-object v8, Lorg/apache/tools/ant/taskdefs/ExecTask;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    new-instance v9, Ljava/io/File;

    aget-object v10, v0, v5

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9, p1}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method protected runExec(Lorg/apache/tools/ant/taskdefs/Execute;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->describeCommand()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->runExecute(Lorg/apache/tools/ant/taskdefs/Execute;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->logFlush()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failIfExecFails:Z

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Execute failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->logFlush()V

    throw v1

    :cond_0
    :try_start_2
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Execute failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->logFlush()V

    goto :goto_0
.end method

.method protected final runExecute(Lorg/apache/tools/ant/taskdefs/Execute;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, -0x1

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->spawn:Z

    if-nez v2, :cond_4

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Execute;->killedProcess()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "Timeout: killed the sub-process"

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failOnError:Z

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V

    :cond_1
    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->maybeSetResultPropertyValue(I)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Redirector;->complete()V

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->isFailure(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failOnError:Z

    if-eqz v2, :cond_2

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getTaskType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " returned: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_2
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Execute;->spawn()V

    goto :goto_0
.end method

.method public setAppend(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setAppend(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setCommand(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    const-string v0, "The command attribute is deprecated.\nPlease use the executable attribute and nested arg elements."

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/ExecTask;->log(Ljava/lang/String;I)V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    return-void
.end method

.method public setDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->dir:Ljava/io/File;

    return-void
.end method

.method public setError(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->error:Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setErrorProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setErrorProperty(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setExecutable(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->executable:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->cmdl:Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    return-void
.end method

.method public setFailIfExecutionFails(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failIfExecFails:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setFailonerror(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->failOnError:Z

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setInput(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->inputString:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "The \"input\" and \"inputstring\" attributes cannot both be specified"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->input:Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setInputString(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->input:Ljava/io/File;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "The \"input\" and \"inputstring\" attributes cannot both be specified"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->inputString:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setLogError(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setLogError(Z)V

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setNewenvironment(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->newEnvironment:Z

    return-void
.end method

.method public setOs(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->os:Ljava/lang/String;

    return-void
.end method

.method public setOsFamily(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->osFamily:Ljava/lang/String;

    return-void
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->output:Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setOutputproperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutputProperty(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setResolveExecutable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resolveExecutable:Z

    return-void
.end method

.method public setResultProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->resultProperty:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setSearchPath(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->searchPath:Z

    return-void
.end method

.method public setSpawn(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->spawn:Z

    return-void
.end method

.method public setTimeout(Ljava/lang/Integer;)V
    .locals 3
    .param p1    # Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->setTimeout(Ljava/lang/Long;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    goto :goto_0
.end method

.method public setTimeout(Ljava/lang/Long;)V
    .locals 1
    .param p1    # Ljava/lang/Long;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->timeout:Ljava/lang/Long;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setVMLauncher(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->vmLauncher:Z

    return-void
.end method

.method protected setupRedirector()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->input:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInput(Ljava/io/File;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->inputString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInputString(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->output:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutput(Ljava/io/File;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/ExecTask;->error:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setError(Ljava/io/File;)V

    return-void
.end method
