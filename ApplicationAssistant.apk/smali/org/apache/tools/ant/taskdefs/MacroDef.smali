.class public Lorg/apache/tools/ant/taskdefs/MacroDef;
.super Lorg/apache/tools/ant/taskdefs/AntlibDefinition;
.source "MacroDef.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/MacroDef$MyAntTypeDefinition;,
        Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;,
        Lorg/apache/tools/ant/taskdefs/MacroDef$Text;,
        Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;,
        Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;
    }
.end annotation


# static fields
.field static class$org$apache$tools$ant$taskdefs$MacroInstance:Ljava/lang/Class;


# instance fields
.field private attributes:Ljava/util/List;

.field private backTrace:Z

.field private elements:Ljava/util/Map;

.field private hasImplicitElement:Z

.field private name:Ljava/lang/String;

.field private nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

.field private text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

.field private textName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/AntlibDefinition;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->backTrace:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->elements:Ljava/util/Map;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->textName:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->hasImplicitElement:Z

    return-void
.end method

.method static access$000(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->isValidName(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static access$100(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    invoke-static {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->objectHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static isValidName(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/MacroDef;->isValidNameCharacter(C)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isValidNameCharacter(C)Z
    .locals 1
    .param p0    # C

    invoke-static {p0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x2e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2d

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static objectHashCode(Ljava/lang/Object;)I
    .locals 1
    .param p0    # Ljava/lang/Object;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method private sameOrSimilar(Ljava/lang/Object;Z)Z
    .locals 5
    .param p1    # Ljava/lang/Object;
    .param p2    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    move v2, v1

    :cond_0
    :goto_0
    return v2

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/taskdefs/MacroDef;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, v0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/Location;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-nez p2, :cond_4

    move v2, v1

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    if-nez v3, :cond_8

    iget-object v3, v0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    if-nez v3, :cond_0

    :cond_5
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v3

    const-string v4, "antlib:org.apache.tools.ant"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_6
    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v3

    const-string v4, "antlib:org.apache.tools.ant"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_7
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->similar(Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->elements:Ljava/util/Map;

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/MacroDef;->elements:Ljava/util/Map;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    goto/16 :goto_0

    :cond_8
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    goto/16 :goto_0
.end method


# virtual methods
.method public addConfiguredAttribute(Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "the attribute nested element needed a \"name\" attribute"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->textName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "the name \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\" has already been used by the text element"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "the name \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\" has already been used in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "another attribute element"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addConfiguredElement(Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "the element nested element needed a \"name\" attribute"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->elements:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "the element "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " has already been specified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->hasImplicitElement:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->isImplicit()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->elements:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one element allowed when using implicit elements"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->isImplicit()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->hasImplicitElement:Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->elements:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public addConfiguredText(Lorg/apache/tools/ant/taskdefs/MacroDef$Text;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Only one nested text element allowed"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->getName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "the text nested element needed a \"name\" attribute"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "the name \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\" is already used as an attribute"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->textName:Ljava/lang/String;

    return-void
.end method

.method public createSequential()Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one sequential allowed"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    return-object v0
.end method

.method public execute()V
    .locals 4

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Missing sequential element"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    if-nez v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Name not specified"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getURI()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lorg/apache/tools/ant/ProjectHelper;->genComponentName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/MacroDef$MyAntTypeDefinition;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/MacroDef$MyAntTypeDefinition;-><init>(Lorg/apache/tools/ant/taskdefs/MacroDef;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/MacroDef$MyAntTypeDefinition;->setName(Ljava/lang/String;)V

    sget-object v2, Lorg/apache/tools/ant/taskdefs/MacroDef;->class$org$apache$tools$ant$taskdefs$MacroInstance:Ljava/lang/Class;

    if-nez v2, :cond_2

    const-string v2, "org.apache.tools.ant.taskdefs.MacroInstance"

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/MacroDef;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/taskdefs/MacroDef;->class$org$apache$tools$ant$taskdefs$MacroInstance:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/MacroDef$MyAntTypeDefinition;->setClass(Ljava/lang/Class;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/ComponentHelper;->addDataTypeDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "creating macro  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/MacroDef;->log(Ljava/lang/String;I)V

    return-void

    :cond_2
    sget-object v2, Lorg/apache/tools/ant/taskdefs/MacroDef;->class$org$apache$tools$ant$taskdefs$MacroInstance:Ljava/lang/Class;

    goto :goto_0
.end method

.method public getAttributes()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->attributes:Ljava/util/List;

    return-object v0
.end method

.method public getBackTrace()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->backTrace:Z

    return v0
.end method

.method public getElements()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->elements:Ljava/util/Map;

    return-object v0
.end method

.method public getNestedTask()Lorg/apache/tools/ant/UnknownElement;
    .locals 5

    new-instance v2, Lorg/apache/tools/ant/UnknownElement;

    const-string v3, "sequential"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/UnknownElement;-><init>(Ljava/lang/String;)V

    const-string v3, "sequential"

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/UnknownElement;->setTaskName(Ljava/lang/String;)V

    const-string v3, ""

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/UnknownElement;->setNamespace(Ljava/lang/String;)V

    const-string v3, "sequential"

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/UnknownElement;->setQName(Ljava/lang/String;)V

    new-instance v3, Lorg/apache/tools/ant/RuntimeConfigurable;

    const-string v4, "sequential"

    invoke-direct {v3, v2, v4}, Lorg/apache/tools/ant/RuntimeConfigurable;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->getNested()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->nestedSequential:Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/MacroDef$NestedSequential;->getNested()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/UnknownElement;->addChild(Lorg/apache/tools/ant/UnknownElement;)V

    invoke-virtual {v2}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/RuntimeConfigurable;->addChild(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public getText()Lorg/apache/tools/ant/taskdefs/MacroDef$Text;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->text:Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    return-object v0
.end method

.method public sameDefinition(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->sameOrSimilar(Ljava/lang/Object;Z)Z

    move-result v0

    return v0
.end method

.method public setBackTrace(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->backTrace:Z

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/MacroDef;->name:Ljava/lang/String;

    return-void
.end method

.method public similar(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/MacroDef;->sameOrSimilar(Ljava/lang/Object;Z)Z

    move-result v0

    return v0
.end method
