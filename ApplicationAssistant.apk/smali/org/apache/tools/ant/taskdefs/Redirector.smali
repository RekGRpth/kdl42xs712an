.class public Lorg/apache/tools/ant/taskdefs/Redirector;
.super Ljava/lang/Object;
.source "Redirector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;
    }
.end annotation


# static fields
.field private static final DEFAULT_ENCODING:Ljava/lang/String;


# instance fields
.field private alwaysLog:Z

.field private append:Z

.field private appendProperties:Z

.field private baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

.field private createEmptyFiles:Z

.field private error:[Ljava/io/File;

.field private errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

.field private errorEncoding:Ljava/lang/String;

.field private errorFilterChains:Ljava/util/Vector;

.field private errorPrintStream:Ljava/io/PrintStream;

.field private errorProperty:Ljava/lang/String;

.field private errorStream:Ljava/io/OutputStream;

.field private input:[Ljava/io/File;

.field private inputEncoding:Ljava/lang/String;

.field private inputFilterChains:Ljava/util/Vector;

.field private inputStream:Ljava/io/InputStream;

.field private inputString:Ljava/lang/String;

.field private logError:Z

.field private logInputString:Z

.field private managingTask:Lorg/apache/tools/ant/ProjectComponent;

.field private out:[Ljava/io/File;

.field private outPrintStream:Ljava/io/PrintStream;

.field private outputEncoding:Ljava/lang/String;

.field private outputFilterChains:Ljava/util/Vector;

.field private outputProperty:Ljava/lang/String;

.field private outputStream:Ljava/io/OutputStream;

.field private threadGroup:Ljava/lang/ThreadGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "file.encoding"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Redirector;->DEFAULT_ENCODING:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/ProjectComponent;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/ProjectComponent;

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->logError:Z

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->append:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->alwaysLog:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->createEmptyFiles:Z

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Redirector;->DEFAULT_ENCODING:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputEncoding:Ljava/lang/String;

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Redirector;->DEFAULT_ENCODING:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorEncoding:Ljava/lang/String;

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Redirector;->DEFAULT_ENCODING:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->appendProperties:Z

    new-instance v0, Ljava/lang/ThreadGroup;

    const-string v1, "redirector"

    invoke-direct {v0, v1}, Ljava/lang/ThreadGroup;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->threadGroup:Ljava/lang/ThreadGroup;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->logInputString:Z

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/Task;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Task;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;-><init>(Lorg/apache/tools/ant/ProjectComponent;)V

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/taskdefs/Redirector;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->append:Z

    return v0
.end method

.method static access$100(Lorg/apache/tools/ant/taskdefs/Redirector;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->appendProperties:Z

    return v0
.end method

.method static access$200(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/taskdefs/Redirector;
    .param p1    # Ljava/io/ByteArrayOutputStream;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/Redirector;->setPropertyFromBAOS(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V

    return-void
.end method

.method private foldFiles([Ljava/io/File;Ljava/lang/String;I)Ljava/io/OutputStream;
    .locals 10
    .param p1    # [Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v7, 0x0

    new-instance v3, Lorg/apache/tools/ant/util/LazyFileOutputStream;

    aget-object v4, p1, v7

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->append:Z

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->createEmptyFiles:Z

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/tools/ant/util/LazyFileOutputStream;-><init>(Ljava/io/File;ZZ)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v6, p1, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p3}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    new-array v0, v4, [C

    const/16 v4, 0x20

    invoke-static {v0, v4}, Ljava/util/Arrays;->fill([CC)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    const/4 v1, 0x1

    :goto_0
    array-length v4, p1

    if-ge v1, v4, :cond_0

    new-instance v4, Lorg/apache/tools/ant/util/TeeOutputStream;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    new-instance v6, Lorg/apache/tools/ant/util/LazyFileOutputStream;

    aget-object v7, p1, v1

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->append:Z

    iget-boolean v9, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->createEmptyFiles:Z

    invoke-direct {v6, v7, v8, v9}, Lorg/apache/tools/ant/util/LazyFileOutputStream;-><init>(Ljava/io/File;ZZ)V

    invoke-direct {v4, v5, v6}, Lorg/apache/tools/ant/util/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    iput-object v4, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v6, p1, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p3}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private setPropertyFromBAOS(Ljava/io/ByteArrayOutputStream;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/io/ByteArrayOutputStream;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/StringReader;

    invoke-static {p1}, Lorg/apache/tools/ant/taskdefs/Execute;->toString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {v3}, Lorg/apache/tools/ant/ProjectComponent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p2, v4}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized complete()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/PrintStream;->flush()V

    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/PrintStream;->flush()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->threadGroup:Ljava/lang/ThreadGroup;

    invoke-virtual {v2}, Ljava/lang/ThreadGroup;->activeCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_2

    :try_start_1
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "waiting for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->threadGroup:Ljava/lang/ThreadGroup;

    invoke-virtual {v4}, Ljava/lang/ThreadGroup;->activeCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " Threads:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->threadGroup:Ljava/lang/ThreadGroup;

    invoke-virtual {v2}, Ljava/lang/ThreadGroup;->activeCount()I

    move-result v2

    new-array v1, v2, [Ljava/lang/Thread;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->threadGroup:Ljava/lang/ThreadGroup;

    invoke-virtual {v2, v1}, Ljava/lang/ThreadGroup;->enumerate([Ljava/lang/Thread;)I

    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_1

    :try_start_2
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-wide/16 v2, 0x3e8

    :try_start_3
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_2
    :try_start_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Redirector;->setProperties()V

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :catch_1
    move-exception v2

    goto :goto_2
.end method

.method public declared-synchronized createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Redirector;->createStreams()V

    new-instance v0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized createStreams()V
    .locals 23

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->out:[Ljava/io/File;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->out:[Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    if-lez v19, :cond_0

    new-instance v20, Ljava/lang/StringBuffer;

    const-string v19, "Output "

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->append:Z

    move/from16 v19, v0

    if-eqz v19, :cond_13

    const-string v19, "appended"

    :goto_0
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, " to "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->out:[Ljava/io/File;

    move-object/from16 v19, v0

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v13, v2}, Lorg/apache/tools/ant/taskdefs/Redirector;->foldFiles([Ljava/io/File;Ljava/lang/String;I)Ljava/io/OutputStream;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputProperty:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v19, v0

    if-nez v19, :cond_1

    new-instance v19, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputProperty:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;-><init>(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string v21, "Output redirected to property: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputProperty:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x3

    invoke-virtual/range {v19 .. v21}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    :cond_1
    new-instance v12, Lorg/apache/tools/ant/util/KeepAliveOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v12, v0}, Lorg/apache/tools/ant/util/KeepAliveOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    if-nez v19, :cond_14

    :goto_1
    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->error:[Ljava/io/File;

    move-object/from16 v19, v0

    if-eqz v19, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->error:[Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    if-lez v19, :cond_17

    new-instance v20, Ljava/lang/StringBuffer;

    const-string v19, "Error "

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->append:Z

    move/from16 v19, v0

    if-eqz v19, :cond_16

    const-string v19, "appended"

    :goto_3
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, " to "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->error:[Ljava/io/File;

    move-object/from16 v19, v0

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v13, v2}, Lorg/apache/tools/ant/taskdefs/Redirector;->foldFiles([Ljava/io/File;Ljava/lang/String;I)Ljava/io/OutputStream;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorProperty:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v19, v0

    if-nez v19, :cond_3

    new-instance v19, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorProperty:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;-><init>(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string v21, "Error redirected to property: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorProperty:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x3

    invoke-virtual/range {v19 .. v21}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    :cond_3
    new-instance v11, Lorg/apache/tools/ant/util/KeepAliveOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Lorg/apache/tools/ant/util/KeepAliveOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->error:[Ljava/io/File;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->error:[Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    if-nez v19, :cond_18

    :cond_4
    :goto_5
    move-object/from16 v0, p0

    iput-object v11, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->alwaysLog:Z

    move/from16 v19, v0

    if-nez v19, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    if-nez v19, :cond_6

    :cond_5
    new-instance v15, Lorg/apache/tools/ant/taskdefs/LogOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    const/16 v20, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v15, v0, v1}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;-><init>(Lorg/apache/tools/ant/ProjectComponent;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    if-nez v19, :cond_1a

    :goto_7
    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->alwaysLog:Z

    move/from16 v19, v0

    if-nez v19, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    if-nez v19, :cond_8

    :cond_7
    new-instance v5, Lorg/apache/tools/ant/taskdefs/LogOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v5, v0, v1}, Lorg/apache/tools/ant/taskdefs/LogOutputStream;-><init>(Lorg/apache/tools/ant/ProjectComponent;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    if-nez v19, :cond_1b

    :goto_8
    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    if-eqz v19, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    if-gtz v19, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v19

    if-nez v19, :cond_c

    :cond_a
    :try_start_1
    new-instance v17, Lorg/apache/tools/ant/util/LeadPipeInputStream;

    invoke-direct/range {v17 .. v17}, Lorg/apache/tools/ant/util/LeadPipeInputStream;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/LeadPipeInputStream;->setManagingComponent(Lorg/apache/tools/ant/ProjectComponent;)V

    move-object/from16 v14, v17

    new-instance v16, Ljava/io/InputStreamReader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v14, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    if-eqz v19, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    if-lez v19, :cond_b

    new-instance v10, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v10}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/ProjectComponent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    invoke-virtual {v10}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v16

    :cond_b
    new-instance v14, Lorg/apache/tools/ant/util/ReaderInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lorg/apache/tools/ant/util/ReaderInputStream;-><init>(Ljava/io/Reader;Ljava/lang/String;)V

    new-instance v18, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->threadGroup:Ljava/lang/ThreadGroup;

    move-object/from16 v19, v0

    new-instance v20, Lorg/apache/tools/ant/taskdefs/StreamPumper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v14, v1, v2}, Lorg/apache/tools/ant/taskdefs/StreamPumper;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    const-string v21, "output pumper"

    invoke-direct/range {v18 .. v21}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Thread;->setPriority(I)V

    new-instance v19, Ljava/io/PipedOutputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/PipedOutputStream;-><init>(Ljava/io/PipedInputStream;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_c
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    if-eqz v19, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    if-gtz v19, :cond_e

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v19

    if-nez v19, :cond_10

    :cond_e
    :try_start_3
    new-instance v17, Lorg/apache/tools/ant/util/LeadPipeInputStream;

    invoke-direct/range {v17 .. v17}, Lorg/apache/tools/ant/util/LeadPipeInputStream;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/LeadPipeInputStream;->setManagingComponent(Lorg/apache/tools/ant/ProjectComponent;)V

    move-object/from16 v4, v17

    new-instance v16, Ljava/io/InputStreamReader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v4, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    if-eqz v19, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    if-lez v19, :cond_f

    new-instance v10, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v10}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/ProjectComponent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    invoke-virtual {v10}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v16

    :cond_f
    new-instance v4, Lorg/apache/tools/ant/util/ReaderInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorEncoding:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v4, v0, v1}, Lorg/apache/tools/ant/util/ReaderInputStream;-><init>(Ljava/io/Reader;Ljava/lang/String;)V

    new-instance v18, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->threadGroup:Ljava/lang/ThreadGroup;

    move-object/from16 v19, v0

    new-instance v20, Lorg/apache/tools/ant/taskdefs/StreamPumper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v4, v1, v2}, Lorg/apache/tools/ant/taskdefs/StreamPumper;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    const-string v21, "error pumper"

    invoke-direct/range {v18 .. v21}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Ljava/lang/Thread;->setPriority(I)V

    new-instance v19, Ljava/io/PipedOutputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/PipedOutputStream;-><init>(Ljava/io/PipedInputStream;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Thread;->start()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_10
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->input:[Ljava/io/File;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->input:[Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    if-lez v19, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v20, v0

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v21, "Redirecting input from file"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->input:[Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    const/16 v22, 0x1

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_1c

    const-string v19, ""

    :goto_9
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v21, 0x3

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    new-instance v19, Lorg/apache/tools/ant/util/ConcatFileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->input:[Ljava/io/File;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lorg/apache/tools/ant/util/ConcatFileInputStream;-><init>([Ljava/io/File;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    move-object/from16 v19, v0

    check-cast v19, Lorg/apache/tools/ant/util/ConcatFileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lorg/apache/tools/ant/util/ConcatFileInputStream;->setManagingComponent(Lorg/apache/tools/ant/ProjectComponent;)V

    :cond_11
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    move-object/from16 v19, v0

    if-eqz v19, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    if-eqz v19, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Vector;->size()I

    move-result v19

    if-lez v19, :cond_12

    new-instance v10, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v10}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/tools/ant/ProjectComponent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    new-instance v19, Ljava/io/InputStreamReader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputFilterChains:Ljava/util/Vector;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    new-instance v19, Lorg/apache/tools/ant/util/ReaderInputStream;

    invoke-virtual {v10}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-direct/range {v19 .. v21}, Lorg/apache/tools/ant/util/ReaderInputStream;-><init>(Ljava/io/Reader;Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_12
    monitor-exit p0

    return-void

    :cond_13
    :try_start_9
    const-string v19, "redirected"

    goto/16 :goto_0

    :cond_14
    new-instance v19, Lorg/apache/tools/ant/util/TeeOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v12}, Lorg/apache/tools/ant/util/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    move-object/from16 v12, v19

    goto/16 :goto_1

    :cond_15
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2

    :catchall_0
    move-exception v19

    monitor-exit p0

    throw v19

    :cond_16
    :try_start_a
    const-string v19, "redirected"

    goto/16 :goto_3

    :cond_17
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->logError:Z

    move/from16 v19, v0

    if-nez v19, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    if-eqz v19, :cond_2

    const-wide/16 v7, 0x0

    new-instance v9, Lorg/apache/tools/ant/util/OutputStreamFunneler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v9, v0, v7, v8}, Lorg/apache/tools/ant/util/OutputStreamFunneler;-><init>(Ljava/io/OutputStream;J)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    invoke-virtual {v9}, Lorg/apache/tools/ant/util/OutputStreamFunneler;->getFunnelInstance()Ljava/io/OutputStream;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v9}, Lorg/apache/tools/ant/util/OutputStreamFunneler;->getFunnelInstance()Ljava/io/OutputStream;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_4

    :catch_0
    move-exception v6

    :try_start_c
    new-instance v19, Lorg/apache/tools/ant/BuildException;

    const-string v20, "error splitting output/error streams"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19

    :cond_18
    new-instance v19, Lorg/apache/tools/ant/util/TeeOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v11}, Lorg/apache/tools/ant/util/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    move-object/from16 v11, v19

    goto/16 :goto_5

    :cond_19
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    goto/16 :goto_6

    :cond_1a
    new-instance v19, Lorg/apache/tools/ant/util/TeeOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v15, v1}, Lorg/apache/tools/ant/util/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    move-object/from16 v15, v19

    goto/16 :goto_7

    :cond_1b
    new-instance v19, Lorg/apache/tools/ant/util/TeeOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v5, v1}, Lorg/apache/tools/ant/util/TeeOutputStream;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    move-object/from16 v5, v19

    goto/16 :goto_8

    :catch_1
    move-exception v6

    new-instance v19, Lorg/apache/tools/ant/BuildException;

    const-string v20, "error setting up output stream"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19

    :catch_2
    move-exception v6

    new-instance v19, Lorg/apache/tools/ant/BuildException;

    const-string v20, "error setting up error stream"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19

    :cond_1c
    const-string v19, "s"

    goto/16 :goto_9

    :catch_3
    move-exception v6

    new-instance v19, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v19

    invoke-direct {v0, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v19

    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputString:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_11

    new-instance v3, Ljava/lang/StringBuffer;

    const-string v19, "Using input "

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->logInputString:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1e

    const/16 v19, 0x22

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const/16 v20, 0x22

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    move-object/from16 v19, v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x3

    invoke-virtual/range {v19 .. v21}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    new-instance v19, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputString:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    goto/16 :goto_a

    :cond_1e
    const-string v19, "string"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_b

    :catch_4
    move-exception v6

    new-instance v19, Lorg/apache/tools/ant/BuildException;

    const-string v20, "error setting up input stream"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0
.end method

.method public declared-synchronized getErrorStream()Ljava/io/OutputStream;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getInputStream()Ljava/io/InputStream;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getOutputStream()Ljava/io/OutputStream;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized handleErrorFlush(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/PrintStream;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized handleErrorOutput(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/PrintStream;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorStream:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorPrintStream:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized handleFlush(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/PrintStream;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized handleInput([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->managingTask:Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {v0}, Lorg/apache/tools/ant/ProjectComponent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/Project;->defaultInput([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized handleOutput(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/PrintStream;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputStream:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outPrintStream:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAlwaysLog(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->alwaysLog:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAppend(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->append:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAppendProperties(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->appendProperties:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCreateEmptyFiles(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->createEmptyFiles:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setError(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->setError([Ljava/io/File;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/File;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    goto :goto_0
.end method

.method public declared-synchronized setError([Ljava/io/File;)V
    .locals 1
    .param p1    # [Ljava/io/File;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->error:[Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setErrorEncoding(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "errorEncoding must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorEncoding:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setErrorFilterChains(Ljava/util/Vector;)V
    .locals 1
    .param p1    # Ljava/util/Vector;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorFilterChains:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setErrorProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorProperty:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorProperty:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setInput(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInput([Ljava/io/File;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/File;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    goto :goto_0
.end method

.method public declared-synchronized setInput([Ljava/io/File;)V
    .locals 1
    .param p1    # [Ljava/io/File;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->input:[Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setInputEncoding(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "inputEncoding must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputEncoding:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setInputFilterChains(Ljava/util/Vector;)V
    .locals 1
    .param p1    # Ljava/util/Vector;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputFilterChains:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method setInputStream(Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputStream:Ljava/io/InputStream;

    return-void
.end method

.method public declared-synchronized setInputString(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->inputString:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setLogError(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->logError:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setLogInputString(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->logInputString:Z

    return-void
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutput([Ljava/io/File;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/File;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    goto :goto_0
.end method

.method public declared-synchronized setOutput([Ljava/io/File;)V
    .locals 1
    .param p1    # [Ljava/io/File;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->out:[Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setOutputEncoding(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "outputEncoding must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputEncoding:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setOutputFilterChains(Ljava/util/Vector;)V
    .locals 1
    .param p1    # Ljava/util/Vector;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputFilterChains:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setOutputProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputProperty:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->outputProperty:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setProperties()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->baos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    :try_start_3
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Redirector;->errorBaos:Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Redirector$PropertyOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
