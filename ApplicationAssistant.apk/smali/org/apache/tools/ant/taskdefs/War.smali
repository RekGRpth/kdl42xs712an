.class public Lorg/apache/tools/ant/taskdefs/War;
.super Lorg/apache/tools/ant/taskdefs/Jar;
.source "War.java"


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field private static final XML_DESCRIPTOR_PATH:Ljava/lang/String; = "WEB-INF/web.xml"

.field private static final XML_DESCRIPTOR_PATH_LC:Ljava/lang/String;


# instance fields
.field private addedWebXmlFile:Ljava/io/File;

.field private deploymentDescriptor:Ljava/io/File;

.field private needxmlfile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/War;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v0, "WEB-INF/web.xml"

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/War;->XML_DESCRIPTOR_PATH_LC:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Jar;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/War;->needxmlfile:Z

    const-string v0, "war"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/War;->archiveType:Ljava/lang/String;

    const-string v0, "create"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/War;->emptyBehavior:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addClasses(Lorg/apache/tools/ant/types/ZipFileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ZipFileSet;

    const-string v0, "WEB-INF/classes/"

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/ZipFileSet;->setPrefix(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->addFileset(Lorg/apache/tools/ant/types/FileSet;)V

    return-void
.end method

.method public addLib(Lorg/apache/tools/ant/types/ZipFileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ZipFileSet;

    const-string v0, "WEB-INF/lib/"

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/ZipFileSet;->setPrefix(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->addFileset(Lorg/apache/tools/ant/types/FileSet;)V

    return-void
.end method

.method public addWebinf(Lorg/apache/tools/ant/types/ZipFileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ZipFileSet;

    const-string v0, "WEB-INF/"

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/ZipFileSet;->setPrefix(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->addFileset(Lorg/apache/tools/ant/types/FileSet;)V

    return-void
.end method

.method protected cleanUp()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/War;->addedWebXmlFile:Ljava/io/File;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/War;->deploymentDescriptor:Ljava/io/File;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/War;->needxmlfile:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/War;->isInUpdateMode()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "No WEB-INF/web.xml file was added.\nIf this is your intent, set needxml=\'false\' "

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/War;->addedWebXmlFile:Ljava/io/File;

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->cleanUp()V

    return-void
.end method

.method protected initZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 0
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->initZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V

    return-void
.end method

.method public setNeedxmlfile(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/War;->needxmlfile:Z

    return-void
.end method

.method public setWarfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/War;->setDestFile(Ljava/io/File;)V

    return-void
.end method

.method public setWebxml(Ljava/io/File;)V
    .locals 4
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/War;->deploymentDescriptor:Ljava/io/File;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/War;->deploymentDescriptor:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Deployment descriptor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/War;->deploymentDescriptor:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " does not exist."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/ZipFileSet;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/War;->deploymentDescriptor:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipFileSet;->setFile(Ljava/io/File;)V

    const-string v1, "WEB-INF/web.xml"

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipFileSet;->setFullpath(Ljava/lang/String;)V

    invoke-super {p0, v0}, Lorg/apache/tools/ant/taskdefs/Jar;->addFileset(Lorg/apache/tools/ant/types/FileSet;)V

    return-void
.end method

.method protected zipFile(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p3, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    sget-object v2, Lorg/apache/tools/ant/taskdefs/War;->XML_DESCRIPTOR_PATH_LC:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/War;->addedWebXmlFile:Ljava/io/File;

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    sget-object v2, Lorg/apache/tools/ant/taskdefs/War;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/War;->addedWebXmlFile:Ljava/io/File;

    invoke-virtual {v2, v3, p1}, Lorg/apache/tools/ant/util/FileUtils;->fileNameEquals(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Warning: selected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/War;->archiveType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " files include a second "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "WEB-INF/web.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " which will be ignored.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "The duplicate entry is at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "The file that will be used is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/War;->addedWebXmlFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/War;->log(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/tools/ant/taskdefs/Jar;->zipFile(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I)V

    :cond_1
    return-void

    :cond_2
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/War;->addedWebXmlFile:Ljava/io/File;

    const/4 v0, 0x1

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/War;->deploymentDescriptor:Ljava/io/File;

    goto :goto_0
.end method
