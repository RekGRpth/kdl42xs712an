.class public Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;
.super Ljava/lang/Object;
.source "Replace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Replace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Replacefilter"
.end annotation


# instance fields
.field private inputBuffer:Ljava/lang/StringBuffer;

.field private outputBuffer:Ljava/lang/StringBuffer;

.field private property:Ljava/lang/String;

.field private replaceValue:Ljava/lang/String;

.field private final this$0:Lorg/apache/tools/ant/taskdefs/Replace;

.field private token:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/taskdefs/Replace;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->outputBuffer:Ljava/lang/StringBuffer;

    return-void
.end method

.method private replace()I
    .locals 5

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->replaceValue:Ljava/lang/String;

    invoke-virtual {v2, v0, v3, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->replaceValue:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int v1, v0, v2

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Replace;->access$304(Lorg/apache/tools/ant/taskdefs/Replace;)I

    goto :goto_0

    :cond_0
    return v1
.end method


# virtual methods
.method flush()V
    .locals 3

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->replace()I

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->outputBuffer:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    return-void
.end method

.method getOutputBuffer()Ljava/lang/StringBuffer;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->outputBuffer:Ljava/lang/StringBuffer;

    return-object v0
.end method

.method public getProperty()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    return-object v0
.end method

.method public getReplaceValue()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Replace;->access$100(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/util/Properties;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->value:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->value:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Replace;->access$200(Lorg/apache/tools/ant/taskdefs/Replace;)Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Replace;->access$200(Lorg/apache/tools/ant/taskdefs/Replace;)Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->value:Ljava/lang/String;

    return-object v0
.end method

.method process()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->replace()I

    move-result v0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->outputBuffer:Ljava/lang/StringBuffer;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v1, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v1, v0}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method setInputBuffer(Ljava/lang/StringBuffer;)V
    .locals 0
    .param p1    # Ljava/lang/StringBuffer;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->inputBuffer:Ljava/lang/StringBuffer;

    return-void
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->value:Ljava/lang/String;

    return-void
.end method

.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v0, "token is a mandatory attribute of replacefilter."

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const-string v1, ""

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "The token attribute must not be an empty string."

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->value:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v0, "Either value or property can be specified, but a replacefilter element cannot have both."

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Replace;->access$000(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v0, "The replacefilter\'s property attribute can only be used with the replacetask\'s propertyFile attribute."

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Replace;->access$100(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/util/Properties;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Replace;->access$100(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/util/Properties;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    :cond_4
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "property \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->property:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\" was not found in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->this$0:Lorg/apache/tools/ant/taskdefs/Replace;

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Replace;->access$000(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->getReplaceValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->replaceValue:Ljava/lang/String;

    return-void
.end method
