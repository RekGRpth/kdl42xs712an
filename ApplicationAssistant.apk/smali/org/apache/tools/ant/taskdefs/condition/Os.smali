.class public Lorg/apache/tools/ant/taskdefs/condition/Os;
.super Ljava/lang/Object;
.source "Os.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/condition/Condition;


# static fields
.field public static final FAMILY_9X:Ljava/lang/String; = "win9x"

.field public static final FAMILY_DOS:Ljava/lang/String; = "dos"

.field public static final FAMILY_MAC:Ljava/lang/String; = "mac"

.field public static final FAMILY_NETWARE:Ljava/lang/String; = "netware"

.field public static final FAMILY_NT:Ljava/lang/String; = "winnt"

.field public static final FAMILY_OS2:Ljava/lang/String; = "os/2"

.field public static final FAMILY_OS400:Ljava/lang/String; = "os/400"

.field public static final FAMILY_TANDEM:Ljava/lang/String; = "tandem"

.field public static final FAMILY_UNIX:Ljava/lang/String; = "unix"

.field public static final FAMILY_VMS:Ljava/lang/String; = "openvms"

.field public static final FAMILY_WINDOWS:Ljava/lang/String; = "windows"

.field public static final FAMILY_ZOS:Ljava/lang/String; = "z/os"

.field private static final OS_ARCH:Ljava/lang/String;

.field private static final OS_NAME:Ljava/lang/String;

.field private static final OS_VERSION:Ljava/lang/String;

.field private static final PATH_SEP:Ljava/lang/String;


# instance fields
.field private arch:Ljava/lang/String;

.field private family:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "os.name"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v0, "os.arch"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_ARCH:Ljava/lang/String;

    const-string v0, "os.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_VERSION:Ljava/lang/String;

    const-string v0, "path.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/condition/Os;->PATH_SEP:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->setFamily(Ljava/lang/String;)V

    return-void
.end method

.method public static isArch(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v0, v0, p0, v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isOs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isFamily(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p0, v0, v0, v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isOs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isName(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v0, p0, v0, v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isOs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isOs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v7, 0x0

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-eqz p3, :cond_7

    :cond_0
    const/4 v2, 0x1

    const/4 v4, 0x1

    const/4 v1, 0x1

    const/4 v5, 0x1

    if-eqz p0, :cond_3

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "windows"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_8

    const/4 v6, 0x1

    :goto_0
    const/4 v0, 0x0

    const/4 v3, 0x0

    if-eqz v6, :cond_2

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "95"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-gez v8, :cond_1

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "98"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-gez v8, :cond_1

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "me"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-gez v8, :cond_1

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "ce"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-ltz v8, :cond_9

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_a

    const/4 v3, 0x1

    :cond_2
    :goto_2
    const-string v8, "windows"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    move v2, v6

    :cond_3
    :goto_3
    if-eqz p1, :cond_4

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    :cond_4
    if-eqz p2, :cond_5

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_ARCH:Ljava/lang/String;

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_5
    if-eqz p3, :cond_6

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_VERSION:Ljava/lang/String;

    invoke-virtual {p3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    :cond_6
    if-eqz v2, :cond_24

    if-eqz v4, :cond_24

    if-eqz v1, :cond_24

    if-eqz v5, :cond_24

    const/4 v7, 0x1

    :cond_7
    :goto_4
    return v7

    :cond_8
    const/4 v6, 0x0

    goto :goto_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_1

    :cond_a
    const/4 v3, 0x0

    goto :goto_2

    :cond_b
    const-string v8, "win9x"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    if-eqz v6, :cond_c

    if-eqz v0, :cond_c

    const/4 v2, 0x1

    :goto_5
    goto :goto_3

    :cond_c
    const/4 v2, 0x0

    goto :goto_5

    :cond_d
    const-string v8, "winnt"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    if-eqz v6, :cond_e

    if-eqz v3, :cond_e

    const/4 v2, 0x1

    :goto_6
    goto :goto_3

    :cond_e
    const/4 v2, 0x0

    goto :goto_6

    :cond_f
    const-string v8, "os/2"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "os/2"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_10

    const/4 v2, 0x1

    :goto_7
    goto :goto_3

    :cond_10
    const/4 v2, 0x0

    goto :goto_7

    :cond_11
    const-string v8, "netware"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_13

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "netware"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_12

    const/4 v2, 0x1

    :goto_8
    goto :goto_3

    :cond_12
    const/4 v2, 0x0

    goto :goto_8

    :cond_13
    const-string v8, "dos"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_15

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->PATH_SEP:Ljava/lang/String;

    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_14

    const-string v8, "netware"

    invoke-static {v8}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_14

    const/4 v2, 0x1

    :goto_9
    goto/16 :goto_3

    :cond_14
    const/4 v2, 0x0

    goto :goto_9

    :cond_15
    const-string v8, "mac"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_17

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "mac"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_16

    const/4 v2, 0x1

    :goto_a
    goto/16 :goto_3

    :cond_16
    const/4 v2, 0x0

    goto :goto_a

    :cond_17
    const-string v8, "tandem"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_19

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "nonstop_kernel"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_18

    const/4 v2, 0x1

    :goto_b
    goto/16 :goto_3

    :cond_18
    const/4 v2, 0x0

    goto :goto_b

    :cond_19
    const-string v8, "unix"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1c

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->PATH_SEP:Ljava/lang/String;

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1b

    const-string v8, "openvms"

    invoke-static {v8}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1b

    const-string v8, "mac"

    invoke-static {v8}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1a

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1b

    :cond_1a
    const/4 v2, 0x1

    :goto_c
    goto/16 :goto_3

    :cond_1b
    const/4 v2, 0x0

    goto :goto_c

    :cond_1c
    const-string v8, "z/os"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1f

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "z/os"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-gt v8, v9, :cond_1d

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "os/390"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_1e

    :cond_1d
    const/4 v2, 0x1

    :goto_d
    goto/16 :goto_3

    :cond_1e
    const/4 v2, 0x0

    goto :goto_d

    :cond_1f
    const-string v8, "os/400"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_21

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "os/400"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_20

    const/4 v2, 0x1

    :goto_e
    goto/16 :goto_3

    :cond_20
    const/4 v2, 0x0

    goto :goto_e

    :cond_21
    const-string v8, "openvms"

    invoke-virtual {p0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_23

    sget-object v8, Lorg/apache/tools/ant/taskdefs/condition/Os;->OS_NAME:Ljava/lang/String;

    const-string v9, "openvms"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-le v8, v9, :cond_22

    const/4 v2, 0x1

    :goto_f
    goto/16 :goto_3

    :cond_22
    const/4 v2, 0x0

    goto :goto_f

    :cond_23
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Don\'t know how to detect os family \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_24
    const/4 v7, 0x0

    goto/16 :goto_4
.end method

.method public static isVersion(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v0, v0, v0, p0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isOs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public eval()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->family:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->name:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->arch:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->version:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isOs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setArch(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->arch:Ljava/lang/String;

    return-void
.end method

.method public setFamily(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->family:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->name:Ljava/lang/String;

    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/Os;->version:Ljava/lang/String;

    return-void
.end method
