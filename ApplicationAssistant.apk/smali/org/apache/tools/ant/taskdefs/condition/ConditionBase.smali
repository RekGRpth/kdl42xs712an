.class public abstract Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "ConditionBase.java"

# interfaces
.implements Lorg/apache/tools/ant/DynamicElement;


# static fields
.field private static final CONDITION_ANTLIB:Ljava/lang/String; = "antlib:org.apache.tools.ant.types.conditions:"


# instance fields
.field private conditions:Ljava/util/Vector;

.field private taskName:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    const-string v0, "condition"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->taskName:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    const-string v0, "component"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->taskName:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    const-string v0, "condition"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->taskName:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->taskName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/taskdefs/condition/Condition;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Condition;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addAnd(Lorg/apache/tools/ant/taskdefs/condition/And;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/And;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addAvailable(Lorg/apache/tools/ant/taskdefs/Available;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Available;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addChecksum(Lorg/apache/tools/ant/taskdefs/Checksum;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Checksum;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addContains(Lorg/apache/tools/ant/taskdefs/condition/Contains;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Contains;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addEquals(Lorg/apache/tools/ant/taskdefs/condition/Equals;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Equals;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addFilesMatch(Lorg/apache/tools/ant/taskdefs/condition/FilesMatch;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/FilesMatch;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addHttp(Lorg/apache/tools/ant/taskdefs/condition/Http;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Http;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addIsFalse(Lorg/apache/tools/ant/taskdefs/condition/IsFalse;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/IsFalse;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addIsFileSelected(Lorg/apache/tools/ant/taskdefs/condition/IsFileSelected;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/IsFileSelected;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addIsReference(Lorg/apache/tools/ant/taskdefs/condition/IsReference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/IsReference;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addIsSet(Lorg/apache/tools/ant/taskdefs/condition/IsSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/IsSet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addIsTrue(Lorg/apache/tools/ant/taskdefs/condition/IsTrue;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/IsTrue;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addNot(Lorg/apache/tools/ant/taskdefs/condition/Not;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Not;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addOr(Lorg/apache/tools/ant/taskdefs/condition/Or;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Or;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addOs(Lorg/apache/tools/ant/taskdefs/condition/Os;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Os;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addSocket(Lorg/apache/tools/ant/taskdefs/condition/Socket;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/condition/Socket;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addUptodate(Lorg/apache/tools/ant/taskdefs/UpToDate;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/UpToDate;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method protected countConditions()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public createDynamicElement(Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "antlib:org.apache.tools.ant.types.conditions:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/ComponentHelper;->createComponent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/tools/ant/taskdefs/condition/Condition;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Dynamically discovered \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->log(Ljava/lang/String;I)V

    move-object v1, v0

    check-cast v1, Lorg/apache/tools/ant/taskdefs/condition/Condition;

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->add(Lorg/apache/tools/ant/taskdefs/condition/Condition;)V

    goto :goto_0
.end method

.method protected final getConditions()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->conditions:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->taskName:Ljava/lang/String;

    return-object v0
.end method

.method public setTaskName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/ConditionBase;->taskName:Ljava/lang/String;

    return-void
.end method
