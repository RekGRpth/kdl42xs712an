.class public Lorg/apache/tools/ant/taskdefs/condition/AntVersion;
.super Ljava/lang/Object;
.source "AntVersion.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/condition/Condition;


# instance fields
.field private atLeast:Ljava/lang/String;

.field private exactly:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    return-void
.end method

.method private getVersion()Lorg/apache/tools/ant/util/DeweyDecimal;
    .locals 7

    new-instance v2, Lorg/apache/tools/ant/Project;

    invoke-direct {v2}, Lorg/apache/tools/ant/Project;-><init>()V

    invoke-virtual {v2}, Lorg/apache/tools/ant/Project;->init()V

    const-string v5, "ant.version"

    invoke-virtual {v2, v5}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v5, v4

    if-ge v1, v5, :cond_2

    aget-char v5, v4, v1

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_0

    aget-char v5, v4, v1

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v0, 0x1

    :cond_0
    aget-char v5, v4, v1

    const/16 v6, 0x2e

    if-ne v5, v6, :cond_1

    if-eqz v0, :cond_1

    aget-char v5, v4, v1

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    aget-char v5, v4, v1

    invoke-static {v5}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v0, :cond_3

    :cond_2
    new-instance v5, Lorg/apache/tools/ant/util/DeweyDecimal;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/tools/ant/util/DeweyDecimal;-><init>(Ljava/lang/String;)V

    return-object v5

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "Only one of atleast or exactly may be set."

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "One of atleast or exactly must be set."

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    if-eqz v1, :cond_2

    new-instance v1, Lorg/apache/tools/ant/util/DeweyDecimal;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/util/DeweyDecimal;-><init>(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    new-instance v1, Lorg/apache/tools/ant/util/DeweyDecimal;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/util/DeweyDecimal;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "The argument is not a Dewey Decimal eg 1.1.0"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public eval()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->validate()V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->getVersion()Lorg/apache/tools/ant/util/DeweyDecimal;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/util/DeweyDecimal;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/util/DeweyDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/DeweyDecimal;->isGreaterThanOrEqual(Lorg/apache/tools/ant/util/DeweyDecimal;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/util/DeweyDecimal;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/util/DeweyDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/DeweyDecimal;->isEqual(Lorg/apache/tools/ant/util/DeweyDecimal;)Z

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAtLeast()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    return-object v0
.end method

.method public getExactly()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    return-object v0
.end method

.method public setAtLeast(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->atLeast:Ljava/lang/String;

    return-void
.end method

.method public setExactly(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/condition/AntVersion;->exactly:Ljava/lang/String;

    return-void
.end method
