.class public Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;
.super Lorg/apache/tools/ant/taskdefs/Copy;
.source "Sync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/taskdefs/Sync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyCopy"
.end annotation


# instance fields
.field private nonOrphans:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Copy;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->nonOrphans:Ljava/util/Set;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;)Ljava/util/Set;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->nonOrphans:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public getIncludeEmptyDirs()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->includeEmpty:Z

    return v0
.end method

.method public getToDir()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->destDir:Ljava/io/File;

    return-object v0
.end method

.method protected scan([Lorg/apache/tools/ant/types/Resource;Ljava/io/File;)Ljava/util/Map;
    .locals 4
    .param p1    # [Lorg/apache/tools/ant/types/Resource;
    .param p2    # Ljava/io/File;

    const-string v3, "No mapper"

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v2}, Lorg/apache/tools/ant/taskdefs/Sync;->access$100(Ljava/lang/String;Z)V

    invoke-super {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/Copy;->scan([Lorg/apache/tools/ant/types/Resource;Ljava/io/File;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->nonOrphans:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method protected scan(Ljava/io/File;Ljava/io/File;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .param p3    # [Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const-string v2, "No mapper"

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v1}, Lorg/apache/tools/ant/taskdefs/Sync;->access$100(Ljava/lang/String;Z)V

    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/tools/ant/taskdefs/Copy;->scan(Ljava/io/File;Ljava/io/File;[Ljava/lang/String;[Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_1
    array-length v1, p3

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->nonOrphans:Ljava/util/Set;

    aget-object v2, p3, v0

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_2
    array-length v1, p4

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->nonOrphans:Ljava/util/Set;

    aget-object v2, p4, v0

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method protected supportsNonFileResources()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
