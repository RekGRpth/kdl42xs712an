.class public Lorg/apache/tools/ant/taskdefs/RecorderEntry;
.super Ljava/lang/Object;
.source "RecorderEntry.java"

# interfaces
.implements Lorg/apache/tools/ant/BuildLogger;
.implements Lorg/apache/tools/ant/SubBuildListener;


# instance fields
.field private emacsMode:Z

.field private filename:Ljava/lang/String;

.field private loglevel:I

.field private out:Ljava/io/PrintStream;

.field private project:Lorg/apache/tools/ant/Project;

.field private record:Z

.field private targetStartTime:J


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->filename:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->record:Z

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->loglevel:I

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->targetStartTime:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->emacsMode:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->targetStartTime:J

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->filename:Ljava/lang/String;

    return-void
.end method

.method private flush()V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->record:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->flush()V

    :cond_0
    return-void
.end method

.method private static formatTime(J)Ljava/lang/String;
    .locals 11
    .param p0    # J

    const-wide/16 v9, 0x1

    const-wide/16 v7, 0x3c

    const-wide/16 v4, 0x3e8

    div-long v2, p0, v4

    div-long v0, v2, v7

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_2

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " minute"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    cmp-long v4, v0, v9

    if-nez v4, :cond_0

    const-string v4, " "

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    rem-long v5, v2, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " second"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    rem-long v6, v2, v7

    cmp-long v4, v6, v9

    if-nez v4, :cond_1

    const-string v4, ""

    :goto_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_2
    return-object v4

    :cond_0
    const-string v4, "s "

    goto :goto_0

    :cond_1
    const-string v4, "s"

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " second"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    rem-long v6, v2, v7

    cmp-long v4, v6, v9

    if-nez v4, :cond_3

    const-string v4, ""

    :goto_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_3
    const-string v4, "s"

    goto :goto_3
.end method

.method private log(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->record:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->loglevel:I

    if-gt p2, v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private openFileImpl(Z)V
    .locals 4
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    if-nez v1, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/PrintStream;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->filename:Ljava/lang/String;

    invoke-direct {v2, v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v1, v2}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "Problems opening file using a recorder entry"

    invoke-direct {v1, v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public buildFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    const-string v1, "< BUILD FINISHED"

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->record:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getException()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "BUILD SUCCESSFUL"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->cleanup()V

    return-void

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "BUILD FAILED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method

.method public buildStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    const-string v0, "> BUILD STARTED"

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    return-void
.end method

.method public cleanup()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->closeFile()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->project:Lorg/apache/tools/ant/Project;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/Project;->removeBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->project:Lorg/apache/tools/ant/Project;

    return-void
.end method

.method closeFile()V
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    :cond_0
    return-void
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->filename:Ljava/lang/String;

    return-object v0
.end method

.method public messageLogged(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 7
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    const-string v5, "--- MESSAGE LOGGED"

    const/4 v6, 0x4

    invoke-direct {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/Task;->getTaskName()Ljava/lang/String;

    move-result-object v3

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->emacsMode:Z

    if-nez v5, :cond_1

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    rsub-int/lit8 v4, v5, 0xc

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getPriority()I

    move-result v6

    invoke-direct {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    return-void
.end method

.method openFile(Z)V
    .locals 0
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->openFileImpl(Z)V

    return-void
.end method

.method reopenFile()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->openFileImpl(Z)V

    return-void
.end method

.method public setEmacsMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->emacsMode:Z

    return-void
.end method

.method public setErrorPrintStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1    # Ljava/io/PrintStream;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->setOutputPrintStream(Ljava/io/PrintStream;)V

    return-void
.end method

.method public setMessageOutputLevel(I)V
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->loglevel:I

    :cond_0
    return-void
.end method

.method public setOutputPrintStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1    # Ljava/io/PrintStream;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->closeFile()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->out:Ljava/io/PrintStream;

    return-void
.end method

.method public setProject(Lorg/apache/tools/ant/Project;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->project:Lorg/apache/tools/ant/Project;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lorg/apache/tools/ant/Project;->addBuildListener(Lorg/apache/tools/ant/BuildListener;)V

    :cond_0
    return-void
.end method

.method public setRecordState(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->flush()V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->record:Z

    :cond_0
    return-void
.end method

.method public subBuildFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->project:Lorg/apache/tools/ant/Project;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->cleanup()V

    :cond_0
    return-void
.end method

.method public subBuildStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public targetFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "<< TARGET FINISHED -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->targetStartTime:J

    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->formatTime(J)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ":  duration "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->flush()V

    return-void
.end method

.method public targetStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ">> TARGET STARTED -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v1, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->targetStartTime:J

    return-void
.end method

.method public taskFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "<<< TASK FINISHED -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->flush()V

    return-void
.end method

.method public taskStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ">>> TASK STARTED -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/RecorderEntry;->log(Ljava/lang/String;I)V

    return-void
.end method
