.class public Lorg/apache/tools/ant/taskdefs/Rmic;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "Rmic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Rmic$ImplementationSpecificArgument;
    }
.end annotation


# static fields
.field public static final ERROR_BASE_NOT_SET:Ljava/lang/String; = "base attribute must be set!"

.field public static final ERROR_LOADING_CAUSED_EXCEPTION:Ljava/lang/String; = ". Loading caused Exception: "

.field public static final ERROR_NOT_A_DIR:Ljava/lang/String; = "base is not a directory:"

.field public static final ERROR_NOT_DEFINED:Ljava/lang/String; = ". It is not defined."

.field public static final ERROR_NOT_FOUND:Ljava/lang/String; = ". It could not be found."

.field public static final ERROR_NO_BASE_EXISTS:Ljava/lang/String; = "base does not exist: "

.field public static final ERROR_RMIC_FAILED:Ljava/lang/String; = "Rmic failed; see the compiler error output for details."

.field public static final ERROR_UNABLE_TO_VERIFY_CLASS:Ljava/lang/String; = "Unable to verify class "

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field static class$java$rmi$Remote:Ljava/lang/Class;


# instance fields
.field private baseDir:Ljava/io/File;

.field private classname:Ljava/lang/String;

.field private compileClasspath:Lorg/apache/tools/ant/types/Path;

.field private compileList:Ljava/util/Vector;

.field private debug:Z

.field private extDirs:Lorg/apache/tools/ant/types/Path;

.field private facade:Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

.field private filtering:Z

.field private idl:Z

.field private idlOpts:Ljava/lang/String;

.field private iiop:Z

.field private iiopOpts:Ljava/lang/String;

.field private includeAntRuntime:Z

.field private includeJavaRuntime:Z

.field private loader:Ljava/lang/ClassLoader;

.field private sourceBase:Ljava/io/File;

.field private stubVersion:Ljava/lang/String;

.field private verify:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Rmic;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->verify:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->filtering:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiop:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idl:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->debug:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->includeAntRuntime:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->includeJavaRuntime:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->loader:Ljava/lang/ClassLoader;

    new-instance v0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

    const-string v1, "default"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->facade:Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private isValidRmiRemote(Ljava/lang/Class;)Z
    .locals 1
    .param p1    # Ljava/lang/Class;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Rmic;->getRemoteInterface(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private moveGeneratedFile(Ljava/io/File;Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;)V
    .locals 14
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v12, 0x2e

    sget-char v13, Ljava/io/File;->separatorChar:C

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, ".class"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface/range {p4 .. p4}, Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;->getMapper()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v11

    invoke-interface {v11, v1}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    :goto_0
    array-length v11, v3

    if-ge v4, v11, :cond_3

    aget-object v2, v3, v4

    const-string v11, ".class"

    invoke-virtual {v2, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v11

    const-string v12, ".class"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    sub-int v9, v11, v12

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v12, 0x0

    invoke-virtual {v2, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, ".java"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v7, v0, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v11, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->filtering:Z

    if-eqz v11, :cond_2

    sget-object v11, Lorg/apache/tools/ant/taskdefs/Rmic;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    new-instance v12, Lorg/apache/tools/ant/types/FilterSetCollection;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/tools/ant/Project;->getGlobalFilterSet()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/apache/tools/ant/types/FilterSetCollection;-><init>(Lorg/apache/tools/ant/types/FilterSet;)V

    invoke-virtual {v11, v8, v7, v12}, Lorg/apache/tools/ant/util/FileUtils;->copyFile(Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/types/FilterSetCollection;)V

    :goto_2
    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v5

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Failed to copy "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " due to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v12

    invoke-direct {v11, v6, v5, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v11

    :cond_2
    :try_start_1
    sget-object v11, Lorg/apache/tools/ant/taskdefs/Rmic;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v11, v8, v7}, Lorg/apache/tools/ant/util/FileUtils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_3
    return-void
.end method


# virtual methods
.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createCompilerArg()Lorg/apache/tools/ant/taskdefs/Rmic$ImplementationSpecificArgument;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Rmic$ImplementationSpecificArgument;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Rmic$ImplementationSpecificArgument;-><init>(Lorg/apache/tools/ant/taskdefs/Rmic;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->facade:Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->addImplementationArgument(Lorg/apache/tools/ant/util/facade/ImplementationSpecificArgument;)V

    return-object v0
.end method

.method public createExtdirs()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->extDirs:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->extDirs:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->extDirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public execute()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v13, 0x1

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    if-nez v6, :cond_0

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "base attribute must be set!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v6

    :cond_0
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "base does not exist: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v6

    :cond_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_2

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "base is not a directory:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v6

    :cond_2
    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->verify:Z

    if-eqz v6, :cond_3

    const-string v6, "Verify has been turned on."

    const/4 v7, 0x3

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getCompiler()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p0}, Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapterFactory;->getRmic(Ljava/lang/String;Lorg/apache/tools/ant/Task;)Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;

    move-result-object v0

    invoke-interface {v0, p0}, Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;->setRmic(Lorg/apache/tools/ant/taskdefs/Rmic;)V

    invoke-interface {v0}, Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-virtual {v6, v1}, Lorg/apache/tools/ant/Project;->createClassLoader(Lorg/apache/tools/ant/types/Path;)Lorg/apache/tools/ant/AntClassLoader;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->loader:Ljava/lang/ClassLoader;

    :try_start_0
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->classname:Ljava/lang/String;

    if-nez v6, :cond_4

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    invoke-virtual {p0, v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->getDirectoryScanner(Ljava/io/File;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    invoke-interface {v0}, Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;->getMapper()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v7

    invoke-virtual {p0, v6, v4, v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->scanDir(Ljava/io/File;[Ljava/lang/String;Lorg/apache/tools/ant/util/FileNameMapper;)V

    :goto_0
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_6

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "RMI Compiling "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " class"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    if-le v3, v13, :cond_5

    const-string v6, "es"

    :goto_1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    invoke-interface {v0}, Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;->execute()Z

    move-result v6

    if-nez v6, :cond_6

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Rmic failed; see the compiler error output for details."

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->removeAllElements()V

    throw v6

    :cond_4
    :try_start_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->classname:Ljava/lang/String;

    const/16 v11, 0x2e

    sget-char v12, Ljava/io/File;->separatorChar:C

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, ".class"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-interface {v0}, Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;->getMapper()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v8

    invoke-virtual {p0, v6, v7, v8}, Lorg/apache/tools/ant/taskdefs/Rmic;->scanDir(Ljava/io/File;[Ljava/lang/String;Lorg/apache/tools/ant/util/FileNameMapper;)V

    goto :goto_0

    :cond_5
    const-string v6, ""

    goto :goto_1

    :cond_6
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->sourceBase:Ljava/io/File;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->sourceBase:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    if-lez v3, :cond_7

    iget-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idl:Z

    if-eqz v6, :cond_8

    const-string v6, "Cannot determine sourcefiles in idl mode, "

    const/4 v7, 0x1

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    const-string v6, "sourcebase attribute will be ignored."

    const/4 v7, 0x1

    invoke-virtual {p0, v6, v7}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_7
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->removeAllElements()V

    return-void

    :cond_8
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v3, :cond_7

    :try_start_2
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->sourceBase:Ljava/io/File;

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    invoke-virtual {v6, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {p0, v7, v8, v6, v0}, Lorg/apache/tools/ant/taskdefs/Rmic;->moveGeneratedFile(Ljava/io/File;Ljava/io/File;Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/rmic/RmicAdapter;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public getBase()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    return-object v0
.end method

.method public getClassname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->classname:Ljava/lang/String;

    return-object v0
.end method

.method public getClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    return-object v0
.end method

.method public getCompileList()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    return-object v0
.end method

.method public getCompiler()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->facade:Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    const-string v2, "build.rmic"

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->setMagicValue(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->facade:Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->getImplementation()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentCompilerArgs()[Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->getCompiler()Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->facade:Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->getArgs()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDebug()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->debug:Z

    return v0
.end method

.method public getExtdirs()Lorg/apache/tools/ant/types/Path;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->extDirs:Lorg/apache/tools/ant/types/Path;

    return-object v0
.end method

.method public getFileList()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    return-object v0
.end method

.method public getFiltering()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->filtering:Z

    return v0
.end method

.method public getIdl()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idl:Z

    return v0
.end method

.method public getIdlopts()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idlOpts:Ljava/lang/String;

    return-object v0
.end method

.method public getIiop()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiop:Z

    return v0
.end method

.method public getIiopopts()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiopOpts:Ljava/lang/String;

    return-object v0
.end method

.method public getIncludeantruntime()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->includeAntRuntime:Z

    return v0
.end method

.method public getIncludejavaruntime()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->includeJavaRuntime:Z

    return v0
.end method

.method public getLoader()Ljava/lang/ClassLoader;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->loader:Ljava/lang/ClassLoader;

    return-object v0
.end method

.method public getRemoteInterface(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 4
    .param p1    # Ljava/lang/Class;

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Rmic;->class$java$rmi$Remote:Ljava/lang/Class;

    if-nez v2, :cond_0

    const-string v2, "java.rmi.Remote"

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/taskdefs/Rmic;->class$java$rmi$Remote:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_3

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Rmic;->class$java$rmi$Remote:Ljava/lang/Class;

    if-nez v2, :cond_1

    const-string v2, "java.rmi.Remote"

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/taskdefs/Rmic;->class$java$rmi$Remote:Ljava/lang/Class;

    :goto_2
    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2

    aget-object v2, v1, v0

    :goto_3
    return-object v2

    :cond_0
    sget-object v2, Lorg/apache/tools/ant/taskdefs/Rmic;->class$java$rmi$Remote:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v2, Lorg/apache/tools/ant/taskdefs/Rmic;->class$java$rmi$Remote:Ljava/lang/Class;

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public getSourceBase()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->sourceBase:Ljava/io/File;

    return-object v0
.end method

.method public getStubVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->stubVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getVerify()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->verify:Z

    return v0
.end method

.method public isValidRmiRemote(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v6, 0x1

    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->loader:Ljava/lang/ClassLoader;

    invoke-virtual {v4, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->isInterface()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiop:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idl:Z

    if-nez v4, :cond_0

    :goto_0
    return v3

    :cond_0
    invoke-direct {p0, v2}, Lorg/apache/tools/ant/taskdefs/Rmic;->isValidRmiRemote(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result v3

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unable to verify class "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ". It could not be found."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unable to verify class "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ". It is not defined."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    goto :goto_0

    :catch_2
    move-exception v1

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unable to verify class "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ". Loading caused Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected scanDir(Ljava/io/File;[Ljava/lang/String;Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # [Ljava/lang/String;
    .param p3    # Lorg/apache/tools/ant/util/FileNameMapper;

    const/4 v6, 0x3

    move-object v2, p2

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idl:Z

    if-eqz v4, :cond_0

    const-string v4, "will leave uptodate test to rmic implementation in idl mode."

    invoke-virtual {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_2

    aget-object v4, v2, v0

    sget-char v5, Ljava/io/File;->separatorChar:C

    const/16 v6, 0x2e

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    const-string v5, ".class"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileList:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiop:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiopOpts:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiopOpts:Ljava/lang/String;

    const-string v5, "-always"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-le v4, v5, :cond_1

    const-string v4, "no uptodate test as -always option has been specified"

    invoke-virtual {p0, v4, v6}, Lorg/apache/tools/ant/taskdefs/Rmic;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    new-instance v3, Lorg/apache/tools/ant/util/SourceFileScanner;

    invoke-direct {v3, p0}, Lorg/apache/tools/ant/util/SourceFileScanner;-><init>(Lorg/apache/tools/ant/Task;)V

    invoke-virtual {v3, p2, p1, p1, p3}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)[Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setBase(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->baseDir:Ljava/io/File;

    return-void
.end method

.method public setClassname(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->classname:Ljava/lang/String;

    return-void
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->compileClasspath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Rmic;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setCompiler(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->facade:Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->setImplementation(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setDebug(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->debug:Z

    return-void
.end method

.method public setExtdirs(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->extDirs:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->extDirs:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->extDirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setFiltering(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->filtering:Z

    return-void
.end method

.method public setIdl(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idl:Z

    return-void
.end method

.method public setIdlopts(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->idlOpts:Ljava/lang/String;

    return-void
.end method

.method public setIiop(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiop:Z

    return-void
.end method

.method public setIiopopts(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->iiopOpts:Ljava/lang/String;

    return-void
.end method

.method public setIncludeantruntime(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->includeAntRuntime:Z

    return-void
.end method

.method public setIncludejavaruntime(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->includeJavaRuntime:Z

    return-void
.end method

.method public setSourceBase(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->sourceBase:Ljava/io/File;

    return-void
.end method

.method public setStubVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->stubVersion:Ljava/lang/String;

    return-void
.end method

.method public setVerify(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Rmic;->verify:Z

    return-void
.end method
