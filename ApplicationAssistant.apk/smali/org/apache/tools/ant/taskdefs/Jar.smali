.class public Lorg/apache/tools/ant/taskdefs/Jar;
.super Lorg/apache/tools/ant/taskdefs/Zip;
.source "Jar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;
    }
.end annotation


# static fields
.field private static final INDEX_NAME:Ljava/lang/String; = "META-INF/INDEX.LIST"

.field private static final JAR_MARKER:[Lorg/apache/tools/zip/ZipExtraField;

.field private static final MANIFEST_NAME:Ljava/lang/String; = "META-INF/MANIFEST.MF"


# instance fields
.field private configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

.field private createEmpty:Z

.field protected emptyBehavior:Ljava/lang/String;

.field private filesetManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

.field private filesetManifestConfig:Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

.field private index:Z

.field private indexJars:Lorg/apache/tools/ant/types/Path;

.field private manifest:Lorg/apache/tools/ant/taskdefs/Manifest;

.field private manifestEncoding:Ljava/lang/String;

.field private manifestFile:Ljava/io/File;

.field private mergeManifestsMain:Z

.field private originalManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

.field private rootEntries:Ljava/util/Vector;

.field private savedConfiguredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

.field private serviceList:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/tools/zip/ZipExtraField;

    const/4 v1, 0x0

    invoke-static {}, Lorg/apache/tools/zip/JarMarker;->getInstance()Lorg/apache/tools/zip/JarMarker;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Jar;->JAR_MARKER:[Lorg/apache/tools/zip/ZipExtraField;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Zip;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->serviceList:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->mergeManifestsMain:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->index:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->createEmpty:Z

    const-string v0, "create"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->emptyBehavior:Ljava/lang/String;

    const-string v0, "jar"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->archiveType:Ljava/lang/String;

    const-string v0, "create"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->emptyBehavior:Ljava/lang/String;

    const-string v0, "UTF8"

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Jar;->setEncoding(Ljava/lang/String;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->rootEntries:Ljava/util/Vector;

    return-void
.end method

.method private createIndexList(Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 25
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v12, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v12}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v24, Ljava/io/PrintWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    const-string v6, "UTF8"

    invoke-direct {v4, v12, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const-string v4, "JarIndex-Version: 1.0"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintWriter;->println()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/Jar;->zipFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/Jar;->addedDirs:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/Jar;->rootEntries:Ljava/util/Vector;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v6, v1}, Lorg/apache/tools/ant/taskdefs/Jar;->writeIndexLikeList(Ljava/util/List;Ljava/util/List;Ljava/io/PrintWriter;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintWriter;->println()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/Jar;->indexJars:Lorg/apache/tools/ant/types/Path;

    if-eqz v4, :cond_2

    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Jar;->createManifest()Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lorg/apache/tools/ant/taskdefs/Manifest;->getMainSection()Lorg/apache/tools/ant/taskdefs/Manifest$Section;

    move-result-object v4

    const-string v6, "Class-Path"

    invoke-virtual {v4, v6}, Lorg/apache/tools/ant/taskdefs/Manifest$Section;->getAttribute(Ljava/lang/String;)Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;

    move-result-object v15

    const/16 v16, 0x0

    if-eqz v15, :cond_0

    invoke-virtual {v15}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getValue()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    new-instance v23, Ljava/util/StringTokenizer;

    invoke-virtual {v15}, Lorg/apache/tools/ant/taskdefs/Manifest$Attribute;->getValue()Ljava/lang/String;

    move-result-object v4

    const-string v6, " "

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v4

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/4 v13, 0x0

    :goto_0
    invoke-virtual/range {v23 .. v23}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v14, v13, 0x1

    invoke-virtual/range {v23 .. v23}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v16, v13

    move v13, v14

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/Jar;->indexJars:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Path;->list()[Ljava/lang/String;

    move-result-object v20

    const/16 v19, 0x0

    :goto_1
    move-object/from16 v0, v20

    array-length v4, v0

    move/from16 v0, v19

    if-ge v0, v4, :cond_2

    aget-object v4, v20, v19

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lorg/apache/tools/ant/taskdefs/Jar;->findJarName(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_1

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    aget-object v4, v20, v19

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v4, v0, v1}, Lorg/apache/tools/ant/taskdefs/Jar;->grabFilesAndDirs(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/2addr v4, v6

    if-lez v4, :cond_1

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->writeIndexLikeList(Ljava/util/List;Ljava/util/List;Ljava/io/PrintWriter;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintWriter;->println()V

    :cond_1
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintWriter;->flush()V

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string v7, "META-INF/INDEX.LIST"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const/4 v10, 0x0

    const v11, 0x81a4

    move-object/from16 v4, p0

    move-object/from16 v6, p1

    invoke-super/range {v4 .. v11}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V

    return-void
.end method

.method private createManifest()Lorg/apache/tools/ant/taskdefs/Manifest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lorg/apache/tools/ant/taskdefs/Manifest;->getDefaultManifest()Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestFile:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestFile:Ljava/io/File;

    invoke-direct {p0, v2}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifest(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->isInUpdateMode()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->originalManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->mergeManifestsMain:Z

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v1, v4, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;Z)V
    :try_end_0
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :cond_2
    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Manifest is invalid: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/ManifestException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Invalid Manifest"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v2
.end method

.method private filesetManifest(Ljava/io/File;Ljava/io/InputStream;)V
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x3

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestFile:Ljava/io/File;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestFile:Ljava/io/File;

    invoke-virtual {v3, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Found manifest "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    if-eqz p2, :cond_2

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestEncoding:Ljava/lang/String;

    if-nez v3, :cond_1

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    :goto_0
    invoke-direct {p0, v1}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifest(Ljava/io/Reader;)Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestEncoding:Ljava/lang/String;

    invoke-direct {v1, p2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifest(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifest:Lorg/apache/tools/ant/taskdefs/Manifest;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unsupported encoding while reading manifest: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifestConfig:Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifestConfig:Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v4, "skip"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Found manifest to merge in file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    const/4 v2, 0x0

    if-eqz p2, :cond_5

    :try_start_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestEncoding:Ljava/lang/String;

    if-nez v3, :cond_4

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    :goto_2
    invoke-direct {p0, v1}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifest(Ljava/io/Reader;)Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v2

    :goto_3
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    if-nez v3, :cond_6

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifest:Lorg/apache/tools/ant/taskdefs/Manifest;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Unsupported encoding while reading manifest: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :cond_4
    :try_start_2
    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestEncoding:Ljava/lang/String;

    invoke-direct {v1, p2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifest(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v2

    goto :goto_3

    :cond_6
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Manifest in file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " is invalid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/ManifestException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    const-string v4, "Invalid Manifest"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v3, v4, v0, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v3
.end method

.method protected static final findJarName(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    sget-char v4, Ljava/io/File;->separatorChar:C

    const/16 v5, 0x2f

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    new-instance v2, Ljava/util/TreeMap;

    new-instance v4, Lorg/apache/tools/ant/taskdefs/Jar$1;

    invoke-direct {v4}, Lorg/apache/tools/ant/taskdefs/Jar$1;-><init>()V

    invoke-direct {v2, v4}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    const/4 v1, 0x0

    :goto_1
    array-length v4, p1

    if-ge v1, v4, :cond_4

    aget-object v4, p1, v1

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    aget-object v4, p1, v1

    aget-object v5, p1, v1

    invoke-virtual {v2, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    aget-object v4, p1, v1

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    aget-object v0, p1, v1

    :goto_3
    const/4 v4, -0x1

    if-le v3, v4, :cond_1

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    aget-object v4, p1, v1

    invoke-virtual {v2, v0, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    :cond_4
    invoke-virtual {v2}, Ljava/util/TreeMap;->size()I

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {v2}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_0
.end method

.method private getManifest(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Manifest;
    .locals 9
    .param p1    # Ljava/io/File;

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestEncoding:Ljava/lang/String;

    if-nez v6, :cond_1

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v3, v4

    :goto_0
    invoke-direct {p0, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifest(Ljava/io/Reader;)Lorg/apache/tools/ant/taskdefs/Manifest;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_1
    return-object v5

    :cond_1
    :try_start_3
    new-instance v4, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestEncoding:Ljava/lang/String;

    invoke-direct {v4, v2, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v3, v4

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_2
    :try_start_4
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Unsupported encoding while reading manifest: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v3, :cond_2

    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_4
    throw v6

    :catch_1
    move-exception v0

    :goto_5
    :try_start_6
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Unable to read manifest file: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception v6

    goto :goto_1

    :catch_3
    move-exception v7

    goto :goto_4

    :catchall_1
    move-exception v6

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_5

    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method private getManifest(Ljava/io/Reader;)Lorg/apache/tools/ant/taskdefs/Manifest;
    .locals 5
    .param p1    # Ljava/io/Reader;

    const/4 v1, 0x0

    :try_start_0
    new-instance v1, Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-direct {v1, p1}, Lorg/apache/tools/ant/taskdefs/Manifest;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/taskdefs/ManifestException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Manifest is invalid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/ManifestException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Invalid Manifest: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestFile:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v2

    :catch_1
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Unable to read manifest file ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private getManifestFromJar(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Manifest;
    .locals 7
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/util/zip/ZipFile;

    invoke-direct {v4, p1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/zip/ZipEntry;

    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "META-INF/MANIFEST.MF"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {v4, v2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-direct {v1, v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifest(Ljava/io/Reader;)Lorg/apache/tools/ant/taskdefs/Manifest;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    if-eqz v4, :cond_1

    :try_start_2
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_0
    return-object v5

    :cond_2
    const/4 v5, 0x0

    if-eqz v4, :cond_1

    :try_start_3
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    goto :goto_0

    :catchall_0
    move-exception v5

    :goto_1
    if-eqz v3, :cond_3

    :try_start_4
    invoke-virtual {v3}, Ljava/util/zip/ZipFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_3
    :goto_2
    throw v5

    :catch_1
    move-exception v6

    goto :goto_0

    :catch_2
    move-exception v6

    goto :goto_2

    :catchall_1
    move-exception v5

    move-object v3, v4

    goto :goto_1
.end method

.method protected static final grabFilesAndDirs(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/util/List;
    .param p2    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Lorg/apache/tools/zip/ZipFile;

    const-string v6, "utf-8"

    invoke-direct {v5, p0, v6}, Lorg/apache/tools/zip/ZipFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipFile;->getEntries()Ljava/util/Enumeration;

    move-result-object v1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/zip/ZipEntry;

    invoke-virtual {v3}, Lorg/apache/tools/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v6, "META-INF/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v3}, Lorg/apache/tools/zip/ZipEntry;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    move-object v4, v5

    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lorg/apache/tools/zip/ZipFile;->close()V

    :cond_1
    throw v6

    :cond_2
    :try_start_2
    const-string v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_3

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lorg/apache/tools/zip/ZipFile;->close()V

    :cond_5
    return-void

    :catchall_1
    move-exception v6

    goto :goto_1
.end method

.method private writeManifest(Lorg/apache/tools/zip/ZipOutputStream;Lorg/apache/tools/ant/taskdefs/Manifest;)V
    .locals 12
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p2    # Lorg/apache/tools/ant/taskdefs/Manifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p2}, Lorg/apache/tools/ant/taskdefs/Manifest;->getWarnings()Ljava/util/Enumeration;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Manifest warning: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-interface {v9}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const-string v3, "META-INF/"

    const/16 v4, 0x41ed

    sget-object v5, Lorg/apache/tools/ant/taskdefs/Jar;->JAR_MARKER:[Lorg/apache/tools/zip/ZipExtraField;

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/taskdefs/Jar;->zipDir(Ljava/io/File;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;I[Lorg/apache/tools/zip/ZipExtraField;)V

    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v10, Ljava/io/OutputStreamWriter;

    const-string v0, "UTF-8"

    invoke-direct {v10, v8, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    new-instance v11, Ljava/io/PrintWriter;

    invoke-direct {v11, v10}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p2, v11}, Lorg/apache/tools/ant/taskdefs/Manifest;->write(Ljava/io/PrintWriter;)V

    invoke-virtual {v11}, Ljava/io/PrintWriter;->flush()V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string v3, "META-INF/MANIFEST.MF"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    const v7, 0x81a4

    move-object v0, p0

    move-object v2, p1

    invoke-super/range {v0 .. v7}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/Zip;->initZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V

    return-void
.end method

.method private writeServices(Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 10
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->serviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/tools/ant/types/spi/Service;

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/spi/Service;->getAsStream()Ljava/io/InputStream;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "META-INF/service/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/spi/Service;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    const v7, 0x81a4

    move-object v0, p0

    move-object v2, p1

    invoke-super/range {v0 .. v7}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addConfiguredIndexJars(Lorg/apache/tools/ant/types/Path;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->indexJars:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->indexJars:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->indexJars:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public addConfiguredManifest(Lorg/apache/tools/ant/taskdefs/Manifest;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Manifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/taskdefs/ManifestException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    :goto_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->savedConfiguredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Manifest;->merge(Lorg/apache/tools/ant/taskdefs/Manifest;)V

    goto :goto_0
.end method

.method public addConfiguredService(Lorg/apache/tools/ant/types/spi/Service;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/spi/Service;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/spi/Service;->check()V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->serviceList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addMetainf(Lorg/apache/tools/ant/types/ZipFileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ZipFileSet;

    const-string v0, "META-INF/"

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/ZipFileSet;->setPrefix(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lorg/apache/tools/ant/taskdefs/Zip;->addFileset(Lorg/apache/tools/ant/types/FileSet;)V

    return-void
.end method

.method protected cleanUp()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->cleanUp()V

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->doubleFilePass:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->doubleFilePass:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->skipWriting:Z

    if-nez v0, :cond_1

    :cond_0
    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->savedConfiguredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->originalManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->rootEntries:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    return-void
.end method

.method protected createEmptyZip(Ljava/io/File;)Z
    .locals 7
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->createEmpty:Z

    if-nez v3, :cond_0

    :goto_0
    return v5

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->emptyBehavior:Ljava/lang/String;

    const-string v4, "skip"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Warning: skipping "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Jar;->archiveType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " archive "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " because no files were included."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->emptyBehavior:Ljava/lang/String;

    const-string v4, "fail"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Cannot create "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Jar;->archiveType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " archive "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ": no files were included."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v3

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Building MANIFEST-only jar: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getDestFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;)V

    new-instance v2, Lorg/apache/tools/zip/ZipOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getDestFile()Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Lorg/apache/tools/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getEncoding()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/tools/zip/ZipOutputStream;->setEncoding(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->isCompress()Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lorg/apache/tools/zip/ZipOutputStream;->setMethod(I)V

    :goto_1
    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/taskdefs/Jar;->initZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/taskdefs/Jar;->finalizeZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_3

    :try_start_2
    invoke-virtual {v2}, Lorg/apache/tools/zip/ZipOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    :goto_2
    iput-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Jar;->createEmpty:Z

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    :try_start_3
    invoke-virtual {v2, v3}, Lorg/apache/tools/zip/ZipOutputStream;->setMethod(I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_4
    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Could not create almost empty JAR archive ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v3, v4, v0, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v3

    :goto_4
    if-eqz v1, :cond_5

    :try_start_5
    invoke-virtual {v1}, Lorg/apache/tools/zip/ZipOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_5
    :goto_5
    iput-boolean v6, p0, Lorg/apache/tools/ant/taskdefs/Jar;->createEmpty:Z

    throw v3

    :catch_1
    move-exception v3

    goto :goto_2

    :catch_2
    move-exception v4

    goto :goto_5

    :catchall_1
    move-exception v3

    move-object v1, v2

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_3
.end method

.method protected finalizeZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 1
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->index:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->createIndexList(Lorg/apache/tools/zip/ZipOutputStream;)V

    :cond_0
    return-void
.end method

.method protected getResourcesToAdd([Lorg/apache/tools/ant/types/ResourceCollection;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;
    .locals 4
    .param p1    # [Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # Ljava/io/File;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    invoke-direct {p0, p2}, Lorg/apache/tools/ant/taskdefs/Jar;->getManifestFromJar(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->originalManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->originalManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    if-nez v2, :cond_1

    const-string v2, "Updating jar since the current jar has no manifest"

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p3, 0x1

    :cond_0
    :goto_0
    iput-boolean p3, p0, Lorg/apache/tools/ant/taskdefs/Jar;->createEmpty:Z

    invoke-super {p0, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/Zip;->getResourcesToAdd([Lorg/apache/tools/ant/types/ResourceCollection;Ljava/io/File;Z)Lorg/apache/tools/ant/taskdefs/Zip$ArchiveState;

    move-result-object v2

    return-object v2

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->createManifest()Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->originalManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Manifest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Updating jar since jar manifest has changed"

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    const/4 p3, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "error while reading original manifest in file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    const/4 p3, 0x1

    goto :goto_0

    :cond_2
    const/4 p3, 0x1

    goto :goto_0
.end method

.method protected initZipOutputStream(Lorg/apache/tools/zip/ZipOutputStream;)V
    .locals 2
    .param p1    # Lorg/apache/tools/zip/ZipOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->skipWriting:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->createManifest()Lorg/apache/tools/ant/taskdefs/Manifest;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Jar;->writeManifest(Lorg/apache/tools/zip/ZipOutputStream;Lorg/apache/tools/ant/taskdefs/Manifest;)V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->writeServices(Lorg/apache/tools/zip/ZipOutputStream;)V

    :cond_0
    return-void
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/Zip;->reset()V

    const-string v0, "create"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->emptyBehavior:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->configuredManifest:Lorg/apache/tools/ant/taskdefs/Manifest;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifestConfig:Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->mergeManifestsMain:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestFile:Ljava/io/File;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Jar;->index:Z

    return-void
.end method

.method public setFilesetmanifest(Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifestConfig:Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

    const-string v0, "merge"

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->mergeManifestsMain:Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifestConfig:Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifestConfig:Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Jar$FilesetManifestConfig;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "skip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->doubleFilePass:Z

    :cond_0
    return-void
.end method

.method public setIndex(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->index:Z

    return-void
.end method

.method public setJarfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->setDestFile(Ljava/io/File;)V

    return-void
.end method

.method public setManifest(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Manifest file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " does not exist."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Jar;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestFile:Ljava/io/File;

    return-void
.end method

.method public setManifestEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->manifestEncoding:Ljava/lang/String;

    return-void
.end method

.method public setWhenempty(Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;

    const-string v0, "JARs are never empty, they contain at least a manifest file"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    return-void
.end method

.method public setWhenmanifestonly(Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Zip$WhenEmpty;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->emptyBehavior:Ljava/lang/String;

    return-void
.end method

.method protected final writeIndexLikeList(Ljava/util/List;Ljava/util/List;Ljava/io/PrintWriter;)V
    .locals 5
    .param p1    # Ljava/util/List;
    .param p2    # Ljava/util/List;
    .param p3    # Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x2f

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {p2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v3, 0x5c

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const-string v3, "./"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string v3, "META-INF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto :goto_2

    :cond_5
    return-void
.end method

.method protected zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V
    .locals 2
    .param p1    # Ljava/io/InputStream;
    .param p2    # Lorg/apache/tools/zip/ZipOutputStream;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/io/File;
    .param p7    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "META-INF/MANIFEST.MF"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->doubleFilePass:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->doubleFilePass:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->skipWriting:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, p6, p1}, Lorg/apache/tools/ant/taskdefs/Jar;->filesetManifest(Ljava/io/File;Ljava/io/InputStream;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "META-INF/INDEX.LIST"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->index:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Warning: selected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Jar;->archiveType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " files include a META-INF/INDEX.LIST which will"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " be replaced by a newly generated one."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Jar;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->index:Z

    if-eqz v0, :cond_4

    const-string v0, "/"

    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Jar;->rootEntries:Ljava/util/Vector;

    invoke-virtual {v0, p3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_4
    invoke-super/range {p0 .. p7}, Lorg/apache/tools/ant/taskdefs/Zip;->zipFile(Ljava/io/InputStream;Lorg/apache/tools/zip/ZipOutputStream;Ljava/lang/String;JLjava/io/File;I)V

    goto :goto_0
.end method
