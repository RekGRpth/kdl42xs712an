.class public final Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;
.super Ljava/lang/Object;
.source "CompilerAdapterFactory.java"


# static fields
.field private static final MODERN_COMPILER:Ljava/lang/String; = "com.sun.tools.javac.Main"

.field static class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapter:Ljava/lang/Class;

.field static class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapterFactory:Ljava/lang/Class;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static doesModernCompilerExist()Z
    .locals 4

    const/4 v2, 0x1

    :try_start_0
    const-string v3, "com.sun.tools.javac.Main"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception v1

    :try_start_1
    sget-object v3, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapterFactory:Ljava/lang/Class;

    if-nez v3, :cond_1

    const-string v3, "org.apache.tools.ant.taskdefs.compilers.CompilerAdapterFactory"

    invoke-static {v3}, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapterFactory:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "com.sun.tools.javac.Main"

    invoke-virtual {v0, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    goto :goto_0

    :catch_1
    move-exception v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    sget-object v3, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapterFactory:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1
.end method

.method public static getCompiler(Ljava/lang/String;Lorg/apache/tools/ant/Task;)Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapter;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Lorg/apache/tools/ant/Task;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v0, 0x1

    const-string v1, "1.2"

    invoke-static {v1}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "1.3"

    invoke-static {v1}, Lorg/apache/tools/ant/util/JavaEnvUtils;->isJavaVersion(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    const-string v1, "jikes"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Jikes;-><init>()V

    :goto_0
    return-object v1

    :cond_1
    const-string v1, "extJavac"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/JavacExternal;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/JavacExternal;-><init>()V

    goto :goto_0

    :cond_2
    const-string v1, "classic"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "javac1.1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "javac1.2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    if-eqz v0, :cond_4

    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Javac12;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Javac12;-><init>()V

    goto :goto_0

    :cond_4
    const-string v1, "This version of java does not support the classic compiler; upgrading to modern"

    invoke-virtual {p1, v1, v2}, Lorg/apache/tools/ant/Task;->log(Ljava/lang/String;I)V

    const-string p0, "modern"

    :cond_5
    const-string v1, "modern"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "javac1.3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "javac1.4"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "javac1.5"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "javac1.6"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_6
    invoke-static {}, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->doesModernCompilerExist()Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Javac13;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Javac13;-><init>()V

    goto :goto_0

    :cond_7
    if-eqz v0, :cond_8

    const-string v1, "Modern compiler not found - looking for classic compiler"

    invoke-virtual {p1, v1, v2}, Lorg/apache/tools/ant/Task;->log(Ljava/lang/String;I)V

    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Javac12;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Javac12;-><init>()V

    goto :goto_0

    :cond_8
    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Unable to find a javac compiler;\ncom.sun.tools.javac.Main is not on the classpath.\nPerhaps JAVA_HOME does not point to the JDK.\nIt is currently set to \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJavaHome()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    const-string v1, "jvc"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "microsoft"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_a
    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;-><init>()V

    goto/16 :goto_0

    :cond_b
    const-string v1, "kjc"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Kjc;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Kjc;-><init>()V

    goto/16 :goto_0

    :cond_c
    const-string v1, "gcj"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Gcj;-><init>()V

    goto/16 :goto_0

    :cond_d
    const-string v1, "sj"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "symantec"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    :cond_e
    new-instance v1, Lorg/apache/tools/ant/taskdefs/compilers/Sj;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/compilers/Sj;-><init>()V

    goto/16 :goto_0

    :cond_f
    invoke-static {p0}, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->resolveClassName(Ljava/lang/String;)Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapter;

    move-result-object v1

    goto/16 :goto_0
.end method

.method private static resolveClassName(Ljava/lang/String;)Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapter;
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    sget-object v0, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapterFactory:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.taskdefs.compilers.CompilerAdapterFactory"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapterFactory:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapter:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "org.apache.tools.ant.taskdefs.compilers.CompilerAdapter"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapter:Ljava/lang/Class;

    :goto_1
    invoke-static {p0, v1, v0}, Lorg/apache/tools/ant/util/ClasspathUtils;->newInstance(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapter;

    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapterFactory:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v0, Lorg/apache/tools/ant/taskdefs/compilers/CompilerAdapterFactory;->class$org$apache$tools$ant$taskdefs$compilers$CompilerAdapter:Ljava/lang/Class;

    goto :goto_1
.end method
