.class public Lorg/apache/tools/ant/taskdefs/compilers/Jvc;
.super Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;
.source "Jvc.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v8, "Using jvc compiler"

    const/4 v9, 0x3

    invoke-virtual {v7, v8, v9}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->project:Lorg/apache/tools/ant/Project;

    invoke-direct {v0, v7}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->getBootClassPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/Path;->size()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {v0, v6}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :cond_0
    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->includeJavaRuntime:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->extdirs:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v7}, Lorg/apache/tools/ant/types/Path;->addExtdirs(Lorg/apache/tools/ant/types/Path;)V

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->getCompileClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v7

    invoke-virtual {v0, v7}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->compileSourcepath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v7}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    :goto_0
    new-instance v1, Lorg/apache/tools/ant/types/Commandline;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/Commandline;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Javac;->getExecutable()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, "jvc"

    :cond_2
    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->destDir:Ljava/io/File;

    if-eqz v7, :cond_3

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/d"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->destDir:Ljava/io/File;

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    :cond_3
    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/cp:p"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    invoke-virtual {v7, v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    const/4 v4, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    const-string v8, "build.compiler.jvc.extensions"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-static {v5}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v4

    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/x-"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/nomessage"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/nologo"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->debug:Z

    if-eqz v7, :cond_6

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/g"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_6
    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->optimize:Z

    if-eqz v7, :cond_7

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/O"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_7
    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->verbose:Z

    if-eqz v7, :cond_8

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "/verbose"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->addCurrentCompilerArgs(Lorg/apache/tools/ant/types/Commandline;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->size()I

    move-result v3

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->logAndAddFilesToCompile(Lorg/apache/tools/ant/types/Commandline;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7, v3}, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->executeExternalCompile([Ljava/lang/String;I)I

    move-result v7

    if-nez v7, :cond_a

    const/4 v7, 0x1

    :goto_1
    return v7

    :cond_9
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/Jvc;->src:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, v7}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto/16 :goto_0

    :cond_a
    const/4 v7, 0x0

    goto :goto_1
.end method
