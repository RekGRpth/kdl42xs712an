.class public Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;
.super Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;
.source "AptCompilerAdapter.java"


# static fields
.field private static final APT_COMPILER_SUCCESS:I = 0x0

.field public static final APT_ENTRY_POINT:Ljava/lang/String; = "com.sun.tools.apt.Main"

.field public static final APT_METHOD_NAME:Ljava/lang/String; = "process"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/compilers/DefaultCompilerAdapter;-><init>()V

    return-void
.end method

.method static setAptCommandlineSwitches(Lorg/apache/tools/ant/taskdefs/Apt;Lorg/apache/tools/ant/types/Commandline;)V
    .locals 9
    .param p0    # Lorg/apache/tools/ant/taskdefs/Apt;
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Apt;->isCompile()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "-nocompile"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Apt;->getFactory()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "-factory"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    invoke-virtual {v7, v2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Apt;->getFactoryPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "-factorypath"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    invoke-virtual {v7, v3}, Lorg/apache/tools/ant/types/Commandline$Argument;->setPath(Lorg/apache/tools/ant/types/Path;)V

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Apt;->getPreprocessDir()Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    const-string v8, "-s"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/tools/ant/types/Commandline$Argument;->setFile(Ljava/io/File;)V

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Apt;->getOptions()Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/taskdefs/Apt$Option;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "-A"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Apt$Option;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Apt$Option;->getValue()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    const-string v7, "="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Apt$Option;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Commandline;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    return-void
.end method


# virtual methods
.method public execute()Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;->attributes:Lorg/apache/tools/ant/taskdefs/Javac;

    const-string v10, "Using apt compiler"

    const/4 v11, 0x3

    invoke-virtual {v7, v10, v11}, Lorg/apache/tools/ant/taskdefs/Javac;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;->setupModernJavacCommand()Lorg/apache/tools/ant/types/Commandline;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;->setAptCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;)V

    :try_start_0
    const-string v7, "com.sun.tools.apt.Main"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    const-string v7, "process"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v1, v7, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Commandline;->getArguments()[Ljava/lang/String;

    move-result-object v11

    aput-object v11, v7, v10

    invoke-virtual {v3, v4, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    if-nez v6, :cond_0

    move v7, v8

    :goto_0
    return v7

    :cond_0
    move v7, v9

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v5

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Error starting apt compiler"

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;->location:Lorg/apache/tools/ant/Location;

    invoke-direct {v7, v8, v5, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7
.end method

.method protected getApt()Lorg/apache/tools/ant/taskdefs/Apt;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;->getJavac()Lorg/apache/tools/ant/taskdefs/Javac;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/Apt;

    return-object v0
.end method

.method protected setAptCommandlineSwitches(Lorg/apache/tools/ant/types/Commandline;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Commandline;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;->getApt()Lorg/apache/tools/ant/taskdefs/Apt;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/tools/ant/taskdefs/compilers/AptCompilerAdapter;->setAptCommandlineSwitches(Lorg/apache/tools/ant/taskdefs/Apt;Lorg/apache/tools/ant/types/Commandline;)V

    return-void
.end method
