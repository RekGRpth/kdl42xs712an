.class public Lorg/apache/tools/ant/taskdefs/Sync;
.super Lorg/apache/tools/ant/Task;
.source "Sync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;,
        Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;
    }
.end annotation


# instance fields
.field private myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

.field private syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    return-void
.end method

.method static access$100(Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Ljava/lang/String;
    .param p1    # Z

    invoke-static {p0, p1}, Lorg/apache/tools/ant/taskdefs/Sync;->assertTrue(Ljava/lang/String;Z)V

    return-void
.end method

.method private static assertTrue(Ljava/lang/String;Z)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Z

    if-nez p1, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Assertion Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private configureTask(Lorg/apache/tools/ant/Task;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Task;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/Task;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getTaskName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/Task;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/Task;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/Task;->init()V

    return-void
.end method

.method private logRemovedCount(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v4, 0x2

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->getToDir()Ljava/io/File;

    move-result-object v0

    if-nez p2, :cond_0

    const-string v1, ""

    :goto_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    if-ge p1, v4, :cond_1

    :goto_1
    invoke-virtual {v2, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    if-lez p1, :cond_2

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Removed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v4}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    :goto_2
    return-void

    :cond_0
    move-object v1, p2

    goto :goto_0

    :cond_1
    move-object p3, p4

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "NO "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " to remove from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    goto :goto_2
.end method

.method private removeEmptyDirectories(Ljava/io/File;Z)I
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Z

    const/4 v5, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_1

    aget-object v1, v0, v2

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v1, v5}, Lorg/apache/tools/ant/taskdefs/Sync;->removeEmptyDirectories(Ljava/io/File;Z)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    array-length v4, v0

    if-lez v4, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    :cond_2
    array-length v4, v0

    if-ge v4, v5, :cond_3

    if-eqz p2, :cond_3

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Removing empty directory: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    add-int/lit8 v3, v3, 0x1

    :cond_3
    return v3
.end method

.method private removeOrphanFiles(Ljava/util/Set;Ljava/io/File;)[I
    .locals 13
    .param p1    # Ljava/util/Set;
    .param p2    # Ljava/io/File;

    const/4 v11, 0x2

    new-array v9, v11, [I

    fill-array-data v9, :array_0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    invoke-interface {p1, v11}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    move-object v2, v11

    check-cast v2, [Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v11

    const-string v12, ""

    aput-object v12, v2, v11

    const/4 v1, 0x0

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    if-eqz v11, :cond_3

    new-instance v5, Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {v5}, Lorg/apache/tools/ant/types/FileSet;-><init>()V

    invoke-virtual {v5, p2}, Lorg/apache/tools/ant/types/FileSet;->setDir(Ljava/io/File;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    invoke-virtual {v11}, Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;->isCaseSensitive()Z

    move-result v11

    invoke-virtual {v5, v11}, Lorg/apache/tools/ant/types/FileSet;->setCaseSensitive(Z)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    invoke-virtual {v11}, Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;->isFollowSymlinks()Z

    move-result v11

    invoke-virtual {v5, v11}, Lorg/apache/tools/ant/types/FileSet;->setFollowSymlinks(Z)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v12

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;->mergePatterns(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/PatternSet;

    move-result-object v8

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v8, v11}, Lorg/apache/tools/ant/types/PatternSet;->getIncludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lorg/apache/tools/ant/types/FileSet;->appendExcludes([Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v8, v11}, Lorg/apache/tools/ant/types/PatternSet;->getExcludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lorg/apache/tools/ant/types/FileSet;->appendIncludes([Ljava/lang/String;)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    invoke-virtual {v11}, Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;->getDefaultexcludes()Z

    move-result v11

    if-nez v11, :cond_0

    const/4 v11, 0x1

    :goto_0
    invoke-virtual {v5, v11}, Lorg/apache/tools/ant/types/FileSet;->setDefaultexcludes(Z)V

    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v12

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;->getSelectors(Lorg/apache/tools/ant/Project;)[Lorg/apache/tools/ant/types/selectors/FileSelector;

    move-result-object v10

    array-length v11, v10

    if-lez v11, :cond_2

    new-instance v7, Lorg/apache/tools/ant/types/selectors/NoneSelector;

    invoke-direct {v7}, Lorg/apache/tools/ant/types/selectors/NoneSelector;-><init>()V

    const/4 v6, 0x0

    :goto_1
    array-length v11, v10

    if-ge v6, v11, :cond_1

    aget-object v11, v10, v6

    invoke-virtual {v7, v11}, Lorg/apache/tools/ant/types/selectors/NoneSelector;->appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v7}, Lorg/apache/tools/ant/types/FileSet;->appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Sync;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual {v5, v11}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v1

    :goto_2
    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/DirectoryScanner;->addExcludes([Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/DirectoryScanner;->scan()V

    invoke-virtual {v1}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    :goto_3
    array-length v11, v4

    if-ge v6, v11, :cond_4

    new-instance v3, Ljava/io/File;

    aget-object v11, v4, v6

    invoke-direct {v3, p2, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Removing orphan file: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    const/4 v11, 0x1

    aget v12, v9, v11

    add-int/lit8 v12, v12, 0x1

    aput v12, v9, v11

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_3
    new-instance v1, Lorg/apache/tools/ant/DirectoryScanner;

    invoke-direct {v1}, Lorg/apache/tools/ant/DirectoryScanner;-><init>()V

    invoke-virtual {v1, p2}, Lorg/apache/tools/ant/DirectoryScanner;->setBasedir(Ljava/io/File;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v0

    array-length v11, v0

    add-int/lit8 v6, v11, -0x1

    :goto_4
    if-ltz v6, :cond_6

    new-instance v3, Ljava/io/File;

    aget-object v11, v0, v6

    invoke-direct {v3, p2, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v11

    array-length v11, v11

    const/4 v12, 0x1

    if-ge v11, v12, :cond_5

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "Removing orphan directory: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    const/4 v11, 0x0

    aget v12, v9, v11

    add-int/lit8 v12, v12, 0x1

    aput v12, v9, v11

    :cond_5
    add-int/lit8 v6, v6, -0x1

    goto :goto_4

    :cond_6
    return-object v9

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Sync;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addPreserveInTarget(Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you must not specify multiple preserveintarget elements."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Sync;->syncTarget:Lorg/apache/tools/ant/taskdefs/Sync$SyncTarget;

    return-void
.end method

.method public execute()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v11, 0x4

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->getToDir()Ljava/io/File;

    move-result-object v4

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-static {v7}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->access$000(Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v7

    array-length v7, v7

    if-ge v7, v6, :cond_2

    :cond_0
    move v1, v6

    :goto_0
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "PASS#1: Copying files to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7, v11}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->execute()V

    if-eqz v1, :cond_3

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "NO removing necessary in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v11}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v5

    goto :goto_0

    :cond_3
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "PASS#2: Removing orphan files from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7, v11}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    invoke-direct {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/Sync;->removeOrphanFiles(Ljava/util/Set;Ljava/io/File;)[I

    move-result-object v3

    aget v7, v3, v5

    const-string v8, "dangling director"

    const-string v9, "y"

    const-string v10, "ies"

    invoke-direct {p0, v7, v8, v9, v10}, Lorg/apache/tools/ant/taskdefs/Sync;->logRemovedCount(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aget v6, v3, v6

    const-string v7, "dangling file"

    const-string v8, ""

    const-string v9, "s"

    invoke-direct {p0, v6, v7, v8, v9}, Lorg/apache/tools/ant/taskdefs/Sync;->logRemovedCount(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->getIncludeEmptyDirs()Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "PASS#3: Removing empty directories from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6, v11}, Lorg/apache/tools/ant/taskdefs/Sync;->log(Ljava/lang/String;I)V

    invoke-direct {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Sync;->removeEmptyDirectories(Ljava/io/File;Z)I

    move-result v2

    const-string v5, "empty director"

    const-string v6, "y"

    const-string v7, "ies"

    invoke-direct {p0, v2, v5, v6, v7}, Lorg/apache/tools/ant/taskdefs/Sync;->logRemovedCount(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public init()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v1, 0x0

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Sync;->configureTask(Lorg/apache/tools/ant/Task;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setFiltering(Z)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setIncludeEmptyDirs(Z)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setPreserveLastModified(Z)V

    return-void
.end method

.method public setFailOnError(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setFailOnError(Z)V

    return-void
.end method

.method public setGranularity(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, p1, p2}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setGranularity(J)V

    return-void
.end method

.method public setIncludeEmptyDirs(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setIncludeEmptyDirs(Z)V

    return-void
.end method

.method public setOverwrite(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setOverwrite(Z)V

    return-void
.end method

.method public setTodir(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setTodir(Ljava/io/File;)V

    return-void
.end method

.method public setVerbose(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Sync;->myCopy:Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Sync$MyCopy;->setVerbose(Z)V

    return-void
.end method
