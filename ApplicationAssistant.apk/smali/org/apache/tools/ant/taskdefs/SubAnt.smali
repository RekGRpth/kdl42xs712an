.class public Lorg/apache/tools/ant/taskdefs/SubAnt;
.super Lorg/apache/tools/ant/Task;
.source "SubAnt.java"


# instance fields
.field private ant:Lorg/apache/tools/ant/taskdefs/Ant;

.field private antfile:Ljava/lang/String;

.field private buildpath:Lorg/apache/tools/ant/types/Path;

.field private failOnError:Z

.field private genericantfile:Ljava/io/File;

.field private inheritAll:Z

.field private inheritRefs:Z

.field private output:Ljava/lang/String;

.field private properties:Ljava/util/Vector;

.field private propertySets:Ljava/util/Vector;

.field private references:Ljava/util/Vector;

.field private subTarget:Ljava/lang/String;

.field private targets:Ljava/util/Vector;

.field private verbose:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->subTarget:Ljava/lang/String;

    const-string v0, "build.xml"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->antfile:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->genericantfile:Ljava/io/File;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->verbose:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->inheritAll:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->inheritRefs:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->failOnError:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->output:Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->properties:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->references:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->propertySets:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->targets:Ljava/util/Vector;

    return-void
.end method

.method private static copyProperty(Lorg/apache/tools/ant/taskdefs/Property;Lorg/apache/tools/ant/taskdefs/Property;)V
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Property;
    .param p1    # Lorg/apache/tools/ant/taskdefs/Property;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setName(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setValue(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setFile(Ljava/io/File;)V

    :cond_1
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getResource()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getResource()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setResource(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getPrefix()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setPrefix(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    :cond_4
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getEnvironment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getEnvironment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setEnvironment(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Property;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Property;->setClasspath(Lorg/apache/tools/ant/types/Path;)V

    :cond_6
    return-void
.end method

.method private createAntTask(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Ant;
    .locals 4
    .param p1    # Ljava/io/File;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Ant;-><init>(Lorg/apache/tools/ant/Task;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Ant;->init()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->subTarget:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->subTarget:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->subTarget:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->setTarget(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->output:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->output:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->setOutput(Ljava/lang/String;)V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->setDir(Ljava/io/File;)V

    :cond_2
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->inheritAll:Z

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->setInheritAll(Z)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->properties:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Ant;->createProperty()Lorg/apache/tools/ant/taskdefs/Property;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/Property;

    invoke-static {v3, v2}, Lorg/apache/tools/ant/taskdefs/SubAnt;->copyProperty(Lorg/apache/tools/ant/taskdefs/Property;Lorg/apache/tools/ant/taskdefs/Property;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->propertySets:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/PropertySet;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->addPropertyset(Lorg/apache/tools/ant/types/PropertySet;)V

    goto :goto_1

    :cond_4
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->inheritRefs:Z

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->setInheritRefs(Z)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->references:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/Ant$Reference;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Ant;->addReference(Lorg/apache/tools/ant/taskdefs/Ant$Reference;)V

    goto :goto_2

    :cond_5
    return-object v0
.end method

.method private execute(Ljava/io/File;Ljava/io/File;)V
    .locals 9
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Invalid file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->failOnError:Z

    if-eqz v5, :cond_1

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    invoke-virtual {p0, v3, v8}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_2
    invoke-direct {p0, p2}, Lorg/apache/tools/ant/taskdefs/SubAnt;->createAntTask(Ljava/io/File;)Lorg/apache/tools/ant/taskdefs/Ant;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/taskdefs/Ant;->setAntfile(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->targets:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->targets:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v5, v4}, Lorg/apache/tools/ant/taskdefs/Ant;->addConfiguredTarget(Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    :try_start_0
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Ant;->execute()V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->failOnError:Z

    if-eqz v5, :cond_4

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    throw v5

    :cond_4
    :try_start_2
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Failure for target \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->subTarget:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\' of: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/tools/ant/BuildException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_3
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->failOnError:Z

    if-eqz v5, :cond_5

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :cond_5
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Failure for target \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->subTarget:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\' of: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    goto/16 :goto_0
.end method

.method private getBuildpath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->buildpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->buildpath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->buildpath:Lorg/apache/tools/ant/types/Path;

    return-object v0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->getBuildpath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addConfiguredTarget(Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Ant$TargetElement;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "target name must not be empty"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->targets:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addDirset(Lorg/apache/tools/ant/types/DirSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/DirSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/SubAnt;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFilelist(Lorg/apache/tools/ant/types/FileList;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/SubAnt;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/SubAnt;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public addProperty(Lorg/apache/tools/ant/taskdefs/Property;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Property;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->properties:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addPropertyset(Lorg/apache/tools/ant/types/PropertySet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PropertySet;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->propertySets:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addReference(Lorg/apache/tools/ant/taskdefs/Ant$Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Ant$Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->references:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public createBuildpath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->getBuildpath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createBuildpathElement()Lorg/apache/tools/ant/types/Path$PathElement;
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->getBuildpath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPathElement()Lorg/apache/tools/ant/types/Path$PathElement;

    move-result-object v0

    return-object v0
.end method

.method public execute()V
    .locals 14

    const/4 v11, 0x1

    const/4 v13, 0x0

    const/4 v12, 0x2

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->buildpath:Lorg/apache/tools/ant/types/Path;

    if-nez v10, :cond_0

    new-instance v10, Lorg/apache/tools/ant/BuildException;

    const-string v11, "No buildpath specified"

    invoke-direct {v10, v11}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->buildpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v10}, Lorg/apache/tools/ant/types/Path;->list()[Ljava/lang/String;

    move-result-object v6

    array-length v1, v6

    if-ge v1, v11, :cond_2

    const-string v10, "No sub-builds to iterate on"

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v1, :cond_d

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v2, 0x0

    :try_start_0
    new-instance v5, Ljava/io/File;

    aget-object v10, v6, v7

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_e

    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->verbose:Z

    if-eqz v10, :cond_3

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Entering directory: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    :cond_3
    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->genericantfile:Ljava/io/File;

    if-eqz v10, :cond_7

    move-object v2, v5

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->genericantfile:Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    invoke-direct {p0, v4, v2}, Lorg/apache/tools/ant/taskdefs/SubAnt;->execute(Ljava/io/File;Ljava/io/File;)V

    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->verbose:Z

    if-eqz v10, :cond_4

    if-eqz v8, :cond_4

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Leaving directory: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    :goto_2
    if-eqz v9, :cond_6

    instance-of v10, v9, Lorg/apache/tools/ant/BuildException;

    if-eqz v10, :cond_c

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "File \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\' failed with message \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\'."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10, v13}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    if-nez v0, :cond_5

    move-object v0, v9

    check-cast v0, Lorg/apache/tools/ant/BuildException;

    :cond_5
    :goto_3
    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->verbose:Z

    if-eqz v10, :cond_6

    if-eqz v8, :cond_6

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Leaving directory: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10, v12}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_7
    :try_start_3
    new-instance v4, Ljava/io/File;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->antfile:Ljava/lang/String;

    invoke-direct {v4, v5, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    :catch_0
    move-exception v3

    :goto_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/Project;->isKeepGoingMode()Z

    move-result v10

    if-nez v10, :cond_9

    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->verbose:Z

    if-eqz v10, :cond_8

    if-eqz v8, :cond_8

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Leaving directory: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10, v12}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    :cond_8
    throw v3

    :cond_9
    move-object v9, v3

    goto/16 :goto_2

    :catch_1
    move-exception v3

    :goto_5
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/tools/ant/Project;->isKeepGoingMode()Z

    move-result v10

    if-nez v10, :cond_b

    iget-boolean v10, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->verbose:Z

    if-eqz v10, :cond_a

    if-eqz v8, :cond_a

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Leaving directory: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10, v12}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    :cond_a
    new-instance v10, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v10, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    :cond_b
    move-object v9, v3

    goto/16 :goto_2

    :cond_c
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Target \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\' failed with message \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\'."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10, v13}, Lorg/apache/tools/ant/taskdefs/SubAnt;->log(Ljava/lang/String;I)V

    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v9, v10}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    if-nez v0, :cond_5

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    goto/16 :goto_3

    :cond_d
    if-eqz v0, :cond_1

    throw v0

    :catch_2
    move-exception v3

    move-object v4, v5

    goto :goto_5

    :catch_3
    move-exception v3

    move-object v4, v5

    goto/16 :goto_4

    :cond_e
    move-object v4, v5

    goto/16 :goto_1
.end method

.method public handleErrorFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleErrorFlush(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleErrorOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleErrorOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleFlush(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleInput([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/Ant;->handleInput([BII)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/apache/tools/ant/Task;->handleInput([BII)I

    move-result v0

    goto :goto_0
.end method

.method public handleOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->ant:Lorg/apache/tools/ant/taskdefs/Ant;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Ant;->handleOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAntfile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->antfile:Ljava/lang/String;

    return-void
.end method

.method public setBuildpath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->getBuildpath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public setBuildpathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SubAnt;->createBuildpath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setFailonerror(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->failOnError:Z

    return-void
.end method

.method public setGenericAntfile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->genericantfile:Ljava/io/File;

    return-void
.end method

.method public setInheritall(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->inheritAll:Z

    return-void
.end method

.method public setInheritrefs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->inheritRefs:Z

    return-void
.end method

.method public setOutput(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->output:Ljava/lang/String;

    return-void
.end method

.method public setTarget(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->subTarget:Ljava/lang/String;

    return-void
.end method

.method public setVerbose(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SubAnt;->verbose:Z

    return-void
.end method
