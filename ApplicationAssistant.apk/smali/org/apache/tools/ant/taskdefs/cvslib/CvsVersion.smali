.class public Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;
.super Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;
.source "CvsVersion.java"


# static fields
.field static final MULTIPLY:J = 0x64L

.field static final VERSION_1_11_2:J = 0x2b5eL


# instance fields
.field private clientVersion:Ljava/lang/String;

.field private clientVersionProperty:Ljava/lang/String;

.field private serverVersion:Ljava/lang/String;

.field private serverVersionProperty:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 11

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setOutputStream(Ljava/io/OutputStream;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setErrorStream(Ljava/io/OutputStream;)V

    const-string v8, "version"

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->setCommand(Ljava/lang/String;)V

    invoke-super {p0}, Lorg/apache/tools/ant/taskdefs/AbstractCvsTask;->execute()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/util/StringTokenizer;

    invoke-direct {v7, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    :cond_0
    :goto_0
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    const-string v8, "Client:"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v2, 0x1

    :cond_1
    :goto_1
    if-eqz v2, :cond_5

    if-eqz v4, :cond_5

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->clientVersion:Ljava/lang/String;

    :cond_2
    const/4 v2, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    const-string v8, "Server:"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v6, 0x1

    goto :goto_1

    :cond_4
    const-string v8, "(CVS)"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_5
    if-eqz v6, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersion:Ljava/lang/String;

    :cond_6
    const/4 v6, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_7
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->clientVersionProperty:Ljava/lang/String;

    if-eqz v8, :cond_8

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->clientVersionProperty:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->clientVersion:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersionProperty:Ljava/lang/String;

    if-eqz v8, :cond_9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersionProperty:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersion:Ljava/lang/String;

    invoke-virtual {v8, v9, v10}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    return-void
.end method

.method public getClientVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->clientVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getServerVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setClientVersionProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->clientVersionProperty:Ljava/lang/String;

    return-void
.end method

.method public setServerVersionProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersionProperty:Ljava/lang/String;

    return-void
.end method

.method public supportsCvsLogWithSOption()Z
    .locals 11

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersion:Ljava/lang/String;

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return v8

    :cond_1
    new-instance v5, Ljava/util/StringTokenizer;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/cvslib/CvsVersion;->serverVersion:Ljava/lang/String;

    const-string v10, "."

    invoke-direct {v5, v9, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, 0x2710

    const-wide/16 v6, 0x0

    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v2, v9, :cond_2

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->isDigit(C)Z

    move-result v9

    if-nez v9, :cond_4

    :cond_2
    invoke-virtual {v3, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    mul-long/2addr v9, v0

    add-long/2addr v6, v9

    const-wide/16 v9, 0x1

    cmp-long v9, v0, v9

    if-nez v9, :cond_5

    :cond_3
    const-wide/16 v9, 0x2b5e

    cmp-long v9, v6, v9

    if-ltz v9, :cond_0

    const/4 v8, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    const-wide/16 v9, 0x64

    div-long/2addr v0, v9

    goto :goto_1
.end method
