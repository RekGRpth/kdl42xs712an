.class public Lorg/apache/tools/ant/taskdefs/TempFile;
.super Lorg/apache/tools/ant/Task;
.source "TempFile.java"


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private deleteOnExit:Z

.field private destDir:Ljava/io/File;

.field private prefix:Ljava/lang/String;

.field private property:Ljava/lang/String;

.field private suffix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/TempFile;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->destDir:Ljava/io/File;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->suffix:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->property:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->property:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "no property specified"

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->destDir:Ljava/io/File;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/TempFile;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->destDir:Ljava/io/File;

    :cond_2
    sget-object v1, Lorg/apache/tools/ant/taskdefs/TempFile;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->prefix:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->suffix:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->destDir:Ljava/io/File;

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->deleteOnExit:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Z)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/TempFile;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->property:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public isDeleteOnExit()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->deleteOnExit:Z

    return v0
.end method

.method public setDeleteOnExit(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->deleteOnExit:Z

    return-void
.end method

.method public setDestDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->destDir:Ljava/io/File;

    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->prefix:Ljava/lang/String;

    return-void
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->property:Ljava/lang/String;

    return-void
.end method

.method public setSuffix(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/TempFile;->suffix:Ljava/lang/String;

    return-void
.end method
