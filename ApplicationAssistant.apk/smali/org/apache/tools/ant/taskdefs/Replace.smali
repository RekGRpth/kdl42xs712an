.class public Lorg/apache/tools/ant/taskdefs/Replace;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "Replace.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;,
        Lorg/apache/tools/ant/taskdefs/Replace$FileInput;,
        Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;,
        Lorg/apache/tools/ant/taskdefs/Replace$NestedString;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private dir:Ljava/io/File;

.field private encoding:Ljava/lang/String;

.field private fileCount:I

.field private properties:Ljava/util/Properties;

.field private propertyFile:Ljava/io/File;

.field private replaceCount:I

.field private replaceFilterFile:Ljava/io/File;

.field private replacefilters:Ljava/util/Vector;

.field private src:Ljava/io/File;

.field private summary:Z

.field private token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

.field private value:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Replace;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->src:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;-><init>(Lorg/apache/tools/ant/taskdefs/Replace;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->value:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceFilterFile:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->properties:Ljava/util/Properties;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->dir:Ljava/io/File;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->summary:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->encoding:Ljava/lang/String;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/io/File;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Replace;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    return-object v0
.end method

.method static access$100(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/util/Properties;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Replace;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->properties:Ljava/util/Properties;

    return-object v0
.end method

.method static access$200(Lorg/apache/tools/ant/taskdefs/Replace;)Lorg/apache/tools/ant/taskdefs/Replace$NestedString;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Replace;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->value:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    return-object v0
.end method

.method static access$304(Lorg/apache/tools/ant/taskdefs/Replace;)I
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Replace;

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceCount:I

    return v0
.end method

.method static access$400(Lorg/apache/tools/ant/taskdefs/Replace;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Replace;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method private buildFilterChain(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 4
    .param p1    # Ljava/lang/StringBuffer;

    move-object v0, p1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->setInputBuffer(Ljava/lang/StringBuffer;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->getOutputBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private createPrimaryfilter()Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;
    .locals 3

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;-><init>(Lorg/apache/tools/ant/taskdefs/Replace;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    return-object v0
.end method

.method private flushFilterChain()V
    .locals 3

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->flush()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private logFilterChain(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Replacing in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " --> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->getReplaceValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Replace;->log(Ljava/lang/String;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private processFile(Ljava/io/File;)V
    .locals 12
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Replace: source file "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " doesn\'t exist"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v8

    :cond_0
    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    :try_start_0
    new-instance v2, Lorg/apache/tools/ant/taskdefs/Replace$FileInput;

    invoke-direct {v2, p0, p1}, Lorg/apache/tools/ant/taskdefs/Replace$FileInput;-><init>(Lorg/apache/tools/ant/taskdefs/Replace;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v8, Lorg/apache/tools/ant/taskdefs/Replace;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v9, "rep"

    const-string v10, ".tmp"

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    new-instance v5, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;

    invoke-direct {v5, p0, v7}, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;-><init>(Lorg/apache/tools/ant/taskdefs/Replace;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget v6, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceCount:I

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lorg/apache/tools/ant/taskdefs/Replace;->logFilterChain(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Replace$FileInput;->getOutputBuffer()Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-direct {p0, v8}, Lorg/apache/tools/ant/taskdefs/Replace;->buildFilterChain(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v5, v8}, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;->setInputBuffer(Ljava/lang/StringBuffer;)V

    :cond_1
    :goto_0
    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Replace$FileInput;->readChunk()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->processFilterChain()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;->process()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v3

    move-object v4, v5

    move-object v1, v2

    :goto_1
    :try_start_3
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "IOException in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v10

    invoke-direct {v8, v9, v3, v10}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v8

    :goto_2
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Replace$FileInput;->closeQuietly()V

    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;->closeQuietly()V

    :cond_3
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v9

    if-nez v9, :cond_4

    invoke-virtual {v7}, Ljava/io/File;->deleteOnExit()V

    :cond_4
    throw v8

    :cond_5
    :try_start_4
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->flushFilterChain()V

    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;->flush()V

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Replace$FileInput;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const/4 v1, 0x0

    :try_start_5
    invoke-virtual {v5}, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    const/4 v4, 0x0

    :try_start_6
    iget v8, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceCount:I

    if-eq v8, v6, :cond_a

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_6

    sget-object v8, Lorg/apache/tools/ant/taskdefs/Replace;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v8, v7, p1}, Lorg/apache/tools/ant/util/FileUtils;->rename(Ljava/io/File;Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v7, 0x0

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Replace$FileInput;->closeQuietly()V

    :cond_7
    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Replace$FileOutput;->closeQuietly()V

    :cond_8
    if-eqz v7, :cond_9

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result v8

    if-nez v8, :cond_9

    invoke-virtual {v7}, Ljava/io/File;->deleteOnExit()V

    :cond_9
    return-void

    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    :catchall_1
    move-exception v8

    move-object v1, v2

    goto :goto_2

    :catchall_2
    move-exception v8

    move-object v4, v5

    move-object v1, v2

    goto :goto_2

    :catchall_3
    move-exception v8

    move-object v4, v5

    goto :goto_2

    :catch_1
    move-exception v3

    goto/16 :goto_1

    :catch_2
    move-exception v3

    move-object v1, v2

    goto/16 :goto_1

    :catch_3
    move-exception v3

    move-object v4, v5

    goto/16 :goto_1
.end method

.method private processFilterChain()Z
    .locals 3

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->process()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private stringReplace(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/StringBuffer;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p1, v0, v1, p3}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, p2, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public createReplaceToken()Lorg/apache/tools/ant/taskdefs/Replace$NestedString;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;-><init>(Lorg/apache/tools/ant/taskdefs/Replace;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    return-object v0
.end method

.method public createReplaceValue()Lorg/apache/tools/ant/taskdefs/Replace$NestedString;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Replace;->value:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    return-object v0
.end method

.method public createReplacefilter()Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;-><init>(Lorg/apache/tools/ant/taskdefs/Replace;)V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public execute()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v12}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->properties:Ljava/util/Properties;

    if-nez v12, :cond_1

    const/4 v8, 0x0

    :goto_0
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    if-eqz v12, :cond_0

    new-instance v11, Ljava/lang/StringBuffer;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->value:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    invoke-virtual {v12}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v12, "\r\n"

    const-string v13, "\n"

    invoke-direct {p0, v11, v12, v13}, Lorg/apache/tools/ant/taskdefs/Replace;->stringReplace(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "\n"

    sget-object v13, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-direct {p0, v11, v12, v13}, Lorg/apache/tools/ant/taskdefs/Replace;->stringReplace(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v10, Ljava/lang/StringBuffer;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    invoke-virtual {v12}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v12, "\r\n"

    const-string v13, "\n"

    invoke-direct {p0, v10, v12, v13}, Lorg/apache/tools/ant/taskdefs/Replace;->stringReplace(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v12, "\n"

    sget-object v13, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-direct {p0, v10, v12, v13}, Lorg/apache/tools/ant/taskdefs/Replace;->stringReplace(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->createPrimaryfilter()Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    move-result-object v3

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->setToken(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->setValue(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceFilterFile:Ljava/io/File;

    if-eqz v12, :cond_2

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceFilterFile:Ljava/io/File;

    invoke-virtual {p0, v12}, Lorg/apache/tools/ant/taskdefs/Replace;->getProperties(Ljava/io/File;)Ljava/util/Properties;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->createReplacefilter()Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    move-result-object v6

    invoke-virtual {v6, v10}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->setToken(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->setValue(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v12

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/Replace;->properties:Ljava/util/Properties;

    throw v12

    :cond_1
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->properties:Ljava/util/Properties;

    invoke-virtual {v12}, Ljava/util/Properties;->clone()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Properties;

    move-object v8, v12

    goto/16 :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->validateAttributes()V

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    if-eqz v12, :cond_3

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    invoke-virtual {p0, v12}, Lorg/apache/tools/ant/taskdefs/Replace;->getProperties(Ljava/io/File;)Ljava/util/Properties;

    move-result-object v12

    iput-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->properties:Ljava/util/Properties;

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->validateReplacefilters()V

    const/4 v12, 0x0

    iput v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->fileCount:I

    const/4 v12, 0x0

    iput v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceCount:I

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->src:Ljava/io/File;

    if-eqz v12, :cond_4

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->src:Ljava/io/File;

    invoke-direct {p0, v12}, Lorg/apache/tools/ant/taskdefs/Replace;->processFile(Ljava/io/File;)V

    :cond_4
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->dir:Ljava/io/File;

    if-eqz v12, :cond_5

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->dir:Ljava/io/File;

    invoke-super {p0, v12}, Lorg/apache/tools/ant/taskdefs/MatchingTask;->getDirectoryScanner(Ljava/io/File;)Lorg/apache/tools/ant/DirectoryScanner;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x0

    :goto_2
    array-length v12, v9

    if-ge v4, v12, :cond_5

    new-instance v2, Ljava/io/File;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->dir:Ljava/io/File;

    aget-object v13, v9, v4

    invoke-direct {v2, v12, v13}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lorg/apache/tools/ant/taskdefs/Replace;->processFile(Ljava/io/File;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    iget-boolean v12, p0, Lorg/apache/tools/ant/taskdefs/Replace;->summary:Z

    if-eqz v12, :cond_6

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Replaced "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    iget v13, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceCount:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, " occurrences in "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    iget v13, p0, Lorg/apache/tools/ant/taskdefs/Replace;->fileCount:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string v13, " files."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x2

    invoke-virtual {p0, v12, v13}, Lorg/apache/tools/ant/taskdefs/Replace;->log(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    iput-object v8, p0, Lorg/apache/tools/ant/taskdefs/Replace;->properties:Ljava/util/Properties;

    return-void
.end method

.method public getProperties(Ljava/io/File;)Ljava/util/Properties;
    .locals 7
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v4, Ljava/util/Properties;

    invoke-direct {v4}, Ljava/util/Properties;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v4, v2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    return-object v4

    :catch_0
    move-exception v0

    :goto_0
    :try_start_2
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Property file ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") not found."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v5

    :goto_1
    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v5

    :catch_1
    move-exception v0

    :goto_2
    :try_start_3
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Property file ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") cannot be loaded."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_1
    move-exception v5

    move-object v1, v2

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public setDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->dir:Ljava/io/File;

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->src:Ljava/io/File;

    return-void
.end method

.method public setPropertyFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    return-void
.end method

.method public setReplaceFilterFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replaceFilterFile:Ljava/io/File;

    return-void
.end method

.method public setSummary(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->summary:Z

    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->createReplaceToken()Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;->addText(Ljava/lang/String;)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->createReplaceValue()Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;->addText(Ljava/lang/String;)V

    return-void
.end method

.method public validateAttributes()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->src:Ljava/io/File;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->dir:Ljava/io/File;

    if-nez v1, :cond_0

    const-string v0, "Either the file or the dir attribute must be specified"

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Property file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->propertyFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " does not exist."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "Either token or a nested replacefilter must be specified"

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    if-eqz v1, :cond_3

    const-string v1, ""

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->token:Lorg/apache/tools/ant/taskdefs/Replace$NestedString;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Replace$NestedString;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "The token attribute must not be an empty string."

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Replace;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v1

    :cond_3
    return-void
.end method

.method public validateReplacefilters()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Replace;->replacefilters:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Replace$Replacefilter;->validate()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
