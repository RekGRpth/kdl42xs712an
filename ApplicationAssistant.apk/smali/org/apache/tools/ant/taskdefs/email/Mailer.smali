.class public abstract Lorg/apache/tools/ant/taskdefs/email/Mailer;
.super Ljava/lang/Object;
.source "Mailer.java"


# instance fields
.field protected SSL:Z

.field protected bccList:Ljava/util/Vector;

.field protected ccList:Ljava/util/Vector;

.field protected files:Ljava/util/Vector;

.field protected from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

.field protected headers:Ljava/util/Vector;

.field protected host:Ljava/lang/String;

.field protected includeFileNames:Z

.field protected message:Lorg/apache/tools/ant/taskdefs/email/Message;

.field protected password:Ljava/lang/String;

.field protected port:I

.field protected replyToList:Ljava/util/Vector;

.field protected subject:Ljava/lang/String;

.field protected task:Lorg/apache/tools/ant/Task;

.field protected toList:Ljava/util/Vector;

.field protected user:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->host:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->port:I

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->user:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->password:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->SSL:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->replyToList:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->toList:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->ccList:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->bccList:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->files:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->subject:Ljava/lang/String;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->includeFileNames:Z

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->headers:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method protected final getDate()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/DateUtils;->getDateForHeader()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract send()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation
.end method

.method public setBccList(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->bccList:Ljava/util/Vector;

    return-void
.end method

.method public setCcList(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->ccList:Ljava/util/Vector;

    return-void
.end method

.method public setFiles(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->files:Ljava/util/Vector;

    return-void
.end method

.method public setFrom(Lorg/apache/tools/ant/taskdefs/email/EmailAddress;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->from:Lorg/apache/tools/ant/taskdefs/email/EmailAddress;

    return-void
.end method

.method public setHeaders(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->headers:Ljava/util/Vector;

    return-void
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->host:Ljava/lang/String;

    return-void
.end method

.method public setIncludeFileNames(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->includeFileNames:Z

    return-void
.end method

.method public setMessage(Lorg/apache/tools/ant/taskdefs/email/Message;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/email/Message;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->message:Lorg/apache/tools/ant/taskdefs/email/Message;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->password:Ljava/lang/String;

    return-void
.end method

.method public setPort(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->port:I

    return-void
.end method

.method public setReplyToList(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->replyToList:Ljava/util/Vector;

    return-void
.end method

.method public setSSL(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->SSL:Z

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->subject:Ljava/lang/String;

    return-void
.end method

.method public setTask(Lorg/apache/tools/ant/Task;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Task;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->task:Lorg/apache/tools/ant/Task;

    return-void
.end method

.method public setToList(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->toList:Ljava/util/Vector;

    return-void
.end method

.method public setUser(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/Mailer;->user:Ljava/lang/String;

    return-void
.end method
