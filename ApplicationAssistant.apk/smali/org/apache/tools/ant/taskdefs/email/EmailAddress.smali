.class public Lorg/apache/tools/ant/taskdefs/email/EmailAddress;
.super Ljava/lang/Object;
.source "EmailAddress.java"


# instance fields
.field private address:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 14
    .param p1    # Ljava/lang/String;

    const/4 v13, 0x0

    const/16 v12, 0x3e

    const/16 v11, 0x3c

    const/4 v10, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v4, 0x9

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v9, 0x9

    if-le v3, v9, :cond_3

    invoke-virtual {p1, v13}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-eq v9, v11, :cond_0

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v9, v11, :cond_3

    :cond_0
    add-int/lit8 v9, v3, -0x1

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-eq v9, v12, :cond_1

    add-int/lit8 v9, v3, -0x2

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v9, v12, :cond_3

    :cond_1
    invoke-direct {p0, p1, v10}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->trim(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->address:Ljava/lang/String;

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_9

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v9, 0x28

    if-ne v0, v9, :cond_5

    add-int/lit8 v7, v7, 0x1

    if-nez v8, :cond_4

    move v1, v2

    add-int/lit8 v6, v2, 0x1

    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    const/16 v9, 0x29

    if-ne v0, v9, :cond_6

    add-int/lit8 v7, v7, -0x1

    if-nez v1, :cond_4

    add-int/lit8 v8, v2, 0x1

    move v5, v2

    goto :goto_2

    :cond_6
    if-nez v7, :cond_8

    if-ne v0, v11, :cond_8

    if-nez v8, :cond_7

    move v5, v2

    :cond_7
    add-int/lit8 v8, v2, 0x1

    goto :goto_2

    :cond_8
    if-nez v7, :cond_4

    if-ne v0, v12, :cond_4

    move v1, v2

    add-int/lit8 v9, v3, -0x1

    if-eq v1, v9, :cond_4

    add-int/lit8 v6, v2, 0x1

    goto :goto_2

    :cond_9
    if-nez v1, :cond_a

    move v1, v3

    :cond_a
    if-nez v5, :cond_b

    move v5, v3

    :cond_b
    invoke-virtual {p1, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9, v10}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->trim(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->address:Ljava/lang/String;

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9, v13}, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->trim(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->name:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->name:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->address:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    if-le v9, v3, :cond_2

    const/4 v9, 0x0

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private trim(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/16 v6, 0x22

    const/16 v5, 0x20

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x0

    :cond_0
    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x29

    if-eq v3, v4, :cond_3

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3e

    if-ne v3, v4, :cond_1

    if-nez p2, :cond_3

    :cond_1
    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v6, :cond_2

    add-int/lit8 v3, v0, -0x2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_3

    :cond_2
    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v3, v5, :cond_4

    :cond_3
    const/4 v2, 0x1

    add-int/lit8 v0, v0, -0x1

    :cond_4
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x28

    if-eq v3, v4, :cond_6

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3c

    if-ne v3, v4, :cond_5

    if-nez p2, :cond_6

    :cond_5
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v6, :cond_6

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-gt v3, v5, :cond_7

    :cond_6
    const/4 v2, 0x1

    add-int/lit8 v1, v1, 0x1

    :cond_7
    if-nez v2, :cond_0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->address:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->name:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->address:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/email/EmailAddress;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
