.class public Lorg/apache/tools/ant/taskdefs/Copy;
.super Lorg/apache/tools/ant/Task;
.source "Copy.java"


# static fields
.field static final LINE_SEPARATOR:Ljava/lang/String;

.field static final NULL_FILE_PLACEHOLDER:Ljava/io/File;

.field static class$java$io$IOException:Ljava/lang/Class;

.field static class$org$apache$tools$ant$taskdefs$Copy:Ljava/lang/Class;


# instance fields
.field protected completeDirMap:Ljava/util/Hashtable;

.field protected destDir:Ljava/io/File;

.field protected destFile:Ljava/io/File;

.field protected dirCopyMap:Ljava/util/Hashtable;

.field private enableMultipleMappings:Z

.field protected failonerror:Z

.field protected file:Ljava/io/File;

.field protected fileCopyMap:Ljava/util/Hashtable;

.field protected fileUtils:Lorg/apache/tools/ant/util/FileUtils;

.field private filterChains:Ljava/util/Vector;

.field private filterSets:Ljava/util/Vector;

.field protected filtering:Z

.field protected flatten:Z

.field protected forceOverwrite:Z

.field private granularity:J

.field protected includeEmpty:Z

.field private inputEncoding:Ljava/lang/String;

.field protected mapperElement:Lorg/apache/tools/ant/types/Mapper;

.field private outputEncoding:Ljava/lang/String;

.field protected preserveLastModified:Z

.field protected rcs:Ljava/util/Vector;

.field protected verbosity:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/NULL_FILE"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Copy;->NULL_FILE_PLACEHOLDER:Ljava/io/File;

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Copy;->LINE_SEPARATOR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->enableMultipleMappings:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filtering:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->preserveLastModified:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->forceOverwrite:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->flatten:Z

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->verbosity:I

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Copy;->includeEmpty:Z

    iput-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->dirCopyMap:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->completeDirMap:Ljava/util/Hashtable;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filterChains:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filterSets:Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->inputEncoding:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->outputEncoding:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->granularity:J

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->fileUtils:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->fileUtils:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/FileUtils;->getFileTimestampGranularity()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->granularity:J

    return-void
.end method

.method private static add(Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p0    # Ljava/io/File;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Map;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0, p2}, Lorg/apache/tools/ant/taskdefs/Copy;->add(Ljava/io/File;[Ljava/lang/String;Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method private static add(Ljava/io/File;[Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p0    # Ljava/io/File;
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/util/Map;

    if-eqz p1, :cond_1

    invoke-static {p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getKeyFile(Ljava/io/File;)Ljava/io/File;

    move-result-object p0

    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getDueTo(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/Exception;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Copy;->class$java$io$IOException:Ljava/lang/Class;

    if-nez v2, :cond_5

    const-string v2, "java.io.IOException"

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Copy;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/taskdefs/Copy;->class$java$io$IOException:Ljava/lang/Class;

    :goto_0
    if-ne v3, v2, :cond_6

    const/4 v0, 0x1

    :goto_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    if-nez v0, :cond_2

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MalformedInput"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Copy;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "This is normally due to the input file containing invalid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Copy;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "bytes for the character encoding used : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->inputEncoding:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->fileUtils:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2}, Lorg/apache/tools/ant/util/FileUtils;->getDefaultEncoding()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v2, Lorg/apache/tools/ant/taskdefs/Copy;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_5
    sget-object v2, Lorg/apache/tools/ant/taskdefs/Copy;->class$java$io$IOException:Ljava/lang/Class;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->inputEncoding:Ljava/lang/String;

    goto :goto_2
.end method

.method private static getKeyFile(Ljava/io/File;)Ljava/io/File;
    .locals 0
    .param p0    # Ljava/io/File;

    if-nez p0, :cond_0

    sget-object p0, Lorg/apache/tools/ant/taskdefs/Copy;->NULL_FILE_PLACEHOLDER:Ljava/io/File;

    :cond_0
    return-object p0
.end method

.method private getMapper()Lorg/apache/tools/ant/util/FileNameMapper;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->flatten:Z

    if-eqz v1, :cond_1

    new-instance v0, Lorg/apache/tools/ant/util/FlatFileNameMapper;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/FlatFileNameMapper;-><init>()V

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/util/IdentityMapper;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/IdentityMapper;-><init>()V

    goto :goto_0
.end method

.method private getMessage(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Exception;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Copy;->createMapper()Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Copy;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method protected buildMap([Lorg/apache/tools/ant/types/Resource;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)Ljava/util/Map;
    .locals 12
    .param p1    # [Lorg/apache/tools/ant/types/Resource;
    .param p2    # Ljava/io/File;
    .param p3    # Lorg/apache/tools/ant/util/FileNameMapper;

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    const/4 v10, 0x0

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->forceOverwrite:Z

    if-eqz v0, :cond_2

    new-instance v11, Ljava/util/Vector;

    invoke-direct {v11}, Ljava/util/Vector;-><init>()V

    const/4 v6, 0x0

    :goto_0
    array-length v0, p1

    if-ge v6, v0, :cond_1

    aget-object v0, p1, v6

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    aget-object v0, p1, v6

    invoke-virtual {v11, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v10, v0, [Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v11, v10}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    :goto_1
    const/4 v6, 0x0

    :goto_2
    array-length v0, v10

    if-ge v6, v0, :cond_5

    aget-object v0, v10, v6

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->enableMultipleMappings:Z

    if-nez v0, :cond_3

    aget-object v0, v10, v6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/io/File;

    const/4 v4, 0x0

    aget-object v4, v9, v4

    invoke-direct {v3, p2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v8, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_2
    new-instance v3, Lorg/apache/tools/ant/taskdefs/Copy$1;

    invoke-direct {v3, p0, p2}, Lorg/apache/tools/ant/taskdefs/Copy$1;-><init>(Lorg/apache/tools/ant/taskdefs/Copy;Ljava/io/File;)V

    iget-wide v4, p0, Lorg/apache/tools/ant/taskdefs/Copy;->granularity:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lorg/apache/tools/ant/util/ResourceUtils;->selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;J)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v10

    goto :goto_1

    :cond_3
    const/4 v7, 0x0

    :goto_4
    array-length v0, v9

    if-ge v7, v0, :cond_4

    new-instance v0, Ljava/io/File;

    aget-object v1, v9, v7

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_4
    aget-object v0, v10, v6

    invoke-virtual {v8, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_5
    return-object v8
.end method

.method protected buildMap(Ljava/io/File;Ljava/io/File;[Ljava/lang/String;Lorg/apache/tools/ant/util/FileNameMapper;Ljava/util/Hashtable;)V
    .locals 14
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .param p3    # [Ljava/lang/String;
    .param p4    # Lorg/apache/tools/ant/util/FileNameMapper;
    .param p5    # Ljava/util/Hashtable;

    const/4 v12, 0x0

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->forceOverwrite:Z

    if-eqz v2, :cond_2

    new-instance v13, Ljava/util/Vector;

    invoke-direct {v13}, Ljava/util/Vector;-><init>()V

    const/4 v8, 0x0

    :goto_0
    move-object/from16 v0, p3

    array-length v2, v0

    if-ge v8, v2, :cond_1

    aget-object v2, p3, v8

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    aget-object v2, p3, v8

    invoke-virtual {v13, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    move-result v2

    new-array v12, v2, [Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    :goto_1
    const/4 v8, 0x0

    :goto_2
    array-length v2, v12

    if-ge v8, v2, :cond_5

    new-instance v11, Ljava/io/File;

    aget-object v2, v12, v8

    invoke-direct {v11, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    aget-object v2, v12, v8

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->enableMultipleMappings:Z

    if-nez v2, :cond_3

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/io/File;

    const/4 v6, 0x0

    aget-object v6, v10, v6

    move-object/from16 v0, p2

    invoke-direct {v5, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_2
    new-instance v1, Lorg/apache/tools/ant/util/SourceFileScanner;

    invoke-direct {v1, p0}, Lorg/apache/tools/ant/util/SourceFileScanner;-><init>(Lorg/apache/tools/ant/Task;)V

    iget-wide v6, p0, Lorg/apache/tools/ant/taskdefs/Copy;->granularity:J

    move-object/from16 v2, p3

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v7}, Lorg/apache/tools/ant/util/SourceFileScanner;->restrict([Ljava/lang/String;Ljava/io/File;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;J)[Ljava/lang/String;

    move-result-object v12

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    :goto_4
    array-length v2, v10

    if-ge v9, v2, :cond_4

    new-instance v2, Ljava/io/File;

    aget-object v3, v10, v9

    move-object/from16 v0, p2

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_4
    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v10}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_5
    return-void
.end method

.method public createFilterChain()Lorg/apache/tools/ant/types/FilterChain;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/FilterChain;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/FilterChain;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filterChains:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public createFilterSet()Lorg/apache/tools/ant/types/FilterSet;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/FilterSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/FilterSet;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filterSets:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0
.end method

.method public createMapper()Lorg/apache/tools/ant/types/Mapper;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot define more than one mapper"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    return-object v0
.end method

.method protected doFileOperations()V
    .locals 22

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->size()I

    move-result v2

    if-lez v2, :cond_7

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Copying "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->size()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " file"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->size()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    const-string v2, ""

    :goto_0
    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " to "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v15

    :cond_0
    invoke-interface {v15}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v15}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object/from16 v21, v2

    check-cast v21, [Ljava/lang/String;

    const/16 v17, 0x0

    :goto_1
    move-object/from16 v0, v21

    array-length v2, v0

    move/from16 v0, v17

    if-ge v0, v2, :cond_0

    aget-object v4, v21, v17

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Skipping self-copy of "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->verbosity:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    :goto_2
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    :cond_1
    const-string v2, "s"

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Copying "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " to "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->verbosity:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    new-instance v5, Lorg/apache/tools/ant/types/FilterSetCollection;

    invoke-direct {v5}, Lorg/apache/tools/ant/types/FilterSetCollection;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->filtering:Z

    if-eqz v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/Project;->getGlobalFilterSet()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v2

    invoke-virtual {v5, v2}, Lorg/apache/tools/ant/types/FilterSetCollection;->addFilterSet(Lorg/apache/tools/ant/types/FilterSet;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->filterSets:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v16

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface/range {v16 .. v16}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/FilterSet;

    invoke-virtual {v5, v2}, Lorg/apache/tools/ant/types/FilterSetCollection;->addFilterSet(Lorg/apache/tools/ant/types/FilterSet;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v18

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Failed to copy "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " to "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " due to "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Copy;->getDueTo(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " and I couldn\'t delete the corrupt "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    if-eqz v2, :cond_6

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v6

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v2, v0, v1, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileUtils:Lorg/apache/tools/ant/util/FileUtils;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->filterChains:Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lorg/apache/tools/ant/taskdefs/Copy;->forceOverwrite:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lorg/apache/tools/ant/taskdefs/Copy;->preserveLastModified:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/tools/ant/taskdefs/Copy;->inputEncoding:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/tools/ant/taskdefs/Copy;->outputEncoding:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    invoke-virtual/range {v2 .. v11}, Lorg/apache/tools/ant/util/FileUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;Lorg/apache/tools/ant/types/FilterSetCollection;Ljava/util/Vector;ZZLjava/lang/String;Ljava/lang/String;Lorg/apache/tools/ant/Project;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->includeEmpty:Z

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->dirCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v15

    const/4 v12, 0x0

    :cond_8
    invoke-interface {v15}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v15}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v14, v2

    check-cast v14, [Ljava/lang/String;

    const/16 v17, 0x0

    :goto_4
    array-length v2, v14

    move/from16 v0, v17

    if-ge v0, v2, :cond_8

    new-instance v13, Ljava/io/File;

    aget-object v2, v14, v17

    invoke-direct {v13, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v13}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_a

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Unable to create directory "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    :cond_9
    :goto_5
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    :cond_b
    if-lez v12, :cond_c

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Copied "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->dirCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->size()I

    move-result v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " empty director"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->dirCopyMap:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->size()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_d

    const-string v2, "y"

    :goto_6
    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " to "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " empty director"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/4 v2, 0x1

    if-ne v12, v2, :cond_e

    const-string v2, "y"

    :goto_7
    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v6, " under "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;)V

    :cond_c
    return-void

    :cond_d
    const-string v2, "ies"

    goto :goto_6

    :cond_e
    const-string v2, "ies"

    goto :goto_7
.end method

.method protected doResourceOperations(Ljava/util/Map;)V
    .locals 18
    .param p1    # Ljava/util/Map;

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->size()I

    move-result v2

    if-lez v2, :cond_6

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Copying "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, " resource"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_3

    const-string v2, ""

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/Resource;

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object/from16 v17, v2

    check-cast v17, [Ljava/lang/String;

    const/4 v11, 0x0

    :goto_1
    move-object/from16 v0, v17

    array-length v2, v0

    if-ge v11, v2, :cond_0

    aget-object v16, v17, v11

    :try_start_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Copying "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/tools/ant/taskdefs/Copy;->verbosity:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    new-instance v3, Lorg/apache/tools/ant/types/FilterSetCollection;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/FilterSetCollection;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->filtering:Z

    if-eqz v2, :cond_1

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/Project;->getGlobalFilterSet()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v2

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/types/FilterSetCollection;->addFilterSet(Lorg/apache/tools/ant/types/FilterSet;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->filterSets:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v10}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/FilterSet;

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/types/FilterSetCollection;->addFilterSet(Lorg/apache/tools/ant/types/FilterSet;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v12

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Failed to copy "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, " to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, " due to "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lorg/apache/tools/ant/taskdefs/Copy;->getDueTo(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/io/File;

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, " and I couldn\'t delete the corrupt "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    if-eqz v2, :cond_5

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v2, v14, v12, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v2

    :cond_3
    const-string v2, "s"

    goto/16 :goto_0

    :cond_4
    :try_start_1
    new-instance v2, Lorg/apache/tools/ant/types/resources/FileResource;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v2, v4, v0}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/tools/ant/taskdefs/Copy;->filterChains:Ljava/util/Vector;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/tools/ant/taskdefs/Copy;->forceOverwrite:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lorg/apache/tools/ant/taskdefs/Copy;->preserveLastModified:Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/tools/ant/taskdefs/Copy;->inputEncoding:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/tools/ant/taskdefs/Copy;->outputEncoding:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v9

    invoke-static/range {v1 .. v9}, Lorg/apache/tools/ant/util/ResourceUtils;->copyResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/FilterSetCollection;Ljava/util/Vector;ZZLjava/lang/String;Ljava/lang/String;Lorg/apache/tools/ant/Project;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    goto :goto_3

    :cond_6
    return-void
.end method

.method public execute()V
    .locals 37
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    move-object/from16 v26, v0

    const/16 v29, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v32, v0

    if-nez v32, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v32, v0

    if-eqz v32, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/Vector;->size()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lorg/apache/tools/ant/types/ResourceCollection;

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->validateAttributes()V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v32, v0

    if-eqz v32, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->exists()Z

    move-result v32

    if-eqz v32, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v32, v0

    if-nez v32, :cond_1

    new-instance v32, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v34

    invoke-direct/range {v32 .. v34}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->forceOverwrite:Z

    move/from16 v32, v0

    if-nez v32, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->exists()Z

    move-result v32

    if-eqz v32, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->lastModified()J

    move-result-wide v32

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->granularity:J

    move-wide/from16 v34, v0

    sub-long v32, v32, v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->lastModified()J

    move-result-wide v34

    cmp-long v32, v32, v34

    if-lez v32, :cond_6

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v36

    aput-object v36, v34, v35

    invoke-virtual/range {v32 .. v34}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_0
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    const/16 v16, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/Vector;->size()I

    move-result v32

    move/from16 v0, v16

    move/from16 v1, v32

    if-ge v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lorg/apache/tools/ant/types/ResourceCollection;

    move-object/from16 v0, v24

    instance-of v0, v0, Lorg/apache/tools/ant/types/FileSet;

    move/from16 v32, v0

    if-eqz v32, :cond_c

    invoke-interface/range {v24 .. v24}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v32

    if-eqz v32, :cond_c

    move-object/from16 v0, v24

    check-cast v0, Lorg/apache/tools/ant/types/FileSet;

    move-object v15, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v8, 0x0

    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v15, v0}, Lorg/apache/tools/ant/types/FileSet;->getDirectoryScanner(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v15, v0}, Lorg/apache/tools/ant/types/FileSet;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v8}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v31

    invoke-virtual {v8}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->flatten:Z

    move/from16 v32, v0

    if-nez v32, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    move-object/from16 v32, v0

    if-nez v32, :cond_4

    invoke-virtual {v8}, Lorg/apache/tools/ant/DirectoryScanner;->isEverythingIncluded()Z

    move-result v32

    if-eqz v32, :cond_4

    invoke-virtual {v15}, Lorg/apache/tools/ant/types/FileSet;->hasPatterns()Z

    move-result v32

    if-nez v32, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->completeDirMap:Ljava/util/Hashtable;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    move-object/from16 v33, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v0, v14, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    move-object/from16 v0, v31

    invoke-static {v14, v0, v12}, Lorg/apache/tools/ant/taskdefs/Copy;->add(Ljava/io/File;[Ljava/lang/String;Ljava/util/Map;)V

    move-object/from16 v0, v30

    invoke-static {v14, v0, v7}, Lorg/apache/tools/ant/taskdefs/Copy;->add(Ljava/io/File;[Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v5, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    :cond_6
    new-instance v32, Ljava/lang/StringBuffer;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v32

    const-string v33, " omitted as "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v32

    const-string v33, " is up to date."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v32

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    if-eqz v29, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    move-object/from16 v0, v33

    move-object/from16 v1, v29

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Ljava/util/Hashtable;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->dirCopyMap:Ljava/util/Hashtable;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Ljava/util/Hashtable;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->completeDirMap:Ljava/util/Hashtable;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Ljava/util/Hashtable;->clear()V

    throw v32

    :cond_8
    :try_start_3
    new-instance v32, Ljava/lang/StringBuffer;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuffer;-><init>()V

    const-string v33, "Warning: Could not find file "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    const-string v33, " to copy."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    move/from16 v32, v0

    if-nez v32, :cond_9

    const/16 v32, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_9
    new-instance v32, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v32

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v32

    :catch_0
    move-exception v9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    move/from16 v32, v0

    if-nez v32, :cond_a

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/tools/ant/taskdefs/Copy;->getMessage(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v32

    const-string v33, " not found."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v32

    if-nez v32, :cond_b

    :cond_a
    throw v9

    :cond_b
    new-instance v32, Ljava/lang/StringBuffer;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuffer;-><init>()V

    const-string v33, "Warning: "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/tools/ant/taskdefs/Copy;->getMessage(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    goto/16 :goto_2

    :cond_c
    invoke-interface/range {v24 .. v24}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v32

    if-nez v32, :cond_d

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->supportsNonFileResources()Z

    move-result v32

    if-nez v32, :cond_d

    new-instance v32, Lorg/apache/tools/ant/BuildException;

    const-string v33, "Only FileSystem resources are supported."

    invoke-direct/range {v32 .. v33}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v32

    :cond_d
    invoke-interface/range {v24 .. v24}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_e
    :goto_3
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_5

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual/range {v23 .. v23}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v32

    if-eqz v32, :cond_e

    sget-object v4, Lorg/apache/tools/ant/taskdefs/Copy;->NULL_FILE_PLACEHOLDER:Ljava/io/File;

    invoke-virtual/range {v23 .. v23}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v23

    instance-of v0, v0, Lorg/apache/tools/ant/types/resources/FileResource;

    move/from16 v32, v0

    if-eqz v32, :cond_f

    move-object/from16 v0, v23

    check-cast v0, Lorg/apache/tools/ant/types/resources/FileResource;

    move-object v13, v0

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lorg/apache/tools/ant/taskdefs/Copy;->getKeyFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v32

    if-nez v32, :cond_f

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    :cond_f
    invoke-virtual/range {v23 .. v23}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v32

    if-nez v32, :cond_10

    move-object/from16 v0, v23

    instance-of v0, v0, Lorg/apache/tools/ant/types/resources/FileResource;

    move/from16 v32, v0

    if-eqz v32, :cond_12

    :cond_10
    invoke-virtual/range {v23 .. v23}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v32

    if-eqz v32, :cond_11

    move-object/from16 v32, v7

    :goto_4
    move-object/from16 v0, v20

    move-object/from16 v1, v32

    invoke-static {v4, v0, v1}, Lorg/apache/tools/ant/taskdefs/Copy;->add(Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_11
    move-object/from16 v32, v12

    goto :goto_4

    :cond_12
    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_13
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_5
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v32

    if-eqz v32, :cond_17

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/io/File;

    invoke-virtual {v12, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-virtual {v7, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    const/16 v32, 0x0

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v31, v0

    if-eqz v11, :cond_14

    move-object/from16 v0, v31

    invoke-interface {v11, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v32

    check-cast v32, [Ljava/lang/String;

    move-object/from16 v0, v32

    check-cast v0, [Ljava/lang/String;

    move-object/from16 v31, v0

    :cond_14
    const/16 v32, 0x0

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v30, v0

    if-eqz v6, :cond_15

    move-object/from16 v0, v30

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v32

    check-cast v32, [Ljava/lang/String;

    move-object/from16 v0, v32

    check-cast v0, [Ljava/lang/String;

    move-object/from16 v30, v0

    :cond_15
    sget-object v32, Lorg/apache/tools/ant/taskdefs/Copy;->NULL_FILE_PLACEHOLDER:Ljava/io/File;

    move-object/from16 v0, v32

    if-ne v10, v0, :cond_16

    const/4 v10, 0x0

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, v31

    move-object/from16 v3, v30

    invoke-virtual {v0, v10, v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/Copy;->scan(Ljava/io/File;Ljava/io/File;[Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    :cond_17
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Copy;->doFileOperations()V
    :try_end_4
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_6
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v32

    if-lez v32, :cond_18

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v32

    move/from16 v0, v32

    new-array v0, v0, [Lorg/apache/tools/ant/types/Resource;

    move-object/from16 v32, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v32

    check-cast v32, [Lorg/apache/tools/ant/types/Resource;

    move-object/from16 v0, v32

    check-cast v0, [Lorg/apache/tools/ant/types/Resource;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->scan([Lorg/apache/tools/ant/types/Resource;Ljava/io/File;)Ljava/util/Map;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v18

    :try_start_6
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Copy;->doResourceOperations(Ljava/util/Map;)V
    :try_end_6
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_18
    :goto_7
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    if-eqz v29, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/Hashtable;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->dirCopyMap:Ljava/util/Hashtable;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/Hashtable;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->completeDirMap:Ljava/util/Hashtable;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/Hashtable;->clear()V

    return-void

    :catch_1
    move-exception v9

    :try_start_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    move/from16 v32, v0

    if-nez v32, :cond_1a

    new-instance v32, Ljava/lang/StringBuffer;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuffer;-><init>()V

    const-string v33, "Warning: "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/tools/ant/taskdefs/Copy;->getMessage(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    goto/16 :goto_6

    :cond_1a
    throw v9

    :catch_2
    move-exception v9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    move/from16 v32, v0

    if-nez v32, :cond_1b

    new-instance v32, Ljava/lang/StringBuffer;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuffer;-><init>()V

    const-string v33, "Warning: "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/tools/ant/taskdefs/Copy;->getMessage(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Copy;->log(Ljava/lang/String;I)V

    goto/16 :goto_7

    :cond_1b
    throw v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->inputEncoding:Ljava/lang/String;

    return-object v0
.end method

.method protected getFileUtils()Lorg/apache/tools/ant/util/FileUtils;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->fileUtils:Lorg/apache/tools/ant/util/FileUtils;

    return-object v0
.end method

.method protected getFilterChains()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filterChains:Ljava/util/Vector;

    return-object v0
.end method

.method protected getFilterSets()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filterSets:Ljava/util/Vector;

    return-object v0
.end method

.method public getOutputEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->outputEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getPreserveLastModified()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->preserveLastModified:Z

    return v0
.end method

.method public isEnableMultipleMapping()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->enableMultipleMappings:Z

    return v0
.end method

.method protected scan([Lorg/apache/tools/ant/types/Resource;Ljava/io/File;)Ljava/util/Map;
    .locals 1
    .param p1    # [Lorg/apache/tools/ant/types/Resource;
    .param p2    # Ljava/io/File;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getMapper()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/tools/ant/taskdefs/Copy;->buildMap([Lorg/apache/tools/ant/types/Resource;Ljava/io/File;Lorg/apache/tools/ant/util/FileNameMapper;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected scan(Ljava/io/File;Ljava/io/File;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .param p3    # [Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Copy;->getMapper()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Copy;->fileCopyMap:Ljava/util/Hashtable;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/taskdefs/Copy;->buildMap(Ljava/io/File;Ljava/io/File;[Ljava/lang/String;Lorg/apache/tools/ant/util/FileNameMapper;Ljava/util/Hashtable;)V

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->includeEmpty:Z

    if-eqz v0, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Copy;->dirCopyMap:Ljava/util/Hashtable;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/taskdefs/Copy;->buildMap(Ljava/io/File;Ljava/io/File;[Ljava/lang/String;Lorg/apache/tools/ant/util/FileNameMapper;Ljava/util/Hashtable;)V

    :cond_0
    return-void
.end method

.method public setEnableMultipleMappings(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->enableMultipleMappings:Z

    return-void
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->inputEncoding:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->outputEncoding:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->outputEncoding:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setFailOnError(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->failonerror:Z

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    return-void
.end method

.method public setFiltering(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->filtering:Z

    return-void
.end method

.method public setFlatten(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->flatten:Z

    return-void
.end method

.method public setGranularity(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->granularity:J

    return-void
.end method

.method public setIncludeEmptyDirs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->includeEmpty:Z

    return-void
.end method

.method public setOutputEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->outputEncoding:Ljava/lang/String;

    return-void
.end method

.method public setOverwrite(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->forceOverwrite:Z

    return-void
.end method

.method public setPreserveLastModified(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Copy;->setPreserveLastModified(Z)V

    return-void
.end method

.method public setPreserveLastModified(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->preserveLastModified:Z

    return-void
.end method

.method public setTodir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    return-void
.end method

.method public setTofile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    return-void
.end method

.method public setVerbose(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Copy;->verbosity:I

    return-void

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method protected supportsNonFileResources()Z
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Copy;->class$org$apache$tools$ant$taskdefs$Copy:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.taskdefs.Copy"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Copy;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/Copy;->class$org$apache$tools$ant$taskdefs$Copy:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/taskdefs/Copy;->class$org$apache$tools$ant$taskdefs$Copy:Ljava/lang/Class;

    goto :goto_0
.end method

.method protected validateAttributes()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Specify at least one source--a file or a resource collection."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Only one of tofile and todir may be set."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    if-nez v2, :cond_2

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "One of tofile or todir must be set."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Use a resource collection to copy directories."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_7

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-le v2, v4, :cond_4

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Cannot concatenate multiple files into a single file."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->isFilesystemOnly()Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Only FileSystem resources are supported when concatenating files."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Cannot perform operation from directory to file."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v2

    if-ne v2, v4, :cond_a

    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/resources/FileResource;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    if-nez v2, :cond_9

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->file:Ljava/io/File;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->rcs:Ljava/util/Vector;

    invoke-virtual {v2, v3}, Ljava/util/Vector;->removeElementAt(I)V

    :cond_7
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Copy;->destDir:Ljava/io/File;

    :cond_8
    return-void

    :cond_9
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Cannot concatenate multiple files into a single file."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_a
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Cannot concatenate multiple files into a single file."

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
