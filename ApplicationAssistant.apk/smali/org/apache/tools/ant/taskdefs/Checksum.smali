.class public Lorg/apache/tools/ant/taskdefs/Checksum;
.super Lorg/apache/tools/ant/taskdefs/MatchingTask;
.source "Checksum.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/condition/Condition;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Checksum$FormatElement;,
        Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;
    }
.end annotation


# instance fields
.field private algorithm:Ljava/lang/String;

.field private allDigests:Ljava/util/Map;

.field private file:Ljava/io/File;

.field private fileext:Ljava/lang/String;

.field private forceOverwrite:Z

.field private format:Ljava/text/MessageFormat;

.field private includeFileMap:Ljava/util/Hashtable;

.field private isCondition:Z

.field private messageDigest:Ljava/security/MessageDigest;

.field private property:Ljava/lang/String;

.field private provider:Ljava/lang/String;

.field private readBufferSize:I

.field private relativeFilePaths:Ljava/util/Map;

.field private resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

.field private todir:Ljava/io/File;

.field private totalproperty:Ljava/lang/String;

.field private verifyProperty:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MatchingTask;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    const-string v0, "MD5"

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->algorithm:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->provider:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->allDigests:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->relativeFilePaths:Ljava/util/Map;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->includeFileMap:Ljava/util/Hashtable;

    const/16 v0, 0x2000

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->readBufferSize:I

    invoke-static {}, Lorg/apache/tools/ant/taskdefs/Checksum$FormatElement;->getDefault()Lorg/apache/tools/ant/taskdefs/Checksum$FormatElement;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Checksum$FormatElement;->getFormat()Ljava/text/MessageFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->format:Ljava/text/MessageFormat;

    return-void
.end method

.method private addToIncludeFileMap(Ljava/io/File;)V
    .locals 8
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->property:Ljava/lang/String;

    if-nez v4, :cond_3

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Checksum;->getChecksumFile(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->forceOverwrite:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    if-nez v4, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    :cond_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->includeFileMap:Ljava/util/Hashtable;

    invoke-virtual {v4, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " omitted as "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " is up to date."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/Checksum;->log(Ljava/lang/String;I)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/taskdefs/Checksum;->readChecksum(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-static {v4}, Lorg/apache/tools/ant/taskdefs/Checksum;->decodeHex([C)[B

    move-result-object v2

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->allDigests:Ljava/util/Map;

    invoke-interface {v4, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->includeFileMap:Ljava/util/Hashtable;

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->property:Ljava/lang/String;

    invoke-virtual {v4, p1, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Could not find file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " to generate checksum for."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/taskdefs/Checksum;->log(Ljava/lang/String;)V

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v4
.end method

.method private createDigestString([B)Ljava/lang/String;
    .locals 5
    .param p1    # [B

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_1

    aget-byte v3, p1, v2

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static decodeHex([C)[B
    .locals 8
    .param p0    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/16 v7, 0x10

    array-length v4, p0

    and-int/lit8 v6, v4, 0x1

    if-eqz v6, :cond_0

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "odd number of characters."

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    shr-int/lit8 v6, v4, 0x1

    new-array v5, v6, [B

    const/4 v1, 0x0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    add-int/lit8 v2, v3, 0x1

    aget-char v6, p0, v3

    invoke-static {v6, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    shl-int/lit8 v0, v6, 0x4

    add-int/lit8 v3, v2, 0x1

    aget-char v6, p0, v2

    invoke-static {v6, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    or-int/2addr v0, v6

    and-int/lit16 v6, v0, 0xff

    int-to-byte v6, v6

    aput-byte v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v5
.end method

.method private generateChecksums()Z
    .locals 30
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v15, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->readBufferSize:I

    move/from16 v26, v0

    move/from16 v0, v26

    new-array v4, v0, [B

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->includeFileMap:Ljava/util/Hashtable;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    move-object/from16 v18, v17

    move-object/from16 v16, v15

    :goto_0
    :try_start_1
    invoke-interface {v11}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v26

    if-eqz v26, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/security/MessageDigest;->reset()V

    invoke-interface {v11}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/io/File;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    move/from16 v26, v0

    if-nez v26, :cond_0

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Calculating "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->algorithm:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " checksum for "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Checksum;->log(Ljava/lang/String;I)V

    :cond_0
    new-instance v15, Ljava/io/FileInputStream;

    move-object/from16 v0, v23

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v10, Ljava/security/DigestInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-direct {v10, v15, v0}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V

    :cond_1
    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->readBufferSize:I

    move/from16 v27, v0

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v10, v4, v0, v1}, Ljava/security/DigestInputStream;->read([BII)I

    move-result v26

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1

    invoke-virtual {v10}, Ljava/security/DigestInputStream;->close()V

    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->allDigests:Ljava/util/Map;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-interface {v0, v1, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/tools/ant/taskdefs/Checksum;->createDigestString([B)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->includeFileMap:Ljava/util/Hashtable;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    instance-of v0, v8, Ljava/lang/String;

    move/from16 v26, v0

    if-eqz v26, :cond_5

    move-object v0, v8

    check-cast v0, Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    move/from16 v26, v0

    if-eqz v26, :cond_4

    if-eqz v6, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->property:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    const/4 v6, 0x1

    :goto_1
    move-object/from16 v17, v18

    :goto_2
    move-object/from16 v18, v17

    move-object/from16 v16, v15

    goto/16 :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v5}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    :catch_0
    move-exception v11

    move-object/from16 v17, v18

    :goto_3
    :try_start_3
    new-instance v26, Lorg/apache/tools/ant/BuildException;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v0, v11, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v26
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v26

    :goto_4
    invoke-static {v15}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    throw v26

    :cond_5
    :try_start_4
    instance-of v0, v8, Ljava/io/File;

    move/from16 v26, v0

    if-eqz v26, :cond_c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    move/from16 v26, v0

    if-eqz v26, :cond_8

    move-object v0, v8

    check-cast v0, Ljava/io/File;

    move-object v12, v0

    invoke-virtual {v12}, Ljava/io/File;->exists()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v26

    if-eqz v26, :cond_7

    :try_start_5
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lorg/apache/tools/ant/taskdefs/Checksum;->readChecksum(Ljava/io/File;)Ljava/lang/String;

    move-result-object v24

    if-eqz v6, :cond_6

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v26

    if-eqz v26, :cond_6

    const/4 v6, 0x1

    :goto_5
    move-object/from16 v17, v18

    goto :goto_2

    :cond_6
    const/4 v6, 0x0

    goto :goto_5

    :catch_1
    move-exception v3

    const/4 v6, 0x0

    goto :goto_5

    :cond_7
    const/4 v6, 0x0

    goto :goto_5

    :cond_8
    :try_start_6
    move-object v0, v8

    check-cast v0, Ljava/io/File;

    move-object v7, v0

    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->format:Ljava/text/MessageFormat;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v5, v27, v28

    const/16 v28, 0x1

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-virtual/range {v26 .. v27}, Ljava/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->getBytes()[B

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    sget-object v26, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->getBytes()[B

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const/16 v17, 0x0

    goto/16 :goto_2

    :cond_9
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->allDigests:Ljava/util/Map;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/security/MessageDigest;->reset()V

    const/16 v19, 0x0

    :goto_6
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v19

    move/from16 v1, v26

    if-ge v0, v1, :cond_a

    aget-object v23, v20, v19

    check-cast v23, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->allDigests:Ljava/util/Map;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, [B

    move-object/from16 v0, v26

    check-cast v0, [B

    move-object v9, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Ljava/security/MessageDigest;->update([B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->relativeFilePaths:Ljava/util/Map;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    move-object/from16 v26, v0

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/security/MessageDigest;->update([B)V

    add-int/lit8 v19, v19, 0x1

    goto :goto_6

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Checksum;->createDigestString([B)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_b
    invoke-static/range {v16 .. v16}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static/range {v18 .. v18}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    return v6

    :catchall_1
    move-exception v26

    move-object/from16 v17, v18

    move-object/from16 v15, v16

    goto/16 :goto_4

    :catchall_2
    move-exception v26

    move-object/from16 v17, v18

    goto/16 :goto_4

    :catch_2
    move-exception v11

    goto/16 :goto_3

    :catch_3
    move-exception v11

    move-object/from16 v17, v18

    move-object/from16 v15, v16

    goto/16 :goto_3

    :cond_c
    move-object/from16 v17, v18

    goto/16 :goto_2
.end method

.method private getChecksumFile(Ljava/io/File;)Ljava/io/File;
    .locals 6
    .param p1    # Ljava/io/File;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->todir:Ljava/io/File;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->relativeFilePaths:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Internal error: relativeFilePaths could not match file"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "please file a bug report on this"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->todir:Ljava/io/File;

    invoke-direct {v3, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :goto_0
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    goto :goto_0
.end method

.method private readChecksum(Ljava/io/File;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/io/File;

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->format:Ljava/text/MessageFormat;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/MessageFormat;->parse(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v4, v3

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    aget-object v4, v3, v4

    if-nez v4, :cond_1

    :cond_0
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "failed to find a checksum"

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catch_0
    move-exception v2

    move-object v0, v1

    :goto_0
    :try_start_2
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Couldn\'t read checksum file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v4

    :goto_1
    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    throw v4

    :cond_1
    const/4 v4, 0x0

    :try_start_3
    aget-object v4, v3, v4

    check-cast v4, Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    return-object v4

    :catch_1
    move-exception v2

    :goto_2
    :try_start_4
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Couldn\'t read checksum file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_1
    move-exception v4

    move-object v0, v1

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v0, v1

    goto :goto_2

    :catch_3
    move-exception v2

    goto :goto_0
.end method

.method private validateAndExecute()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v8, 0x1

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    if-nez v7, :cond_1

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;->size()I

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Specify at least one source - a file or a resource collection."

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;->isFilesystemOnly()Z

    move-result v7

    if-nez v7, :cond_2

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Can only calculate checksums for file-based resources."

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_3

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Checksum cannot be generated for directories"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_3
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    if-eqz v7, :cond_4

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "File and Totalproperty cannot co-exist."

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_4
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->property:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    if-eqz v7, :cond_5

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Property and FileExt cannot co-exist."

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_5
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->property:Ljava/lang/String;

    if-eqz v7, :cond_9

    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->forceOverwrite:Z

    if-eqz v7, :cond_6

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "ForceOverwrite cannot be used when Property is specified"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_6
    const/4 v0, 0x0

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;->size()I

    move-result v7

    add-int/2addr v0, v7

    :cond_7
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    if-eqz v7, :cond_8

    add-int/lit8 v0, v0, 0x1

    :cond_8
    if-le v0, v8, :cond_9

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Multiple files cannot be used when Property is specified"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_9
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->verifyProperty:Ljava/lang/String;

    if-eqz v7, :cond_a

    iput-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    :cond_a
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->verifyProperty:Ljava/lang/String;

    if-eqz v7, :cond_b

    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->forceOverwrite:Z

    if-eqz v7, :cond_b

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "VerifyProperty and ForceOverwrite cannot co-exist."

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_b
    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    if-eqz v7, :cond_c

    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->forceOverwrite:Z

    if-eqz v7, :cond_c

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "ForceOverwrite cannot be used when conditions are being used."

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_c
    const/4 v7, 0x0

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->provider:Ljava/lang/String;

    if-eqz v7, :cond_d

    :try_start_0
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->algorithm:Ljava/lang/String;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->provider:Ljava/lang/String;

    invoke-static {v7, v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;

    if-nez v7, :cond_e

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Unable to create Message Digest"

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v7

    :catch_0
    move-exception v3

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v7, v3, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7

    :catch_1
    move-exception v4

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v7, v4, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7

    :cond_d
    :try_start_1
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->algorithm:Ljava/lang/String;

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->messageDigest:Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_2
    move-exception v3

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-direct {v7, v3, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v7

    :cond_e
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    if-nez v7, :cond_12

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->algorithm:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    :cond_f
    :try_start_2
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    if-eqz v7, :cond_13

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    if-nez v7, :cond_10

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->todir:Ljava/io/File;

    if-eqz v7, :cond_11

    :cond_10
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->relativeFilePaths:Ljava/util/Map;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/FileResource;->getName()Ljava/lang/String;

    move-result-object v8

    sget-char v9, Ljava/io/File;->separatorChar:C

    const/16 v10, 0x2f

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_11
    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/Checksum;->addToIncludeFileMap(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v7

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->includeFileMap:Ljava/util/Hashtable;

    invoke-virtual {v8}, Ljava/util/Hashtable;->clear()V

    throw v7

    :cond_12
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_f

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "File extension when specified must not be an empty string"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_13
    :try_start_3
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    if-eqz v7, :cond_16

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    if-nez v7, :cond_14

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->todir:Ljava/io/File;

    if-eqz v7, :cond_15

    :cond_14
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->relativeFilePaths:Ljava/util/Map;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    iget-object v9, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    sget-char v10, Ljava/io/File;->separatorChar:C

    const/16 v11, 0x2f

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_15
    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    invoke-direct {p0, v7}, Lorg/apache/tools/ant/taskdefs/Checksum;->addToIncludeFileMap(Ljava/io/File;)V

    :cond_16
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->generateChecksums()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v7

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->includeFileMap:Ljava/util/Hashtable;

    invoke-virtual {v8}, Ljava/util/Hashtable;->clear()V

    return v7
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;-><init>()V

    :goto_1
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->resources:Lorg/apache/tools/ant/taskdefs/Checksum$FileUnion;

    goto :goto_1
.end method

.method public addFileset(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Checksum;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method public eval()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->validateAndExecute()Z

    move-result v0

    return v0
.end method

.method public execute()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->isCondition:Z

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->validateAndExecute()Z

    move-result v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->verifyProperty:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Checksum;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->verifyProperty:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v1}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public setAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->algorithm:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->file:Ljava/io/File;

    return-void
.end method

.method public setFileext(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->fileext:Ljava/lang/String;

    return-void
.end method

.method public setForceOverwrite(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->forceOverwrite:Z

    return-void
.end method

.method public setFormat(Lorg/apache/tools/ant/taskdefs/Checksum$FormatElement;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Checksum$FormatElement;

    invoke-virtual {p1}, Lorg/apache/tools/ant/taskdefs/Checksum$FormatElement;->getFormat()Ljava/text/MessageFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->format:Ljava/text/MessageFormat;

    return-void
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/text/MessageFormat;

    invoke-direct {v0, p1}, Ljava/text/MessageFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->format:Ljava/text/MessageFormat;

    return-void
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->property:Ljava/lang/String;

    return-void
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->provider:Ljava/lang/String;

    return-void
.end method

.method public setReadBufferSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->readBufferSize:I

    return-void
.end method

.method public setTodir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->todir:Ljava/io/File;

    return-void
.end method

.method public setTotalproperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->totalproperty:Ljava/lang/String;

    return-void
.end method

.method public setVerifyproperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Checksum;->verifyProperty:Ljava/lang/String;

    return-void
.end method
