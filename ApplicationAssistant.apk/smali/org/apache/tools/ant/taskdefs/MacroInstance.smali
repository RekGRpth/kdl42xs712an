.class public Lorg/apache/tools/ant/taskdefs/MacroInstance;
.super Lorg/apache/tools/ant/Task;
.source "MacroInstance.java"

# interfaces
.implements Lorg/apache/tools/ant/DynamicAttribute;
.implements Lorg/apache/tools/ant/TaskContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/MacroInstance$Element;
    }
.end annotation


# static fields
.field private static final STATE_EXPECT_BRACKET:I = 0x1

.field private static final STATE_EXPECT_NAME:I = 0x2

.field private static final STATE_NORMAL:I


# instance fields
.field private implicitTag:Ljava/lang/String;

.field private localAttributes:Ljava/util/Hashtable;

.field private macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

.field private map:Ljava/util/Map;

.field private nsElements:Ljava/util/Map;

.field private presentElements:Ljava/util/Map;

.field private text:Ljava/lang/String;

.field private unknownElements:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->map:Ljava/util/Map;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->nsElements:Ljava/util/Map;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->implicitTag:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->unknownElements:Ljava/util/List;

    return-void
.end method

.method private copy(Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/UnknownElement;
    .locals 21
    .param p1    # Lorg/apache/tools/ant/UnknownElement;

    new-instance v13, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v13, v0}, Lorg/apache/tools/ant/UnknownElement;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getNamespace()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/UnknownElement;->setNamespace(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/UnknownElement;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getQName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/UnknownElement;->setQName(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getTaskType()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/UnknownElement;->setTaskType(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getTaskName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/UnknownElement;->setTaskName(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getBackTrace()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v18

    :goto_0
    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/UnknownElement;->setLocation(Lorg/apache/tools/ant/Location;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v18

    if-nez v18, :cond_1

    new-instance v14, Lorg/apache/tools/ant/Target;

    invoke-direct {v14}, Lorg/apache/tools/ant/Target;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/apache/tools/ant/Target;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v13, v14}, Lorg/apache/tools/ant/UnknownElement;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    :goto_1
    new-instance v12, Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getTaskName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v12, v13, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/RuntimeConfigurable;->getPolyType()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->setPolyType(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/RuntimeConfigurable;->getAttributeMap()Ljava/util/Hashtable;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroSubs(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Lorg/apache/tools/ant/RuntimeConfigurable;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v18

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/UnknownElement;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    goto :goto_1

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/RuntimeConfigurable;->getText()Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroSubs(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->addText(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/RuntimeConfigurable;->getChildren()Ljava/util/Enumeration;

    move-result-object v4

    :cond_3
    :goto_3
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v18

    if-eqz v18, :cond_a

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {v11}, Lorg/apache/tools/ant/RuntimeConfigurable;->getProxy()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/UnknownElement;->getTaskType()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_4

    sget-object v18, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    :cond_4
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getNsElements()Ljava/util/Map;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v0, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;

    if-nez v16, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->copy(Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/UnknownElement;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->addChild(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {v13, v3}, Lorg/apache/tools/ant/UnknownElement;->addChild(Lorg/apache/tools/ant/UnknownElement;)V

    goto :goto_3

    :cond_5
    invoke-virtual/range {v16 .. v16}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->isImplicit()Z

    move-result v18

    if-eqz v18, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->unknownElements:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    if-nez v18, :cond_6

    invoke-virtual/range {v16 .. v16}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->isOptional()Z

    move-result v18

    if-nez v18, :cond_6

    new-instance v18, Lorg/apache/tools/ant/BuildException;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v20, "Missing nested elements for implicit element "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v16 .. v16}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->unknownElements:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/tools/ant/UnknownElement;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->copy(Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/UnknownElement;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->addChild(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {v13, v3}, Lorg/apache/tools/ant/UnknownElement;->addChild(Lorg/apache/tools/ant/UnknownElement;)V

    goto :goto_4

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->presentElements:Ljava/util/Map;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/UnknownElement;

    if-nez v9, :cond_8

    invoke-virtual/range {v16 .. v16}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->isOptional()Z

    move-result v18

    if-nez v18, :cond_3

    new-instance v18, Lorg/apache/tools/ant/BuildException;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string v20, "Required nested element "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v16 .. v16}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    const-string v20, " missing"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_8
    invoke-virtual {v9}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/apache/tools/ant/RuntimeConfigurable;->getText()Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v10, v1}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroSubs(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->addText(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v9}, Lorg/apache/tools/ant/UnknownElement;->getChildren()Ljava/util/List;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/tools/ant/UnknownElement;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->copy(Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/UnknownElement;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->addChild(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {v13, v3}, Lorg/apache/tools/ant/UnknownElement;->addChild(Lorg/apache/tools/ant/UnknownElement;)V

    goto :goto_5

    :cond_a
    return-object v13
.end method

.method private getNsElements()Ljava/util/Map;
    .locals 6

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->nsElements:Ljava/util/Map;

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->nsElements:Ljava/util/Map;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getElements()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->nsElements:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->isImplicit()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/MacroDef$TemplateElement;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->implicitTag:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->nsElements:Ljava/util/Map;

    return-object v3
.end method

.method private macroSubs(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Map;

    const/16 v9, 0x40

    if-nez p1, :cond_0

    const/4 v7, 0x0

    :goto_0
    return-object v7

    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_6

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    packed-switch v5, :pswitch_data_0

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :pswitch_0
    if-ne v0, v9, :cond_1

    const/4 v5, 0x1

    goto :goto_2

    :cond_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :pswitch_1
    const/16 v7, 0x7b

    if-ne v0, v7, :cond_2

    const/4 v5, 0x2

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    goto :goto_2

    :cond_2
    if-ne v0, v9, :cond_3

    const/4 v5, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :pswitch_2
    const/16 v7, 0x7d

    if-ne v0, v7, :cond_5

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-nez v6, :cond_4

    const-string v7, "@{"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v7, "}"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_6
    packed-switch v5, :pswitch_data_1

    :goto_4
    :pswitch_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :pswitch_4
    invoke-virtual {v4, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    :pswitch_5
    const-string v7, "@{"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private processTasks()V
    .locals 6

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->implicitTag:Ljava/lang/String;

    if-eqz v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->unknownElements:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v2}, Lorg/apache/tools/ant/UnknownElement;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/tools/ant/ProjectHelper;->extractNameFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getNsElements()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "unsupported element "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->presentElements:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Element "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " already present"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->presentElements:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public addTask(Lorg/apache/tools/ant/Task;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Task;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->unknownElements:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addText(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    return-void
.end method

.method public createDynamicElement(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Not implemented any more"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public execute()V
    .locals 10

    const/4 v9, 0x0

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->presentElements:Ljava/util/Map;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getNsElements()Ljava/util/Map;

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->processTasks()V

    new-instance v6, Ljava/util/Hashtable;

    invoke-direct {v6}, Ljava/util/Hashtable;-><init>()V

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    new-instance v2, Ljava/util/HashSet;

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->map:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getAttributes()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->map:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-nez v5, :cond_0

    const-string v6, "description"

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getDescription()Ljava/lang/String;

    move-result-object v5

    :cond_0
    if-nez v5, :cond_1

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getDefault()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    invoke-direct {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroSubs(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v5

    :cond_1
    if-nez v5, :cond_2

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "required attribute "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " not set"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_2
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/MacroDef$Attribute;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string v6, "id"

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "id"

    invoke-interface {v2, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_4
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getText()Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    move-result-object v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    if-nez v6, :cond_6

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getText()Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->getOptional()Z

    move-result v6

    if-nez v6, :cond_5

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "required text missing"

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_5
    const-string v6, ""

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    :cond_6
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getText()Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->getTrim()Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    :cond_7
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getText()Lorg/apache/tools/ant/taskdefs/MacroDef$Text;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/tools/ant/taskdefs/MacroDef$Text;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v6

    if-eqz v6, :cond_b

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Unknown attribute"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v6

    const/4 v9, 0x1

    if-le v6, v9, :cond_a

    const-string v6, "s "

    :goto_1
    invoke-virtual {v8, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_9
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->text:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "The \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getTaskName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "\" macro does not support"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " nested text data."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_a
    const-string v6, " "

    goto :goto_1

    :cond_b
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getNestedTask()Lorg/apache/tools/ant/UnknownElement;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->copy(Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/UnknownElement;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/UnknownElement;->init()V

    :try_start_0
    invoke-virtual {v1}, Lorg/apache/tools/ant/UnknownElement;->perform()V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->presentElements:Ljava/util/Map;

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    return-void

    :catch_0
    move-exception v3

    :try_start_1
    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    invoke-virtual {v6}, Lorg/apache/tools/ant/taskdefs/MacroDef;->getBackTrace()Z

    move-result v6

    if-eqz v6, :cond_c

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v6

    invoke-static {v3, v6}, Lorg/apache/tools/ant/ProjectHelper;->addLocationToBuildException(Lorg/apache/tools/ant/BuildException;Lorg/apache/tools/ant/Location;)Lorg/apache/tools/ant/BuildException;

    move-result-object v6

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v6

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->presentElements:Ljava/util/Map;

    iput-object v9, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->localAttributes:Ljava/util/Hashtable;

    throw v6

    :cond_c
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/MacroInstance;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v6

    invoke-virtual {v3, v6}, Lorg/apache/tools/ant/BuildException;->setLocation(Lorg/apache/tools/ant/Location;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getMacroDef()Lorg/apache/tools/ant/taskdefs/MacroDef;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    return-object v0
.end method

.method public setDynamicAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->map:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setMacroDef(Lorg/apache/tools/ant/taskdefs/MacroDef;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/MacroDef;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/MacroInstance;->macroDef:Lorg/apache/tools/ant/taskdefs/MacroDef;

    return-void
.end method
