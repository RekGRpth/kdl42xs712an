.class public Lorg/apache/tools/ant/taskdefs/rmic/ForkingSunRmic;
.super Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;
.source "ForkingSunRmic.java"


# static fields
.field public static final COMPILER_NAME:Ljava/lang/String; = "forking"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/rmic/DefaultRmicAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v6, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/ForkingSunRmic;->getRmic()Lorg/apache/tools/ant/taskdefs/Rmic;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/ForkingSunRmic;->setupRmicCommand()Lorg/apache/tools/ant/types/Commandline;

    move-result-object v1

    invoke-virtual {v4}, Lorg/apache/tools/ant/taskdefs/Rmic;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/ForkingSunRmic;->getExecutableName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJdkExecutable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lorg/apache/tools/ant/types/Commandline;->setExecutable(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Commandline;->getCommandline()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v3, Lorg/apache/tools/ant/taskdefs/Execute;

    new-instance v7, Lorg/apache/tools/ant/taskdefs/LogStreamHandler;

    const/4 v8, 0x2

    const/4 v9, 0x1

    invoke-direct {v7, v4, v8, v9}, Lorg/apache/tools/ant/taskdefs/LogStreamHandler;-><init>(Lorg/apache/tools/ant/Task;II)V

    invoke-direct {v3, v7}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    invoke-virtual {v3, v5}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v3, v7}, Lorg/apache/tools/ant/taskdefs/Execute;->setWorkingDirectory(Ljava/io/File;)V

    invoke-virtual {v3, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Execute;->isFailure()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_0

    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Error running "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/rmic/ForkingSunRmic;->getExecutableName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " -maybe it is not on the path"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method

.method protected getExecutableName()Ljava/lang/String;
    .locals 1

    const-string v0, "rmic"

    return-object v0
.end method
