.class public Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;
.super Ljava/lang/Object;
.source "PumpStreamHandler.java"

# interfaces
.implements Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;


# instance fields
.field private err:Ljava/io/OutputStream;

.field private errorThread:Ljava/lang/Thread;

.field private input:Ljava/io/InputStream;

.field private inputPump:Lorg/apache/tools/ant/taskdefs/StreamPumper;

.field private out:Ljava/io/OutputStream;

.field private outputThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0
    .param p1    # Ljava/io/OutputStream;

    invoke-direct {p0, p1, p1}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Ljava/io/OutputStream;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;Ljava/io/OutputStream;Ljava/io/InputStream;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/io/OutputStream;Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->out:Ljava/io/OutputStream;

    iput-object p2, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->err:Ljava/io/OutputStream;

    iput-object p3, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->input:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method createInputPump(Ljava/io/InputStream;Ljava/io/OutputStream;Z)Lorg/apache/tools/ant/taskdefs/StreamPumper;
    .locals 2
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Z

    new-instance v0, Lorg/apache/tools/ant/taskdefs/StreamPumper;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/StreamPumper;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/StreamPumper;->setAutoflush(Z)V

    return-object v0
.end method

.method protected createProcessErrorPump(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;

    invoke-virtual {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->createPump(Ljava/io/InputStream;Ljava/io/OutputStream;)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->errorThread:Ljava/lang/Thread;

    return-void
.end method

.method protected createProcessOutputPump(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;

    invoke-virtual {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->createPump(Ljava/io/InputStream;Ljava/io/OutputStream;)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->outputThread:Ljava/lang/Thread;

    return-void
.end method

.method protected createPump(Ljava/io/InputStream;Ljava/io/OutputStream;)Ljava/lang/Thread;
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->createPump(Ljava/io/InputStream;Ljava/io/OutputStream;Z)Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method

.method protected createPump(Ljava/io/InputStream;Ljava/io/OutputStream;Z)Ljava/lang/Thread;
    .locals 2
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lorg/apache/tools/ant/taskdefs/StreamPumper;

    invoke-direct {v1, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/StreamPumper;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    return-object v0
.end method

.method protected getErr()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->err:Ljava/io/OutputStream;

    return-object v0
.end method

.method protected getOut()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->out:Ljava/io/OutputStream;

    return-object v0
.end method

.method public setProcessErrorStream(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->err:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->err:Ljava/io/OutputStream;

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->createProcessErrorPump(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    :cond_0
    return-void
.end method

.method public setProcessInputStream(Ljava/io/OutputStream;)V
    .locals 2
    .param p1    # Ljava/io/OutputStream;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->input:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->input:Ljava/io/InputStream;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->createInputPump(Ljava/io/InputStream;Ljava/io/OutputStream;Z)Lorg/apache/tools/ant/taskdefs/StreamPumper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->inputPump:Lorg/apache/tools/ant/taskdefs/StreamPumper;

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setProcessOutputStream(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->out:Ljava/io/OutputStream;

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->createProcessOutputPump(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->outputThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->errorThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->inputPump:Lorg/apache/tools/ant/taskdefs/StreamPumper;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->inputPump:Lorg/apache/tools/ant/taskdefs/StreamPumper;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->outputThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->errorThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->inputPump:Lorg/apache/tools/ant/taskdefs/StreamPumper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->inputPump:Lorg/apache/tools/ant/taskdefs/StreamPumper;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/StreamPumper;->stop()V

    :cond_0
    :try_start_2
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->err:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method
