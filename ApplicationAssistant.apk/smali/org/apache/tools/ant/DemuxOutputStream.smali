.class public Lorg/apache/tools/ant/DemuxOutputStream;
.super Ljava/io/OutputStream;
.source "DemuxOutputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/DemuxOutputStream$1;,
        Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;
    }
.end annotation


# static fields
.field private static final CR:I = 0xd

.field private static final INTIAL_SIZE:I = 0x84

.field private static final LF:I = 0xa

.field private static final MAX_SIZE:I = 0x400


# instance fields
.field private buffers:Ljava/util/WeakHashMap;

.field private isErrorStream:Z

.field private project:Lorg/apache/tools/ant/Project;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/Project;Z)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Z

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/DemuxOutputStream;->buffers:Ljava/util/WeakHashMap;

    iput-object p1, p0, Lorg/apache/tools/ant/DemuxOutputStream;->project:Lorg/apache/tools/ant/Project;

    iput-boolean p2, p0, Lorg/apache/tools/ant/DemuxOutputStream;->isErrorStream:Z

    return-void
.end method

.method private getBufferInfo()Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;
    .locals 4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/DemuxOutputStream;->buffers:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;-><init>(Lorg/apache/tools/ant/DemuxOutputStream$1;)V

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x84

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {v0, v2}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$102(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$202(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;Z)Z

    iget-object v2, p0, Lorg/apache/tools/ant/DemuxOutputStream;->buffers:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private removeBuffer()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/DemuxOutputStream;->buffers:Ljava/util/WeakHashMap;

    invoke-virtual {v1, v0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private resetBufferInfo()V
    .locals 3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/DemuxOutputStream;->buffers:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;

    :try_start_0
    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {v0, v2}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$102(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;Ljava/io/ByteArrayOutputStream;)Ljava/io/ByteArrayOutputStream;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$202(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;Z)Z

    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/DemuxOutputStream;->flush()V

    invoke-direct {p0}, Lorg/apache/tools/ant/DemuxOutputStream;->removeBuffer()V

    return-void
.end method

.method public flush()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/DemuxOutputStream;->getBufferInfo()Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/DemuxOutputStream;->processFlush(Ljava/io/ByteArrayOutputStream;)V

    :cond_0
    return-void
.end method

.method protected processBuffer(Ljava/io/ByteArrayOutputStream;)V
    .locals 3
    .param p1    # Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/DemuxOutputStream;->project:Lorg/apache/tools/ant/Project;

    iget-boolean v2, p0, Lorg/apache/tools/ant/DemuxOutputStream;->isErrorStream:Z

    invoke-virtual {v1, v0, v2}, Lorg/apache/tools/ant/Project;->demuxOutput(Ljava/lang/String;Z)V

    invoke-direct {p0}, Lorg/apache/tools/ant/DemuxOutputStream;->resetBufferInfo()V

    return-void
.end method

.method protected processFlush(Ljava/io/ByteArrayOutputStream;)V
    .locals 3
    .param p1    # Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/DemuxOutputStream;->project:Lorg/apache/tools/ant/Project;

    iget-boolean v2, p0, Lorg/apache/tools/ant/DemuxOutputStream;->isErrorStream:Z

    invoke-virtual {v1, v0, v2}, Lorg/apache/tools/ant/Project;->demuxFlush(Ljava/lang/String;Z)V

    invoke-direct {p0}, Lorg/apache/tools/ant/DemuxOutputStream;->resetBufferInfo()V

    return-void
.end method

.method public write(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    int-to-byte v1, p1

    invoke-direct {p0}, Lorg/apache/tools/ant/DemuxOutputStream;->getBufferInfo()Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;

    move-result-object v0

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/DemuxOutputStream;->processBuffer(Ljava/io/ByteArrayOutputStream;)V

    :goto_0
    const/16 v2, 0xd

    if-ne v1, v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-static {v0, v2}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$202(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;Z)Z

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$200(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v2

    const/16 v3, 0x400

    if-le v2, v3, :cond_0

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/DemuxOutputStream;->processBuffer(Ljava/io/ByteArrayOutputStream;)V

    :cond_0
    return-void

    :cond_1
    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$200(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/DemuxOutputStream;->processBuffer(Ljava/io/ByteArrayOutputStream;)V

    :cond_2
    invoke-static {v0}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public write([BII)V
    .locals 8
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v7, 0xd

    const/16 v6, 0xa

    move v3, p2

    move v1, v3

    move v4, p3

    invoke-direct {p0}, Lorg/apache/tools/ant/DemuxOutputStream;->getBufferInfo()Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;

    move-result-object v2

    :goto_0
    if-lez v4, :cond_4

    :goto_1
    if-lez v4, :cond_0

    aget-byte v5, p1, v3

    if-eq v5, v6, :cond_0

    aget-byte v5, p1, v3

    if-eq v5, v7, :cond_0

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    :cond_0
    sub-int v0, v3, v1

    if-lez v0, :cond_1

    invoke-static {v2}, Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;->access$100(Lorg/apache/tools/ant/DemuxOutputStream$BufferInfo;)Ljava/io/ByteArrayOutputStream;

    move-result-object v5

    invoke-virtual {v5, p1, v1, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_1
    :goto_2
    if-lez v4, :cond_3

    aget-byte v5, p1, v3

    if-eq v5, v6, :cond_2

    aget-byte v5, p1, v3

    if-ne v5, v7, :cond_3

    :cond_2
    aget-byte v5, p1, v3

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/DemuxOutputStream;->write(I)V

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    return-void
.end method
