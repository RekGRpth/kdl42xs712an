.class public final Lorg/apache/tools/ant/filters/LineContainsRegExp;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "LineContainsRegExp.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# static fields
.field private static final NEGATE_KEY:Ljava/lang/String; = "negate"

.field private static final REGEXP_KEY:Ljava/lang/String; = "regexp"


# instance fields
.field private line:Ljava/lang/String;

.field private negate:Z

.field private regexps:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->negate:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->negate:Z

    return-void
.end method

.method private getRegexps()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    return-object v0
.end method

.method private initialize()V
    .locals 6

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->getParameters()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_2

    const-string v4, "regexp"

    aget-object v5, v1, v0

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Parameter;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/apache/tools/ant/types/RegularExpression;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/RegularExpression;-><init>()V

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/types/RegularExpression;->setPattern(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v4, "negate"

    aget-object v5, v1, v0

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Parameter;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->setNegate(Z)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private setRegexps(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public addConfiguredRegexp(Lorg/apache/tools/ant/types/RegularExpression;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/RegularExpression;

    iget-object v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/LineContainsRegExp;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/LineContainsRegExp;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->getRegexps()Ljava/util/Vector;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->setRegexps(Ljava/util/Vector;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->isNegated()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->setNegate(Z)V

    return-object v0
.end method

.method public isNegated()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->negate:Z

    return v0
.end method

.method public read()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->getInitialized()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->initialize()V

    invoke-virtual {p0, v8}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->setInitialized(Z)V

    :cond_0
    const/4 v0, -0x1

    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v8, :cond_2

    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    :cond_1
    :goto_0
    move v6, v0

    :goto_1
    return v6

    :cond_2
    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->readLine()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    :goto_2
    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    if-eqz v6, :cond_5

    const/4 v2, 0x1

    const/4 v1, 0x0

    :goto_3
    if-eqz v2, :cond_4

    if-ge v1, v5, :cond_4

    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->regexps:Ljava/util/Vector;

    invoke-virtual {v6, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/RegularExpression;

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-virtual {v4, v6}, Lorg/apache/tools/ant/types/RegularExpression;->getRegexp(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/Regexp;

    move-result-object v3

    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    invoke-interface {v3, v6}, Lorg/apache/tools/ant/util/regexp/Regexp;->matches(Ljava/lang/String;)Z

    move-result v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->isNegated()Z

    move-result v6

    xor-int/2addr v6, v2

    if-eqz v6, :cond_6

    :cond_5
    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->read()I

    move-result v6

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContainsRegExp;->readLine()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->line:Ljava/lang/String;

    goto :goto_2
.end method

.method public setNegate(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/filters/LineContainsRegExp;->negate:Z

    return-void
.end method
