.class public final Lorg/apache/tools/ant/filters/TabsToSpaces;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "TabsToSpaces.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# static fields
.field private static final DEFAULT_TAB_LENGTH:I = 0x8

.field private static final TAB_LENGTH_KEY:Ljava/lang/String; = "tablength"


# instance fields
.field private spacesRemaining:I

.field private tabLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->tabLength:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->spacesRemaining:I

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->tabLength:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->spacesRemaining:I

    return-void
.end method

.method private getTablength()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->tabLength:I

    return v0
.end method

.method private initialize()V
    .locals 4

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/TabsToSpaces;->getParameters()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    if-eqz v2, :cond_1

    const-string v2, "tablength"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/Integer;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->tabLength:I

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/TabsToSpaces;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/TabsToSpaces;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/TabsToSpaces;->getTablength()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/TabsToSpaces;->setTablength(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/TabsToSpaces;->setInitialized(Z)V

    return-object v0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/TabsToSpaces;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/TabsToSpaces;->initialize()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/filters/TabsToSpaces;->setInitialized(Z)V

    :cond_0
    const/4 v0, -0x1

    iget v1, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->spacesRemaining:I

    if-lez v1, :cond_2

    iget v1, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->spacesRemaining:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->spacesRemaining:I

    const/16 v0, 0x20

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->in:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    iget v1, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->tabLength:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->spacesRemaining:I

    const/16 v0, 0x20

    goto :goto_0
.end method

.method public setTablength(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/filters/TabsToSpaces;->tabLength:I

    return-void
.end method
