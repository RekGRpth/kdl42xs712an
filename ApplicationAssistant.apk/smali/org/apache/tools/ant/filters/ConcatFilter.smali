.class public final Lorg/apache/tools/ant/filters/ConcatFilter;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "ConcatFilter.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# instance fields
.field private append:Ljava/io/File;

.field private appendReader:Ljava/io/Reader;

.field private prepend:Ljava/io/File;

.field private prependReader:Ljava/io/Reader;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prependReader:Ljava/io/Reader;

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->appendReader:Ljava/io/Reader;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prependReader:Ljava/io/Reader;

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->appendReader:Ljava/io/Reader;

    return-void
.end method

.method private initialize()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ConcatFilter;->getParameters()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    const-string v2, "prepend"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/io/File;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/filters/ConcatFilter;->setPrepend(Ljava/io/File;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "append"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/io/File;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/filters/ConcatFilter;->setAppend(Ljava/io/File;)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prepend:Ljava/io/File;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prepend:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isAbsolute()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ConcatFilter;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prepend:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prepend:Ljava/io/File;

    :cond_3
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prepend:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prependReader:Ljava/io/Reader;

    :cond_4
    iget-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->append:Ljava/io/File;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->append:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isAbsolute()Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ConcatFilter;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->append:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->append:Ljava/io/File;

    :cond_5
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->append:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->appendReader:Ljava/io/Reader;

    :cond_6
    return-void
.end method


# virtual methods
.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/ConcatFilter;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/ConcatFilter;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ConcatFilter;->getPrepend()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/ConcatFilter;->setPrepend(Ljava/io/File;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ConcatFilter;->getAppend()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/ConcatFilter;->setAppend(Ljava/io/File;)V

    return-object v0
.end method

.method public getAppend()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->append:Ljava/io/File;

    return-object v0
.end method

.method public getPrepend()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prepend:Ljava/io/File;

    return-object v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ConcatFilter;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/ConcatFilter;->initialize()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/filters/ConcatFilter;->setInitialized(Z)V

    :cond_0
    const/4 v0, -0x1

    iget-object v1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prependReader:Ljava/io/Reader;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prependReader:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prependReader:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->close()V

    iput-object v3, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prependReader:Ljava/io/Reader;

    :cond_1
    if-ne v0, v2, :cond_2

    invoke-super {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;->read()I

    move-result v0

    :cond_2
    if-ne v0, v2, :cond_3

    iget-object v1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->appendReader:Ljava/io/Reader;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->appendReader:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    if-ne v0, v2, :cond_3

    iget-object v1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->appendReader:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->close()V

    iput-object v3, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->appendReader:Ljava/io/Reader;

    :cond_3
    return v0
.end method

.method public setAppend(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->append:Ljava/io/File;

    return-void
.end method

.method public setPrepend(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/ConcatFilter;->prepend:Ljava/io/File;

    return-void
.end method
