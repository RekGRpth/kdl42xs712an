.class public final Lorg/apache/tools/ant/filters/StripJavaComments;
.super Lorg/apache/tools/ant/filters/BaseFilterReader;
.source "StripJavaComments.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# instance fields
.field private inString:Z

.field private quoted:Z

.field private readAheadCh:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseFilterReader;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->readAheadCh:I

    iput-boolean v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->inString:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->quoted:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1    # Ljava/io/Reader;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseFilterReader;-><init>(Ljava/io/Reader;)V

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->readAheadCh:I

    iput-boolean v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->inString:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->quoted:Z

    return-void
.end method


# virtual methods
.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 1
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/StripJavaComments;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/StripJavaComments;-><init>(Ljava/io/Reader;)V

    return-object v0
.end method

.method public read()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    const/16 v6, 0x2f

    const/16 v5, 0x2a

    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v0, -0x1

    iget v3, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->readAheadCh:I

    if-eq v3, v4, :cond_1

    iget v0, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->readAheadCh:I

    iput v4, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->readAheadCh:I

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v3, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->in:Ljava/io/Reader;

    invoke-virtual {v3}, Ljava/io/Reader;->read()I

    move-result v0

    const/16 v3, 0x22

    if-ne v0, v3, :cond_3

    iget-boolean v3, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->quoted:Z

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->inString:Z

    if-nez v3, :cond_2

    :goto_1
    iput-boolean v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->inString:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->quoted:Z

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    const/16 v3, 0x5c

    if-ne v0, v3, :cond_5

    iget-boolean v3, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->quoted:Z

    if-nez v3, :cond_4

    :goto_2
    iput-boolean v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->quoted:Z

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    iput-boolean v2, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->quoted:Z

    iget-boolean v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->inString:Z

    if-nez v1, :cond_0

    if-ne v0, v6, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->in:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    if-ne v0, v6, :cond_6

    :goto_3
    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    if-eq v0, v4, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->in:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    goto :goto_3

    :cond_6
    if-ne v0, v5, :cond_9

    :cond_7
    if-eq v0, v4, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->in:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    if-ne v0, v5, :cond_7

    iget-object v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->in:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    :goto_4
    if-ne v0, v5, :cond_8

    if-eq v0, v4, :cond_8

    iget-object v1, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->in:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    goto :goto_4

    :cond_8
    if-ne v0, v6, :cond_7

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/StripJavaComments;->read()I

    move-result v0

    goto :goto_0

    :cond_9
    iput v0, p0, Lorg/apache/tools/ant/filters/StripJavaComments;->readAheadCh:I

    const/16 v0, 0x2f

    goto :goto_0
.end method
