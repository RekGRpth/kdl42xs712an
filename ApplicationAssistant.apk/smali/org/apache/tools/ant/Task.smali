.class public abstract Lorg/apache/tools/ant/Task;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "Task.java"


# instance fields
.field private invalid:Z

.field private replacement:Lorg/apache/tools/ant/UnknownElement;

.field protected target:Lorg/apache/tools/ant/Target;

.field protected taskName:Ljava/lang/String;

.field protected taskType:Ljava/lang/String;

.field protected wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    return-void
.end method

.method private getReplacement()Lorg/apache/tools/ant/UnknownElement;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/UnknownElement;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->taskType:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/UnknownElement;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/UnknownElement;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->taskType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/UnknownElement;->setTaskType(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->taskName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/UnknownElement;->setTaskName(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->location:Lorg/apache/tools/ant/Location;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/UnknownElement;->setLocation(Lorg/apache/tools/ant/Location;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/UnknownElement;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/UnknownElement;->setRuntimeConfigurableWrapper(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/RuntimeConfigurable;->setProxy(Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/Task;->replaceChildren(Lorg/apache/tools/ant/RuntimeConfigurable;Lorg/apache/tools/ant/UnknownElement;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->target:Lorg/apache/tools/ant/Target;

    iget-object v1, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v0, p0, v1}, Lorg/apache/tools/ant/Target;->replaceChild(Lorg/apache/tools/ant/Task;Lorg/apache/tools/ant/Task;)V

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v0}, Lorg/apache/tools/ant/UnknownElement;->maybeConfigure()V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/Task;->replacement:Lorg/apache/tools/ant/UnknownElement;

    return-object v0
.end method

.method private replaceChildren(Lorg/apache/tools/ant/RuntimeConfigurable;Lorg/apache/tools/ant/UnknownElement;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/RuntimeConfigurable;
    .param p2    # Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {p1}, Lorg/apache/tools/ant/RuntimeConfigurable;->getChildren()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/RuntimeConfigurable;

    new-instance v0, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v1}, Lorg/apache/tools/ant/RuntimeConfigurable;->getElementTag()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lorg/apache/tools/ant/UnknownElement;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lorg/apache/tools/ant/UnknownElement;->addChild(Lorg/apache/tools/ant/UnknownElement;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/UnknownElement;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/UnknownElement;->setRuntimeConfigurableWrapper(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/RuntimeConfigurable;->setProxy(Ljava/lang/Object;)V

    invoke-direct {p0, v1, v0}, Lorg/apache/tools/ant/Task;->replaceChildren(Lorg/apache/tools/ant/RuntimeConfigurable;Lorg/apache/tools/ant/UnknownElement;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final bindToOwner(Lorg/apache/tools/ant/Task;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Task;

    invoke-virtual {p1}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/Task;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/Task;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/Task;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/Task;->getTaskName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/Task;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/Task;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/Task;->setDescription(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/Task;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/Task;->setLocation(Lorg/apache/tools/ant/Location;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/Task;->getTaskType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/Task;->setTaskType(Ljava/lang/String;)V

    return-void
.end method

.method public execute()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    return-void
.end method

.method public getOwningTarget()Lorg/apache/tools/ant/Target;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->target:Lorg/apache/tools/ant/Target;

    return-object v0
.end method

.method public getRuntimeConfigurableWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getTaskName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/tools/ant/RuntimeConfigurable;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    return-object v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->taskName:Ljava/lang/String;

    return-object v0
.end method

.method public getTaskType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->taskType:Ljava/lang/String;

    return-object v0
.end method

.method protected getWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    return-object v0
.end method

.method protected handleErrorFlush(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    return-void
.end method

.method protected handleErrorOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/Task;->log(Ljava/lang/String;I)V

    return-void
.end method

.method protected handleFlush(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/Task;->handleOutput(Ljava/lang/String;)V

    return-void
.end method

.method protected handleInput([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/Project;->defaultInput([BII)I

    move-result v0

    return v0
.end method

.method protected handleOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/Task;->log(Ljava/lang/String;I)V

    return-void
.end method

.method public init()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    return-void
.end method

.method protected final isInvalid()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/Task;->invalid:Z

    return v0
.end method

.method public log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/Task;->log(Ljava/lang/String;I)V

    return-void
.end method

.method public log(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lorg/apache/tools/ant/Project;->log(Lorg/apache/tools/ant/Task;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public log(Ljava/lang/String;Ljava/lang/Throwable;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;
    .param p3    # I

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lorg/apache/tools/ant/Project;->log(Lorg/apache/tools/ant/Task;Ljava/lang/String;Ljava/lang/Throwable;I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p3}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public log(Ljava/lang/Throwable;I)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
    .param p2    # I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lorg/apache/tools/ant/Task;->log(Ljava/lang/String;Ljava/lang/Throwable;I)V

    :cond_0
    return-void
.end method

.method final markInvalid()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/Task;->invalid:Z

    return-void
.end method

.method public maybeConfigure()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/ant/Task;->invalid:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/RuntimeConfigurable;->maybeConfigure(Lorg/apache/tools/ant/Project;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/ant/Task;->getReplacement()Lorg/apache/tools/ant/UnknownElement;

    goto :goto_0
.end method

.method public final perform()V
    .locals 7

    iget-boolean v5, p0, Lorg/apache/tools/ant/Task;->invalid:Z

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {v5, p0}, Lorg/apache/tools/ant/Project;->fireTaskStarted(Lorg/apache/tools/ant/Task;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->maybeConfigure()V

    invoke-static {p0}, Lorg/apache/tools/ant/dispatch/DispatchUtils;->execute(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {v5, p0, v2}, Lorg/apache/tools/ant/Project;->fireTaskFinished(Lorg/apache/tools/ant/Task;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v1}, Lorg/apache/tools/ant/BuildException;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    sget-object v6, Lorg/apache/tools/ant/Location;->UNKNOWN_LOCATION:Lorg/apache/tools/ant/Location;

    if-ne v5, v6, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/apache/tools/ant/BuildException;->setLocation(Lorg/apache/tools/ant/Location;)V

    :cond_0
    move-object v2, v1

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-virtual {v6, p0, v2}, Lorg/apache/tools/ant/Project;->fireTaskFinished(Lorg/apache/tools/ant/Task;Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v1

    move-object v2, v1

    :try_start_2
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/BuildException;->setLocation(Lorg/apache/tools/ant/Location;)V

    throw v0

    :catch_2
    move-exception v1

    move-object v2, v1

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/ant/Task;->getReplacement()Lorg/apache/tools/ant/UnknownElement;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/UnknownElement;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/Task;->perform()V

    goto :goto_0
.end method

.method public reconfigure()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/RuntimeConfigurable;->reconfigure(Lorg/apache/tools/ant/Project;)V

    :cond_0
    return-void
.end method

.method public setOwningTarget(Lorg/apache/tools/ant/Target;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Target;

    iput-object p1, p0, Lorg/apache/tools/ant/Task;->target:Lorg/apache/tools/ant/Target;

    return-void
.end method

.method public setRuntimeConfigurableWrapper(Lorg/apache/tools/ant/RuntimeConfigurable;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/RuntimeConfigurable;

    iput-object p1, p0, Lorg/apache/tools/ant/Task;->wrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    return-void
.end method

.method public setTaskName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/Task;->taskName:Ljava/lang/String;

    return-void
.end method

.method public setTaskType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/Task;->taskType:Ljava/lang/String;

    return-void
.end method
