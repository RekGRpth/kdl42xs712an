.class Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;
.super Ljava/lang/Object;
.source "AntClassLoader.java"

# interfaces
.implements Ljava/util/Enumeration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/AntClassLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResourceEnumeration"
.end annotation


# instance fields
.field private nextResource:Ljava/net/URL;

.field private pathElementsIndex:I

.field private resourceName:Ljava/lang/String;

.field private final this$0:Lorg/apache/tools/ant/AntClassLoader;


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/AntClassLoader;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->this$0:Lorg/apache/tools/ant/AntClassLoader;

    iput-object p2, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->resourceName:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->pathElementsIndex:I

    invoke-direct {p0}, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->findNextResource()V

    return-void
.end method

.method private findNextResource()V
    .locals 4

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->pathElementsIndex:I

    iget-object v3, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->this$0:Lorg/apache/tools/ant/AntClassLoader;

    invoke-static {v3}, Lorg/apache/tools/ant/AntClassLoader;->access$000(Lorg/apache/tools/ant/AntClassLoader;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    if-nez v1, :cond_0

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->this$0:Lorg/apache/tools/ant/AntClassLoader;

    invoke-static {v2}, Lorg/apache/tools/ant/AntClassLoader;->access$000(Lorg/apache/tools/ant/AntClassLoader;)Ljava/util/Vector;

    move-result-object v2

    iget v3, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->pathElementsIndex:I

    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iget-object v2, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->this$0:Lorg/apache/tools/ant/AntClassLoader;

    iget-object v3, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->resourceName:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lorg/apache/tools/ant/AntClassLoader;->getResourceURL(Ljava/io/File;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->pathElementsIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->pathElementsIndex:I
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->nextResource:Ljava/net/URL;

    return-void
.end method


# virtual methods
.method public hasMoreElements()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->nextResource:Ljava/net/URL;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextElement()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->nextResource:Ljava/net/URL;

    invoke-direct {p0}, Lorg/apache/tools/ant/AntClassLoader$ResourceEnumeration;->findNextResource()V

    return-object v0
.end method
