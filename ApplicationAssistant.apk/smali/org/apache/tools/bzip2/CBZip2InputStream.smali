.class public Lorg/apache/tools/bzip2/CBZip2InputStream;
.super Ljava/io/InputStream;
.source "CBZip2InputStream.java"

# interfaces
.implements Lorg/apache/tools/bzip2/BZip2Constants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/bzip2/CBZip2InputStream$Data;
    }
.end annotation


# static fields
.field private static final EOF:I = 0x0

.field private static final NO_RAND_PART_A_STATE:I = 0x5

.field private static final NO_RAND_PART_B_STATE:I = 0x6

.field private static final NO_RAND_PART_C_STATE:I = 0x7

.field private static final RAND_PART_A_STATE:I = 0x2

.field private static final RAND_PART_B_STATE:I = 0x3

.field private static final RAND_PART_C_STATE:I = 0x4

.field private static final START_BLOCK_STATE:I = 0x1


# instance fields
.field private blockRandomised:Z

.field private blockSize100k:I

.field private bsBuff:I

.field private bsLive:I

.field private computedBlockCRC:I

.field private computedCombinedCRC:I

.field private final crc:Lorg/apache/tools/bzip2/CRC;

.field private currentChar:I

.field private currentState:I

.field private data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

.field private in:Ljava/io/InputStream;

.field private last:I

.field private nInUse:I

.field private origPtr:I

.field private storedBlockCRC:I

.field private storedCombinedCRC:I

.field private su_ch2:I

.field private su_chPrev:I

.field private su_count:I

.field private su_i2:I

.field private su_j2:I

.field private su_rNToGo:I

.field private su_rTPos:I

.field private su_tPos:I

.field private su_z:C


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    new-instance v0, Lorg/apache/tools/bzip2/CRC;

    invoke-direct {v0}, Lorg/apache/tools/bzip2/CRC;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentChar:I

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    iput-object p1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->init()V

    return-void
.end method

.method private bsGetBit()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v2

    if-gez v2, :cond_0

    new-instance v3, Ljava/io/IOException;

    const-string v4, "unexpected end of stream"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    shl-int/lit8 v4, v0, 0x8

    or-int v0, v4, v2

    add-int/lit8 v1, v1, 0x8

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    :cond_1
    add-int/lit8 v4, v1, -0x1

    iput v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    add-int/lit8 v4, v1, -0x1

    shr-int v4, v0, v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    :goto_0
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private bsGetInt()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x8

    invoke-direct {p0, v2}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    or-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x8

    invoke-direct {p0, v2}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    or-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x8

    invoke-direct {p0, v2}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method private bsGetUByte()C
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method private bsR(I)I
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    if-ge v1, p1, :cond_2

    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    :cond_0
    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v3

    if-gez v3, :cond_1

    new-instance v4, Ljava/io/IOException;

    const-string v5, "unexpected end of stream"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    shl-int/lit8 v4, v0, 0x8

    or-int v0, v4, v3

    add-int/lit8 v1, v1, 0x8

    if-lt v1, p1, :cond_0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    :cond_2
    sub-int v4, v1, p1

    iput v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    sub-int v4, v1, p1

    shr-int v4, v0, v4

    const/4 v5, 0x1

    shl-int/2addr v5, p1

    add-int/lit8 v5, v5, -0x1

    and-int/2addr v4, v5

    return v4
.end method

.method private complete()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetInt()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->storedCombinedCRC:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->storedCombinedCRC:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    if-eq v0, v1, :cond_0

    invoke-static {}, Lorg/apache/tools/bzip2/CBZip2InputStream;->reportCRCError()V

    :cond_0
    return-void
.end method

.method private createHuffmanDecodingTables(II)V
    .locals 19
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v12, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->temp_charArray2d:[[C

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->minLens:[I

    move-object/from16 v16, v0

    iget-object v15, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->limit:[[I

    iget-object v9, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->base:[[I

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->perm:[[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    :goto_0
    move/from16 v0, v18

    move/from16 v1, p2

    if-ge v0, v1, :cond_3

    const/16 v6, 0x20

    const/4 v7, 0x0

    aget-object v13, v12, v18

    move/from16 v11, p1

    :cond_0
    :goto_1
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_2

    aget-char v14, v13, v11

    if-le v14, v7, :cond_1

    move v7, v14

    :cond_1
    if-ge v14, v6, :cond_0

    move v6, v14

    goto :goto_1

    :cond_2
    aget-object v2, v15, v18

    aget-object v3, v9, v18

    aget-object v4, v17, v18

    aget-object v5, v12, v18

    move/from16 v8, p1

    invoke-static/range {v2 .. v8}, Lorg/apache/tools/bzip2/CBZip2InputStream;->hbCreateDecodeTables([I[I[I[CIII)V

    aput v6, v16, v18

    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private endBlock()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v0}, Lorg/apache/tools/bzip2/CRC;->getFinalCRC()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedBlockCRC:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->storedBlockCRC:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedBlockCRC:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->storedCombinedCRC:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->storedCombinedCRC:I

    ushr-int/lit8 v1, v1, 0x1f

    or-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->storedBlockCRC:I

    xor-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    invoke-static {}, Lorg/apache/tools/bzip2/CBZip2InputStream;->reportCRCError()V

    :cond_0
    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    ushr-int/lit8 v1, v1, 0x1f

    or-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedBlockCRC:I

    xor-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->computedCombinedCRC:I

    return-void
.end method

.method private getAndMoveToFrontDecode()V
    .locals 43
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v40, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/bzip2/CBZip2InputStream;->origPtr:I

    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->recvDecodingTables()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->ll8:[B

    move-object/from16 v22, v0

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->unzftab:[I

    move-object/from16 v35, v0

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->selector:[B

    move-object/from16 v31, v0

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->seqToUnseq:[B

    move-object/from16 v32, v0

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->getAndMoveToFrontDecode_yy:[C

    move-object/from16 v36, v0

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->minLens:[I

    move-object/from16 v23, v0

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->limit:[[I

    move-object/from16 v19, v0

    iget-object v5, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->base:[[I

    iget-object v0, v10, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->perm:[[I

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->blockSize100k:I

    move/from16 v40, v0

    const v41, 0x186a0

    mul-int v20, v40, v41

    const/16 v14, 0x100

    :goto_0
    add-int/lit8 v14, v14, -0x1

    if-ltz v14, :cond_0

    int-to-char v0, v14

    move/from16 v40, v0

    aput-char v40, v36, v14

    const/16 v40, 0x0

    aput v40, v35, v14

    goto :goto_0

    :cond_0
    const/4 v12, 0x0

    const/16 v13, 0x31

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->nInUse:I

    move/from16 v40, v0

    add-int/lit8 v11, v40, 0x1

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-direct {v0, v1}, Lorg/apache/tools/bzip2/CBZip2InputStream;->getAndMoveToFrontDecode0(I)I

    move-result v26

    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    const/16 v18, -0x1

    aget-byte v40, v31, v12

    move/from16 v0, v40

    and-int/lit16 v0, v0, 0xff

    move/from16 v38, v0

    aget-object v6, v5, v38

    aget-object v21, v19, v38

    aget-object v28, v27, v38

    aget v24, v23, v38

    :cond_1
    :goto_1
    move/from16 v0, v26

    if-eq v0, v11, :cond_16

    if-eqz v26, :cond_2

    const/16 v40, 0x1

    move/from16 v0, v26

    move/from16 v1, v40

    if-ne v0, v1, :cond_c

    :cond_2
    const/16 v29, -0x1

    const/16 v25, 0x1

    :goto_2
    if-nez v26, :cond_3

    add-int v29, v29, v25

    :goto_3
    if-nez v13, :cond_4

    const/16 v13, 0x31

    add-int/lit8 v12, v12, 0x1

    aget-byte v40, v31, v12

    move/from16 v0, v40

    and-int/lit16 v0, v0, 0xff

    move/from16 v38, v0

    aget-object v6, v5, v38

    aget-object v21, v19, v38

    aget-object v28, v27, v38

    aget v24, v23, v38

    :goto_4
    move/from16 v37, v24

    :goto_5
    move/from16 v0, v37

    if-ge v8, v0, :cond_6

    invoke-virtual {v15}, Ljava/io/InputStream;->read()I

    move-result v33

    if-ltz v33, :cond_5

    shl-int/lit8 v40, v7, 0x8

    or-int v7, v40, v33

    add-int/lit8 v8, v8, 0x8

    goto :goto_5

    :cond_3
    const/16 v40, 0x1

    move/from16 v0, v26

    move/from16 v1, v40

    if-ne v0, v1, :cond_a

    shl-int/lit8 v40, v25, 0x1

    add-int v29, v29, v40

    goto :goto_3

    :cond_4
    add-int/lit8 v13, v13, -0x1

    goto :goto_4

    :cond_5
    new-instance v40, Ljava/io/IOException;

    const-string v41, "unexpected end of stream"

    invoke-direct/range {v40 .. v41}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v40

    :cond_6
    sub-int v40, v8, v37

    shr-int v40, v7, v40

    const/16 v41, 0x1

    shl-int v41, v41, v37

    add-int/lit8 v41, v41, -0x1

    and-int v39, v40, v41

    sub-int v8, v8, v37

    :goto_6
    aget v40, v21, v37

    move/from16 v0, v39

    move/from16 v1, v40

    if-le v0, v1, :cond_9

    add-int/lit8 v37, v37, 0x1

    :goto_7
    const/16 v40, 0x1

    move/from16 v0, v40

    if-ge v8, v0, :cond_8

    invoke-virtual {v15}, Ljava/io/InputStream;->read()I

    move-result v33

    if-ltz v33, :cond_7

    shl-int/lit8 v40, v7, 0x8

    or-int v7, v40, v33

    add-int/lit8 v8, v8, 0x8

    goto :goto_7

    :cond_7
    new-instance v40, Ljava/io/IOException;

    const-string v41, "unexpected end of stream"

    invoke-direct/range {v40 .. v41}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v40

    :cond_8
    add-int/lit8 v8, v8, -0x1

    shl-int/lit8 v40, v39, 0x1

    shr-int v41, v7, v8

    and-int/lit8 v41, v41, 0x1

    or-int v39, v40, v41

    goto :goto_6

    :cond_9
    aget v40, v6, v37

    sub-int v40, v39, v40

    aget v26, v28, v40

    shl-int/lit8 v25, v25, 0x1

    goto/16 :goto_2

    :cond_a
    const/16 v40, 0x0

    aget-char v40, v36, v40

    aget-byte v9, v32, v40

    and-int/lit16 v0, v9, 0xff

    move/from16 v40, v0

    aget v41, v35, v40

    add-int/lit8 v42, v29, 0x1

    add-int v41, v41, v42

    aput v41, v35, v40

    move/from16 v30, v29

    :goto_8
    add-int/lit8 v29, v30, -0x1

    if-ltz v30, :cond_b

    add-int/lit8 v18, v18, 0x1

    aput-byte v9, v22, v18

    move/from16 v30, v29

    goto :goto_8

    :cond_b
    move/from16 v0, v18

    move/from16 v1, v20

    if-lt v0, v1, :cond_1

    new-instance v40, Ljava/io/IOException;

    const-string v41, "block overrun"

    invoke-direct/range {v40 .. v41}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v40

    :cond_c
    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move/from16 v1, v20

    if-lt v0, v1, :cond_d

    new-instance v40, Ljava/io/IOException;

    const-string v41, "block overrun"

    invoke-direct/range {v40 .. v41}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v40

    :cond_d
    add-int/lit8 v40, v26, -0x1

    aget-char v34, v36, v40

    aget-byte v40, v32, v34

    move/from16 v0, v40

    and-int/lit16 v0, v0, 0xff

    move/from16 v40, v0

    aget v41, v35, v40

    add-int/lit8 v41, v41, 0x1

    aput v41, v35, v40

    aget-byte v40, v32, v34

    aput-byte v40, v22, v18

    const/16 v40, 0x10

    move/from16 v0, v26

    move/from16 v1, v40

    if-gt v0, v1, :cond_e

    add-int/lit8 v16, v26, -0x1

    move/from16 v17, v16

    :goto_9
    if-lez v17, :cond_f

    add-int/lit8 v16, v17, -0x1

    aget-char v40, v36, v16

    aput-char v40, v36, v17

    move/from16 v17, v16

    goto :goto_9

    :cond_e
    const/16 v40, 0x0

    const/16 v41, 0x1

    add-int/lit8 v42, v26, -0x1

    move-object/from16 v0, v36

    move/from16 v1, v40

    move-object/from16 v2, v36

    move/from16 v3, v41

    move/from16 v4, v42

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    const/16 v40, 0x0

    aput-char v34, v36, v40

    if-nez v13, :cond_10

    const/16 v13, 0x31

    add-int/lit8 v12, v12, 0x1

    aget-byte v40, v31, v12

    move/from16 v0, v40

    and-int/lit16 v0, v0, 0xff

    move/from16 v38, v0

    aget-object v6, v5, v38

    aget-object v21, v19, v38

    aget-object v28, v27, v38

    aget v24, v23, v38

    :goto_a
    move/from16 v37, v24

    :goto_b
    move/from16 v0, v37

    if-ge v8, v0, :cond_12

    invoke-virtual {v15}, Ljava/io/InputStream;->read()I

    move-result v33

    if-ltz v33, :cond_11

    shl-int/lit8 v40, v7, 0x8

    or-int v7, v40, v33

    add-int/lit8 v8, v8, 0x8

    goto :goto_b

    :cond_10
    add-int/lit8 v13, v13, -0x1

    goto :goto_a

    :cond_11
    new-instance v40, Ljava/io/IOException;

    const-string v41, "unexpected end of stream"

    invoke-direct/range {v40 .. v41}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v40

    :cond_12
    sub-int v40, v8, v37

    shr-int v40, v7, v40

    const/16 v41, 0x1

    shl-int v41, v41, v37

    add-int/lit8 v41, v41, -0x1

    and-int v39, v40, v41

    sub-int v8, v8, v37

    :goto_c
    aget v40, v21, v37

    move/from16 v0, v39

    move/from16 v1, v40

    if-le v0, v1, :cond_15

    add-int/lit8 v37, v37, 0x1

    :goto_d
    const/16 v40, 0x1

    move/from16 v0, v40

    if-ge v8, v0, :cond_14

    invoke-virtual {v15}, Ljava/io/InputStream;->read()I

    move-result v33

    if-ltz v33, :cond_13

    shl-int/lit8 v40, v7, 0x8

    or-int v7, v40, v33

    add-int/lit8 v8, v8, 0x8

    goto :goto_d

    :cond_13
    new-instance v40, Ljava/io/IOException;

    const-string v41, "unexpected end of stream"

    invoke-direct/range {v40 .. v41}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v40

    :cond_14
    add-int/lit8 v8, v8, -0x1

    shl-int/lit8 v40, v39, 0x1

    shr-int v41, v7, v8

    and-int/lit8 v41, v41, 0x1

    or-int v39, v40, v41

    goto :goto_c

    :cond_15
    aget v40, v6, v37

    sub-int v40, v39, v40

    aget v26, v28, v40

    goto/16 :goto_1

    :cond_16
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/bzip2/CBZip2InputStream;->last:I

    move-object/from16 v0, p0

    iput v8, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    move-object/from16 v0, p0

    iput v7, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    return-void
.end method

.method private getAndMoveToFrontDecode0(I)I
    .locals 11
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v9, v2, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->selector:[B

    aget-byte v9, v9, p1

    and-int/lit16 v7, v9, 0xff

    iget-object v9, v2, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->limit:[[I

    aget-object v4, v9, v7

    iget-object v9, v2, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->minLens:[I

    aget v6, v9, v7

    invoke-direct {p0, v6}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v8

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    :goto_0
    aget v9, v4, v6

    if-le v8, v9, :cond_2

    add-int/lit8 v6, v6, 0x1

    :goto_1
    const/4 v9, 0x1

    if-ge v1, v9, :cond_1

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v5

    if-ltz v5, :cond_0

    shl-int/lit8 v9, v0, 0x8

    or-int v0, v9, v5

    add-int/lit8 v1, v1, 0x8

    goto :goto_1

    :cond_0
    new-instance v9, Ljava/io/IOException;

    const-string v10, "unexpected end of stream"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_1
    add-int/lit8 v1, v1, -0x1

    shl-int/lit8 v9, v8, 0x1

    shr-int v10, v0, v1

    and-int/lit8 v10, v10, 0x1

    or-int v8, v9, v10

    goto :goto_0

    :cond_2
    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsLive:I

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsBuff:I

    iget-object v9, v2, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->perm:[[I

    aget-object v9, v9, v7

    iget-object v10, v2, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->base:[[I

    aget-object v10, v10, v7

    aget v10, v10, v6

    sub-int v10, v8, v10

    aget v9, v9, v10

    return v9
.end method

.method private static hbCreateDecodeTables([I[I[I[CIII)V
    .locals 9
    .param p0    # [I
    .param p1    # [I
    .param p2    # [I
    .param p3    # [C
    .param p4    # I
    .param p5    # I
    .param p6    # I

    move v1, p4

    const/4 v4, 0x0

    :goto_0
    if-gt v1, p5, :cond_1

    const/4 v2, 0x0

    move v5, v4

    :goto_1
    if-ge v2, p6, :cond_0

    aget-char v7, p3, v2

    if-ne v7, v1, :cond_7

    add-int/lit8 v4, v5, 0x1

    aput v2, p2, v5

    :goto_2
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v4, v5

    goto :goto_0

    :cond_1
    const/16 v1, 0x17

    :goto_3
    add-int/lit8 v1, v1, -0x1

    if-lez v1, :cond_2

    const/4 v7, 0x0

    aput v7, p1, v1

    const/4 v7, 0x0

    aput v7, p0, v1

    goto :goto_3

    :cond_2
    const/4 v1, 0x0

    :goto_4
    if-ge v1, p6, :cond_3

    aget-char v7, p3, v1

    add-int/lit8 v7, v7, 0x1

    aget v8, p1, v7

    add-int/lit8 v8, v8, 0x1

    aput v8, p1, v7

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    const/4 v1, 0x1

    const/4 v7, 0x0

    aget v0, p1, v7

    :goto_5
    const/16 v7, 0x17

    if-ge v1, v7, :cond_4

    aget v7, p1, v1

    add-int/2addr v0, v7

    aput v0, p1, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_4
    move v1, p4

    const/4 v6, 0x0

    aget v0, p1, v1

    :goto_6
    if-gt v1, p5, :cond_5

    add-int/lit8 v7, v1, 0x1

    aget v3, p1, v7

    sub-int v7, v3, v0

    add-int/2addr v6, v7

    move v0, v3

    add-int/lit8 v7, v6, -0x1

    aput v7, p0, v1

    shl-int/lit8 v6, v6, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_5
    add-int/lit8 v1, p4, 0x1

    :goto_7
    if-gt v1, p5, :cond_6

    add-int/lit8 v7, v1, -0x1

    aget v7, p0, v7

    add-int/lit8 v7, v7, 0x1

    shl-int/lit8 v7, v7, 0x1

    aget v8, p1, v1

    sub-int/2addr v7, v8

    aput v7, p1, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_6
    return-void

    :cond_7
    move v4, v5

    goto :goto_2
.end method

.method private init()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v1

    const/16 v2, 0x68

    if-eq v1, v2, :cond_0

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Stream is not BZip2 formatted: expected \'h\' as first byte but got \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    int-to-char v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v0

    const/16 v2, 0x31

    if-lt v0, v2, :cond_1

    const/16 v2, 0x39

    if-le v0, v2, :cond_2

    :cond_1
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Stream is not BZip2 formatted: illegal blocksize "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    int-to-char v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    add-int/lit8 v2, v0, -0x30

    iput v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->blockSize100k:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->initBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupBlock()V

    return-void
.end method

.method private initBlock()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0x59

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetUByte()C

    move-result v0

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetUByte()C

    move-result v1

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetUByte()C

    move-result v2

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetUByte()C

    move-result v3

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetUByte()C

    move-result v4

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetUByte()C

    move-result v5

    const/16 v8, 0x17

    if-ne v0, v8, :cond_0

    const/16 v8, 0x72

    if-ne v1, v8, :cond_0

    const/16 v8, 0x45

    if-ne v2, v8, :cond_0

    const/16 v8, 0x38

    if-ne v3, v8, :cond_0

    const/16 v8, 0x50

    if-ne v4, v8, :cond_0

    const/16 v8, 0x90

    if-ne v5, v8, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->complete()V

    :goto_0
    return-void

    :cond_0
    const/16 v8, 0x31

    if-ne v0, v8, :cond_1

    const/16 v8, 0x41

    if-ne v1, v8, :cond_1

    if-ne v2, v9, :cond_1

    const/16 v8, 0x26

    if-ne v3, v8, :cond_1

    const/16 v8, 0x53

    if-ne v4, v8, :cond_1

    if-eq v5, v9, :cond_2

    :cond_1
    iput v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    new-instance v6, Ljava/io/IOException;

    const-string v7, "bad block header"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_2
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetInt()I

    move-result v8

    iput v8, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->storedBlockCRC:I

    invoke-direct {p0, v7}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v8

    if-ne v8, v7, :cond_3

    move v6, v7

    :cond_3
    iput-boolean v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->blockRandomised:Z

    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    if-nez v6, :cond_4

    new-instance v6, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget v8, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->blockSize100k:I

    invoke-direct {v6, v8}, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;-><init>(I)V

    iput-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    :cond_4
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->getAndMoveToFrontDecode()V

    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v6}, Lorg/apache/tools/bzip2/CRC;->initialiseCRC()V

    iput v7, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    goto :goto_0
.end method

.method private makeMaps()V
    .locals 6

    iget-object v5, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v1, v5, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->inUse:[Z

    iget-object v5, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v4, v5, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->seqToUnseq:[B

    const/4 v2, 0x0

    const/4 v0, 0x0

    move v3, v2

    :goto_0
    const/16 v5, 0x100

    if-ge v0, v5, :cond_0

    aget-boolean v5, v1, v0

    if-eqz v5, :cond_1

    add-int/lit8 v2, v3, 0x1

    int-to-byte v5, v0

    aput-byte v5, v4, v3

    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    goto :goto_0

    :cond_0
    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->nInUse:I

    return-void

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private read0()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentChar:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :pswitch_2
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :pswitch_3
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupRandPartB()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupRandPartC()V

    goto :goto_0

    :pswitch_5
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    :pswitch_6
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupNoRandPartB()V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupNoRandPartC()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private recvDecodingTables()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v7, v4, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->inUse:[Z

    iget-object v14, v4, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->recvDecodingTables_pos:[B

    iget-object v15, v4, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->selector:[B

    iget-object v0, v4, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->selectorMtf:[B

    move-object/from16 v16, v0

    const/4 v8, 0x0

    const/4 v5, 0x0

    :goto_0
    const/16 v20, 0x10

    move/from16 v0, v20

    if-ge v5, v0, :cond_1

    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetBit()Z

    move-result v20

    if-eqz v20, :cond_0

    const/16 v20, 0x1

    shl-int v20, v20, v5

    or-int v8, v8, v20

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/16 v5, 0x100

    :goto_1
    add-int/lit8 v5, v5, -0x1

    if-ltz v5, :cond_2

    const/16 v20, 0x0

    aput-boolean v20, v7, v5

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_2
    const/16 v20, 0x10

    move/from16 v0, v20

    if-ge v5, v0, :cond_5

    const/16 v20, 0x1

    shl-int v20, v20, v5

    and-int v20, v20, v8

    if-eqz v20, :cond_4

    shl-int/lit8 v6, v5, 0x4

    const/4 v9, 0x0

    :goto_3
    const/16 v20, 0x10

    move/from16 v0, v20

    if-ge v9, v0, :cond_4

    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetBit()Z

    move-result v20

    if-eqz v20, :cond_3

    add-int v20, v6, v9

    const/16 v21, 0x1

    aput-boolean v21, v7, v20

    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->makeMaps()V

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2InputStream;->nInUse:I

    move/from16 v20, v0

    add-int/lit8 v2, v20, 0x2

    const/16 v20, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v12

    const/16 v20, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v13

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v13, :cond_7

    const/4 v9, 0x0

    :goto_5
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetBit()Z

    move-result v20

    if-eqz v20, :cond_6

    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_6
    int-to-byte v0, v9

    move/from16 v20, v0

    aput-byte v20, v16, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_7
    move/from16 v19, v12

    :goto_6
    add-int/lit8 v19, v19, -0x1

    if-ltz v19, :cond_8

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v20, v0

    aput-byte v20, v14, v19

    goto :goto_6

    :cond_8
    const/4 v5, 0x0

    :goto_7
    if-ge v5, v13, :cond_a

    aget-byte v20, v16, v5

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    aget-byte v18, v14, v19

    :goto_8
    if-lez v19, :cond_9

    add-int/lit8 v20, v19, -0x1

    aget-byte v20, v14, v20

    aput-byte v20, v14, v19

    add-int/lit8 v19, v19, -0x1

    goto :goto_8

    :cond_9
    const/16 v20, 0x0

    aput-byte v18, v14, v20

    aput-byte v18, v15, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_a
    iget-object v10, v4, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->temp_charArray2d:[[C

    const/16 v17, 0x0

    :goto_9
    move/from16 v0, v17

    if-ge v0, v12, :cond_e

    const/16 v20, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsR(I)I

    move-result v3

    aget-object v11, v10, v17

    const/4 v5, 0x0

    :goto_a
    if-ge v5, v2, :cond_d

    :goto_b
    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetBit()Z

    move-result v20

    if-eqz v20, :cond_c

    invoke-direct/range {p0 .. p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->bsGetBit()Z

    move-result v20

    if-eqz v20, :cond_b

    const/16 v20, -0x1

    :goto_c
    add-int v3, v3, v20

    goto :goto_b

    :cond_b
    const/16 v20, 0x1

    goto :goto_c

    :cond_c
    int-to-char v0, v3

    move/from16 v20, v0

    aput-char v20, v11, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :cond_d
    add-int/lit8 v17, v17, 0x1

    goto :goto_9

    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v12}, Lorg/apache/tools/bzip2/CBZip2InputStream;->createHuffmanDecodingTables(II)V

    return-void
.end method

.method private static reportCRCError()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "BZip2 CRC error"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private setupBlock()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v10, 0x100

    const/4 v9, 0x0

    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v1, v6, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->cftab:[I

    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget v7, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->last:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->initTT(I)[I

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v4, v6, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->ll8:[B

    aput v9, v1, v9

    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v6, v6, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->unzftab:[I

    const/4 v7, 0x1

    invoke-static {v6, v9, v1, v7, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v2, 0x1

    aget v0, v1, v9

    :goto_1
    if-gt v2, v10, :cond_1

    aget v6, v1, v2

    add-int/2addr v0, v6

    aput v0, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->last:I

    :goto_2
    if-gt v2, v3, :cond_2

    aget-byte v6, v4, v2

    and-int/lit16 v6, v6, 0xff

    aget v7, v1, v6

    add-int/lit8 v8, v7, 0x1

    aput v8, v1, v6

    aput v2, v5, v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    iget v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->origPtr:I

    if-ltz v6, :cond_3

    iget v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->origPtr:I

    array-length v7, v5

    if-lt v6, v7, :cond_4

    :cond_3
    new-instance v6, Ljava/io/IOException;

    const-string v7, "stream corrupted"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_4
    iget v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->origPtr:I

    aget v6, v5, v6

    iput v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    iput v9, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    iput v9, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    iput v10, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iget-boolean v6, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->blockRandomised:Z

    if-eqz v6, :cond_5

    iput v9, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    iput v9, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupRandPartA()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    goto :goto_0
.end method

.method private setupNoRandPartA()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    iget v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->last:I

    if-gt v1, v2, :cond_0

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_chPrev:I

    iget-object v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v1, v1, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->ll8:[B

    iget v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget-byte v1, v1, v2

    and-int/lit16 v0, v1, 0xff

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iget-object v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v1, v1, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->tt:[I

    iget v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentChar:I

    const/4 v1, 0x6

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    iget-object v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v1, v0}, Lorg/apache/tools/bzip2/CRC;->updateCRC(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x5

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->endBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->initBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupBlock()V

    goto :goto_0
.end method

.method private setupNoRandPartB()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_chPrev:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->ll8:[B

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    int-to-char v0, v0

    iput-char v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_z:C

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->tt:[I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupNoRandPartC()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    goto :goto_0
.end method

.method private setupNoRandPartC()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    iget-char v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_z:C

    if-ge v1, v2, :cond_0

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentChar:I

    iget-object v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v1, v0}, Lorg/apache/tools/bzip2/CRC;->updateCRC(I)V

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    const/4 v1, 0x7

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupNoRandPartA()V

    goto :goto_0
.end method

.method private setupRandPartA()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    iget v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->last:I

    if-gt v3, v4, :cond_3

    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_chPrev:I

    iget-object v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v3, v3, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->ll8:[B

    iget v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget-byte v3, v3, v4

    and-int/lit16 v0, v3, 0xff

    iget-object v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v3, v3, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->tt:[I

    iget v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget v3, v3, v4

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    if-nez v3, :cond_1

    sget-object v3, Lorg/apache/tools/bzip2/BZip2Constants;->rNums:[I

    iget v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    aget v3, v3, v4

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    const/16 v4, 0x200

    if-ne v3, v4, :cond_0

    iput v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    :cond_0
    :goto_0
    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    if-ne v3, v1, :cond_2

    :goto_1
    xor-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentChar:I

    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    iget-object v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v1, v0}, Lorg/apache/tools/bzip2/CRC;->updateCRC(I)V

    :goto_2
    return-void

    :cond_1
    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->endBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->initBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupBlock()V

    goto :goto_2
.end method

.method private setupRandPartB()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_chPrev:I

    if-eq v0, v1, :cond_0

    iput v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupRandPartA()V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    if-lt v0, v5, :cond_4

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->ll8:[B

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    int-to-char v0, v0

    iput-char v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_z:C

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2InputStream$Data;->tt:[I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_tPos:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    if-nez v0, :cond_3

    sget-object v0, Lorg/apache/tools/bzip2/BZip2Constants;->rNums:[I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    aget v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    const/16 v1, 0x200

    if-ne v0, v1, :cond_1

    iput v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rTPos:I

    :cond_1
    :goto_1
    iput v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    iput v5, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    if-ne v0, v3, :cond_2

    iget-char v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_z:C

    xor-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    iput-char v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_z:C

    :cond_2
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupRandPartC()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_rNToGo:I

    goto :goto_1

    :cond_4
    iput v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupRandPartA()V

    goto :goto_0
.end method

.method private setupRandPartC()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    iget-char v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_z:C

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentChar:I

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_ch2:I

    invoke-virtual {v0, v1}, Lorg/apache/tools/bzip2/CRC;->updateCRC(I)V

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_j2:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->currentState:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_i2:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->su_count:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->setupRandPartA()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    :try_start_0
    sget-object v1, Ljava/lang/System;->in:Ljava/io/InputStream;

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->data:Lorg/apache/tools/bzip2/CBZip2InputStream$Data;

    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    throw v1
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->read0()I

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([BII)I
    .locals 7
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-gez p2, :cond_0

    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "offs("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") < 0."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    if-gez p3, :cond_1

    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "len("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") < 0."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    add-int v4, p2, p3

    array-length v5, p1

    if-le v4, v5, :cond_2

    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "offs("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") + len("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") > dest.length("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    array-length v6, p1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ")."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/bzip2/CBZip2InputStream;->in:Ljava/io/InputStream;

    if-nez v4, :cond_3

    new-instance v4, Ljava/io/IOException;

    const-string v5, "stream closed"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3
    add-int v3, p2, p3

    move v1, p2

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_4

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2InputStream;->read0()I

    move-result v0

    if-ltz v0, :cond_4

    add-int/lit8 v1, v2, 0x1

    int-to-byte v4, v0

    aput-byte v4, p1, v2

    move v2, v1

    goto :goto_0

    :cond_4
    if-ne v2, p2, :cond_5

    const/4 v4, -0x1

    :goto_1
    return v4

    :cond_5
    sub-int v4, v2, p2

    goto :goto_1
.end method
