.class public Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "PictureDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/PictureDesk;


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_Dynamic_Contrast:[I

.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_LOCALDIMMING:[I

.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_MPEG_NR:[I

.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SkinToneMode:[I

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

.field private static pictureMgrImpl:Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;


# instance fields
.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

.field enableSetBacklight:Z

.field private videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_Dynamic_Contrast()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_Dynamic_Contrast:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_Dynamic_Contrast:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_LOCALDIMMING()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_LOCALDIMMING:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->LOCALDIMMING_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->LOCALDIMMING_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->LOCALDIMMING_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_LOCALDIMMING:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_MPEG_NR()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_MPEG_NR:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_HIGH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_LOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_MPEG_NR:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_HIGH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_LOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SkinToneMode()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SkinToneMode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->SKIN_TONE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->SKIN_TONE_RED:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->SKIN_TONE_YELLOW:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SkinToneMode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->pictureMgrImpl:Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->enableSetBacklight:Z

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    return-void
.end method

.method public static getPictureMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->pictureMgrImpl:Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->pictureMgrImpl:Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->pictureMgrImpl:Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;

    return-object v0
.end method


# virtual methods
.method public ExecAutoPc()Z
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "ExecAutoPc: Start!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl$1;

    invoke-direct {v1, p0}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl$1;-><init>(Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "ExecAutoPc: End!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public ExecVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;S)Z
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;
    .param p2    # S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iput-short p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iput-short p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iput-short p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iput-short p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iput-short p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iput-short p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public GetBacklight()S
    .locals 4

    const/4 v0, 0x0

    const/16 v1, 0x50

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v0

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {p0, v3}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v0

    iget-short v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    return v2
.end method

.method public GetBacklightOfPicMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)S
    .locals 2
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v0

    iget-short v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    return v1
.end method

.method public GetColorTempIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryColorTemp(II)I

    move-result v0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v3

    aget-object v3, v3, v0

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    return-object v2
.end method

.method public GetColorTempPara()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideoTempEx()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    move-result-object v0

    return-object v0
.end method

.method public GetMpegNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;
    .locals 5

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetMpegNR:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    return-object v1
.end method

.method public GetNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;
    .locals 5

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GetNR:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    return-object v1
.end method

.method public GetPCClock()S
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCClock()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public GetPCHPos()S
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCHPos()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public GetPCModeIndex(I)S
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public GetPCPhase()S
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCPhase()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public GetPCVPos()S
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPCVPos()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public GetPictureModeIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    return-object v0
.end method

.method public GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;
    .locals 4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryVideoArcMode(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetVideoArcIdx:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    return-object v0
.end method

.method public GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S
    .locals 5
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v0

    const/16 v1, 0x50

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_VIDEOITEM()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvService"

    const-string v4, "Haven\'t this item internal error !!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v1

    :pswitch_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v2, p1, v3, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v2, p1, v3, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v2, p1, v3, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v2, p1, v3, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v2, p1, v3, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2, p1, v4, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPicModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public SetBacklight(S)Z
    .locals 5
    .param p1    # S

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v1

    iput-short p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v3, v3, v1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoAstPicture(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;II)V

    iget-boolean v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->enableSetBacklight:Z

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetColorTempIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;)Z
    .locals 14
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "SetColorTempIdx nothing to do!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v0, v0, v11

    iput-object p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    new-instance v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;

    invoke-direct {v13}, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;-><init>()V

    const/4 v8, 0x0

    const/4 v12, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    :cond_0
    :goto_0
    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v0

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "Haven\'t this input source type !!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget v0, v12, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    iget v0, v12, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    iget v0, v12, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->buleGain:S

    iget v0, v12, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    iget v0, v12, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    iget v0, v12, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->values()[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    aget-object v1, v1, v2

    iget-short v2, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    iget-short v3, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    iget-short v4, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->buleGain:S

    iget-short v5, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    iget-short v6, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    iget-short v7, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    invoke-virtual/range {v0 .. v8}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setWbGainOffsetEx(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;IIIIIILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v11

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2, v11}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoAstPicture(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;II)V

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_ATV:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_CVBS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SVIDEO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_YPBPR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SCART:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_7
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_HDMI:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_8
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_DTV:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_9
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :catch_1
    move-exception v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_0
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public SetColorTempPara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;)Z
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setVideoTempEx(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;)Z

    move-result v0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2, p1, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUsrColorTmpExData(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;I)V

    return v0
.end method

.method public SetKTVVideoArc()Z
    .locals 2

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;-><init>()V

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;->E_ASP_16TO9:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->setAspectRatioCode(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;)V

    const/4 v1, 0x1

    return v1
.end method

.method public SetMpegNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)Z
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_OFF:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v3, v3, v1

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "TvService"

    const-string v5, "SetMpegNR nothing to do!!"

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v3, v3, v1

    iput-object p1, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoNRMode(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;II)V

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_MPEG_NR()[I

    move-result-object v3

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setMpegNoiseReduction(Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v3, 0x1

    return v3

    :pswitch_0
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_OFF:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_LOW:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    goto :goto_0

    :pswitch_2
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_MIDDLE:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    goto :goto_0

    :pswitch_3
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_HIGH:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    goto :goto_0

    :pswitch_4
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_NUM:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public SetNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v3, v3, v1

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v3, v3, v1

    iput-object p1, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoNRMode(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;II)V

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR()[I

    move-result-object v3

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setNoiseReduction(Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v3, 0x1

    return v3

    :pswitch_0
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_LOW:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_2
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIDDLE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_3
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_HIGH:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_4
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_AUTO:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public SetPCClock(S)Z
    .locals 4
    .param p1    # S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    const-string v3, "SetPCVPos Parameter!!"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setSize(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetPCHPos(S)Z
    .locals 4
    .param p1    # S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    const-string v3, "SetPCHPos Parameter!!"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setHPosition(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetPCPhase(S)Z
    .locals 4
    .param p1    # S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    const-string v3, "SetPCPhase Parameter!!"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setPhase(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetPCVPos(S)Z
    .locals 4
    .param p1    # S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    const-string v3, "SetPCVPos Parameter!!"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setVPosition(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z
    .locals 5
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object p1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iget-short v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iget-short v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iget-short v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iget-short v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iget-short v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoAstPicture(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;II)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetVideoArc(Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)Z
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    const/4 v7, 0x1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getS3DMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getS3DMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getDisplayFormat()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-eq v2, v3, :cond_1

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v3

    if-ge v2, v3, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    move-result-object v2

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v3

    aget-object v0, v2, v3

    :cond_2
    :goto_1
    :try_start_1
    const-string v2, "zoom"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "arcType==========="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->setAspectRatio(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getS3DMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getS3DMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getDisplayFormat()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-eq v2, v3, :cond_5

    :cond_4
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_5
    :goto_3
    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_2

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_16x9:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method public UpdateVideoItem()V
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoAstPicture(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;II)V

    return-void
.end method

.method public disableBacklight()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->disableBacklight()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableBacklight()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->enableBacklight()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableSetBacklight(Z)Z
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->enableSetBacklight:Z

    const/4 v0, 0x1

    return v0
.end method

.method public getCustomerPqRuleNumber()I
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->getCustomerPqRuleNumber()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getDNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->DNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    return-object v0
.end method

.method public getDetailEnhance()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->detailEnhance:Z

    return v0
.end method

.method public getDlcAverageLuma()S
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->getDlcAverageLuma()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDlcLumArray(I)[I
    .locals 2
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->getDlcLumArray(I)[I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDynamicBLModeIdx()I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDynamicBLMode()I

    move-result v0

    return v0
.end method

.method public getDynamicContrast()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    return-object v0
.end method

.method public getDynamicContrastCurve()[I
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->getDynamicContrastCurve()[I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEnergyEnable()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getCustomerCfgMiscSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    move-result-object v0

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyEnable:Z

    return v0
.end method

.method public getEnergyPercent()S
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getCustomerCfgMiscSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyPercent:S

    return v0
.end method

.method public getSkinToneMode()Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->skinTone:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    return-object v0
.end method

.method public getStatusNumberByCustomerPqRule(I)I
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->getStatusNumberByCustomerPqRule(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public refreshVideoPara()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const/4 v0, 0x1

    return v0
.end method

.method public setDNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z
    .locals 5
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->DNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_AUTO:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setNoiseReduction(Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)Z

    :cond_0
    :goto_1
    const/4 v2, 0x1

    return v2

    :pswitch_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_LOW:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIDDLE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_HIGH:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    goto :goto_0

    :pswitch_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIDDLE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setDetailEnhance(Z)Z
    .locals 6
    .param p1    # Z

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-boolean p1, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->detailEnhance:Z

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_DETAILS_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    if-eqz p1, :cond_1

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_DETAILS_Index_Main;->PQ_GRule_DETAILS_On_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_DETAILS_Index_Main;

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->ordinal()I

    move-result v4

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_DETAILS_Index_Main;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/PictureManager;->setStatusByCustomerPqRule(II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v3, 0x1

    return v3

    :cond_1
    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_DETAILS_Index_Main;->PQ_GRule_DETAILS_Off_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_DETAILS_Index_Main;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDynamicBLModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;)Z
    .locals 3
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_LOCALDIMMING()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetLocalDimmingMode_Off"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetLocalDimmingMode_On"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDynamicContrast(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;)Z
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object p1, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_COLOR_LEVEL_STRTECH_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_Dynamic_Contrast()[I

    move-result-object v3

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->PQ_GRule_COLOR_LEVEL_STRETCH_Off_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->ordinal()I

    move-result v4

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/PictureManager;->setStatusByCustomerPqRule(II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v3, 0x1

    return v3

    :pswitch_0
    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->PQ_GRule_COLOR_LEVEL_STRETCH_On_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public setEnergyEnable(Z)Z
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    return v0
.end method

.method public setEnergyPercent(S)Z
    .locals 1
    .param p1    # S

    const/4 v0, 0x0

    return v0
.end method

.method public setSkinToneMode(Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;)Z
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object p1, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->skinTone:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->videoPara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoBasePara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;I)V

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->PQ_GRule_SKIN_TONE_Main_Color:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SkinToneMode()[I

    move-result-object v3

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_SKIN_TONE_Index_Main;->PQ_GRule_SKIN_TONE_Off_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_SKIN_TONE_Index_Main;

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_Index_Main;->ordinal()I

    move-result v4

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_SKIN_TONE_Index_Main;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/PictureManager;->setStatusByCustomerPqRule(II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v3, 0x1

    return v3

    :pswitch_0
    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_SKIN_TONE_Index_Main;->PQ_GRule_SKIN_TONE_Red_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_SKIN_TONE_Index_Main;

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_SKIN_TONE_Index_Main;->PQ_GRule_SKIN_TONE_Yellow_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_SKIN_TONE_Index_Main;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setStatusByCustomerPqRule(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->setStatusByCustomerPqRule(II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
